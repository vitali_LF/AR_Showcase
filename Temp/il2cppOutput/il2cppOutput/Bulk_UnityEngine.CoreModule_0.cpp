﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t3130506276;
// System.Type
struct Type_t;
// System.Attribute
struct Attribute_t3130080784;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t566310773;
// System.String
struct String_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2893751101;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t662635263;
// UnityEngine.Application/LowMemoryCallback
struct LowMemoryCallback_t1978334135;
// UnityEngine.Application/LogCallback
struct LogCallback_t2572717340;
// UnityEngine.Events.UnityAction
struct UnityAction_t191347160;
// System.IAsyncResult
struct IAsyncResult_t4224419158;
// System.AsyncCallback
struct AsyncCallback_t4283869127;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3797817720;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t1275866152;
// System.Action`1<System.Object>
struct Action_1_t2325042916;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t3041366545;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2545034337;
// System.Type[]
struct TypeU5BU5D_t98989581;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t2722562487;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2226230279;
// System.Object[]
struct ObjectU5BU5D_t3385344125;
// UnityEngine.RequireComponent
struct RequireComponent_t3219866953;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t1108411129;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t4131774042;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t2915138156;
// UnityEngine.Behaviour
struct Behaviour_t3297694025;
// UnityEngine.Component
struct Component_t2335505321;
// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t1385813216;
// UnityEngine.Camera
struct Camera_t362346687;
// UnityEngine.RenderTexture
struct RenderTexture_t3737750657;
// UnityEngine.Camera[]
struct CameraU5BU5D_t365668710;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t3349755816;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t3383400165;
// UnityEngine.GameObject
struct GameObject_t2881801184;
// UnityEngine.Object
struct Object_t3645472222;
// UnityEngine.Transform
struct Transform_t3580444445;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t4009708412;
// UnityEngine.Coroutine
struct Coroutine_t260305101;
// UnityEngine.YieldInstruction
struct YieldInstruction_t1540633877;
// UnityEngine.CSSLayout.CSSMeasureFunc
struct CSSMeasureFunc_t2649428469;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct Dictionary_2_t1470255310;
// System.WeakReference
struct WeakReference_t1433439652;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t588842846;
// UnityEngine.CullingGroup
struct CullingGroup_t637827745;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t1464331389;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t2091205422;
// UnityEngine.ILogger
struct ILogger_t1005929009;
// System.Exception
struct Exception_t214279536;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t2540765360;
// UnityEngine.Logger
struct Logger_t4035514390;
// UnityEngine.ILogHandler
struct ILogHandler_t4143530507;
// UnityEngine.Display
struct Display_t81008966;
// System.IntPtr[]
struct IntPtrU5BU5D_t3930959911;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t1656811884;
// UnityEngine.RectTransform
struct RectTransform_t1161610249;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t537934761;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t3648497698;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.ArgumentNullException
struct ArgumentNullException_t246139103;
// System.Delegate
struct Delegate_t4015201940;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t1976387117;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t3172597217;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t1027733493;
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct Predicate_1_t959054467;
// System.Predicate`1<System.Object>
struct Predicate_1_t2157551253;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerable_1_t3735452067;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t638981557;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1968425864;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t2081553920;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t404005347;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t3021784382;
// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t2765728243;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t58234032;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t1946426838;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3011499035;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3408587689;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t3642628955;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t4085066839;
// System.Reflection.Binder
struct Binder_t2026886273;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1706708300;
// System.Reflection.ParameterInfo
struct ParameterInfo_t3156340899;
// UnityEngine.Experimental.Rendering.IRenderPipeline
struct IRenderPipeline_t1337497652;
// UnityEngine.Experimental.Rendering.IRenderPipelineAsset
struct IRenderPipelineAsset_t3550769973;
// UnityEngine.Gradient
struct Gradient_t2667682126;
// UnityEngine.GUIElement
struct GUIElement_t2757512779;
// UnityEngine.GUILayer
struct GUILayer_t1634068716;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t1170597384;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4289523592;
// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t1993002847;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t1912603919;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t1059923024;
// UnityEngine.iOS.LocalNotification
struct LocalNotification_t2192530767;
// UnityEngine.iOS.RemoteNotification
struct RemoteNotification_t357781426;
// UnityEngine.Light
struct Light_t455885974;
// System.IO.Stream
struct Stream_t360920014;
// System.ArgumentException
struct ArgumentException_t489606696;
// System.Byte[]
struct ByteU5BU5D_t3567143369;
// UnityEngine.Material
struct Material_t1926439680;
// UnityEngine.Texture
struct Texture_t954505614;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3731058050;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3659678378;
// UnityEngine.Mesh
struct Mesh_t4237566844;
// System.Int32[]
struct Int32U5BU5D_t3215173111;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t894813333;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1432878832;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1632896738;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3868613195;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2352998046;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1311628880;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t3575182278;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t2336627130;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3920572369;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4008345588;
// System.Collections.IEnumerator
struct IEnumerator_t2051810174;
// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t417698780;
// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct MessageEventArgs_t2772358345;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3190352615;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t1009044977;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1324617639;
// UnityEngine.IPlayerEditorConnectionNative
struct IPlayerEditorConnectionNative_t3906153058;
// UnityEngine.PlayerConnectionInternal
struct PlayerConnectionInternal_t3959366414;
// UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityAction_1_t1148388365;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerable_1_t762398783;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityEvent_1_t1459204717;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3533840856;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3223024504;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t1891607558;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2202423910;
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t2868271383;
// System.Collections.IDictionary
struct IDictionary_t1084331215;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t1362056370;
// System.WeakReference[]
struct WeakReferenceU5BU5D_t2467492685;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_t2692725172;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1812684756;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.WeakReference,System.Collections.DictionaryEntry>
struct Transform_1_t3581913007;
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t139144537;
// UnityEngine.Gyroscope
struct Gyroscope_t3917155460;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers[]
struct MessageTypeSubscribersU5BU5D_t1032573067;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct List_1_t2349647505;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct ConnectionChangeEvent_t190635484;
// UnityEngine.Component[]
struct ComponentU5BU5D_t3701357460;
// System.Char[]
struct CharU5BU5D_t2953840665;
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t3764267775;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1903432293;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t3043391348;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t816250014;
// System.Boolean[]
struct BooleanU5BU5D_t1963761663;
// System.Void
struct Void_t4071739332;
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t1635551419;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3568795819;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2272071576;
// UnityEngine.Display[]
struct DisplayU5BU5D_t2949376739;
// System.DelegateData
struct DelegateData_t2468626509;
// System.Reflection.MemberFilter
struct MemberFilter_t2882044324;
// System.String[]
struct StringU5BU5D_t1589106382;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t4202419344;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1147600571;

extern RuntimeClass* Application_t1595095978_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLowMemory_m1111488080_MetadataUsageId;
extern const uint32_t Application_CallLogCallback_m1390027458_MetadataUsageId;
extern const uint32_t Application_InvokeOnBeforeRender_m3241731550_MetadataUsageId;
extern RuntimeClass* LogType_t3043133256_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m2650058531_MetadataUsageId;
extern RuntimeClass* Action_1_t1275866152_il2cpp_TypeInfo_var;
extern const uint32_t AssetBundleCreateRequest_t2350432906_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleCreateRequest_t2350432906_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t3347087835_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AssetBundleRequest_t3347087835_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t3797817720_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t AsyncOperation_t3797817720_com_FromNativeMethodDefinition_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m2532647139_RuntimeMethod_var;
extern const uint32_t AsyncOperation_InvokeCompletionEvent_m2214301657_MetadataUsageId;
extern const RuntimeType* MonoBehaviour_t4008345588_0_0_0_var;
extern const RuntimeType* DisallowMultipleComponent_t4131774042_0_0_0_var;
extern RuntimeClass* Stack_1_t3041366545_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Stack_1__ctor_m3056069383_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m1690848609_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m2531133666_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m234060416_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m230801942_MetadataUsageId;
extern const RuntimeType* RequireComponent_t3219866953_0_0_0_var;
extern RuntimeClass* RequireComponentU5BU5D_t3043391348_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t98989581_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2722562487_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m946701582_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3025470644_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m2051283729_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m2635372436_MetadataUsageId;
extern const RuntimeType* ExecuteInEditMode_t2915138156_0_0_0_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m397014713_MetadataUsageId;
extern RuntimeClass* AttributeHelperEngine_t2253936689_il2cpp_TypeInfo_var;
extern const RuntimeMethod* AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t1108411129_m62422214_RuntimeMethod_var;
extern const uint32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m784596665_MetadataUsageId;
extern RuntimeClass* DisallowMultipleComponentU5BU5D_t3764267775_il2cpp_TypeInfo_var;
extern RuntimeClass* ExecuteInEditModeU5BU5D_t1903432293_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m1940743422_MetadataUsageId;
extern RuntimeClass* Vector3_t3932393085_il2cpp_TypeInfo_var;
extern const uint32_t Bounds__ctor_m3205218803_MetadataUsageId;
extern RuntimeClass* Bounds_t3940617471_il2cpp_TypeInfo_var;
extern const uint32_t Bounds_Equals_m3886448245_MetadataUsageId;
extern const uint32_t Bounds_get_size_m3922782264_MetadataUsageId;
extern const uint32_t Bounds_set_size_m3828080681_MetadataUsageId;
extern const uint32_t Bounds_get_min_m2956001499_MetadataUsageId;
extern const uint32_t Bounds_get_max_m1445821865_MetadataUsageId;
extern const uint32_t Bounds_op_Equality_m4088733844_MetadataUsageId;
extern const uint32_t Bounds_SetMinMax_m1614096005_MetadataUsageId;
extern const uint32_t Bounds_Encapsulate_m3006561347_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2570740258;
extern const uint32_t Bounds_ToString_m3206324941_MetadataUsageId;
extern RuntimeClass* Camera_t362346687_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m72709191_MetadataUsageId;
extern const uint32_t Camera_FireOnPreRender_m4106667072_MetadataUsageId;
extern const uint32_t Camera_FireOnPostRender_m4047589566_MetadataUsageId;
extern RuntimeClass* Single_t897798503_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1036966564;
extern const uint32_t Color_ToString_m961366009_MetadataUsageId;
extern RuntimeClass* Color_t1361298052_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m249927524_MetadataUsageId;
extern RuntimeClass* Vector4_t1900979187_il2cpp_TypeInfo_var;
extern const uint32_t Color_op_Equality_m2552885420_MetadataUsageId;
extern RuntimeClass* Mathf_t9189715_il2cpp_TypeInfo_var;
extern const uint32_t Color_Lerp_m1950920091_MetadataUsageId;
extern const uint32_t Color32_op_Implicit_m1747497104_MetadataUsageId;
extern RuntimeClass* Byte_t2640180184_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2277880093;
extern const uint32_t Color32_ToString_m649264193_MetadataUsageId;
extern RuntimeClass* Object_t3645472222_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m3044674138_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureMode_t50254874_il2cpp_TypeInfo_var;
extern const uint32_t CSSMeasureFunc_BeginInvoke_m183478383_MetadataUsageId;
extern RuntimeClass* Native_t1351913248_il2cpp_TypeInfo_var;
extern RuntimeClass* CSSMeasureFunc_t2649428469_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2231896620_RuntimeMethod_var;
extern const uint32_t Native_CSSNodeGetMeasureFunc_m485971160_MetadataUsageId;
extern const uint32_t Native_CSSNodeMeasureInvoke_m3401257635_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1470255310_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2012856830_RuntimeMethod_var;
extern const uint32_t Native__cctor_m3423934896_MetadataUsageId;
extern RuntimeClass* StateChanged_t1464331389_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t637827745_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_t637827745_com_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_Finalize_m1014941570_MetadataUsageId;
extern RuntimeClass* CullingGroupEvent_t505730933_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_SendEvents_m4083933383_MetadataUsageId;
extern const uint32_t StateChanged_BeginInvoke_m653102028_MetadataUsageId;
extern RuntimeClass* Debug_t959145111_il2cpp_TypeInfo_var;
extern const uint32_t Debug_get_unityLogger_m866078178_MetadataUsageId;
extern RuntimeClass* ILogger_t1005929009_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m3687530845_MetadataUsageId;
extern const uint32_t Debug_LogError_m2611403583_MetadataUsageId;
extern const uint32_t Debug_LogError_m2528395896_MetadataUsageId;
extern RuntimeClass* ILogHandler_t4143530507_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogErrorFormat_m1494465668_MetadataUsageId;
extern const uint32_t Debug_LogException_m1375071567_MetadataUsageId;
extern const uint32_t Debug_LogException_m3647006827_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m4161798195_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m3675514459_MetadataUsageId;
extern const uint32_t Debug_LogWarningFormat_m3546591144_MetadataUsageId;
extern RuntimeClass* DebugLogHandler_t2540765360_il2cpp_TypeInfo_var;
extern RuntimeClass* Logger_t4035514390_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m649192451_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLogHandler_LogFormat_m1693934444_MetadataUsageId;
extern RuntimeClass* Display_t81008966_il2cpp_TypeInfo_var;
extern const uint32_t Display_get_renderingWidth_m4193687592_MetadataUsageId;
extern const uint32_t Display_get_renderingHeight_m767707977_MetadataUsageId;
extern const uint32_t Display_get_systemWidth_m3725717722_MetadataUsageId;
extern const uint32_t Display_get_systemHeight_m1986252539_MetadataUsageId;
extern const uint32_t Display_RelativeMouseAt_m1286882000_MetadataUsageId;
extern RuntimeClass* DisplayU5BU5D_t2949376739_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m3667820612_MetadataUsageId;
extern const uint32_t Display_FireDisplaysUpdated_m1036156850_MetadataUsageId;
extern const uint32_t Display__cctor_m1902176577_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4204795500;
extern Il2CppCodeGenString* _stringLiteral4083982080;
extern Il2CppCodeGenString* _stringLiteral2925840332;
extern Il2CppCodeGenString* _stringLiteral2761045697;
extern Il2CppCodeGenString* _stringLiteral1699466734;
extern Il2CppCodeGenString* _stringLiteral126451750;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m464151535_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t246139103_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4055933771;
extern Il2CppCodeGenString* _stringLiteral1338428238;
extern const uint32_t BaseInvokableCall__ctor_m2255002992_MetadataUsageId;
extern const uint32_t BaseInvokableCall_AllowInvoke_m3930864202_MetadataUsageId;
extern const RuntimeType* UnityAction_t191347160_0_0_0_var;
extern RuntimeClass* UnityAction_t191347160_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m631237306_MetadataUsageId;
extern const uint32_t InvokableCall_add_Delegate_m660321946_MetadataUsageId;
extern const uint32_t InvokableCall_remove_Delegate_m4267960666_MetadataUsageId;
extern RuntimeClass* List_1_t1027733493_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2996853262_RuntimeMethod_var;
extern const uint32_t InvokableCallList__ctor_m3925837574_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m116104406_RuntimeMethod_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m678893588_MetadataUsageId;
extern const uint32_t InvokableCallList_AddListener_m3921073616_MetadataUsageId;
extern RuntimeClass* Predicate_1_t959054467_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m4078513680_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1195210665_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m1890478580_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m106168190_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m601647338_RuntimeMethod_var;
extern const uint32_t InvokableCallList_RemoveListener_m3353336194_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3693544616_RuntimeMethod_var;
extern const uint32_t InvokableCallList_ClearPersistent_m2913537599_MetadataUsageId;
extern const RuntimeMethod* List_1_AddRange_m55576690_RuntimeMethod_var;
extern const uint32_t InvokableCallList_PrepareInvoke_m965587067_MetadataUsageId;
extern RuntimeClass* ArgumentCache_t537934761_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m3715022066_MetadataUsageId;
extern const uint32_t PersistentCall_IsValid_m2393902497_MetadataUsageId;
extern RuntimeClass* CachedInvokableCall_1_t404005347_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t3021784382_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t2765728243_il2cpp_TypeInfo_var;
extern RuntimeClass* CachedInvokableCall_1_t1946426838_il2cpp_TypeInfo_var;
extern RuntimeClass* InvokableCall_t1976387117_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m2394803162_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m1010281405_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m163013044_RuntimeMethod_var;
extern const RuntimeMethod* CachedInvokableCall_1__ctor_m4230261306_RuntimeMethod_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m2894046812_MetadataUsageId;
extern const RuntimeType* Object_t3645472222_0_0_0_var;
extern const RuntimeType* CachedInvokableCall_1_t1723064271_0_0_0_var;
extern const RuntimeType* MethodInfo_t_0_0_0_var;
extern RuntimeClass* BaseInvokableCall_t3648497698_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m155688820_MetadataUsageId;
extern RuntimeClass* List_1_t3642628955_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1383490399_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup__ctor_m158630386_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m4154154611_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2770969459_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1658440964_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2859623709_RuntimeMethod_var;
extern const uint32_t PersistentCallGroup_Initialize_m1336092061_MetadataUsageId;
extern const uint32_t UnityEvent_FindMethod_Impl_m1609998361_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m3904563982_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m3591116204_MetadataUsageId;
extern const uint32_t UnityEvent_Invoke_m1855632032_MetadataUsageId;
extern RuntimeClass* InvokableCallList_t3172597217_il2cpp_TypeInfo_var;
extern RuntimeClass* PersistentCallGroup_t3408587689_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventBase__ctor_m1267297027_MetadataUsageId;
extern const uint32_t UnityEventBase_FindMethod_m3743863011_MetadataUsageId;
extern const RuntimeType* Single_t897798503_0_0_0_var;
extern const RuntimeType* Int32_t3515577538_0_0_0_var;
extern const RuntimeType* Boolean_t2440219994_0_0_0_var;
extern const RuntimeType* String_t_0_0_0_var;
extern const uint32_t UnityEventBase_FindMethod_m2734136314_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral558356722;
extern const uint32_t UnityEventBase_ToString_m3570276379_MetadataUsageId;
extern const RuntimeType* RuntimeObject_0_0_0_var;
extern const uint32_t UnityEventBase_GetValidMethodInfo_m1984273667_MetadataUsageId;
extern RuntimeClass* RenderPipelineManager_t280631689_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_get_currentPipeline_m2834317064_MetadataUsageId;
extern const uint32_t RenderPipelineManager_set_currentPipeline_m603220370_MetadataUsageId;
extern RuntimeClass* IRenderPipelineAsset_t3550769973_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_CleanupRenderPipeline_m1732771618_MetadataUsageId;
extern RuntimeClass* IRenderPipeline_t1337497652_il2cpp_TypeInfo_var;
extern const uint32_t RenderPipelineManager_DoRenderLoop_Internal_m435582623_MetadataUsageId;
extern const uint32_t RenderPipelineManager_PrepareRenderPipeline_m4070566852_MetadataUsageId;
extern const uint32_t GameObject__ctor_m2683929848_MetadataUsageId;
extern const uint32_t GameObject__ctor_m4156075956_MetadataUsageId;
extern const uint32_t GameObject__ctor_m4126152586_MetadataUsageId;
extern RuntimeClass* Input_t1636556003_il2cpp_TypeInfo_var;
extern const uint32_t Input_get_mousePosition_m3071837934_MetadataUsageId;
extern const uint32_t Input_get_mouseScrollDelta_m3998771486_MetadataUsageId;
extern const uint32_t Input_GetTouch_m1074987538_MetadataUsageId;
extern const uint32_t Input_get_compositionCursorPos_m2540437126_MetadataUsageId;
extern const uint32_t Input_set_compositionCursorPos_m2547795730_MetadataUsageId;
extern const uint32_t Input__cctor_m1061768007_MetadataUsageId;
extern RuntimeClass* DefaultValueAttribute_t1912603919_il2cpp_TypeInfo_var;
extern const uint32_t DefaultValueAttribute_Equals_m4044023824_MetadataUsageId;
extern RuntimeClass* LocalNotification_t2192530767_il2cpp_TypeInfo_var;
extern const uint32_t LocalNotification__cctor_m1797189921_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3399283310;
extern const uint32_t Logger_GetString_m2315344356_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1808051577;
extern const uint32_t Logger_Log_m2721486313_MetadataUsageId;
extern const uint32_t Logger_Log_m2254554809_MetadataUsageId;
extern const uint32_t Logger_LogFormat_m422274544_MetadataUsageId;
extern const uint32_t Logger_LogException_m1408988682_MetadataUsageId;
extern RuntimeClass* ArgumentException_t489606696_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1214607453;
extern Il2CppCodeGenString* _stringLiteral2667027303;
extern Il2CppCodeGenString* _stringLiteral1098340457;
extern Il2CppCodeGenString* _stringLiteral885527817;
extern const uint32_t ManagedStreamHelpers_ValidateLoadFromStream_m2234507788_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1994973061;
extern Il2CppCodeGenString* _stringLiteral2859367373;
extern const uint32_t ManagedStreamHelpers_ManagedStreamRead_m1646611388_MetadataUsageId;
extern const uint32_t ManagedStreamHelpers_ManagedStreamSeek_m2841650013_MetadataUsageId;
extern const uint32_t ManagedStreamHelpers_ManagedStreamLength_m2317062430_MetadataUsageId;
extern const uint32_t Material__ctor_m2996058918_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3523295012;
extern const uint32_t Material_set_color_m899657254_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3551009901;
extern const uint32_t Material_get_mainTexture_m3281846005_MetadataUsageId;
extern const uint32_t Mathf_Lerp_m1810328684_MetadataUsageId;
extern const uint32_t Mathf_Approximately_m4050652555_MetadataUsageId;
extern const uint32_t Mathf_SmoothDamp_m139100806_MetadataUsageId;
extern const uint32_t Mathf_Repeat_m220049890_MetadataUsageId;
extern const uint32_t Mathf_InverseLerp_m3361658789_MetadataUsageId;
extern RuntimeClass* MathfInternal_t1878424006_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m3492373530_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t787966842_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_TRS_m3108051110_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral736594712;
extern const uint32_t Matrix4x4_get_Item_m4189649815_MetadataUsageId;
extern const uint32_t Matrix4x4_set_Item_m1436863399_MetadataUsageId;
extern const uint32_t Matrix4x4_Equals_m4284243969_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2565397915;
extern const uint32_t Matrix4x4_GetColumn_m1725584070_MetadataUsageId;
extern const uint32_t Matrix4x4_get_identity_m579580882_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1508432694;
extern const uint32_t Matrix4x4_ToString_m151622620_MetadataUsageId;
extern const uint32_t Matrix4x4__cctor_m1789245472_MetadataUsageId;
extern const uint32_t Mesh__ctor_m2346141194_MetadataUsageId;
extern const RuntimeMethod* Mesh_SafeLength_TisInt32_t3515577538_m12458220_RuntimeMethod_var;
extern const uint32_t Mesh_SetTriangles_m879784383_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2159627391;
extern Il2CppCodeGenString* _stringLiteral2343396546;
extern const uint32_t Mesh_GetUVChannel_m1891453954_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral553365222;
extern Il2CppCodeGenString* _stringLiteral2518796331;
extern const uint32_t Mesh_DefaultDimensionForChannel_m405795618_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823_RuntimeMethod_var;
extern const uint32_t Mesh_get_vertices_m1024139665_MetadataUsageId;
extern const uint32_t Mesh_get_normals_m3412584569_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisVector4_t1900979187_m1344632441_RuntimeMethod_var;
extern const uint32_t Mesh_get_tangents_m8030836_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_RuntimeMethod_var;
extern const uint32_t Mesh_get_uv_m644261647_MetadataUsageId;
extern const uint32_t Mesh_get_uv2_m2611581962_MetadataUsageId;
extern const uint32_t Mesh_get_uv3_m113880564_MetadataUsageId;
extern const uint32_t Mesh_get_uv4_m1968302483_MetadataUsageId;
extern const RuntimeMethod* Mesh_GetAllocArrayFromChannel_TisColor32_t662424039_m1356534377_RuntimeMethod_var;
extern const uint32_t Mesh_get_colors32_m37522156_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733_RuntimeMethod_var;
extern const uint32_t Mesh_SetVertices_m2880691109_MetadataUsageId;
extern const uint32_t Mesh_SetNormals_m1347736657_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetListForChannel_TisVector4_t1900979187_m2034622288_RuntimeMethod_var;
extern const uint32_t Mesh_SetTangents_m1906139770_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetListForChannel_TisColor32_t662424039_m1488066680_RuntimeMethod_var;
extern const uint32_t Mesh_SetColors_m1083011859_MetadataUsageId;
extern const RuntimeMethod* Mesh_SetUvsImpl_TisVector2_t2246369278_m3260026371_RuntimeMethod_var;
extern const uint32_t Mesh_SetUVs_m1031607811_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2489973965;
extern const uint32_t Mesh_PrintErrorCantAccessIndices_m3788685066_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2881092165;
extern Il2CppCodeGenString* _stringLiteral3105790013;
extern Il2CppCodeGenString* _stringLiteral626827535;
extern const uint32_t Mesh_CheckCanAccessSubmesh_m2652095279_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t3215173111_il2cpp_TypeInfo_var;
extern const uint32_t Mesh_GetIndices_m2848004958_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2636767007;
extern const uint32_t Mesh_RecalculateBounds_m3942591469_MetadataUsageId;
extern RuntimeClass* PlayerEditorConnectionEvents_t1009044977_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t894813333_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2297325353_RuntimeMethod_var;
extern const uint32_t PlayerConnection__ctor_m1346962482_MetadataUsageId;
extern RuntimeClass* PlayerConnection_t3190352615_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_get_instance_m2295652940_MetadataUsageId;
extern RuntimeClass* IPlayerEditorConnectionNative_t3906153058_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_get_isConnected_m2943830747_MetadataUsageId;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisPlayerConnection_t3190352615_m3861853114_RuntimeMethod_var;
extern const uint32_t PlayerConnection_CreateInstance_m3049947590_MetadataUsageId;
extern RuntimeClass* PlayerConnectionInternal_t3959366414_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_GetConnectionNativeApi_m2366024494_MetadataUsageId;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m291452717_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral319603750;
extern Il2CppCodeGenString* _stringLiteral2951852423;
extern const uint32_t PlayerConnection_Register_m3487291422_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m2611892158_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3396935530_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_1_Invoke_m278533989_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3554339858_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m933170505_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_AddListener_m2986871176_RuntimeMethod_var;
extern const uint32_t PlayerConnection_RegisterConnection_m1216171267_MetadataUsageId;
extern const uint32_t PlayerConnection_RegisterDisconnection_m4134895671_MetadataUsageId;
extern const uint32_t PlayerConnection_Send_m1533960837_MetadataUsageId;
extern const uint32_t PlayerConnection_DisconnectAll_m1540013891_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t3567143369_il2cpp_TypeInfo_var;
extern RuntimeClass* Marshal_t132568556_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_MessageCallbackInternal_m194187481_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m1589466641_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_Invoke_m305340876_RuntimeMethod_var;
extern const uint32_t PlayerConnection_ConnectedCallbackInternal_m491598696_MetadataUsageId;
extern const uint32_t PlayerConnection_DisconnectedCallback_m3386176482_MetadataUsageId;

struct KeyframeU5BU5D_t662635263;
struct ObjectU5BU5D_t3385344125;
struct TypeU5BU5D_t98989581;
struct RequireComponentU5BU5D_t3043391348;
struct DisallowMultipleComponentU5BU5D_t3764267775;
struct ExecuteInEditModeU5BU5D_t1903432293;
struct CameraU5BU5D_t365668710;
struct IntPtrU5BU5D_t3930959911;
struct DisplayU5BU5D_t2949376739;
struct ParameterInfoU5BU5D_t1048445298;
struct ParameterModifierU5BU5D_t1706708300;
struct ByteU5BU5D_t3567143369;
struct Int32U5BU5D_t3215173111;
struct Vector3U5BU5D_t1432878832;
struct Vector4U5BU5D_t1632896738;
struct Vector2U5BU5D_t3868613195;
struct Color32U5BU5D_t2352998046;


#ifndef U3CMODULEU3E_T1227682081_H
#define U3CMODULEU3E_T1227682081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1227682081 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1227682081_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T1027733493_H
#define LIST_1_T1027733493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct  List_1_t1027733493  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	BaseInvokableCallU5BU5D_t2868271383* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1027733493, ____items_1)); }
	inline BaseInvokableCallU5BU5D_t2868271383* get__items_1() const { return ____items_1; }
	inline BaseInvokableCallU5BU5D_t2868271383** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(BaseInvokableCallU5BU5D_t2868271383* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1027733493, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1027733493, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1027733493_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	BaseInvokableCallU5BU5D_t2868271383* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1027733493_StaticFields, ___EmptyArray_4)); }
	inline BaseInvokableCallU5BU5D_t2868271383* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline BaseInvokableCallU5BU5D_t2868271383** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(BaseInvokableCallU5BU5D_t2868271383* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1027733493_H
#ifndef INVOKABLECALLLIST_T3172597217_H
#define INVOKABLECALLLIST_T3172597217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCallList
struct  InvokableCallList_t3172597217  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_PersistentCalls
	List_1_t1027733493 * ___m_PersistentCalls_0;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_RuntimeCalls
	List_1_t1027733493 * ___m_RuntimeCalls_1;
	// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::m_ExecutingCalls
	List_1_t1027733493 * ___m_ExecutingCalls_2;
	// System.Boolean UnityEngine.Events.InvokableCallList::m_NeedsUpdate
	bool ___m_NeedsUpdate_3;

public:
	inline static int32_t get_offset_of_m_PersistentCalls_0() { return static_cast<int32_t>(offsetof(InvokableCallList_t3172597217, ___m_PersistentCalls_0)); }
	inline List_1_t1027733493 * get_m_PersistentCalls_0() const { return ___m_PersistentCalls_0; }
	inline List_1_t1027733493 ** get_address_of_m_PersistentCalls_0() { return &___m_PersistentCalls_0; }
	inline void set_m_PersistentCalls_0(List_1_t1027733493 * value)
	{
		___m_PersistentCalls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_0), value);
	}

	inline static int32_t get_offset_of_m_RuntimeCalls_1() { return static_cast<int32_t>(offsetof(InvokableCallList_t3172597217, ___m_RuntimeCalls_1)); }
	inline List_1_t1027733493 * get_m_RuntimeCalls_1() const { return ___m_RuntimeCalls_1; }
	inline List_1_t1027733493 ** get_address_of_m_RuntimeCalls_1() { return &___m_RuntimeCalls_1; }
	inline void set_m_RuntimeCalls_1(List_1_t1027733493 * value)
	{
		___m_RuntimeCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_RuntimeCalls_1), value);
	}

	inline static int32_t get_offset_of_m_ExecutingCalls_2() { return static_cast<int32_t>(offsetof(InvokableCallList_t3172597217, ___m_ExecutingCalls_2)); }
	inline List_1_t1027733493 * get_m_ExecutingCalls_2() const { return ___m_ExecutingCalls_2; }
	inline List_1_t1027733493 ** get_address_of_m_ExecutingCalls_2() { return &___m_ExecutingCalls_2; }
	inline void set_m_ExecutingCalls_2(List_1_t1027733493 * value)
	{
		___m_ExecutingCalls_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExecutingCalls_2), value);
	}

	inline static int32_t get_offset_of_m_NeedsUpdate_3() { return static_cast<int32_t>(offsetof(InvokableCallList_t3172597217, ___m_NeedsUpdate_3)); }
	inline bool get_m_NeedsUpdate_3() const { return ___m_NeedsUpdate_3; }
	inline bool* get_address_of_m_NeedsUpdate_3() { return &___m_NeedsUpdate_3; }
	inline void set_m_NeedsUpdate_3(bool value)
	{
		___m_NeedsUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALLLIST_T3172597217_H
#ifndef BASEINVOKABLECALL_T3648497698_H
#define BASEINVOKABLECALL_T3648497698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.BaseInvokableCall
struct  BaseInvokableCall_t3648497698  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINVOKABLECALL_T3648497698_H
#ifndef ARGUMENTCACHE_T537934761_H
#define ARGUMENTCACHE_T537934761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.ArgumentCache
struct  ArgumentCache_t537934761  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.ArgumentCache::m_ObjectArgument
	Object_t3645472222 * ___m_ObjectArgument_0;
	// System.String UnityEngine.Events.ArgumentCache::m_ObjectArgumentAssemblyTypeName
	String_t* ___m_ObjectArgumentAssemblyTypeName_1;
	// System.Int32 UnityEngine.Events.ArgumentCache::m_IntArgument
	int32_t ___m_IntArgument_2;
	// System.Single UnityEngine.Events.ArgumentCache::m_FloatArgument
	float ___m_FloatArgument_3;
	// System.String UnityEngine.Events.ArgumentCache::m_StringArgument
	String_t* ___m_StringArgument_4;
	// System.Boolean UnityEngine.Events.ArgumentCache::m_BoolArgument
	bool ___m_BoolArgument_5;

public:
	inline static int32_t get_offset_of_m_ObjectArgument_0() { return static_cast<int32_t>(offsetof(ArgumentCache_t537934761, ___m_ObjectArgument_0)); }
	inline Object_t3645472222 * get_m_ObjectArgument_0() const { return ___m_ObjectArgument_0; }
	inline Object_t3645472222 ** get_address_of_m_ObjectArgument_0() { return &___m_ObjectArgument_0; }
	inline void set_m_ObjectArgument_0(Object_t3645472222 * value)
	{
		___m_ObjectArgument_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgument_0), value);
	}

	inline static int32_t get_offset_of_m_ObjectArgumentAssemblyTypeName_1() { return static_cast<int32_t>(offsetof(ArgumentCache_t537934761, ___m_ObjectArgumentAssemblyTypeName_1)); }
	inline String_t* get_m_ObjectArgumentAssemblyTypeName_1() const { return ___m_ObjectArgumentAssemblyTypeName_1; }
	inline String_t** get_address_of_m_ObjectArgumentAssemblyTypeName_1() { return &___m_ObjectArgumentAssemblyTypeName_1; }
	inline void set_m_ObjectArgumentAssemblyTypeName_1(String_t* value)
	{
		___m_ObjectArgumentAssemblyTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectArgumentAssemblyTypeName_1), value);
	}

	inline static int32_t get_offset_of_m_IntArgument_2() { return static_cast<int32_t>(offsetof(ArgumentCache_t537934761, ___m_IntArgument_2)); }
	inline int32_t get_m_IntArgument_2() const { return ___m_IntArgument_2; }
	inline int32_t* get_address_of_m_IntArgument_2() { return &___m_IntArgument_2; }
	inline void set_m_IntArgument_2(int32_t value)
	{
		___m_IntArgument_2 = value;
	}

	inline static int32_t get_offset_of_m_FloatArgument_3() { return static_cast<int32_t>(offsetof(ArgumentCache_t537934761, ___m_FloatArgument_3)); }
	inline float get_m_FloatArgument_3() const { return ___m_FloatArgument_3; }
	inline float* get_address_of_m_FloatArgument_3() { return &___m_FloatArgument_3; }
	inline void set_m_FloatArgument_3(float value)
	{
		___m_FloatArgument_3 = value;
	}

	inline static int32_t get_offset_of_m_StringArgument_4() { return static_cast<int32_t>(offsetof(ArgumentCache_t537934761, ___m_StringArgument_4)); }
	inline String_t* get_m_StringArgument_4() const { return ___m_StringArgument_4; }
	inline String_t** get_address_of_m_StringArgument_4() { return &___m_StringArgument_4; }
	inline void set_m_StringArgument_4(String_t* value)
	{
		___m_StringArgument_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringArgument_4), value);
	}

	inline static int32_t get_offset_of_m_BoolArgument_5() { return static_cast<int32_t>(offsetof(ArgumentCache_t537934761, ___m_BoolArgument_5)); }
	inline bool get_m_BoolArgument_5() const { return ___m_BoolArgument_5; }
	inline bool* get_address_of_m_BoolArgument_5() { return &___m_BoolArgument_5; }
	inline void set_m_BoolArgument_5(bool value)
	{
		___m_BoolArgument_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTCACHE_T537934761_H
#ifndef DEBUGLOGHANDLER_T2540765360_H
#define DEBUGLOGHANDLER_T2540765360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DebugLogHandler
struct  DebugLogHandler_t2540765360  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGHANDLER_T2540765360_H
#ifndef EXCEPTION_T214279536_H
#define EXCEPTION_T214279536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t214279536  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t3930959911* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t214279536 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t3930959911* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t3930959911** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t3930959911* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___inner_exception_1)); }
	inline Exception_t214279536 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t214279536 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t214279536 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t214279536, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t214279536, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T214279536_H
#ifndef DEBUG_T959145111_H
#define DEBUG_T959145111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Debug
struct  Debug_t959145111  : public RuntimeObject
{
public:

public:
};

struct Debug_t959145111_StaticFields
{
public:
	// UnityEngine.ILogger UnityEngine.Debug::s_Logger
	RuntimeObject* ___s_Logger_0;

public:
	inline static int32_t get_offset_of_s_Logger_0() { return static_cast<int32_t>(offsetof(Debug_t959145111_StaticFields, ___s_Logger_0)); }
	inline RuntimeObject* get_s_Logger_0() const { return ___s_Logger_0; }
	inline RuntimeObject** get_address_of_s_Logger_0() { return &___s_Logger_0; }
	inline void set_s_Logger_0(RuntimeObject* value)
	{
		___s_Logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Logger_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T959145111_H
#ifndef CUSTOMYIELDINSTRUCTION_T2091205422_H
#define CUSTOMYIELDINSTRUCTION_T2091205422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t2091205422  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T2091205422_H
#ifndef CURSOR_T3300883153_H
#define CURSOR_T3300883153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Cursor
struct  Cursor_t3300883153  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSOR_T3300883153_H
#ifndef VALUETYPE_T1108148719_H
#define VALUETYPE_T1108148719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1108148719  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_com
{
};
#endif // VALUETYPE_T1108148719_H
#ifndef DICTIONARY_2_T1470255310_H
#define DICTIONARY_2_T1470255310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct  Dictionary_2_t1470255310  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3215173111* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t1362056370* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	IntPtrU5BU5D_t3930959911* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	WeakReferenceU5BU5D_t2467492685* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t1812684756 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___table_4)); }
	inline Int32U5BU5D_t3215173111* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3215173111** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3215173111* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___linkSlots_5)); }
	inline LinkU5BU5D_t1362056370* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t1362056370** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t1362056370* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___keySlots_6)); }
	inline IntPtrU5BU5D_t3930959911* get_keySlots_6() const { return ___keySlots_6; }
	inline IntPtrU5BU5D_t3930959911** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(IntPtrU5BU5D_t3930959911* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___valueSlots_7)); }
	inline WeakReferenceU5BU5D_t2467492685* get_valueSlots_7() const { return ___valueSlots_7; }
	inline WeakReferenceU5BU5D_t2467492685** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(WeakReferenceU5BU5D_t2467492685* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___serialization_info_13)); }
	inline SerializationInfo_t1812684756 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t1812684756 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t1812684756 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1470255310_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3581913007 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1470255310_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3581913007 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3581913007 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3581913007 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1470255310_H
#ifndef NATIVE_T1351913248_H
#define NATIVE_T1351913248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.Native
struct  Native_t1351913248  : public RuntimeObject
{
public:

public:
};

struct Native_t1351913248_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference> UnityEngine.CSSLayout.Native::s_MeasureFunctions
	Dictionary_2_t1470255310 * ___s_MeasureFunctions_0;

public:
	inline static int32_t get_offset_of_s_MeasureFunctions_0() { return static_cast<int32_t>(offsetof(Native_t1351913248_StaticFields, ___s_MeasureFunctions_0)); }
	inline Dictionary_2_t1470255310 * get_s_MeasureFunctions_0() const { return ___s_MeasureFunctions_0; }
	inline Dictionary_2_t1470255310 ** get_address_of_s_MeasureFunctions_0() { return &___s_MeasureFunctions_0; }
	inline void set_s_MeasureFunctions_0(Dictionary_2_t1470255310 * value)
	{
		___s_MeasureFunctions_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_MeasureFunctions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVE_T1351913248_H
#ifndef YIELDINSTRUCTION_T1540633877_H
#define YIELDINSTRUCTION_T1540633877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t1540633877  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1540633877_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1540633877_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T1540633877_H
#ifndef UNITYEVENTBASE_T2081553920_H
#define UNITYEVENTBASE_T2081553920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t2081553920  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t3172597217 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3408587689 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_Calls_0)); }
	inline InvokableCallList_t3172597217 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t3172597217 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t3172597217 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3408587689 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3408587689 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3408587689 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T2081553920_H
#ifndef PERSISTENTCALLGROUP_T3408587689_H
#define PERSISTENTCALLGROUP_T3408587689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCallGroup
struct  PersistentCallGroup_t3408587689  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall> UnityEngine.Events.PersistentCallGroup::m_Calls
	List_1_t3642628955 * ___m_Calls_0;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(PersistentCallGroup_t3408587689, ___m_Calls_0)); }
	inline List_1_t3642628955 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline List_1_t3642628955 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(List_1_t3642628955 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALLGROUP_T3408587689_H
#ifndef LIST_1_T3642628955_H
#define LIST_1_T3642628955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct  List_1_t3642628955  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PersistentCallU5BU5D_t139144537* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3642628955, ____items_1)); }
	inline PersistentCallU5BU5D_t139144537* get__items_1() const { return ____items_1; }
	inline PersistentCallU5BU5D_t139144537** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PersistentCallU5BU5D_t139144537* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3642628955, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3642628955, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3642628955_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PersistentCallU5BU5D_t139144537* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3642628955_StaticFields, ___EmptyArray_4)); }
	inline PersistentCallU5BU5D_t139144537* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PersistentCallU5BU5D_t139144537** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PersistentCallU5BU5D_t139144537* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3642628955_H
#ifndef BINDER_T2026886273_H
#define BINDER_T2026886273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t2026886273  : public RuntimeObject
{
public:

public:
};

struct Binder_t2026886273_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t2026886273 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t2026886273_StaticFields, ___default_binder_0)); }
	inline Binder_t2026886273 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t2026886273 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t2026886273 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T2026886273_H
#ifndef RENDERPIPELINEMANAGER_T280631689_H
#define RENDERPIPELINEMANAGER_T280631689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipelineManager
struct  RenderPipelineManager_t280631689  : public RuntimeObject
{
public:

public:
};

struct RenderPipelineManager_t280631689_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.IRenderPipelineAsset UnityEngine.Experimental.Rendering.RenderPipelineManager::s_CurrentPipelineAsset
	RuntimeObject* ___s_CurrentPipelineAsset_0;
	// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::<currentPipeline>k__BackingField
	RuntimeObject* ___U3CcurrentPipelineU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_s_CurrentPipelineAsset_0() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t280631689_StaticFields, ___s_CurrentPipelineAsset_0)); }
	inline RuntimeObject* get_s_CurrentPipelineAsset_0() const { return ___s_CurrentPipelineAsset_0; }
	inline RuntimeObject** get_address_of_s_CurrentPipelineAsset_0() { return &___s_CurrentPipelineAsset_0; }
	inline void set_s_CurrentPipelineAsset_0(RuntimeObject* value)
	{
		___s_CurrentPipelineAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentPipelineAsset_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t280631689_StaticFields, ___U3CcurrentPipelineU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CcurrentPipelineU3Ek__BackingField_1() const { return ___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CcurrentPipelineU3Ek__BackingField_1() { return &___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline void set_U3CcurrentPipelineU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CcurrentPipelineU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentPipelineU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINEMANAGER_T280631689_H
#ifndef GYROSCOPE_T3917155460_H
#define GYROSCOPE_T3917155460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gyroscope
struct  Gyroscope_t3917155460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPE_T3917155460_H
#ifndef INPUT_T1636556003_H
#define INPUT_T1636556003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Input
struct  Input_t1636556003  : public RuntimeObject
{
public:

public:
};

struct Input_t1636556003_StaticFields
{
public:
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t3917155460 * ___m_MainGyro_0;

public:
	inline static int32_t get_offset_of_m_MainGyro_0() { return static_cast<int32_t>(offsetof(Input_t1636556003_StaticFields, ___m_MainGyro_0)); }
	inline Gyroscope_t3917155460 * get_m_MainGyro_0() const { return ___m_MainGyro_0; }
	inline Gyroscope_t3917155460 ** get_address_of_m_MainGyro_0() { return &___m_MainGyro_0; }
	inline void set_m_MainGyro_0(Gyroscope_t3917155460 * value)
	{
		___m_MainGyro_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainGyro_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T1636556003_H
#ifndef MANAGEDSTREAMHELPERS_T3491728309_H
#define MANAGEDSTREAMHELPERS_T3491728309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ManagedStreamHelpers
struct  ManagedStreamHelpers_t3491728309  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGEDSTREAMHELPERS_T3491728309_H
#ifndef STREAM_T360920014_H
#define STREAM_T360920014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t360920014  : public RuntimeObject
{
public:

public:
};

struct Stream_t360920014_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t360920014 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t360920014_StaticFields, ___Null_0)); }
	inline Stream_t360920014 * get_Null_0() const { return ___Null_0; }
	inline Stream_t360920014 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t360920014 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T360920014_H
#ifndef LIST_1_T894813333_H
#define LIST_1_T894813333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t894813333  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t3215173111* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t894813333, ____items_1)); }
	inline Int32U5BU5D_t3215173111* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t3215173111** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t3215173111* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t894813333, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t894813333, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t894813333_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t3215173111* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t894813333_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t3215173111* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t3215173111** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t3215173111* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T894813333_H
#ifndef LIST_1_T1311628880_H
#define LIST_1_T1311628880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t1311628880  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1432878832* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1311628880, ____items_1)); }
	inline Vector3U5BU5D_t1432878832* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1432878832** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1432878832* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1311628880, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1311628880, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1311628880_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t1432878832* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1311628880_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t1432878832* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t1432878832** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t1432878832* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1311628880_H
#ifndef LIST_1_T3575182278_H
#define LIST_1_T3575182278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_t3575182278  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t1632896738* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3575182278, ____items_1)); }
	inline Vector4U5BU5D_t1632896738* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t1632896738** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t1632896738* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3575182278, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3575182278, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3575182278_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector4U5BU5D_t1632896738* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3575182278_StaticFields, ___EmptyArray_4)); }
	inline Vector4U5BU5D_t1632896738* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector4U5BU5D_t1632896738** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector4U5BU5D_t1632896738* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3575182278_H
#ifndef LIST_1_T2336627130_H
#define LIST_1_T2336627130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t2336627130  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_t2352998046* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2336627130, ____items_1)); }
	inline Color32U5BU5D_t2352998046* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_t2352998046** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_t2352998046* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2336627130, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2336627130, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2336627130_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Color32U5BU5D_t2352998046* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2336627130_StaticFields, ___EmptyArray_4)); }
	inline Color32U5BU5D_t2352998046* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Color32U5BU5D_t2352998046** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Color32U5BU5D_t2352998046* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2336627130_H
#ifndef LIST_1_T3920572369_H
#define LIST_1_T3920572369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t3920572369  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_t3868613195* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3920572369, ____items_1)); }
	inline Vector2U5BU5D_t3868613195* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_t3868613195** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_t3868613195* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3920572369, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3920572369, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3920572369_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_t3868613195* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3920572369_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_t3868613195* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_t3868613195** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_t3868613195* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3920572369_H
#ifndef LIST_1_T2349647505_H
#define LIST_1_T2349647505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  List_1_t2349647505  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MessageTypeSubscribersU5BU5D_t1032573067* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2349647505, ____items_1)); }
	inline MessageTypeSubscribersU5BU5D_t1032573067* get__items_1() const { return ____items_1; }
	inline MessageTypeSubscribersU5BU5D_t1032573067** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MessageTypeSubscribersU5BU5D_t1032573067* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2349647505, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2349647505, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2349647505_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MessageTypeSubscribersU5BU5D_t1032573067* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2349647505_StaticFields, ___EmptyArray_4)); }
	inline MessageTypeSubscribersU5BU5D_t1032573067* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MessageTypeSubscribersU5BU5D_t1032573067** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MessageTypeSubscribersU5BU5D_t1032573067* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2349647505_H
#ifndef MESSAGEEVENTARGS_T2772358345_H
#define MESSAGEEVENTARGS_T2772358345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct  MessageEventArgs_t2772358345  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Networking.PlayerConnection.MessageEventArgs::playerId
	int32_t ___playerId_0;
	// System.Byte[] UnityEngine.Networking.PlayerConnection.MessageEventArgs::data
	ByteU5BU5D_t3567143369* ___data_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2772358345, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2772358345, ___data_1)); }
	inline ByteU5BU5D_t3567143369* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3567143369** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3567143369* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T2772358345_H
#ifndef PLAYEREDITORCONNECTIONEVENTS_T1009044977_H
#define PLAYEREDITORCONNECTIONEVENTS_T1009044977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct  PlayerEditorConnectionEvents_t1009044977  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::messageTypeSubscribers
	List_1_t2349647505 * ___messageTypeSubscribers_0;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::connectionEvent
	ConnectionChangeEvent_t190635484 * ___connectionEvent_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::disconnectionEvent
	ConnectionChangeEvent_t190635484 * ___disconnectionEvent_2;

public:
	inline static int32_t get_offset_of_messageTypeSubscribers_0() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t1009044977, ___messageTypeSubscribers_0)); }
	inline List_1_t2349647505 * get_messageTypeSubscribers_0() const { return ___messageTypeSubscribers_0; }
	inline List_1_t2349647505 ** get_address_of_messageTypeSubscribers_0() { return &___messageTypeSubscribers_0; }
	inline void set_messageTypeSubscribers_0(List_1_t2349647505 * value)
	{
		___messageTypeSubscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageTypeSubscribers_0), value);
	}

	inline static int32_t get_offset_of_connectionEvent_1() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t1009044977, ___connectionEvent_1)); }
	inline ConnectionChangeEvent_t190635484 * get_connectionEvent_1() const { return ___connectionEvent_1; }
	inline ConnectionChangeEvent_t190635484 ** get_address_of_connectionEvent_1() { return &___connectionEvent_1; }
	inline void set_connectionEvent_1(ConnectionChangeEvent_t190635484 * value)
	{
		___connectionEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___connectionEvent_1), value);
	}

	inline static int32_t get_offset_of_disconnectionEvent_2() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t1009044977, ___disconnectionEvent_2)); }
	inline ConnectionChangeEvent_t190635484 * get_disconnectionEvent_2() const { return ___disconnectionEvent_2; }
	inline ConnectionChangeEvent_t190635484 ** get_address_of_disconnectionEvent_2() { return &___disconnectionEvent_2; }
	inline void set_disconnectionEvent_2(ConnectionChangeEvent_t190635484 * value)
	{
		___disconnectionEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectionEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYEREDITORCONNECTIONEVENTS_T1009044977_H
#ifndef LIST_1_T4009708412_H
#define LIST_1_T4009708412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Component>
struct  List_1_t4009708412  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ComponentU5BU5D_t3701357460* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4009708412, ____items_1)); }
	inline ComponentU5BU5D_t3701357460* get__items_1() const { return ____items_1; }
	inline ComponentU5BU5D_t3701357460** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ComponentU5BU5D_t3701357460* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4009708412, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4009708412, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4009708412_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ComponentU5BU5D_t3701357460* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4009708412_StaticFields, ___EmptyArray_4)); }
	inline ComponentU5BU5D_t3701357460* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ComponentU5BU5D_t3701357460** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ComponentU5BU5D_t3701357460* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4009708412_H
#ifndef CLASSLIBRARYINITIALIZER_T1722056075_H
#define CLASSLIBRARYINITIALIZER_T1722056075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ClassLibraryInitializer
struct  ClassLibraryInitializer_t1722056075  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSLIBRARYINITIALIZER_T1722056075_H
#ifndef PLAYERCONNECTIONINTERNAL_T3959366414_H
#define PLAYERCONNECTIONINTERNAL_T3959366414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayerConnectionInternal
struct  PlayerConnectionInternal_t3959366414  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTIONINTERNAL_T3959366414_H
#ifndef STACK_1_T3041366545_H
#define STACK_1_T3041366545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Type>
struct  Stack_1_t3041366545  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	TypeU5BU5D_t98989581* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t3041366545, ____array_0)); }
	inline TypeU5BU5D_t98989581* get__array_0() const { return ____array_0; }
	inline TypeU5BU5D_t98989581** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(TypeU5BU5D_t98989581* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t3041366545, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t3041366545, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T3041366545_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2953840665* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2953840665* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2953840665** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2953840665* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef ATTRIBUTEHELPERENGINE_T2253936689_H
#define ATTRIBUTEHELPERENGINE_T2253936689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t2253936689  : public RuntimeObject
{
public:

public:
};

struct AttributeHelperEngine_t2253936689_StaticFields
{
public:
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t3764267775* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t1903432293* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t3043391348* ____requireComponentArray_2;

public:
	inline static int32_t get_offset_of__disallowMultipleComponentArray_0() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t2253936689_StaticFields, ____disallowMultipleComponentArray_0)); }
	inline DisallowMultipleComponentU5BU5D_t3764267775* get__disallowMultipleComponentArray_0() const { return ____disallowMultipleComponentArray_0; }
	inline DisallowMultipleComponentU5BU5D_t3764267775** get_address_of__disallowMultipleComponentArray_0() { return &____disallowMultipleComponentArray_0; }
	inline void set__disallowMultipleComponentArray_0(DisallowMultipleComponentU5BU5D_t3764267775* value)
	{
		____disallowMultipleComponentArray_0 = value;
		Il2CppCodeGenWriteBarrier((&____disallowMultipleComponentArray_0), value);
	}

	inline static int32_t get_offset_of__executeInEditModeArray_1() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t2253936689_StaticFields, ____executeInEditModeArray_1)); }
	inline ExecuteInEditModeU5BU5D_t1903432293* get__executeInEditModeArray_1() const { return ____executeInEditModeArray_1; }
	inline ExecuteInEditModeU5BU5D_t1903432293** get_address_of__executeInEditModeArray_1() { return &____executeInEditModeArray_1; }
	inline void set__executeInEditModeArray_1(ExecuteInEditModeU5BU5D_t1903432293* value)
	{
		____executeInEditModeArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeInEditModeArray_1), value);
	}

	inline static int32_t get_offset_of__requireComponentArray_2() { return static_cast<int32_t>(offsetof(AttributeHelperEngine_t2253936689_StaticFields, ____requireComponentArray_2)); }
	inline RequireComponentU5BU5D_t3043391348* get__requireComponentArray_2() const { return ____requireComponentArray_2; }
	inline RequireComponentU5BU5D_t3043391348** get_address_of__requireComponentArray_2() { return &____requireComponentArray_2; }
	inline void set__requireComponentArray_2(RequireComponentU5BU5D_t3043391348* value)
	{
		____requireComponentArray_2 = value;
		Il2CppCodeGenWriteBarrier((&____requireComponentArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEHELPERENGINE_T2253936689_H
#ifndef APPLICATION_T1595095978_H
#define APPLICATION_T1595095978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application
struct  Application_t1595095978  : public RuntimeObject
{
public:

public:
};

struct Application_t1595095978_StaticFields
{
public:
	// UnityEngine.Application/LowMemoryCallback UnityEngine.Application::lowMemory
	LowMemoryCallback_t1978334135 * ___lowMemory_0;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandler
	LogCallback_t2572717340 * ___s_LogCallbackHandler_1;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandlerThreaded
	LogCallback_t2572717340 * ___s_LogCallbackHandlerThreaded_2;
	// UnityEngine.Events.UnityAction UnityEngine.Application::onBeforeRender
	UnityAction_t191347160 * ___onBeforeRender_3;

public:
	inline static int32_t get_offset_of_lowMemory_0() { return static_cast<int32_t>(offsetof(Application_t1595095978_StaticFields, ___lowMemory_0)); }
	inline LowMemoryCallback_t1978334135 * get_lowMemory_0() const { return ___lowMemory_0; }
	inline LowMemoryCallback_t1978334135 ** get_address_of_lowMemory_0() { return &___lowMemory_0; }
	inline void set_lowMemory_0(LowMemoryCallback_t1978334135 * value)
	{
		___lowMemory_0 = value;
		Il2CppCodeGenWriteBarrier((&___lowMemory_0), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandler_1() { return static_cast<int32_t>(offsetof(Application_t1595095978_StaticFields, ___s_LogCallbackHandler_1)); }
	inline LogCallback_t2572717340 * get_s_LogCallbackHandler_1() const { return ___s_LogCallbackHandler_1; }
	inline LogCallback_t2572717340 ** get_address_of_s_LogCallbackHandler_1() { return &___s_LogCallbackHandler_1; }
	inline void set_s_LogCallbackHandler_1(LogCallback_t2572717340 * value)
	{
		___s_LogCallbackHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandler_1), value);
	}

	inline static int32_t get_offset_of_s_LogCallbackHandlerThreaded_2() { return static_cast<int32_t>(offsetof(Application_t1595095978_StaticFields, ___s_LogCallbackHandlerThreaded_2)); }
	inline LogCallback_t2572717340 * get_s_LogCallbackHandlerThreaded_2() const { return ___s_LogCallbackHandlerThreaded_2; }
	inline LogCallback_t2572717340 ** get_address_of_s_LogCallbackHandlerThreaded_2() { return &___s_LogCallbackHandlerThreaded_2; }
	inline void set_s_LogCallbackHandlerThreaded_2(LogCallback_t2572717340 * value)
	{
		___s_LogCallbackHandlerThreaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_LogCallbackHandlerThreaded_2), value);
	}

	inline static int32_t get_offset_of_onBeforeRender_3() { return static_cast<int32_t>(offsetof(Application_t1595095978_StaticFields, ___onBeforeRender_3)); }
	inline UnityAction_t191347160 * get_onBeforeRender_3() const { return ___onBeforeRender_3; }
	inline UnityAction_t191347160 ** get_address_of_onBeforeRender_3() { return &___onBeforeRender_3; }
	inline void set_onBeforeRender_3(UnityAction_t191347160 * value)
	{
		___onBeforeRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onBeforeRender_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATION_T1595095978_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef ATTRIBUTE_T3130080784_H
#define ATTRIBUTE_T3130080784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t3130080784  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T3130080784_H
#ifndef LIST_1_T2722562487_H
#define LIST_1_T2722562487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Type>
struct  List_1_t2722562487  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeU5BU5D_t98989581* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2722562487, ____items_1)); }
	inline TypeU5BU5D_t98989581* get__items_1() const { return ____items_1; }
	inline TypeU5BU5D_t98989581** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeU5BU5D_t98989581* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2722562487, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2722562487, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2722562487_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TypeU5BU5D_t98989581* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2722562487_StaticFields, ___EmptyArray_4)); }
	inline TypeU5BU5D_t98989581* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TypeU5BU5D_t98989581** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TypeU5BU5D_t98989581* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2722562487_H
#ifndef ENUMERATOR_T1984039949_H
#define ENUMERATOR_T1984039949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t1984039949 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2226230279 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1984039949, ___l_0)); }
	inline List_1_t2226230279 * get_l_0() const { return ___l_0; }
	inline List_1_t2226230279 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2226230279 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1984039949, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1984039949, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1984039949, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1984039949_H
#ifndef CULLINGGROUPEVENT_T505730933_H
#define CULLINGGROUPEVENT_T505730933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroupEvent
struct  CullingGroupEvent_t505730933 
{
public:
	// System.Int32 UnityEngine.CullingGroupEvent::m_Index
	int32_t ___m_Index_0;
	// System.Byte UnityEngine.CullingGroupEvent::m_PrevState
	uint8_t ___m_PrevState_1;
	// System.Byte UnityEngine.CullingGroupEvent::m_ThisState
	uint8_t ___m_ThisState_2;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t505730933, ___m_Index_0)); }
	inline int32_t get_m_Index_0() const { return ___m_Index_0; }
	inline int32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(int32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_1() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t505730933, ___m_PrevState_1)); }
	inline uint8_t get_m_PrevState_1() const { return ___m_PrevState_1; }
	inline uint8_t* get_address_of_m_PrevState_1() { return &___m_PrevState_1; }
	inline void set_m_PrevState_1(uint8_t value)
	{
		___m_PrevState_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisState_2() { return static_cast<int32_t>(offsetof(CullingGroupEvent_t505730933, ___m_ThisState_2)); }
	inline uint8_t get_m_ThisState_2() const { return ___m_ThisState_2; }
	inline uint8_t* get_address_of_m_ThisState_2() { return &___m_ThisState_2; }
	inline void set_m_ThisState_2(uint8_t value)
	{
		___m_ThisState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGGROUPEVENT_T505730933_H
#ifndef QUATERNION_T3497065016_H
#define QUATERNION_T3497065016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t3497065016 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t3497065016_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t3497065016  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t3497065016  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t3497065016 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t3497065016  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T3497065016_H
#ifndef ENUMERATOR_T652623003_H
#define ENUMERATOR_T652623003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t652623003 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t894813333 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t652623003, ___l_0)); }
	inline List_1_t894813333 * get_l_0() const { return ___l_0; }
	inline List_1_t894813333 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t894813333 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t652623003, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t652623003, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t652623003, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T652623003_H
#ifndef BOOLEAN_T2440219994_H
#define BOOLEAN_T2440219994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2440219994 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2440219994, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2440219994_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2440219994_H
#ifndef DOUBLE_T3942828892_H
#define DOUBLE_T3942828892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t3942828892 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t3942828892, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T3942828892_H
#ifndef MATHF_T9189715_H
#define MATHF_T9189715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mathf
struct  Mathf_t9189715 
{
public:

public:
};

struct Mathf_t9189715_StaticFields
{
public:
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;

public:
	inline static int32_t get_offset_of_Epsilon_0() { return static_cast<int32_t>(offsetof(Mathf_t9189715_StaticFields, ___Epsilon_0)); }
	inline float get_Epsilon_0() const { return ___Epsilon_0; }
	inline float* get_address_of_Epsilon_0() { return &___Epsilon_0; }
	inline void set_Epsilon_0(float value)
	{
		___Epsilon_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHF_T9189715_H
#ifndef UINT32_T1085449242_H
#define UINT32_T1085449242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1085449242 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1085449242, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1085449242_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T110585296_H
#define DRIVENRECTTRANSFORMTRACKER_T110585296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t110585296 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T110585296_H
#ifndef UNITYEVENT_1_T2202423910_H
#define UNITYEVENT_1_T2202423910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2202423910  : public UnityEventBase_t2081553920
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3385344125* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2202423910, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3385344125* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3385344125** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3385344125* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2202423910_H
#ifndef INVOKABLECALL_1_T4136797902_H
#define INVOKABLECALL_1_T4136797902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct  InvokableCall_1_t4136797902  : public BaseInvokableCall_t3648497698
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t816250014 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t4136797902, ___Delegate_0)); }
	inline UnityAction_1_t816250014 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t816250014 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t816250014 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T4136797902_H
#ifndef KEYFRAME_T1233380954_H
#define KEYFRAME_T1233380954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t1233380954 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t1233380954, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t1233380954, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t1233380954, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t1233380954, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T1233380954_H
#ifndef METHODBASE_T3670318294_H
#define METHODBASE_T3670318294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t3670318294  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T3670318294_H
#ifndef UNITYEVENT_T4085066839_H
#define UNITYEVENT_T4085066839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t4085066839  : public UnityEventBase_t2081553920
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3385344125* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t4085066839, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3385344125* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3385344125** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3385344125* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T4085066839_H
#ifndef LAYERMASK_T4067055223_H
#define LAYERMASK_T4067055223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t4067055223 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t4067055223, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T4067055223_H
#ifndef ASSEMBLYISEDITORASSEMBLY_T4029989084_H
#define ASSEMBLYISEDITORASSEMBLY_T4029989084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssemblyIsEditorAssembly
struct  AssemblyIsEditorAssembly_t4029989084  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYISEDITORASSEMBLY_T4029989084_H
#ifndef SYSTEMEXCEPTION_T1379941789_H
#define SYSTEMEXCEPTION_T1379941789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t1379941789  : public Exception_t214279536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T1379941789_H
#ifndef PARAMETERMODIFIER_T1933193809_H
#define PARAMETERMODIFIER_T1933193809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t1933193809 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t1963761663* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t1933193809, ____byref_0)); }
	inline BooleanU5BU5D_t1963761663* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t1963761663** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t1963761663* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1933193809_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1933193809_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T1933193809_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef EXCLUDEFROMDOCSATTRIBUTE_T1059923024_H
#define EXCLUDEFROMDOCSATTRIBUTE_T1059923024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct  ExcludeFromDocsAttribute_t1059923024  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEFROMDOCSATTRIBUTE_T1059923024_H
#ifndef PROPERTYATTRIBUTE_T4289523592_H
#define PROPERTYATTRIBUTE_T4289523592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4289523592  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T4289523592_H
#ifndef INVOKABLECALL_1_T661132011_H
#define INVOKABLECALL_1_T661132011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t661132011  : public BaseInvokableCall_t3648497698
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t1635551419 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t661132011, ___Delegate_0)); }
	inline UnityAction_1_t1635551419 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t1635551419 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t1635551419 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T661132011_H
#ifndef DEFAULTVALUEATTRIBUTE_T1912603919_H
#define DEFAULTVALUEATTRIBUTE_T1912603919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t1912603919  : public Attribute_t3130080784
{
public:
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	RuntimeObject * ___DefaultValue_0;

public:
	inline static int32_t get_offset_of_DefaultValue_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t1912603919, ___DefaultValue_0)); }
	inline RuntimeObject * get_DefaultValue_0() const { return ___DefaultValue_0; }
	inline RuntimeObject ** get_address_of_DefaultValue_0() { return &___DefaultValue_0; }
	inline void set_DefaultValue_0(RuntimeObject * value)
	{
		___DefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T1912603919_H
#ifndef VECTOR2_T2246369278_H
#define VECTOR2_T2246369278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2246369278 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2246369278_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2246369278  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2246369278  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2246369278  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2246369278  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2246369278  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2246369278  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2246369278  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2246369278  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2246369278  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2246369278 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2246369278  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___oneVector_3)); }
	inline Vector2_t2246369278  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2246369278 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2246369278  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___upVector_4)); }
	inline Vector2_t2246369278  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2246369278 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2246369278  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___downVector_5)); }
	inline Vector2_t2246369278  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2246369278 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2246369278  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___leftVector_6)); }
	inline Vector2_t2246369278  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2246369278 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2246369278  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___rightVector_7)); }
	inline Vector2_t2246369278  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2246369278 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2246369278  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2246369278  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2246369278 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2246369278  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2246369278  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2246369278 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2246369278  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2246369278_H
#ifndef TIMESPAN_T595369841_H
#define TIMESPAN_T595369841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t595369841 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t595369841_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t595369841  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t595369841  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t595369841  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t595369841  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t595369841 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t595369841  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t595369841  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t595369841 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t595369841  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841_StaticFields, ___Zero_2)); }
	inline TimeSpan_t595369841  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t595369841 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t595369841  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T595369841_H
#ifndef IL2CPPSTRUCTALIGNMENTATTRIBUTE_T1993002847_H
#define IL2CPPSTRUCTALIGNMENTATTRIBUTE_T1993002847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.IL2CPPStructAlignmentAttribute
struct  IL2CPPStructAlignmentAttribute_t1993002847  : public Attribute_t3130080784
{
public:
	// System.Int32 UnityEngine.IL2CPPStructAlignmentAttribute::Align
	int32_t ___Align_0;

public:
	inline static int32_t get_offset_of_Align_0() { return static_cast<int32_t>(offsetof(IL2CPPStructAlignmentAttribute_t1993002847, ___Align_0)); }
	inline int32_t get_Align_0() const { return ___Align_0; }
	inline int32_t* get_address_of_Align_0() { return &___Align_0; }
	inline void set_Align_0(int32_t value)
	{
		___Align_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IL2CPPSTRUCTALIGNMENTATTRIBUTE_T1993002847_H
#ifndef ENUMERATOR_T3400438625_H
#define ENUMERATOR_T3400438625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
struct  Enumerator_t3400438625 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3642628955 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PersistentCall_t1968425864 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3400438625, ___l_0)); }
	inline List_1_t3642628955 * get_l_0() const { return ___l_0; }
	inline List_1_t3642628955 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3642628955 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3400438625, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3400438625, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3400438625, ___current_3)); }
	inline PersistentCall_t1968425864 * get_current_3() const { return ___current_3; }
	inline PersistentCall_t1968425864 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PersistentCall_t1968425864 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3400438625_H
#ifndef INVOKABLECALL_T1976387117_H
#define INVOKABLECALL_T1976387117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall
struct  InvokableCall_t1976387117  : public BaseInvokableCall_t3648497698
{
public:
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t191347160 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_t1976387117, ___Delegate_0)); }
	inline UnityAction_t191347160 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_t191347160 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_t191347160 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_T1976387117_H
#ifndef INT64_T3603203610_H
#define INT64_T3603203610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3603203610 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3603203610, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3603203610_H
#ifndef UINT64_T462540778_H
#define UINT64_T462540778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t462540778 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t462540778, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T462540778_H
#ifndef NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T1926766966_H
#define NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T1926766966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsMinMaxWriteRestrictionAttribute
struct  NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1926766966  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSMINMAXWRITERESTRICTIONATTRIBUTE_T1926766966_H
#ifndef COLOR_T1361298052_H
#define COLOR_T1361298052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t1361298052 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T1361298052_H
#ifndef WRITEONLYATTRIBUTE_T3763130347_H
#define WRITEONLYATTRIBUTE_T3763130347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.WriteOnlyAttribute
struct  WriteOnlyAttribute_t3763130347  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEONLYATTRIBUTE_T3763130347_H
#ifndef READWRITEATTRIBUTE_T2222226965_H
#define READWRITEATTRIBUTE_T2222226965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadWriteAttribute
struct  ReadWriteAttribute_t2222226965  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READWRITEATTRIBUTE_T2222226965_H
#ifndef INVOKABLECALL_1_T2594376411_H
#define INVOKABLECALL_1_T2594376411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t2594376411  : public BaseInvokableCall_t3648497698
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t3568795819 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t2594376411, ___Delegate_0)); }
	inline UnityAction_1_t3568795819 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t3568795819 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t3568795819 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T2594376411_H
#ifndef READONLYATTRIBUTE_T2782882542_H
#define READONLYATTRIBUTE_T2782882542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.ReadOnlyAttribute
struct  ReadOnlyAttribute_t2782882542  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T2782882542_H
#ifndef REQUIRECOMPONENT_T3219866953_H
#define REQUIRECOMPONENT_T3219866953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t3219866953  : public Attribute_t3130080784
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t3219866953, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t3219866953, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t3219866953, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T3219866953_H
#ifndef VECTOR4_T1900979187_H
#define VECTOR4_T1900979187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t1900979187 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t1900979187_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t1900979187  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t1900979187  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t1900979187  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t1900979187  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___zeroVector_5)); }
	inline Vector4_t1900979187  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t1900979187 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t1900979187  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___oneVector_6)); }
	inline Vector4_t1900979187  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t1900979187 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t1900979187  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t1900979187  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t1900979187 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t1900979187  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t1900979187  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t1900979187 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t1900979187  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T1900979187_H
#ifndef MATRIX4X4_T787966842_H
#define MATRIX4X4_T787966842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t787966842 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t787966842_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t787966842  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t787966842  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t787966842  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t787966842 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t787966842  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t787966842  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t787966842 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t787966842  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T787966842_H
#ifndef NATIVECONTAINERATTRIBUTE_T2362250235_H
#define NATIVECONTAINERATTRIBUTE_T2362250235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerAttribute
struct  NativeContainerAttribute_t2362250235  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERATTRIBUTE_T2362250235_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2272071576 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2272071576 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2272071576 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2272071576 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2272071576 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2272071576 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2272071576 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2272071576 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T4282775488_H
#define DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T4282775488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.DeallocateOnJobCompletionAttribute
struct  DeallocateOnJobCompletionAttribute_t4282775488  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEALLOCATEONJOBCOMPLETIONATTRIBUTE_T4282775488_H
#ifndef EXECUTEINEDITMODE_T2915138156_H
#define EXECUTEINEDITMODE_T2915138156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExecuteInEditMode
struct  ExecuteInEditMode_t2915138156  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEINEDITMODE_T2915138156_H
#ifndef UNITYEVENT_1_T1459204717_H
#define UNITYEVENT_1_T1459204717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityEvent_1_t1459204717  : public UnityEventBase_t2081553920
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3385344125* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1459204717, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3385344125* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3385344125** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3385344125* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1459204717_H
#ifndef VOID_T4071739332_H
#define VOID_T4071739332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t4071739332 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T4071739332_H
#ifndef NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T3723763688_H
#define NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T3723763688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collections.NativeContainerSupportsAtomicWriteAttribute
struct  NativeContainerSupportsAtomicWriteAttribute_t3723763688  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECONTAINERSUPPORTSATOMICWRITEATTRIBUTE_T3723763688_H
#ifndef GCHANDLE_T2748005453_H
#define GCHANDLE_T2748005453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t2748005453 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t2748005453, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T2748005453_H
#ifndef RECT_T952252086_H
#define RECT_T952252086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t952252086 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T952252086_H
#ifndef BYTE_T2640180184_H
#define BYTE_T2640180184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t2640180184 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t2640180184, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T2640180184_H
#ifndef CSSSIZE_T83932994_H
#define CSSSIZE_T83932994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSSize
struct  CSSSize_t83932994 
{
public:
	// System.Single UnityEngine.CSSLayout.CSSSize::width
	float ___width_0;
	// System.Single UnityEngine.CSSLayout.CSSSize::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(CSSSize_t83932994, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CSSSize_t83932994, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSSIZE_T83932994_H
#ifndef UNMARSHALLEDATTRIBUTE_T1385813216_H
#define UNMARSHALLEDATTRIBUTE_T1385813216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bindings.UnmarshalledAttribute
struct  UnmarshalledAttribute_t1385813216  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLEDATTRIBUTE_T1385813216_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T3130506276_H
#define MONOPINVOKECALLBACKATTRIBUTE_T3130506276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AOT.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t3130506276  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T3130506276_H
#ifndef VECTOR3_T3932393085_H
#define VECTOR3_T3932393085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3932393085 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3932393085_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3932393085  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3932393085  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3932393085  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3932393085  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3932393085  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3932393085  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3932393085  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3932393085  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3932393085  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3932393085  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3932393085  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3932393085 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3932393085  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___oneVector_5)); }
	inline Vector3_t3932393085  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3932393085 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3932393085  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___upVector_6)); }
	inline Vector3_t3932393085  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3932393085 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3932393085  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___downVector_7)); }
	inline Vector3_t3932393085  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3932393085 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3932393085  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___leftVector_8)); }
	inline Vector3_t3932393085  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3932393085 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3932393085  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___rightVector_9)); }
	inline Vector3_t3932393085  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3932393085 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3932393085  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3932393085  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3932393085 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3932393085  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___backVector_11)); }
	inline Vector3_t3932393085  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3932393085 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3932393085  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3932393085  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3932393085 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3932393085  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3932393085  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3932393085 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3932393085  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3932393085_H
#ifndef INT32_T3515577538_H
#define INT32_T3515577538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3515577538 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3515577538, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3515577538_H
#ifndef SINGLE_T897798503_H
#define SINGLE_T897798503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t897798503 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t897798503, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T897798503_H
#ifndef COLOR32_T662424039_H
#define COLOR32_T662424039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t662424039 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T662424039_H
#ifndef DEFAULTEXECUTIONORDER_T1108411129_H
#define DEFAULTEXECUTIONORDER_T1108411129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DefaultExecutionOrder
struct  DefaultExecutionOrder_t1108411129  : public Attribute_t3130080784
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t1108411129, ___U3CorderU3Ek__BackingField_0)); }
	inline int32_t get_U3CorderU3Ek__BackingField_0() const { return ___U3CorderU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_0() { return &___U3CorderU3Ek__BackingField_0; }
	inline void set_U3CorderU3Ek__BackingField_0(int32_t value)
	{
		___U3CorderU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXECUTIONORDER_T1108411129_H
#ifndef CONTEXTMENU_T4072484265_H
#define CONTEXTMENU_T4072484265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContextMenu
struct  ContextMenu_t4072484265  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTMENU_T4072484265_H
#ifndef DISALLOWMULTIPLECOMPONENT_T4131774042_H
#define DISALLOWMULTIPLECOMPONENT_T4131774042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DisallowMultipleComponent
struct  DisallowMultipleComponent_t4131774042  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISALLOWMULTIPLECOMPONENT_T4131774042_H
#ifndef ENUM_T2432427458_H
#define ENUM_T2432427458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2432427458  : public ValueType_t1108148719
{
public:

public:
};

struct Enum_t2432427458_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2953840665* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2432427458_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2953840665* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2953840665** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2953840665* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2432427458_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2432427458_marshaled_com
{
};
#endif // ENUM_T2432427458_H
#ifndef INVOKABLECALL_1_T917188150_H
#define INVOKABLECALL_1_T917188150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct  InvokableCall_1_t917188150  : public BaseInvokableCall_t3648497698
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t1891607558 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t917188150, ___Delegate_0)); }
	inline UnityAction_1_t1891607558 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t1891607558 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t1891607558 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T917188150_H
#ifndef ADDCOMPONENTMENU_T566310773_H
#define ADDCOMPONENTMENU_T566310773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t566310773  : public Attribute_t3130080784
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t566310773, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AddComponentMenu_0), value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t566310773, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOMPONENTMENU_T566310773_H
#ifndef NATIVECLASSATTRIBUTE_T417698780_H
#define NATIVECLASSATTRIBUTE_T417698780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NativeClassAttribute
struct  NativeClassAttribute_t417698780  : public Attribute_t3130080784
{
public:
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeClassAttribute_t417698780, ___U3CQualifiedNativeNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CQualifiedNativeNameU3Ek__BackingField_0() const { return ___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CQualifiedNativeNameU3Ek__BackingField_0() { return &___U3CQualifiedNativeNameU3Ek__BackingField_0; }
	inline void set_U3CQualifiedNativeNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CQualifiedNativeNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQualifiedNativeNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECLASSATTRIBUTE_T417698780_H
#ifndef IMECOMPOSITIONMODE_T1081719256_H
#define IMECOMPOSITIONMODE_T1081719256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.IMECompositionMode
struct  IMECompositionMode_t1081719256 
{
public:
	// System.Int32 UnityEngine.IMECompositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IMECompositionMode_t1081719256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMECOMPOSITIONMODE_T1081719256_H
#ifndef HIDEFLAGS_T2048557387_H
#define HIDEFLAGS_T2048557387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t2048557387 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t2048557387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T2048557387_H
#ifndef HEADERATTRIBUTE_T1170597384_H
#define HEADERATTRIBUTE_T1170597384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t1170597384  : public PropertyAttribute_t4289523592
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t1170597384, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERATTRIBUTE_T1170597384_H
#ifndef INTERNALVERTEXCHANNELTYPE_T1483901026_H
#define INTERNALVERTEXCHANNELTYPE_T1483901026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh/InternalVertexChannelType
struct  InternalVertexChannelType_t1483901026 
{
public:
	// System.Int32 UnityEngine.Mesh/InternalVertexChannelType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InternalVertexChannelType_t1483901026, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALVERTEXCHANNELTYPE_T1483901026_H
#ifndef DATETIMEKIND_T682864375_H
#define DATETIMEKIND_T682864375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t682864375 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t682864375, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T682864375_H
#ifndef REMOTENOTIFICATION_T357781426_H
#define REMOTENOTIFICATION_T357781426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.RemoteNotification
struct  RemoteNotification_t357781426  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.RemoteNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(RemoteNotification_t357781426, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTENOTIFICATION_T357781426_H
#ifndef KEYCODE_T2321673325_H
#define KEYCODE_T2321673325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2321673325 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2321673325, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2321673325_H
#ifndef ARGUMENTEXCEPTION_T489606696_H
#define ARGUMENTEXCEPTION_T489606696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t489606696  : public SystemException_t1379941789
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t489606696, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T489606696_H
#ifndef SEEKORIGIN_T2340993086_H
#define SEEKORIGIN_T2340993086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SeekOrigin
struct  SeekOrigin_t2340993086 
{
public:
	// System.Int32 System.IO.SeekOrigin::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeekOrigin_t2340993086, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEKORIGIN_T2340993086_H
#ifndef MATERIALPROPERTYBLOCK_T3731058050_H
#define MATERIALPROPERTYBLOCK_T3731058050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MaterialPropertyBlock
struct  MaterialPropertyBlock_t3731058050  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t3731058050, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYBLOCK_T3731058050_H
#ifndef MATHFINTERNAL_T1878424006_H
#define MATHFINTERNAL_T1878424006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.MathfInternal
struct  MathfInternal_t1878424006 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MathfInternal_t1878424006__padding[1];
	};

public:
};

struct MathfInternal_t1878424006_StaticFields
{
public:
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinNormal
	float ___FloatMinNormal_0;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinDenormal
	float ___FloatMinDenormal_1;
	// System.Boolean UnityEngineInternal.MathfInternal::IsFlushToZeroEnabled
	bool ___IsFlushToZeroEnabled_2;

public:
	inline static int32_t get_offset_of_FloatMinNormal_0() { return static_cast<int32_t>(offsetof(MathfInternal_t1878424006_StaticFields, ___FloatMinNormal_0)); }
	inline float get_FloatMinNormal_0() const { return ___FloatMinNormal_0; }
	inline float* get_address_of_FloatMinNormal_0() { return &___FloatMinNormal_0; }
	inline void set_FloatMinNormal_0(float value)
	{
		___FloatMinNormal_0 = value;
	}

	inline static int32_t get_offset_of_FloatMinDenormal_1() { return static_cast<int32_t>(offsetof(MathfInternal_t1878424006_StaticFields, ___FloatMinDenormal_1)); }
	inline float get_FloatMinDenormal_1() const { return ___FloatMinDenormal_1; }
	inline float* get_address_of_FloatMinDenormal_1() { return &___FloatMinDenormal_1; }
	inline void set_FloatMinDenormal_1(float value)
	{
		___FloatMinDenormal_1 = value;
	}

	inline static int32_t get_offset_of_IsFlushToZeroEnabled_2() { return static_cast<int32_t>(offsetof(MathfInternal_t1878424006_StaticFields, ___IsFlushToZeroEnabled_2)); }
	inline bool get_IsFlushToZeroEnabled_2() const { return ___IsFlushToZeroEnabled_2; }
	inline bool* get_address_of_IsFlushToZeroEnabled_2() { return &___IsFlushToZeroEnabled_2; }
	inline void set_IsFlushToZeroEnabled_2(bool value)
	{
		___IsFlushToZeroEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHFINTERNAL_T1878424006_H
#ifndef CONNECTIONCHANGEEVENT_T190635484_H
#define CONNECTIONCHANGEEVENT_T190635484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct  ConnectionChangeEvent_t190635484  : public UnityEvent_1_t2202423910
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCHANGEEVENT_T190635484_H
#ifndef ANIMATIONCURVE_T2893751101_H
#define ANIMATIONCURVE_T2893751101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t2893751101  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2893751101, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2893751101_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2893751101_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T2893751101_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T3659678378_H
#define INDEXOUTOFRANGEEXCEPTION_T3659678378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t3659678378  : public SystemException_t1379941789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T3659678378_H
#ifndef INTERNALSHADERCHANNEL_T679993459_H
#define INTERNALSHADERCHANNEL_T679993459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh/InternalShaderChannel
struct  InternalShaderChannel_t679993459 
{
public:
	// System.Int32 UnityEngine.Mesh/InternalShaderChannel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InternalShaderChannel_t679993459, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALSHADERCHANNEL_T679993459_H
#ifndef LOCALNOTIFICATION_T2192530767_H
#define LOCALNOTIFICATION_T2192530767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.LocalNotification
struct  LocalNotification_t2192530767  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.iOS.LocalNotification::notificationWrapper
	intptr_t ___notificationWrapper_0;

public:
	inline static int32_t get_offset_of_notificationWrapper_0() { return static_cast<int32_t>(offsetof(LocalNotification_t2192530767, ___notificationWrapper_0)); }
	inline intptr_t get_notificationWrapper_0() const { return ___notificationWrapper_0; }
	inline intptr_t* get_address_of_notificationWrapper_0() { return &___notificationWrapper_0; }
	inline void set_notificationWrapper_0(intptr_t value)
	{
		___notificationWrapper_0 = value;
	}
};

struct LocalNotification_t2192530767_StaticFields
{
public:
	// System.Int64 UnityEngine.iOS.LocalNotification::m_NSReferenceDateTicks
	int64_t ___m_NSReferenceDateTicks_1;

public:
	inline static int32_t get_offset_of_m_NSReferenceDateTicks_1() { return static_cast<int32_t>(offsetof(LocalNotification_t2192530767_StaticFields, ___m_NSReferenceDateTicks_1)); }
	inline int64_t get_m_NSReferenceDateTicks_1() const { return ___m_NSReferenceDateTicks_1; }
	inline int64_t* get_address_of_m_NSReferenceDateTicks_1() { return &___m_NSReferenceDateTicks_1; }
	inline void set_m_NSReferenceDateTicks_1(int64_t value)
	{
		___m_NSReferenceDateTicks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALNOTIFICATION_T2192530767_H
#ifndef GRADIENT_T2667682126_H
#define GRADIENT_T2667682126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t2667682126  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t2667682126, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t2667682126_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t2667682126_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T2667682126_H
#ifndef CACHEDINVOKABLECALL_1_T3021784382_H
#define CACHEDINVOKABLECALL_1_T3021784382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct  CachedInvokableCall_1_t3021784382  : public InvokableCall_1_t917188150
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	int32_t ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t3021784382, ___m_Arg1_1)); }
	inline int32_t get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline int32_t* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(int32_t value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T3021784382_H
#ifndef FILTERMODE_T327170756_H
#define FILTERMODE_T327170756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t327170756 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t327170756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T327170756_H
#ifndef CURSORLOCKMODE_T2153962017_H
#define CURSORLOCKMODE_T2153962017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorLockMode
struct  CursorLockMode_t2153962017 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorLockMode_t2153962017, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORLOCKMODE_T2153962017_H
#ifndef RUNTIMETYPEHANDLE_T1986816146_H
#define RUNTIMETYPEHANDLE_T1986816146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t1986816146 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t1986816146, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T1986816146_H
#ifndef ASYNCOPERATION_T3797817720_H
#define ASYNCOPERATION_T3797817720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t3797817720  : public YieldInstruction_t1540633877
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t1275866152 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t3797817720, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t3797817720, ___m_completeCallback_1)); }
	inline Action_1_t1275866152 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t1275866152 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t1275866152 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3797817720_marshaled_pinvoke : public YieldInstruction_t1540633877_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3797817720_marshaled_com : public YieldInstruction_t1540633877_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T3797817720_H
#ifndef WEAKREFERENCE_T1433439652_H
#define WEAKREFERENCE_T1433439652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.WeakReference
struct  WeakReference_t1433439652  : public RuntimeObject
{
public:
	// System.Boolean System.WeakReference::isLongReference
	bool ___isLongReference_0;
	// System.Runtime.InteropServices.GCHandle System.WeakReference::gcHandle
	GCHandle_t2748005453  ___gcHandle_1;

public:
	inline static int32_t get_offset_of_isLongReference_0() { return static_cast<int32_t>(offsetof(WeakReference_t1433439652, ___isLongReference_0)); }
	inline bool get_isLongReference_0() const { return ___isLongReference_0; }
	inline bool* get_address_of_isLongReference_0() { return &___isLongReference_0; }
	inline void set_isLongReference_0(bool value)
	{
		___isLongReference_0 = value;
	}

	inline static int32_t get_offset_of_gcHandle_1() { return static_cast<int32_t>(offsetof(WeakReference_t1433439652, ___gcHandle_1)); }
	inline GCHandle_t2748005453  get_gcHandle_1() const { return ___gcHandle_1; }
	inline GCHandle_t2748005453 * get_address_of_gcHandle_1() { return &___gcHandle_1; }
	inline void set_gcHandle_1(GCHandle_t2748005453  value)
	{
		___gcHandle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKREFERENCE_T1433439652_H
#ifndef CSSMEASUREMODE_T50254874_H
#define CSSMEASUREMODE_T50254874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureMode
struct  CSSMeasureMode_t50254874 
{
public:
	// System.Int32 UnityEngine.CSSLayout.CSSMeasureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CSSMeasureMode_t50254874, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREMODE_T50254874_H
#ifndef COROUTINE_T260305101_H
#define COROUTINE_T260305101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t260305101  : public YieldInstruction_t1540633877
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t260305101, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t260305101_marshaled_pinvoke : public YieldInstruction_t1540633877_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t260305101_marshaled_com : public YieldInstruction_t1540633877_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T260305101_H
#ifndef DISPLAY_T81008966_H
#define DISPLAY_T81008966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display
struct  Display_t81008966  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Display::nativeDisplay
	intptr_t ___nativeDisplay_0;

public:
	inline static int32_t get_offset_of_nativeDisplay_0() { return static_cast<int32_t>(offsetof(Display_t81008966, ___nativeDisplay_0)); }
	inline intptr_t get_nativeDisplay_0() const { return ___nativeDisplay_0; }
	inline intptr_t* get_address_of_nativeDisplay_0() { return &___nativeDisplay_0; }
	inline void set_nativeDisplay_0(intptr_t value)
	{
		___nativeDisplay_0 = value;
	}
};

struct Display_t81008966_StaticFields
{
public:
	// UnityEngine.Display[] UnityEngine.Display::displays
	DisplayU5BU5D_t2949376739* ___displays_1;
	// UnityEngine.Display UnityEngine.Display::_mainDisplay
	Display_t81008966 * ____mainDisplay_2;
	// UnityEngine.Display/DisplaysUpdatedDelegate UnityEngine.Display::onDisplaysUpdated
	DisplaysUpdatedDelegate_t1656811884 * ___onDisplaysUpdated_3;

public:
	inline static int32_t get_offset_of_displays_1() { return static_cast<int32_t>(offsetof(Display_t81008966_StaticFields, ___displays_1)); }
	inline DisplayU5BU5D_t2949376739* get_displays_1() const { return ___displays_1; }
	inline DisplayU5BU5D_t2949376739** get_address_of_displays_1() { return &___displays_1; }
	inline void set_displays_1(DisplayU5BU5D_t2949376739* value)
	{
		___displays_1 = value;
		Il2CppCodeGenWriteBarrier((&___displays_1), value);
	}

	inline static int32_t get_offset_of__mainDisplay_2() { return static_cast<int32_t>(offsetof(Display_t81008966_StaticFields, ____mainDisplay_2)); }
	inline Display_t81008966 * get__mainDisplay_2() const { return ____mainDisplay_2; }
	inline Display_t81008966 ** get_address_of__mainDisplay_2() { return &____mainDisplay_2; }
	inline void set__mainDisplay_2(Display_t81008966 * value)
	{
		____mainDisplay_2 = value;
		Il2CppCodeGenWriteBarrier((&____mainDisplay_2), value);
	}

	inline static int32_t get_offset_of_onDisplaysUpdated_3() { return static_cast<int32_t>(offsetof(Display_t81008966_StaticFields, ___onDisplaysUpdated_3)); }
	inline DisplaysUpdatedDelegate_t1656811884 * get_onDisplaysUpdated_3() const { return ___onDisplaysUpdated_3; }
	inline DisplaysUpdatedDelegate_t1656811884 ** get_address_of_onDisplaysUpdated_3() { return &___onDisplaysUpdated_3; }
	inline void set_onDisplaysUpdated_3(DisplaysUpdatedDelegate_t1656811884 * value)
	{
		___onDisplaysUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDisplaysUpdated_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAY_T81008966_H
#ifndef SENDMESSAGEOPTIONS_T4227618630_H
#define SENDMESSAGEOPTIONS_T4227618630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t4227618630 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t4227618630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T4227618630_H
#ifndef DRIVENTRANSFORMPROPERTIES_T1252734602_H
#define DRIVENTRANSFORMPROPERTIES_T1252734602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t1252734602 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t1252734602, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENTRANSFORMPROPERTIES_T1252734602_H
#ifndef OBJECT_T3645472222_H
#define OBJECT_T3645472222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t3645472222  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t3645472222, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t3645472222_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t3645472222_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t3645472222_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t3645472222_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T3645472222_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t3670318294
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef COMMANDBUFFER_T3383400165_H
#define COMMANDBUFFER_T3383400165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t3383400165  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t3383400165, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T3383400165_H
#ifndef DELEGATE_T4015201940_H
#define DELEGATE_T4015201940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t4015201940  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t2468626509 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___data_8)); }
	inline DelegateData_t2468626509 * get_data_8() const { return ___data_8; }
	inline DelegateData_t2468626509 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t2468626509 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T4015201940_H
#ifndef PARAMETERATTRIBUTES_T3508466278_H
#define PARAMETERATTRIBUTES_T3508466278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t3508466278 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t3508466278, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T3508466278_H
#ifndef CULLINGGROUP_T637827745_H
#define CULLINGGROUP_T637827745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup
struct  CullingGroup_t637827745  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.CullingGroup::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.CullingGroup/StateChanged UnityEngine.CullingGroup::m_OnStateChanged
	StateChanged_t1464331389 * ___m_OnStateChanged_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CullingGroup_t637827745, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_OnStateChanged_1() { return static_cast<int32_t>(offsetof(CullingGroup_t637827745, ___m_OnStateChanged_1)); }
	inline StateChanged_t1464331389 * get_m_OnStateChanged_1() const { return ___m_OnStateChanged_1; }
	inline StateChanged_t1464331389 ** get_address_of_m_OnStateChanged_1() { return &___m_OnStateChanged_1; }
	inline void set_m_OnStateChanged_1(StateChanged_t1464331389 * value)
	{
		___m_OnStateChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnStateChanged_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.CullingGroup
struct CullingGroup_t637827745_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
// Native definition for COM marshalling of UnityEngine.CullingGroup
struct CullingGroup_t637827745_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_OnStateChanged_1;
};
#endif // CULLINGGROUP_T637827745_H
#ifndef CAMERAEVENT_T1403981775_H
#define CAMERAEVENT_T1403981775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t1403981775 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t1403981775, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T1403981775_H
#ifndef RAY_T1807385980_H
#define RAY_T1807385980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1807385980 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3932393085  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3932393085  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1807385980, ___m_Origin_0)); }
	inline Vector3_t3932393085  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3932393085 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3932393085  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1807385980, ___m_Direction_1)); }
	inline Vector3_t3932393085  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3932393085 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3932393085  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1807385980_H
#ifndef BOUNDS_T3940617471_H
#define BOUNDS_T3940617471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3940617471 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3932393085  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3932393085  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3940617471, ___m_Center_0)); }
	inline Vector3_t3932393085  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3932393085 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3932393085  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3940617471, ___m_Extents_1)); }
	inline Vector3_t3932393085  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3932393085 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3932393085  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3940617471_H
#ifndef SCRIPTABLERENDERCONTEXT_T3554271739_H
#define SCRIPTABLERENDERCONTEXT_T3554271739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.ScriptableRenderContext
struct  ScriptableRenderContext_t3554271739 
{
public:
	// System.IntPtr UnityEngine.Experimental.Rendering.ScriptableRenderContext::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ScriptableRenderContext_t3554271739, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLERENDERCONTEXT_T3554271739_H
#ifndef RUNTIMEPLATFORM_T1521016828_H
#define RUNTIMEPLATFORM_T1521016828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t1521016828 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t1521016828, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T1521016828_H
#ifndef CAMERACLEARFLAGS_T3846301771_H
#define CAMERACLEARFLAGS_T3846301771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t3846301771 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t3846301771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T3846301771_H
#ifndef LOGTYPE_T3043133256_H
#define LOGTYPE_T3043133256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t3043133256 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t3043133256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T3043133256_H
#ifndef BINDINGFLAGS_T100712108_H
#define BINDINGFLAGS_T100712108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t100712108 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t100712108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T100712108_H
#ifndef PERSISTENTLISTENERMODE_T2310439664_H
#define PERSISTENTLISTENERMODE_T2310439664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentListenerMode
struct  PersistentListenerMode_t2310439664 
{
public:
	// System.Int32 UnityEngine.Events.PersistentListenerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PersistentListenerMode_t2310439664, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTLISTENERMODE_T2310439664_H
#ifndef TOUCHTYPE_T3178845108_H
#define TOUCHTYPE_T3178845108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t3178845108 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t3178845108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T3178845108_H
#ifndef CONSTRUCTORINFO_T3011499035_H
#define CONSTRUCTORINFO_T3011499035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ConstructorInfo
struct  ConstructorInfo_t3011499035  : public MethodBase_t3670318294
{
public:

public:
};

struct ConstructorInfo_t3011499035_StaticFields
{
public:
	// System.String System.Reflection.ConstructorInfo::ConstructorName
	String_t* ___ConstructorName_0;
	// System.String System.Reflection.ConstructorInfo::TypeConstructorName
	String_t* ___TypeConstructorName_1;

public:
	inline static int32_t get_offset_of_ConstructorName_0() { return static_cast<int32_t>(offsetof(ConstructorInfo_t3011499035_StaticFields, ___ConstructorName_0)); }
	inline String_t* get_ConstructorName_0() const { return ___ConstructorName_0; }
	inline String_t** get_address_of_ConstructorName_0() { return &___ConstructorName_0; }
	inline void set_ConstructorName_0(String_t* value)
	{
		___ConstructorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorName_0), value);
	}

	inline static int32_t get_offset_of_TypeConstructorName_1() { return static_cast<int32_t>(offsetof(ConstructorInfo_t3011499035_StaticFields, ___TypeConstructorName_1)); }
	inline String_t* get_TypeConstructorName_1() const { return ___TypeConstructorName_1; }
	inline String_t** get_address_of_TypeConstructorName_1() { return &___TypeConstructorName_1; }
	inline void set_TypeConstructorName_1(String_t* value)
	{
		___TypeConstructorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeConstructorName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORINFO_T3011499035_H
#ifndef CACHEDINVOKABLECALL_1_T1946426838_H
#define CACHEDINVOKABLECALL_1_T1946426838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct  CachedInvokableCall_1_t1946426838  : public InvokableCall_1_t4136797902
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	bool ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t1946426838, ___m_Arg1_1)); }
	inline bool get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline bool* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(bool value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T1946426838_H
#ifndef CACHEDINVOKABLECALL_1_T2765728243_H
#define CACHEDINVOKABLECALL_1_T2765728243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct  CachedInvokableCall_1_t2765728243  : public InvokableCall_1_t661132011
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	String_t* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t2765728243, ___m_Arg1_1)); }
	inline String_t* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline String_t** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(String_t* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T2765728243_H
#ifndef CACHEDINVOKABLECALL_1_T404005347_H
#define CACHEDINVOKABLECALL_1_T404005347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct  CachedInvokableCall_1_t404005347  : public InvokableCall_1_t2594376411
{
public:
	// T UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	float ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t404005347, ___m_Arg1_1)); }
	inline float get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline float* get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(float value)
	{
		___m_Arg1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T404005347_H
#ifndef UNITYEVENTCALLSTATE_T3116531571_H
#define UNITYEVENTCALLSTATE_T3116531571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventCallState
struct  UnityEventCallState_t3116531571 
{
public:
	// System.Int32 UnityEngine.Events.UnityEventCallState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityEventCallState_t3116531571, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTCALLSTATE_T3116531571_H
#ifndef TOUCHPHASE_T616769899_H
#define TOUCHPHASE_T616769899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t616769899 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t616769899, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T616769899_H
#ifndef CUBEMAPFACE_T923220006_H
#define CUBEMAPFACE_T923220006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t923220006 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t923220006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T923220006_H
#ifndef MULTICASTDELEGATE_T599438524_H
#define MULTICASTDELEGATE_T599438524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t599438524  : public Delegate_t4015201940
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t599438524 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t599438524 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t599438524, ___prev_9)); }
	inline MulticastDelegate_t599438524 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t599438524 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t599438524 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t599438524, ___kpm_next_10)); }
	inline MulticastDelegate_t599438524 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t599438524 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t599438524 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T599438524_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t1986816146  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t1986816146  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t1986816146 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t1986816146  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t98989581* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2882044324 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2882044324 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2882044324 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t98989581* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t98989581** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t98989581* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2882044324 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2882044324 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2882044324 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2882044324 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2882044324 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2882044324 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T2881801184_H
#define GAMEOBJECT_T2881801184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2881801184  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2881801184_H
#ifndef SCRIPTABLEOBJECT_T1324617639_H
#define SCRIPTABLEOBJECT_T1324617639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1324617639  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1324617639_marshaled_pinvoke : public Object_t3645472222_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1324617639_marshaled_com : public Object_t3645472222_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1324617639_H
#ifndef ASSETBUNDLEREQUEST_T3347087835_H
#define ASSETBUNDLEREQUEST_T3347087835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleRequest
struct  AssetBundleRequest_t3347087835  : public AsyncOperation_t3797817720
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t3347087835_marshaled_pinvoke : public AsyncOperation_t3797817720_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t3347087835_marshaled_com : public AsyncOperation_t3797817720_marshaled_com
{
};
#endif // ASSETBUNDLEREQUEST_T3347087835_H
#ifndef LOGGER_T4035514390_H
#define LOGGER_T4035514390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Logger
struct  Logger_t4035514390  : public RuntimeObject
{
public:
	// UnityEngine.ILogHandler UnityEngine.Logger::<logHandler>k__BackingField
	RuntimeObject* ___U3ClogHandlerU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Logger::<logEnabled>k__BackingField
	bool ___U3ClogEnabledU3Ek__BackingField_1;
	// UnityEngine.LogType UnityEngine.Logger::<filterLogType>k__BackingField
	int32_t ___U3CfilterLogTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClogHandlerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Logger_t4035514390, ___U3ClogHandlerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3ClogHandlerU3Ek__BackingField_0() const { return ___U3ClogHandlerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3ClogHandlerU3Ek__BackingField_0() { return &___U3ClogHandlerU3Ek__BackingField_0; }
	inline void set_U3ClogHandlerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3ClogHandlerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClogHandlerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClogEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Logger_t4035514390, ___U3ClogEnabledU3Ek__BackingField_1)); }
	inline bool get_U3ClogEnabledU3Ek__BackingField_1() const { return ___U3ClogEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3ClogEnabledU3Ek__BackingField_1() { return &___U3ClogEnabledU3Ek__BackingField_1; }
	inline void set_U3ClogEnabledU3Ek__BackingField_1(bool value)
	{
		___U3ClogEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Logger_t4035514390, ___U3CfilterLogTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CfilterLogTypeU3Ek__BackingField_2() const { return ___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CfilterLogTypeU3Ek__BackingField_2() { return &___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline void set_U3CfilterLogTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CfilterLogTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T4035514390_H
#ifndef TEXTURE_T954505614_H
#define TEXTURE_T954505614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t954505614  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T954505614_H
#ifndef MATERIAL_T1926439680_H
#define MATERIAL_T1926439680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t1926439680  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T1926439680_H
#ifndef ASSETBUNDLECREATEREQUEST_T2350432906_H
#define ASSETBUNDLECREATEREQUEST_T2350432906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AssetBundleCreateRequest
struct  AssetBundleCreateRequest_t2350432906  : public AsyncOperation_t3797817720
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t2350432906_marshaled_pinvoke : public AsyncOperation_t3797817720_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t2350432906_marshaled_com : public AsyncOperation_t3797817720_marshaled_com
{
};
#endif // ASSETBUNDLECREATEREQUEST_T2350432906_H
#ifndef ARGUMENTNULLEXCEPTION_T246139103_H
#define ARGUMENTNULLEXCEPTION_T246139103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t246139103  : public ArgumentException_t489606696
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T246139103_H
#ifndef PERSISTENTCALL_T1968425864_H
#define PERSISTENTCALL_T1968425864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.PersistentCall
struct  PersistentCall_t1968425864  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Events.PersistentCall::m_Target
	Object_t3645472222 * ___m_Target_0;
	// System.String UnityEngine.Events.PersistentCall::m_MethodName
	String_t* ___m_MethodName_1;
	// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::m_Mode
	int32_t ___m_Mode_2;
	// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::m_Arguments
	ArgumentCache_t537934761 * ___m_Arguments_3;
	// UnityEngine.Events.UnityEventCallState UnityEngine.Events.PersistentCall::m_CallState
	int32_t ___m_CallState_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(PersistentCall_t1968425864, ___m_Target_0)); }
	inline Object_t3645472222 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t3645472222 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t3645472222 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodName_1() { return static_cast<int32_t>(offsetof(PersistentCall_t1968425864, ___m_MethodName_1)); }
	inline String_t* get_m_MethodName_1() const { return ___m_MethodName_1; }
	inline String_t** get_address_of_m_MethodName_1() { return &___m_MethodName_1; }
	inline void set_m_MethodName_1(String_t* value)
	{
		___m_MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodName_1), value);
	}

	inline static int32_t get_offset_of_m_Mode_2() { return static_cast<int32_t>(offsetof(PersistentCall_t1968425864, ___m_Mode_2)); }
	inline int32_t get_m_Mode_2() const { return ___m_Mode_2; }
	inline int32_t* get_address_of_m_Mode_2() { return &___m_Mode_2; }
	inline void set_m_Mode_2(int32_t value)
	{
		___m_Mode_2 = value;
	}

	inline static int32_t get_offset_of_m_Arguments_3() { return static_cast<int32_t>(offsetof(PersistentCall_t1968425864, ___m_Arguments_3)); }
	inline ArgumentCache_t537934761 * get_m_Arguments_3() const { return ___m_Arguments_3; }
	inline ArgumentCache_t537934761 ** get_address_of_m_Arguments_3() { return &___m_Arguments_3; }
	inline void set_m_Arguments_3(ArgumentCache_t537934761 * value)
	{
		___m_Arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arguments_3), value);
	}

	inline static int32_t get_offset_of_m_CallState_4() { return static_cast<int32_t>(offsetof(PersistentCall_t1968425864, ___m_CallState_4)); }
	inline int32_t get_m_CallState_4() const { return ___m_CallState_4; }
	inline int32_t* get_address_of_m_CallState_4() { return &___m_CallState_4; }
	inline void set_m_CallState_4(int32_t value)
	{
		___m_CallState_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTCALL_T1968425864_H
#ifndef DATETIME_T218649865_H
#define DATETIME_T218649865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t218649865 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t595369841  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t218649865, ___ticks_0)); }
	inline TimeSpan_t595369841  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t595369841 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t595369841  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t218649865, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t218649865_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t218649865  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t218649865  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1589106382* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1589106382* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1589106382* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1589106382* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1589106382* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1589106382* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1589106382* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3215173111* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3215173111* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___MaxValue_2)); }
	inline DateTime_t218649865  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t218649865 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t218649865  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___MinValue_3)); }
	inline DateTime_t218649865  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t218649865 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t218649865  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1589106382* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1589106382* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1589106382* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1589106382* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1589106382* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1589106382* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1589106382* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1589106382* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1589106382* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1589106382* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1589106382* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1589106382** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1589106382* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1589106382* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1589106382** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1589106382* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t3215173111* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t3215173111** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t3215173111* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t3215173111* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t3215173111** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t3215173111* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T218649865_H
#ifndef TOUCH_T2641233222_H
#define TOUCH_T2641233222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t2641233222 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2246369278  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2246369278  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2246369278  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Position_1)); }
	inline Vector2_t2246369278  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2246369278 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2246369278  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_RawPosition_2)); }
	inline Vector2_t2246369278  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2246369278 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2246369278  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_PositionDelta_3)); }
	inline Vector2_t2246369278  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2246369278 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2246369278  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T2641233222_H
#ifndef PARAMETERINFO_T3156340899_H
#define PARAMETERINFO_T3156340899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t3156340899  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t4202419344 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___marshalAs_6)); }
	inline UnmanagedMarshal_t4202419344 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t4202419344 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t4202419344 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T3156340899_H
#ifndef FAILEDTOLOADSCRIPTOBJECT_T2081117541_H
#define FAILEDTOLOADSCRIPTOBJECT_T2081117541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FailedToLoadScriptObject
struct  FailedToLoadScriptObject_t2081117541  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t2081117541_marshaled_pinvoke : public Object_t3645472222_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.FailedToLoadScriptObject
struct FailedToLoadScriptObject_t2081117541_marshaled_com : public Object_t3645472222_marshaled_com
{
};
#endif // FAILEDTOLOADSCRIPTOBJECT_T2081117541_H
#ifndef MESH_T4237566844_H
#define MESH_T4237566844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t4237566844  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T4237566844_H
#ifndef COMPONENT_T2335505321_H
#define COMPONENT_T2335505321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t2335505321  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T2335505321_H
#ifndef UNITYACTION_1_T1891607558_H
#define UNITYACTION_1_T1891607558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Int32>
struct  UnityAction_1_t1891607558  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1891607558_H
#ifndef UNITYACTION_1_T1148388365_H
#define UNITYACTION_1_T1148388365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityAction_1_t1148388365  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1148388365_H
#ifndef PREDICATE_1_T959054467_H
#define PREDICATE_1_T959054467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct  Predicate_1_t959054467  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T959054467_H
#ifndef TRANSFORM_T3580444445_H
#define TRANSFORM_T3580444445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3580444445  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3580444445_H
#ifndef LOWMEMORYCALLBACK_T1978334135_H
#define LOWMEMORYCALLBACK_T1978334135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LowMemoryCallback
struct  LowMemoryCallback_t1978334135  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOWMEMORYCALLBACK_T1978334135_H
#ifndef LOGCALLBACK_T2572717340_H
#define LOGCALLBACK_T2572717340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LogCallback
struct  LogCallback_t2572717340  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCALLBACK_T2572717340_H
#ifndef UNITYACTION_T191347160_H
#define UNITYACTION_T191347160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t191347160  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T191347160_H
#ifndef ASYNCCALLBACK_T4283869127_H
#define ASYNCCALLBACK_T4283869127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t4283869127  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T4283869127_H
#ifndef ACTION_1_T1275866152_H
#define ACTION_1_T1275866152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.AsyncOperation>
struct  Action_1_t1275866152  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1275866152_H
#ifndef BEHAVIOUR_T3297694025_H
#define BEHAVIOUR_T3297694025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t3297694025  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T3297694025_H
#ifndef RENDERTEXTURE_T3737750657_H
#define RENDERTEXTURE_T3737750657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t3737750657  : public Texture_t954505614
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T3737750657_H
#ifndef CAMERACALLBACK_T3349755816_H
#define CAMERACALLBACK_T3349755816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t3349755816  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACALLBACK_T3349755816_H
#ifndef PLAYERCONNECTION_T3190352615_H
#define PLAYERCONNECTION_T3190352615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct  PlayerConnection_t3190352615  : public ScriptableObject_t1324617639
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents UnityEngine.Networking.PlayerConnection.PlayerConnection::m_PlayerEditorConnectionEvents
	PlayerEditorConnectionEvents_t1009044977 * ___m_PlayerEditorConnectionEvents_3;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Networking.PlayerConnection.PlayerConnection::m_connectedPlayers
	List_1_t894813333 * ___m_connectedPlayers_4;
	// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::m_IsInitilized
	bool ___m_IsInitilized_5;

public:
	inline static int32_t get_offset_of_m_PlayerEditorConnectionEvents_3() { return static_cast<int32_t>(offsetof(PlayerConnection_t3190352615, ___m_PlayerEditorConnectionEvents_3)); }
	inline PlayerEditorConnectionEvents_t1009044977 * get_m_PlayerEditorConnectionEvents_3() const { return ___m_PlayerEditorConnectionEvents_3; }
	inline PlayerEditorConnectionEvents_t1009044977 ** get_address_of_m_PlayerEditorConnectionEvents_3() { return &___m_PlayerEditorConnectionEvents_3; }
	inline void set_m_PlayerEditorConnectionEvents_3(PlayerEditorConnectionEvents_t1009044977 * value)
	{
		___m_PlayerEditorConnectionEvents_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerEditorConnectionEvents_3), value);
	}

	inline static int32_t get_offset_of_m_connectedPlayers_4() { return static_cast<int32_t>(offsetof(PlayerConnection_t3190352615, ___m_connectedPlayers_4)); }
	inline List_1_t894813333 * get_m_connectedPlayers_4() const { return ___m_connectedPlayers_4; }
	inline List_1_t894813333 ** get_address_of_m_connectedPlayers_4() { return &___m_connectedPlayers_4; }
	inline void set_m_connectedPlayers_4(List_1_t894813333 * value)
	{
		___m_connectedPlayers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectedPlayers_4), value);
	}

	inline static int32_t get_offset_of_m_IsInitilized_5() { return static_cast<int32_t>(offsetof(PlayerConnection_t3190352615, ___m_IsInitilized_5)); }
	inline bool get_m_IsInitilized_5() const { return ___m_IsInitilized_5; }
	inline bool* get_address_of_m_IsInitilized_5() { return &___m_IsInitilized_5; }
	inline void set_m_IsInitilized_5(bool value)
	{
		___m_IsInitilized_5 = value;
	}
};

struct PlayerConnection_t3190352615_StaticFields
{
public:
	// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::connectionNative
	RuntimeObject* ___connectionNative_2;
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::s_Instance
	PlayerConnection_t3190352615 * ___s_Instance_6;

public:
	inline static int32_t get_offset_of_connectionNative_2() { return static_cast<int32_t>(offsetof(PlayerConnection_t3190352615_StaticFields, ___connectionNative_2)); }
	inline RuntimeObject* get_connectionNative_2() const { return ___connectionNative_2; }
	inline RuntimeObject** get_address_of_connectionNative_2() { return &___connectionNative_2; }
	inline void set_connectionNative_2(RuntimeObject* value)
	{
		___connectionNative_2 = value;
		Il2CppCodeGenWriteBarrier((&___connectionNative_2), value);
	}

	inline static int32_t get_offset_of_s_Instance_6() { return static_cast<int32_t>(offsetof(PlayerConnection_t3190352615_StaticFields, ___s_Instance_6)); }
	inline PlayerConnection_t3190352615 * get_s_Instance_6() const { return ___s_Instance_6; }
	inline PlayerConnection_t3190352615 ** get_address_of_s_Instance_6() { return &___s_Instance_6; }
	inline void set_s_Instance_6(PlayerConnection_t3190352615 * value)
	{
		___s_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTION_T3190352615_H
#ifndef RENDERER_T3456269968_H
#define RENDERER_T3456269968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t3456269968  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T3456269968_H
#ifndef STATECHANGED_T1464331389_H
#define STATECHANGED_T1464331389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CullingGroup/StateChanged
struct  StateChanged_t1464331389  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATECHANGED_T1464331389_H
#ifndef DISPLAYSUPDATEDDELEGATE_T1656811884_H
#define DISPLAYSUPDATEDDELEGATE_T1656811884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Display/DisplaysUpdatedDelegate
struct  DisplaysUpdatedDelegate_t1656811884  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYSUPDATEDDELEGATE_T1656811884_H
#ifndef MESHFILTER_T3228362100_H
#define MESHFILTER_T3228362100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t3228362100  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T3228362100_H
#ifndef CSSMEASUREFUNC_T2649428469_H
#define CSSMEASUREFUNC_T2649428469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureFunc
struct  CSSMeasureFunc_t2649428469  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREFUNC_T2649428469_H
#ifndef CAMERA_T362346687_H
#define CAMERA_T362346687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t362346687  : public Behaviour_t3297694025
{
public:

public:
};

struct Camera_t362346687_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t3349755816 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t3349755816 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t3349755816 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t3349755816 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t3349755816 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t3349755816 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t3349755816 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t3349755816 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t3349755816 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t3349755816 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t3349755816 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t3349755816 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T362346687_H
#ifndef RECTTRANSFORM_T1161610249_H
#define RECTTRANSFORM_T1161610249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t1161610249  : public Transform_t3580444445
{
public:

public:
};

struct RectTransform_t1161610249_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1147600571 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t1161610249_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1147600571 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1147600571 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1147600571 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T1161610249_H
#ifndef GUIELEMENT_T2757512779_H
#define GUIELEMENT_T2757512779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t2757512779  : public Behaviour_t3297694025
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T2757512779_H
#ifndef GUILAYER_T1634068716_H
#define GUILAYER_T1634068716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t1634068716  : public Behaviour_t3297694025
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T1634068716_H
#ifndef LIGHT_T455885974_H
#define LIGHT_T455885974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Light
struct  Light_t455885974  : public Behaviour_t3297694025
{
public:
	// System.Int32 UnityEngine.Light::m_BakedIndex
	int32_t ___m_BakedIndex_2;

public:
	inline static int32_t get_offset_of_m_BakedIndex_2() { return static_cast<int32_t>(offsetof(Light_t455885974, ___m_BakedIndex_2)); }
	inline int32_t get_m_BakedIndex_2() const { return ___m_BakedIndex_2; }
	inline int32_t* get_address_of_m_BakedIndex_2() { return &___m_BakedIndex_2; }
	inline void set_m_BakedIndex_2(int32_t value)
	{
		___m_BakedIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHT_T455885974_H
#ifndef MESHRENDERER_T1975789539_H
#define MESHRENDERER_T1975789539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t1975789539  : public Renderer_t3456269968
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T1975789539_H
#ifndef MONOBEHAVIOUR_T4008345588_H
#define MONOBEHAVIOUR_T4008345588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4008345588  : public Behaviour_t3297694025
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4008345588_H
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t662635263  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t1233380954  m_Items[1];

public:
	inline Keyframe_t1233380954  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t1233380954 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t1233380954  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t1233380954  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t1233380954 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t1233380954  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3385344125  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t98989581  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t3043391348  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RequireComponent_t3219866953 * m_Items[1];

public:
	inline RequireComponent_t3219866953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RequireComponent_t3219866953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RequireComponent_t3219866953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RequireComponent_t3219866953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RequireComponent_t3219866953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RequireComponent_t3219866953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t3764267775  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DisallowMultipleComponent_t4131774042 * m_Items[1];

public:
	inline DisallowMultipleComponent_t4131774042 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t4131774042 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DisallowMultipleComponent_t4131774042 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DisallowMultipleComponent_t4131774042 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t4131774042 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DisallowMultipleComponent_t4131774042 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1903432293  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ExecuteInEditMode_t2915138156 * m_Items[1];

public:
	inline ExecuteInEditMode_t2915138156 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExecuteInEditMode_t2915138156 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExecuteInEditMode_t2915138156 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExecuteInEditMode_t2915138156 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExecuteInEditMode_t2915138156 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExecuteInEditMode_t2915138156 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t365668710  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t362346687 * m_Items[1];

public:
	inline Camera_t362346687 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t362346687 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t362346687 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t362346687 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t362346687 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t362346687 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t3930959911  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) intptr_t m_Items[1];

public:
	inline intptr_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline intptr_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, intptr_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline intptr_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline intptr_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, intptr_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Display[]
struct DisplayU5BU5D_t2949376739  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Display_t81008966 * m_Items[1];

public:
	inline Display_t81008966 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Display_t81008966 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Display_t81008966 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Display_t81008966 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Display_t81008966 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Display_t81008966 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1048445298  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t3156340899 * m_Items[1];

public:
	inline ParameterInfo_t3156340899 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t3156340899 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t3156340899 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t3156340899 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t3156340899 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t3156340899 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1706708300  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t1933193809  m_Items[1];

public:
	inline ParameterModifier_t1933193809  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t1933193809 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t1933193809  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t1933193809  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t1933193809 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t1933193809  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t3567143369  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3215173111  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1432878832  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3932393085  m_Items[1];

public:
	inline Vector3_t3932393085  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3932393085 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3932393085  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3932393085  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3932393085 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3932393085  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1632896738  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_t1900979187  m_Items[1];

public:
	inline Vector4_t1900979187  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_t1900979187 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_t1900979187  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_t1900979187  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_t1900979187 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_t1900979187  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3868613195  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t2246369278  m_Items[1];

public:
	inline Vector2_t2246369278  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2246369278 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2246369278  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2246369278  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2246369278 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2246369278  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t2352998046  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t662424039  m_Items[1];

public:
	inline Color32_t662424039  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t662424039 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t662424039  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t662424039  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t662424039 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t662424039  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m518628164_gshared (Action_1_t2325042916 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m3317293189_gshared (Stack_1_t2545034337 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m3775744293_gshared (Stack_1_t2545034337 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m3823303058_gshared (Stack_1_t2545034337 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m4229184994_gshared (Stack_1_t2545034337 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m1366553884_gshared (List_1_t2226230279 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m2901067668_gshared (List_1_t2226230279 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3385344125* List_1_ToArray_m1576318576_gshared (List_1_t2226230279 * __this, const RuntimeMethod* method);
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  RuntimeObject * AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m2224392416_gshared (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3171303015_gshared (Dictionary_2_t588842846 * __this, intptr_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2595651922_gshared (Dictionary_2_t588842846 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m389503476_gshared (List_1_t2226230279 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m3143676148_gshared (List_1_t2226230279 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m471181499_gshared (Predicate_1_t2157551253 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C"  int32_t List_1_RemoveAll_m2349324857_gshared (List_1_t2226230279 * __this, Predicate_1_t2157551253 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m2716127220_gshared (List_1_t2226230279 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m3084508301_gshared (List_1_t2226230279 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2394803162_gshared (CachedInvokableCall_1_t404005347 * __this, Object_t3645472222 * p0, MethodInfo_t * p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1010281405_gshared (CachedInvokableCall_1_t3021784382 * __this, Object_t3645472222 * p0, MethodInfo_t * p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1496783833_gshared (CachedInvokableCall_1_t58234032 * __this, Object_t3645472222 * p0, MethodInfo_t * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m4230261306_gshared (CachedInvokableCall_1_t1946426838 * __this, Object_t3645472222 * p0, MethodInfo_t * p1, bool p2, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1984039949  List_1_GetEnumerator_m71231483_gshared (List_1_t2226230279 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3174814902_gshared (Enumerator_t1984039949 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2658185187_gshared (Enumerator_t1984039949 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3723791761_gshared (Enumerator_t1984039949 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mesh::SafeLength<System.Int32>(System.Collections.Generic.List`1<T>)
extern "C"  int32_t Mesh_SafeLength_TisInt32_t3515577538_m12458220_gshared (Mesh_t4237566844 * __this, List_1_t894813333 * ___values0, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1432878832* Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t1632896738* Mesh_GetAllocArrayFromChannel_TisVector4_t1900979187_m1344632441_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t3868613195* Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, const RuntimeMethod* method);
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t2352998046* Mesh_GetAllocArrayFromChannel_TisColor32_t662424039_m1356534377_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, List_1_t1311628880 * ___values1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisVector4_t1900979187_m2034622288_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, List_1_t3575182278 * ___values1, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetListForChannel_TisColor32_t662424039_m1488066680_gshared (Mesh_t4237566844 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, List_1_t2336627130 * ___values3, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::SetUvsImpl<UnityEngine.Vector2>(System.Int32,System.Int32,System.Collections.Generic.List`1<T>)
extern "C"  void Mesh_SetUvsImpl_TisVector2_t2246369278_m3260026371_gshared (Mesh_t4237566844 * __this, int32_t ___uvIndex0, int32_t ___dim1, List_1_t3920572369 * ___uvs2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m2297325353_gshared (List_1_t894813333 * __this, const RuntimeMethod* method);
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m3027478402_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisRuntimeObject_m3816297283_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m3455660670_gshared (UnityEvent_1_t3533840856 * __this, UnityAction_1_t3223024504 * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t652623003  List_1_GetEnumerator_m2611892158_gshared (List_1_t894813333 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3396935530_gshared (Enumerator_t652623003 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m278533989_gshared (UnityAction_1_t1891607558 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3554339858_gshared (Enumerator_t652623003 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m933170505_gshared (Enumerator_t652623003 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2986871176_gshared (UnityEvent_1_t2202423910 * __this, UnityAction_1_t1891607558 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m1589466641_gshared (List_1_t894813333 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m305340876_gshared (UnityEvent_1_t2202423910 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m30995859 (Attribute_t3130080784 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2939335827 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m2273450792 (AnimationCurve_t2893751101 * __this, KeyframeU5BU5D_t662635263* ___keys0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m2375103925 (AnimationCurve_t2893751101 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m2448312333 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3832791678 (LowMemoryCallback_t1978334135 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m2422533362 (LogCallback_t2572717340 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m4106354529 (UnityAction_t191347160 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m515855777 (AsyncOperation_t3797817720 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.AsyncOperation>::Invoke(!0)
#define Action_1_Invoke_m2532647139(__this, p0, method) ((  void (*) (Action_1_t1275866152 *, AsyncOperation_t3797817720 *, const RuntimeMethod*))Action_1_Invoke_m518628164_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
#define Stack_1__ctor_m3056069383(__this, method) ((  void (*) (Stack_1_t3041366545 *, const RuntimeMethod*))Stack_1__ctor_m3317293189_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(!0)
#define Stack_1_Push_m1690848609(__this, p0, method) ((  void (*) (Stack_1_t3041366545 *, Type_t *, const RuntimeMethod*))Stack_1_Push_m3775744293_gshared)(__this, p0, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m181058154 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t1986816146  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m2531133666(__this, method) ((  Type_t * (*) (Stack_1_t3041366545 *, const RuntimeMethod*))Stack_1_Pop_m3823303058_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m234060416(__this, method) ((  int32_t (*) (Stack_1_t3041366545 *, const RuntimeMethod*))Stack_1_get_Count_m4229184994_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
#define List_1__ctor_m946701582(__this, method) ((  void (*) (List_1_t2722562487 *, const RuntimeMethod*))List_1__ctor_m1366553884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(!0)
#define List_1_Add_m3025470644(__this, p0, method) ((  void (*) (List_1_t2722562487 *, Type_t *, const RuntimeMethod*))List_1_Add_m2901067668_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m2051283729(__this, method) ((  TypeU5BU5D_t98989581* (*) (List_1_t2722562487 *, const RuntimeMethod*))List_1_ToArray_m1576318576_gshared)(__this, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<UnityEngine.DefaultExecutionOrder>(System.Type)
#define AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t1108411129_m62422214(__this /* static, unused */, ___klass0, method) ((  DefaultExecutionOrder_t1108411129 * (*) (RuntimeObject * /* static, unused */, Type_t *, const RuntimeMethod*))AttributeHelperEngine_GetCustomAttributeOfType_TisRuntimeObject_m2224392416_gshared)(__this /* static, unused */, ___klass0, method)
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3740981065 (DefaultExecutionOrder_t1108411129 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3044674138 (Component_t2335505321 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3932393085  Vector3_op_Multiply_m2900776395 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m3205218803 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___center0, Vector3_t3932393085  ___size1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t3932393085  Bounds_get_center_m1014667176 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m2995138410 (Vector3_t3932393085 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t3932393085  Bounds_get_extents_m1234217580 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m1070078697 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m1554169531 (Vector3_t3932393085 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m3886448245 (Bounds_t3940617471 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m3304453970 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t3932393085  Bounds_get_size_m3922782264 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m3828080681 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2266073082 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Vector3_op_Subtraction_m2511661424 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___a0, Vector3_t3932393085  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t3932393085  Bounds_get_min_m2956001499 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Vector3_op_Addition_m2513759451 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___a0, Vector3_t3932393085  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t3932393085  Bounds_get_max_m1445821865 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m2587075067 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___lhs0, Vector3_t3932393085  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m4088733844 (RuntimeObject * __this /* static, unused */, Bounds_t3940617471  ___lhs0, Bounds_t3940617471  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1614096005 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___min0, Vector3_t3932393085  ___max1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Vector3_Min_m1113176117 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___lhs0, Vector3_t3932393085  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Vector3_Max_m197737547 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___lhs0, Vector3_t3932393085  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3006561347 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m1784030070 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3385344125* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m3206324941 (Bounds_t3940617471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m1626382119 (Behaviour_t3297694025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m479895925 (Camera_t362346687 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m428635793 (Camera_t362346687 * __this, Matrix4x4_t787966842 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m139610015 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m503392759 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m12666471 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Vector3_t3932393085 * ___position1, Ray_t1807385980 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m3225907431 (CameraCallback_t3349755816 * __this, Camera_t362346687 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_INTERNAL_CALL_RaycastTry_m473860371 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Ray_t1807385980 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_INTERNAL_CALL_RaycastTry2D_m1075685860 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Ray_t1807385980 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m1685913522 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3386337842 (Color_t1361298052 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3950769051 (Color_t1361298052 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m961366009 (Color_t1361298052 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t1900979187  Color_op_Implicit_m1778966118 (RuntimeObject * __this /* static, unused */, Color_t1361298052  ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m2425648894 (Vector4_t1900979187 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m762054233 (Color_t1361298052 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m847285714 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m249927524 (Color_t1361298052 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m1785435548 (RuntimeObject * __this /* static, unused */, Vector4_t1900979187  ___lhs0, Vector4_t1900979187  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2797960148 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m898810465 (Vector4_t1900979187 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m2963160534 (Color32_t662424039 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m649264193 (Color32_t662424039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m2395954052 (Object_t3645472222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2881801184 * Component_get_gameObject_m3138408676 (Component_t2335505321 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t2335505321 * GameObject_GetComponent_m1768480708 (GameObject_t2881801184 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t2335505321 * GameObject_GetComponentInChildren_m58285979 (GameObject_t2881801184 * __this, Type_t * ___type0, bool ___includeInactive1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t2335505321 * GameObject_GetComponentInParent_m390353812 (GameObject_t2881801184 * __this, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m2424663091 (Component_t2335505321 * __this, Type_t * ___searchType0, RuntimeObject * ___resultList1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m2841548285 (YieldInstruction_t1540633877 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m1956794736 (Coroutine_t260305101 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t83932994  CSSMeasureFunc_Invoke_m2931059520 (CSSMeasureFunc_t2649428469 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m2231896620(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1470255310 *, intptr_t, WeakReference_t1433439652 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3171303015_gshared)(__this, p0, p1, method)
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2649428469 * Native_CSSNodeGetMeasureFunc_m485971160 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m259387555 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>::.ctor()
#define Dictionary_2__ctor_m2012856830(__this, method) ((  void (*) (Dictionary_2_t1470255310 *, const RuntimeMethod*))Dictionary_2__ctor_m2595651922_gshared)(__this, method)
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m183498676 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m3638411044 (CullingGroup_t637827745 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m1034235824 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m688567370 (StateChanged_t1464331389 * __this, CullingGroupEvent_t505730933  ___sphere0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m866078178 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m165201388 (DebugLogHandler_t2540765360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m998766840 (Logger_t4035514390 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m3139124310 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3385344125* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m3491589508 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t3645472222 * ___obj2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m102761498 (RuntimeObject * __this /* static, unused */, Exception_t214279536 * ___exception0, Object_t3645472222 * ___obj1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int32)
extern "C"  void IntPtr__ctor_m1737770000 (intptr_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m2661054513 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m1135348170 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m571773009 (RuntimeObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m1727525657 (Display_t81008966 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m1549941880 (DisplaysUpdatedDelegate_t1656811884 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m1198470498 (Display_t81008966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m1543013735 (DrivenRectTransformTracker_t110585296 * __this, Object_t3645472222 * ___driver0, RectTransform_t1161610249 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear(System.Boolean)
extern "C"  void DrivenRectTransformTracker_Clear_m465650438 (DrivenRectTransformTracker_t110585296 * __this, bool ___revertValues0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m156265900 (DrivenRectTransformTracker_t110585296 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2536109821 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m3835433624 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m2069972063 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1162620119 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m2337007599 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2806582524 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m464151535 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m2488792999 (ArgumentNullException_t246139103 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  RuntimeObject * Delegate_get_Target_m1225911449 (Delegate_t4015201940 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m898979290 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1808560565 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___x0, Object_t3645472222 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2255002992 (BaseInvokableCall_t3648497698 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t4015201940 * NetFxCoreExtensions_CreateDelegate_m3762475387 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m660321946 (InvokableCall_t1976387117 * __this, UnityAction_t191347160 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m2962492325 (BaseInvokableCall_t3648497698 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t4015201940 * Delegate_Combine_m4046213137 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * p0, Delegate_t4015201940 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t4015201940 * Delegate_Remove_m897454397 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * p0, Delegate_t4015201940 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m3930864202 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * ___delegate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m3556049827 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m2996853262(__this, method) ((  void (*) (List_1_t1027733493 *, const RuntimeMethod*))List_1__ctor_m1366553884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0)
#define List_1_Add_m116104406(__this, p0, method) ((  void (*) (List_1_t1027733493 *, BaseInvokableCall_t3648497698 *, const RuntimeMethod*))List_1_Add_m2901067668_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m4078513680(__this, p0, method) ((  BaseInvokableCall_t3648497698 * (*) (List_1_t1027733493 *, int32_t, const RuntimeMethod*))List_1_get_Item_m389503476_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m1195210665(__this, method) ((  int32_t (*) (List_1_t1027733493 *, const RuntimeMethod*))List_1_get_Count_m3143676148_gshared)(__this, method)
// System.Void System.Predicate`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m106168190(__this, p0, p1, method) ((  void (*) (Predicate_1_t959054467 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m471181499_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<!0>)
#define List_1_RemoveAll_m601647338(__this, p0, method) ((  int32_t (*) (List_1_t1027733493 *, Predicate_1_t959054467 *, const RuntimeMethod*))List_1_RemoveAll_m2349324857_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m3693544616(__this, method) ((  void (*) (List_1_t1027733493 *, const RuntimeMethod*))List_1_Clear_m2716127220_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m55576690(__this, p0, method) ((  void (*) (List_1_t1027733493 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m3084508301_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m4246442324 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t3645472222 * PersistentCall_get_target_m1664545602 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m3175548779 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m3743863011 (UnityEventBase_t2081553920 * __this, PersistentCall_t1968425864 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t3648497698 * PersistentCall_GetObjectCall_m155688820 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t537934761 * ___arguments2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m1788594777 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2394803162(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t404005347 *, Object_t3645472222 *, MethodInfo_t *, float, const RuntimeMethod*))CachedInvokableCall_1__ctor_m2394803162_gshared)(__this, p0, p1, p2, method)
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m2390490498 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m1010281405(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3021784382 *, Object_t3645472222 *, MethodInfo_t *, int32_t, const RuntimeMethod*))CachedInvokableCall_1__ctor_m1010281405_gshared)(__this, p0, p1, p2, method)
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m3913918529 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m163013044(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t2765728243 *, Object_t3645472222 *, MethodInfo_t *, String_t*, const RuntimeMethod*))CachedInvokableCall_1__ctor_m1496783833_gshared)(__this, p0, p1, p2, method)
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m2685124831 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m4230261306(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t1946426838 *, Object_t3645472222 *, MethodInfo_t *, bool, const RuntimeMethod*))CachedInvokableCall_1__ctor_m4230261306_gshared)(__this, p0, p1, p2, method)
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m631237306 (InvokableCall_t1976387117 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2699605163 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C"  Type_t * Type_GetType_m1907918864 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[])
extern "C"  ConstructorInfo_t3011499035 * Type_GetConstructor_m4289629996 (Type_t * __this, TypeU5BU5D_t98989581* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t3645472222 * ArgumentCache_get_unityObjectArgument_m1264291904 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m392475457 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern "C"  RuntimeObject * ConstructorInfo_Invoke_m3356180212 (ConstructorInfo_t3011499035 * __this, ObjectU5BU5D_t3385344125* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
#define List_1__ctor_m1383490399(__this, method) ((  void (*) (List_1_t3642628955 *, const RuntimeMethod*))List_1__ctor_m1366553884_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m4154154611(__this, method) ((  Enumerator_t3400438625  (*) (List_1_t3642628955 *, const RuntimeMethod*))List_1_GetEnumerator_m71231483_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m2770969459(__this, method) ((  PersistentCall_t1968425864 * (*) (Enumerator_t3400438625 *, const RuntimeMethod*))Enumerator_get_Current_m3174814902_gshared)(__this, method)
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m2393902497 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t3648497698 * PersistentCall_GetRuntimeCall_m2894046812 (PersistentCall_t1968425864 * __this, UnityEventBase_t2081553920 * ___theEvent0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m678893588 (InvokableCallList_t3172597217 * __this, BaseInvokableCall_t3648497698 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m1658440964(__this, method) ((  bool (*) (Enumerator_t3400438625 *, const RuntimeMethod*))Enumerator_MoveNext_m2658185187_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m2859623709(__this, method) ((  void (*) (Enumerator_t3400438625 *, const RuntimeMethod*))Enumerator_Dispose_m3723791761_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1267297027 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t3648497698 * UnityEvent_GetDelegate_m3591116204 (RuntimeObject * __this /* static, unused */, UnityAction_t191347160 * ___action0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3895696039 (UnityEventBase_t2081553920 * __this, BaseInvokableCall_t3648497698 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1984273667 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t98989581* ___argumentTypes2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m334505956 (InvokableCall_t1976387117 * __this, UnityAction_t191347160 * ___action0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern "C"  List_1_t1027733493 * UnityEventBase_PrepareInvoke_m1597816360 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::Invoke()
extern "C"  void InvokableCall_Invoke_m1537249999 (InvokableCall_t1976387117 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m3925837574 (InvokableCallList_t3172597217 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m158630386 (PersistentCallGroup_t3408587689 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m1757731517 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t537934761 * PersistentCall_get_arguments_m1403296607 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m3633496894 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m2734136314 (UnityEventBase_t2081553920 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m2913537599 (InvokableCallList_t3172597217 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m1336092061 (PersistentCallGroup_t3408587689 * __this, InvokableCallList_t3172597217 * ___invokableList0, UnityEventBase_t2081553920 * ___unityEventBase1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m3921073616 (InvokableCallList_t3172597217 * __this, BaseInvokableCall_t3648497698 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m3353336194 (InvokableCallList_t3172597217 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m4077085260 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern "C"  List_1_t1027733493 * InvokableCallList_PrepareInvoke_m965587067 (InvokableCallList_t3172597217 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Object::ToString()
extern "C"  String_t* Object_ToString_m3653455109 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3213599556 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  MethodInfo_t * Type_GetMethod_m4168175761 (Type_t * __this, String_t* p0, int32_t p1, Binder_t2026886273 * p2, TypeU5BU5D_t98989581* p3, ParameterModifierU5BU5D_t1706708300* p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m1820091081 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m603220370 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m4070566852 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m2834317064 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m3085940871 (ScriptableRenderContext_t3554271739 * __this, intptr_t ___ptr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m1732771618 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m4138994077 (RuntimeObject * __this /* static, unused */, GameObject_t2881801184 * ___mono0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t2335505321 * GameObject_AddComponent_m1531083216 (GameObject_t2881801184 * __this, Type_t * ___componentType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C"  Component_t2335505321 * GameObject_Internal_AddComponentWithType_m246566982 (GameObject_t2881801184 * __this, Type_t * ___componentType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m3509320913 (Gradient_t2667682126 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m3098355422 (Gradient_t2667682126 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t2757512779 * GUILayer_INTERNAL_CALL_HitTest_m4196901861 (RuntimeObject * __this /* static, unused */, GUILayer_t1634068716 * ___self0, Vector3_t3932393085 * ___screenPosition1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3690955331 (PropertyAttribute_t4289523592 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m1318263342 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_mouseScrollDelta_m2002340172 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m4079784025 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Touch_t2641233222 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_compositionCursorPos_m4215801410 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_set_compositionCursorPos_m2989051778 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m3489993842 (DefaultValueAttribute_t1912603919 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Attribute::GetHashCode()
extern "C"  int32_t Attribute_GetHashCode_m844029469 (Attribute_t3130080784 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m2598449680 (LocalNotification_t2192530767 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.DateTimeKind)
extern "C"  void DateTime__ctor_m1351701445 (DateTime_t218649865 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m629892755 (DateTime_t218649865 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m1378408698 (RemoteNotification_t357781426 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m1865923159 (Logger_t4035514390 * __this, RuntimeObject* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m81927742 (Logger_t4035514390 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m3869200416 (Logger_t4035514390 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m2365136669 (Logger_t4035514390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m3881820577 (Logger_t4035514390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m3980787710 (Logger_t4035514390 * __this, int32_t ___logType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  RuntimeObject* Logger_get_logHandler_m1556655871 (Logger_t4035514390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Logger::GetString(System.Object)
extern "C"  String_t* Logger_GetString_m2315344356 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
extern "C"  void ArgumentNullException__ctor_m2341126836 (ArgumentNullException_t246139103 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m158539512 (ArgumentException_t489606696 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3432723813 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern "C"  void ManagedStreamHelpers_ValidateLoadFromStream_m2234507788 (RuntimeObject * __this /* static, unused */, Stream_t360920014 * ___stream0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m1324188081 (RuntimeObject * __this /* static, unused */, Material_t1926439680 * ___mono0, Material_t1926439680 * ___source1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m3068886589 (Material_t1926439680 * __this, String_t* ___name0, Color_t1361298052  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C"  Texture_t954505614 * Material_GetTexture_m4101001118 (Material_t1926439680 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColorImpl_m1120750802 (RuntimeObject * __this /* static, unused */, Material_t1926439680 * ___self0, int32_t ___nameID1, Color_t1361298052 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Material_INTERNAL_CALL_SetMatrixImpl_m1727523669 (RuntimeObject * __this /* static, unused */, Material_t1926439680 * ___self0, int32_t ___nameID1, Matrix4x4_t787966842 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m740525257 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C"  bool Material_HasProperty_m1510906695 (Material_t1926439680 * __this, int32_t ___nameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern "C"  void Material_SetInt_m4218872196 (Material_t1926439680 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
extern "C"  void Material_SetIntImpl_m3376002208 (Material_t1926439680 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m54426709 (Material_t1926439680 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColorImpl_m583017531 (Material_t1926439680 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m2604306289 (Material_t1926439680 * __this, int32_t ___nameID0, Matrix4x4_t787966842  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrixImpl_m3459530554 (Material_t1926439680 * __this, int32_t ___nameID0, Matrix4x4_t787966842  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1458093430 (Material_t1926439680 * __this, int32_t ___nameID0, Texture_t954505614 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTextureImpl_m1670116897 (Material_t1926439680 * __this, int32_t ___nameID0, Texture_t954505614 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C"  Texture_t954505614 * Material_GetTexture_m1586524227 (Material_t1926439680 * __this, int32_t ___nameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern "C"  Texture_t954505614 * Material_GetTextureImpl_m1308611253 (Material_t1926439680 * __this, int32_t ___nameID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
extern "C"  void MaterialPropertyBlock_InitBlock_m2591129074 (MaterialPropertyBlock_t3731058050 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
extern "C"  void MaterialPropertyBlock_DestroyBlock_m3884907647 (MaterialPropertyBlock_t3731058050 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m3247465324 (RuntimeObject * __this /* static, unused */, MaterialPropertyBlock_t3731058050 * ___self0, int32_t ___nameID1, Color_t1361298052 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m2921944880 (MaterialPropertyBlock_t3731058050 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColorImpl_m2535405673 (MaterialPropertyBlock_t3731058050 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double,System.Double)
extern "C"  double Math_Log_m2970697177 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3686719278 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3347905369 (RuntimeObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m4089366256 (Matrix4x4_t787966842 * __this, Vector4_t1900979187  ___column00, Vector4_t1900979187  ___column11, Vector4_t1900979187  ___column22, Vector4_t1900979187  ___column33, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m789796904 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___pos0, Quaternion_t3497065016 * ___q1, Vector3_t3932393085 * ___s2, Matrix4x4_t787966842 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m4189649815 (Matrix4x4_t787966842 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m894208093 (Matrix4x4_t787966842 * __this, int32_t ___row0, int32_t ___column1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1436863399 (Matrix4x4_t787966842 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m2958094017 (Matrix4x4_t787966842 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m4093382970 (IndexOutOfRangeException_t3659678378 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t1900979187  Matrix4x4_GetColumn_m1725584070 (Matrix4x4_t787966842 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m3151071673 (Matrix4x4_t787966842 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m308574669 (Vector4_t1900979187 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m4284243969 (Matrix4x4_t787966842 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m1898437273 (Matrix4x4_t787966842 * __this, int32_t ___index0, Vector4_t1900979187  ___column1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Matrix4x4_MultiplyPoint3x4_m800395395 (Matrix4x4_t787966842 * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Matrix4x4::ToString()
extern "C"  String_t* Matrix4x4_ToString_m151622620 (Matrix4x4_t787966842 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m4004593593 (RuntimeObject * __this /* static, unused */, Mesh_t4237566844 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTriangles_m879784383 (Mesh_t4237566844 * __this, List_1_t894813333 * ___triangles0, int32_t ___submesh1, bool ___calculateBounds2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshTriangles_m3405578695 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
extern "C"  RuntimeArray * Mesh_ExtractArrayFromList_m2973470114 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___list0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::SafeLength<System.Int32>(System.Collections.Generic.List`1<T>)
#define Mesh_SafeLength_TisInt32_t3515577538_m12458220(__this, ___values0, method) ((  int32_t (*) (Mesh_t4237566844 *, List_1_t894813333 *, const RuntimeMethod*))Mesh_SafeLength_TisInt32_t3515577538_m12458220_gshared)(__this, ___values0, method)
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTrianglesImpl_m3429015926 (Mesh_t4237566844 * __this, int32_t ___submesh0, RuntimeArray * ___triangles1, int32_t ___arraySize2, bool ___calculateBounds3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m3443663181 (Mesh_t4237566844 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetArrayForChannelImpl_m4170069227 (Mesh_t4237566844 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, RuntimeArray * ___values3, int32_t ___arraySize4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessChannel_m3291228881 (Mesh_t4237566844 * __this, int32_t ___ch0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
#define Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823(__this, ___channel0, method) ((  Vector3U5BU5D_t1432878832* (*) (Mesh_t4237566844 *, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
#define Mesh_GetAllocArrayFromChannel_TisVector4_t1900979187_m1344632441(__this, ___channel0, method) ((  Vector4U5BU5D_t1632896738* (*) (Mesh_t4237566844 *, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisVector4_t1900979187_m1344632441_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439(__this, ___channel0, method) ((  Vector2U5BU5D_t3868613195* (*) (Mesh_t4237566844 *, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_gshared)(__this, ___channel0, method)
// T[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
#define Mesh_GetAllocArrayFromChannel_TisColor32_t662424039_m1356534377(__this, ___channel0, ___format1, ___dim2, method) ((  Color32U5BU5D_t2352998046* (*) (Mesh_t4237566844 *, int32_t, int32_t, int32_t, const RuntimeMethod*))Mesh_GetAllocArrayFromChannel_TisColor32_t662424039_m1356534377_gshared)(__this, ___channel0, ___format1, ___dim2, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
#define Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t4237566844 *, int32_t, List_1_t1311628880 *, const RuntimeMethod*))Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733_gshared)(__this, ___channel0, ___values1, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<T>)
#define Mesh_SetListForChannel_TisVector4_t1900979187_m2034622288(__this, ___channel0, ___values1, method) ((  void (*) (Mesh_t4237566844 *, int32_t, List_1_t3575182278 *, const RuntimeMethod*))Mesh_SetListForChannel_TisVector4_t1900979187_m2034622288_gshared)(__this, ___channel0, ___values1, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<T>)
#define Mesh_SetListForChannel_TisColor32_t662424039_m1488066680(__this, ___channel0, ___format1, ___dim2, ___values3, method) ((  void (*) (Mesh_t4237566844 *, int32_t, int32_t, int32_t, List_1_t2336627130 *, const RuntimeMethod*))Mesh_SetListForChannel_TisColor32_t662424039_m1488066680_gshared)(__this, ___channel0, ___format1, ___dim2, ___values3, method)
// System.Void UnityEngine.Mesh::SetUvsImpl<UnityEngine.Vector2>(System.Int32,System.Int32,System.Collections.Generic.List`1<T>)
#define Mesh_SetUvsImpl_TisVector2_t2246369278_m3260026371(__this, ___uvIndex0, ___dim1, ___uvs2, method) ((  void (*) (Mesh_t4237566844 *, int32_t, int32_t, List_1_t3920572369 *, const RuntimeMethod*))Mesh_SetUvsImpl_TisVector2_t2246369278_m3260026371_gshared)(__this, ___uvIndex0, ___dim1, ___uvs2, method)
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m527277425 (Object_t3645472222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2251207222 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2611403583 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::PrintErrorCantAccessIndices()
extern "C"  void Mesh_PrintErrorCantAccessIndices_m3788685066 (Mesh_t4237566844 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m2145839308 (Mesh_t4237566844 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m2528395896 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t3645472222 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern "C"  bool Mesh_CheckCanAccessSubmesh_m2652095279 (Mesh_t4237566844 * __this, int32_t ___submesh0, bool ___errorAboutTriangles1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshIndices_m1413433297 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
extern "C"  Int32U5BU5D_t3215173111* Mesh_GetIndicesImpl_m2513887997 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
extern "C"  void Mesh_ClearImpl_m264595268 (Mesh_t4237566844 * __this, bool ___keepVertexLayout0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
extern "C"  void Mesh_RecalculateBoundsImpl_m2101905381 (Mesh_t4237566844 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t260305101 * MonoBehaviour_StartCoroutine_Auto_Internal_m927892698 (MonoBehaviour_t4008345588 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m3066659708 (MonoBehaviour_t4008345588 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m552725154 (MonoBehaviour_t4008345588 * __this, Coroutine_t260305101 * ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m2734820921 (NativeClassAttribute_t417698780 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m2224928885 (PlayerEditorConnectionEvents_t1009044977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m2297325353(__this, method) ((  void (*) (List_1_t894813333 *, const RuntimeMethod*))List_1__ctor_m2297325353_gshared)(__this, method)
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m3277333935 (ScriptableObject_t1324617639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3342981539 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___x0, Object_t3645472222 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t3190352615 * PlayerConnection_CreateInstance_m3049947590 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern "C"  RuntimeObject* PlayerConnection_GetConnectionNativeApi_m2366024494 (PlayerConnection_t3190352615 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.ScriptableObject::CreateInstance<UnityEngine.Networking.PlayerConnection.PlayerConnection>()
#define ScriptableObject_CreateInstance_TisPlayerConnection_t3190352615_m3861853114(__this /* static, unused */, method) ((  PlayerConnection_t3190352615 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m3027478402_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2254825582 (Object_t3645472222 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern "C"  void PlayerConnectionInternal__ctor_m2449099525 (PlayerConnectionInternal_t3959366414 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C"  bool Guid_op_Equality_m2100028761 (RuntimeObject * __this /* static, unused */, Guid_t  p0, Guid_t  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413(__this /* static, unused */, p0, method) ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m3816297283_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern "C"  UnityEvent_1_t1459204717 * PlayerEditorConnectionEvents_AddAndCreate_m378817323 (PlayerEditorConnectionEvents_t1009044977 * __this, Guid_t  ___messageId0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m291452717(__this, p0, method) ((  void (*) (UnityEvent_1_t1459204717 *, UnityAction_1_t1148388365 *, const RuntimeMethod*))UnityEvent_1_AddListener_m3455660670_gshared)(__this, p0, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
#define List_1_GetEnumerator_m2611892158(__this, method) ((  Enumerator_t652623003  (*) (List_1_t894813333 *, const RuntimeMethod*))List_1_GetEnumerator_m2611892158_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m3396935530(__this, method) ((  int32_t (*) (Enumerator_t652623003 *, const RuntimeMethod*))Enumerator_get_Current_m3396935530_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
#define UnityAction_1_Invoke_m278533989(__this, p0, method) ((  void (*) (UnityAction_1_t1891607558 *, int32_t, const RuntimeMethod*))UnityAction_1_Invoke_m278533989_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m3554339858(__this, method) ((  bool (*) (Enumerator_t652623003 *, const RuntimeMethod*))Enumerator_MoveNext_m3554339858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m933170505(__this, method) ((  void (*) (Enumerator_t652623003 *, const RuntimeMethod*))Enumerator_Dispose_m933170505_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m2986871176(__this, p0, method) ((  void (*) (UnityEvent_1_t2202423910 *, UnityAction_1_t1891607558 *, const RuntimeMethod*))UnityEvent_1_AddListener_m2986871176_gshared)(__this, p0, method)
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern "C"  void Marshal_Copy_m3981143475 (RuntimeObject * __this /* static, unused */, intptr_t p0, ByteU5BU5D_t3567143369* p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3190352615 * PlayerConnection_get_instance_m2295652940 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m869444109 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4106540626 (PlayerEditorConnectionEvents_t1009044977 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3567143369* ___data1, int32_t ___playerId2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m1589466641(__this, p0, method) ((  void (*) (List_1_t894813333 *, int32_t, const RuntimeMethod*))List_1_Add_m1589466641_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
#define UnityEvent_1_Invoke_m305340876(__this, p0, method) ((  void (*) (UnityEvent_1_t2202423910 *, int32_t, const RuntimeMethod*))UnityEvent_1_Invoke_m305340876_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m4163673745 (MonoPInvokeCallbackAttribute_t3130506276 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m2362397773 (AddComponentMenu_t566310773 * __this, String_t* ___menuName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C"  void AddComponentMenu__ctor_m236626794 (AddComponentMenu_t566310773 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_m_Ordering_1(L_1);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2893751101_marshal_pinvoke(const AnimationCurve_t2893751101& unmarshaled, AnimationCurve_t2893751101_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t2893751101_marshal_pinvoke_back(const AnimationCurve_t2893751101_marshaled_pinvoke& marshaled, AnimationCurve_t2893751101& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2893751101_marshal_pinvoke_cleanup(AnimationCurve_t2893751101_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2893751101_marshal_com(const AnimationCurve_t2893751101& unmarshaled, AnimationCurve_t2893751101_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void AnimationCurve_t2893751101_marshal_com_back(const AnimationCurve_t2893751101_marshaled_com& marshaled, AnimationCurve_t2893751101& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t2893751101_marshal_com_cleanup(AnimationCurve_t2893751101_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2677974774 (AnimationCurve_t2893751101 * __this, KeyframeU5BU5D_t662635263* ___keys0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t662635263* L_0 = ___keys0;
		AnimationCurve_Init_m2273450792(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m2958895521 (AnimationCurve_t2893751101 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m2273450792(__this, (KeyframeU5BU5D_t662635263*)(KeyframeU5BU5D_t662635263*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m2375103925 (AnimationCurve_t2893751101 * __this, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Cleanup_m2375103925_ftn) (AnimationCurve_t2893751101 *);
	static AnimationCurve_Cleanup_m2375103925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m2375103925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m3048692647 (AnimationCurve_t2893751101 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m2375103925(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m2273450792 (AnimationCurve_t2893751101 * __this, KeyframeU5BU5D_t662635263* ___keys0, const RuntimeMethod* method)
{
	typedef void (*AnimationCurve_Init_m2273450792_ftn) (AnimationCurve_t2893751101 *, KeyframeU5BU5D_t662635263*);
	static AnimationCurve_Init_m2273450792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m2273450792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// System.Void UnityEngine.Application::CallLowMemory()
extern "C"  void Application_CallLowMemory_m1111488080 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLowMemory_m1111488080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LowMemoryCallback_t1978334135 * V_0 = NULL;
	{
		LowMemoryCallback_t1978334135 * L_0 = ((Application_t1595095978_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1595095978_il2cpp_TypeInfo_var))->get_lowMemory_0();
		V_0 = L_0;
		LowMemoryCallback_t1978334135 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		LowMemoryCallback_t1978334135 * L_2 = V_0;
		NullCheck(L_2);
		LowMemoryCallback_Invoke_m3832791678(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m2688420401 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Application_get_isPlaying_m2688420401_ftn) ();
	static Application_get_isPlaying_m2688420401_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m2688420401_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m739979537 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Application_get_isEditor_m739979537_ftn) ();
	static Application_get_isEditor_m739979537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m739979537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m2201190236 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Application_get_platform_m2201190236_ftn) ();
	static Application_get_platform_m2201190236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m2201190236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m4249571968 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Application_set_targetFrameRate_m4249571968_ftn) (int32_t);
	static Application_set_targetFrameRate_m4249571968_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m4249571968_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern "C"  void Application_CallLogCallback_m1390027458 (RuntimeObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m1390027458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LogCallback_t2572717340 * V_0 = NULL;
	LogCallback_t2572717340 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		LogCallback_t2572717340 * L_1 = ((Application_t1595095978_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1595095978_il2cpp_TypeInfo_var))->get_s_LogCallbackHandler_1();
		V_0 = L_1;
		LogCallback_t2572717340 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		LogCallback_t2572717340 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m2422533362(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001d:
	{
	}

IL_001e:
	{
		LogCallback_t2572717340 * L_7 = ((Application_t1595095978_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1595095978_il2cpp_TypeInfo_var))->get_s_LogCallbackHandlerThreaded_2();
		V_1 = L_7;
		LogCallback_t2572717340 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		LogCallback_t2572717340 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m2422533362(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern "C"  void Application_InvokeOnBeforeRender_m3241731550 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_InvokeOnBeforeRender_m3241731550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t191347160 * V_0 = NULL;
	{
		UnityAction_t191347160 * L_0 = ((Application_t1595095978_StaticFields*)il2cpp_codegen_static_fields_for(Application_t1595095978_il2cpp_TypeInfo_var))->get_onBeforeRender_3();
		V_0 = L_0;
		UnityAction_t191347160 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UnityAction_t191347160 * L_2 = V_0;
		NullCheck(L_2);
		UnityAction_Invoke_m4106354529(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t2572717340 (LogCallback_t2572717340 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m4090962565 (LogCallback_t2572717340 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m2422533362 (LogCallback_t2572717340 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m2422533362((LogCallback_t2572717340 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LogCallback_BeginInvoke_m2650058531 (LogCallback_t2572717340 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t4283869127 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m2650058531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t3043133256_il2cpp_TypeInfo_var, &___type2);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m1362813540 (LogCallback_t2572717340 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_LowMemoryCallback_t1978334135 (LowMemoryCallback_t1978334135 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LowMemoryCallback__ctor_m396821354 (LowMemoryCallback_t1978334135 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3832791678 (LowMemoryCallback_t1978334135 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LowMemoryCallback_Invoke_m3832791678((LowMemoryCallback_t1978334135 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* LowMemoryCallback_BeginInvoke_m3146912502 (LowMemoryCallback_t1978334135 * __this, AsyncCallback_t4283869127 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LowMemoryCallback_EndInvoke_m2142288848 (LowMemoryCallback_t1978334135 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_pinvoke(const AssetBundleCreateRequest_t2350432906& unmarshaled, AssetBundleCreateRequest_t2350432906_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_pinvoke_back(const AssetBundleCreateRequest_t2350432906_marshaled_pinvoke& marshaled, AssetBundleCreateRequest_t2350432906& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t2350432906_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t1275866152>(marshaled.___m_completeCallback_1, Action_1_t1275866152_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_pinvoke_cleanup(AssetBundleCreateRequest_t2350432906_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_com(const AssetBundleCreateRequest_t2350432906& unmarshaled, AssetBundleCreateRequest_t2350432906_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_com_back(const AssetBundleCreateRequest_t2350432906_marshaled_com& marshaled, AssetBundleCreateRequest_t2350432906& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleCreateRequest_t2350432906_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t1275866152>(marshaled.___m_completeCallback_1, Action_1_t1275866152_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleCreateRequest
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_com_cleanup(AssetBundleCreateRequest_t2350432906_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t3347087835_marshal_pinvoke(const AssetBundleRequest_t3347087835& unmarshaled, AssetBundleRequest_t3347087835_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t3347087835_marshal_pinvoke_back(const AssetBundleRequest_t3347087835_marshaled_pinvoke& marshaled, AssetBundleRequest_t3347087835& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t3347087835_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t1275866152>(marshaled.___m_completeCallback_1, Action_1_t1275866152_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t3347087835_marshal_pinvoke_cleanup(AssetBundleRequest_t3347087835_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t3347087835_marshal_com(const AssetBundleRequest_t3347087835& unmarshaled, AssetBundleRequest_t3347087835_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AssetBundleRequest_t3347087835_marshal_com_back(const AssetBundleRequest_t3347087835_marshaled_com& marshaled, AssetBundleRequest_t3347087835& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundleRequest_t3347087835_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t1275866152>(marshaled.___m_completeCallback_1, Action_1_t1275866152_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t3347087835_marshal_com_cleanup(AssetBundleRequest_t3347087835_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3797817720_marshal_pinvoke(const AsyncOperation_t3797817720& unmarshaled, AsyncOperation_t3797817720_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t3797817720_marshal_pinvoke_back(const AsyncOperation_t3797817720_marshaled_pinvoke& marshaled, AsyncOperation_t3797817720& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t3797817720_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t1275866152>(marshaled.___m_completeCallback_1, Action_1_t1275866152_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3797817720_marshal_pinvoke_cleanup(AsyncOperation_t3797817720_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3797817720_marshal_com(const AsyncOperation_t3797817720& unmarshaled, AsyncOperation_t3797817720_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_completeCallback_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_completeCallback_1()));
}
extern "C" void AsyncOperation_t3797817720_marshal_com_back(const AsyncOperation_t3797817720_marshaled_com& marshaled, AsyncOperation_t3797817720& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_t3797817720_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_completeCallback_1(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_1_t1275866152>(marshaled.___m_completeCallback_1, Action_1_t1275866152_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3797817720_marshal_com_cleanup(AsyncOperation_t3797817720_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m515855777 (AsyncOperation_t3797817720 * __this, const RuntimeMethod* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m515855777_ftn) (AsyncOperation_t3797817720 *);
	static AsyncOperation_InternalDestroy_m515855777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m515855777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m2318158878 (AsyncOperation_t3797817720 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m515855777(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern "C"  void AsyncOperation_InvokeCompletionEvent_m2214301657 (AsyncOperation_t3797817720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperation_InvokeCompletionEvent_m2214301657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1275866152 * L_0 = __this->get_m_completeCallback_1();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Action_1_t1275866152 * L_1 = __this->get_m_completeCallback_1();
		NullCheck(L_1);
		Action_1_Invoke_m2532647139(L_1, __this, /*hidden argument*/Action_1_Invoke_m2532647139_RuntimeMethod_var);
		__this->set_m_completeCallback_1((Action_1_t1275866152 *)NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m230801942 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m230801942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t3041366545 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t3385344125* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	{
		Stack_1_t3041366545 * L_0 = (Stack_1_t3041366545 *)il2cpp_codegen_object_new(Stack_1_t3041366545_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3056069383(L_0, /*hidden argument*/Stack_1__ctor_m3056069383_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_001d;
	}

IL_000c:
	{
		Stack_1_t3041366545 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m1690848609(L_1, L_2, /*hidden argument*/Stack_1_Push_m1690848609_RuntimeMethod_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001d:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t4008345588_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_6) == ((RuntimeObject*)(Type_t *)L_7))))
		{
			goto IL_000c;
		}
	}

IL_0033:
	{
		V_1 = (Type_t *)NULL;
		goto IL_0067;
	}

IL_003a:
	{
		Stack_1_t3041366545 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m2531133666(L_8, /*hidden argument*/Stack_1_Pop_m2531133666_RuntimeMethod_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t4131774042_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t3385344125* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t3385344125*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t3385344125* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_15 = V_1;
		V_4 = L_15;
		goto IL_007b;
	}

IL_0066:
	{
	}

IL_0067:
	{
		Stack_1_t3041366545 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m234060416(L_16, /*hidden argument*/Stack_1_get_Count_m234060416_RuntimeMethod_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		V_4 = (Type_t *)NULL;
		goto IL_007b;
	}

IL_007b:
	{
		Type_t * L_18 = V_4;
		return L_18;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern "C"  TypeU5BU5D_t98989581* AttributeHelperEngine_GetRequiredComponents_m2635372436 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m2635372436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2722562487 * V_0 = NULL;
	RequireComponentU5BU5D_t3043391348* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t3219866953 * V_3 = NULL;
	RequireComponentU5BU5D_t3043391348* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t98989581* V_6 = NULL;
	TypeU5BU5D_t98989581* V_7 = NULL;
	{
		V_0 = (List_1_t2722562487 *)NULL;
		goto IL_00ef;
	}

IL_0008:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t3219866953_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t3385344125* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t3385344125*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t3043391348*)Castclass((RuntimeObject*)L_2, RequireComponentU5BU5D_t3043391348_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t3043391348* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00e0;
	}

IL_0033:
	{
		RequireComponentU5BU5D_t3043391348* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RequireComponent_t3219866953 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t2722562487 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		RequireComponentU5BU5D_t3043391348* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t4008345588_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_12) == ((RuntimeObject*)(Type_t *)L_13))))
		{
			goto IL_0086;
		}
	}
	{
		TypeU5BU5D_t98989581* L_14 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t3219866953 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t98989581* L_17 = L_14;
		RequireComponent_t3219866953 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t98989581* L_20 = L_17;
		RequireComponent_t3219866953 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t98989581* L_23 = V_6;
		V_7 = L_23;
		goto IL_0120;
	}

IL_0086:
	{
		List_1_t2722562487 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t2722562487 * L_25 = (List_1_t2722562487 *)il2cpp_codegen_object_new(List_1_t2722562487_il2cpp_TypeInfo_var);
		List_1__ctor_m946701582(L_25, /*hidden argument*/List_1__ctor_m946701582_RuntimeMethod_var);
		V_0 = L_25;
	}

IL_0093:
	{
		RequireComponent_t3219866953 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t2722562487 * L_28 = V_0;
		RequireComponent_t3219866953 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m3025470644(L_28, L_30, /*hidden argument*/List_1_Add_m3025470644_RuntimeMethod_var);
	}

IL_00aa:
	{
		RequireComponent_t3219866953 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		List_1_t2722562487 * L_33 = V_0;
		RequireComponent_t3219866953 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m3025470644(L_33, L_35, /*hidden argument*/List_1_Add_m3025470644_RuntimeMethod_var);
	}

IL_00c1:
	{
		RequireComponent_t3219866953 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		List_1_t2722562487 * L_38 = V_0;
		RequireComponent_t3219866953 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m3025470644(L_38, L_40, /*hidden argument*/List_1_Add_m3025470644_RuntimeMethod_var);
	}

IL_00d8:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t3043391348* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00ef:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t4008345588_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_46) == ((RuntimeObject*)(Type_t *)L_47))))
		{
			goto IL_0008;
		}
	}

IL_0105:
	{
		List_1_t2722562487 * L_48 = V_0;
		if (L_48)
		{
			goto IL_0113;
		}
	}
	{
		V_7 = (TypeU5BU5D_t98989581*)NULL;
		goto IL_0120;
	}

IL_0113:
	{
		List_1_t2722562487 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t98989581* L_50 = List_1_ToArray_m2051283729(L_49, /*hidden argument*/List_1_ToArray_m2051283729_RuntimeMethod_var);
		V_7 = L_50;
		goto IL_0120;
	}

IL_0120:
	{
		TypeU5BU5D_t98989581* L_51 = V_7;
		return L_51;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m397014713 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m397014713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3385344125* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		goto IL_0033;
	}

IL_0006:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t2915138156_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t3385344125* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t3385344125*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t3385344125* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0050;
	}

IL_002a:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_0033:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t4008345588_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Type_t *)L_8) == ((RuntimeObject*)(Type_t *)L_9))))
		{
			goto IL_0006;
		}
	}

IL_0049:
	{
		V_2 = (bool)0;
		goto IL_0050;
	}

IL_0050:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern "C"  int32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m784596665 (RuntimeObject * __this /* static, unused */, Type_t * ___klass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetDefaultExecutionOrderFor_m784596665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultExecutionOrder_t1108411129 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(AttributeHelperEngine_t2253936689_il2cpp_TypeInfo_var);
		DefaultExecutionOrder_t1108411129 * L_1 = AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t1108411129_m62422214(NULL /*static, unused*/, L_0, /*hidden argument*/AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t1108411129_m62422214_RuntimeMethod_var);
		V_0 = L_1;
		DefaultExecutionOrder_t1108411129 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_0015:
	{
		DefaultExecutionOrder_t1108411129 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = DefaultExecutionOrder_get_order_m3740981065(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern "C"  void AttributeHelperEngine__cctor_m1940743422 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m1940743422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AttributeHelperEngine_t2253936689_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t2253936689_il2cpp_TypeInfo_var))->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t3764267775*)SZArrayNew(DisallowMultipleComponentU5BU5D_t3764267775_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t2253936689_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t2253936689_il2cpp_TypeInfo_var))->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t1903432293*)SZArrayNew(ExecuteInEditModeU5BU5D_t1903432293_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t2253936689_StaticFields*)il2cpp_codegen_static_fields_for(AttributeHelperEngine_t2253936689_il2cpp_TypeInfo_var))->set__requireComponentArray_2(((RequireComponentU5BU5D_t3043391348*)SZArrayNew(RequireComponentU5BU5D_t3043391348_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m1626382119 (Behaviour_t3297694025 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m3044674138(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m1944389595 (Behaviour_t3297694025 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_enabled_m1944389595_ftn) (Behaviour_t3297694025 *);
	static Behaviour_get_enabled_m1944389595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m1944389595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1045693044 (Behaviour_t3297694025 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Behaviour_set_enabled_m1045693044_ftn) (Behaviour_t3297694025 *, bool);
	static Behaviour_set_enabled_m1045693044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m1045693044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m2933447908 (Behaviour_t3297694025 * __this, const RuntimeMethod* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m2933447908_ftn) (Behaviour_t3297694025 *);
	static Behaviour_get_isActiveAndEnabled_m2933447908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m2933447908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Bindings.UnmarshalledAttribute::.ctor()
extern "C"  void UnmarshalledAttribute__ctor_m2887456093 (UnmarshalledAttribute_t1385813216 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m3205218803 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___center0, Vector3_t3932393085  ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds__ctor_m3205218803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3932393085  L_0 = ___center0;
		__this->set_m_Center_0(L_0);
		Vector3_t3932393085  L_1 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_2 = Vector3_op_Multiply_m2900776395(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_2);
		return;
	}
}
extern "C"  void Bounds__ctor_m3205218803_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___center0, Vector3_t3932393085  ___size1, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	Bounds__ctor_m3205218803(_thisAdjusted, ___center0, ___size1, method);
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m1070078697 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Vector3_t3932393085  L_0 = Bounds_get_center_m1014667176(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m2995138410((&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_2 = Bounds_get_extents_m1234217580(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m2995138410((&V_1), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_0032;
	}

IL_0032:
	{
		int32_t L_4 = V_2;
		return L_4;
	}
}
extern "C"  int32_t Bounds_GetHashCode_m1070078697_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_GetHashCode_m1070078697(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m3886448245 (Bounds_t3940617471 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Equals_m3886448245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Bounds_t3940617471  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3932393085  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3932393085  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B5_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Bounds_t3940617471_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0068;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Bounds_t3940617471 *)((Bounds_t3940617471 *)UnBox(L_1, Bounds_t3940617471_il2cpp_TypeInfo_var))));
		Vector3_t3932393085  L_2 = Bounds_get_center_m1014667176(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t3932393085  L_3 = Bounds_get_center_m1014667176((&V_1), /*hidden argument*/NULL);
		Vector3_t3932393085  L_4 = L_3;
		RuntimeObject * L_5 = Box(Vector3_t3932393085_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m1554169531((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t3932393085  L_7 = Bounds_get_extents_m1234217580(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector3_t3932393085  L_8 = Bounds_get_extents_m1234217580((&V_1), /*hidden argument*/NULL);
		Vector3_t3932393085  L_9 = L_8;
		RuntimeObject * L_10 = Box(Vector3_t3932393085_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m1554169531((&V_3), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0062;
	}

IL_0061:
	{
		G_B5_0 = 0;
	}

IL_0062:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0068;
	}

IL_0068:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
extern "C"  bool Bounds_Equals_m3886448245_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_Equals_m3886448245(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t3932393085  Bounds_get_center_m1014667176 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = __this->get_m_Center_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3932393085  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3932393085  Bounds_get_center_m1014667176_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_get_center_m1014667176(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m3304453970 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3932393085  L_0 = ___value0;
		__this->set_m_Center_0(L_0);
		return;
	}
}
extern "C"  void Bounds_set_center_m3304453970_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	Bounds_set_center_m3304453970(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t3932393085  Bounds_get_size_m3922782264 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_size_m3922782264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = __this->get_m_Extents_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_1 = Vector3_op_Multiply_m2900776395(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t3932393085  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector3_t3932393085  Bounds_get_size_m3922782264_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_get_size_m3922782264(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m3828080681 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_set_size_m3828080681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3932393085  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_1 = Vector3_op_Multiply_m2900776395(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_1);
		return;
	}
}
extern "C"  void Bounds_set_size_m3828080681_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	Bounds_set_size_m3828080681(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t3932393085  Bounds_get_extents_m1234217580 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = __this->get_m_Extents_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3932393085  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3932393085  Bounds_get_extents_m1234217580_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_get_extents_m1234217580(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2266073082 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3932393085  L_0 = ___value0;
		__this->set_m_Extents_1(L_0);
		return;
	}
}
extern "C"  void Bounds_set_extents_m2266073082_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	Bounds_set_extents_m2266073082(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t3932393085  Bounds_get_min_m2956001499 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_min_m2956001499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = Bounds_get_center_m1014667176(__this, /*hidden argument*/NULL);
		Vector3_t3932393085  L_1 = Bounds_get_extents_m1234217580(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_2 = Vector3_op_Subtraction_m2511661424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t3932393085  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t3932393085  Bounds_get_min_m2956001499_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_get_min_m2956001499(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t3932393085  Bounds_get_max_m1445821865 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_get_max_m1445821865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = Bounds_get_center_m1014667176(__this, /*hidden argument*/NULL);
		Vector3_t3932393085  L_1 = Bounds_get_extents_m1234217580(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_2 = Vector3_op_Addition_m2513759451(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t3932393085  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t3932393085  Bounds_get_max_m1445821865_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_get_max_m1445821865(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m4088733844 (RuntimeObject * __this /* static, unused */, Bounds_t3940617471  ___lhs0, Bounds_t3940617471  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_op_Equality_m4088733844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Vector3_t3932393085  L_0 = Bounds_get_center_m1014667176((&___lhs0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_1 = Bounds_get_center_m1014667176((&___rhs1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		bool L_2 = Vector3_op_Equality_m2587075067(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Vector3_t3932393085  L_3 = Bounds_get_extents_m1234217580((&___lhs0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_4 = Bounds_get_extents_m1234217580((&___rhs1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		bool L_5 = Vector3_op_Equality_m2587075067(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m4191932201 (RuntimeObject * __this /* static, unused */, Bounds_t3940617471  ___lhs0, Bounds_t3940617471  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Bounds_t3940617471  L_0 = ___lhs0;
		Bounds_t3940617471  L_1 = ___rhs1;
		bool L_2 = Bounds_op_Equality_m4088733844(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1614096005 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___min0, Vector3_t3932393085  ___max1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_SetMinMax_m1614096005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3932393085  L_0 = ___max1;
		Vector3_t3932393085  L_1 = ___min0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_2 = Vector3_op_Subtraction_m2511661424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t3932393085  L_3 = Vector3_op_Multiply_m2900776395(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m2266073082(__this, L_3, /*hidden argument*/NULL);
		Vector3_t3932393085  L_4 = ___min0;
		Vector3_t3932393085  L_5 = Bounds_get_extents_m1234217580(__this, /*hidden argument*/NULL);
		Vector3_t3932393085  L_6 = Vector3_op_Addition_m2513759451(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m3304453970(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_SetMinMax_m1614096005_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___min0, Vector3_t3932393085  ___max1, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	Bounds_SetMinMax_m1614096005(_thisAdjusted, ___min0, ___max1, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3006561347 (Bounds_t3940617471 * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Encapsulate_m3006561347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3932393085  L_0 = Bounds_get_min_m2956001499(__this, /*hidden argument*/NULL);
		Vector3_t3932393085  L_1 = ___point0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_2 = Vector3_Min_m1113176117(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t3932393085  L_3 = Bounds_get_max_m1445821865(__this, /*hidden argument*/NULL);
		Vector3_t3932393085  L_4 = ___point0;
		Vector3_t3932393085  L_5 = Vector3_Max_m197737547(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m1614096005(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m3006561347_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	Bounds_Encapsulate_m3006561347(_thisAdjusted, ___point0, method);
}
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m3206324941 (Bounds_t3940617471 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_ToString_m3206324941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t3932393085  L_1 = __this->get_m_Center_0();
		Vector3_t3932393085  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t3932393085_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		Vector3_t3932393085  L_5 = __this->get_m_Extents_1();
		Vector3_t3932393085  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t3932393085_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral2570740258, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Bounds_ToString_m3206324941_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Bounds_t3940617471 * _thisAdjusted = reinterpret_cast<Bounds_t3940617471 *>(__this + 1);
	return Bounds_ToString_m3206324941(_thisAdjusted, method);
}
// System.Void UnityEngine.Camera::.ctor()
extern "C"  void Camera__ctor_m4228244505 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m1626382119(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m4120344687 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_nearClipPlane_m4120344687_ftn) (Camera_t362346687 *);
	static Camera_get_nearClipPlane_m4120344687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m4120344687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m1473945392 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_farClipPlane_m1473945392_ftn) (Camera_t362346687 *);
	static Camera_get_farClipPlane_m1473945392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m1473945392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Camera::get_depth()
extern "C"  float Camera_get_depth_m1330802308 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef float (*Camera_get_depth_m1330802308_ftn) (Camera_t362346687 *);
	static Camera_get_depth_m1330802308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m1330802308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m4086893319 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_cullingMask_m4086893319_ftn) (Camera_t362346687 *);
	static Camera_get_cullingMask_m4086893319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m4086893319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m2361608952 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_eventMask_m2361608952_ftn) (Camera_t362346687 *);
	static Camera_get_eventMask_m2361608952_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m2361608952_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t952252086  Camera_get_pixelRect_m3824066811 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t952252086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_pixelRect_m479895925(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t952252086  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t952252086  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m479895925 (Camera_t362346687 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m479895925_ftn) (Camera_t362346687 *, Rect_t952252086 *);
	static Camera_INTERNAL_get_pixelRect_m479895925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m479895925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t3737750657 * Camera_get_targetTexture_m3060139219 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef RenderTexture_t3737750657 * (*Camera_get_targetTexture_m3060139219_ftn) (Camera_t362346687 *);
	static Camera_get_targetTexture_m3060139219_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m3060139219_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	RenderTexture_t3737750657 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m440103708 (Camera_t362346687 * __this, Matrix4x4_t787966842  ___value0, const RuntimeMethod* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m428635793(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m428635793 (Camera_t362346687 * __this, Matrix4x4_t787966842 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m428635793_ftn) (Camera_t362346687 *, Matrix4x4_t787966842 *);
	static Camera_INTERNAL_set_projectionMatrix_m428635793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m428635793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m1043993872 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_clearFlags_m1043993872_ftn) (Camera_t362346687 *);
	static Camera_get_clearFlags_m1043993872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m1043993872_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_targetDisplay()
extern "C"  int32_t Camera_get_targetDisplay_m1889234090 (Camera_t362346687 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_targetDisplay_m1889234090_ftn) (Camera_t362346687 *);
	static Camera_get_targetDisplay_m1889234090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetDisplay_m1889234090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetDisplay()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Camera_ScreenToWorldPoint_m823138372 (Camera_t362346687 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToWorldPoint_m139610015(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m139610015 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m139610015_ftn) (Camera_t362346687 *, Vector3_t3932393085 *, Vector3_t3932393085 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m139610015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m139610015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Camera_ScreenToViewportPoint_m2111382939 (Camera_t362346687 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToViewportPoint_m503392759(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m503392759 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m503392759_ftn) (Camera_t362346687 *, Vector3_t3932393085 *, Vector3_t3932393085 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m503392759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m503392759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t1807385980  Camera_ScreenPointToRay_m3701064168 (Camera_t362346687 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method)
{
	Ray_t1807385980  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t1807385980  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m12666471(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t1807385980  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Ray_t1807385980  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m12666471 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Vector3_t3932393085 * ___position1, Ray_t1807385980 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m12666471_ftn) (Camera_t362346687 *, Vector3_t3932393085 *, Ray_t1807385980 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m12666471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m12666471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t362346687 * Camera_get_main_m2332403648 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Camera_t362346687 * (*Camera_get_main_m2332403648_ftn) ();
	static Camera_get_main_m2332403648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m2332403648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	Camera_t362346687 * retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m190271839 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m190271839_ftn) ();
	static Camera_get_allCamerasCount_m190271839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m190271839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m635401728 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t365668710* ___cameras0, const RuntimeMethod* method)
{
	typedef int32_t (*Camera_GetAllCameras_m635401728_ftn) (CameraU5BU5D_t365668710*);
	static Camera_GetAllCameras_m635401728_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m635401728_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	int32_t retVal = _il2cpp_icall_func(___cameras0);
	return retVal;
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreCull_m72709191 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m72709191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t3349755816 * L_0 = ((Camera_t362346687_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t362346687_il2cpp_TypeInfo_var))->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t3349755816 * L_1 = ((Camera_t362346687_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t362346687_il2cpp_TypeInfo_var))->get_onPreCull_2();
		Camera_t362346687 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3225907431(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreRender_m4106667072 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m4106667072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t3349755816 * L_0 = ((Camera_t362346687_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t362346687_il2cpp_TypeInfo_var))->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t3349755816 * L_1 = ((Camera_t362346687_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t362346687_il2cpp_TypeInfo_var))->get_onPreRender_3();
		Camera_t362346687 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3225907431(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPostRender_m4047589566 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m4047589566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t3349755816 * L_0 = ((Camera_t362346687_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t362346687_il2cpp_TypeInfo_var))->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t3349755816 * L_1 = ((Camera_t362346687_StaticFields*)il2cpp_codegen_static_fields_for(Camera_t362346687_il2cpp_TypeInfo_var))->get_onPostRender_4();
		Camera_t362346687 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3225907431(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m3250154905 (Camera_t362346687 * __this, int32_t ___evt0, CommandBuffer_t3383400165 * ___buffer1, const RuntimeMethod* method)
{
	typedef void (*Camera_AddCommandBuffer_m3250154905_ftn) (Camera_t362346687 *, int32_t, CommandBuffer_t3383400165 *);
	static Camera_AddCommandBuffer_m3250154905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_AddCommandBuffer_m3250154905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m1605224397 (Camera_t362346687 * __this, int32_t ___evt0, CommandBuffer_t3383400165 * ___buffer1, const RuntimeMethod* method)
{
	typedef void (*Camera_RemoveCommandBuffer_m1605224397_ftn) (Camera_t362346687 *, int32_t, CommandBuffer_t3383400165 *);
	static Camera_RemoveCommandBuffer_m1605224397_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_RemoveCommandBuffer_m1605224397_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_RaycastTry_m3889099052 (Camera_t362346687 * __this, Ray_t1807385980  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t2881801184 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t2881801184 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m473860371(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t2881801184 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_INTERNAL_CALL_RaycastTry_m473860371 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Ray_t1807385980 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t2881801184 * (*Camera_INTERNAL_CALL_RaycastTry_m473860371_ftn) (Camera_t362346687 *, Ray_t1807385980 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m473860371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m473860371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t2881801184 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_RaycastTry2D_m3772444891 (Camera_t362346687 * __this, Ray_t1807385980  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	GameObject_t2881801184 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t2881801184 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m1075685860(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t2881801184 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_INTERNAL_CALL_RaycastTry2D_m1075685860 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___self0, Ray_t1807385980 * ___ray1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef GameObject_t2881801184 * (*Camera_INTERNAL_CALL_RaycastTry2D_m1075685860_ftn) (Camera_t362346687 *, Ray_t1807385980 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m1075685860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m1075685860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	GameObject_t2881801184 * retVal = _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
	return retVal;
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m67294258 (CameraCallback_t3349755816 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m3225907431 (CameraCallback_t3349755816 * __this, Camera_t362346687 * ___cam0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m3225907431((CameraCallback_t3349755816 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Camera_t362346687 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t362346687 * ___cam0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CameraCallback_BeginInvoke_m3250084619 (CameraCallback_t3349755816 * __this, Camera_t362346687 * ___cam0, AsyncCallback_t4283869127 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m82598808 (CameraCallback_t3349755816 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.ClassLibraryInitializer::Init()
extern "C"  void ClassLibraryInitializer_Init_m1860967955 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		UnityLogWriter_Init_m1685913522(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3386337842 (Color_t1361298052 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m3386337842_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	Color_t1361298052 * _thisAdjusted = reinterpret_cast<Color_t1361298052 *>(__this + 1);
	Color__ctor_m3386337842(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3950769051 (Color_t1361298052 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		__this->set_a_3((1.0f));
		return;
	}
}
extern "C"  void Color__ctor_m3950769051_AdjustorThunk (RuntimeObject * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method)
{
	Color_t1361298052 * _thisAdjusted = reinterpret_cast<Color_t1361298052 *>(__this + 1);
	Color__ctor_m3950769051(_thisAdjusted, ___r0, ___g1, ___b2, method);
}
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m961366009 (Color_t1361298052 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m961366009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3385344125* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral1036966564, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color_ToString_m961366009_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t1361298052 * _thisAdjusted = reinterpret_cast<Color_t1361298052 *>(__this + 1);
	return Color_ToString_m961366009(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m762054233 (Color_t1361298052 * __this, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector4_t1900979187  L_0 = Color_op_Implicit_m1778966118(NULL /*static, unused*/, (*(Color_t1361298052 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m2425648894((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0020;
	}

IL_0020:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t Color_GetHashCode_m762054233_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color_t1361298052 * _thisAdjusted = reinterpret_cast<Color_t1361298052 *>(__this + 1);
	return Color_GetHashCode_m762054233(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m249927524 (Color_t1361298052 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m249927524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Color_t1361298052  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Color_t1361298052_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Color_t1361298052 *)((Color_t1361298052 *)UnBox(L_1, Color_t1361298052_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_1)->get_r_0();
		bool L_4 = Single_Equals_m847285714(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_1)->get_g_1();
		bool L_7 = Single_Equals_m847285714(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_1)->get_b_2();
		bool L_10 = Single_Equals_m847285714(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_1)->get_a_3();
		bool L_13 = Single_Equals_m847285714(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Color_Equals_m249927524_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Color_t1361298052 * _thisAdjusted = reinterpret_cast<Color_t1361298052 *>(__this + 1);
	return Color_Equals_m249927524(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t1361298052  Color_op_Multiply_m3945062513 (RuntimeObject * __this /* static, unused */, Color_t1361298052  ___a0, float ___b1, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = ___b1;
		float L_2 = (&___a0)->get_g_1();
		float L_3 = ___b1;
		float L_4 = (&___a0)->get_b_2();
		float L_5 = ___b1;
		float L_6 = (&___a0)->get_a_3();
		float L_7 = ___b1;
		Color_t1361298052  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m3386337842((&L_8), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Color_t1361298052  L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m2552885420 (RuntimeObject * __this /* static, unused */, Color_t1361298052  ___lhs0, Color_t1361298052  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_op_Equality_m2552885420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Color_t1361298052  L_0 = ___lhs0;
		Vector4_t1900979187  L_1 = Color_op_Implicit_m1778966118(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t1361298052  L_2 = ___rhs1;
		Vector4_t1900979187  L_3 = Color_op_Implicit_m1778966118(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t1900979187_il2cpp_TypeInfo_var);
		bool L_4 = Vector4_op_Equality_m1785435548(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t1361298052  Color_Lerp_m1950920091 (RuntimeObject * __this /* static, unused */, Color_t1361298052  ___a0, Color_t1361298052  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Lerp_m1950920091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_r_0();
		float L_3 = (&___b1)->get_r_0();
		float L_4 = (&___a0)->get_r_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_g_1();
		float L_7 = (&___b1)->get_g_1();
		float L_8 = (&___a0)->get_g_1();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_b_2();
		float L_11 = (&___b1)->get_b_2();
		float L_12 = (&___a0)->get_b_2();
		float L_13 = ___t2;
		float L_14 = (&___a0)->get_a_3();
		float L_15 = (&___b1)->get_a_3();
		float L_16 = (&___a0)->get_a_3();
		float L_17 = ___t2;
		Color_t1361298052  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m3386337842((&L_18), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_0078;
	}

IL_0078:
	{
		Color_t1361298052  L_19 = V_0;
		return L_19;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t1361298052  Color_get_white_m1321996523 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t1361298052  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3386337842((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t1361298052  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t1361298052  Color_get_black_m2828138583 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t1361298052  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3386337842((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t1361298052  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t1361298052  Color_get_clear_m1190250505 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t1361298052  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3386337842((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t1361298052  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t1900979187  Color_op_Implicit_m1778966118 (RuntimeObject * __this /* static, unused */, Color_t1361298052  ___c0, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t1900979187  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m898810465((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		Vector4_t1900979187  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m2963160534 (Color32_t662424039 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___r0;
		__this->set_r_0(L_0);
		uint8_t L_1 = ___g1;
		__this->set_g_1(L_1);
		uint8_t L_2 = ___b2;
		__this->set_b_2(L_2);
		uint8_t L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color32__ctor_m2963160534_AdjustorThunk (RuntimeObject * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method)
{
	Color32_t662424039 * _thisAdjusted = reinterpret_cast<Color32_t662424039 *>(__this + 1);
	Color32__ctor_m2963160534(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t662424039  Color32_op_Implicit_m1747497104 (RuntimeObject * __this /* static, unused */, Color_t1361298052  ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_op_Implicit_m1747497104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t662424039  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___c0)->get_g_1();
		float L_3 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___c0)->get_b_2();
		float L_5 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (&___c0)->get_a_3();
		float L_7 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t662424039  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color32__ctor_m2963160534((&L_8), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0058;
	}

IL_0058:
	{
		Color32_t662424039  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t1361298052  Color32_op_Implicit_m1032808107 (RuntimeObject * __this /* static, unused */, Color32_t662424039  ___c0, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		uint8_t L_0 = (&___c0)->get_r_0();
		uint8_t L_1 = (&___c0)->get_g_1();
		uint8_t L_2 = (&___c0)->get_b_2();
		uint8_t L_3 = (&___c0)->get_a_3();
		Color_t1361298052  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m3386337842((&L_4), ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0044;
	}

IL_0044:
	{
		Color_t1361298052  L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m649264193 (Color32_t662424039 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_ToString_m649264193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_1 = __this->get_r_0();
		uint8_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Byte_t2640180184_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		uint8_t L_5 = __this->get_g_1();
		uint8_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Byte_t2640180184_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		uint8_t L_9 = __this->get_b_2();
		uint8_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Byte_t2640180184_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3385344125* L_12 = L_8;
		uint8_t L_13 = __this->get_a_3();
		uint8_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Byte_t2640180184_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral2277880093, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color32_ToString_m649264193_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Color32_t662424039 * _thisAdjusted = reinterpret_cast<Color32_t662424039 *>(__this + 1);
	return Color32_ToString_m649264193(_thisAdjusted, method);
}
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3044674138 (Component_t2335505321 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m3044674138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3580444445 * Component_get_transform_m1425942785 (Component_t2335505321 * __this, const RuntimeMethod* method)
{
	typedef Transform_t3580444445 * (*Component_get_transform_m1425942785_ftn) (Component_t2335505321 *);
	static Component_get_transform_m1425942785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m1425942785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	Transform_t3580444445 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2881801184 * Component_get_gameObject_m3138408676 (Component_t2335505321 * __this, const RuntimeMethod* method)
{
	typedef GameObject_t2881801184 * (*Component_get_gameObject_m3138408676_ftn) (Component_t2335505321 *);
	static Component_get_gameObject_m3138408676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m3138408676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	GameObject_t2881801184 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t2335505321 * Component_GetComponent_m2148776168 (Component_t2335505321 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	Component_t2335505321 * V_0 = NULL;
	{
		GameObject_t2881801184 * L_0 = Component_get_gameObject_m3138408676(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t2335505321 * L_2 = GameObject_GetComponent_m1768480708(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t2335505321 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m2645822270 (Component_t2335505321 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentFastPath_m2645822270_ftn) (Component_t2335505321 *, Type_t *, intptr_t);
	static Component_GetComponentFastPath_m2645822270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m2645822270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t2335505321 * Component_GetComponentInChildren_m4112821146 (Component_t2335505321 * __this, Type_t * ___t0, bool ___includeInactive1, const RuntimeMethod* method)
{
	Component_t2335505321 * V_0 = NULL;
	{
		GameObject_t2881801184 * L_0 = Component_get_gameObject_m3138408676(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t2335505321 * L_3 = GameObject_GetComponentInChildren_m58285979(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Component_t2335505321 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t2335505321 * Component_GetComponentInParent_m1249747129 (Component_t2335505321 * __this, Type_t * ___t0, const RuntimeMethod* method)
{
	Component_t2335505321 * V_0 = NULL;
	{
		GameObject_t2881801184 * L_0 = Component_get_gameObject_m3138408676(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t2335505321 * L_2 = GameObject_GetComponentInParent_m390353812(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t2335505321 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m2424663091 (Component_t2335505321 * __this, Type_t * ___searchType0, RuntimeObject * ___resultList1, const RuntimeMethod* method)
{
	typedef void (*Component_GetComponentsForListInternal_m2424663091_ftn) (Component_t2335505321 *, Type_t *, RuntimeObject *);
	static Component_GetComponentsForListInternal_m2424663091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m2424663091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m708268600 (Component_t2335505321 * __this, Type_t * ___type0, List_1_t4009708412 * ___results1, const RuntimeMethod* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t4009708412 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m2424663091(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t260305101_marshal_pinvoke(const Coroutine_t260305101& unmarshaled, Coroutine_t260305101_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t260305101_marshal_pinvoke_back(const Coroutine_t260305101_marshaled_pinvoke& marshaled, Coroutine_t260305101& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t260305101_marshal_pinvoke_cleanup(Coroutine_t260305101_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t260305101_marshal_com(const Coroutine_t260305101& unmarshaled, Coroutine_t260305101_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Coroutine_t260305101_marshal_com_back(const Coroutine_t260305101_marshaled_com& marshaled, Coroutine_t260305101& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t260305101_marshal_com_cleanup(Coroutine_t260305101_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m1557358023 (Coroutine_t260305101 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m2841548285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m1956794736 (Coroutine_t260305101 * __this, const RuntimeMethod* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m1956794736_ftn) (Coroutine_t260305101 *);
	static Coroutine_ReleaseCoroutine_m1956794736_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m1956794736_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m3776365656 (Coroutine_t260305101 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m1956794736(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
extern "C"  CSSSize_t83932994  DelegatePInvokeWrapper_CSSMeasureFunc_t2649428469 (CSSMeasureFunc_t2649428469 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	typedef CSSSize_t83932994  (STDCALL *PInvokeFunc)(intptr_t, float, int32_t, float, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	CSSSize_t83932994  returnValue = il2cppPInvokeFunc(___node0, ___width1, ___widthMode2, ___height3, ___heightMode4);

	return returnValue;
}
// System.Void UnityEngine.CSSLayout.CSSMeasureFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void CSSMeasureFunc__ctor_m317151031 (CSSMeasureFunc_t2649428469 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::Invoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode)
extern "C"  CSSSize_t83932994  CSSMeasureFunc_Invoke_m2931059520 (CSSMeasureFunc_t2649428469 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CSSMeasureFunc_Invoke_m2931059520((CSSMeasureFunc_t2649428469 *)__this->get_prev_9(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef CSSSize_t83932994  (*FunctionPointerType) (RuntimeObject *, void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef CSSSize_t83932994  (*FunctionPointerType) (void* __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___node0, ___width1, ___widthMode2, ___height3, ___heightMode4,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CSSLayout.CSSMeasureFunc::BeginInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CSSMeasureFunc_BeginInvoke_m183478383 (CSSMeasureFunc_t2649428469 * __this, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, AsyncCallback_t4283869127 * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CSSMeasureFunc_BeginInvoke_m183478383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___node0);
	__d_args[1] = Box(Single_t897798503_il2cpp_TypeInfo_var, &___width1);
	__d_args[2] = Box(CSSMeasureMode_t50254874_il2cpp_TypeInfo_var, &___widthMode2);
	__d_args[3] = Box(Single_t897798503_il2cpp_TypeInfo_var, &___height3);
	__d_args[4] = Box(CSSMeasureMode_t50254874_il2cpp_TypeInfo_var, &___heightMode4);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// UnityEngine.CSSLayout.CSSSize UnityEngine.CSSLayout.CSSMeasureFunc::EndInvoke(System.IAsyncResult)
extern "C"  CSSSize_t83932994  CSSMeasureFunc_EndInvoke_m1816537768 (CSSMeasureFunc_t2649428469 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(CSSSize_t83932994 *)UnBox ((RuntimeObject*)__result);
}
// UnityEngine.CSSLayout.CSSMeasureFunc UnityEngine.CSSLayout.Native::CSSNodeGetMeasureFunc(System.IntPtr)
extern "C"  CSSMeasureFunc_t2649428469 * Native_CSSNodeGetMeasureFunc_m485971160 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeGetMeasureFunc_m485971160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WeakReference_t1433439652 * V_0 = NULL;
	CSSMeasureFunc_t2649428469 * V_1 = NULL;
	{
		V_0 = (WeakReference_t1433439652 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t1351913248_il2cpp_TypeInfo_var);
		Dictionary_2_t1470255310 * L_0 = ((Native_t1351913248_StaticFields*)il2cpp_codegen_static_fields_for(Native_t1351913248_il2cpp_TypeInfo_var))->get_s_MeasureFunctions_0();
		intptr_t L_1 = ___node0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m2231896620(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m2231896620_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t1433439652 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.WeakReference::get_IsAlive() */, L_3);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		WeakReference_t1433439652 * L_5 = V_0;
		NullCheck(L_5);
		RuntimeObject * L_6 = VirtFuncInvoker0< RuntimeObject * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, L_5);
		V_1 = ((CSSMeasureFunc_t2649428469 *)IsInstSealed((RuntimeObject*)L_6, CSSMeasureFunc_t2649428469_il2cpp_TypeInfo_var));
		goto IL_003a;
	}

IL_0032:
	{
		V_1 = (CSSMeasureFunc_t2649428469 *)NULL;
		goto IL_003a;
	}

IL_003a:
	{
		CSSMeasureFunc_t2649428469 * L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.CSSLayout.Native::CSSNodeMeasureInvoke(System.IntPtr,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.Single,UnityEngine.CSSLayout.CSSMeasureMode,System.IntPtr)
extern "C"  void Native_CSSNodeMeasureInvoke_m3401257635 (RuntimeObject * __this /* static, unused */, intptr_t ___node0, float ___width1, int32_t ___widthMode2, float ___height3, int32_t ___heightMode4, intptr_t ___returnValueAddress5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native_CSSNodeMeasureInvoke_m3401257635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CSSMeasureFunc_t2649428469 * V_0 = NULL;
	{
		intptr_t L_0 = ___node0;
		IL2CPP_RUNTIME_CLASS_INIT(Native_t1351913248_il2cpp_TypeInfo_var);
		CSSMeasureFunc_t2649428469 * L_1 = Native_CSSNodeGetMeasureFunc_m485971160(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CSSMeasureFunc_t2649428469 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		intptr_t L_3 = ___returnValueAddress5;
		void* L_4 = IntPtr_op_Explicit_m259387555(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		CSSMeasureFunc_t2649428469 * L_5 = V_0;
		intptr_t L_6 = ___node0;
		float L_7 = ___width1;
		int32_t L_8 = ___widthMode2;
		float L_9 = ___height3;
		int32_t L_10 = ___heightMode4;
		NullCheck(L_5);
		CSSSize_t83932994  L_11 = CSSMeasureFunc_Invoke_m2931059520(L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		*(CSSSize_t83932994 *)L_4 = L_11;
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.CSSLayout.Native::.cctor()
extern "C"  void Native__cctor_m3423934896 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Native__cctor_m3423934896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1470255310 * L_0 = (Dictionary_2_t1470255310 *)il2cpp_codegen_object_new(Dictionary_2_t1470255310_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2012856830(L_0, /*hidden argument*/Dictionary_2__ctor_m2012856830_RuntimeMethod_var);
		((Native_t1351913248_StaticFields*)il2cpp_codegen_static_fields_for(Native_t1351913248_il2cpp_TypeInfo_var))->set_s_MeasureFunctions_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t637827745_marshal_pinvoke(const CullingGroup_t637827745& unmarshaled, CullingGroup_t637827745_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t637827745_marshal_pinvoke_back(const CullingGroup_t637827745_marshaled_pinvoke& marshaled, CullingGroup_t637827745& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t637827745_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t1464331389>(marshaled.___m_OnStateChanged_1, StateChanged_t1464331389_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t637827745_marshal_pinvoke_cleanup(CullingGroup_t637827745_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t637827745_marshal_com(const CullingGroup_t637827745& unmarshaled, CullingGroup_t637827745_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t637827745_marshal_com_back(const CullingGroup_t637827745_marshaled_com& marshaled, CullingGroup_t637827745& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t637827745_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t1464331389>(marshaled.___m_OnStateChanged_1, StateChanged_t1464331389_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t637827745_marshal_com_cleanup(CullingGroup_t637827745_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern "C"  void CullingGroup_Finalize_m1014941570 (CullingGroup_t637827745 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m1014941570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			intptr_t L_0 = __this->get_m_Ptr_0();
			bool L_1 = IntPtr_op_Inequality_m183498676(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_001e;
			}
		}

IL_0016:
		{
			CullingGroup_FinalizerFailure_m3638411044(__this, /*hidden argument*/NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m2792278093 (CullingGroup_t637827745 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_Dispose_m2792278093_ftn) (CullingGroup_t637827745 *);
	static CullingGroup_Dispose_m2792278093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m2792278093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m4083933383 (RuntimeObject * __this /* static, unused */, CullingGroup_t637827745 * ___cullingGroup0, intptr_t ___eventsPtr1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_SendEvents_m4083933383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CullingGroupEvent_t505730933 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m1034235824((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t505730933 *)L_0;
		CullingGroup_t637827745 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t1464331389 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0046;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0020:
	{
		CullingGroup_t637827745 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t1464331389 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t505730933 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = il2cpp_codegen_sizeof(CullingGroupEvent_t505730933_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		StateChanged_Invoke_m688567370(L_4, (*(CullingGroupEvent_t505730933 *)((CullingGroupEvent_t505730933 *)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m3638411044 (CullingGroup_t637827745 * __this, const RuntimeMethod* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m3638411044_ftn) (CullingGroup_t637827745 *);
	static CullingGroup_FinalizerFailure_m3638411044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m3638411044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t1464331389 (StateChanged_t1464331389 * __this, CullingGroupEvent_t505730933  ___sphere0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t505730933 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___sphere0);

}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m2714859245 (StateChanged_t1464331389 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m688567370 (StateChanged_t1464331389 * __this, CullingGroupEvent_t505730933  ___sphere0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m688567370((StateChanged_t1464331389 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, CullingGroupEvent_t505730933  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t505730933  ___sphere0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sphere0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* StateChanged_BeginInvoke_m653102028 (StateChanged_t1464331389 * __this, CullingGroupEvent_t505730933  ___sphere0, AsyncCallback_t4283869127 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m653102028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t505730933_il2cpp_TypeInfo_var, &___sphere0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m3780546076 (StateChanged_t1464331389 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m1010319172 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Cursor_get_lockState_m1010319172_ftn) ();
	static Cursor_get_lockState_m1010319172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_lockState_m1010319172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_lockState()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m4123350112 (CustomYieldInstruction_t2091205422 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern "C"  RuntimeObject * CustomYieldInstruction_get_Current_m2488953613 (CustomYieldInstruction_t2091205422 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		V_0 = NULL;
		goto IL_0008;
	}

IL_0008:
	{
		RuntimeObject * L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern "C"  bool CustomYieldInstruction_MoveNext_m4192021646 (CustomYieldInstruction_t2091205422 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, __this);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.CustomYieldInstruction::Reset()
extern "C"  void CustomYieldInstruction_Reset_m3928489937 (CustomYieldInstruction_t2091205422 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern "C"  RuntimeObject* Debug_get_unityLogger_m866078178 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_unityLogger_m866078178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((Debug_t959145111_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t959145111_il2cpp_TypeInfo_var))->get_s_Logger_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m3687530845 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m3687530845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1005929009_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2611403583 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m2611403583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1005929009_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m2528395896 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t3645472222 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m2528395896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		Object_t3645472222 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, RuntimeObject *, Object_t3645472222 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1005929009_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogErrorFormat_m1494465668 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___context0, String_t* ___format1, ObjectU5BU5D_t3385344125* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogErrorFormat_m1494465668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3645472222 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t3385344125* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t3645472222 *, String_t*, ObjectU5BU5D_t3385344125* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m1375071567 (RuntimeObject * __this /* static, unused */, Exception_t214279536 * ___exception0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m1375071567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t214279536 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t214279536 *, Object_t3645472222 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_0, L_1, (Object_t3645472222 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Debug_LogException_m3647006827 (RuntimeObject * __this /* static, unused */, Exception_t214279536 * ___exception0, Object_t3645472222 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m3647006827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t214279536 * L_1 = ___exception0;
		Object_t3645472222 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t214279536 *, Object_t3645472222 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m4161798195 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m4161798195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, RuntimeObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1005929009_il2cpp_TypeInfo_var, L_0, 2, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m3675514459 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t3645472222 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m3675514459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___message0;
		Object_t3645472222 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, RuntimeObject *, Object_t3645472222 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1005929009_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m3546591144 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___context0, String_t* ___format1, ObjectU5BU5D_t3385344125* ___args2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarningFormat_m3546591144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_m866078178(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3645472222 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t3385344125* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t3645472222 *, String_t*, ObjectU5BU5D_t3385344125* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern "C"  void Debug__cctor_m649192451 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m649192451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebugLogHandler_t2540765360 * L_0 = (DebugLogHandler_t2540765360 *)il2cpp_codegen_object_new(DebugLogHandler_t2540765360_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m165201388(L_0, /*hidden argument*/NULL);
		Logger_t4035514390 * L_1 = (Logger_t4035514390 *)il2cpp_codegen_object_new(Logger_t4035514390_il2cpp_TypeInfo_var);
		Logger__ctor_m998766840(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t959145111_StaticFields*)il2cpp_codegen_static_fields_for(Debug_t959145111_il2cpp_TypeInfo_var))->set_s_Logger_0(L_1);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m165201388 (DebugLogHandler_t2540765360 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m3491589508 (RuntimeObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t3645472222 * ___obj2, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m3491589508_ftn) (int32_t, String_t*, Object_t3645472222 *);
	static DebugLogHandler_Internal_Log_m3491589508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m3491589508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m102761498 (RuntimeObject * __this /* static, unused */, Exception_t214279536 * ___exception0, Object_t3645472222 * ___obj1, const RuntimeMethod* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m102761498_ftn) (Exception_t214279536 *, Object_t3645472222 *);
	static DebugLogHandler_Internal_LogException_m102761498_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m102761498_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void DebugLogHandler_LogFormat_m1693934444 (DebugLogHandler_t2540765360 * __this, int32_t ___logType0, Object_t3645472222 * ___context1, String_t* ___format2, ObjectU5BU5D_t3385344125* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m1693934444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t3385344125* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m3139124310(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t3645472222 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m3491589508(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m209496829 (DebugLogHandler_t2540765360 * __this, Exception_t214279536 * ___exception0, Object_t3645472222 * ___context1, const RuntimeMethod* method)
{
	{
		Exception_t214279536 * L_0 = ___exception0;
		Object_t3645472222 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m102761498(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m3740981065 (DefaultExecutionOrder_t1108411129 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CorderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C"  void DisallowMultipleComponent__ctor_m980831504 (DisallowMultipleComponent_t4131774042 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m1198470498 (Display_t81008966 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		intptr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m1737770000((&L_0), 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m1727525657 (Display_t81008966 * __this, intptr_t ___nativeDisplay0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		intptr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern "C"  int32_t Display_get_renderingWidth_m4193687592 (Display_t81008966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingWidth_m4193687592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m2661054513(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern "C"  int32_t Display_get_renderingHeight_m767707977 (Display_t81008966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingHeight_m767707977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m2661054513(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern "C"  int32_t Display_get_systemWidth_m3725717722 (Display_t81008966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemWidth_m3725717722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m1135348170(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern "C"  int32_t Display_get_systemHeight_m1986252539 (Display_t81008966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemHeight_m1986252539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		intptr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m1135348170(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Display_RelativeMouseAt_m1286882000 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___inputMouseCoordinates0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RelativeMouseAt_m1286882000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t3932393085  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = (&___inputMouseCoordinates0)->get_x_1();
		V_3 = (((int32_t)((int32_t)L_0)));
		float L_1 = (&___inputMouseCoordinates0)->get_y_2();
		V_4 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m571773009(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->set_z_3((((float)((float)L_4))));
		int32_t L_5 = V_1;
		(&V_0)->set_x_1((((float)((float)L_5))));
		int32_t L_6 = V_2;
		(&V_0)->set_y_2((((float)((float)L_6))));
		Vector3_t3932393085  L_7 = V_0;
		V_5 = L_7;
		goto IL_0046;
	}

IL_0046:
	{
		Vector3_t3932393085  L_8 = V_5;
		return L_8;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern "C"  void Display_RecreateDisplayList_m3667820612 (RuntimeObject * __this /* static, unused */, IntPtrU5BU5D_t3930959911* ___nativeDisplay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m3667820612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t3930959911* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->set_displays_1(((DisplayU5BU5D_t2949376739*)SZArrayNew(DisplayU5BU5D_t2949376739_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t2949376739* L_1 = ((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t3930959911* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		intptr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t81008966 * L_7 = (Display_t81008966 *)il2cpp_codegen_object_new(Display_t81008966_il2cpp_TypeInfo_var);
		Display__ctor_m1727525657(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t81008966 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t3930959911* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t2949376739* L_11 = ((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Display_t81008966 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern "C"  void Display_FireDisplaysUpdated_m1036156850 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m1036156850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1656811884 * L_0 = ((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t81008966_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t1656811884 * L_1 = ((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m1549941880(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m1135348170 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	typedef void (*Display_GetSystemExtImpl_m1135348170_ftn) (intptr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m1135348170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m1135348170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m2661054513 (RuntimeObject * __this /* static, unused */, intptr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const RuntimeMethod* method)
{
	typedef void (*Display_GetRenderingExtImpl_m2661054513_ftn) (intptr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m2661054513_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m2661054513_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m571773009 (RuntimeObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const RuntimeMethod* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m571773009_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m571773009_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m571773009_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	int32_t retVal = _il2cpp_icall_func(___x0, ___y1, ___rx2, ___ry3);
	return retVal;
}
// System.Void UnityEngine.Display::.cctor()
extern "C"  void Display__cctor_m1902176577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m1902176577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisplayU5BU5D_t2949376739* L_0 = ((DisplayU5BU5D_t2949376739*)SZArrayNew(DisplayU5BU5D_t2949376739_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t81008966 * L_1 = (Display_t81008966 *)il2cpp_codegen_object_new(Display_t81008966_il2cpp_TypeInfo_var);
		Display__ctor_m1198470498(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t81008966 *)L_1);
		((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->set_displays_1(L_0);
		DisplayU5BU5D_t2949376739* L_2 = ((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->get_displays_1();
		NullCheck(L_2);
		int32_t L_3 = 0;
		Display_t81008966 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->set__mainDisplay_2(L_4);
		((Display_t81008966_StaticFields*)il2cpp_codegen_static_fields_for(Display_t81008966_il2cpp_TypeInfo_var))->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t1656811884 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1656811884 (DisplaysUpdatedDelegate_t1656811884 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m883062485 (DisplaysUpdatedDelegate_t1656811884 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m1549941880 (DisplaysUpdatedDelegate_t1656811884 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m1549941880((DisplaysUpdatedDelegate_t1656811884 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* DisplaysUpdatedDelegate_BeginInvoke_m1979503819 (DisplaysUpdatedDelegate_t1656811884 * __this, AsyncCallback_t4283869127 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m3820543321 (DisplaysUpdatedDelegate_t1656811884 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m1543013735 (DrivenRectTransformTracker_t110585296 * __this, Object_t3645472222 * ___driver0, RectTransform_t1161610249 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Add_m1543013735_AdjustorThunk (RuntimeObject * __this, Object_t3645472222 * ___driver0, RectTransform_t1161610249 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t110585296 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t110585296 *>(__this + 1);
	DrivenRectTransformTracker_Add_m1543013735(_thisAdjusted, ___driver0, ___rectTransform1, ___drivenProperties2, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m156265900 (DrivenRectTransformTracker_t110585296 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		DrivenRectTransformTracker_Clear_m465650438(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m156265900_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t110585296 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t110585296 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m156265900(_thisAdjusted, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear(System.Boolean)
extern "C"  void DrivenRectTransformTracker_Clear_m465650438 (DrivenRectTransformTracker_t110585296 * __this, bool ___revertValues0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m465650438_AdjustorThunk (RuntimeObject * __this, bool ___revertValues0, const RuntimeMethod* method)
{
	DrivenRectTransformTracker_t110585296 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t110585296 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m465650438(_thisAdjusted, ___revertValues0, method);
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m4246442324 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t3645472222 * ArgumentCache_get_unityObjectArgument_m1264291904 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	Object_t3645472222 * V_0 = NULL;
	{
		Object_t3645472222 * L_0 = __this->get_m_ObjectArgument_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t3645472222 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2699605163 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m2390490498 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m1788594777 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatArgument_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m3913918529 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m2685124831 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m464151535 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m464151535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2536109821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_00e4;
	}

IL_0016:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m3835433624(L_2, _stringLiteral4204795500, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m2069972063(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003c:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m3835433624(L_8, _stringLiteral4083982080, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m2069972063(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_005c:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m3835433624(L_14, _stringLiteral2925840332, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m2069972063(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_007c:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_009a;
		}
	}
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m1162620119(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
	}

IL_009a:
	{
		String_t* L_24 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_24);
		int32_t L_25 = String_IndexOf_m3835433624(L_24, _stringLiteral2761045697, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = V_1;
		if ((((int32_t)L_26) == ((int32_t)(-1))))
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_27 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_27);
		bool L_28 = String_EndsWith_m2337007599(L_27, _stringLiteral1699466734, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e4;
		}
	}
	{
		String_t* L_29 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		String_t* L_31 = String_Substring_m1162620119(L_29, 0, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2806582524(NULL /*static, unused*/, L_31, _stringLiteral126451750, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_32);
	}

IL_00e4:
	{
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m4109818181 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m464151535(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m300100220 (ArgumentCache_t537934761 * __this, const RuntimeMethod* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m464151535(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m2962492325 (BaseInvokableCall_t3648497698 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2255002992 (BaseInvokableCall_t3648497698 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m2255002992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t246139103 * L_1 = (ArgumentNullException_t246139103 *)il2cpp_codegen_object_new(ArgumentNullException_t246139103_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2488792999(L_1, _stringLiteral4055933771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t246139103 * L_3 = (ArgumentNullException_t246139103 *)il2cpp_codegen_object_new(ArgumentNullException_t246139103_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2488792999(L_3, _stringLiteral1338428238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m3930864202 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * ___delegate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m3930864202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Object_t3645472222 * V_2 = NULL;
	{
		Delegate_t4015201940 * L_0 = ___delegate0;
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m1225911449(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0015:
	{
		RuntimeObject * L_3 = V_0;
		V_2 = ((Object_t3645472222 *)IsInstClass((RuntimeObject*)L_3, Object_t3645472222_il2cpp_TypeInfo_var));
		Object_t3645472222 * L_4 = V_2;
		bool L_5 = Object_ReferenceEquals_m898979290(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Object_t3645472222 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_6, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m631237306 (InvokableCall_t1976387117 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m631237306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m2255002992(__this, L_0, L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(UnityAction_t191347160_0_0_0_var), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t4015201940 * L_5 = NetFxCoreExtensions_CreateDelegate_m3762475387(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		InvokableCall_add_Delegate_m660321946(__this, ((UnityAction_t191347160 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t191347160_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m334505956 (InvokableCall_t1976387117 * __this, UnityAction_t191347160 * ___action0, const RuntimeMethod* method)
{
	{
		BaseInvokableCall__ctor_m2962492325(__this, /*hidden argument*/NULL);
		UnityAction_t191347160 * L_0 = ___action0;
		InvokableCall_add_Delegate_m660321946(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m660321946 (InvokableCall_t1976387117 * __this, UnityAction_t191347160 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_add_Delegate_m660321946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t191347160 * V_0 = NULL;
	UnityAction_t191347160 * V_1 = NULL;
	{
		UnityAction_t191347160 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t191347160 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t191347160 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t191347160 * L_3 = V_1;
		UnityAction_t191347160 * L_4 = ___value0;
		Delegate_t4015201940 * L_5 = Delegate_Combine_m4046213137(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t191347160 * L_6 = V_0;
		UnityAction_t191347160 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t191347160 *>(L_2, ((UnityAction_t191347160 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t191347160_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t191347160 * L_8 = V_0;
		UnityAction_t191347160 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t191347160 *)L_8) == ((RuntimeObject*)(UnityAction_t191347160 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_remove_Delegate_m4267960666 (InvokableCall_t1976387117 * __this, UnityAction_t191347160 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_remove_Delegate_m4267960666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t191347160 * V_0 = NULL;
	UnityAction_t191347160 * V_1 = NULL;
	{
		UnityAction_t191347160 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t191347160 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t191347160 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t191347160 * L_3 = V_1;
		UnityAction_t191347160 * L_4 = ___value0;
		Delegate_t4015201940 * L_5 = Delegate_Remove_m897454397(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t191347160 * L_6 = V_0;
		UnityAction_t191347160 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t191347160 *>(L_2, ((UnityAction_t191347160 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t191347160_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t191347160 * L_8 = V_0;
		UnityAction_t191347160 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t191347160 *)L_8) == ((RuntimeObject*)(UnityAction_t191347160 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m3911892756 (InvokableCall_t1976387117 * __this, ObjectU5BU5D_t3385344125* ___args0, const RuntimeMethod* method)
{
	{
		UnityAction_t191347160 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m3930864202(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t191347160 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m4106354529(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke()
extern "C"  void InvokableCall_Invoke_m1537249999 (InvokableCall_t1976387117 * __this, const RuntimeMethod* method)
{
	{
		UnityAction_t191347160 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m3930864202(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t191347160 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m4106354529(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_Find_m2571718888 (InvokableCall_t1976387117 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_t191347160 * L_0 = __this->get_Delegate_0();
		NullCheck(L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m1225911449(L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_t191347160 * L_3 = __this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3556049827(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m3925837574 (InvokableCallList_t3172597217 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m3925837574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1027733493 * L_0 = (List_1_t1027733493 *)il2cpp_codegen_object_new(List_1_t1027733493_il2cpp_TypeInfo_var);
		List_1__ctor_m2996853262(L_0, /*hidden argument*/List_1__ctor_m2996853262_RuntimeMethod_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t1027733493 * L_1 = (List_1_t1027733493 *)il2cpp_codegen_object_new(List_1_t1027733493_il2cpp_TypeInfo_var);
		List_1__ctor_m2996853262(L_1, /*hidden argument*/List_1__ctor_m2996853262_RuntimeMethod_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t1027733493 * L_2 = (List_1_t1027733493 *)il2cpp_codegen_object_new(List_1_t1027733493_il2cpp_TypeInfo_var);
		List_1__ctor_m2996853262(L_2, /*hidden argument*/List_1__ctor_m2996853262_RuntimeMethod_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m678893588 (InvokableCallList_t3172597217 * __this, BaseInvokableCall_t3648497698 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m678893588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1027733493 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t3648497698 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m116104406(L_0, L_1, /*hidden argument*/List_1_Add_m116104406_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m3921073616 (InvokableCallList_t3172597217 * __this, BaseInvokableCall_t3648497698 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddListener_m3921073616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1027733493 * L_0 = __this->get_m_RuntimeCalls_1();
		BaseInvokableCall_t3648497698 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m116104406(L_0, L_1, /*hidden argument*/List_1_Add_m116104406_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m3353336194 (InvokableCallList_t3172597217 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_RemoveListener_m3353336194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1027733493 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t1027733493 * L_0 = (List_1_t1027733493 *)il2cpp_codegen_object_new(List_1_t1027733493_il2cpp_TypeInfo_var);
		List_1__ctor_m2996853262(L_0, /*hidden argument*/List_1__ctor_m2996853262_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003e;
	}

IL_000e:
	{
		List_1_t1027733493 * L_1 = __this->get_m_RuntimeCalls_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t3648497698 * L_3 = List_1_get_Item_m4078513680(L_1, L_2, /*hidden argument*/List_1_get_Item_m4078513680_RuntimeMethod_var);
		RuntimeObject * L_4 = ___targetObj0;
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_3);
		bool L_6 = VirtFuncInvoker2< bool, RuntimeObject *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		List_1_t1027733493 * L_7 = V_0;
		List_1_t1027733493 * L_8 = __this->get_m_RuntimeCalls_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t3648497698 * L_10 = List_1_get_Item_m4078513680(L_8, L_9, /*hidden argument*/List_1_get_Item_m4078513680_RuntimeMethod_var);
		NullCheck(L_7);
		List_1_Add_m116104406(L_7, L_10, /*hidden argument*/List_1_Add_m116104406_RuntimeMethod_var);
	}

IL_0039:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_12 = V_1;
		List_1_t1027733493 * L_13 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m1195210665(L_13, /*hidden argument*/List_1_get_Count_m1195210665_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t1027733493 * L_15 = __this->get_m_RuntimeCalls_1();
		List_1_t1027733493 * L_16 = V_0;
		intptr_t L_17 = (intptr_t)List_1_Contains_m1890478580_RuntimeMethod_var;
		Predicate_1_t959054467 * L_18 = (Predicate_1_t959054467 *)il2cpp_codegen_object_new(Predicate_1_t959054467_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m106168190(L_18, L_16, L_17, /*hidden argument*/Predicate_1__ctor_m106168190_RuntimeMethod_var);
		NullCheck(L_15);
		List_1_RemoveAll_m601647338(L_15, L_18, /*hidden argument*/List_1_RemoveAll_m601647338_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m2913537599 (InvokableCallList_t3172597217 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m2913537599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1027733493 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m3693544616(L_0, /*hidden argument*/List_1_Clear_m3693544616_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern "C"  List_1_t1027733493 * InvokableCallList_PrepareInvoke_m965587067 (InvokableCallList_t3172597217 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_PrepareInvoke_m965587067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1027733493 * V_0 = NULL;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t1027733493 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m3693544616(L_1, /*hidden argument*/List_1_Clear_m3693544616_RuntimeMethod_var);
		List_1_t1027733493 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t1027733493 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m55576690(L_2, L_3, /*hidden argument*/List_1_AddRange_m55576690_RuntimeMethod_var);
		List_1_t1027733493 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t1027733493 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m55576690(L_4, L_5, /*hidden argument*/List_1_AddRange_m55576690_RuntimeMethod_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_0042:
	{
		List_1_t1027733493 * L_6 = __this->get_m_ExecutingCalls_2();
		V_0 = L_6;
		goto IL_004e;
	}

IL_004e:
	{
		List_1_t1027733493 * L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern "C"  void PersistentCall__ctor_m3715022066 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m3715022066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Mode_2(0);
		ArgumentCache_t537934761 * L_0 = (ArgumentCache_t537934761 *)il2cpp_codegen_object_new(ArgumentCache_t537934761_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m4246442324(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t3645472222 * PersistentCall_get_target_m1664545602 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method)
{
	Object_t3645472222 * V_0 = NULL;
	{
		Object_t3645472222 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t3645472222 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m3175548779 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m3633496894 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Mode_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t537934761 * PersistentCall_get_arguments_m1403296607 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method)
{
	ArgumentCache_t537934761 * V_0 = NULL;
	{
		ArgumentCache_t537934761 * L_0 = __this->get_m_Arguments_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ArgumentCache_t537934761 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m2393902497 (PersistentCall_t1968425864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m2393902497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Object_t3645472222 * L_0 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m3175548779(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2536109821(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t3648497698 * PersistentCall_GetRuntimeCall_m2894046812 (PersistentCall_t1968425864 * __this, UnityEventBase_t2081553920 * ___theEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m2894046812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t3648497698 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		UnityEventBase_t2081553920 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0019;
		}
	}

IL_0012:
	{
		V_0 = (BaseInvokableCall_t3648497698 *)NULL;
		goto IL_0114;
	}

IL_0019:
	{
		UnityEventBase_t2081553920 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m3743863011(L_2, __this, /*hidden argument*/NULL);
		V_1 = L_3;
		MethodInfo_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = (BaseInvokableCall_t3648497698 *)NULL;
		goto IL_0114;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_2 = L_5;
		int32_t L_6 = V_2;
		switch (L_6)
		{
			case 0:
			{
				goto IL_005c;
			}
			case 1:
			{
				goto IL_00fb;
			}
			case 2:
			{
				goto IL_006f;
			}
			case 3:
			{
				goto IL_00a4;
			}
			case 4:
			{
				goto IL_0087;
			}
			case 5:
			{
				goto IL_00c1;
			}
			case 6:
			{
				goto IL_00de;
			}
		}
	}
	{
		goto IL_010d;
	}

IL_005c:
	{
		UnityEventBase_t2081553920 * L_7 = ___theEvent0;
		Object_t3645472222 * L_8 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_1;
		NullCheck(L_7);
		BaseInvokableCall_t3648497698 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t3648497698 *, RuntimeObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		V_0 = L_10;
		goto IL_0114;
	}

IL_006f:
	{
		Object_t3645472222 * L_11 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_1;
		ArgumentCache_t537934761 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t3648497698 * L_14 = PersistentCall_GetObjectCall_m155688820(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0114;
	}

IL_0087:
	{
		Object_t3645472222 * L_15 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_1;
		ArgumentCache_t537934761 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m1788594777(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t404005347 * L_19 = (CachedInvokableCall_1_t404005347 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t404005347_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2394803162(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m2394803162_RuntimeMethod_var);
		V_0 = L_19;
		goto IL_0114;
	}

IL_00a4:
	{
		Object_t3645472222 * L_20 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_1;
		ArgumentCache_t537934761 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m2390490498(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3021784382 * L_24 = (CachedInvokableCall_1_t3021784382 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3021784382_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m1010281405(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m1010281405_RuntimeMethod_var);
		V_0 = L_24;
		goto IL_0114;
	}

IL_00c1:
	{
		Object_t3645472222 * L_25 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_1;
		ArgumentCache_t537934761 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m3913918529(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t2765728243 * L_29 = (CachedInvokableCall_1_t2765728243 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t2765728243_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m163013044(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m163013044_RuntimeMethod_var);
		V_0 = L_29;
		goto IL_0114;
	}

IL_00de:
	{
		Object_t3645472222 * L_30 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_1;
		ArgumentCache_t537934761 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m2685124831(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t1946426838 * L_34 = (CachedInvokableCall_1_t1946426838 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t1946426838_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m4230261306(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m4230261306_RuntimeMethod_var);
		V_0 = L_34;
		goto IL_0114;
	}

IL_00fb:
	{
		Object_t3645472222 * L_35 = PersistentCall_get_target_m1664545602(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_1;
		InvokableCall_t1976387117 * L_37 = (InvokableCall_t1976387117 *)il2cpp_codegen_object_new(InvokableCall_t1976387117_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m631237306(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		goto IL_0114;
	}

IL_010d:
	{
		V_0 = (BaseInvokableCall_t3648497698 *)NULL;
		goto IL_0114;
	}

IL_0114:
	{
		BaseInvokableCall_t3648497698 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t3648497698 * PersistentCall_GetObjectCall_m155688820 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t537934761 * ___arguments2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m155688820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t3011499035 * V_3 = NULL;
	Object_t3645472222 * V_4 = NULL;
	BaseInvokableCall_t3648497698 * V_5 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Object_t3645472222_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t537934761 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2699605163(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2536109821(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentCache_t537934761 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2699605163(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m1907918864, L_5, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Object_t3645472222_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0039:
	{
		V_0 = G_B3_0;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t1723064271_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t98989581* L_11 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t98989581* L_15 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Object_t3645472222_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t98989581* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t98989581* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t3011499035 * L_21 = Type_GetConstructor_m4289629996(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t537934761 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t3645472222 * L_23 = ArgumentCache_get_unityObjectArgument_m1264291904(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t3645472222 * L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_24, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ab;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t3645472222 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m392475457(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00ab;
		}
	}
	{
		V_4 = (Object_t3645472222 *)NULL;
	}

IL_00ab:
	{
		ConstructorInfo_t3011499035 * L_30 = V_3;
		ObjectU5BU5D_t3385344125* L_31 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t3645472222 * L_32 = ___target0;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_32);
		ObjectU5BU5D_t3385344125* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_34);
		ObjectU5BU5D_t3385344125* L_35 = L_33;
		Object_t3645472222 * L_36 = V_4;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_36);
		NullCheck(L_30);
		RuntimeObject * L_37 = ConstructorInfo_Invoke_m3356180212(L_30, L_35, /*hidden argument*/NULL);
		V_5 = ((BaseInvokableCall_t3648497698 *)IsInstClass((RuntimeObject*)L_37, BaseInvokableCall_t3648497698_il2cpp_TypeInfo_var));
		goto IL_00d0;
	}

IL_00d0:
	{
		BaseInvokableCall_t3648497698 * L_38 = V_5;
		return L_38;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m158630386 (PersistentCallGroup_t3408587689 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m158630386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		List_1_t3642628955 * L_0 = (List_1_t3642628955 *)il2cpp_codegen_object_new(List_1_t3642628955_il2cpp_TypeInfo_var);
		List_1__ctor_m1383490399(L_0, /*hidden argument*/List_1__ctor_m1383490399_RuntimeMethod_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m1336092061 (PersistentCallGroup_t3408587689 * __this, InvokableCallList_t3172597217 * ___invokableList0, UnityEventBase_t2081553920 * ___unityEventBase1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m1336092061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersistentCall_t1968425864 * V_0 = NULL;
	Enumerator_t3400438625  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t3648497698 * V_2 = NULL;
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3642628955 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t3400438625  L_1 = List_1_GetEnumerator_m4154154611(L_0, /*hidden argument*/List_1_GetEnumerator_m4154154611_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0013:
		{
			PersistentCall_t1968425864 * L_2 = Enumerator_get_Current_m2770969459((&V_1), /*hidden argument*/Enumerator_get_Current_m2770969459_RuntimeMethod_var);
			V_0 = L_2;
			PersistentCall_t1968425864 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m2393902497(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			goto IL_0042;
		}

IL_002c:
		{
			PersistentCall_t1968425864 * L_5 = V_0;
			UnityEventBase_t2081553920 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t3648497698 * L_7 = PersistentCall_GetRuntimeCall_m2894046812(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t3648497698 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			InvokableCallList_t3172597217 * L_9 = ___invokableList0;
			BaseInvokableCall_t3648497698 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m678893588(L_9, L_10, /*hidden argument*/NULL);
		}

IL_0041:
		{
		}

IL_0042:
		{
			bool L_11 = Enumerator_MoveNext_m1658440964((&V_1), /*hidden argument*/Enumerator_MoveNext_m1658440964_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0013;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2859623709((&V_1), /*hidden argument*/Enumerator_Dispose_m2859623709_RuntimeMethod_var);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0061:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t191347160 (UnityAction_t191347160 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2049701706 (UnityAction_t191347160 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m4106354529 (UnityAction_t191347160 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m4106354529((UnityAction_t191347160 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_BeginInvoke_m335023972 (UnityAction_t191347160 * __this, AsyncCallback_t4283869127 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m1395328725 (UnityAction_t191347160 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern "C"  void UnityEvent__ctor_m603194707 (UnityEvent_t4085066839 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_InvokeArray_4((ObjectU5BU5D_t3385344125*)NULL);
		UnityEventBase__ctor_m1267297027(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m601952475 (UnityEvent_t4085066839 * __this, UnityAction_t191347160 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_t191347160 * L_0 = ___call0;
		BaseInvokableCall_t3648497698 * L_1 = UnityEvent_GetDelegate_m3591116204(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m3895696039(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m1609998361 (UnityEvent_t4085066839 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m1609998361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		MethodInfo_t * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t3648497698 * UnityEvent_GetDelegate_m3904563982 (UnityEvent_t4085066839 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m3904563982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t3648497698 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t1976387117 * L_2 = (InvokableCall_t1976387117 *)il2cpp_codegen_object_new(InvokableCall_t1976387117_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m631237306(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t3648497698 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t3648497698 * UnityEvent_GetDelegate_m3591116204 (RuntimeObject * __this /* static, unused */, UnityAction_t191347160 * ___action0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m3591116204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t3648497698 * V_0 = NULL;
	{
		UnityAction_t191347160 * L_0 = ___action0;
		InvokableCall_t1976387117 * L_1 = (InvokableCall_t1976387117 *)il2cpp_codegen_object_new(InvokableCall_t1976387117_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m334505956(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t3648497698 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m1855632032 (UnityEvent_t4085066839 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_Invoke_m1855632032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1027733493 * V_0 = NULL;
	int32_t V_1 = 0;
	InvokableCall_t1976387117 * V_2 = NULL;
	BaseInvokableCall_t3648497698 * V_3 = NULL;
	{
		List_1_t1027733493 * L_0 = UnityEventBase_PrepareInvoke_m1597816360(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0060;
	}

IL_000f:
	{
		List_1_t1027733493 * L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t3648497698 * L_3 = List_1_get_Item_m4078513680(L_1, L_2, /*hidden argument*/List_1_get_Item_m4078513680_RuntimeMethod_var);
		V_2 = ((InvokableCall_t1976387117 *)IsInstClass((RuntimeObject*)L_3, InvokableCall_t1976387117_il2cpp_TypeInfo_var));
		InvokableCall_t1976387117 * L_4 = V_2;
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		InvokableCall_t1976387117 * L_5 = V_2;
		NullCheck(L_5);
		InvokableCall_Invoke_m1537249999(L_5, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_002e:
	{
		List_1_t1027733493 * L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		BaseInvokableCall_t3648497698 * L_8 = List_1_get_Item_m4078513680(L_6, L_7, /*hidden argument*/List_1_get_Item_m4078513680_RuntimeMethod_var);
		V_3 = L_8;
		ObjectU5BU5D_t3385344125* L_9 = __this->get_m_InvokeArray_4();
		if (L_9)
		{
			goto IL_004e;
		}
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)0)));
	}

IL_004e:
	{
		BaseInvokableCall_t3648497698 * L_10 = V_3;
		ObjectU5BU5D_t3385344125* L_11 = __this->get_m_InvokeArray_4();
		NullCheck(L_10);
		VirtActionInvoker1< ObjectU5BU5D_t3385344125* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_10, L_11);
	}

IL_005b:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_13 = V_1;
		List_1_t1027733493 * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m1195210665(L_14, /*hidden argument*/List_1_get_Count_m1195210665_RuntimeMethod_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1267297027 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase__ctor_m1267297027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_CallsDirty_3((bool)1);
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		InvokableCallList_t3172597217 * L_0 = (InvokableCallList_t3172597217 *)il2cpp_codegen_object_new(InvokableCallList_t3172597217_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m3925837574(L_0, /*hidden argument*/NULL);
		__this->set_m_Calls_0(L_0);
		PersistentCallGroup_t3408587689 * L_1 = (PersistentCallGroup_t3408587689 *)il2cpp_codegen_object_new(PersistentCallGroup_t3408587689_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m158630386(L_1, /*hidden argument*/NULL);
		__this->set_m_PersistentCalls_1(L_1);
		Type_t * L_2 = Object_GetType_m392475457(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->set_m_TypeName_2(L_3);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1927417081 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m791522482 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m1757731517(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m392475457(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_m_TypeName_2(L_1);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m3743863011 (UnityEventBase_t2081553920 * __this, PersistentCall_t1968425864 * ___call0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m3743863011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Object_t3645472222_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t1968425864 * L_1 = ___call0;
		NullCheck(L_1);
		ArgumentCache_t537934761 * L_2 = PersistentCall_get_arguments_m1403296607(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2699605163(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2536109821(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		PersistentCall_t1968425864 * L_5 = ___call0;
		NullCheck(L_5);
		ArgumentCache_t537934761 * L_6 = PersistentCall_get_arguments_m1403296607(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m2699605163(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m1907918864, L_7, (bool)0, "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Object_t3645472222_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0043:
	{
		V_0 = G_B3_0;
	}

IL_0044:
	{
		PersistentCall_t1968425864 * L_11 = ___call0;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m3175548779(L_11, /*hidden argument*/NULL);
		PersistentCall_t1968425864 * L_13 = ___call0;
		NullCheck(L_13);
		Object_t3645472222 * L_14 = PersistentCall_get_target_m1664545602(L_13, /*hidden argument*/NULL);
		PersistentCall_t1968425864 * L_15 = ___call0;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m3633496894(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m2734136314(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		goto IL_0063;
	}

IL_0063:
	{
		MethodInfo_t * L_19 = V_1;
		return L_19;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m2734136314 (UnityEventBase_t2081553920 * __this, String_t* ___name0, RuntimeObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m2734136314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t98989581* G_B10_2 = NULL;
	TypeU5BU5D_t98989581* G_B10_3 = NULL;
	String_t* G_B10_4 = NULL;
	RuntimeObject * G_B10_5 = NULL;
	Type_t * G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t98989581* G_B9_2 = NULL;
	TypeU5BU5D_t98989581* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	RuntimeObject * G_B9_5 = NULL;
	{
		int32_t L_0 = ___mode2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_00c9;
			}
			case 3:
			{
				goto IL_0069;
			}
			case 4:
			{
				goto IL_0049;
			}
			case 5:
			{
				goto IL_00a9;
			}
			case 6:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_00f2;
	}

IL_0028:
	{
		String_t* L_1 = ___name0;
		RuntimeObject * L_2 = ___listener1;
		MethodInfo_t * L_3 = VirtFuncInvoker2< MethodInfo_t *, String_t*, RuntimeObject * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_1, L_2);
		V_0 = L_3;
		goto IL_00f9;
	}

IL_0036:
	{
		RuntimeObject * L_4 = ___listener1;
		String_t* L_5 = ___name0;
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, L_4, L_5, ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_00f9;
	}

IL_0049:
	{
		RuntimeObject * L_7 = ___listener1;
		String_t* L_8 = ___name0;
		TypeU5BU5D_t98989581* L_9 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Single_t897798503_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		MethodInfo_t * L_11 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_00f9;
	}

IL_0069:
	{
		RuntimeObject * L_12 = ___listener1;
		String_t* L_13 = ___name0;
		TypeU5BU5D_t98989581* L_14 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Int32_t3515577538_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_15);
		MethodInfo_t * L_16 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00f9;
	}

IL_0089:
	{
		RuntimeObject * L_17 = ___listener1;
		String_t* L_18 = ___name0;
		TypeU5BU5D_t98989581* L_19 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Boolean_t2440219994_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_20);
		MethodInfo_t * L_21 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00f9;
	}

IL_00a9:
	{
		RuntimeObject * L_22 = ___listener1;
		String_t* L_23 = ___name0;
		TypeU5BU5D_t98989581* L_24 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_25);
		MethodInfo_t * L_26 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00f9;
	}

IL_00c9:
	{
		RuntimeObject * L_27 = ___listener1;
		String_t* L_28 = ___name0;
		TypeU5BU5D_t98989581* L_29 = ((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_30 = ___argumentType3;
		Type_t * L_31 = L_30;
		G_B9_0 = L_31;
		G_B9_1 = 0;
		G_B9_2 = L_29;
		G_B9_3 = L_29;
		G_B9_4 = L_28;
		G_B9_5 = L_27;
		if (L_31)
		{
			G_B10_0 = L_31;
			G_B10_1 = 0;
			G_B10_2 = L_29;
			G_B10_3 = L_29;
			G_B10_4 = L_28;
			G_B10_5 = L_27;
			goto IL_00e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(Object_t3645472222_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_32;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00e6:
	{
		NullCheck(G_B10_2);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (Type_t *)G_B10_0);
		MethodInfo_t * L_33 = UnityEventBase_GetValidMethodInfo_m1984273667(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00f9;
	}

IL_00f2:
	{
		V_0 = (MethodInfo_t *)NULL;
		goto IL_00f9;
	}

IL_00f9:
	{
		MethodInfo_t * L_34 = V_0;
		return L_34;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m1757731517 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	{
		InvokableCallList_t3172597217 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m2913537599(L_0, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m4077085260 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_CallsDirty_3();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		PersistentCallGroup_t3408587689 * L_1 = __this->get_m_PersistentCalls_1();
		InvokableCallList_t3172597217 * L_2 = __this->get_m_Calls_0();
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m1336092061(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)0);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3895696039 (UnityEventBase_t2081553920 * __this, BaseInvokableCall_t3648497698 * ___call0, const RuntimeMethod* method)
{
	{
		InvokableCallList_t3172597217 * L_0 = __this->get_m_Calls_0();
		BaseInvokableCall_t3648497698 * L_1 = ___call0;
		NullCheck(L_0);
		InvokableCallList_AddListener_m3921073616(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m379147049 (UnityEventBase_t2081553920 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	{
		InvokableCallList_t3172597217 * L_0 = __this->get_m_Calls_0();
		RuntimeObject * L_1 = ___targetObj0;
		MethodInfo_t * L_2 = ___method1;
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m3353336194(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern "C"  List_1_t1027733493 * UnityEventBase_PrepareInvoke_m1597816360 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	List_1_t1027733493 * V_0 = NULL;
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m4077085260(__this, /*hidden argument*/NULL);
		InvokableCallList_t3172597217 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		List_1_t1027733493 * L_1 = InvokableCallList_PrepareInvoke_m965587067(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		List_1_t1027733493 * L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern "C"  String_t* UnityEventBase_ToString_m3570276379 (UnityEventBase_t2081553920 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_ToString_m3570276379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m3653455109(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m392475457(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3213599556(NULL /*static, unused*/, L_0, _stringLiteral558356722, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1984273667 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t98989581* ___argumentTypes2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_GetValidMethodInfo_m1984273667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	ParameterInfoU5BU5D_t1048445298* V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t3156340899 * V_5 = NULL;
	ParameterInfoU5BU5D_t1048445298* V_6 = NULL;
	int32_t V_7 = 0;
	Type_t * V_8 = NULL;
	Type_t * V_9 = NULL;
	MethodInfo_t * V_10 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m392475457(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_009c;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName1;
		TypeU5BU5D_t98989581* L_4 = ___argumentTypes2;
		NullCheck(L_2);
		MethodInfo_t * L_5 = Type_GetMethod_m4168175761(L_2, L_3, ((int32_t)52), (Binder_t2026886273 *)NULL, L_4, (ParameterModifierU5BU5D_t1706708300*)(ParameterModifierU5BU5D_t1706708300*)NULL, /*hidden argument*/NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t1048445298* L_8 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1048445298* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = (bool)1;
		V_4 = 0;
		ParameterInfoU5BU5D_t1048445298* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_007a;
	}

IL_003a:
	{
		ParameterInfoU5BU5D_t1048445298* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ParameterInfo_t3156340899 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		TypeU5BU5D_t98989581* L_14 = ___argumentTypes2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_8 = L_17;
		ParameterInfo_t3156340899 * L_18 = V_5;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_18);
		V_9 = L_19;
		Type_t * L_20 = V_8;
		NullCheck(L_20);
		bool L_21 = Type_get_IsPrimitive_m1820091081(L_20, /*hidden argument*/NULL);
		Type_t * L_22 = V_9;
		NullCheck(L_22);
		bool L_23 = Type_get_IsPrimitive_m1820091081(L_22, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_21) == ((int32_t)L_23))? 1 : 0);
		bool L_24 = V_3;
		if (L_24)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0085;
	}

IL_006d:
	{
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
		int32_t L_26 = V_7;
		V_7 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_27 = V_7;
		ParameterInfoU5BU5D_t1048445298* L_28 = V_6;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0085:
	{
		bool L_29 = V_3;
		if (!L_29)
		{
			goto IL_0093;
		}
	}
	{
		MethodInfo_t * L_30 = V_1;
		V_10 = L_30;
		goto IL_00ba;
	}

IL_0093:
	{
	}

IL_0094:
	{
		Type_t * L_31 = V_0;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_31);
		V_0 = L_32;
	}

IL_009c:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, LoadTypeToken(RuntimeObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_33) == ((RuntimeObject*)(Type_t *)L_34)))
		{
			goto IL_00b2;
		}
	}
	{
		Type_t * L_35 = V_0;
		if (L_35)
		{
			goto IL_000d;
		}
	}

IL_00b2:
	{
		V_10 = (MethodInfo_t *)NULL;
		goto IL_00ba;
	}

IL_00ba:
	{
		MethodInfo_t * L_36 = V_10;
		return L_36;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C"  void ExecuteInEditMode__ctor_m2945631696 (ExecuteInEditMode_t2915138156 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::get_currentPipeline()
extern "C"  RuntimeObject* RenderPipelineManager_get_currentPipeline_m2834317064 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_get_currentPipeline_m2834317064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_U3CcurrentPipelineU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000b;
	}

IL_000b:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Experimental.Rendering.IRenderPipeline)
extern "C"  void RenderPipelineManager_set_currentPipeline_m603220370 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_set_currentPipeline_m603220370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___value0;
		((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->set_U3CcurrentPipelineU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern "C"  void RenderPipelineManager_CleanupRenderPipeline_m1732771618 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_CleanupRenderPipeline_m1732771618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject* L_1 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipelineAsset::DestroyCreatedInstances() */, IRenderPipelineAsset_t3550769973_il2cpp_TypeInfo_var, L_1);
	}

IL_0015:
	{
		((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0((RuntimeObject*)NULL);
		RenderPipelineManager_set_currentPipeline_m603220370(NULL /*static, unused*/, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Experimental.Rendering.IRenderPipelineAsset,UnityEngine.Camera[],System.IntPtr)
extern "C"  void RenderPipelineManager_DoRenderLoop_Internal_m435582623 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, CameraU5BU5D_t365668710* ___cameras1, intptr_t ___loopPtr2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_DoRenderLoop_Internal_m435582623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ScriptableRenderContext_t3554271739  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject* L_0 = ___pipe0;
		RenderPipelineManager_PrepareRenderPipeline_m4070566852(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RuntimeObject* L_1 = RenderPipelineManager_get_currentPipeline_m2834317064(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_002a;
	}

IL_0016:
	{
		intptr_t L_2 = ___loopPtr2;
		ScriptableRenderContext__ctor_m3085940871((&V_0), L_2, /*hidden argument*/NULL);
		RuntimeObject* L_3 = RenderPipelineManager_get_currentPipeline_m2834317064(NULL /*static, unused*/, /*hidden argument*/NULL);
		ScriptableRenderContext_t3554271739  L_4 = V_0;
		CameraU5BU5D_t365668710* L_5 = ___cameras1;
		NullCheck(L_3);
		InterfaceActionInvoker2< ScriptableRenderContext_t3554271739 , CameraU5BU5D_t365668710* >::Invoke(1 /* System.Void UnityEngine.Experimental.Rendering.IRenderPipeline::Render(UnityEngine.Experimental.Rendering.ScriptableRenderContext,UnityEngine.Camera[]) */, IRenderPipeline_t1337497652_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Experimental.Rendering.IRenderPipelineAsset)
extern "C"  void RenderPipelineManager_PrepareRenderPipeline_m4070566852 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___pipe0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderPipelineManager_PrepareRenderPipeline_m4070566852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		RuntimeObject* L_1 = ___pipe0;
		if ((((RuntimeObject*)(RuntimeObject*)L_0) == ((RuntimeObject*)(RuntimeObject*)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		RuntimeObject* L_2 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RenderPipelineManager_CleanupRenderPipeline_m1732771618(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001e:
	{
		RuntimeObject* L_3 = ___pipe0;
		((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->set_s_CurrentPipelineAsset_0(L_3);
	}

IL_0025:
	{
		RuntimeObject* L_4 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_5 = RenderPipelineManager_get_currentPipeline_m2834317064(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		RuntimeObject* L_6 = RenderPipelineManager_get_currentPipeline_m2834317064(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.Experimental.Rendering.IRenderPipeline::get_disposed() */, IRenderPipeline_t1337497652_il2cpp_TypeInfo_var, L_6);
		if (!L_7)
		{
			goto IL_0057;
		}
	}

IL_0048:
	{
		RuntimeObject* L_8 = ((RenderPipelineManager_t280631689_StaticFields*)il2cpp_codegen_static_fields_for(RenderPipelineManager_t280631689_il2cpp_TypeInfo_var))->get_s_CurrentPipelineAsset_0();
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.IRenderPipelineAsset::CreatePipeline() */, IRenderPipelineAsset_t3550769973_il2cpp_TypeInfo_var, L_8);
		RenderPipelineManager_set_currentPipeline_m603220370(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UnityEngine.Experimental.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern "C"  void ScriptableRenderContext__ctor_m3085940871 (ScriptableRenderContext_t3554271739 * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___ptr0;
		__this->set_m_Ptr_0(L_0);
		return;
	}
}
extern "C"  void ScriptableRenderContext__ctor_m3085940871_AdjustorThunk (RuntimeObject * __this, intptr_t ___ptr0, const RuntimeMethod* method)
{
	ScriptableRenderContext_t3554271739 * _thisAdjusted = reinterpret_cast<ScriptableRenderContext_t3554271739 *>(__this + 1);
	ScriptableRenderContext__ctor_m3085940871(_thisAdjusted, ___ptr0, method);
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_pinvoke(const FailedToLoadScriptObject_t2081117541& unmarshaled, FailedToLoadScriptObject_t2081117541_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_pinvoke_back(const FailedToLoadScriptObject_t2081117541_marshaled_pinvoke& marshaled, FailedToLoadScriptObject_t2081117541& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_pinvoke_cleanup(FailedToLoadScriptObject_t2081117541_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_com(const FailedToLoadScriptObject_t2081117541& unmarshaled, FailedToLoadScriptObject_t2081117541_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_com_back(const FailedToLoadScriptObject_t2081117541_marshaled_com& marshaled, FailedToLoadScriptObject_t2081117541& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.FailedToLoadScriptObject
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_com_cleanup(FailedToLoadScriptObject_t2081117541_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m2683929848 (GameObject_t2881801184 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m2683929848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		GameObject_Internal_CreateGameObject_m4138994077(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m4156075956 (GameObject_t2881801184 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m4156075956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m4138994077(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m4126152586 (GameObject_t2881801184 * __this, String_t* ___name0, TypeU5BU5D_t98989581* ___components1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObject__ctor_m4126152586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	TypeU5BU5D_t98989581* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		GameObject_Internal_CreateGameObject_m4138994077(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_1 = ___components1;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0028;
	}

IL_0018:
	{
		TypeU5BU5D_t98989581* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Type_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		Type_t * L_6 = V_0;
		GameObject_AddComponent_m1531083216(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_8 = V_2;
		TypeU5BU5D_t98989581* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t2335505321 * GameObject_GetComponent_m1768480708 (GameObject_t2881801184 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef Component_t2335505321 * (*GameObject_GetComponent_m1768480708_ftn) (GameObject_t2881801184 *, Type_t *);
	static GameObject_GetComponent_m1768480708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m1768480708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	Component_t2335505321 * retVal = _il2cpp_icall_func(__this, ___type0);
	return retVal;
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void GameObject_GetComponentFastPath_m4146226308 (GameObject_t2881801184 * __this, Type_t * ___type0, intptr_t ___oneFurtherThanResultValue1, const RuntimeMethod* method)
{
	typedef void (*GameObject_GetComponentFastPath_m4146226308_ftn) (GameObject_t2881801184 *, Type_t *, intptr_t);
	static GameObject_GetComponentFastPath_m4146226308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m4146226308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t2335505321 * GameObject_GetComponentInChildren_m58285979 (GameObject_t2881801184 * __this, Type_t * ___type0, bool ___includeInactive1, const RuntimeMethod* method)
{
	typedef Component_t2335505321 * (*GameObject_GetComponentInChildren_m58285979_ftn) (GameObject_t2881801184 *, Type_t *, bool);
	static GameObject_GetComponentInChildren_m58285979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInChildren_m58285979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)");
	Component_t2335505321 * retVal = _il2cpp_icall_func(__this, ___type0, ___includeInactive1);
	return retVal;
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t2335505321 * GameObject_GetComponentInParent_m390353812 (GameObject_t2881801184 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef Component_t2335505321 * (*GameObject_GetComponentInParent_m390353812_ftn) (GameObject_t2881801184 *, Type_t *);
	static GameObject_GetComponentInParent_m390353812_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentInParent_m390353812_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentInParent(System.Type)");
	Component_t2335505321 * retVal = _il2cpp_icall_func(__this, ___type0);
	return retVal;
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C"  RuntimeArray * GameObject_GetComponentsInternal_m221992220 (GameObject_t2881801184 * __this, Type_t * ___type0, bool ___useSearchTypeAsArrayReturnType1, bool ___recursive2, bool ___includeInactive3, bool ___reverse4, RuntimeObject * ___resultList5, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*GameObject_GetComponentsInternal_m221992220_ftn) (GameObject_t2881801184 *, Type_t *, bool, bool, bool, bool, RuntimeObject *);
	static GameObject_GetComponentsInternal_m221992220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m221992220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	RuntimeArray * retVal = _il2cpp_icall_func(__this, ___type0, ___useSearchTypeAsArrayReturnType1, ___recursive2, ___includeInactive3, ___reverse4, ___resultList5);
	return retVal;
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3580444445 * GameObject_get_transform_m1920753955 (GameObject_t2881801184 * __this, const RuntimeMethod* method)
{
	typedef Transform_t3580444445 * (*GameObject_get_transform_m1920753955_ftn) (GameObject_t2881801184 *);
	static GameObject_get_transform_m1920753955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m1920753955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	Transform_t3580444445 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m2443177874 (GameObject_t2881801184 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*GameObject_get_layer_m2443177874_ftn) (GameObject_t2881801184 *);
	static GameObject_get_layer_m2443177874_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m2443177874_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m1022229276 (GameObject_t2881801184 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*GameObject_set_layer_m1022229276_ftn) (GameObject_t2881801184 *, int32_t);
	static GameObject_set_layer_m1022229276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m1022229276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m181914636 (GameObject_t2881801184 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*GameObject_SetActive_m181914636_ftn) (GameObject_t2881801184 *, bool);
	static GameObject_SetActive_m181914636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m181914636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C"  bool GameObject_get_activeSelf_m401693689 (GameObject_t2881801184 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_activeSelf_m401693689_ftn) (GameObject_t2881801184 *);
	static GameObject_get_activeSelf_m401693689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeSelf_m401693689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeSelf()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m3756050688 (GameObject_t2881801184 * __this, const RuntimeMethod* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m3756050688_ftn) (GameObject_t2881801184 *);
	static GameObject_get_activeInHierarchy_m3756050688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m3756050688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m4104693933 (GameObject_t2881801184 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method)
{
	typedef void (*GameObject_SendMessage_m4104693933_ftn) (GameObject_t2881801184 *, String_t*, RuntimeObject *, int32_t);
	static GameObject_SendMessage_m4104693933_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m4104693933_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C"  Component_t2335505321 * GameObject_Internal_AddComponentWithType_m246566982 (GameObject_t2881801184 * __this, Type_t * ___componentType0, const RuntimeMethod* method)
{
	typedef Component_t2335505321 * (*GameObject_Internal_AddComponentWithType_m246566982_ftn) (GameObject_t2881801184 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m246566982_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m246566982_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	Component_t2335505321 * retVal = _il2cpp_icall_func(__this, ___componentType0);
	return retVal;
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t2335505321 * GameObject_AddComponent_m1531083216 (GameObject_t2881801184 * __this, Type_t * ___componentType0, const RuntimeMethod* method)
{
	Component_t2335505321 * V_0 = NULL;
	{
		Type_t * L_0 = ___componentType0;
		Component_t2335505321 * L_1 = GameObject_Internal_AddComponentWithType_m246566982(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Component_t2335505321 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C"  void GameObject_Internal_CreateGameObject_m4138994077 (RuntimeObject * __this /* static, unused */, GameObject_t2881801184 * ___mono0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m4138994077_ftn) (GameObject_t2881801184 *, String_t*);
	static GameObject_Internal_CreateGameObject_m4138994077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m4138994077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono0, ___name1);
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t2667682126_marshal_pinvoke(const Gradient_t2667682126& unmarshaled, Gradient_t2667682126_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t2667682126_marshal_pinvoke_back(const Gradient_t2667682126_marshaled_pinvoke& marshaled, Gradient_t2667682126& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t2667682126_marshal_pinvoke_cleanup(Gradient_t2667682126_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t2667682126_marshal_com(const Gradient_t2667682126& unmarshaled, Gradient_t2667682126_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void Gradient_t2667682126_marshal_com_back(const Gradient_t2667682126_marshaled_com& marshaled, Gradient_t2667682126& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t2667682126_marshal_com_cleanup(Gradient_t2667682126_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m2788690162 (Gradient_t2667682126 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		Gradient_Init_m3509320913(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C"  void Gradient_Init_m3509320913 (Gradient_t2667682126 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Init_m3509320913_ftn) (Gradient_t2667682126 *);
	static Gradient_Init_m3509320913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m3509320913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C"  void Gradient_Cleanup_m3098355422 (Gradient_t2667682126 * __this, const RuntimeMethod* method)
{
	typedef void (*Gradient_Cleanup_m3098355422_ftn) (Gradient_t2667682126 *);
	static Gradient_Cleanup_m3098355422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m3098355422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C"  void Gradient_Finalize_m2226337423 (Gradient_t2667682126 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m3098355422(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.GUIElement::.ctor()
extern "C"  void GUIElement__ctor_m766547788 (GUIElement_t2757512779 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m1626382119(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t2757512779 * GUILayer_HitTest_m1524866709 (GUILayer_t1634068716 * __this, Vector3_t3932393085  ___screenPosition0, const RuntimeMethod* method)
{
	GUIElement_t2757512779 * V_0 = NULL;
	{
		GUIElement_t2757512779 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m4196901861(NULL /*static, unused*/, __this, (&___screenPosition0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		GUIElement_t2757512779 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C"  GUIElement_t2757512779 * GUILayer_INTERNAL_CALL_HitTest_m4196901861 (RuntimeObject * __this /* static, unused */, GUILayer_t1634068716 * ___self0, Vector3_t3932393085 * ___screenPosition1, const RuntimeMethod* method)
{
	typedef GUIElement_t2757512779 * (*GUILayer_INTERNAL_CALL_HitTest_m4196901861_ftn) (GUILayer_t1634068716 *, Vector3_t3932393085 *);
	static GUILayer_INTERNAL_CALL_HitTest_m4196901861_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m4196901861_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	GUIElement_t2757512779 * retVal = _il2cpp_icall_func(___self0, ___screenPosition1);
	return retVal;
}
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
extern "C"  void HeaderAttribute__ctor_m2307250818 (HeaderAttribute_t1170597384 * __this, String_t* ___header0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3690955331(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___header0;
		__this->set_header_0(L_0);
		return;
	}
}
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C"  void IL2CPPStructAlignmentAttribute__ctor_m587733786 (IL2CPPStructAlignmentAttribute_t1993002847 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		__this->set_Align_0(1);
		return;
	}
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m4266694756 (RuntimeObject * __this /* static, unused */, String_t* ___axisName0, const RuntimeMethod* method)
{
	typedef float (*Input_GetAxisRaw_m4266694756_ftn) (String_t*);
	static Input_GetAxisRaw_m4266694756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m4266694756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	float retVal = _il2cpp_icall_func(___axisName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m2622867223 (RuntimeObject * __this /* static, unused */, String_t* ___buttonName0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetButtonDown_m2622867223_ftn) (String_t*);
	static Input_GetButtonDown_m2622867223_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m2622867223_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	bool retVal = _il2cpp_icall_func(___buttonName0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m2505676056 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButton_m2505676056_ftn) (int32_t);
	static Input_GetMouseButton_m2505676056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m2505676056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m4077202833 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonDown_m4077202833_ftn) (int32_t);
	static Input_GetMouseButtonDown_m4077202833_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m4077202833_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m45659326 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method)
{
	typedef bool (*Input_GetMouseButtonUp_m45659326_ftn) (int32_t);
	static Input_GetMouseButtonUp_m45659326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m45659326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	bool retVal = _il2cpp_icall_func(___button0);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3932393085  Input_get_mousePosition_m3071837934 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mousePosition_m3071837934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m1318263342(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C"  void Input_INTERNAL_get_mousePosition_m1318263342 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m1318263342_ftn) (Vector3_t3932393085 *);
	static Input_INTERNAL_get_mousePosition_m1318263342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m1318263342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern "C"  Vector2_t2246369278  Input_get_mouseScrollDelta_m3998771486 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_mouseScrollDelta_m3998771486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m2002340172(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_mouseScrollDelta_m2002340172 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m2002340172_ftn) (Vector2_t2246369278 *);
	static Input_INTERNAL_get_mouseScrollDelta_m2002340172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m2002340172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C"  bool Input_get_mousePresent_m3731036923 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Input_get_mousePresent_m3731036923_ftn) ();
	static Input_get_mousePresent_m3731036923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePresent_m3731036923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePresent()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t2641233222  Input_GetTouch_m1074987538 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_GetTouch_m1074987538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t2641233222  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t2641233222  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		Input_INTERNAL_CALL_GetTouch_m4079784025(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Touch_t2641233222  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Touch_t2641233222  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
extern "C"  void Input_INTERNAL_CALL_GetTouch_m4079784025 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Touch_t2641233222 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_CALL_GetTouch_m4079784025_ftn) (int32_t, Touch_t2641233222 *);
	static Input_INTERNAL_CALL_GetTouch_m4079784025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_CALL_GetTouch_m4079784025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)");
	_il2cpp_icall_func(___index0, ___value1);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3620982801 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Input_get_touchCount_m3620982801_ftn) ();
	static Input_get_touchCount_m3620982801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m3620982801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C"  bool Input_get_touchSupported_m345400211 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Input_get_touchSupported_m345400211_ftn) ();
	static Input_get_touchSupported_m345400211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchSupported_m345400211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchSupported()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
extern "C"  int32_t Input_get_imeCompositionMode_m3370018153 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Input_get_imeCompositionMode_m3370018153_ftn) ();
	static Input_get_imeCompositionMode_m3370018153_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_imeCompositionMode_m3370018153_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_imeCompositionMode()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C"  void Input_set_imeCompositionMode_m1312012973 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_set_imeCompositionMode_m1312012973_ftn) (int32_t);
	static Input_set_imeCompositionMode_m1312012973_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m1312012973_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C"  String_t* Input_get_compositionString_m935978119 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef String_t* (*Input_get_compositionString_m935978119_ftn) ();
	static Input_get_compositionString_m935978119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m935978119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	String_t* retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Vector2 UnityEngine.Input::get_compositionCursorPos()
extern "C"  Vector2_t2246369278  Input_get_compositionCursorPos_m2540437126 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_get_compositionCursorPos_m2540437126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_compositionCursorPos_m4215801410(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern "C"  void Input_set_compositionCursorPos_m2547795730 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input_set_compositionCursorPos_m2547795730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m2989051778(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_get_compositionCursorPos_m4215801410 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_get_compositionCursorPos_m4215801410_ftn) (Vector2_t2246369278 *);
	static Input_INTERNAL_get_compositionCursorPos_m4215801410_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_compositionCursorPos_m4215801410_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C"  void Input_INTERNAL_set_compositionCursorPos_m2989051778 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m2989051778_ftn) (Vector2_t2246369278 *);
	static Input_INTERNAL_set_compositionCursorPos_m2989051778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m2989051778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Input::.cctor()
extern "C"  void Input__cctor_m1061768007 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Input__cctor_m1061768007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Input_t1636556003_StaticFields*)il2cpp_codegen_static_fields_for(Input_t1636556003_il2cpp_TypeInfo_var))->set_m_MainGyro_0((Gyroscope_t3917155460 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C"  void DefaultValueAttribute__ctor_m2408158159 (DefaultValueAttribute_t1912603919 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_DefaultValue_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C"  RuntimeObject * DefaultValueAttribute_get_Value_m3489993842 (DefaultValueAttribute_t1912603919 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern "C"  bool DefaultValueAttribute_Equals_m4044023824 (DefaultValueAttribute_t1912603919 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultValueAttribute_Equals_m4044023824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t1912603919 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((DefaultValueAttribute_t1912603919 *)IsInstClass((RuntimeObject*)L_0, DefaultValueAttribute_t1912603919_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t1912603919 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0046;
	}

IL_0015:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		DefaultValueAttribute_t1912603919 * L_3 = V_0;
		NullCheck(L_3);
		RuntimeObject * L_4 = DefaultValueAttribute_get_Value_m3489993842(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((RuntimeObject*)(RuntimeObject *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		goto IL_0046;
	}

IL_002f:
	{
		RuntimeObject * L_5 = __this->get_DefaultValue_0();
		DefaultValueAttribute_t1912603919 * L_6 = V_0;
		NullCheck(L_6);
		RuntimeObject * L_7 = DefaultValueAttribute_get_Value_m3489993842(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		V_1 = L_8;
		goto IL_0046;
	}

IL_0046:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C"  int32_t DefaultValueAttribute_GetHashCode_m1372908040 (DefaultValueAttribute_t1912603919 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RuntimeObject * L_0 = __this->get_DefaultValue_0();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m844029469(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0029;
	}

IL_0018:
	{
		RuntimeObject * L_2 = __this->get_DefaultValue_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_0029:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C"  void ExcludeFromDocsAttribute__ctor_m1373106694 (ExcludeFromDocsAttribute_t1059923024 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::Destroy()
extern "C"  void LocalNotification_Destroy_m2598449680 (LocalNotification_t2192530767 * __this, const RuntimeMethod* method)
{
	typedef void (*LocalNotification_Destroy_m2598449680_ftn) (LocalNotification_t2192530767 *);
	static LocalNotification_Destroy_m2598449680_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LocalNotification_Destroy_m2598449680_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.LocalNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.LocalNotification::Finalize()
extern "C"  void LocalNotification_Finalize_m122283322 (LocalNotification_t2192530767 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		LocalNotification_Destroy_m2598449680(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.iOS.LocalNotification::.cctor()
extern "C"  void LocalNotification__cctor_m1797189921 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LocalNotification__cctor_m1797189921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t218649865  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m1351701445((&V_0), ((int32_t)2001), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		int64_t L_0 = DateTime_get_Ticks_m629892755((&V_0), /*hidden argument*/NULL);
		((LocalNotification_t2192530767_StaticFields*)il2cpp_codegen_static_fields_for(LocalNotification_t2192530767_il2cpp_TypeInfo_var))->set_m_NSReferenceDateTicks_1(L_0);
		return;
	}
}
// System.Void UnityEngine.iOS.RemoteNotification::Destroy()
extern "C"  void RemoteNotification_Destroy_m1378408698 (RemoteNotification_t357781426 * __this, const RuntimeMethod* method)
{
	typedef void (*RemoteNotification_Destroy_m1378408698_ftn) (RemoteNotification_t357781426 *);
	static RemoteNotification_Destroy_m1378408698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RemoteNotification_Destroy_m1378408698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.iOS.RemoteNotification::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.iOS.RemoteNotification::Finalize()
extern "C"  void RemoteNotification_Finalize_m2516548014 (RemoteNotification_t357781426 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		RemoteNotification_Destroy_m1378408698(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m1256804247 (RuntimeObject * __this /* static, unused */, LayerMask_t4067055223  ___mask0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (&___mask0)->get_m_Mask_0();
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t4067055223  LayerMask_op_Implicit_m2032535732 (RuntimeObject * __this /* static, unused */, int32_t ___intVal0, const RuntimeMethod* method)
{
	LayerMask_t4067055223  V_0;
	memset(&V_0, 0, sizeof(V_0));
	LayerMask_t4067055223  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___intVal0;
		(&V_0)->set_m_Mask_0(L_0);
		LayerMask_t4067055223  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		LayerMask_t4067055223  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Light::set_colorTemperature(System.Single)
extern "C"  void Light_set_colorTemperature_m2352608539 (Light_t455885974 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Light_set_colorTemperature_m2352608539_ftn) (Light_t455885974 *, float);
	static Light_set_colorTemperature_m2352608539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_colorTemperature_m2352608539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_colorTemperature(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m1741009146 (Light_t455885974 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Light_set_intensity_m1741009146_ftn) (Light_t455885974 *, float);
	static Light_set_intensity_m1741009146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_intensity_m1741009146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_intensity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m998766840 (Logger_t4035514390 * __this, RuntimeObject* ___logHandler0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___logHandler0;
		Logger_set_logHandler_m1865923159(__this, L_0, /*hidden argument*/NULL);
		Logger_set_logEnabled_m81927742(__this, (bool)1, /*hidden argument*/NULL);
		Logger_set_filterLogType_m3869200416(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  RuntimeObject* Logger_get_logHandler_m1556655871 (Logger_t4035514390 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->get_U3ClogHandlerU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m1865923159 (Logger_t4035514390 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___value0;
		__this->set_U3ClogHandlerU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m2365136669 (Logger_t4035514390 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3ClogEnabledU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m81927742 (Logger_t4035514390 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ClogEnabledU3Ek__BackingField_1(L_0);
		return;
	}
}
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m3881820577 (Logger_t4035514390 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CfilterLogTypeU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m3869200416 (Logger_t4035514390 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfilterLogTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m3980787710 (Logger_t4035514390 * __this, int32_t ___logType0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = Logger_get_logEnabled_m2365136669(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_1 = ___logType0;
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_001b;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_0041;
	}

IL_001b:
	{
		int32_t L_2 = Logger_get_filterLogType_m3881820577(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = ___logType0;
		int32_t L_4 = Logger_get_filterLogType_m3881820577(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_3) > ((int32_t)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0041;
	}

IL_0039:
	{
	}

IL_003a:
	{
		V_0 = (bool)0;
		goto IL_0041;
	}

IL_0041:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Logger::GetString(System.Object)
extern "C"  String_t* Logger_GetString_m2315344356 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_GetString_m2315344356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		RuntimeObject * L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_1 = ___message0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral3399283310;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001d;
	}

IL_001d:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern "C"  void Logger_Log_m2721486313 (Logger_t4035514390 * __this, int32_t ___logType0, RuntimeObject * ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m2721486313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m3980787710(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1556655871(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		ObjectU5BU5D_t3385344125* L_4 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeObject * L_5 = ___message1;
		String_t* L_6 = Logger_GetString_m2315344356(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t3645472222 *, String_t*, ObjectU5BU5D_t3385344125* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_2, L_3, (Object_t3645472222 *)NULL, _stringLiteral1808051577, L_4);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern "C"  void Logger_Log_m2254554809 (Logger_t4035514390 * __this, int32_t ___logType0, RuntimeObject * ___message1, Object_t3645472222 * ___context2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m2254554809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m3980787710(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1556655871(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t3645472222 * L_4 = ___context2;
		ObjectU5BU5D_t3385344125* L_5 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeObject * L_6 = ___message1;
		String_t* L_7 = Logger_GetString_m2315344356(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_7);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t3645472222 *, String_t*, ObjectU5BU5D_t3385344125* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_2, L_3, L_4, _stringLiteral1808051577, L_5);
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void Logger_LogFormat_m422274544 (Logger_t4035514390 * __this, int32_t ___logType0, Object_t3645472222 * ___context1, String_t* ___format2, ObjectU5BU5D_t3385344125* ___args3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m422274544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m3980787710(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_2 = Logger_get_logHandler_m1556655871(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t3645472222 * L_4 = ___context1;
		String_t* L_5 = ___format2;
		ObjectU5BU5D_t3385344125* L_6 = ___args3;
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t3645472222 *, String_t*, ObjectU5BU5D_t3385344125* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Logger_LogException_m1408988682 (Logger_t4035514390 * __this, Exception_t214279536 * ___exception0, Object_t3645472222 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogException_m1408988682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Logger_get_logEnabled_m2365136669(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_1 = Logger_get_logHandler_m1556655871(__this, /*hidden argument*/NULL);
		Exception_t214279536 * L_2 = ___exception0;
		Object_t3645472222 * L_3 = ___context1;
		NullCheck(L_1);
		InterfaceActionInvoker2< Exception_t214279536 *, Object_t3645472222 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t4143530507_il2cpp_TypeInfo_var, L_1, L_2, L_3);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern "C"  void ManagedStreamHelpers_ValidateLoadFromStream_m2234507788 (RuntimeObject * __this /* static, unused */, Stream_t360920014 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ValidateLoadFromStream_m2234507788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t360920014 * L_0 = ___stream0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t246139103 * L_1 = (ArgumentNullException_t246139103 *)il2cpp_codegen_object_new(ArgumentNullException_t246139103_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2341126836(L_1, _stringLiteral1214607453, _stringLiteral2667027303, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Stream_t360920014 * L_2 = ___stream0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Stream::get_CanRead() */, L_2);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		ArgumentException_t489606696 * L_4 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_4, _stringLiteral1098340457, _stringLiteral2667027303, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0032:
	{
		Stream_t360920014 * L_5 = ___stream0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.IO.Stream::get_CanSeek() */, L_5);
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		ArgumentException_t489606696 * L_7 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_7, _stringLiteral885527817, _stringLiteral2667027303, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamRead(System.Byte[],System.Int32,System.Int32,System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamRead_m1646611388 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3567143369* ___buffer0, int32_t ___offset1, int32_t ___count2, Stream_t360920014 * ___stream3, intptr_t ___returnValueAddress4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamRead_m1646611388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress4;
		bool L_1 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_2, _stringLiteral1994973061, _stringLiteral2859367373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Stream_t360920014 * L_3 = ___stream3;
		ManagedStreamHelpers_ValidateLoadFromStream_m2234507788(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress4;
		void* L_5 = IntPtr_op_Explicit_m259387555(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t360920014 * L_6 = ___stream3;
		ByteU5BU5D_t3567143369* L_7 = ___buffer0;
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___count2;
		NullCheck(L_6);
		int32_t L_10 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3567143369*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, L_8, L_9);
		*((int32_t*)(L_5)) = (int32_t)L_10;
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamSeek(System.Int64,System.UInt32,System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamSeek_m2841650013 (RuntimeObject * __this /* static, unused */, int64_t ___offset0, uint32_t ___origin1, Stream_t360920014 * ___stream2, intptr_t ___returnValueAddress3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamSeek_m2841650013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress3;
		bool L_1 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_2, _stringLiteral1994973061, _stringLiteral2859367373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		Stream_t360920014 * L_3 = ___stream2;
		ManagedStreamHelpers_ValidateLoadFromStream_m2234507788(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress3;
		void* L_5 = IntPtr_op_Explicit_m259387555(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t360920014 * L_6 = ___stream2;
		int64_t L_7 = ___offset0;
		uint32_t L_8 = ___origin1;
		NullCheck(L_6);
		int64_t L_9 = VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(16 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_6, L_7, L_8);
		*((int64_t*)(L_5)) = (int64_t)L_9;
		return;
	}
}
// System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamLength(System.IO.Stream,System.IntPtr)
extern "C"  void ManagedStreamHelpers_ManagedStreamLength_m2317062430 (RuntimeObject * __this /* static, unused */, Stream_t360920014 * ___stream0, intptr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManagedStreamHelpers_ManagedStreamLength_m2317062430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress1;
		bool L_1 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_2, _stringLiteral1994973061, _stringLiteral2859367373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		Stream_t360920014 * L_3 = ___stream0;
		ManagedStreamHelpers_ValidateLoadFromStream_m2234507788(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		intptr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m259387555(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Stream_t360920014 * L_6 = ___stream0;
		NullCheck(L_6);
		int64_t L_7 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_6);
		*((int64_t*)(L_5)) = (int64_t)L_7;
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m2996058918 (Material_t1926439680 * __this, Material_t1926439680 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Material__ctor_m2996058918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		Material_t1926439680 * L_0 = ___source0;
		Material_Internal_CreateWithMaterial_m1324188081(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m899657254 (Material_t1926439680 * __this, Color_t1361298052  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Material_set_color_m899657254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t1361298052  L_0 = ___value0;
		Material_SetColor_m3068886589(__this, _stringLiteral3523295012, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern "C"  Texture_t954505614 * Material_get_mainTexture_m3281846005 (Material_t1926439680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Material_get_mainTexture_m3281846005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture_t954505614 * V_0 = NULL;
	{
		Texture_t954505614 * L_0 = Material_GetTexture_m4101001118(__this, _stringLiteral3551009901, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Texture_t954505614 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
extern "C"  void Material_SetIntImpl_m3376002208 (Material_t1926439680 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*Material_SetIntImpl_m3376002208_ftn) (Material_t1926439680 *, int32_t, int32_t);
	static Material_SetIntImpl_m3376002208_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetIntImpl_m3376002208_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColorImpl_m583017531 (Material_t1926439680 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetColorImpl_m1120750802(NULL /*static, unused*/, __this, L_0, (&___value1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColorImpl_m1120750802 (RuntimeObject * __this /* static, unused */, Material_t1926439680 * ___self0, int32_t ___nameID1, Color_t1361298052 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColorImpl_m1120750802_ftn) (Material_t1926439680 *, int32_t, Color_t1361298052 *);
	static Material_INTERNAL_CALL_SetColorImpl_m1120750802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColorImpl_m1120750802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrixImpl_m3459530554 (Material_t1926439680 * __this, int32_t ___nameID0, Matrix4x4_t787966842  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetMatrixImpl_m1727523669(NULL /*static, unused*/, __this, L_0, (&___value1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)
extern "C"  void Material_INTERNAL_CALL_SetMatrixImpl_m1727523669 (RuntimeObject * __this /* static, unused */, Material_t1926439680 * ___self0, int32_t ___nameID1, Matrix4x4_t787966842 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Material_INTERNAL_CALL_SetMatrixImpl_m1727523669_ftn) (Material_t1926439680 *, int32_t, Matrix4x4_t787966842 *);
	static Material_INTERNAL_CALL_SetMatrixImpl_m1727523669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetMatrixImpl_m1727523669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetMatrixImpl(UnityEngine.Material,System.Int32,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTextureImpl_m1670116897 (Material_t1926439680 * __this, int32_t ___nameID0, Texture_t954505614 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Material_SetTextureImpl_m1670116897_ftn) (Material_t1926439680 *, int32_t, Texture_t954505614 *);
	static Material_SetTextureImpl_m1670116897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTextureImpl_m1670116897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern "C"  Texture_t954505614 * Material_GetTextureImpl_m1308611253 (Material_t1926439680 * __this, int32_t ___nameID0, const RuntimeMethod* method)
{
	typedef Texture_t954505614 * (*Material_GetTextureImpl_m1308611253_ftn) (Material_t1926439680 *, int32_t);
	static Material_GetTextureImpl_m1308611253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTextureImpl_m1308611253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTextureImpl(System.Int32)");
	Texture_t954505614 * retVal = _il2cpp_icall_func(__this, ___nameID0);
	return retVal;
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C"  bool Material_HasProperty_m3078718302 (Material_t1926439680 * __this, String_t* ___propertyName0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m1510906695(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C"  bool Material_HasProperty_m1510906695 (Material_t1926439680 * __this, int32_t ___nameID0, const RuntimeMethod* method)
{
	typedef bool (*Material_HasProperty_m1510906695_ftn) (Material_t1926439680 *, int32_t);
	static Material_HasProperty_m1510906695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m1510906695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	bool retVal = _il2cpp_icall_func(__this, ___nameID0);
	return retVal;
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m1324188081 (RuntimeObject * __this /* static, unused */, Material_t1926439680 * ___mono0, Material_t1926439680 * ___source1, const RuntimeMethod* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m1324188081_ftn) (Material_t1926439680 *, Material_t1926439680 *);
	static Material_Internal_CreateWithMaterial_m1324188081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m1324188081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono0, ___source1);
}
// System.Void UnityEngine.Material::EnableKeyword(System.String)
extern "C"  void Material_EnableKeyword_m1953490200 (Material_t1926439680 * __this, String_t* ___keyword0, const RuntimeMethod* method)
{
	typedef void (*Material_EnableKeyword_m1953490200_ftn) (Material_t1926439680 *, String_t*);
	static Material_EnableKeyword_m1953490200_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_EnableKeyword_m1953490200_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::EnableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Material::DisableKeyword(System.String)
extern "C"  void Material_DisableKeyword_m574383288 (Material_t1926439680 * __this, String_t* ___keyword0, const RuntimeMethod* method)
{
	typedef void (*Material_DisableKeyword_m574383288_ftn) (Material_t1926439680 *, String_t*);
	static Material_DisableKeyword_m574383288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_DisableKeyword_m574383288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::DisableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C"  void Material_SetInt_m1785225799 (Material_t1926439680 * __this, String_t* ___name0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value1;
		Material_SetInt_m4218872196(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern "C"  void Material_SetInt_m4218872196 (Material_t1926439680 * __this, int32_t ___nameID0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		int32_t L_1 = ___value1;
		Material_SetIntImpl_m3376002208(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m3068886589 (Material_t1926439680 * __this, String_t* ___name0, Color_t1361298052  ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t1361298052  L_2 = ___value1;
		Material_SetColor_m54426709(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m54426709 (Material_t1926439680 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Color_t1361298052  L_1 = ___value1;
		Material_SetColorImpl_m583017531(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m1083643080 (Material_t1926439680 * __this, String_t* ___name0, Matrix4x4_t787966842  ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Matrix4x4_t787966842  L_2 = ___value1;
		Material_SetMatrix_m2604306289(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m2604306289 (Material_t1926439680 * __this, int32_t ___nameID0, Matrix4x4_t787966842  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Matrix4x4_t787966842  L_1 = ___value1;
		Material_SetMatrixImpl_m3459530554(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1477839489 (Material_t1926439680 * __this, String_t* ___name0, Texture_t954505614 * ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t954505614 * L_2 = ___value1;
		Material_SetTexture_m1458093430(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1458093430 (Material_t1926439680 * __this, int32_t ___nameID0, Texture_t954505614 * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Texture_t954505614 * L_1 = ___value1;
		Material_SetTextureImpl_m1670116897(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C"  Texture_t954505614 * Material_GetTexture_m4101001118 (Material_t1926439680 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	Texture_t954505614 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t954505614 * L_2 = Material_GetTexture_m1586524227(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Texture_t954505614 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C"  Texture_t954505614 * Material_GetTexture_m1586524227 (Material_t1926439680 * __this, int32_t ___nameID0, const RuntimeMethod* method)
{
	Texture_t954505614 * V_0 = NULL;
	{
		int32_t L_0 = ___nameID0;
		Texture_t954505614 * L_1 = Material_GetTextureImpl_m1308611253(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Texture_t954505614 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern "C"  void MaterialPropertyBlock__ctor_m1620852487 (MaterialPropertyBlock_t3731058050 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		MaterialPropertyBlock_InitBlock_m2591129074(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::InitBlock()
extern "C"  void MaterialPropertyBlock_InitBlock_m2591129074 (MaterialPropertyBlock_t3731058050 * __this, const RuntimeMethod* method)
{
	typedef void (*MaterialPropertyBlock_InitBlock_m2591129074_ftn) (MaterialPropertyBlock_t3731058050 *);
	static MaterialPropertyBlock_InitBlock_m2591129074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MaterialPropertyBlock_InitBlock_m2591129074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MaterialPropertyBlock::InitBlock()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MaterialPropertyBlock::DestroyBlock()
extern "C"  void MaterialPropertyBlock_DestroyBlock_m3884907647 (MaterialPropertyBlock_t3731058050 * __this, const RuntimeMethod* method)
{
	typedef void (*MaterialPropertyBlock_DestroyBlock_m3884907647_ftn) (MaterialPropertyBlock_t3731058050 *);
	static MaterialPropertyBlock_DestroyBlock_m3884907647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MaterialPropertyBlock_DestroyBlock_m3884907647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MaterialPropertyBlock::DestroyBlock()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MaterialPropertyBlock::Finalize()
extern "C"  void MaterialPropertyBlock_Finalize_m984157310 (MaterialPropertyBlock_t3731058050 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		MaterialPropertyBlock_DestroyBlock_m3884907647(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::SetColorImpl(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColorImpl_m2535405673 (MaterialPropertyBlock_t3731058050 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m3247465324(NULL /*static, unused*/, __this, L_0, (&___value1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)
extern "C"  void MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m3247465324 (RuntimeObject * __this /* static, unused */, MaterialPropertyBlock_t3731058050 * ___self0, int32_t ___nameID1, Color_t1361298052 * ___value2, const RuntimeMethod* method)
{
	typedef void (*MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m3247465324_ftn) (MaterialPropertyBlock_t3731058050 *, int32_t, Color_t1361298052 *);
	static MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m3247465324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MaterialPropertyBlock_INTERNAL_CALL_SetColorImpl_m3247465324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MaterialPropertyBlock::INTERNAL_CALL_SetColorImpl(UnityEngine.MaterialPropertyBlock,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.String,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m1956670574 (MaterialPropertyBlock_t3731058050 * __this, String_t* ___name0, Color_t1361298052  ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Shader_PropertyToID_m740525257(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t1361298052  L_2 = ___value1;
		MaterialPropertyBlock_SetColor_m2921944880(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void MaterialPropertyBlock_SetColor_m2921944880 (MaterialPropertyBlock_t3731058050 * __this, int32_t ___nameID0, Color_t1361298052  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nameID0;
		Color_t1361298052  L_1 = ___value1;
		MaterialPropertyBlock_SetColorImpl_m2535405673(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C"  float Mathf_Sin_m4037999444 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = sin((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C"  float Mathf_Cos_m927021741 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = cos((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C"  float Mathf_Sqrt_m3853091784 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = sqrt((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m4074566312 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		V_0 = (((float)((float)L_1)));
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m186319185 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m2055142955 (RuntimeObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3686719278 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m1061756332 (RuntimeObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000f;
	}

IL_000e:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000f:
	{
		V_0 = G_B3_0;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C"  float Mathf_Pow_m1025228877 (RuntimeObject * __this /* static, unused */, float ___f0, float ___p1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = pow((((double)((double)L_0))), (((double)((double)L_1))));
		V_0 = (((float)((float)L_2)));
		goto IL_0011;
	}

IL_0011:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C"  float Mathf_Log_m2070958819 (RuntimeObject * __this /* static, unused */, float ___f0, float ___p1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = Math_Log_m2970697177(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		V_0 = (((float)((float)L_2)));
		goto IL_0011;
	}

IL_0011:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C"  float Mathf_Floor_m4281700908 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C"  float Mathf_Round_m3653581481 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		V_0 = (((float)((float)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C"  int32_t Mathf_CeilToInt_m905730346 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m469689964 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m3856821223 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m248632674 (RuntimeObject * __this /* static, unused */, float ___f0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f0;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001b;
	}

IL_0016:
	{
		G_B3_0 = (-1.0f);
	}

IL_001b:
	{
		V_0 = G_B3_0;
		goto IL_0021;
	}

IL_0021:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3347905369 (RuntimeObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0010;
		}
	}
	{
		float L_2 = ___min1;
		___value0 = L_2;
		goto IL_001a;
	}

IL_0010:
	{
		float L_3 = ___value0;
		float L_4 = ___max2;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_001a;
		}
	}
	{
		float L_5 = ___max2;
		___value0 = L_5;
	}

IL_001a:
	{
		float L_6 = ___value0;
		V_0 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m4133013370 (RuntimeObject * __this /* static, unused */, int32_t ___value0, int32_t ___min1, int32_t ___max2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___min1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = ___min1;
		___value0 = L_2;
		goto IL_001a;
	}

IL_0010:
	{
		int32_t L_3 = ___value0;
		int32_t L_4 = ___max2;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_5 = ___max2;
		___value0 = L_5;
	}

IL_001a:
	{
		int32_t L_6 = ___value0;
		V_0 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2797960148 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___value0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		V_0 = (0.0f);
		goto IL_0034;
	}

IL_0017:
	{
		float L_1 = ___value0;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		V_0 = (1.0f);
		goto IL_0034;
	}

IL_002d:
	{
		float L_2 = ___value0;
		V_0 = L_2;
		goto IL_0034;
	}

IL_0034:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1810328684 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Lerp_m1810328684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
		goto IL_0013;
	}

IL_0013:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m4050652555 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m4050652555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m3686719278(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t9189715_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t9189715_il2cpp_TypeInfo_var))->get_Epsilon_0();
		float L_9 = Mathf_Max_m3686719278(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
		goto IL_0038;
	}

IL_0038:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern "C"  float Mathf_SmoothDamp_m139100806 (RuntimeObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, float ___maxSpeed4, float ___deltaTime5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDamp_m139100806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		float L_0 = ___smoothTime3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m3686719278(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime3 = L_1;
		float L_2 = ___smoothTime3;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime5;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current0;
		float L_12 = ___target1;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target1;
		V_4 = L_13;
		float L_14 = ___maxSpeed4;
		float L_15 = ___smoothTime3;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m3347905369(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current0;
		float L_21 = V_3;
		___target1 = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity2;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime5;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity2;
		float* L_27 = ___currentVelocity2;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target1;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current0;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a3;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity2;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime5;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a3:
	{
		float L_44 = V_7;
		V_8 = L_44;
		goto IL_00ac;
	}

IL_00ac:
	{
		float L_45 = V_8;
		return L_45;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C"  float Mathf_Repeat_m220049890 (RuntimeObject * __this /* static, unused */, float ___t0, float ___length1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Repeat_m220049890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___t0;
		float L_1 = ___t0;
		float L_2 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length1;
		float L_5 = ___length1;
		float L_6 = Mathf_Clamp_m3347905369(NULL /*static, unused*/, ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4)))), (0.0f), L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_InverseLerp_m3361658789 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, float ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf_InverseLerp_m3361658789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		float L_2 = ___value2;
		float L_3 = ___a0;
		float L_4 = ___b1;
		float L_5 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, ((float)((float)((float)((float)L_2-(float)L_3))/(float)((float)((float)L_4-(float)L_5)))), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0025;
	}

IL_001a:
	{
		V_0 = (0.0f);
		goto IL_0025;
	}

IL_0025:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Mathf::.cctor()
extern "C"  void Mathf__cctor_m3492373530 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m3492373530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1878424006_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t1878424006_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1878424006_il2cpp_TypeInfo_var))->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1878424006_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t1878424006_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1878424006_il2cpp_TypeInfo_var))->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t1878424006_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t1878424006_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t1878424006_il2cpp_TypeInfo_var))->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t9189715_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t9189715_il2cpp_TypeInfo_var))->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  void Matrix4x4__ctor_m4089366256 (Matrix4x4_t787966842 * __this, Vector4_t1900979187  ___column00, Vector4_t1900979187  ___column11, Vector4_t1900979187  ___column22, Vector4_t1900979187  ___column33, const RuntimeMethod* method)
{
	{
		float L_0 = (&___column00)->get_x_1();
		__this->set_m00_0(L_0);
		float L_1 = (&___column11)->get_x_1();
		__this->set_m01_4(L_1);
		float L_2 = (&___column22)->get_x_1();
		__this->set_m02_8(L_2);
		float L_3 = (&___column33)->get_x_1();
		__this->set_m03_12(L_3);
		float L_4 = (&___column00)->get_y_2();
		__this->set_m10_1(L_4);
		float L_5 = (&___column11)->get_y_2();
		__this->set_m11_5(L_5);
		float L_6 = (&___column22)->get_y_2();
		__this->set_m12_9(L_6);
		float L_7 = (&___column33)->get_y_2();
		__this->set_m13_13(L_7);
		float L_8 = (&___column00)->get_z_3();
		__this->set_m20_2(L_8);
		float L_9 = (&___column11)->get_z_3();
		__this->set_m21_6(L_9);
		float L_10 = (&___column22)->get_z_3();
		__this->set_m22_10(L_10);
		float L_11 = (&___column33)->get_z_3();
		__this->set_m23_14(L_11);
		float L_12 = (&___column00)->get_w_4();
		__this->set_m30_3(L_12);
		float L_13 = (&___column11)->get_w_4();
		__this->set_m31_7(L_13);
		float L_14 = (&___column22)->get_w_4();
		__this->set_m32_11(L_14);
		float L_15 = (&___column33)->get_w_4();
		__this->set_m33_15(L_15);
		return;
	}
}
extern "C"  void Matrix4x4__ctor_m4089366256_AdjustorThunk (RuntimeObject * __this, Vector4_t1900979187  ___column00, Vector4_t1900979187  ___column11, Vector4_t1900979187  ___column22, Vector4_t1900979187  ___column33, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	Matrix4x4__ctor_m4089366256(_thisAdjusted, ___column00, ___column11, ___column22, ___column33, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t787966842  Matrix4x4_TRS_m3108051110 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___pos0, Quaternion_t3497065016  ___q1, Vector3_t3932393085  ___s2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_TRS_m3108051110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t787966842  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t787966842  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t787966842_il2cpp_TypeInfo_var);
		Matrix4x4_INTERNAL_CALL_TRS_m789796904(NULL /*static, unused*/, (&___pos0), (&___q1), (&___s2), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t787966842  L_0 = V_0;
		V_1 = L_0;
		goto IL_0015;
	}

IL_0015:
	{
		Matrix4x4_t787966842  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m789796904 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___pos0, Quaternion_t3497065016 * ___q1, Vector3_t3932393085 * ___s2, Matrix4x4_t787966842 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_TRS_m789796904_ftn) (Vector3_t3932393085 *, Quaternion_t3497065016 *, Vector3_t3932393085 *, Matrix4x4_t787966842 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m789796904_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m789796904_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___pos0, ___q1, ___s2, ___value3);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m894208093 (Matrix4x4_t787966842 * __this, int32_t ___row0, int32_t ___column1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = Matrix4x4_get_Item_m4189649815(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		float L_3 = V_0;
		return L_3;
	}
}
extern "C"  float Matrix4x4_get_Item_m894208093_AdjustorThunk (RuntimeObject * __this, int32_t ___row0, int32_t ___column1, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_get_Item_m894208093(_thisAdjusted, ___row0, ___column1, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m2958094017 (Matrix4x4_t787966842 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = ___value2;
		Matrix4x4_set_Item_m1436863399(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m2958094017_AdjustorThunk (RuntimeObject * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	Matrix4x4_set_Item_m2958094017(_thisAdjusted, ___row0, ___column1, ___value2, method);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m4189649815 (Matrix4x4_t787966842 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_Item_m4189649815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_004c;
			}
			case 1:
			{
				goto IL_0058;
			}
			case 2:
			{
				goto IL_0064;
			}
			case 3:
			{
				goto IL_0070;
			}
			case 4:
			{
				goto IL_007c;
			}
			case 5:
			{
				goto IL_0088;
			}
			case 6:
			{
				goto IL_0094;
			}
			case 7:
			{
				goto IL_00a0;
			}
			case 8:
			{
				goto IL_00ac;
			}
			case 9:
			{
				goto IL_00b8;
			}
			case 10:
			{
				goto IL_00c4;
			}
			case 11:
			{
				goto IL_00d0;
			}
			case 12:
			{
				goto IL_00dc;
			}
			case 13:
			{
				goto IL_00e8;
			}
			case 14:
			{
				goto IL_00f4;
			}
			case 15:
			{
				goto IL_0100;
			}
		}
	}
	{
		goto IL_010c;
	}

IL_004c:
	{
		float L_1 = __this->get_m00_0();
		V_0 = L_1;
		goto IL_0117;
	}

IL_0058:
	{
		float L_2 = __this->get_m10_1();
		V_0 = L_2;
		goto IL_0117;
	}

IL_0064:
	{
		float L_3 = __this->get_m20_2();
		V_0 = L_3;
		goto IL_0117;
	}

IL_0070:
	{
		float L_4 = __this->get_m30_3();
		V_0 = L_4;
		goto IL_0117;
	}

IL_007c:
	{
		float L_5 = __this->get_m01_4();
		V_0 = L_5;
		goto IL_0117;
	}

IL_0088:
	{
		float L_6 = __this->get_m11_5();
		V_0 = L_6;
		goto IL_0117;
	}

IL_0094:
	{
		float L_7 = __this->get_m21_6();
		V_0 = L_7;
		goto IL_0117;
	}

IL_00a0:
	{
		float L_8 = __this->get_m31_7();
		V_0 = L_8;
		goto IL_0117;
	}

IL_00ac:
	{
		float L_9 = __this->get_m02_8();
		V_0 = L_9;
		goto IL_0117;
	}

IL_00b8:
	{
		float L_10 = __this->get_m12_9();
		V_0 = L_10;
		goto IL_0117;
	}

IL_00c4:
	{
		float L_11 = __this->get_m22_10();
		V_0 = L_11;
		goto IL_0117;
	}

IL_00d0:
	{
		float L_12 = __this->get_m32_11();
		V_0 = L_12;
		goto IL_0117;
	}

IL_00dc:
	{
		float L_13 = __this->get_m03_12();
		V_0 = L_13;
		goto IL_0117;
	}

IL_00e8:
	{
		float L_14 = __this->get_m13_13();
		V_0 = L_14;
		goto IL_0117;
	}

IL_00f4:
	{
		float L_15 = __this->get_m23_14();
		V_0 = L_15;
		goto IL_0117;
	}

IL_0100:
	{
		float L_16 = __this->get_m33_15();
		V_0 = L_16;
		goto IL_0117;
	}

IL_010c:
	{
		IndexOutOfRangeException_t3659678378 * L_17 = (IndexOutOfRangeException_t3659678378 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m4093382970(L_17, _stringLiteral736594712, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0117:
	{
		float L_18 = V_0;
		return L_18;
	}
}
extern "C"  float Matrix4x4_get_Item_m4189649815_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_get_Item_m4189649815(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1436863399 (Matrix4x4_t787966842 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_set_Item_m1436863399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_004c;
			}
			case 1:
			{
				goto IL_0058;
			}
			case 2:
			{
				goto IL_0064;
			}
			case 3:
			{
				goto IL_0070;
			}
			case 4:
			{
				goto IL_007c;
			}
			case 5:
			{
				goto IL_0088;
			}
			case 6:
			{
				goto IL_0094;
			}
			case 7:
			{
				goto IL_00a0;
			}
			case 8:
			{
				goto IL_00ac;
			}
			case 9:
			{
				goto IL_00b8;
			}
			case 10:
			{
				goto IL_00c4;
			}
			case 11:
			{
				goto IL_00d0;
			}
			case 12:
			{
				goto IL_00dc;
			}
			case 13:
			{
				goto IL_00e8;
			}
			case 14:
			{
				goto IL_00f4;
			}
			case 15:
			{
				goto IL_0100;
			}
		}
	}
	{
		goto IL_010c;
	}

IL_004c:
	{
		float L_1 = ___value1;
		__this->set_m00_0(L_1);
		goto IL_0117;
	}

IL_0058:
	{
		float L_2 = ___value1;
		__this->set_m10_1(L_2);
		goto IL_0117;
	}

IL_0064:
	{
		float L_3 = ___value1;
		__this->set_m20_2(L_3);
		goto IL_0117;
	}

IL_0070:
	{
		float L_4 = ___value1;
		__this->set_m30_3(L_4);
		goto IL_0117;
	}

IL_007c:
	{
		float L_5 = ___value1;
		__this->set_m01_4(L_5);
		goto IL_0117;
	}

IL_0088:
	{
		float L_6 = ___value1;
		__this->set_m11_5(L_6);
		goto IL_0117;
	}

IL_0094:
	{
		float L_7 = ___value1;
		__this->set_m21_6(L_7);
		goto IL_0117;
	}

IL_00a0:
	{
		float L_8 = ___value1;
		__this->set_m31_7(L_8);
		goto IL_0117;
	}

IL_00ac:
	{
		float L_9 = ___value1;
		__this->set_m02_8(L_9);
		goto IL_0117;
	}

IL_00b8:
	{
		float L_10 = ___value1;
		__this->set_m12_9(L_10);
		goto IL_0117;
	}

IL_00c4:
	{
		float L_11 = ___value1;
		__this->set_m22_10(L_11);
		goto IL_0117;
	}

IL_00d0:
	{
		float L_12 = ___value1;
		__this->set_m32_11(L_12);
		goto IL_0117;
	}

IL_00dc:
	{
		float L_13 = ___value1;
		__this->set_m03_12(L_13);
		goto IL_0117;
	}

IL_00e8:
	{
		float L_14 = ___value1;
		__this->set_m13_13(L_14);
		goto IL_0117;
	}

IL_00f4:
	{
		float L_15 = ___value1;
		__this->set_m23_14(L_15);
		goto IL_0117;
	}

IL_0100:
	{
		float L_16 = ___value1;
		__this->set_m33_15(L_16);
		goto IL_0117;
	}

IL_010c:
	{
		IndexOutOfRangeException_t3659678378 * L_17 = (IndexOutOfRangeException_t3659678378 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m4093382970(L_17, _stringLiteral736594712, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0117:
	{
		return;
	}
}
extern "C"  void Matrix4x4_set_Item_m1436863399_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	Matrix4x4_set_Item_m1436863399(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m3151071673 (Matrix4x4_t787966842 * __this, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t1900979187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t1900979187  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t1900979187  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	{
		Vector4_t1900979187  L_0 = Matrix4x4_GetColumn_m1725584070(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m2425648894((&V_0), /*hidden argument*/NULL);
		Vector4_t1900979187  L_2 = Matrix4x4_GetColumn_m1725584070(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m2425648894((&V_1), /*hidden argument*/NULL);
		Vector4_t1900979187  L_4 = Matrix4x4_GetColumn_m1725584070(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m2425648894((&V_2), /*hidden argument*/NULL);
		Vector4_t1900979187  L_6 = Matrix4x4_GetColumn_m1725584070(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m2425648894((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0065;
	}

IL_0065:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Matrix4x4_GetHashCode_m3151071673_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_GetHashCode_m3151071673(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern "C"  bool Matrix4x4_Equals_m4284243969 (Matrix4x4_t787966842 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_Equals_m4284243969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Matrix4x4_t787966842  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t1900979187  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t1900979187  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t1900979187  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector4_t1900979187  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Matrix4x4_t787966842_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_00bc;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Matrix4x4_t787966842 *)((Matrix4x4_t787966842 *)UnBox(L_1, Matrix4x4_t787966842_il2cpp_TypeInfo_var))));
		Vector4_t1900979187  L_2 = Matrix4x4_GetColumn_m1725584070(__this, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector4_t1900979187  L_3 = Matrix4x4_GetColumn_m1725584070((&V_1), 0, /*hidden argument*/NULL);
		Vector4_t1900979187  L_4 = L_3;
		RuntimeObject * L_5 = Box(Vector4_t1900979187_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m308574669((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t1900979187  L_7 = Matrix4x4_GetColumn_m1725584070(__this, 1, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector4_t1900979187  L_8 = Matrix4x4_GetColumn_m1725584070((&V_1), 1, /*hidden argument*/NULL);
		Vector4_t1900979187  L_9 = L_8;
		RuntimeObject * L_10 = Box(Vector4_t1900979187_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m308574669((&V_3), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t1900979187  L_12 = Matrix4x4_GetColumn_m1725584070(__this, 2, /*hidden argument*/NULL);
		V_4 = L_12;
		Vector4_t1900979187  L_13 = Matrix4x4_GetColumn_m1725584070((&V_1), 2, /*hidden argument*/NULL);
		Vector4_t1900979187  L_14 = L_13;
		RuntimeObject * L_15 = Box(Vector4_t1900979187_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m308574669((&V_4), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b5;
		}
	}
	{
		Vector4_t1900979187  L_17 = Matrix4x4_GetColumn_m1725584070(__this, 3, /*hidden argument*/NULL);
		V_5 = L_17;
		Vector4_t1900979187  L_18 = Matrix4x4_GetColumn_m1725584070((&V_1), 3, /*hidden argument*/NULL);
		Vector4_t1900979187  L_19 = L_18;
		RuntimeObject * L_20 = Box(Vector4_t1900979187_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m308574669((&V_5), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B7_0 = 0;
	}

IL_00b6:
	{
		V_0 = (bool)G_B7_0;
		goto IL_00bc;
	}

IL_00bc:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
extern "C"  bool Matrix4x4_Equals_m4284243969_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_Equals_m4284243969(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t1900979187  Matrix4x4_GetColumn_m1725584070 (Matrix4x4_t787966842 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_GetColumn_m1725584070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_0085;
			}
		}
	}
	{
		goto IL_00a8;
	}

IL_001c:
	{
		float L_1 = __this->get_m00_0();
		float L_2 = __this->get_m10_1();
		float L_3 = __this->get_m20_2();
		float L_4 = __this->get_m30_3();
		Vector4_t1900979187  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m898810465((&L_5), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_00b3;
	}

IL_003f:
	{
		float L_6 = __this->get_m01_4();
		float L_7 = __this->get_m11_5();
		float L_8 = __this->get_m21_6();
		float L_9 = __this->get_m31_7();
		Vector4_t1900979187  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector4__ctor_m898810465((&L_10), L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_00b3;
	}

IL_0062:
	{
		float L_11 = __this->get_m02_8();
		float L_12 = __this->get_m12_9();
		float L_13 = __this->get_m22_10();
		float L_14 = __this->get_m32_11();
		Vector4_t1900979187  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector4__ctor_m898810465((&L_15), L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_00b3;
	}

IL_0085:
	{
		float L_16 = __this->get_m03_12();
		float L_17 = __this->get_m13_13();
		float L_18 = __this->get_m23_14();
		float L_19 = __this->get_m33_15();
		Vector4_t1900979187  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m898810465((&L_20), L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_00b3;
	}

IL_00a8:
	{
		IndexOutOfRangeException_t3659678378 * L_21 = (IndexOutOfRangeException_t3659678378 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m4093382970(L_21, _stringLiteral2565397915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00b3:
	{
		Vector4_t1900979187  L_22 = V_0;
		return L_22;
	}
}
extern "C"  Vector4_t1900979187  Matrix4x4_GetColumn_m1725584070_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_GetColumn_m1725584070(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m1898437273 (Matrix4x4_t787966842 * __this, int32_t ___index0, Vector4_t1900979187  ___column1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		float L_1 = (&___column1)->get_x_1();
		Matrix4x4_set_Item_m2958094017(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___index0;
		float L_3 = (&___column1)->get_y_2();
		Matrix4x4_set_Item_m2958094017(__this, 1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___index0;
		float L_5 = (&___column1)->get_z_3();
		Matrix4x4_set_Item_m2958094017(__this, 2, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___index0;
		float L_7 = (&___column1)->get_w_4();
		Matrix4x4_set_Item_m2958094017(__this, 3, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Matrix4x4_SetColumn_m1898437273_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, Vector4_t1900979187  ___column1, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	Matrix4x4_SetColumn_m1898437273(_thisAdjusted, ___index0, ___column1, method);
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Matrix4x4_MultiplyPoint3x4_m800395395 (Matrix4x4_t787966842 * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = __this->get_m00_0();
		float L_1 = (&___point0)->get_x_1();
		float L_2 = __this->get_m01_4();
		float L_3 = (&___point0)->get_y_2();
		float L_4 = __this->get_m02_8();
		float L_5 = (&___point0)->get_z_3();
		float L_6 = __this->get_m03_12();
		(&V_0)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6)));
		float L_7 = __this->get_m10_1();
		float L_8 = (&___point0)->get_x_1();
		float L_9 = __this->get_m11_5();
		float L_10 = (&___point0)->get_y_2();
		float L_11 = __this->get_m12_9();
		float L_12 = (&___point0)->get_z_3();
		float L_13 = __this->get_m13_13();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13)));
		float L_14 = __this->get_m20_2();
		float L_15 = (&___point0)->get_x_1();
		float L_16 = __this->get_m21_6();
		float L_17 = (&___point0)->get_y_2();
		float L_18 = __this->get_m22_10();
		float L_19 = (&___point0)->get_z_3();
		float L_20 = __this->get_m23_14();
		(&V_0)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20)));
		Vector3_t3932393085  L_21 = V_0;
		V_1 = L_21;
		goto IL_00b6;
	}

IL_00b6:
	{
		Vector3_t3932393085  L_22 = V_1;
		return L_22;
	}
}
extern "C"  Vector3_t3932393085  Matrix4x4_MultiplyPoint3x4_m800395395_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_MultiplyPoint3x4_m800395395(_thisAdjusted, ___point0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern "C"  Matrix4x4_t787966842  Matrix4x4_get_identity_m579580882 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_identity_m579580882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t787966842  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t787966842_il2cpp_TypeInfo_var);
		Matrix4x4_t787966842  L_0 = ((Matrix4x4_t787966842_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t787966842_il2cpp_TypeInfo_var))->get_identityMatrix_17();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Matrix4x4_t787966842  L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Matrix4x4::ToString()
extern "C"  String_t* Matrix4x4_ToString_m151622620 (Matrix4x4_t787966842 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_ToString_m151622620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		float L_1 = __this->get_m00_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		float L_5 = __this->get_m01_4();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		float L_9 = __this->get_m02_8();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3385344125* L_12 = L_8;
		float L_13 = __this->get_m03_12();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3385344125* L_16 = L_12;
		float L_17 = __this->get_m10_1();
		float L_18 = L_17;
		RuntimeObject * L_19 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_19);
		ObjectU5BU5D_t3385344125* L_20 = L_16;
		float L_21 = __this->get_m11_5();
		float L_22 = L_21;
		RuntimeObject * L_23 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_23);
		ObjectU5BU5D_t3385344125* L_24 = L_20;
		float L_25 = __this->get_m12_9();
		float L_26 = L_25;
		RuntimeObject * L_27 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_27);
		ObjectU5BU5D_t3385344125* L_28 = L_24;
		float L_29 = __this->get_m13_13();
		float L_30 = L_29;
		RuntimeObject * L_31 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_31);
		ObjectU5BU5D_t3385344125* L_32 = L_28;
		float L_33 = __this->get_m20_2();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_35);
		ObjectU5BU5D_t3385344125* L_36 = L_32;
		float L_37 = __this->get_m21_6();
		float L_38 = L_37;
		RuntimeObject * L_39 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_39);
		ObjectU5BU5D_t3385344125* L_40 = L_36;
		float L_41 = __this->get_m22_10();
		float L_42 = L_41;
		RuntimeObject * L_43 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_43);
		ObjectU5BU5D_t3385344125* L_44 = L_40;
		float L_45 = __this->get_m23_14();
		float L_46 = L_45;
		RuntimeObject * L_47 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_47);
		ObjectU5BU5D_t3385344125* L_48 = L_44;
		float L_49 = __this->get_m30_3();
		float L_50 = L_49;
		RuntimeObject * L_51 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_51);
		ObjectU5BU5D_t3385344125* L_52 = L_48;
		float L_53 = __this->get_m31_7();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_55);
		ObjectU5BU5D_t3385344125* L_56 = L_52;
		float L_57 = __this->get_m32_11();
		float L_58 = L_57;
		RuntimeObject * L_59 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_59);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_59);
		ObjectU5BU5D_t3385344125* L_60 = L_56;
		float L_61 = __this->get_m33_15();
		float L_62 = L_61;
		RuntimeObject * L_63 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_63);
		String_t* L_64 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral1508432694, L_60, /*hidden argument*/NULL);
		V_0 = L_64;
		goto IL_00ff;
	}

IL_00ff:
	{
		String_t* L_65 = V_0;
		return L_65;
	}
}
extern "C"  String_t* Matrix4x4_ToString_m151622620_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Matrix4x4_t787966842 * _thisAdjusted = reinterpret_cast<Matrix4x4_t787966842 *>(__this + 1);
	return Matrix4x4_ToString_m151622620(_thisAdjusted, method);
}
// System.Void UnityEngine.Matrix4x4::.cctor()
extern "C"  void Matrix4x4__cctor_m1789245472 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4__cctor_m1789245472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t1900979187  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m898810465((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t1900979187  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m898810465((&L_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t1900979187  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m898810465((&L_2), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t1900979187  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m898810465((&L_3), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Matrix4x4_t787966842  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Matrix4x4__ctor_m4089366256((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		((Matrix4x4_t787966842_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t787966842_il2cpp_TypeInfo_var))->set_zeroMatrix_16(L_4);
		Vector4_t1900979187  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m898810465((&L_5), (1.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t1900979187  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector4__ctor_m898810465((&L_6), (0.0f), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t1900979187  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector4__ctor_m898810465((&L_7), (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector4_t1900979187  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m898810465((&L_8), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t787966842  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Matrix4x4__ctor_m4089366256((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		((Matrix4x4_t787966842_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_t787966842_il2cpp_TypeInfo_var))->set_identityMatrix_17(L_9);
		return;
	}
}
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m2346141194 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh__ctor_m2346141194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m4004593593(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m4004593593 (RuntimeObject * __this /* static, unused */, Mesh_t4237566844 * ___mono0, const RuntimeMethod* method)
{
	typedef void (*Mesh_Internal_Create_m4004593593_ftn) (Mesh_t4237566844 *);
	static Mesh_Internal_Create_m4004593593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m4004593593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetArrayForChannelImpl_m4170069227 (Mesh_t4237566844 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, RuntimeArray * ___values3, int32_t ___arraySize4, const RuntimeMethod* method)
{
	typedef void (*Mesh_SetArrayForChannelImpl_m4170069227_ftn) (Mesh_t4237566844 *, int32_t, int32_t, int32_t, RuntimeArray *, int32_t);
	static Mesh_SetArrayForChannelImpl_m4170069227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetArrayForChannelImpl_m4170069227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)");
	_il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2, ___values3, ___arraySize4);
}
// System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  RuntimeArray * Mesh_GetAllocArrayFromChannelImpl_m2146056978 (Mesh_t4237566844 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*Mesh_GetAllocArrayFromChannelImpl_m2146056978_ftn) (Mesh_t4237566844 *, int32_t, int32_t, int32_t);
	static Mesh_GetAllocArrayFromChannelImpl_m2146056978_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetAllocArrayFromChannelImpl_m2146056978_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)");
	RuntimeArray * retVal = _il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2);
	return retVal;
}
// System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  bool Mesh_HasChannel_m2687655067 (Mesh_t4237566844 * __this, int32_t ___channel0, const RuntimeMethod* method)
{
	typedef bool (*Mesh_HasChannel_m2687655067_ftn) (Mesh_t4237566844 *, int32_t);
	static Mesh_HasChannel_m2687655067_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_HasChannel_m2687655067_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)");
	bool retVal = _il2cpp_icall_func(__this, ___channel0);
	return retVal;
}
// System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
extern "C"  RuntimeArray * Mesh_ExtractArrayFromList_m2973470114 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___list0, const RuntimeMethod* method)
{
	typedef RuntimeArray * (*Mesh_ExtractArrayFromList_m2973470114_ftn) (RuntimeObject *);
	static Mesh_ExtractArrayFromList_m2973470114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_ExtractArrayFromList_m2973470114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::ExtractArrayFromList(System.Object)");
	RuntimeArray * retVal = _il2cpp_icall_func(___list0);
	return retVal;
}
// System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
extern "C"  Int32U5BU5D_t3215173111* Mesh_GetIndicesImpl_m2513887997 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	typedef Int32U5BU5D_t3215173111* (*Mesh_GetIndicesImpl_m2513887997_ftn) (Mesh_t4237566844 *, int32_t);
	static Mesh_GetIndicesImpl_m2513887997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetIndicesImpl_m2513887997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetIndicesImpl(System.Int32)");
	Int32U5BU5D_t3215173111* retVal = _il2cpp_icall_func(__this, ___submesh0);
	return retVal;
}
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTrianglesImpl_m3429015926 (Mesh_t4237566844 * __this, int32_t ___submesh0, RuntimeArray * ___triangles1, int32_t ___arraySize2, bool ___calculateBounds3, const RuntimeMethod* method)
{
	typedef void (*Mesh_SetTrianglesImpl_m3429015926_ftn) (Mesh_t4237566844 *, int32_t, RuntimeArray *, int32_t, bool);
	static Mesh_SetTrianglesImpl_m3429015926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTrianglesImpl_m3429015926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___submesh0, ___triangles1, ___arraySize2, ___calculateBounds3);
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void Mesh_SetTriangles_m424882913 (Mesh_t4237566844 * __this, List_1_t894813333 * ___triangles0, int32_t ___submesh1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		List_1_t894813333 * L_0 = ___triangles0;
		int32_t L_1 = ___submesh1;
		bool L_2 = V_0;
		Mesh_SetTriangles_m879784383(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTriangles_m879784383 (Mesh_t4237566844 * __this, List_1_t894813333 * ___triangles0, int32_t ___submesh1, bool ___calculateBounds2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetTriangles_m879784383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___submesh1;
		bool L_1 = Mesh_CheckCanAccessSubmeshTriangles_m3405578695(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = ___submesh1;
		List_1_t894813333 * L_3 = ___triangles0;
		RuntimeArray * L_4 = Mesh_ExtractArrayFromList_m2973470114(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		List_1_t894813333 * L_5 = ___triangles0;
		int32_t L_6 = Mesh_SafeLength_TisInt32_t3515577538_m12458220(__this, L_5, /*hidden argument*/Mesh_SafeLength_TisInt32_t3515577538_m12458220_RuntimeMethod_var);
		bool L_7 = ___calculateBounds2;
		Mesh_SetTrianglesImpl_m3429015926(__this, L_2, L_4, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m3443663181 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	typedef bool (*Mesh_get_canAccess_m3443663181_ftn) (Mesh_t4237566844 *);
	static Mesh_get_canAccess_m3443663181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_canAccess_m3443663181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_canAccess()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m2145839308 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Mesh_get_subMeshCount_m2145839308_ftn) (Mesh_t4237566844 *);
	static Mesh_get_subMeshCount_m2145839308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_subMeshCount_m2145839308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_subMeshCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
extern "C"  void Mesh_ClearImpl_m264595268 (Mesh_t4237566844 * __this, bool ___keepVertexLayout0, const RuntimeMethod* method)
{
	typedef void (*Mesh_ClearImpl_m264595268_ftn) (Mesh_t4237566844 *, bool);
	static Mesh_ClearImpl_m264595268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_ClearImpl_m264595268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::ClearImpl(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout0);
}
// System.Void UnityEngine.Mesh::RecalculateBoundsImpl()
extern "C"  void Mesh_RecalculateBoundsImpl_m2101905381 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	typedef void (*Mesh_RecalculateBoundsImpl_m2101905381_ftn) (Mesh_t4237566844 *);
	static Mesh_RecalculateBoundsImpl_m2101905381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateBoundsImpl_m2101905381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateBoundsImpl()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessChannel_m3291228881 (Mesh_t4237566844 * __this, int32_t ___ch0, const RuntimeMethod* method)
{
	typedef void (*Mesh_PrintErrorCantAccessChannel_m3291228881_ftn) (Mesh_t4237566844 *, int32_t);
	static Mesh_PrintErrorCantAccessChannel_m3291228881_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorCantAccessChannel_m3291228881_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Mesh/InternalShaderChannel)");
	_il2cpp_icall_func(__this, ___ch0);
}
// UnityEngine.Mesh/InternalShaderChannel UnityEngine.Mesh::GetUVChannel(System.Int32)
extern "C"  int32_t Mesh_GetUVChannel_m1891453954 (Mesh_t4237566844 * __this, int32_t ___uvIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_GetUVChannel_m1891453954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___uvIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___uvIndex0;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_001f;
		}
	}

IL_000f:
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_2, _stringLiteral2159627391, _stringLiteral2343396546, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		int32_t L_3 = ___uvIndex0;
		V_0 = ((int32_t)((int32_t)3+(int32_t)L_3));
		goto IL_0028;
	}

IL_0028:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  int32_t Mesh_DefaultDimensionForChannel_m405795618 (RuntimeObject * __this /* static, unused */, int32_t ___channel0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_DefaultDimensionForChannel_m405795618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___channel0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___channel0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0015;
		}
	}

IL_000e:
	{
		V_0 = 3;
		goto IL_004f;
	}

IL_0015:
	{
		int32_t L_2 = ___channel0;
		if ((((int32_t)L_2) < ((int32_t)3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_3 = ___channel0;
		if ((((int32_t)L_3) > ((int32_t)6)))
		{
			goto IL_002a;
		}
	}
	{
		V_0 = 2;
		goto IL_004f;
	}

IL_002a:
	{
		int32_t L_4 = ___channel0;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_5 = ___channel0;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003f;
		}
	}

IL_0038:
	{
		V_0 = 4;
		goto IL_004f;
	}

IL_003f:
	{
		ArgumentException_t489606696 * L_6 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_6, _stringLiteral553365222, _stringLiteral2518796331, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_004f:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetSizedArrayForChannel_m3982906445 (Mesh_t4237566844 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, RuntimeArray * ___values3, int32_t ___valuesCount4, const RuntimeMethod* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m3443663181(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = ___channel0;
		int32_t L_2 = ___format1;
		int32_t L_3 = ___dim2;
		RuntimeArray * L_4 = ___values3;
		int32_t L_5 = ___valuesCount4;
		Mesh_SetArrayForChannelImpl_m4170069227(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001e:
	{
		int32_t L_6 = ___channel0;
		Mesh_PrintErrorCantAccessChannel_m3291228881(__this, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1432878832* Mesh_get_vertices_m1024139665 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_vertices_m1024139665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1432878832* V_0 = NULL;
	{
		Vector3U5BU5D_t1432878832* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823(__this, 0, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector3U5BU5D_t1432878832* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t1432878832* Mesh_get_normals_m3412584569 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_normals_m3412584569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1432878832* V_0 = NULL;
	{
		Vector3U5BU5D_t1432878832* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823(__this, 1, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t3932393085_m94624823_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector3U5BU5D_t1432878832* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern "C"  Vector4U5BU5D_t1632896738* Mesh_get_tangents_m8030836 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_tangents_m8030836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4U5BU5D_t1632896738* V_0 = NULL;
	{
		Vector4U5BU5D_t1632896738* L_0 = Mesh_GetAllocArrayFromChannel_TisVector4_t1900979187_m1344632441(__this, 7, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector4_t1900979187_m1344632441_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector4U5BU5D_t1632896738* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C"  Vector2U5BU5D_t3868613195* Mesh_get_uv_m644261647 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv_m644261647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t3868613195* V_0 = NULL;
	{
		Vector2U5BU5D_t3868613195* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439(__this, 3, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t3868613195* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern "C"  Vector2U5BU5D_t3868613195* Mesh_get_uv2_m2611581962 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv2_m2611581962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t3868613195* V_0 = NULL;
	{
		Vector2U5BU5D_t3868613195* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439(__this, 4, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t3868613195* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
extern "C"  Vector2U5BU5D_t3868613195* Mesh_get_uv3_m113880564 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv3_m113880564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t3868613195* V_0 = NULL;
	{
		Vector2U5BU5D_t3868613195* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439(__this, 5, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t3868613195* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
extern "C"  Vector2U5BU5D_t3868613195* Mesh_get_uv4_m1968302483 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv4_m1968302483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t3868613195* V_0 = NULL;
	{
		Vector2U5BU5D_t3868613195* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439(__this, 6, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2246369278_m3814627439_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Vector2U5BU5D_t3868613195* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern "C"  Color32U5BU5D_t2352998046* Mesh_get_colors32_m37522156 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_colors32_m37522156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32U5BU5D_t2352998046* V_0 = NULL;
	{
		Color32U5BU5D_t2352998046* L_0 = Mesh_GetAllocArrayFromChannel_TisColor32_t662424039_m1356534377(__this, 2, 2, 1, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisColor32_t662424039_m1356534377_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Color32U5BU5D_t2352998046* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetVertices_m2880691109 (Mesh_t4237566844 * __this, List_1_t1311628880 * ___inVertices0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetVertices_m2880691109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1311628880 * L_0 = ___inVertices0;
		Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733(__this, 0, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetNormals_m1347736657 (Mesh_t4237566844 * __this, List_1_t1311628880 * ___inNormals0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetNormals_m1347736657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1311628880 * L_0 = ___inNormals0;
		Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733(__this, 1, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector3_t3932393085_m4191470733_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_SetTangents_m1906139770 (Mesh_t4237566844 * __this, List_1_t3575182278 * ___inTangents0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetTangents_m1906139770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3575182278 * L_0 = ___inTangents0;
		Mesh_SetListForChannel_TisVector4_t1900979187_m2034622288(__this, 7, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector4_t1900979187_m2034622288_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern "C"  void Mesh_SetColors_m1083011859 (Mesh_t4237566844 * __this, List_1_t2336627130 * ___inColors0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetColors_m1083011859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2336627130 * L_0 = ___inColors0;
		Mesh_SetListForChannel_TisColor32_t662424039_m1488066680(__this, 2, 2, 1, L_0, /*hidden argument*/Mesh_SetListForChannel_TisColor32_t662424039_m1488066680_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void Mesh_SetUVs_m1031607811 (Mesh_t4237566844 * __this, int32_t ___channel0, List_1_t3920572369 * ___uvs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetUVs_m1031607811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___channel0;
		List_1_t3920572369 * L_1 = ___uvs1;
		Mesh_SetUvsImpl_TisVector2_t2246369278_m3260026371(__this, L_0, 2, L_1, /*hidden argument*/Mesh_SetUvsImpl_TisVector2_t2246369278_m3260026371_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessIndices()
extern "C"  void Mesh_PrintErrorCantAccessIndices_m3788685066 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_PrintErrorCantAccessIndices_m3788685066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Object_get_name_m527277425(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2251207222(NULL /*static, unused*/, _stringLiteral2489973965, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogError_m2611403583(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern "C"  bool Mesh_CheckCanAccessSubmesh_m2652095279 (Mesh_t4237566844 * __this, int32_t ___submesh0, bool ___errorAboutTriangles1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_CheckCanAccessSubmesh_m2652095279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B6_0 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	{
		bool L_0 = Mesh_get_canAccess_m3443663181(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		Mesh_PrintErrorCantAccessIndices_m3788685066(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_0061;
	}

IL_001a:
	{
		int32_t L_1 = ___submesh0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_2 = ___submesh0;
		int32_t L_3 = Mesh_get_subMeshCount_m2145839308(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_005a;
		}
	}

IL_002d:
	{
		bool L_4 = ___errorAboutTriangles1;
		G_B5_0 = _stringLiteral2881092165;
		if (!L_4)
		{
			G_B6_0 = _stringLiteral2881092165;
			goto IL_0043;
		}
	}
	{
		G_B7_0 = _stringLiteral3105790013;
		G_B7_1 = G_B5_0;
		goto IL_0048;
	}

IL_0043:
	{
		G_B7_0 = _stringLiteral626827535;
		G_B7_1 = G_B6_0;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2251207222(NULL /*static, unused*/, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogError_m2528395896(NULL /*static, unused*/, L_5, __this, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_0061;
	}

IL_005a:
	{
		V_0 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshTriangles_m3405578695 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmesh_m2652095279(__this, L_0, (bool)1, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshIndices_m1413433297 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmesh_m2652095279(__this, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern "C"  Int32U5BU5D_t3215173111* Mesh_GetIndices_m2848004958 (Mesh_t4237566844 * __this, int32_t ___submesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_GetIndices_m2848004958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3215173111* V_0 = NULL;
	Int32U5BU5D_t3215173111* G_B3_0 = NULL;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmeshIndices_m1413433297(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = ___submesh0;
		Int32U5BU5D_t3215173111* L_3 = Mesh_GetIndicesImpl_m2513887997(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_001f;
	}

IL_0019:
	{
		G_B3_0 = ((Int32U5BU5D_t3215173111*)SZArrayNew(Int32U5BU5D_t3215173111_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_001f:
	{
		V_0 = G_B3_0;
		goto IL_0025;
	}

IL_0025:
	{
		Int32U5BU5D_t3215173111* L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m4176655358 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	{
		Mesh_ClearImpl_m264595268(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m3942591469 (Mesh_t4237566844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mesh_RecalculateBounds_m3942591469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Mesh_get_canAccess_m3443663181(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Mesh_RecalculateBoundsImpl_m2101905381(__this, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_0017:
	{
		String_t* L_1 = Object_get_name_m527277425(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2251207222(NULL /*static, unused*/, _stringLiteral2636767007, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogError_m2611403583(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m3966010194 (MonoBehaviour_t4008345588 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m1626382119(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t260305101 * MonoBehaviour_StartCoroutine_m880782023 (MonoBehaviour_t4008345588 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	Coroutine_t260305101 * V_0 = NULL;
	{
		RuntimeObject* L_0 = ___routine0;
		Coroutine_t260305101 * L_1 = MonoBehaviour_StartCoroutine_Auto_Internal_m927892698(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t260305101 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t260305101 * MonoBehaviour_StartCoroutine_Auto_Internal_m927892698 (MonoBehaviour_t4008345588 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	typedef Coroutine_t260305101 * (*MonoBehaviour_StartCoroutine_Auto_Internal_m927892698_ftn) (MonoBehaviour_t4008345588 *, RuntimeObject*);
	static MonoBehaviour_StartCoroutine_Auto_Internal_m927892698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_Internal_m927892698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)");
	Coroutine_t260305101 * retVal = _il2cpp_icall_func(__this, ___routine0);
	return retVal;
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m764443603 (MonoBehaviour_t4008345588 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m3066659708(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m847468060 (MonoBehaviour_t4008345588 * __this, Coroutine_t260305101 * ___routine0, const RuntimeMethod* method)
{
	{
		Coroutine_t260305101 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m552725154(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m3066659708 (MonoBehaviour_t4008345588 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m3066659708_ftn) (MonoBehaviour_t4008345588 *, RuntimeObject*);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m3066659708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m3066659708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m552725154 (MonoBehaviour_t4008345588 * __this, Coroutine_t260305101 * ___routine0, const RuntimeMethod* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m552725154_ftn) (MonoBehaviour_t4008345588 *, Coroutine_t260305101 *);
	static MonoBehaviour_StopCoroutine_Auto_m552725154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m552725154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.NativeClassAttribute::.ctor(System.String)
extern "C"  void NativeClassAttribute__ctor_m3366348483 (NativeClassAttribute_t417698780 * __this, String_t* ___qualifiedCppName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___qualifiedCppName0;
		NativeClassAttribute_set_QualifiedNativeName_m2734820921(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m2734820921 (NativeClassAttribute_t417698780 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CQualifiedNativeNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m3912586882 (MessageEventArgs_t2772358345 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern "C"  void PlayerConnection__ctor_m1346962482 (PlayerConnection_t3190352615 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection__ctor_m1346962482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerEditorConnectionEvents_t1009044977 * L_0 = (PlayerEditorConnectionEvents_t1009044977 *)il2cpp_codegen_object_new(PlayerEditorConnectionEvents_t1009044977_il2cpp_TypeInfo_var);
		PlayerEditorConnectionEvents__ctor_m2224928885(L_0, /*hidden argument*/NULL);
		__this->set_m_PlayerEditorConnectionEvents_3(L_0);
		List_1_t894813333 * L_1 = (List_1_t894813333 *)il2cpp_codegen_object_new(List_1_t894813333_il2cpp_TypeInfo_var);
		List_1__ctor_m2297325353(L_1, /*hidden argument*/List_1__ctor_m2297325353_RuntimeMethod_var);
		__this->set_m_connectedPlayers_4(L_1);
		ScriptableObject__ctor_m3277333935(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3190352615 * PlayerConnection_get_instance_m2295652940 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_get_instance_m2295652940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t3190352615 * V_0 = NULL;
	{
		PlayerConnection_t3190352615 * L_0 = ((PlayerConnection_t3190352615_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3190352615_il2cpp_TypeInfo_var))->get_s_Instance_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		PlayerConnection_t3190352615 * L_2 = PlayerConnection_CreateInstance_m3049947590(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0028;
	}

IL_001d:
	{
		PlayerConnection_t3190352615 * L_3 = ((PlayerConnection_t3190352615_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3190352615_il2cpp_TypeInfo_var))->get_s_Instance_6();
		V_0 = L_3;
		goto IL_0028;
	}

IL_0028:
	{
		PlayerConnection_t3190352615 * L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::get_isConnected()
extern "C"  bool PlayerConnection_get_isConnected_m2943830747 (PlayerConnection_t3190352615 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_get_isConnected_m2943830747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject* L_0 = PlayerConnection_GetConnectionNativeApi_m2366024494(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean UnityEngine.IPlayerEditorConnectionNative::IsConnected() */, IPlayerEditorConnectionNative_t3906153058_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t3190352615 * PlayerConnection_CreateInstance_m3049947590 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_CreateInstance_m3049947590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t3190352615 * V_0 = NULL;
	{
		PlayerConnection_t3190352615 * L_0 = ScriptableObject_CreateInstance_TisPlayerConnection_t3190352615_m3861853114(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayerConnection_t3190352615_m3861853114_RuntimeMethod_var);
		((PlayerConnection_t3190352615_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3190352615_il2cpp_TypeInfo_var))->set_s_Instance_6(L_0);
		PlayerConnection_t3190352615 * L_1 = ((PlayerConnection_t3190352615_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3190352615_il2cpp_TypeInfo_var))->get_s_Instance_6();
		NullCheck(L_1);
		Object_set_hideFlags_m2254825582(L_1, ((int32_t)61), /*hidden argument*/NULL);
		PlayerConnection_t3190352615 * L_2 = ((PlayerConnection_t3190352615_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3190352615_il2cpp_TypeInfo_var))->get_s_Instance_6();
		V_0 = L_2;
		goto IL_0022;
	}

IL_0022:
	{
		PlayerConnection_t3190352615 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern "C"  RuntimeObject* PlayerConnection_GetConnectionNativeApi_m2366024494 (PlayerConnection_t3190352615 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_GetConnectionNativeApi_m2366024494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* G_B2_0 = NULL;
	RuntimeObject* G_B1_0 = NULL;
	{
		RuntimeObject* L_0 = ((PlayerConnection_t3190352615_StaticFields*)il2cpp_codegen_static_fields_for(PlayerConnection_t3190352615_il2cpp_TypeInfo_var))->get_connectionNative_2();
		RuntimeObject* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		PlayerConnectionInternal_t3959366414 * L_2 = (PlayerConnectionInternal_t3959366414 *)il2cpp_codegen_object_new(PlayerConnectionInternal_t3959366414_il2cpp_TypeInfo_var);
		PlayerConnectionInternal__ctor_m2449099525(L_2, /*hidden argument*/NULL);
		G_B2_0 = ((RuntimeObject*)(L_2));
	}

IL_0012:
	{
		V_0 = G_B2_0;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Register(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern "C"  void PlayerConnection_Register_m3487291422 (PlayerConnection_t3190352615 * __this, Guid_t  ___messageId0, UnityAction_1_t1148388365 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_Register_m3487291422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_1 = ((Guid_t_StaticFields*)il2cpp_codegen_static_fields_for(Guid_t_il2cpp_TypeInfo_var))->get_Empty_11();
		bool L_2 = Guid_op_Equality_m2100028761(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t489606696 * L_3 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_3, _stringLiteral319603750, _stringLiteral2951852423, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		PlayerEditorConnectionEvents_t1009044977 * L_4 = __this->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_4);
		List_1_t2349647505 * L_5 = L_4->get_messageTypeSubscribers_0();
		bool L_6 = Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_0045;
		}
	}
	{
		RuntimeObject* L_7 = PlayerConnection_GetConnectionNativeApi_m2366024494(__this, /*hidden argument*/NULL);
		Guid_t  L_8 = ___messageId0;
		NullCheck(L_7);
		InterfaceActionInvoker1< Guid_t  >::Invoke(2 /* System.Void UnityEngine.IPlayerEditorConnectionNative::RegisterInternal(System.Guid) */, IPlayerEditorConnectionNative_t3906153058_il2cpp_TypeInfo_var, L_7, L_8);
	}

IL_0045:
	{
		PlayerEditorConnectionEvents_t1009044977 * L_9 = __this->get_m_PlayerEditorConnectionEvents_3();
		Guid_t  L_10 = ___messageId0;
		NullCheck(L_9);
		UnityEvent_1_t1459204717 * L_11 = PlayerEditorConnectionEvents_AddAndCreate_m378817323(L_9, L_10, /*hidden argument*/NULL);
		UnityAction_1_t1148388365 * L_12 = ___callback1;
		NullCheck(L_11);
		UnityEvent_1_AddListener_m291452717(L_11, L_12, /*hidden argument*/UnityEvent_1_AddListener_m291452717_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern "C"  void PlayerConnection_RegisterConnection_m1216171267 (PlayerConnection_t3190352615 * __this, UnityAction_1_t1891607558 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_RegisterConnection_m1216171267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t652623003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t894813333 * L_0 = __this->get_m_connectedPlayers_4();
		NullCheck(L_0);
		Enumerator_t652623003  L_1 = List_1_GetEnumerator_m2611892158(L_0, /*hidden argument*/List_1_GetEnumerator_m2611892158_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0013:
		{
			int32_t L_2 = Enumerator_get_Current_m3396935530((&V_1), /*hidden argument*/Enumerator_get_Current_m3396935530_RuntimeMethod_var);
			V_0 = L_2;
			UnityAction_1_t1891607558 * L_3 = ___callback0;
			int32_t L_4 = V_0;
			NullCheck(L_3);
			UnityAction_1_Invoke_m278533989(L_3, L_4, /*hidden argument*/UnityAction_1_Invoke_m278533989_RuntimeMethod_var);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m3554339858((&V_1), /*hidden argument*/Enumerator_MoveNext_m3554339858_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0013;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m933170505((&V_1), /*hidden argument*/Enumerator_Dispose_m933170505_RuntimeMethod_var);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0043:
	{
		PlayerEditorConnectionEvents_t1009044977 * L_6 = __this->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_6);
		ConnectionChangeEvent_t190635484 * L_7 = L_6->get_connectionEvent_1();
		UnityAction_1_t1891607558 * L_8 = ___callback0;
		NullCheck(L_7);
		UnityEvent_1_AddListener_m2986871176(L_7, L_8, /*hidden argument*/UnityEvent_1_AddListener_m2986871176_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern "C"  void PlayerConnection_RegisterDisconnection_m4134895671 (PlayerConnection_t3190352615 * __this, UnityAction_1_t1891607558 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_RegisterDisconnection_m4134895671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerEditorConnectionEvents_t1009044977 * L_0 = __this->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_0);
		ConnectionChangeEvent_t190635484 * L_1 = L_0->get_disconnectionEvent_2();
		UnityAction_1_t1891607558 * L_2 = ___callback0;
		NullCheck(L_1);
		UnityEvent_1_AddListener_m2986871176(L_1, L_2, /*hidden argument*/UnityEvent_1_AddListener_m2986871176_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Send(System.Guid,System.Byte[])
extern "C"  void PlayerConnection_Send_m1533960837 (PlayerConnection_t3190352615 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3567143369* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_Send_m1533960837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_1 = ((Guid_t_StaticFields*)il2cpp_codegen_static_fields_for(Guid_t_il2cpp_TypeInfo_var))->get_Empty_11();
		bool L_2 = Guid_op_Equality_m2100028761(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t489606696 * L_3 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_3, _stringLiteral319603750, _stringLiteral2951852423, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		RuntimeObject* L_4 = PlayerConnection_GetConnectionNativeApi_m2366024494(__this, /*hidden argument*/NULL);
		Guid_t  L_5 = ___messageId0;
		ByteU5BU5D_t3567143369* L_6 = ___data1;
		NullCheck(L_4);
		InterfaceActionInvoker3< Guid_t , ByteU5BU5D_t3567143369*, int32_t >::Invoke(1 /* System.Void UnityEngine.IPlayerEditorConnectionNative::SendMessage(System.Guid,System.Byte[],System.Int32) */, IPlayerEditorConnectionNative_t3906153058_il2cpp_TypeInfo_var, L_4, L_5, L_6, 0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectAll()
extern "C"  void PlayerConnection_DisconnectAll_m1540013891 (PlayerConnection_t3190352615 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_DisconnectAll_m1540013891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = PlayerConnection_GetConnectionNativeApi_m2366024494(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.IPlayerEditorConnectionNative::DisconnectAll() */, IPlayerEditorConnectionNative_t3906153058_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern "C"  void PlayerConnection_MessageCallbackInternal_m194187481 (RuntimeObject * __this /* static, unused */, intptr_t ___data0, uint64_t ___size1, uint64_t ___guid2, String_t* ___messageId3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_MessageCallbackInternal_m194187481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3567143369* V_0 = NULL;
	{
		V_0 = (ByteU5BU5D_t3567143369*)NULL;
		uint64_t L_0 = ___size1;
		if ((!(((uint64_t)L_0) > ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001f;
		}
	}
	{
		uint64_t L_1 = ___size1;
		if ((uint64_t)(L_1) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t3567143369*)SZArrayNew(ByteU5BU5D_t3567143369_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_1))));
		intptr_t L_2 = ___data0;
		ByteU5BU5D_t3567143369* L_3 = V_0;
		uint64_t L_4 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t132568556_il2cpp_TypeInfo_var);
		Marshal_Copy_m3981143475(NULL /*static, unused*/, L_2, L_3, 0, (((int32_t)((int32_t)L_4))), /*hidden argument*/NULL);
	}

IL_001f:
	{
		PlayerConnection_t3190352615 * L_5 = PlayerConnection_get_instance_m2295652940(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		PlayerEditorConnectionEvents_t1009044977 * L_6 = L_5->get_m_PlayerEditorConnectionEvents_3();
		String_t* L_7 = ___messageId3;
		Guid_t  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Guid__ctor_m869444109((&L_8), L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t3567143369* L_9 = V_0;
		uint64_t L_10 = ___guid2;
		NullCheck(L_6);
		PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4106540626(L_6, L_8, L_9, (((int32_t)((int32_t)L_10))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern "C"  void PlayerConnection_ConnectedCallbackInternal_m491598696 (RuntimeObject * __this /* static, unused */, int32_t ___playerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_ConnectedCallbackInternal_m491598696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t3190352615 * L_0 = PlayerConnection_get_instance_m2295652940(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t894813333 * L_1 = L_0->get_m_connectedPlayers_4();
		int32_t L_2 = ___playerId0;
		NullCheck(L_1);
		List_1_Add_m1589466641(L_1, L_2, /*hidden argument*/List_1_Add_m1589466641_RuntimeMethod_var);
		PlayerConnection_t3190352615 * L_3 = PlayerConnection_get_instance_m2295652940(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		PlayerEditorConnectionEvents_t1009044977 * L_4 = L_3->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_4);
		ConnectionChangeEvent_t190635484 * L_5 = L_4->get_connectionEvent_1();
		int32_t L_6 = ___playerId0;
		NullCheck(L_5);
		UnityEvent_1_Invoke_m305340876(L_5, L_6, /*hidden argument*/UnityEvent_1_Invoke_m305340876_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern "C"  void PlayerConnection_DisconnectedCallback_m3386176482 (RuntimeObject * __this /* static, unused */, int32_t ___playerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_DisconnectedCallback_m3386176482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t3190352615 * L_0 = PlayerConnection_get_instance_m2295652940(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayerEditorConnectionEvents_t1009044977 * L_1 = L_0->get_m_PlayerEditorConnectionEvents_3();
		NullCheck(L_1);
		ConnectionChangeEvent_t190635484 * L_2 = L_1->get_disconnectionEvent_2();
		int32_t L_3 = ___playerId0;
		NullCheck(L_2);
		UnityEvent_1_Invoke_m305340876(L_2, L_3, /*hidden argument*/UnityEvent_1_Invoke_m305340876_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
