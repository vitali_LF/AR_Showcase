﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t3515577538_0_0_0;
extern const Il2CppType Char_t1622636488_0_0_0;
extern const Il2CppType Int64_t3603203610_0_0_0;
extern const Il2CppType UInt32_t1085449242_0_0_0;
extern const Il2CppType UInt64_t462540778_0_0_0;
extern const Il2CppType Byte_t2640180184_0_0_0;
extern const Il2CppType SByte_t2299796046_0_0_0;
extern const Il2CppType Int16_t2935336495_0_0_0;
extern const Il2CppType UInt16_t3430003764_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t1386358354_0_0_0;
extern const Il2CppType IComparable_t3923264286_0_0_0;
extern const Il2CppType IEnumerable_t307620497_0_0_0;
extern const Il2CppType ICloneable_t3227696483_0_0_0;
extern const Il2CppType IComparable_1_t1965733923_0_0_0;
extern const Il2CppType IEquatable_1_t733774393_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t371090060_0_0_0;
extern const Il2CppType _Type_t3580695744_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2532673682_0_0_0;
extern const Il2CppType _MemberInfo_t1879838719_0_0_0;
extern const Il2CppType Double_t3942828892_0_0_0;
extern const Il2CppType Single_t897798503_0_0_0;
extern const Il2CppType Decimal_t2641346350_0_0_0;
extern const Il2CppType Boolean_t2440219994_0_0_0;
extern const Il2CppType Delegate_t4015201940_0_0_0;
extern const Il2CppType ISerializable_t3561356945_0_0_0;
extern const Il2CppType ParameterInfo_t3156340899_0_0_0;
extern const Il2CppType _ParameterInfo_t2470192644_0_0_0;
extern const Il2CppType ParameterModifier_t1933193809_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t2737396514_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t3421021404_0_0_0;
extern const Il2CppType MethodBase_t3670318294_0_0_0;
extern const Il2CppType _MethodBase_t1498739407_0_0_0;
extern const Il2CppType ConstructorInfo_t3011499035_0_0_0;
extern const Il2CppType _ConstructorInfo_t2887962050_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t739736251_0_0_0;
extern const Il2CppType TailoringInfo_t1494168405_0_0_0;
extern const Il2CppType KeyValuePair_2_t178173863_0_0_0;
extern const Il2CppType Link_t2678926947_0_0_0;
extern const Il2CppType DictionaryEntry_t3507565976_0_0_0;
extern const Il2CppType KeyValuePair_2_t2676903416_0_0_0;
extern const Il2CppType Contraction_t2685614877_0_0_0;
extern const Il2CppType Level2Map_t2701131617_0_0_0;
extern const Il2CppType BigInteger_t1843659260_0_0_0;
extern const Il2CppType KeySizes_t3274741244_0_0_0;
extern const Il2CppType KeyValuePair_2_t1509590809_0_0_0;
extern const Il2CppType Slot_t1231688804_0_0_0;
extern const Il2CppType Slot_t697738515_0_0_0;
extern const Il2CppType StackFrame_t3907017807_0_0_0;
extern const Il2CppType Calendar_t1639743412_0_0_0;
extern const Il2CppType ModuleBuilder_t2525481856_0_0_0;
extern const Il2CppType _ModuleBuilder_t1154404907_0_0_0;
extern const Il2CppType Module_t575570506_0_0_0;
extern const Il2CppType _Module_t1610937700_0_0_0;
extern const Il2CppType CustomAttributeBuilder_t1604210647_0_0_0;
extern const Il2CppType _CustomAttributeBuilder_t2558201917_0_0_0;
extern const Il2CppType MonoResource_t2877269471_0_0_0;
extern const Il2CppType MonoWin32Resource_t2032464017_0_0_0;
extern const Il2CppType RefEmitPermissionSet_t4262511222_0_0_0;
extern const Il2CppType ParameterBuilder_t4081109430_0_0_0;
extern const Il2CppType _ParameterBuilder_t4215768685_0_0_0;
extern const Il2CppType TypeU5BU5D_t98989581_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t2118996832_0_0_0;
extern const Il2CppType IList_t3112248398_0_0_0;
extern const Il2CppType IList_1_t813021139_0_0_0;
extern const Il2CppType ICollection_1_t1154546467_0_0_0;
extern const Il2CppType IEnumerable_1_t1135313765_0_0_0;
extern const Il2CppType IList_1_t135751803_0_0_0;
extern const Il2CppType ICollection_1_t477277131_0_0_0;
extern const Il2CppType IEnumerable_1_t458044429_0_0_0;
extern const Il2CppType IList_1_t3345357487_0_0_0;
extern const Il2CppType ICollection_1_t3686882815_0_0_0;
extern const Il2CppType IEnumerable_1_t3667650113_0_0_0;
extern const Il2CppType IList_1_t1896635693_0_0_0;
extern const Il2CppType ICollection_1_t2238161021_0_0_0;
extern const Il2CppType IEnumerable_1_t2218928319_0_0_0;
extern const Il2CppType IList_1_t2297335425_0_0_0;
extern const Il2CppType ICollection_1_t2638860753_0_0_0;
extern const Il2CppType IEnumerable_1_t2619628051_0_0_0;
extern const Il2CppType IList_1_t1644500462_0_0_0;
extern const Il2CppType ICollection_1_t1986025790_0_0_0;
extern const Il2CppType IEnumerable_1_t1966793088_0_0_0;
extern const Il2CppType IList_1_t316688931_0_0_0;
extern const Il2CppType ICollection_1_t658214259_0_0_0;
extern const Il2CppType IEnumerable_1_t638981557_0_0_0;
extern const Il2CppType LocalBuilder_t3984406356_0_0_0;
extern const Il2CppType _LocalBuilder_t2858090211_0_0_0;
extern const Il2CppType LocalVariableInfo_t3511563403_0_0_0;
extern const Il2CppType ILTokenInfo_t598344192_0_0_0;
extern const Il2CppType LabelData_t637884969_0_0_0;
extern const Il2CppType LabelFixup_t3130180171_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t2133930691_0_0_0;
extern const Il2CppType TypeBuilder_t1295847866_0_0_0;
extern const Il2CppType _TypeBuilder_t2508567353_0_0_0;
extern const Il2CppType MethodBuilder_t1042188494_0_0_0;
extern const Il2CppType _MethodBuilder_t3881765320_0_0_0;
extern const Il2CppType FieldBuilder_t2554673315_0_0_0;
extern const Il2CppType _FieldBuilder_t321201139_0_0_0;
extern const Il2CppType ConstructorBuilder_t4189780687_0_0_0;
extern const Il2CppType _ConstructorBuilder_t1016963516_0_0_0;
extern const Il2CppType PropertyBuilder_t282572772_0_0_0;
extern const Il2CppType _PropertyBuilder_t829064024_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t116083861_0_0_0;
extern const Il2CppType EventBuilder_t1568348350_0_0_0;
extern const Il2CppType _EventBuilder_t3684978793_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t4128697880_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t4247477102_0_0_0;
extern const Il2CppType CustomAttributeData_t2324393084_0_0_0;
extern const Il2CppType ResourceInfo_t3026636918_0_0_0;
extern const Il2CppType ResourceCacheItem_t2363372085_0_0_0;
extern const Il2CppType IContextProperty_t2535791102_0_0_0;
extern const Il2CppType Header_t4229290717_0_0_0;
extern const Il2CppType ITrackingHandler_t3307359081_0_0_0;
extern const Il2CppType IContextAttribute_t2746893575_0_0_0;
extern const Il2CppType DateTime_t218649865_0_0_0;
extern const Il2CppType TimeSpan_t595369841_0_0_0;
extern const Il2CppType TypeTag_t1400222038_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t3151463888_0_0_0;
extern const Il2CppType IBuiltInEvidence_t1063146314_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t674530215_0_0_0;
extern const Il2CppType WaitHandle_t2040275096_0_0_0;
extern const Il2CppType IDisposable_t3181680589_0_0_0;
extern const Il2CppType MarshalByRefObject_t714030913_0_0_0;
extern const Il2CppType DateTimeOffset_t206488804_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t1358777808_0_0_0;
extern const Il2CppType BigInteger_t1843659261_0_0_0;
extern const Il2CppType ByteU5BU5D_t3567143369_0_0_0;
extern const Il2CppType IList_1_t2404841927_0_0_0;
extern const Il2CppType ICollection_1_t2746367255_0_0_0;
extern const Il2CppType IEnumerable_1_t2727134553_0_0_0;
extern const Il2CppType X509Certificate_t1117940400_0_0_0;
extern const Il2CppType IDeserializationCallback_t3618342850_0_0_0;
extern const Il2CppType ClientCertificateType_t373382451_0_0_0;
extern const Il2CppType KeyValuePair_2_t3397783615_0_0_0;
extern const Il2CppType KeyValuePair_2_t1601545872_0_0_0;
extern const Il2CppType X509ChainStatus_t261084987_0_0_0;
extern const Il2CppType Capture_t1485804594_0_0_0;
extern const Il2CppType Group_t1812461930_0_0_0;
extern const Il2CppType Mark_t3196564960_0_0_0;
extern const Il2CppType UriScheme_t3530988980_0_0_0;
extern const Il2CppType Link_t890348390_0_0_0;
extern const Il2CppType AsyncOperation_t3797817720_0_0_0;
extern const Il2CppType Camera_t362346687_0_0_0;
extern const Il2CppType Behaviour_t3297694025_0_0_0;
extern const Il2CppType Component_t2335505321_0_0_0;
extern const Il2CppType Object_t3645472222_0_0_0;
extern const Il2CppType Display_t81008966_0_0_0;
extern const Il2CppType Keyframe_t1233380954_0_0_0;
extern const Il2CppType Vector3_t3932393085_0_0_0;
extern const Il2CppType Vector4_t1900979187_0_0_0;
extern const Il2CppType Vector2_t2246369278_0_0_0;
extern const Il2CppType Color32_t662424039_0_0_0;
extern const Il2CppType Playable_t379060559_0_0_0;
extern const Il2CppType PlayableOutput_t2773044158_0_0_0;
extern const Il2CppType Scene_t1083318001_0_0_0;
extern const Il2CppType LoadSceneMode_t3029863218_0_0_0;
extern const Il2CppType SpriteAtlas_t2782010525_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t4131774042_0_0_0;
extern const Il2CppType Attribute_t3130080784_0_0_0;
extern const Il2CppType _Attribute_t1312228502_0_0_0;
extern const Il2CppType ExecuteInEditMode_t2915138156_0_0_0;
extern const Il2CppType RequireComponent_t3219866953_0_0_0;
extern const Il2CppType HitInfo_t3525137541_0_0_0;
extern const Il2CppType PersistentCall_t1968425864_0_0_0;
extern const Il2CppType BaseInvokableCall_t3648497698_0_0_0;
extern const Il2CppType WorkRequest_t266095197_0_0_0;
extern const Il2CppType PlayableBinding_t2941134609_0_0_0;
extern const Il2CppType MessageEventArgs_t2772358345_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t675444414_0_0_0;
extern const Il2CppType WeakReference_t1433439652_0_0_0;
extern const Il2CppType KeyValuePair_2_t2055206595_0_0_0;
extern const Il2CppType KeyValuePair_2_t2936619059_0_0_0;
extern const Il2CppType Rigidbody2D_t1466291965_0_0_0;
extern const Il2CppType Font_t2643454529_0_0_0;
extern const Il2CppType UIVertex_t2944109340_0_0_0;
extern const Il2CppType UICharInfo_t839829031_0_0_0;
extern const Il2CppType UILineInfo_t2156392613_0_0_0;
extern const Il2CppType AnimationClipPlayable_t1463126785_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t1879541177_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t4125999351_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t999774996_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t4278763719_0_0_0;
extern const Il2CppType AudioSpatializerExtensionDefinition_t2663594034_0_0_0;
extern const Il2CppType AudioAmbisonicExtensionDefinition_t4031934303_0_0_0;
extern const Il2CppType AudioSourceExtension_t4018014667_0_0_0;
extern const Il2CppType ScriptableObject_t1324617639_0_0_0;
extern const Il2CppType AudioMixerPlayable_t3997886707_0_0_0;
extern const Il2CppType AudioClipPlayable_t2374979939_0_0_0;
extern const Il2CppType AchievementDescription_t1703388966_0_0_0;
extern const Il2CppType IAchievementDescription_t1992040264_0_0_0;
extern const Il2CppType UserProfile_t2802009794_0_0_0;
extern const Il2CppType IUserProfile_t2218596927_0_0_0;
extern const Il2CppType GcLeaderboard_t3922680439_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t185225881_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t2787719602_0_0_0;
extern const Il2CppType IAchievement_t3527372131_0_0_0;
extern const Il2CppType GcAchievementData_t1576042669_0_0_0;
extern const Il2CppType Achievement_t1548866956_0_0_0;
extern const Il2CppType IScoreU5BU5D_t3386159579_0_0_0;
extern const Il2CppType IScore_t2300007342_0_0_0;
extern const Il2CppType GcScoreData_t2566310542_0_0_0;
extern const Il2CppType Score_t1607899041_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t1348948454_0_0_0;
extern const Il2CppType GUILayoutOption_t1561297139_0_0_0;
extern const Il2CppType LayoutCache_t2616876984_0_0_0;
extern const Il2CppType KeyValuePair_2_t1339419795_0_0_0;
extern const Il2CppType KeyValuePair_2_t3404269591_0_0_0;
extern const Il2CppType GUILayoutEntry_t3231862407_0_0_0;
extern const Il2CppType Exception_t214279536_0_0_0;
extern const Il2CppType GUIStyle_t3615497928_0_0_0;
extern const Il2CppType KeyValuePair_2_t2776823806_0_0_0;
extern const Il2CppType Particle_t1283715429_0_0_0;
extern const Il2CppType RaycastHit_t3372097066_0_0_0;
extern const Il2CppType ContactPoint_t99991485_0_0_0;
extern const Il2CppType EventSystem_t1423129782_0_0_0;
extern const Il2CppType UIBehaviour_t1053087876_0_0_0;
extern const Il2CppType MonoBehaviour_t4008345588_0_0_0;
extern const Il2CppType BaseInputModule_t893951129_0_0_0;
extern const Il2CppType RaycastResult_t1958542877_0_0_0;
extern const Il2CppType IDeselectHandler_t1423095504_0_0_0;
extern const Il2CppType IEventSystemHandler_t2679299517_0_0_0;
extern const Il2CppType List_1_t58535312_0_0_0;
extern const Il2CppType List_1_t2226230279_0_0_0;
extern const Il2CppType List_1_t4009708412_0_0_0;
extern const Il2CppType ISelectHandler_t637179496_0_0_0;
extern const Il2CppType BaseRaycaster_t2460995416_0_0_0;
extern const Il2CppType Entry_t2441154801_0_0_0;
extern const Il2CppType BaseEventData_t3589491243_0_0_0;
extern const Il2CppType IPointerEnterHandler_t3124626600_0_0_0;
extern const Il2CppType IPointerExitHandler_t1395786574_0_0_0;
extern const Il2CppType IPointerDownHandler_t44025902_0_0_0;
extern const Il2CppType IPointerUpHandler_t102393737_0_0_0;
extern const Il2CppType IPointerClickHandler_t4047079811_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t818614831_0_0_0;
extern const Il2CppType IBeginDragHandler_t2428313557_0_0_0;
extern const Il2CppType IDragHandler_t2984229372_0_0_0;
extern const Il2CppType IEndDragHandler_t1280646970_0_0_0;
extern const Il2CppType IDropHandler_t1307228819_0_0_0;
extern const Il2CppType IScrollHandler_t194649380_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t2808857881_0_0_0;
extern const Il2CppType IMoveHandler_t1328422412_0_0_0;
extern const Il2CppType ISubmitHandler_t3368026263_0_0_0;
extern const Il2CppType ICancelHandler_t1174209038_0_0_0;
extern const Il2CppType Transform_t3580444445_0_0_0;
extern const Il2CppType GameObject_t2881801184_0_0_0;
extern const Il2CppType BaseInput_t2888570827_0_0_0;
extern const Il2CppType PointerEventData_t507157825_0_0_0;
extern const Il2CppType AbstractEventData_t3911574392_0_0_0;
extern const Il2CppType KeyValuePair_2_t1294550432_0_0_0;
extern const Il2CppType ButtonState_t2740850036_0_0_0;
extern const Il2CppType RaycastHit2D_t218385889_0_0_0;
extern const Il2CppType Color_t1361298052_0_0_0;
extern const Il2CppType ICanvasElement_t2313639799_0_0_0;
extern const Il2CppType ColorBlock_t3446061115_0_0_0;
extern const Il2CppType OptionData_t436826826_0_0_0;
extern const Il2CppType DropdownItem_t2659469254_0_0_0;
extern const Il2CppType FloatTween_t2756583341_0_0_0;
extern const Il2CppType Sprite_t2497061588_0_0_0;
extern const Il2CppType Canvas_t2667390772_0_0_0;
extern const Il2CppType List_1_t46626567_0_0_0;
extern const Il2CppType HashSet_1_t3142691717_0_0_0;
extern const Il2CppType Text_t1445358712_0_0_0;
extern const Il2CppType Link_t1783679914_0_0_0;
extern const Il2CppType ILayoutElement_t368149833_0_0_0;
extern const Il2CppType MaskableGraphic_t4128200649_0_0_0;
extern const Il2CppType IClippable_t3544737781_0_0_0;
extern const Il2CppType IMaskable_t997864649_0_0_0;
extern const Il2CppType IMaterialModifier_t805903918_0_0_0;
extern const Il2CppType Graphic_t2696902410_0_0_0;
extern const Il2CppType KeyValuePair_2_t3866178953_0_0_0;
extern const Il2CppType ColorTween_t2856906502_0_0_0;
extern const Il2CppType IndexedSet_1_t1130276133_0_0_0;
extern const Il2CppType KeyValuePair_2_t3771799818_0_0_0;
extern const Il2CppType KeyValuePair_2_t1169793209_0_0_0;
extern const Il2CppType KeyValuePair_2_t223188984_0_0_0;
extern const Il2CppType Type_t2837774015_0_0_0;
extern const Il2CppType FillMethod_t2931617579_0_0_0;
extern const Il2CppType ContentType_t1122571944_0_0_0;
extern const Il2CppType LineType_t3638610376_0_0_0;
extern const Il2CppType InputType_t4187126303_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t1103124205_0_0_0;
extern const Il2CppType CharacterValidation_t2769550230_0_0_0;
extern const Il2CppType Mask_t745540708_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t2306748731_0_0_0;
extern const Il2CppType List_1_t2419743799_0_0_0;
extern const Il2CppType RectMask2D_t2972282963_0_0_0;
extern const Il2CppType IClipper_t386535878_0_0_0;
extern const Il2CppType List_1_t351518758_0_0_0;
extern const Il2CppType Navigation_t2227424972_0_0_0;
extern const Il2CppType Link_t3883058983_0_0_0;
extern const Il2CppType Direction_t1205246216_0_0_0;
extern const Il2CppType Selectable_t1875500212_0_0_0;
extern const Il2CppType Transition_t2703339633_0_0_0;
extern const Il2CppType SpriteState_t710398810_0_0_0;
extern const Il2CppType CanvasGroup_t2669816671_0_0_0;
extern const Il2CppType Direction_t299841542_0_0_0;
extern const Il2CppType MatEntry_t930024495_0_0_0;
extern const Il2CppType Toggle_t403198564_0_0_0;
extern const Il2CppType KeyValuePair_2_t1494153101_0_0_0;
extern const Il2CppType AspectMode_t3475277687_0_0_0;
extern const Il2CppType FitMode_t3582979200_0_0_0;
extern const Il2CppType RectTransform_t1161610249_0_0_0;
extern const Il2CppType LayoutRebuilder_t2288684892_0_0_0;
extern const Il2CppType List_1_t1311628880_0_0_0;
extern const Il2CppType List_1_t2336627130_0_0_0;
extern const Il2CppType List_1_t3920572369_0_0_0;
extern const Il2CppType List_1_t3575182278_0_0_0;
extern const Il2CppType List_1_t894813333_0_0_0;
extern const Il2CppType List_1_t323345135_0_0_0;
extern const Il2CppType Link_t3597842601_0_0_0;
extern const Il2CppType ARHitTestResult_t942592945_0_0_0;
extern const Il2CppType ARHitTestResultType_t1760080916_0_0_0;
extern const Il2CppType ParticleSystem_t3890943298_0_0_0;
extern const Il2CppType ARPlaneAnchorGameObject_t3125390466_0_0_0;
extern const Il2CppType KeyValuePair_2_t2286716344_0_0_0;
extern const Il2CppType UnityARSessionRunOption_t572719712_0_0_0;
extern const Il2CppType UnityARAlignment_t2001134742_0_0_0;
extern const Il2CppType UnityARPlaneDetection_t1240827406_0_0_0;
extern const Il2CppType IEnumerable_1_t361495900_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m3921660370_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3350998347_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m689900404_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m689900404_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m4285269846_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1330797580_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1330797580_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m869698846_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2635216824_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2635216824_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3791969856_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2053374032_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2053374032_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3529074503_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1046918090_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m3074224534_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m3074224534_gp_1_0_0_0;
extern const Il2CppType Array_compare_m182040922_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m3653394148_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m1475766408_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m1259694655_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m1921540296_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m2082766338_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m2082766338_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2403343833_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2322380597_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1895958268_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m2364170290_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3411283154_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1488736383_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m1842855399_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m619051682_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m625219582_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m863042731_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m1718101709_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m1667751126_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m4170047889_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m1415329759_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m4008997880_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2739852194_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m641447484_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m3014889077_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m2697550731_gp_0_0_0_0;
extern const Il2CppType Array_Find_m408926873_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m4132394418_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t3560912388_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t1444333090_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3454464003_gp_0_0_0_0;
extern const Il2CppType IList_1_t3296908666_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t842930506_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t2719303306_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t1340870152_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t2305410469_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t4291314091_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3686457887_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3686457887_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t2794658882_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m1146410757_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2837955638_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2837955638_gp_1_0_0_0;
extern const Il2CppType Enumerator_t3167388891_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3167388891_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1888873938_0_0_0;
extern const Il2CppType ValueCollection_t4200311898_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t4200311898_gp_1_0_0_0;
extern const Il2CppType Enumerator_t2729626900_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2729626900_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t962836640_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t3887701940_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t3218101675_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1744688238_0_0_0;
extern const Il2CppType IDictionary_2_t188967788_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t188967788_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t759847414_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t759847414_gp_1_0_0_0;
extern const Il2CppType List_1_t3212367358_gp_0_0_0_0;
extern const Il2CppType Enumerator_t280605556_gp_0_0_0_0;
extern const Il2CppType Collection_1_t3428355205_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t4027728135_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m3258900994_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m3258900994_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m3164454857_gp_0_0_0_0;
extern const Il2CppType Queue_1_t1245512621_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3225660046_gp_0_0_0_0;
extern const Il2CppType Stack_1_t4193425384_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2833440198_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t3707433416_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2154081504_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t1212413519_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m1190044049_gp_0_0_0_0;
extern const Il2CppType Enumerable_Single_m243410678_gp_0_0_0_0;
extern const Il2CppType Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0;
extern const Il2CppType Enumerable_ToList_m978993884_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m3875999180_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m278292298_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m432043675_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m3978037784_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m594717923_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m983846299_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m547656136_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m3576923290_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m1754205259_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m933510971_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m394723642_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m2979658420_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m4006422251_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m207993587_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m3613586775_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m2818067362_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t336718215_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t3655347980_0_0_0;
extern const Il2CppType InvokableCall_2_t2897724777_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2897724777_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t2251939000_0_0_0;
extern const Il2CppType InvokableCall_3_t3004736123_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3004736123_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3004736123_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t2799906667_0_0_0;
extern const Il2CppType InvokableCall_4_t3272338627_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3272338627_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3272338627_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3272338627_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1723064271_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t354605367_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t697054180_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t697054180_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1308998058_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1308998058_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1308998058_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t646578554_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t646578554_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t646578554_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t646578554_gp_3_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m4182767292_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m2697298702_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m1972297587_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m4032946386_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m1635484251_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t1184621110_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m1943470346_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3902694711_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t2290263430_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t4260954668_gp_0_0_0_0;
extern const Il2CppType List_1_t2287822912_0_0_0;
extern const Il2CppType ObjectPool_1_t1813863383_gp_0_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t1108411129_0_0_0;
extern const Il2CppType PlayerConnection_t3190352615_0_0_0;
extern const Il2CppType GUILayer_t1634068716_0_0_0;
extern const Il2CppType AxisEventData_t513062718_0_0_0;
extern const Il2CppType SpriteRenderer_t3285139998_0_0_0;
extern const Il2CppType GraphicRaycaster_t1046486851_0_0_0;
extern const Il2CppType Image_t1384762407_0_0_0;
extern const Il2CppType Button_t3512035522_0_0_0;
extern const Il2CppType Dropdown_t597371120_0_0_0;
extern const Il2CppType CanvasRenderer_t3854993866_0_0_0;
extern const Il2CppType Corner_t562422201_0_0_0;
extern const Il2CppType Axis_t1187875949_0_0_0;
extern const Il2CppType Constraint_t2335916233_0_0_0;
extern const Il2CppType SubmitEvent_t1157603230_0_0_0;
extern const Il2CppType OnChangeEvent_t3348859603_0_0_0;
extern const Il2CppType OnValidateInput_t3514437264_0_0_0;
extern const Il2CppType LayoutElement_t708853819_0_0_0;
extern const Il2CppType RectOffset_t817686154_0_0_0;
extern const Il2CppType TextAnchor_t2808300235_0_0_0;
extern const Il2CppType AnimationTriggers_t1153907273_0_0_0;
extern const Il2CppType Animator_t2047190299_0_0_0;
extern const Il2CppType UnityARVideo_t3776543285_0_0_0;
extern const Il2CppType MeshRenderer_t1975789539_0_0_0;
extern const Il2CppType Slider_t402745866_0_0_0;
extern const Il2CppType RawImage_t2028672565_0_0_0;
extern const Il2CppType InputField_t611956437_0_0_0;
extern const Il2CppType BoxSlider_t1609811286_0_0_0;
extern const Il2CppType UnityARUserAnchorComponent_t3923313875_0_0_0;
extern const Il2CppType serializableFromEditorMessage_t755252547_0_0_0;
extern const Il2CppType Light_t455885974_0_0_0;
extern const Il2CppType DontDestroyOnLoad_t530518805_0_0_0;
extern const Il2CppType MeshFilter_t3228362100_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0 = { 1, GenInst_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_Char_t1622636488_0_0_0_Types[] = { (&Char_t1622636488_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t1622636488_0_0_0 = { 1, GenInst_Char_t1622636488_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t3603203610_0_0_0_Types[] = { (&Int64_t3603203610_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t3603203610_0_0_0 = { 1, GenInst_Int64_t3603203610_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t1085449242_0_0_0_Types[] = { (&UInt32_t1085449242_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t1085449242_0_0_0 = { 1, GenInst_UInt32_t1085449242_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t462540778_0_0_0_Types[] = { (&UInt64_t462540778_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t462540778_0_0_0 = { 1, GenInst_UInt64_t462540778_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t2640180184_0_0_0_Types[] = { (&Byte_t2640180184_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t2640180184_0_0_0 = { 1, GenInst_Byte_t2640180184_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t2299796046_0_0_0_Types[] = { (&SByte_t2299796046_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t2299796046_0_0_0 = { 1, GenInst_SByte_t2299796046_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t2935336495_0_0_0_Types[] = { (&Int16_t2935336495_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t2935336495_0_0_0 = { 1, GenInst_Int16_t2935336495_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t3430003764_0_0_0_Types[] = { (&UInt16_t3430003764_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t3430003764_0_0_0 = { 1, GenInst_UInt16_t3430003764_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t1386358354_0_0_0_Types[] = { (&IConvertible_t1386358354_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t1386358354_0_0_0 = { 1, GenInst_IConvertible_t1386358354_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t3923264286_0_0_0_Types[] = { (&IComparable_t3923264286_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t3923264286_0_0_0 = { 1, GenInst_IComparable_t3923264286_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t307620497_0_0_0_Types[] = { (&IEnumerable_t307620497_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t307620497_0_0_0 = { 1, GenInst_IEnumerable_t307620497_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t3227696483_0_0_0_Types[] = { (&ICloneable_t3227696483_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t3227696483_0_0_0 = { 1, GenInst_ICloneable_t3227696483_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t1965733923_0_0_0_Types[] = { (&IComparable_1_t1965733923_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t1965733923_0_0_0 = { 1, GenInst_IComparable_1_t1965733923_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t733774393_0_0_0_Types[] = { (&IEquatable_1_t733774393_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t733774393_0_0_0 = { 1, GenInst_IEquatable_1_t733774393_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t371090060_0_0_0_Types[] = { (&IReflect_t371090060_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t371090060_0_0_0 = { 1, GenInst_IReflect_t371090060_0_0_0_Types };
static const RuntimeType* GenInst__Type_t3580695744_0_0_0_Types[] = { (&_Type_t3580695744_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t3580695744_0_0_0 = { 1, GenInst__Type_t3580695744_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t2532673682_0_0_0_Types[] = { (&ICustomAttributeProvider_t2532673682_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2532673682_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2532673682_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t1879838719_0_0_0_Types[] = { (&_MemberInfo_t1879838719_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t1879838719_0_0_0 = { 1, GenInst__MemberInfo_t1879838719_0_0_0_Types };
static const RuntimeType* GenInst_Double_t3942828892_0_0_0_Types[] = { (&Double_t3942828892_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t3942828892_0_0_0 = { 1, GenInst_Double_t3942828892_0_0_0_Types };
static const RuntimeType* GenInst_Single_t897798503_0_0_0_Types[] = { (&Single_t897798503_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t897798503_0_0_0 = { 1, GenInst_Single_t897798503_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t2641346350_0_0_0_Types[] = { (&Decimal_t2641346350_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t2641346350_0_0_0 = { 1, GenInst_Decimal_t2641346350_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2440219994_0_0_0_Types[] = { (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2440219994_0_0_0 = { 1, GenInst_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t4015201940_0_0_0_Types[] = { (&Delegate_t4015201940_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t4015201940_0_0_0 = { 1, GenInst_Delegate_t4015201940_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t3561356945_0_0_0_Types[] = { (&ISerializable_t3561356945_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t3561356945_0_0_0 = { 1, GenInst_ISerializable_t3561356945_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t3156340899_0_0_0_Types[] = { (&ParameterInfo_t3156340899_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t3156340899_0_0_0 = { 1, GenInst_ParameterInfo_t3156340899_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t2470192644_0_0_0_Types[] = { (&_ParameterInfo_t2470192644_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2470192644_0_0_0 = { 1, GenInst__ParameterInfo_t2470192644_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t1933193809_0_0_0_Types[] = { (&ParameterModifier_t1933193809_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1933193809_0_0_0 = { 1, GenInst_ParameterModifier_t1933193809_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t2737396514_0_0_0_Types[] = { (&_FieldInfo_t2737396514_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t2737396514_0_0_0 = { 1, GenInst__FieldInfo_t2737396514_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t3421021404_0_0_0_Types[] = { (&_MethodInfo_t3421021404_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t3421021404_0_0_0 = { 1, GenInst__MethodInfo_t3421021404_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t3670318294_0_0_0_Types[] = { (&MethodBase_t3670318294_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t3670318294_0_0_0 = { 1, GenInst_MethodBase_t3670318294_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t1498739407_0_0_0_Types[] = { (&_MethodBase_t1498739407_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t1498739407_0_0_0 = { 1, GenInst__MethodBase_t1498739407_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t3011499035_0_0_0_Types[] = { (&ConstructorInfo_t3011499035_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t3011499035_0_0_0 = { 1, GenInst_ConstructorInfo_t3011499035_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t2887962050_0_0_0_Types[] = { (&_ConstructorInfo_t2887962050_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2887962050_0_0_0 = { 1, GenInst__ConstructorInfo_t2887962050_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t739736251_0_0_0_Types[] = { (&TableRange_t739736251_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t739736251_0_0_0 = { 1, GenInst_TableRange_t739736251_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t1494168405_0_0_0_Types[] = { (&TailoringInfo_t1494168405_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1494168405_0_0_0 = { 1, GenInst_TailoringInfo_t1494168405_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t178173863_0_0_0_Types[] = { (&KeyValuePair_2_t178173863_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t178173863_0_0_0 = { 1, GenInst_KeyValuePair_2_t178173863_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2678926947_0_0_0_Types[] = { (&Link_t2678926947_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2678926947_0_0_0 = { 1, GenInst_Link_t2678926947_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3515577538_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3515577538_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3507565976_0_0_0 = { 1, GenInst_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t178173863_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t3515577538_0_0_0), (&KeyValuePair_2_t178173863_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t178173863_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t178173863_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t3515577538_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2676903416_0_0_0_Types[] = { (&KeyValuePair_2_t2676903416_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2676903416_0_0_0 = { 1, GenInst_KeyValuePair_2_t2676903416_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t2676903416_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t3515577538_0_0_0), (&KeyValuePair_2_t2676903416_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t2676903416_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t2676903416_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t2685614877_0_0_0_Types[] = { (&Contraction_t2685614877_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t2685614877_0_0_0 = { 1, GenInst_Contraction_t2685614877_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t2701131617_0_0_0_Types[] = { (&Level2Map_t2701131617_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t2701131617_0_0_0 = { 1, GenInst_Level2Map_t2701131617_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1843659260_0_0_0_Types[] = { (&BigInteger_t1843659260_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1843659260_0_0_0 = { 1, GenInst_BigInteger_t1843659260_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t3274741244_0_0_0_Types[] = { (&KeySizes_t3274741244_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t3274741244_0_0_0 = { 1, GenInst_KeySizes_t3274741244_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1509590809_0_0_0_Types[] = { (&KeyValuePair_2_t1509590809_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1509590809_0_0_0 = { 1, GenInst_KeyValuePair_2_t1509590809_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1509590809_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t1509590809_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1509590809_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1509590809_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1231688804_0_0_0_Types[] = { (&Slot_t1231688804_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1231688804_0_0_0 = { 1, GenInst_Slot_t1231688804_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t697738515_0_0_0_Types[] = { (&Slot_t697738515_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t697738515_0_0_0 = { 1, GenInst_Slot_t697738515_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t3907017807_0_0_0_Types[] = { (&StackFrame_t3907017807_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t3907017807_0_0_0 = { 1, GenInst_StackFrame_t3907017807_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t1639743412_0_0_0_Types[] = { (&Calendar_t1639743412_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t1639743412_0_0_0 = { 1, GenInst_Calendar_t1639743412_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t2525481856_0_0_0_Types[] = { (&ModuleBuilder_t2525481856_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t2525481856_0_0_0 = { 1, GenInst_ModuleBuilder_t2525481856_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t1154404907_0_0_0_Types[] = { (&_ModuleBuilder_t1154404907_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1154404907_0_0_0 = { 1, GenInst__ModuleBuilder_t1154404907_0_0_0_Types };
static const RuntimeType* GenInst_Module_t575570506_0_0_0_Types[] = { (&Module_t575570506_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t575570506_0_0_0 = { 1, GenInst_Module_t575570506_0_0_0_Types };
static const RuntimeType* GenInst__Module_t1610937700_0_0_0_Types[] = { (&_Module_t1610937700_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t1610937700_0_0_0 = { 1, GenInst__Module_t1610937700_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeBuilder_t1604210647_0_0_0_Types[] = { (&CustomAttributeBuilder_t1604210647_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeBuilder_t1604210647_0_0_0 = { 1, GenInst_CustomAttributeBuilder_t1604210647_0_0_0_Types };
static const RuntimeType* GenInst__CustomAttributeBuilder_t2558201917_0_0_0_Types[] = { (&_CustomAttributeBuilder_t2558201917_0_0_0) };
extern const Il2CppGenericInst GenInst__CustomAttributeBuilder_t2558201917_0_0_0 = { 1, GenInst__CustomAttributeBuilder_t2558201917_0_0_0_Types };
static const RuntimeType* GenInst_MonoResource_t2877269471_0_0_0_Types[] = { (&MonoResource_t2877269471_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoResource_t2877269471_0_0_0 = { 1, GenInst_MonoResource_t2877269471_0_0_0_Types };
static const RuntimeType* GenInst_MonoWin32Resource_t2032464017_0_0_0_Types[] = { (&MonoWin32Resource_t2032464017_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoWin32Resource_t2032464017_0_0_0 = { 1, GenInst_MonoWin32Resource_t2032464017_0_0_0_Types };
static const RuntimeType* GenInst_RefEmitPermissionSet_t4262511222_0_0_0_Types[] = { (&RefEmitPermissionSet_t4262511222_0_0_0) };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t4262511222_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t4262511222_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t4081109430_0_0_0_Types[] = { (&ParameterBuilder_t4081109430_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t4081109430_0_0_0 = { 1, GenInst_ParameterBuilder_t4081109430_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t4215768685_0_0_0_Types[] = { (&_ParameterBuilder_t4215768685_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t4215768685_0_0_0 = { 1, GenInst__ParameterBuilder_t4215768685_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t98989581_0_0_0_Types[] = { (&TypeU5BU5D_t98989581_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t98989581_0_0_0 = { 1, GenInst_TypeU5BU5D_t98989581_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t2118996832_0_0_0_Types[] = { (&ICollection_t2118996832_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t2118996832_0_0_0 = { 1, GenInst_ICollection_t2118996832_0_0_0_Types };
static const RuntimeType* GenInst_IList_t3112248398_0_0_0_Types[] = { (&IList_t3112248398_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t3112248398_0_0_0 = { 1, GenInst_IList_t3112248398_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t813021139_0_0_0_Types[] = { (&IList_1_t813021139_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t813021139_0_0_0 = { 1, GenInst_IList_1_t813021139_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1154546467_0_0_0_Types[] = { (&ICollection_1_t1154546467_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1154546467_0_0_0 = { 1, GenInst_ICollection_1_t1154546467_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1135313765_0_0_0_Types[] = { (&IEnumerable_1_t1135313765_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1135313765_0_0_0 = { 1, GenInst_IEnumerable_1_t1135313765_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t135751803_0_0_0_Types[] = { (&IList_1_t135751803_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t135751803_0_0_0 = { 1, GenInst_IList_1_t135751803_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t477277131_0_0_0_Types[] = { (&ICollection_1_t477277131_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t477277131_0_0_0 = { 1, GenInst_ICollection_1_t477277131_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t458044429_0_0_0_Types[] = { (&IEnumerable_1_t458044429_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t458044429_0_0_0 = { 1, GenInst_IEnumerable_1_t458044429_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3345357487_0_0_0_Types[] = { (&IList_1_t3345357487_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3345357487_0_0_0 = { 1, GenInst_IList_1_t3345357487_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3686882815_0_0_0_Types[] = { (&ICollection_1_t3686882815_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3686882815_0_0_0 = { 1, GenInst_ICollection_1_t3686882815_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3667650113_0_0_0_Types[] = { (&IEnumerable_1_t3667650113_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3667650113_0_0_0 = { 1, GenInst_IEnumerable_1_t3667650113_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1896635693_0_0_0_Types[] = { (&IList_1_t1896635693_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1896635693_0_0_0 = { 1, GenInst_IList_1_t1896635693_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2238161021_0_0_0_Types[] = { (&ICollection_1_t2238161021_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2238161021_0_0_0 = { 1, GenInst_ICollection_1_t2238161021_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2218928319_0_0_0_Types[] = { (&IEnumerable_1_t2218928319_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2218928319_0_0_0 = { 1, GenInst_IEnumerable_1_t2218928319_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2297335425_0_0_0_Types[] = { (&IList_1_t2297335425_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2297335425_0_0_0 = { 1, GenInst_IList_1_t2297335425_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2638860753_0_0_0_Types[] = { (&ICollection_1_t2638860753_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2638860753_0_0_0 = { 1, GenInst_ICollection_1_t2638860753_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2619628051_0_0_0_Types[] = { (&IEnumerable_1_t2619628051_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2619628051_0_0_0 = { 1, GenInst_IEnumerable_1_t2619628051_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1644500462_0_0_0_Types[] = { (&IList_1_t1644500462_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1644500462_0_0_0 = { 1, GenInst_IList_1_t1644500462_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1986025790_0_0_0_Types[] = { (&ICollection_1_t1986025790_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1986025790_0_0_0 = { 1, GenInst_ICollection_1_t1986025790_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1966793088_0_0_0_Types[] = { (&IEnumerable_1_t1966793088_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1966793088_0_0_0 = { 1, GenInst_IEnumerable_1_t1966793088_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t316688931_0_0_0_Types[] = { (&IList_1_t316688931_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t316688931_0_0_0 = { 1, GenInst_IList_1_t316688931_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t658214259_0_0_0_Types[] = { (&ICollection_1_t658214259_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t658214259_0_0_0 = { 1, GenInst_ICollection_1_t658214259_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t638981557_0_0_0_Types[] = { (&IEnumerable_1_t638981557_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t638981557_0_0_0 = { 1, GenInst_IEnumerable_1_t638981557_0_0_0_Types };
static const RuntimeType* GenInst_LocalBuilder_t3984406356_0_0_0_Types[] = { (&LocalBuilder_t3984406356_0_0_0) };
extern const Il2CppGenericInst GenInst_LocalBuilder_t3984406356_0_0_0 = { 1, GenInst_LocalBuilder_t3984406356_0_0_0_Types };
static const RuntimeType* GenInst__LocalBuilder_t2858090211_0_0_0_Types[] = { (&_LocalBuilder_t2858090211_0_0_0) };
extern const Il2CppGenericInst GenInst__LocalBuilder_t2858090211_0_0_0 = { 1, GenInst__LocalBuilder_t2858090211_0_0_0_Types };
static const RuntimeType* GenInst_LocalVariableInfo_t3511563403_0_0_0_Types[] = { (&LocalVariableInfo_t3511563403_0_0_0) };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t3511563403_0_0_0 = { 1, GenInst_LocalVariableInfo_t3511563403_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t598344192_0_0_0_Types[] = { (&ILTokenInfo_t598344192_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t598344192_0_0_0 = { 1, GenInst_ILTokenInfo_t598344192_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t637884969_0_0_0_Types[] = { (&LabelData_t637884969_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t637884969_0_0_0 = { 1, GenInst_LabelData_t637884969_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t3130180171_0_0_0_Types[] = { (&LabelFixup_t3130180171_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t3130180171_0_0_0 = { 1, GenInst_LabelFixup_t3130180171_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t2133930691_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t2133930691_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t2133930691_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t2133930691_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t1295847866_0_0_0_Types[] = { (&TypeBuilder_t1295847866_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1295847866_0_0_0 = { 1, GenInst_TypeBuilder_t1295847866_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t2508567353_0_0_0_Types[] = { (&_TypeBuilder_t2508567353_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2508567353_0_0_0 = { 1, GenInst__TypeBuilder_t2508567353_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t1042188494_0_0_0_Types[] = { (&MethodBuilder_t1042188494_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1042188494_0_0_0 = { 1, GenInst_MethodBuilder_t1042188494_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t3881765320_0_0_0_Types[] = { (&_MethodBuilder_t3881765320_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3881765320_0_0_0 = { 1, GenInst__MethodBuilder_t3881765320_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t2554673315_0_0_0_Types[] = { (&FieldBuilder_t2554673315_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2554673315_0_0_0 = { 1, GenInst_FieldBuilder_t2554673315_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t321201139_0_0_0_Types[] = { (&_FieldBuilder_t321201139_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t321201139_0_0_0 = { 1, GenInst__FieldBuilder_t321201139_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t4189780687_0_0_0_Types[] = { (&ConstructorBuilder_t4189780687_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t4189780687_0_0_0 = { 1, GenInst_ConstructorBuilder_t4189780687_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t1016963516_0_0_0_Types[] = { (&_ConstructorBuilder_t1016963516_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1016963516_0_0_0 = { 1, GenInst__ConstructorBuilder_t1016963516_0_0_0_Types };
static const RuntimeType* GenInst_PropertyBuilder_t282572772_0_0_0_Types[] = { (&PropertyBuilder_t282572772_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t282572772_0_0_0 = { 1, GenInst_PropertyBuilder_t282572772_0_0_0_Types };
static const RuntimeType* GenInst__PropertyBuilder_t829064024_0_0_0_Types[] = { (&_PropertyBuilder_t829064024_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t829064024_0_0_0 = { 1, GenInst__PropertyBuilder_t829064024_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t116083861_0_0_0_Types[] = { (&_PropertyInfo_t116083861_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t116083861_0_0_0 = { 1, GenInst__PropertyInfo_t116083861_0_0_0_Types };
static const RuntimeType* GenInst_EventBuilder_t1568348350_0_0_0_Types[] = { (&EventBuilder_t1568348350_0_0_0) };
extern const Il2CppGenericInst GenInst_EventBuilder_t1568348350_0_0_0 = { 1, GenInst_EventBuilder_t1568348350_0_0_0_Types };
static const RuntimeType* GenInst__EventBuilder_t3684978793_0_0_0_Types[] = { (&_EventBuilder_t3684978793_0_0_0) };
extern const Il2CppGenericInst GenInst__EventBuilder_t3684978793_0_0_0 = { 1, GenInst__EventBuilder_t3684978793_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t4128697880_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t4247477102_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t2324393084_0_0_0_Types[] = { (&CustomAttributeData_t2324393084_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2324393084_0_0_0 = { 1, GenInst_CustomAttributeData_t2324393084_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t3026636918_0_0_0_Types[] = { (&ResourceInfo_t3026636918_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3026636918_0_0_0 = { 1, GenInst_ResourceInfo_t3026636918_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t2363372085_0_0_0_Types[] = { (&ResourceCacheItem_t2363372085_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t2363372085_0_0_0 = { 1, GenInst_ResourceCacheItem_t2363372085_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t2535791102_0_0_0_Types[] = { (&IContextProperty_t2535791102_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t2535791102_0_0_0 = { 1, GenInst_IContextProperty_t2535791102_0_0_0_Types };
static const RuntimeType* GenInst_Header_t4229290717_0_0_0_Types[] = { (&Header_t4229290717_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t4229290717_0_0_0 = { 1, GenInst_Header_t4229290717_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t3307359081_0_0_0_Types[] = { (&ITrackingHandler_t3307359081_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t3307359081_0_0_0 = { 1, GenInst_ITrackingHandler_t3307359081_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t2746893575_0_0_0_Types[] = { (&IContextAttribute_t2746893575_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2746893575_0_0_0 = { 1, GenInst_IContextAttribute_t2746893575_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t218649865_0_0_0_Types[] = { (&DateTime_t218649865_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t218649865_0_0_0 = { 1, GenInst_DateTime_t218649865_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t595369841_0_0_0_Types[] = { (&TimeSpan_t595369841_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t595369841_0_0_0 = { 1, GenInst_TimeSpan_t595369841_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t1400222038_0_0_0_Types[] = { (&TypeTag_t1400222038_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t1400222038_0_0_0 = { 1, GenInst_TypeTag_t1400222038_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t3151463888_0_0_0_Types[] = { (&StrongName_t3151463888_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t3151463888_0_0_0 = { 1, GenInst_StrongName_t3151463888_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t1063146314_0_0_0_Types[] = { (&IBuiltInEvidence_t1063146314_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1063146314_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1063146314_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t674530215_0_0_0_Types[] = { (&IIdentityPermissionFactory_t674530215_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t674530215_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t674530215_0_0_0_Types };
static const RuntimeType* GenInst_WaitHandle_t2040275096_0_0_0_Types[] = { (&WaitHandle_t2040275096_0_0_0) };
extern const Il2CppGenericInst GenInst_WaitHandle_t2040275096_0_0_0 = { 1, GenInst_WaitHandle_t2040275096_0_0_0_Types };
static const RuntimeType* GenInst_IDisposable_t3181680589_0_0_0_Types[] = { (&IDisposable_t3181680589_0_0_0) };
extern const Il2CppGenericInst GenInst_IDisposable_t3181680589_0_0_0 = { 1, GenInst_IDisposable_t3181680589_0_0_0_Types };
static const RuntimeType* GenInst_MarshalByRefObject_t714030913_0_0_0_Types[] = { (&MarshalByRefObject_t714030913_0_0_0) };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t714030913_0_0_0 = { 1, GenInst_MarshalByRefObject_t714030913_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t206488804_0_0_0_Types[] = { (&DateTimeOffset_t206488804_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t206488804_0_0_0 = { 1, GenInst_DateTimeOffset_t206488804_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t1358777808_0_0_0_Types[] = { (&Version_t1358777808_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t1358777808_0_0_0 = { 1, GenInst_Version_t1358777808_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1843659261_0_0_0_Types[] = { (&BigInteger_t1843659261_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1843659261_0_0_0 = { 1, GenInst_BigInteger_t1843659261_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t3567143369_0_0_0_Types[] = { (&ByteU5BU5D_t3567143369_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3567143369_0_0_0 = { 1, GenInst_ByteU5BU5D_t3567143369_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2404841927_0_0_0_Types[] = { (&IList_1_t2404841927_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2404841927_0_0_0 = { 1, GenInst_IList_1_t2404841927_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2746367255_0_0_0_Types[] = { (&ICollection_1_t2746367255_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2746367255_0_0_0 = { 1, GenInst_ICollection_1_t2746367255_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2727134553_0_0_0_Types[] = { (&IEnumerable_1_t2727134553_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2727134553_0_0_0 = { 1, GenInst_IEnumerable_1_t2727134553_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t1117940400_0_0_0_Types[] = { (&X509Certificate_t1117940400_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t1117940400_0_0_0 = { 1, GenInst_X509Certificate_t1117940400_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t3618342850_0_0_0_Types[] = { (&IDeserializationCallback_t3618342850_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t3618342850_0_0_0 = { 1, GenInst_IDeserializationCallback_t3618342850_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t373382451_0_0_0_Types[] = { (&ClientCertificateType_t373382451_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t373382451_0_0_0 = { 1, GenInst_ClientCertificateType_t373382451_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3397783615_0_0_0_Types[] = { (&KeyValuePair_2_t3397783615_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3397783615_0_0_0 = { 1, GenInst_KeyValuePair_2_t3397783615_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2440219994_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2440219994_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t3397783615_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t2440219994_0_0_0), (&KeyValuePair_2_t3397783615_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t3397783615_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t3397783615_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t2440219994_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1601545872_0_0_0_Types[] = { (&KeyValuePair_2_t1601545872_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1601545872_0_0_0 = { 1, GenInst_KeyValuePair_2_t1601545872_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t1601545872_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t2440219994_0_0_0), (&KeyValuePair_2_t1601545872_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t1601545872_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t1601545872_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t261084987_0_0_0_Types[] = { (&X509ChainStatus_t261084987_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t261084987_0_0_0 = { 1, GenInst_X509ChainStatus_t261084987_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t1485804594_0_0_0_Types[] = { (&Capture_t1485804594_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t1485804594_0_0_0 = { 1, GenInst_Capture_t1485804594_0_0_0_Types };
static const RuntimeType* GenInst_Group_t1812461930_0_0_0_Types[] = { (&Group_t1812461930_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t1812461930_0_0_0 = { 1, GenInst_Group_t1812461930_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t3196564960_0_0_0_Types[] = { (&Mark_t3196564960_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t3196564960_0_0_0 = { 1, GenInst_Mark_t3196564960_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t3530988980_0_0_0_Types[] = { (&UriScheme_t3530988980_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t3530988980_0_0_0 = { 1, GenInst_UriScheme_t3530988980_0_0_0_Types };
static const RuntimeType* GenInst_Link_t890348390_0_0_0_Types[] = { (&Link_t890348390_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t890348390_0_0_0 = { 1, GenInst_Link_t890348390_0_0_0_Types };
static const RuntimeType* GenInst_AsyncOperation_t3797817720_0_0_0_Types[] = { (&AsyncOperation_t3797817720_0_0_0) };
extern const Il2CppGenericInst GenInst_AsyncOperation_t3797817720_0_0_0 = { 1, GenInst_AsyncOperation_t3797817720_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t362346687_0_0_0_Types[] = { (&Camera_t362346687_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t362346687_0_0_0 = { 1, GenInst_Camera_t362346687_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t3297694025_0_0_0_Types[] = { (&Behaviour_t3297694025_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t3297694025_0_0_0 = { 1, GenInst_Behaviour_t3297694025_0_0_0_Types };
static const RuntimeType* GenInst_Component_t2335505321_0_0_0_Types[] = { (&Component_t2335505321_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t2335505321_0_0_0 = { 1, GenInst_Component_t2335505321_0_0_0_Types };
static const RuntimeType* GenInst_Object_t3645472222_0_0_0_Types[] = { (&Object_t3645472222_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t3645472222_0_0_0 = { 1, GenInst_Object_t3645472222_0_0_0_Types };
static const RuntimeType* GenInst_Display_t81008966_0_0_0_Types[] = { (&Display_t81008966_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t81008966_0_0_0 = { 1, GenInst_Display_t81008966_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t1233380954_0_0_0_Types[] = { (&Keyframe_t1233380954_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t1233380954_0_0_0 = { 1, GenInst_Keyframe_t1233380954_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t3932393085_0_0_0_Types[] = { (&Vector3_t3932393085_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t3932393085_0_0_0 = { 1, GenInst_Vector3_t3932393085_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t1900979187_0_0_0_Types[] = { (&Vector4_t1900979187_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t1900979187_0_0_0 = { 1, GenInst_Vector4_t1900979187_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2246369278_0_0_0_Types[] = { (&Vector2_t2246369278_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2246369278_0_0_0 = { 1, GenInst_Vector2_t2246369278_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t662424039_0_0_0_Types[] = { (&Color32_t662424039_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t662424039_0_0_0 = { 1, GenInst_Color32_t662424039_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t379060559_0_0_0_Types[] = { (&Playable_t379060559_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t379060559_0_0_0 = { 1, GenInst_Playable_t379060559_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t2773044158_0_0_0_Types[] = { (&PlayableOutput_t2773044158_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t2773044158_0_0_0 = { 1, GenInst_PlayableOutput_t2773044158_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1083318001_0_0_0_LoadSceneMode_t3029863218_0_0_0_Types[] = { (&Scene_t1083318001_0_0_0), (&LoadSceneMode_t3029863218_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1083318001_0_0_0_LoadSceneMode_t3029863218_0_0_0 = { 2, GenInst_Scene_t1083318001_0_0_0_LoadSceneMode_t3029863218_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1083318001_0_0_0_Types[] = { (&Scene_t1083318001_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1083318001_0_0_0 = { 1, GenInst_Scene_t1083318001_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1083318001_0_0_0_Scene_t1083318001_0_0_0_Types[] = { (&Scene_t1083318001_0_0_0), (&Scene_t1083318001_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1083318001_0_0_0_Scene_t1083318001_0_0_0 = { 2, GenInst_Scene_t1083318001_0_0_0_Scene_t1083318001_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t2782010525_0_0_0_Types[] = { (&SpriteAtlas_t2782010525_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t2782010525_0_0_0 = { 1, GenInst_SpriteAtlas_t2782010525_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t4131774042_0_0_0_Types[] = { (&DisallowMultipleComponent_t4131774042_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t4131774042_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t4131774042_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t3130080784_0_0_0_Types[] = { (&Attribute_t3130080784_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t3130080784_0_0_0 = { 1, GenInst_Attribute_t3130080784_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t1312228502_0_0_0_Types[] = { (&_Attribute_t1312228502_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t1312228502_0_0_0 = { 1, GenInst__Attribute_t1312228502_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t2915138156_0_0_0_Types[] = { (&ExecuteInEditMode_t2915138156_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t2915138156_0_0_0 = { 1, GenInst_ExecuteInEditMode_t2915138156_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t3219866953_0_0_0_Types[] = { (&RequireComponent_t3219866953_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t3219866953_0_0_0 = { 1, GenInst_RequireComponent_t3219866953_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t3525137541_0_0_0_Types[] = { (&HitInfo_t3525137541_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t3525137541_0_0_0 = { 1, GenInst_HitInfo_t3525137541_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t1968425864_0_0_0_Types[] = { (&PersistentCall_t1968425864_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t1968425864_0_0_0 = { 1, GenInst_PersistentCall_t1968425864_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t3648497698_0_0_0_Types[] = { (&BaseInvokableCall_t3648497698_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t3648497698_0_0_0 = { 1, GenInst_BaseInvokableCall_t3648497698_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t266095197_0_0_0_Types[] = { (&WorkRequest_t266095197_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t266095197_0_0_0 = { 1, GenInst_WorkRequest_t266095197_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t2941134609_0_0_0_Types[] = { (&PlayableBinding_t2941134609_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t2941134609_0_0_0 = { 1, GenInst_PlayableBinding_t2941134609_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t2772358345_0_0_0_Types[] = { (&MessageEventArgs_t2772358345_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t2772358345_0_0_0 = { 1, GenInst_MessageEventArgs_t2772358345_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t675444414_0_0_0_Types[] = { (&MessageTypeSubscribers_t675444414_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t675444414_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t675444414_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t675444414_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&MessageTypeSubscribers_t675444414_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t675444414_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t675444414_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1433439652_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2055206595_0_0_0_Types[] = { (&KeyValuePair_2_t2055206595_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2055206595_0_0_0 = { 1, GenInst_KeyValuePair_2_t2055206595_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2055206595_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2055206595_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2055206595_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2055206595_0_0_0_Types };
static const RuntimeType* GenInst_WeakReference_t1433439652_0_0_0_Types[] = { (&WeakReference_t1433439652_0_0_0) };
extern const Il2CppGenericInst GenInst_WeakReference_t1433439652_0_0_0 = { 1, GenInst_WeakReference_t1433439652_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1433439652_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2936619059_0_0_0_Types[] = { (&KeyValuePair_2_t2936619059_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2936619059_0_0_0 = { 1, GenInst_KeyValuePair_2_t2936619059_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_KeyValuePair_2_t2936619059_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1433439652_0_0_0), (&KeyValuePair_2_t2936619059_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_KeyValuePair_2_t2936619059_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_KeyValuePair_2_t2936619059_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t1466291965_0_0_0_Types[] = { (&Rigidbody2D_t1466291965_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1466291965_0_0_0 = { 1, GenInst_Rigidbody2D_t1466291965_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2643454529_0_0_0_Types[] = { (&Font_t2643454529_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2643454529_0_0_0 = { 1, GenInst_Font_t2643454529_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t2944109340_0_0_0_Types[] = { (&UIVertex_t2944109340_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t2944109340_0_0_0 = { 1, GenInst_UIVertex_t2944109340_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t839829031_0_0_0_Types[] = { (&UICharInfo_t839829031_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t839829031_0_0_0 = { 1, GenInst_UICharInfo_t839829031_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t2156392613_0_0_0_Types[] = { (&UILineInfo_t2156392613_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t2156392613_0_0_0 = { 1, GenInst_UILineInfo_t2156392613_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t1463126785_0_0_0_Types[] = { (&AnimationClipPlayable_t1463126785_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t1463126785_0_0_0 = { 1, GenInst_AnimationClipPlayable_t1463126785_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t1879541177_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t1879541177_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t1879541177_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t1879541177_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t4125999351_0_0_0_Types[] = { (&AnimationMixerPlayable_t4125999351_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t4125999351_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t4125999351_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t999774996_0_0_0_Types[] = { (&AnimationOffsetPlayable_t999774996_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t999774996_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t999774996_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t4278763719_0_0_0_Types[] = { (&AnimatorControllerPlayable_t4278763719_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t4278763719_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t4278763719_0_0_0_Types };
static const RuntimeType* GenInst_AudioSpatializerExtensionDefinition_t2663594034_0_0_0_Types[] = { (&AudioSpatializerExtensionDefinition_t2663594034_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSpatializerExtensionDefinition_t2663594034_0_0_0 = { 1, GenInst_AudioSpatializerExtensionDefinition_t2663594034_0_0_0_Types };
static const RuntimeType* GenInst_AudioAmbisonicExtensionDefinition_t4031934303_0_0_0_Types[] = { (&AudioAmbisonicExtensionDefinition_t4031934303_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioAmbisonicExtensionDefinition_t4031934303_0_0_0 = { 1, GenInst_AudioAmbisonicExtensionDefinition_t4031934303_0_0_0_Types };
static const RuntimeType* GenInst_AudioSourceExtension_t4018014667_0_0_0_Types[] = { (&AudioSourceExtension_t4018014667_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSourceExtension_t4018014667_0_0_0 = { 1, GenInst_AudioSourceExtension_t4018014667_0_0_0_Types };
static const RuntimeType* GenInst_ScriptableObject_t1324617639_0_0_0_Types[] = { (&ScriptableObject_t1324617639_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptableObject_t1324617639_0_0_0 = { 1, GenInst_ScriptableObject_t1324617639_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t3997886707_0_0_0_Types[] = { (&AudioMixerPlayable_t3997886707_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t3997886707_0_0_0 = { 1, GenInst_AudioMixerPlayable_t3997886707_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t2374979939_0_0_0_Types[] = { (&AudioClipPlayable_t2374979939_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t2374979939_0_0_0 = { 1, GenInst_AudioClipPlayable_t2374979939_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2440219994_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t2440219994_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2440219994_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t2440219994_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2440219994_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t2440219994_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2440219994_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t2440219994_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t1703388966_0_0_0_Types[] = { (&AchievementDescription_t1703388966_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t1703388966_0_0_0 = { 1, GenInst_AchievementDescription_t1703388966_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t1992040264_0_0_0_Types[] = { (&IAchievementDescription_t1992040264_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1992040264_0_0_0 = { 1, GenInst_IAchievementDescription_t1992040264_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t2802009794_0_0_0_Types[] = { (&UserProfile_t2802009794_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t2802009794_0_0_0 = { 1, GenInst_UserProfile_t2802009794_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t2218596927_0_0_0_Types[] = { (&IUserProfile_t2218596927_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t2218596927_0_0_0 = { 1, GenInst_IUserProfile_t2218596927_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t3922680439_0_0_0_Types[] = { (&GcLeaderboard_t3922680439_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t3922680439_0_0_0 = { 1, GenInst_GcLeaderboard_t3922680439_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t185225881_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t185225881_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t185225881_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t185225881_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t2787719602_0_0_0_Types[] = { (&IAchievementU5BU5D_t2787719602_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2787719602_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2787719602_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t3527372131_0_0_0_Types[] = { (&IAchievement_t3527372131_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t3527372131_0_0_0 = { 1, GenInst_IAchievement_t3527372131_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t1576042669_0_0_0_Types[] = { (&GcAchievementData_t1576042669_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1576042669_0_0_0 = { 1, GenInst_GcAchievementData_t1576042669_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t1548866956_0_0_0_Types[] = { (&Achievement_t1548866956_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t1548866956_0_0_0 = { 1, GenInst_Achievement_t1548866956_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t3386159579_0_0_0_Types[] = { (&IScoreU5BU5D_t3386159579_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3386159579_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3386159579_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t2300007342_0_0_0_Types[] = { (&IScore_t2300007342_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t2300007342_0_0_0 = { 1, GenInst_IScore_t2300007342_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t2566310542_0_0_0_Types[] = { (&GcScoreData_t2566310542_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t2566310542_0_0_0 = { 1, GenInst_GcScoreData_t2566310542_0_0_0_Types };
static const RuntimeType* GenInst_Score_t1607899041_0_0_0_Types[] = { (&Score_t1607899041_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t1607899041_0_0_0 = { 1, GenInst_Score_t1607899041_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t1348948454_0_0_0_Types[] = { (&IUserProfileU5BU5D_t1348948454_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1348948454_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t1348948454_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t1561297139_0_0_0_Types[] = { (&GUILayoutOption_t1561297139_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t1561297139_0_0_0 = { 1, GenInst_GUILayoutOption_t1561297139_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&LayoutCache_t2616876984_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0 = { 2, GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1339419795_0_0_0_Types[] = { (&KeyValuePair_2_t1339419795_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1339419795_0_0_0 = { 1, GenInst_KeyValuePair_2_t1339419795_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1339419795_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t1339419795_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1339419795_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1339419795_0_0_0_Types };
static const RuntimeType* GenInst_LayoutCache_t2616876984_0_0_0_Types[] = { (&LayoutCache_t2616876984_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutCache_t2616876984_0_0_0 = { 1, GenInst_LayoutCache_t2616876984_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&LayoutCache_t2616876984_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3404269591_0_0_0_Types[] = { (&KeyValuePair_2_t3404269591_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3404269591_0_0_0 = { 1, GenInst_KeyValuePair_2_t3404269591_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_KeyValuePair_2_t3404269591_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&LayoutCache_t2616876984_0_0_0), (&KeyValuePair_2_t3404269591_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_KeyValuePair_2_t3404269591_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_KeyValuePair_2_t3404269591_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t3231862407_0_0_0_Types[] = { (&GUILayoutEntry_t3231862407_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3231862407_0_0_0 = { 1, GenInst_GUILayoutEntry_t3231862407_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_IntPtr_t_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_IntPtr_t_0_0_0_Boolean_t2440219994_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_IntPtr_t_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t214279536_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Exception_t214279536_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t214279536_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Exception_t214279536_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t3615497928_0_0_0_Types[] = { (&GUIStyle_t3615497928_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t3615497928_0_0_0 = { 1, GenInst_GUIStyle_t3615497928_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t3615497928_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t3615497928_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2776823806_0_0_0_Types[] = { (&KeyValuePair_2_t2776823806_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2776823806_0_0_0 = { 1, GenInst_KeyValuePair_2_t2776823806_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_KeyValuePair_2_t2776823806_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t3615497928_0_0_0), (&KeyValuePair_2_t2776823806_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_KeyValuePair_2_t2776823806_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_KeyValuePair_2_t2776823806_0_0_0_Types };
static const RuntimeType* GenInst_Particle_t1283715429_0_0_0_Types[] = { (&Particle_t1283715429_0_0_0) };
extern const Il2CppGenericInst GenInst_Particle_t1283715429_0_0_0 = { 1, GenInst_Particle_t1283715429_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t3372097066_0_0_0_Types[] = { (&RaycastHit_t3372097066_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t3372097066_0_0_0 = { 1, GenInst_RaycastHit_t3372097066_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint_t99991485_0_0_0_Types[] = { (&ContactPoint_t99991485_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint_t99991485_0_0_0 = { 1, GenInst_ContactPoint_t99991485_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t1423129782_0_0_0_Types[] = { (&EventSystem_t1423129782_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t1423129782_0_0_0 = { 1, GenInst_EventSystem_t1423129782_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t1053087876_0_0_0_Types[] = { (&UIBehaviour_t1053087876_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t1053087876_0_0_0 = { 1, GenInst_UIBehaviour_t1053087876_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t4008345588_0_0_0_Types[] = { (&MonoBehaviour_t4008345588_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t4008345588_0_0_0 = { 1, GenInst_MonoBehaviour_t4008345588_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t893951129_0_0_0_Types[] = { (&BaseInputModule_t893951129_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t893951129_0_0_0 = { 1, GenInst_BaseInputModule_t893951129_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t1958542877_0_0_0_Types[] = { (&RaycastResult_t1958542877_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t1958542877_0_0_0 = { 1, GenInst_RaycastResult_t1958542877_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t1423095504_0_0_0_Types[] = { (&IDeselectHandler_t1423095504_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t1423095504_0_0_0 = { 1, GenInst_IDeselectHandler_t1423095504_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t2679299517_0_0_0_Types[] = { (&IEventSystemHandler_t2679299517_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2679299517_0_0_0 = { 1, GenInst_IEventSystemHandler_t2679299517_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t58535312_0_0_0_Types[] = { (&List_1_t58535312_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t58535312_0_0_0 = { 1, GenInst_List_1_t58535312_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2226230279_0_0_0_Types[] = { (&List_1_t2226230279_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2226230279_0_0_0 = { 1, GenInst_List_1_t2226230279_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t4009708412_0_0_0_Types[] = { (&List_1_t4009708412_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t4009708412_0_0_0 = { 1, GenInst_List_1_t4009708412_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t637179496_0_0_0_Types[] = { (&ISelectHandler_t637179496_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t637179496_0_0_0 = { 1, GenInst_ISelectHandler_t637179496_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t2460995416_0_0_0_Types[] = { (&BaseRaycaster_t2460995416_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2460995416_0_0_0 = { 1, GenInst_BaseRaycaster_t2460995416_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t2441154801_0_0_0_Types[] = { (&Entry_t2441154801_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t2441154801_0_0_0 = { 1, GenInst_Entry_t2441154801_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t3589491243_0_0_0_Types[] = { (&BaseEventData_t3589491243_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t3589491243_0_0_0 = { 1, GenInst_BaseEventData_t3589491243_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t3124626600_0_0_0_Types[] = { (&IPointerEnterHandler_t3124626600_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t3124626600_0_0_0 = { 1, GenInst_IPointerEnterHandler_t3124626600_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t1395786574_0_0_0_Types[] = { (&IPointerExitHandler_t1395786574_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t1395786574_0_0_0 = { 1, GenInst_IPointerExitHandler_t1395786574_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t44025902_0_0_0_Types[] = { (&IPointerDownHandler_t44025902_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t44025902_0_0_0 = { 1, GenInst_IPointerDownHandler_t44025902_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t102393737_0_0_0_Types[] = { (&IPointerUpHandler_t102393737_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t102393737_0_0_0 = { 1, GenInst_IPointerUpHandler_t102393737_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t4047079811_0_0_0_Types[] = { (&IPointerClickHandler_t4047079811_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t4047079811_0_0_0 = { 1, GenInst_IPointerClickHandler_t4047079811_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t818614831_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t818614831_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t818614831_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t818614831_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t2428313557_0_0_0_Types[] = { (&IBeginDragHandler_t2428313557_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t2428313557_0_0_0 = { 1, GenInst_IBeginDragHandler_t2428313557_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t2984229372_0_0_0_Types[] = { (&IDragHandler_t2984229372_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t2984229372_0_0_0 = { 1, GenInst_IDragHandler_t2984229372_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t1280646970_0_0_0_Types[] = { (&IEndDragHandler_t1280646970_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1280646970_0_0_0 = { 1, GenInst_IEndDragHandler_t1280646970_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t1307228819_0_0_0_Types[] = { (&IDropHandler_t1307228819_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t1307228819_0_0_0 = { 1, GenInst_IDropHandler_t1307228819_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t194649380_0_0_0_Types[] = { (&IScrollHandler_t194649380_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t194649380_0_0_0 = { 1, GenInst_IScrollHandler_t194649380_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t2808857881_0_0_0_Types[] = { (&IUpdateSelectedHandler_t2808857881_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t2808857881_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t2808857881_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t1328422412_0_0_0_Types[] = { (&IMoveHandler_t1328422412_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t1328422412_0_0_0 = { 1, GenInst_IMoveHandler_t1328422412_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t3368026263_0_0_0_Types[] = { (&ISubmitHandler_t3368026263_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t3368026263_0_0_0 = { 1, GenInst_ISubmitHandler_t3368026263_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t1174209038_0_0_0_Types[] = { (&ICancelHandler_t1174209038_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1174209038_0_0_0 = { 1, GenInst_ICancelHandler_t1174209038_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3580444445_0_0_0_Types[] = { (&Transform_t3580444445_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3580444445_0_0_0 = { 1, GenInst_Transform_t3580444445_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t2881801184_0_0_0_Types[] = { (&GameObject_t2881801184_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t2881801184_0_0_0 = { 1, GenInst_GameObject_t2881801184_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t2888570827_0_0_0_Types[] = { (&BaseInput_t2888570827_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t2888570827_0_0_0 = { 1, GenInst_BaseInput_t2888570827_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&PointerEventData_t507157825_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0 = { 2, GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t507157825_0_0_0_Types[] = { (&PointerEventData_t507157825_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t507157825_0_0_0 = { 1, GenInst_PointerEventData_t507157825_0_0_0_Types };
static const RuntimeType* GenInst_AbstractEventData_t3911574392_0_0_0_Types[] = { (&AbstractEventData_t3911574392_0_0_0) };
extern const Il2CppGenericInst GenInst_AbstractEventData_t3911574392_0_0_0 = { 1, GenInst_AbstractEventData_t3911574392_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&PointerEventData_t507157825_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1294550432_0_0_0_Types[] = { (&KeyValuePair_2_t1294550432_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1294550432_0_0_0 = { 1, GenInst_KeyValuePair_2_t1294550432_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_KeyValuePair_2_t1294550432_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&PointerEventData_t507157825_0_0_0), (&KeyValuePair_2_t1294550432_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_KeyValuePair_2_t1294550432_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_KeyValuePair_2_t1294550432_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_PointerEventData_t507157825_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&PointerEventData_t507157825_0_0_0), (&PointerEventData_t507157825_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_PointerEventData_t507157825_0_0_0 = { 3, GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_PointerEventData_t507157825_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t2740850036_0_0_0_Types[] = { (&ButtonState_t2740850036_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t2740850036_0_0_0 = { 1, GenInst_ButtonState_t2740850036_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t218385889_0_0_0_Types[] = { (&RaycastHit2D_t218385889_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t218385889_0_0_0 = { 1, GenInst_RaycastHit2D_t218385889_0_0_0_Types };
static const RuntimeType* GenInst_Color_t1361298052_0_0_0_Types[] = { (&Color_t1361298052_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t1361298052_0_0_0 = { 1, GenInst_Color_t1361298052_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2313639799_0_0_0_Types[] = { (&ICanvasElement_t2313639799_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2313639799_0_0_0 = { 1, GenInst_ICanvasElement_t2313639799_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&ICanvasElement_t2313639799_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&ICanvasElement_t2313639799_0_0_0), (&Int32_t3515577538_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t3446061115_0_0_0_Types[] = { (&ColorBlock_t3446061115_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t3446061115_0_0_0 = { 1, GenInst_ColorBlock_t3446061115_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t436826826_0_0_0_Types[] = { (&OptionData_t436826826_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t436826826_0_0_0 = { 1, GenInst_OptionData_t436826826_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t2659469254_0_0_0_Types[] = { (&DropdownItem_t2659469254_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t2659469254_0_0_0 = { 1, GenInst_DropdownItem_t2659469254_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t2756583341_0_0_0_Types[] = { (&FloatTween_t2756583341_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t2756583341_0_0_0 = { 1, GenInst_FloatTween_t2756583341_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t2497061588_0_0_0_Types[] = { (&Sprite_t2497061588_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t2497061588_0_0_0 = { 1, GenInst_Sprite_t2497061588_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2667390772_0_0_0_Types[] = { (&Canvas_t2667390772_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2667390772_0_0_0 = { 1, GenInst_Canvas_t2667390772_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t46626567_0_0_0_Types[] = { (&List_1_t46626567_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t46626567_0_0_0 = { 1, GenInst_List_1_t46626567_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_Types[] = { (&Font_t2643454529_0_0_0), (&HashSet_1_t3142691717_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0 = { 2, GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_Types };
static const RuntimeType* GenInst_Text_t1445358712_0_0_0_Types[] = { (&Text_t1445358712_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t1445358712_0_0_0 = { 1, GenInst_Text_t1445358712_0_0_0_Types };
static const RuntimeType* GenInst_Link_t1783679914_0_0_0_Types[] = { (&Link_t1783679914_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t1783679914_0_0_0 = { 1, GenInst_Link_t1783679914_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t368149833_0_0_0_Types[] = { (&ILayoutElement_t368149833_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t368149833_0_0_0 = { 1, GenInst_ILayoutElement_t368149833_0_0_0_Types };
static const RuntimeType* GenInst_MaskableGraphic_t4128200649_0_0_0_Types[] = { (&MaskableGraphic_t4128200649_0_0_0) };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t4128200649_0_0_0 = { 1, GenInst_MaskableGraphic_t4128200649_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t3544737781_0_0_0_Types[] = { (&IClippable_t3544737781_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t3544737781_0_0_0 = { 1, GenInst_IClippable_t3544737781_0_0_0_Types };
static const RuntimeType* GenInst_IMaskable_t997864649_0_0_0_Types[] = { (&IMaskable_t997864649_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaskable_t997864649_0_0_0 = { 1, GenInst_IMaskable_t997864649_0_0_0_Types };
static const RuntimeType* GenInst_IMaterialModifier_t805903918_0_0_0_Types[] = { (&IMaterialModifier_t805903918_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t805903918_0_0_0 = { 1, GenInst_IMaterialModifier_t805903918_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2696902410_0_0_0_Types[] = { (&Graphic_t2696902410_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2696902410_0_0_0 = { 1, GenInst_Graphic_t2696902410_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t3142691717_0_0_0_Types[] = { (&HashSet_1_t3142691717_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t3142691717_0_0_0 = { 1, GenInst_HashSet_1_t3142691717_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Font_t2643454529_0_0_0), (&HashSet_1_t3142691717_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3866178953_0_0_0_Types[] = { (&KeyValuePair_2_t3866178953_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3866178953_0_0_0 = { 1, GenInst_KeyValuePair_2_t3866178953_0_0_0_Types };
static const RuntimeType* GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_KeyValuePair_2_t3866178953_0_0_0_Types[] = { (&Font_t2643454529_0_0_0), (&HashSet_1_t3142691717_0_0_0), (&KeyValuePair_2_t3866178953_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_KeyValuePair_2_t3866178953_0_0_0 = { 3, GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_KeyValuePair_2_t3866178953_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t2856906502_0_0_0_Types[] = { (&ColorTween_t2856906502_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t2856906502_0_0_0 = { 1, GenInst_ColorTween_t2856906502_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_Types[] = { (&Canvas_t2667390772_0_0_0), (&IndexedSet_1_t1130276133_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0 = { 2, GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&Graphic_t2696902410_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Graphic_t2696902410_0_0_0), (&Int32_t3515577538_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t1130276133_0_0_0_Types[] = { (&IndexedSet_1_t1130276133_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1130276133_0_0_0 = { 1, GenInst_IndexedSet_1_t1130276133_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Canvas_t2667390772_0_0_0), (&IndexedSet_1_t1130276133_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3771799818_0_0_0_Types[] = { (&KeyValuePair_2_t3771799818_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3771799818_0_0_0 = { 1, GenInst_KeyValuePair_2_t3771799818_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_KeyValuePair_2_t3771799818_0_0_0_Types[] = { (&Canvas_t2667390772_0_0_0), (&IndexedSet_1_t1130276133_0_0_0), (&KeyValuePair_2_t3771799818_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_KeyValuePair_2_t3771799818_0_0_0 = { 3, GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_KeyValuePair_2_t3771799818_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1169793209_0_0_0_Types[] = { (&KeyValuePair_2_t1169793209_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1169793209_0_0_0 = { 1, GenInst_KeyValuePair_2_t1169793209_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1169793209_0_0_0_Types[] = { (&Graphic_t2696902410_0_0_0), (&Int32_t3515577538_0_0_0), (&KeyValuePair_2_t1169793209_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1169793209_0_0_0 = { 3, GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1169793209_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t223188984_0_0_0_Types[] = { (&KeyValuePair_2_t223188984_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t223188984_0_0_0 = { 1, GenInst_KeyValuePair_2_t223188984_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t223188984_0_0_0_Types[] = { (&ICanvasElement_t2313639799_0_0_0), (&Int32_t3515577538_0_0_0), (&KeyValuePair_2_t223188984_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t223188984_0_0_0 = { 3, GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t223188984_0_0_0_Types };
static const RuntimeType* GenInst_Type_t2837774015_0_0_0_Types[] = { (&Type_t2837774015_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t2837774015_0_0_0 = { 1, GenInst_Type_t2837774015_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t2931617579_0_0_0_Types[] = { (&FillMethod_t2931617579_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t2931617579_0_0_0 = { 1, GenInst_FillMethod_t2931617579_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t1122571944_0_0_0_Types[] = { (&ContentType_t1122571944_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t1122571944_0_0_0 = { 1, GenInst_ContentType_t1122571944_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t3638610376_0_0_0_Types[] = { (&LineType_t3638610376_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t3638610376_0_0_0 = { 1, GenInst_LineType_t3638610376_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t4187126303_0_0_0_Types[] = { (&InputType_t4187126303_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t4187126303_0_0_0 = { 1, GenInst_InputType_t4187126303_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t1103124205_0_0_0_Types[] = { (&TouchScreenKeyboardType_t1103124205_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t1103124205_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t1103124205_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t2769550230_0_0_0_Types[] = { (&CharacterValidation_t2769550230_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t2769550230_0_0_0 = { 1, GenInst_CharacterValidation_t2769550230_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t745540708_0_0_0_Types[] = { (&Mask_t745540708_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t745540708_0_0_0 = { 1, GenInst_Mask_t745540708_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasRaycastFilter_t2306748731_0_0_0_Types[] = { (&ICanvasRaycastFilter_t2306748731_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t2306748731_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t2306748731_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2419743799_0_0_0_Types[] = { (&List_1_t2419743799_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2419743799_0_0_0 = { 1, GenInst_List_1_t2419743799_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t2972282963_0_0_0_Types[] = { (&RectMask2D_t2972282963_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t2972282963_0_0_0 = { 1, GenInst_RectMask2D_t2972282963_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t386535878_0_0_0_Types[] = { (&IClipper_t386535878_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t386535878_0_0_0 = { 1, GenInst_IClipper_t386535878_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t351518758_0_0_0_Types[] = { (&List_1_t351518758_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t351518758_0_0_0 = { 1, GenInst_List_1_t351518758_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t2227424972_0_0_0_Types[] = { (&Navigation_t2227424972_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t2227424972_0_0_0 = { 1, GenInst_Navigation_t2227424972_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3883058983_0_0_0_Types[] = { (&Link_t3883058983_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3883058983_0_0_0 = { 1, GenInst_Link_t3883058983_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1205246216_0_0_0_Types[] = { (&Direction_t1205246216_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1205246216_0_0_0 = { 1, GenInst_Direction_t1205246216_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t1875500212_0_0_0_Types[] = { (&Selectable_t1875500212_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t1875500212_0_0_0 = { 1, GenInst_Selectable_t1875500212_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t2703339633_0_0_0_Types[] = { (&Transition_t2703339633_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t2703339633_0_0_0 = { 1, GenInst_Transition_t2703339633_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t710398810_0_0_0_Types[] = { (&SpriteState_t710398810_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t710398810_0_0_0 = { 1, GenInst_SpriteState_t710398810_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t2669816671_0_0_0_Types[] = { (&CanvasGroup_t2669816671_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t2669816671_0_0_0 = { 1, GenInst_CanvasGroup_t2669816671_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t299841542_0_0_0_Types[] = { (&Direction_t299841542_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t299841542_0_0_0 = { 1, GenInst_Direction_t299841542_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t930024495_0_0_0_Types[] = { (&MatEntry_t930024495_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t930024495_0_0_0 = { 1, GenInst_MatEntry_t930024495_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t403198564_0_0_0_Types[] = { (&Toggle_t403198564_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t403198564_0_0_0 = { 1, GenInst_Toggle_t403198564_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t403198564_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Toggle_t403198564_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t403198564_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Toggle_t403198564_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&IClipper_t386535878_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&IClipper_t386535878_0_0_0), (&Int32_t3515577538_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1494153101_0_0_0_Types[] = { (&KeyValuePair_2_t1494153101_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1494153101_0_0_0 = { 1, GenInst_KeyValuePair_2_t1494153101_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1494153101_0_0_0_Types[] = { (&IClipper_t386535878_0_0_0), (&Int32_t3515577538_0_0_0), (&KeyValuePair_2_t1494153101_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1494153101_0_0_0 = { 3, GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1494153101_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t3475277687_0_0_0_Types[] = { (&AspectMode_t3475277687_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t3475277687_0_0_0 = { 1, GenInst_AspectMode_t3475277687_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t3582979200_0_0_0_Types[] = { (&FitMode_t3582979200_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t3582979200_0_0_0 = { 1, GenInst_FitMode_t3582979200_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t1161610249_0_0_0_Types[] = { (&RectTransform_t1161610249_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t1161610249_0_0_0 = { 1, GenInst_RectTransform_t1161610249_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t2288684892_0_0_0_Types[] = { (&LayoutRebuilder_t2288684892_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2288684892_0_0_0 = { 1, GenInst_LayoutRebuilder_t2288684892_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t368149833_0_0_0_Single_t897798503_0_0_0_Types[] = { (&ILayoutElement_t368149833_0_0_0), (&Single_t897798503_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t368149833_0_0_0_Single_t897798503_0_0_0 = { 2, GenInst_ILayoutElement_t368149833_0_0_0_Single_t897798503_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t897798503_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t897798503_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t897798503_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t897798503_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1311628880_0_0_0_Types[] = { (&List_1_t1311628880_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1311628880_0_0_0 = { 1, GenInst_List_1_t1311628880_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2336627130_0_0_0_Types[] = { (&List_1_t2336627130_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2336627130_0_0_0 = { 1, GenInst_List_1_t2336627130_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3920572369_0_0_0_Types[] = { (&List_1_t3920572369_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3920572369_0_0_0 = { 1, GenInst_List_1_t3920572369_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3575182278_0_0_0_Types[] = { (&List_1_t3575182278_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3575182278_0_0_0 = { 1, GenInst_List_1_t3575182278_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t894813333_0_0_0_Types[] = { (&List_1_t894813333_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t894813333_0_0_0 = { 1, GenInst_List_1_t894813333_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t323345135_0_0_0_Types[] = { (&List_1_t323345135_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t323345135_0_0_0 = { 1, GenInst_List_1_t323345135_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3597842601_0_0_0_Types[] = { (&Link_t3597842601_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3597842601_0_0_0 = { 1, GenInst_Link_t3597842601_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t942592945_0_0_0_Types[] = { (&ARHitTestResult_t942592945_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t942592945_0_0_0 = { 1, GenInst_ARHitTestResult_t942592945_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResultType_t1760080916_0_0_0_Types[] = { (&ARHitTestResultType_t1760080916_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResultType_t1760080916_0_0_0 = { 1, GenInst_ARHitTestResultType_t1760080916_0_0_0_Types };
static const RuntimeType* GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Types[] = { (&Single_t897798503_0_0_0), (&Single_t897798503_0_0_0), (&Single_t897798503_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Single_t897798503_0_0_0 = { 3, GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Types };
static const RuntimeType* GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Types[] = { (&Single_t897798503_0_0_0), (&Single_t897798503_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0 = { 2, GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Types };
static const RuntimeType* GenInst_ParticleSystem_t3890943298_0_0_0_Types[] = { (&ParticleSystem_t3890943298_0_0_0) };
extern const Il2CppGenericInst GenInst_ParticleSystem_t3890943298_0_0_0 = { 1, GenInst_ParticleSystem_t3890943298_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t3125390466_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0 = { 2, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_Types };
static const RuntimeType* GenInst_ARPlaneAnchorGameObject_t3125390466_0_0_0_Types[] = { (&ARPlaneAnchorGameObject_t3125390466_0_0_0) };
extern const Il2CppGenericInst GenInst_ARPlaneAnchorGameObject_t3125390466_0_0_0 = { 1, GenInst_ARPlaneAnchorGameObject_t3125390466_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t3125390466_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2286716344_0_0_0_Types[] = { (&KeyValuePair_2_t2286716344_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2286716344_0_0_0 = { 1, GenInst_KeyValuePair_2_t2286716344_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_KeyValuePair_2_t2286716344_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t3125390466_0_0_0), (&KeyValuePair_2_t2286716344_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_KeyValuePair_2_t2286716344_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_KeyValuePair_2_t2286716344_0_0_0_Types };
static const RuntimeType* GenInst_UnityARSessionRunOption_t572719712_0_0_0_Types[] = { (&UnityARSessionRunOption_t572719712_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARSessionRunOption_t572719712_0_0_0 = { 1, GenInst_UnityARSessionRunOption_t572719712_0_0_0_Types };
static const RuntimeType* GenInst_UnityARAlignment_t2001134742_0_0_0_Types[] = { (&UnityARAlignment_t2001134742_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARAlignment_t2001134742_0_0_0 = { 1, GenInst_UnityARAlignment_t2001134742_0_0_0_Types };
static const RuntimeType* GenInst_UnityARPlaneDetection_t1240827406_0_0_0_Types[] = { (&UnityARPlaneDetection_t1240827406_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARPlaneDetection_t1240827406_0_0_0 = { 1, GenInst_UnityARPlaneDetection_t1240827406_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t361495900_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t361495900_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t361495900_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t361495900_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3921660370_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m3921660370_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3921660370_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3921660370_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3350998347_gp_0_0_0_0_Array_Sort_m3350998347_gp_0_0_0_0_Types[] = { (&Array_Sort_m3350998347_gp_0_0_0_0), (&Array_Sort_m3350998347_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3350998347_gp_0_0_0_0_Array_Sort_m3350998347_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3350998347_gp_0_0_0_0_Array_Sort_m3350998347_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m689900404_gp_0_0_0_0_Array_Sort_m689900404_gp_1_0_0_0_Types[] = { (&Array_Sort_m689900404_gp_0_0_0_0), (&Array_Sort_m689900404_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m689900404_gp_0_0_0_0_Array_Sort_m689900404_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m689900404_gp_0_0_0_0_Array_Sort_m689900404_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4285269846_gp_0_0_0_0_Types[] = { (&Array_Sort_m4285269846_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4285269846_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4285269846_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4285269846_gp_0_0_0_0_Array_Sort_m4285269846_gp_0_0_0_0_Types[] = { (&Array_Sort_m4285269846_gp_0_0_0_0), (&Array_Sort_m4285269846_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4285269846_gp_0_0_0_0_Array_Sort_m4285269846_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4285269846_gp_0_0_0_0_Array_Sort_m4285269846_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1330797580_gp_0_0_0_0_Types[] = { (&Array_Sort_m1330797580_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1330797580_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1330797580_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1330797580_gp_0_0_0_0_Array_Sort_m1330797580_gp_1_0_0_0_Types[] = { (&Array_Sort_m1330797580_gp_0_0_0_0), (&Array_Sort_m1330797580_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1330797580_gp_0_0_0_0_Array_Sort_m1330797580_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1330797580_gp_0_0_0_0_Array_Sort_m1330797580_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m869698846_gp_0_0_0_0_Array_Sort_m869698846_gp_0_0_0_0_Types[] = { (&Array_Sort_m869698846_gp_0_0_0_0), (&Array_Sort_m869698846_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m869698846_gp_0_0_0_0_Array_Sort_m869698846_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m869698846_gp_0_0_0_0_Array_Sort_m869698846_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2635216824_gp_0_0_0_0_Array_Sort_m2635216824_gp_1_0_0_0_Types[] = { (&Array_Sort_m2635216824_gp_0_0_0_0), (&Array_Sort_m2635216824_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2635216824_gp_0_0_0_0_Array_Sort_m2635216824_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2635216824_gp_0_0_0_0_Array_Sort_m2635216824_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3791969856_gp_0_0_0_0_Types[] = { (&Array_Sort_m3791969856_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3791969856_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3791969856_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3791969856_gp_0_0_0_0_Array_Sort_m3791969856_gp_0_0_0_0_Types[] = { (&Array_Sort_m3791969856_gp_0_0_0_0), (&Array_Sort_m3791969856_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3791969856_gp_0_0_0_0_Array_Sort_m3791969856_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3791969856_gp_0_0_0_0_Array_Sort_m3791969856_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2053374032_gp_0_0_0_0_Types[] = { (&Array_Sort_m2053374032_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2053374032_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2053374032_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2053374032_gp_1_0_0_0_Types[] = { (&Array_Sort_m2053374032_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2053374032_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m2053374032_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2053374032_gp_0_0_0_0_Array_Sort_m2053374032_gp_1_0_0_0_Types[] = { (&Array_Sort_m2053374032_gp_0_0_0_0), (&Array_Sort_m2053374032_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2053374032_gp_0_0_0_0_Array_Sort_m2053374032_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2053374032_gp_0_0_0_0_Array_Sort_m2053374032_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3529074503_gp_0_0_0_0_Types[] = { (&Array_Sort_m3529074503_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3529074503_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3529074503_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1046918090_gp_0_0_0_0_Types[] = { (&Array_Sort_m1046918090_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1046918090_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1046918090_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m3074224534_gp_0_0_0_0_Types[] = { (&Array_qsort_m3074224534_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m3074224534_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3074224534_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m3074224534_gp_0_0_0_0_Array_qsort_m3074224534_gp_1_0_0_0_Types[] = { (&Array_qsort_m3074224534_gp_0_0_0_0), (&Array_qsort_m3074224534_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m3074224534_gp_0_0_0_0_Array_qsort_m3074224534_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m3074224534_gp_0_0_0_0_Array_qsort_m3074224534_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m182040922_gp_0_0_0_0_Types[] = { (&Array_compare_m182040922_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m182040922_gp_0_0_0_0 = { 1, GenInst_Array_compare_m182040922_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m3653394148_gp_0_0_0_0_Types[] = { (&Array_qsort_m3653394148_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m3653394148_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3653394148_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m1475766408_gp_0_0_0_0_Types[] = { (&Array_Resize_m1475766408_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m1475766408_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1475766408_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m1259694655_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m1259694655_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m1259694655_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m1259694655_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m1921540296_gp_0_0_0_0_Types[] = { (&Array_ForEach_m1921540296_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1921540296_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1921540296_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m2082766338_gp_0_0_0_0_Array_ConvertAll_m2082766338_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m2082766338_gp_0_0_0_0), (&Array_ConvertAll_m2082766338_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m2082766338_gp_0_0_0_0_Array_ConvertAll_m2082766338_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m2082766338_gp_0_0_0_0_Array_ConvertAll_m2082766338_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2403343833_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2403343833_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2403343833_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2403343833_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2322380597_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2322380597_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2322380597_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2322380597_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1895958268_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1895958268_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1895958268_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1895958268_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m2364170290_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m2364170290_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m2364170290_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m2364170290_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3411283154_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3411283154_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3411283154_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3411283154_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1488736383_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1488736383_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1488736383_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1488736383_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m1842855399_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m1842855399_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1842855399_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1842855399_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m619051682_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m619051682_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m619051682_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m619051682_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m625219582_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m625219582_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m625219582_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m625219582_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m863042731_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m863042731_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m863042731_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m863042731_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m1718101709_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m1718101709_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1718101709_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1718101709_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m1667751126_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m1667751126_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1667751126_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1667751126_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m4170047889_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m4170047889_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m4170047889_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m4170047889_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m1415329759_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m1415329759_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1415329759_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1415329759_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m4008997880_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m4008997880_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m4008997880_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m4008997880_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2739852194_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2739852194_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2739852194_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2739852194_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m641447484_gp_0_0_0_0_Types[] = { (&Array_FindAll_m641447484_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m641447484_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m641447484_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m3014889077_gp_0_0_0_0_Types[] = { (&Array_Exists_m3014889077_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m3014889077_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m3014889077_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m2697550731_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m2697550731_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m2697550731_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m2697550731_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m408926873_gp_0_0_0_0_Types[] = { (&Array_Find_m408926873_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m408926873_gp_0_0_0_0 = { 1, GenInst_Array_Find_m408926873_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m4132394418_gp_0_0_0_0_Types[] = { (&Array_FindLast_m4132394418_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m4132394418_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m4132394418_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t3560912388_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t3560912388_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3560912388_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3560912388_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t1444333090_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t1444333090_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t1444333090_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t1444333090_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3454464003_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t3454464003_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3454464003_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3454464003_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3296908666_gp_0_0_0_0_Types[] = { (&IList_1_t3296908666_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3296908666_gp_0_0_0_0 = { 1, GenInst_IList_1_t3296908666_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t842930506_gp_0_0_0_0_Types[] = { (&ICollection_1_t842930506_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t842930506_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t842930506_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t2719303306_gp_0_0_0_0_Types[] = { (&Nullable_1_t2719303306_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t2719303306_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2719303306_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t1340870152_gp_0_0_0_0_Types[] = { (&Comparer_1_t1340870152_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t1340870152_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1340870152_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t2305410469_gp_0_0_0_0_Types[] = { (&DefaultComparer_t2305410469_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2305410469_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2305410469_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t4291314091_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t4291314091_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t4291314091_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t4291314091_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_0_0_0_0), (&Dictionary_2_t3686457887_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2794658882_0_0_0_Types[] = { (&KeyValuePair_2_t2794658882_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2794658882_0_0_0 = { 1, GenInst_KeyValuePair_2_t2794658882_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1146410757_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_0_0_0_0), (&Dictionary_2_t3686457887_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m1146410757_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1146410757_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1146410757_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_0_0_0_0), (&Dictionary_2_t3686457887_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_0_0_0_0), (&Dictionary_2_t3686457887_gp_1_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 3, GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t2837955638_gp_0_0_0_0_ShimEnumerator_t2837955638_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t2837955638_gp_0_0_0_0), (&ShimEnumerator_t2837955638_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2837955638_gp_0_0_0_0_ShimEnumerator_t2837955638_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t2837955638_gp_0_0_0_0_ShimEnumerator_t2837955638_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3167388891_gp_0_0_0_0_Enumerator_t3167388891_gp_1_0_0_0_Types[] = { (&Enumerator_t3167388891_gp_0_0_0_0), (&Enumerator_t3167388891_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3167388891_gp_0_0_0_0_Enumerator_t3167388891_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3167388891_gp_0_0_0_0_Enumerator_t3167388891_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1888873938_0_0_0_Types[] = { (&KeyValuePair_2_t1888873938_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1888873938_0_0_0 = { 1, GenInst_KeyValuePair_2_t1888873938_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_Types[] = { (&ValueCollection_t4200311898_gp_0_0_0_0), (&ValueCollection_t4200311898_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t4200311898_gp_1_0_0_0_Types[] = { (&ValueCollection_t4200311898_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t4200311898_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t4200311898_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2729626900_gp_0_0_0_0_Enumerator_t2729626900_gp_1_0_0_0_Types[] = { (&Enumerator_t2729626900_gp_0_0_0_0), (&Enumerator_t2729626900_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2729626900_gp_0_0_0_0_Enumerator_t2729626900_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2729626900_gp_0_0_0_0_Enumerator_t2729626900_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2729626900_gp_1_0_0_0_Types[] = { (&Enumerator_t2729626900_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2729626900_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2729626900_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_Types[] = { (&ValueCollection_t4200311898_gp_0_0_0_0), (&ValueCollection_t4200311898_gp_1_0_0_0), (&ValueCollection_t4200311898_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_Types[] = { (&ValueCollection_t4200311898_gp_1_0_0_0), (&ValueCollection_t4200311898_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3507565976_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types[] = { (&DictionaryEntry_t3507565976_0_0_0), (&DictionaryEntry_t3507565976_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3507565976_0_0_0_DictionaryEntry_t3507565976_0_0_0 = { 2, GenInst_DictionaryEntry_t3507565976_0_0_0_DictionaryEntry_t3507565976_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_KeyValuePair_2_t2794658882_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_0_0_0_0), (&Dictionary_2_t3686457887_gp_1_0_0_0), (&KeyValuePair_2_t2794658882_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_KeyValuePair_2_t2794658882_0_0_0 = { 3, GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_KeyValuePair_2_t2794658882_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2794658882_0_0_0_KeyValuePair_2_t2794658882_0_0_0_Types[] = { (&KeyValuePair_2_t2794658882_0_0_0), (&KeyValuePair_2_t2794658882_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2794658882_0_0_0_KeyValuePair_2_t2794658882_0_0_0 = { 2, GenInst_KeyValuePair_2_t2794658882_0_0_0_KeyValuePair_2_t2794658882_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3686457887_gp_1_0_0_0_Types[] = { (&Dictionary_2_t3686457887_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3686457887_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3686457887_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t962836640_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t962836640_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t962836640_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t962836640_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t3887701940_gp_0_0_0_0_Types[] = { (&DefaultComparer_t3887701940_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3887701940_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3887701940_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t3218101675_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t3218101675_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3218101675_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3218101675_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1744688238_0_0_0_Types[] = { (&KeyValuePair_2_t1744688238_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744688238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744688238_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t188967788_gp_0_0_0_0_IDictionary_2_t188967788_gp_1_0_0_0_Types[] = { (&IDictionary_2_t188967788_gp_0_0_0_0), (&IDictionary_2_t188967788_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t188967788_gp_0_0_0_0_IDictionary_2_t188967788_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t188967788_gp_0_0_0_0_IDictionary_2_t188967788_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t759847414_gp_0_0_0_0_KeyValuePair_2_t759847414_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t759847414_gp_0_0_0_0), (&KeyValuePair_2_t759847414_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t759847414_gp_0_0_0_0_KeyValuePair_2_t759847414_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t759847414_gp_0_0_0_0_KeyValuePair_2_t759847414_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3212367358_gp_0_0_0_0_Types[] = { (&List_1_t3212367358_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3212367358_gp_0_0_0_0 = { 1, GenInst_List_1_t3212367358_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t280605556_gp_0_0_0_0_Types[] = { (&Enumerator_t280605556_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t280605556_gp_0_0_0_0 = { 1, GenInst_Enumerator_t280605556_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t3428355205_gp_0_0_0_0_Types[] = { (&Collection_1_t3428355205_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t3428355205_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3428355205_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t4027728135_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t4027728135_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t4027728135_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t4027728135_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m3258900994_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3258900994_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m3258900994_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m3258900994_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m3258900994_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3258900994_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m3258900994_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3258900994_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m3164454857_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m3164454857_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m3164454857_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m3164454857_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t1245512621_gp_0_0_0_0_Types[] = { (&Queue_1_t1245512621_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t1245512621_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1245512621_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3225660046_gp_0_0_0_0_Types[] = { (&Enumerator_t3225660046_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3225660046_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3225660046_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t4193425384_gp_0_0_0_0_Types[] = { (&Stack_1_t4193425384_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t4193425384_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4193425384_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2833440198_gp_0_0_0_0_Types[] = { (&Enumerator_t2833440198_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2833440198_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2833440198_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t3707433416_gp_0_0_0_0_Types[] = { (&HashSet_1_t3707433416_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t3707433416_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3707433416_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2154081504_gp_0_0_0_0_Types[] = { (&Enumerator_t2154081504_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2154081504_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2154081504_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t1212413519_gp_0_0_0_0_Types[] = { (&PrimeHelper_t1212413519_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t1212413519_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t1212413519_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m1190044049_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m1190044049_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1190044049_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1190044049_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Single_m243410678_gp_0_0_0_0_Types[] = { (&Enumerable_Single_m243410678_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m243410678_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m243410678_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Single_m243410678_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Enumerable_Single_m243410678_gp_0_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m243410678_gp_0_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Enumerable_Single_m243410678_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0_Types[] = { (&Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToList_m978993884_gp_0_0_0_0_Types[] = { (&Enumerable_ToList_m978993884_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m978993884_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m978993884_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m3875999180_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Enumerable_Where_m3875999180_gp_0_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m278292298_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m278292298_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m278292298_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m278292298_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m432043675_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m432043675_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m432043675_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m432043675_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m3978037784_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m3978037784_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3978037784_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3978037784_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m594717923_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m594717923_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m594717923_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m594717923_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m983846299_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m983846299_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m983846299_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m983846299_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m547656136_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m547656136_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m547656136_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m547656136_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m3576923290_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m3576923290_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3576923290_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3576923290_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m1754205259_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m1754205259_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m1754205259_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m1754205259_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m933510971_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m933510971_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m933510971_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m933510971_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m394723642_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m394723642_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m394723642_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m394723642_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m2979658420_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m2979658420_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m2979658420_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m2979658420_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m4006422251_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m4006422251_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m4006422251_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m4006422251_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m207993587_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m207993587_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m207993587_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m207993587_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m3613586775_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m3613586775_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3613586775_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3613586775_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m2818067362_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m2818067362_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m2818067362_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m2818067362_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t336718215_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t336718215_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t336718215_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t336718215_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t3655347980_0_0_0_Types[] = { (&UnityAction_1_t3655347980_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t3655347980_0_0_0 = { 1, GenInst_UnityAction_1_t3655347980_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0_InvokableCall_2_t2897724777_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2897724777_gp_0_0_0_0), (&InvokableCall_2_t2897724777_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0_InvokableCall_2_t2897724777_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0_InvokableCall_2_t2897724777_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_2_t2251939000_0_0_0_Types[] = { (&UnityAction_2_t2251939000_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_2_t2251939000_0_0_0 = { 1, GenInst_UnityAction_2_t2251939000_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t2897724777_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2897724777_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2897724777_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2897724777_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2897724777_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0_InvokableCall_3_t3004736123_gp_1_0_0_0_InvokableCall_3_t3004736123_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3004736123_gp_0_0_0_0), (&InvokableCall_3_t3004736123_gp_1_0_0_0), (&InvokableCall_3_t3004736123_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0_InvokableCall_3_t3004736123_gp_1_0_0_0_InvokableCall_3_t3004736123_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0_InvokableCall_3_t3004736123_gp_1_0_0_0_InvokableCall_3_t3004736123_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_3_t2799906667_0_0_0_Types[] = { (&UnityAction_3_t2799906667_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_3_t2799906667_0_0_0 = { 1, GenInst_UnityAction_3_t2799906667_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t3004736123_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3004736123_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t3004736123_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3004736123_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3004736123_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3004736123_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3004736123_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3004736123_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3004736123_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0_InvokableCall_4_t3272338627_gp_1_0_0_0_InvokableCall_4_t3272338627_gp_2_0_0_0_InvokableCall_4_t3272338627_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t3272338627_gp_0_0_0_0), (&InvokableCall_4_t3272338627_gp_1_0_0_0), (&InvokableCall_4_t3272338627_gp_2_0_0_0), (&InvokableCall_4_t3272338627_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0_InvokableCall_4_t3272338627_gp_1_0_0_0_InvokableCall_4_t3272338627_gp_2_0_0_0_InvokableCall_4_t3272338627_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0_InvokableCall_4_t3272338627_gp_1_0_0_0_InvokableCall_4_t3272338627_gp_2_0_0_0_InvokableCall_4_t3272338627_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t3272338627_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3272338627_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t3272338627_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3272338627_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3272338627_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3272338627_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t3272338627_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3272338627_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3272338627_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t3272338627_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t3272338627_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3272338627_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3272338627_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t1723064271_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t1723064271_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1723064271_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1723064271_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t354605367_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t354605367_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t354605367_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t354605367_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t697054180_gp_0_0_0_0_UnityEvent_2_t697054180_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t697054180_gp_0_0_0_0), (&UnityEvent_2_t697054180_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t697054180_gp_0_0_0_0_UnityEvent_2_t697054180_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t697054180_gp_0_0_0_0_UnityEvent_2_t697054180_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t1308998058_gp_0_0_0_0_UnityEvent_3_t1308998058_gp_1_0_0_0_UnityEvent_3_t1308998058_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t1308998058_gp_0_0_0_0), (&UnityEvent_3_t1308998058_gp_1_0_0_0), (&UnityEvent_3_t1308998058_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1308998058_gp_0_0_0_0_UnityEvent_3_t1308998058_gp_1_0_0_0_UnityEvent_3_t1308998058_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t1308998058_gp_0_0_0_0_UnityEvent_3_t1308998058_gp_1_0_0_0_UnityEvent_3_t1308998058_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t646578554_gp_0_0_0_0_UnityEvent_4_t646578554_gp_1_0_0_0_UnityEvent_4_t646578554_gp_2_0_0_0_UnityEvent_4_t646578554_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t646578554_gp_0_0_0_0), (&UnityEvent_4_t646578554_gp_1_0_0_0), (&UnityEvent_4_t646578554_gp_2_0_0_0), (&UnityEvent_4_t646578554_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t646578554_gp_0_0_0_0_UnityEvent_4_t646578554_gp_1_0_0_0_UnityEvent_4_t646578554_gp_2_0_0_0_UnityEvent_4_t646578554_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t646578554_gp_0_0_0_0_UnityEvent_4_t646578554_gp_1_0_0_0_UnityEvent_4_t646578554_gp_2_0_0_0_UnityEvent_4_t646578554_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m4182767292_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m4182767292_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m4182767292_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m4182767292_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m2697298702_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m2697298702_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m2697298702_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m2697298702_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m1972297587_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m1972297587_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1972297587_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1972297587_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m4032946386_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m4032946386_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m4032946386_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m4032946386_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m1635484251_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m1635484251_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m1635484251_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m1635484251_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t1184621110_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t1184621110_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t1184621110_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t1184621110_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m1943470346_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m1943470346_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1943470346_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1943470346_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m3902694711_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m3902694711_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m3902694711_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m3902694711_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t2290263430_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&IndexedSet_1_t2290263430_gp_0_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t4260954668_gp_0_0_0_0_Types[] = { (&ListPool_1_t4260954668_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t4260954668_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t4260954668_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2287822912_0_0_0_Types[] = { (&List_1_t2287822912_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2287822912_0_0_0 = { 1, GenInst_List_1_t2287822912_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t1813863383_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t1813863383_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t1813863383_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t1813863383_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t1108411129_0_0_0_Types[] = { (&DefaultExecutionOrder_t1108411129_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t1108411129_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t1108411129_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t3190352615_0_0_0_Types[] = { (&PlayerConnection_t3190352615_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3190352615_0_0_0 = { 1, GenInst_PlayerConnection_t3190352615_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t1634068716_0_0_0_Types[] = { (&GUILayer_t1634068716_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t1634068716_0_0_0 = { 1, GenInst_GUILayer_t1634068716_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t513062718_0_0_0_Types[] = { (&AxisEventData_t513062718_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t513062718_0_0_0 = { 1, GenInst_AxisEventData_t513062718_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t3285139998_0_0_0_Types[] = { (&SpriteRenderer_t3285139998_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t3285139998_0_0_0 = { 1, GenInst_SpriteRenderer_t3285139998_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t1046486851_0_0_0_Types[] = { (&GraphicRaycaster_t1046486851_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t1046486851_0_0_0 = { 1, GenInst_GraphicRaycaster_t1046486851_0_0_0_Types };
static const RuntimeType* GenInst_Image_t1384762407_0_0_0_Types[] = { (&Image_t1384762407_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t1384762407_0_0_0 = { 1, GenInst_Image_t1384762407_0_0_0_Types };
static const RuntimeType* GenInst_Button_t3512035522_0_0_0_Types[] = { (&Button_t3512035522_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t3512035522_0_0_0 = { 1, GenInst_Button_t3512035522_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t597371120_0_0_0_Types[] = { (&Dropdown_t597371120_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t597371120_0_0_0 = { 1, GenInst_Dropdown_t597371120_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t3854993866_0_0_0_Types[] = { (&CanvasRenderer_t3854993866_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t3854993866_0_0_0 = { 1, GenInst_CanvasRenderer_t3854993866_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t562422201_0_0_0_Types[] = { (&Corner_t562422201_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t562422201_0_0_0 = { 1, GenInst_Corner_t562422201_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t1187875949_0_0_0_Types[] = { (&Axis_t1187875949_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t1187875949_0_0_0 = { 1, GenInst_Axis_t1187875949_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t2335916233_0_0_0_Types[] = { (&Constraint_t2335916233_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t2335916233_0_0_0 = { 1, GenInst_Constraint_t2335916233_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t1157603230_0_0_0_Types[] = { (&SubmitEvent_t1157603230_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t1157603230_0_0_0 = { 1, GenInst_SubmitEvent_t1157603230_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t3348859603_0_0_0_Types[] = { (&OnChangeEvent_t3348859603_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t3348859603_0_0_0 = { 1, GenInst_OnChangeEvent_t3348859603_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t3514437264_0_0_0_Types[] = { (&OnValidateInput_t3514437264_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t3514437264_0_0_0 = { 1, GenInst_OnValidateInput_t3514437264_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t708853819_0_0_0_Types[] = { (&LayoutElement_t708853819_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t708853819_0_0_0 = { 1, GenInst_LayoutElement_t708853819_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t817686154_0_0_0_Types[] = { (&RectOffset_t817686154_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t817686154_0_0_0 = { 1, GenInst_RectOffset_t817686154_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t2808300235_0_0_0_Types[] = { (&TextAnchor_t2808300235_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t2808300235_0_0_0 = { 1, GenInst_TextAnchor_t2808300235_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t1153907273_0_0_0_Types[] = { (&AnimationTriggers_t1153907273_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t1153907273_0_0_0 = { 1, GenInst_AnimationTriggers_t1153907273_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t2047190299_0_0_0_Types[] = { (&Animator_t2047190299_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t2047190299_0_0_0 = { 1, GenInst_Animator_t2047190299_0_0_0_Types };
static const RuntimeType* GenInst_UnityARVideo_t3776543285_0_0_0_Types[] = { (&UnityARVideo_t3776543285_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARVideo_t3776543285_0_0_0 = { 1, GenInst_UnityARVideo_t3776543285_0_0_0_Types };
static const RuntimeType* GenInst_MeshRenderer_t1975789539_0_0_0_Types[] = { (&MeshRenderer_t1975789539_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1975789539_0_0_0 = { 1, GenInst_MeshRenderer_t1975789539_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t402745866_0_0_0_Types[] = { (&Slider_t402745866_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t402745866_0_0_0 = { 1, GenInst_Slider_t402745866_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t2028672565_0_0_0_Types[] = { (&RawImage_t2028672565_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t2028672565_0_0_0 = { 1, GenInst_RawImage_t2028672565_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t611956437_0_0_0_Types[] = { (&InputField_t611956437_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t611956437_0_0_0 = { 1, GenInst_InputField_t611956437_0_0_0_Types };
static const RuntimeType* GenInst_BoxSlider_t1609811286_0_0_0_Types[] = { (&BoxSlider_t1609811286_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxSlider_t1609811286_0_0_0 = { 1, GenInst_BoxSlider_t1609811286_0_0_0_Types };
static const RuntimeType* GenInst_UnityARUserAnchorComponent_t3923313875_0_0_0_Types[] = { (&UnityARUserAnchorComponent_t3923313875_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARUserAnchorComponent_t3923313875_0_0_0 = { 1, GenInst_UnityARUserAnchorComponent_t3923313875_0_0_0_Types };
static const RuntimeType* GenInst_serializableFromEditorMessage_t755252547_0_0_0_Types[] = { (&serializableFromEditorMessage_t755252547_0_0_0) };
extern const Il2CppGenericInst GenInst_serializableFromEditorMessage_t755252547_0_0_0 = { 1, GenInst_serializableFromEditorMessage_t755252547_0_0_0_Types };
static const RuntimeType* GenInst_Light_t455885974_0_0_0_Types[] = { (&Light_t455885974_0_0_0) };
extern const Il2CppGenericInst GenInst_Light_t455885974_0_0_0 = { 1, GenInst_Light_t455885974_0_0_0_Types };
static const RuntimeType* GenInst_DontDestroyOnLoad_t530518805_0_0_0_Types[] = { (&DontDestroyOnLoad_t530518805_0_0_0) };
extern const Il2CppGenericInst GenInst_DontDestroyOnLoad_t530518805_0_0_0 = { 1, GenInst_DontDestroyOnLoad_t530518805_0_0_0_Types };
static const RuntimeType* GenInst_MeshFilter_t3228362100_0_0_0_Types[] = { (&MeshFilter_t3228362100_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshFilter_t3228362100_0_0_0 = { 1, GenInst_MeshFilter_t3228362100_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0_Types[] = { (&Int32_t3515577538_0_0_0), (&Int32_t3515577538_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0 = { 2, GenInst_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0_CustomAttributeNamedArgument_t4247477102_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t4247477102_0_0_0), (&CustomAttributeNamedArgument_t4247477102_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0_CustomAttributeNamedArgument_t4247477102_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0_CustomAttributeNamedArgument_t4247477102_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0_CustomAttributeTypedArgument_t4128697880_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t4128697880_0_0_0), (&CustomAttributeTypedArgument_t4128697880_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0_CustomAttributeTypedArgument_t4128697880_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0_CustomAttributeTypedArgument_t4128697880_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t662424039_0_0_0_Color32_t662424039_0_0_0_Types[] = { (&Color32_t662424039_0_0_0), (&Color32_t662424039_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t662424039_0_0_0_Color32_t662424039_0_0_0 = { 2, GenInst_Color32_t662424039_0_0_0_Color32_t662424039_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t1958542877_0_0_0_RaycastResult_t1958542877_0_0_0_Types[] = { (&RaycastResult_t1958542877_0_0_0), (&RaycastResult_t1958542877_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t1958542877_0_0_0_RaycastResult_t1958542877_0_0_0 = { 2, GenInst_RaycastResult_t1958542877_0_0_0_RaycastResult_t1958542877_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t839829031_0_0_0_UICharInfo_t839829031_0_0_0_Types[] = { (&UICharInfo_t839829031_0_0_0), (&UICharInfo_t839829031_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t839829031_0_0_0_UICharInfo_t839829031_0_0_0 = { 2, GenInst_UICharInfo_t839829031_0_0_0_UICharInfo_t839829031_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t2156392613_0_0_0_UILineInfo_t2156392613_0_0_0_Types[] = { (&UILineInfo_t2156392613_0_0_0), (&UILineInfo_t2156392613_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t2156392613_0_0_0_UILineInfo_t2156392613_0_0_0 = { 2, GenInst_UILineInfo_t2156392613_0_0_0_UILineInfo_t2156392613_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t2944109340_0_0_0_UIVertex_t2944109340_0_0_0_Types[] = { (&UIVertex_t2944109340_0_0_0), (&UIVertex_t2944109340_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t2944109340_0_0_0_UIVertex_t2944109340_0_0_0 = { 2, GenInst_UIVertex_t2944109340_0_0_0_UIVertex_t2944109340_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2246369278_0_0_0_Vector2_t2246369278_0_0_0_Types[] = { (&Vector2_t2246369278_0_0_0), (&Vector2_t2246369278_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2246369278_0_0_0_Vector2_t2246369278_0_0_0 = { 2, GenInst_Vector2_t2246369278_0_0_0_Vector2_t2246369278_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t3932393085_0_0_0_Vector3_t3932393085_0_0_0_Types[] = { (&Vector3_t3932393085_0_0_0), (&Vector3_t3932393085_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t3932393085_0_0_0_Vector3_t3932393085_0_0_0 = { 2, GenInst_Vector3_t3932393085_0_0_0_Vector3_t3932393085_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t1900979187_0_0_0_Vector4_t1900979187_0_0_0_Types[] = { (&Vector4_t1900979187_0_0_0), (&Vector4_t1900979187_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t1900979187_0_0_0_Vector4_t1900979187_0_0_0 = { 2, GenInst_Vector4_t1900979187_0_0_0_Vector4_t1900979187_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t942592945_0_0_0_ARHitTestResult_t942592945_0_0_0_Types[] = { (&ARHitTestResult_t942592945_0_0_0), (&ARHitTestResult_t942592945_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t942592945_0_0_0_ARHitTestResult_t942592945_0_0_0 = { 2, GenInst_ARHitTestResult_t942592945_0_0_0_ARHitTestResult_t942592945_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1339419795_0_0_0_KeyValuePair_2_t1339419795_0_0_0_Types[] = { (&KeyValuePair_2_t1339419795_0_0_0), (&KeyValuePair_2_t1339419795_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1339419795_0_0_0_KeyValuePair_2_t1339419795_0_0_0 = { 2, GenInst_KeyValuePair_2_t1339419795_0_0_0_KeyValuePair_2_t1339419795_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1339419795_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1339419795_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1339419795_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1339419795_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2055206595_0_0_0_KeyValuePair_2_t2055206595_0_0_0_Types[] = { (&KeyValuePair_2_t2055206595_0_0_0), (&KeyValuePair_2_t2055206595_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2055206595_0_0_0_KeyValuePair_2_t2055206595_0_0_0 = { 2, GenInst_KeyValuePair_2_t2055206595_0_0_0_KeyValuePair_2_t2055206595_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2055206595_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2055206595_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2055206595_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2055206595_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0_Types[] = { (&Boolean_t2440219994_0_0_0), (&Boolean_t2440219994_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0 = { 2, GenInst_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3397783615_0_0_0_KeyValuePair_2_t3397783615_0_0_0_Types[] = { (&KeyValuePair_2_t3397783615_0_0_0), (&KeyValuePair_2_t3397783615_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3397783615_0_0_0_KeyValuePair_2_t3397783615_0_0_0 = { 2, GenInst_KeyValuePair_2_t3397783615_0_0_0_KeyValuePair_2_t3397783615_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3397783615_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3397783615_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3397783615_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3397783615_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t178173863_0_0_0_KeyValuePair_2_t178173863_0_0_0_Types[] = { (&KeyValuePair_2_t178173863_0_0_0), (&KeyValuePair_2_t178173863_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t178173863_0_0_0_KeyValuePair_2_t178173863_0_0_0 = { 2, GenInst_KeyValuePair_2_t178173863_0_0_0_KeyValuePair_2_t178173863_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t178173863_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t178173863_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t178173863_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t178173863_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1509590809_0_0_0_KeyValuePair_2_t1509590809_0_0_0_Types[] = { (&KeyValuePair_2_t1509590809_0_0_0), (&KeyValuePair_2_t1509590809_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1509590809_0_0_0_KeyValuePair_2_t1509590809_0_0_0 = { 2, GenInst_KeyValuePair_2_t1509590809_0_0_0_KeyValuePair_2_t1509590809_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1509590809_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1509590809_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1509590809_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1509590809_0_0_0_RuntimeObject_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[606] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0,
	&GenInst_Char_t1622636488_0_0_0,
	&GenInst_Int64_t3603203610_0_0_0,
	&GenInst_UInt32_t1085449242_0_0_0,
	&GenInst_UInt64_t462540778_0_0_0,
	&GenInst_Byte_t2640180184_0_0_0,
	&GenInst_SByte_t2299796046_0_0_0,
	&GenInst_Int16_t2935336495_0_0_0,
	&GenInst_UInt16_t3430003764_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t1386358354_0_0_0,
	&GenInst_IComparable_t3923264286_0_0_0,
	&GenInst_IEnumerable_t307620497_0_0_0,
	&GenInst_ICloneable_t3227696483_0_0_0,
	&GenInst_IComparable_1_t1965733923_0_0_0,
	&GenInst_IEquatable_1_t733774393_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t371090060_0_0_0,
	&GenInst__Type_t3580695744_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2532673682_0_0_0,
	&GenInst__MemberInfo_t1879838719_0_0_0,
	&GenInst_Double_t3942828892_0_0_0,
	&GenInst_Single_t897798503_0_0_0,
	&GenInst_Decimal_t2641346350_0_0_0,
	&GenInst_Boolean_t2440219994_0_0_0,
	&GenInst_Delegate_t4015201940_0_0_0,
	&GenInst_ISerializable_t3561356945_0_0_0,
	&GenInst_ParameterInfo_t3156340899_0_0_0,
	&GenInst__ParameterInfo_t2470192644_0_0_0,
	&GenInst_ParameterModifier_t1933193809_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2737396514_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3421021404_0_0_0,
	&GenInst_MethodBase_t3670318294_0_0_0,
	&GenInst__MethodBase_t1498739407_0_0_0,
	&GenInst_ConstructorInfo_t3011499035_0_0_0,
	&GenInst__ConstructorInfo_t2887962050_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t739736251_0_0_0,
	&GenInst_TailoringInfo_t1494168405_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_KeyValuePair_2_t178173863_0_0_0,
	&GenInst_Link_t2678926947_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t178173863_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t2676903416_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t2676903416_0_0_0,
	&GenInst_Contraction_t2685614877_0_0_0,
	&GenInst_Level2Map_t2701131617_0_0_0,
	&GenInst_BigInteger_t1843659260_0_0_0,
	&GenInst_KeySizes_t3274741244_0_0_0,
	&GenInst_KeyValuePair_2_t1509590809_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1509590809_0_0_0,
	&GenInst_Slot_t1231688804_0_0_0,
	&GenInst_Slot_t697738515_0_0_0,
	&GenInst_StackFrame_t3907017807_0_0_0,
	&GenInst_Calendar_t1639743412_0_0_0,
	&GenInst_ModuleBuilder_t2525481856_0_0_0,
	&GenInst__ModuleBuilder_t1154404907_0_0_0,
	&GenInst_Module_t575570506_0_0_0,
	&GenInst__Module_t1610937700_0_0_0,
	&GenInst_CustomAttributeBuilder_t1604210647_0_0_0,
	&GenInst__CustomAttributeBuilder_t2558201917_0_0_0,
	&GenInst_MonoResource_t2877269471_0_0_0,
	&GenInst_MonoWin32Resource_t2032464017_0_0_0,
	&GenInst_RefEmitPermissionSet_t4262511222_0_0_0,
	&GenInst_ParameterBuilder_t4081109430_0_0_0,
	&GenInst__ParameterBuilder_t4215768685_0_0_0,
	&GenInst_TypeU5BU5D_t98989581_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t2118996832_0_0_0,
	&GenInst_IList_t3112248398_0_0_0,
	&GenInst_IList_1_t813021139_0_0_0,
	&GenInst_ICollection_1_t1154546467_0_0_0,
	&GenInst_IEnumerable_1_t1135313765_0_0_0,
	&GenInst_IList_1_t135751803_0_0_0,
	&GenInst_ICollection_1_t477277131_0_0_0,
	&GenInst_IEnumerable_1_t458044429_0_0_0,
	&GenInst_IList_1_t3345357487_0_0_0,
	&GenInst_ICollection_1_t3686882815_0_0_0,
	&GenInst_IEnumerable_1_t3667650113_0_0_0,
	&GenInst_IList_1_t1896635693_0_0_0,
	&GenInst_ICollection_1_t2238161021_0_0_0,
	&GenInst_IEnumerable_1_t2218928319_0_0_0,
	&GenInst_IList_1_t2297335425_0_0_0,
	&GenInst_ICollection_1_t2638860753_0_0_0,
	&GenInst_IEnumerable_1_t2619628051_0_0_0,
	&GenInst_IList_1_t1644500462_0_0_0,
	&GenInst_ICollection_1_t1986025790_0_0_0,
	&GenInst_IEnumerable_1_t1966793088_0_0_0,
	&GenInst_IList_1_t316688931_0_0_0,
	&GenInst_ICollection_1_t658214259_0_0_0,
	&GenInst_IEnumerable_1_t638981557_0_0_0,
	&GenInst_LocalBuilder_t3984406356_0_0_0,
	&GenInst__LocalBuilder_t2858090211_0_0_0,
	&GenInst_LocalVariableInfo_t3511563403_0_0_0,
	&GenInst_ILTokenInfo_t598344192_0_0_0,
	&GenInst_LabelData_t637884969_0_0_0,
	&GenInst_LabelFixup_t3130180171_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t2133930691_0_0_0,
	&GenInst_TypeBuilder_t1295847866_0_0_0,
	&GenInst__TypeBuilder_t2508567353_0_0_0,
	&GenInst_MethodBuilder_t1042188494_0_0_0,
	&GenInst__MethodBuilder_t3881765320_0_0_0,
	&GenInst_FieldBuilder_t2554673315_0_0_0,
	&GenInst__FieldBuilder_t321201139_0_0_0,
	&GenInst_ConstructorBuilder_t4189780687_0_0_0,
	&GenInst__ConstructorBuilder_t1016963516_0_0_0,
	&GenInst_PropertyBuilder_t282572772_0_0_0,
	&GenInst__PropertyBuilder_t829064024_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t116083861_0_0_0,
	&GenInst_EventBuilder_t1568348350_0_0_0,
	&GenInst__EventBuilder_t3684978793_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0,
	&GenInst_CustomAttributeData_t2324393084_0_0_0,
	&GenInst_ResourceInfo_t3026636918_0_0_0,
	&GenInst_ResourceCacheItem_t2363372085_0_0_0,
	&GenInst_IContextProperty_t2535791102_0_0_0,
	&GenInst_Header_t4229290717_0_0_0,
	&GenInst_ITrackingHandler_t3307359081_0_0_0,
	&GenInst_IContextAttribute_t2746893575_0_0_0,
	&GenInst_DateTime_t218649865_0_0_0,
	&GenInst_TimeSpan_t595369841_0_0_0,
	&GenInst_TypeTag_t1400222038_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t3151463888_0_0_0,
	&GenInst_IBuiltInEvidence_t1063146314_0_0_0,
	&GenInst_IIdentityPermissionFactory_t674530215_0_0_0,
	&GenInst_WaitHandle_t2040275096_0_0_0,
	&GenInst_IDisposable_t3181680589_0_0_0,
	&GenInst_MarshalByRefObject_t714030913_0_0_0,
	&GenInst_DateTimeOffset_t206488804_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t1358777808_0_0_0,
	&GenInst_BigInteger_t1843659261_0_0_0,
	&GenInst_ByteU5BU5D_t3567143369_0_0_0,
	&GenInst_IList_1_t2404841927_0_0_0,
	&GenInst_ICollection_1_t2746367255_0_0_0,
	&GenInst_IEnumerable_1_t2727134553_0_0_0,
	&GenInst_X509Certificate_t1117940400_0_0_0,
	&GenInst_IDeserializationCallback_t3618342850_0_0_0,
	&GenInst_ClientCertificateType_t373382451_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_KeyValuePair_2_t3397783615_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t3397783615_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t1601545872_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t2440219994_0_0_0_KeyValuePair_2_t1601545872_0_0_0,
	&GenInst_X509ChainStatus_t261084987_0_0_0,
	&GenInst_Capture_t1485804594_0_0_0,
	&GenInst_Group_t1812461930_0_0_0,
	&GenInst_Mark_t3196564960_0_0_0,
	&GenInst_UriScheme_t3530988980_0_0_0,
	&GenInst_Link_t890348390_0_0_0,
	&GenInst_AsyncOperation_t3797817720_0_0_0,
	&GenInst_Camera_t362346687_0_0_0,
	&GenInst_Behaviour_t3297694025_0_0_0,
	&GenInst_Component_t2335505321_0_0_0,
	&GenInst_Object_t3645472222_0_0_0,
	&GenInst_Display_t81008966_0_0_0,
	&GenInst_Keyframe_t1233380954_0_0_0,
	&GenInst_Vector3_t3932393085_0_0_0,
	&GenInst_Vector4_t1900979187_0_0_0,
	&GenInst_Vector2_t2246369278_0_0_0,
	&GenInst_Color32_t662424039_0_0_0,
	&GenInst_Playable_t379060559_0_0_0,
	&GenInst_PlayableOutput_t2773044158_0_0_0,
	&GenInst_Scene_t1083318001_0_0_0_LoadSceneMode_t3029863218_0_0_0,
	&GenInst_Scene_t1083318001_0_0_0,
	&GenInst_Scene_t1083318001_0_0_0_Scene_t1083318001_0_0_0,
	&GenInst_SpriteAtlas_t2782010525_0_0_0,
	&GenInst_DisallowMultipleComponent_t4131774042_0_0_0,
	&GenInst_Attribute_t3130080784_0_0_0,
	&GenInst__Attribute_t1312228502_0_0_0,
	&GenInst_ExecuteInEditMode_t2915138156_0_0_0,
	&GenInst_RequireComponent_t3219866953_0_0_0,
	&GenInst_HitInfo_t3525137541_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PersistentCall_t1968425864_0_0_0,
	&GenInst_BaseInvokableCall_t3648497698_0_0_0,
	&GenInst_WorkRequest_t266095197_0_0_0,
	&GenInst_PlayableBinding_t2941134609_0_0_0,
	&GenInst_MessageEventArgs_t2772358345_0_0_0,
	&GenInst_MessageTypeSubscribers_t675444414_0_0_0,
	&GenInst_MessageTypeSubscribers_t675444414_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2055206595_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2055206595_0_0_0,
	&GenInst_WeakReference_t1433439652_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t2936619059_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1433439652_0_0_0_KeyValuePair_2_t2936619059_0_0_0,
	&GenInst_Rigidbody2D_t1466291965_0_0_0,
	&GenInst_Font_t2643454529_0_0_0,
	&GenInst_UIVertex_t2944109340_0_0_0,
	&GenInst_UICharInfo_t839829031_0_0_0,
	&GenInst_UILineInfo_t2156392613_0_0_0,
	&GenInst_AnimationClipPlayable_t1463126785_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t1879541177_0_0_0,
	&GenInst_AnimationMixerPlayable_t4125999351_0_0_0,
	&GenInst_AnimationOffsetPlayable_t999774996_0_0_0,
	&GenInst_AnimatorControllerPlayable_t4278763719_0_0_0,
	&GenInst_AudioSpatializerExtensionDefinition_t2663594034_0_0_0,
	&GenInst_AudioAmbisonicExtensionDefinition_t4031934303_0_0_0,
	&GenInst_AudioSourceExtension_t4018014667_0_0_0,
	&GenInst_ScriptableObject_t1324617639_0_0_0,
	&GenInst_AudioMixerPlayable_t3997886707_0_0_0,
	&GenInst_AudioClipPlayable_t2374979939_0_0_0,
	&GenInst_Boolean_t2440219994_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t2440219994_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t1703388966_0_0_0,
	&GenInst_IAchievementDescription_t1992040264_0_0_0,
	&GenInst_UserProfile_t2802009794_0_0_0,
	&GenInst_IUserProfile_t2218596927_0_0_0,
	&GenInst_GcLeaderboard_t3922680439_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t185225881_0_0_0,
	&GenInst_IAchievementU5BU5D_t2787719602_0_0_0,
	&GenInst_IAchievement_t3527372131_0_0_0,
	&GenInst_GcAchievementData_t1576042669_0_0_0,
	&GenInst_Achievement_t1548866956_0_0_0,
	&GenInst_IScoreU5BU5D_t3386159579_0_0_0,
	&GenInst_IScore_t2300007342_0_0_0,
	&GenInst_GcScoreData_t2566310542_0_0_0,
	&GenInst_Score_t1607899041_0_0_0,
	&GenInst_IUserProfileU5BU5D_t1348948454_0_0_0,
	&GenInst_GUILayoutOption_t1561297139_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1339419795_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1339419795_0_0_0,
	&GenInst_LayoutCache_t2616876984_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t3404269591_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_LayoutCache_t2616876984_0_0_0_KeyValuePair_2_t3404269591_0_0_0,
	&GenInst_GUILayoutEntry_t3231862407_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_IntPtr_t_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_Exception_t214279536_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_GUIStyle_t3615497928_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t2776823806_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3615497928_0_0_0_KeyValuePair_2_t2776823806_0_0_0,
	&GenInst_Particle_t1283715429_0_0_0,
	&GenInst_RaycastHit_t3372097066_0_0_0,
	&GenInst_ContactPoint_t99991485_0_0_0,
	&GenInst_EventSystem_t1423129782_0_0_0,
	&GenInst_UIBehaviour_t1053087876_0_0_0,
	&GenInst_MonoBehaviour_t4008345588_0_0_0,
	&GenInst_BaseInputModule_t893951129_0_0_0,
	&GenInst_RaycastResult_t1958542877_0_0_0,
	&GenInst_IDeselectHandler_t1423095504_0_0_0,
	&GenInst_IEventSystemHandler_t2679299517_0_0_0,
	&GenInst_List_1_t58535312_0_0_0,
	&GenInst_List_1_t2226230279_0_0_0,
	&GenInst_List_1_t4009708412_0_0_0,
	&GenInst_ISelectHandler_t637179496_0_0_0,
	&GenInst_BaseRaycaster_t2460995416_0_0_0,
	&GenInst_Entry_t2441154801_0_0_0,
	&GenInst_BaseEventData_t3589491243_0_0_0,
	&GenInst_IPointerEnterHandler_t3124626600_0_0_0,
	&GenInst_IPointerExitHandler_t1395786574_0_0_0,
	&GenInst_IPointerDownHandler_t44025902_0_0_0,
	&GenInst_IPointerUpHandler_t102393737_0_0_0,
	&GenInst_IPointerClickHandler_t4047079811_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t818614831_0_0_0,
	&GenInst_IBeginDragHandler_t2428313557_0_0_0,
	&GenInst_IDragHandler_t2984229372_0_0_0,
	&GenInst_IEndDragHandler_t1280646970_0_0_0,
	&GenInst_IDropHandler_t1307228819_0_0_0,
	&GenInst_IScrollHandler_t194649380_0_0_0,
	&GenInst_IUpdateSelectedHandler_t2808857881_0_0_0,
	&GenInst_IMoveHandler_t1328422412_0_0_0,
	&GenInst_ISubmitHandler_t3368026263_0_0_0,
	&GenInst_ICancelHandler_t1174209038_0_0_0,
	&GenInst_Transform_t3580444445_0_0_0,
	&GenInst_GameObject_t2881801184_0_0_0,
	&GenInst_BaseInput_t2888570827_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0,
	&GenInst_PointerEventData_t507157825_0_0_0,
	&GenInst_AbstractEventData_t3911574392_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t1294550432_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_KeyValuePair_2_t1294550432_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_PointerEventData_t507157825_0_0_0_PointerEventData_t507157825_0_0_0,
	&GenInst_ButtonState_t2740850036_0_0_0,
	&GenInst_RaycastHit2D_t218385889_0_0_0,
	&GenInst_Color_t1361298052_0_0_0,
	&GenInst_ICanvasElement_t2313639799_0_0_0,
	&GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_ColorBlock_t3446061115_0_0_0,
	&GenInst_OptionData_t436826826_0_0_0,
	&GenInst_DropdownItem_t2659469254_0_0_0,
	&GenInst_FloatTween_t2756583341_0_0_0,
	&GenInst_Sprite_t2497061588_0_0_0,
	&GenInst_Canvas_t2667390772_0_0_0,
	&GenInst_List_1_t46626567_0_0_0,
	&GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0,
	&GenInst_Text_t1445358712_0_0_0,
	&GenInst_Link_t1783679914_0_0_0,
	&GenInst_ILayoutElement_t368149833_0_0_0,
	&GenInst_MaskableGraphic_t4128200649_0_0_0,
	&GenInst_IClippable_t3544737781_0_0_0,
	&GenInst_IMaskable_t997864649_0_0_0,
	&GenInst_IMaterialModifier_t805903918_0_0_0,
	&GenInst_Graphic_t2696902410_0_0_0,
	&GenInst_HashSet_1_t3142691717_0_0_0,
	&GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t3866178953_0_0_0,
	&GenInst_Font_t2643454529_0_0_0_HashSet_1_t3142691717_0_0_0_KeyValuePair_2_t3866178953_0_0_0,
	&GenInst_ColorTween_t2856906502_0_0_0,
	&GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0,
	&GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_IndexedSet_1_t1130276133_0_0_0,
	&GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t3771799818_0_0_0,
	&GenInst_Canvas_t2667390772_0_0_0_IndexedSet_1_t1130276133_0_0_0_KeyValuePair_2_t3771799818_0_0_0,
	&GenInst_KeyValuePair_2_t1169793209_0_0_0,
	&GenInst_Graphic_t2696902410_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1169793209_0_0_0,
	&GenInst_KeyValuePair_2_t223188984_0_0_0,
	&GenInst_ICanvasElement_t2313639799_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t223188984_0_0_0,
	&GenInst_Type_t2837774015_0_0_0,
	&GenInst_FillMethod_t2931617579_0_0_0,
	&GenInst_ContentType_t1122571944_0_0_0,
	&GenInst_LineType_t3638610376_0_0_0,
	&GenInst_InputType_t4187126303_0_0_0,
	&GenInst_TouchScreenKeyboardType_t1103124205_0_0_0,
	&GenInst_CharacterValidation_t2769550230_0_0_0,
	&GenInst_Mask_t745540708_0_0_0,
	&GenInst_ICanvasRaycastFilter_t2306748731_0_0_0,
	&GenInst_List_1_t2419743799_0_0_0,
	&GenInst_RectMask2D_t2972282963_0_0_0,
	&GenInst_IClipper_t386535878_0_0_0,
	&GenInst_List_1_t351518758_0_0_0,
	&GenInst_Navigation_t2227424972_0_0_0,
	&GenInst_Link_t3883058983_0_0_0,
	&GenInst_Direction_t1205246216_0_0_0,
	&GenInst_Selectable_t1875500212_0_0_0,
	&GenInst_Transition_t2703339633_0_0_0,
	&GenInst_SpriteState_t710398810_0_0_0,
	&GenInst_CanvasGroup_t2669816671_0_0_0,
	&GenInst_Direction_t299841542_0_0_0,
	&GenInst_MatEntry_t930024495_0_0_0,
	&GenInst_Toggle_t403198564_0_0_0,
	&GenInst_Toggle_t403198564_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t1494153101_0_0_0,
	&GenInst_IClipper_t386535878_0_0_0_Int32_t3515577538_0_0_0_KeyValuePair_2_t1494153101_0_0_0,
	&GenInst_AspectMode_t3475277687_0_0_0,
	&GenInst_FitMode_t3582979200_0_0_0,
	&GenInst_RectTransform_t1161610249_0_0_0,
	&GenInst_LayoutRebuilder_t2288684892_0_0_0,
	&GenInst_ILayoutElement_t368149833_0_0_0_Single_t897798503_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t897798503_0_0_0,
	&GenInst_List_1_t1311628880_0_0_0,
	&GenInst_List_1_t2336627130_0_0_0,
	&GenInst_List_1_t3920572369_0_0_0,
	&GenInst_List_1_t3575182278_0_0_0,
	&GenInst_List_1_t894813333_0_0_0,
	&GenInst_List_1_t323345135_0_0_0,
	&GenInst_Link_t3597842601_0_0_0,
	&GenInst_ARHitTestResult_t942592945_0_0_0,
	&GenInst_ARHitTestResultType_t1760080916_0_0_0,
	&GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0_Single_t897798503_0_0_0,
	&GenInst_Single_t897798503_0_0_0_Single_t897798503_0_0_0,
	&GenInst_ParticleSystem_t3890943298_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0,
	&GenInst_ARPlaneAnchorGameObject_t3125390466_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_KeyValuePair_2_t2286716344_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t3125390466_0_0_0_KeyValuePair_2_t2286716344_0_0_0,
	&GenInst_UnityARSessionRunOption_t572719712_0_0_0,
	&GenInst_UnityARAlignment_t2001134742_0_0_0,
	&GenInst_UnityARPlaneDetection_t1240827406_0_0_0,
	&GenInst_IEnumerable_1_t361495900_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3921660370_gp_0_0_0_0,
	&GenInst_Array_Sort_m3350998347_gp_0_0_0_0_Array_Sort_m3350998347_gp_0_0_0_0,
	&GenInst_Array_Sort_m689900404_gp_0_0_0_0_Array_Sort_m689900404_gp_1_0_0_0,
	&GenInst_Array_Sort_m4285269846_gp_0_0_0_0,
	&GenInst_Array_Sort_m4285269846_gp_0_0_0_0_Array_Sort_m4285269846_gp_0_0_0_0,
	&GenInst_Array_Sort_m1330797580_gp_0_0_0_0,
	&GenInst_Array_Sort_m1330797580_gp_0_0_0_0_Array_Sort_m1330797580_gp_1_0_0_0,
	&GenInst_Array_Sort_m869698846_gp_0_0_0_0_Array_Sort_m869698846_gp_0_0_0_0,
	&GenInst_Array_Sort_m2635216824_gp_0_0_0_0_Array_Sort_m2635216824_gp_1_0_0_0,
	&GenInst_Array_Sort_m3791969856_gp_0_0_0_0,
	&GenInst_Array_Sort_m3791969856_gp_0_0_0_0_Array_Sort_m3791969856_gp_0_0_0_0,
	&GenInst_Array_Sort_m2053374032_gp_0_0_0_0,
	&GenInst_Array_Sort_m2053374032_gp_1_0_0_0,
	&GenInst_Array_Sort_m2053374032_gp_0_0_0_0_Array_Sort_m2053374032_gp_1_0_0_0,
	&GenInst_Array_Sort_m3529074503_gp_0_0_0_0,
	&GenInst_Array_Sort_m1046918090_gp_0_0_0_0,
	&GenInst_Array_qsort_m3074224534_gp_0_0_0_0,
	&GenInst_Array_qsort_m3074224534_gp_0_0_0_0_Array_qsort_m3074224534_gp_1_0_0_0,
	&GenInst_Array_compare_m182040922_gp_0_0_0_0,
	&GenInst_Array_qsort_m3653394148_gp_0_0_0_0,
	&GenInst_Array_Resize_m1475766408_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m1259694655_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1921540296_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m2082766338_gp_0_0_0_0_Array_ConvertAll_m2082766338_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m2403343833_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m2322380597_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1895958268_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m2364170290_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3411283154_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1488736383_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1842855399_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m619051682_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m625219582_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m863042731_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1718101709_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1667751126_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m4170047889_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1415329759_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m4008997880_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2739852194_gp_0_0_0_0,
	&GenInst_Array_FindAll_m641447484_gp_0_0_0_0,
	&GenInst_Array_Exists_m3014889077_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m2697550731_gp_0_0_0_0,
	&GenInst_Array_Find_m408926873_gp_0_0_0_0,
	&GenInst_Array_FindLast_m4132394418_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3560912388_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t1444333090_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3454464003_gp_0_0_0_0,
	&GenInst_IList_1_t3296908666_gp_0_0_0_0,
	&GenInst_ICollection_1_t842930506_gp_0_0_0_0,
	&GenInst_Nullable_1_t2719303306_gp_0_0_0_0,
	&GenInst_Comparer_1_t1340870152_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2305410469_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t4291314091_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2794658882_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1146410757_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3201966815_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_ShimEnumerator_t2837955638_gp_0_0_0_0_ShimEnumerator_t2837955638_gp_1_0_0_0,
	&GenInst_Enumerator_t3167388891_gp_0_0_0_0_Enumerator_t3167388891_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1888873938_0_0_0,
	&GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0,
	&GenInst_ValueCollection_t4200311898_gp_1_0_0_0,
	&GenInst_Enumerator_t2729626900_gp_0_0_0_0_Enumerator_t2729626900_gp_1_0_0_0,
	&GenInst_Enumerator_t2729626900_gp_1_0_0_0,
	&GenInst_ValueCollection_t4200311898_gp_0_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0,
	&GenInst_ValueCollection_t4200311898_gp_1_0_0_0_ValueCollection_t4200311898_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3507565976_0_0_0_DictionaryEntry_t3507565976_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_0_0_0_0_Dictionary_2_t3686457887_gp_1_0_0_0_KeyValuePair_2_t2794658882_0_0_0,
	&GenInst_KeyValuePair_2_t2794658882_0_0_0_KeyValuePair_2_t2794658882_0_0_0,
	&GenInst_Dictionary_2_t3686457887_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t962836640_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3887701940_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3218101675_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t1744688238_0_0_0,
	&GenInst_IDictionary_2_t188967788_gp_0_0_0_0_IDictionary_2_t188967788_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t759847414_gp_0_0_0_0_KeyValuePair_2_t759847414_gp_1_0_0_0,
	&GenInst_List_1_t3212367358_gp_0_0_0_0,
	&GenInst_Enumerator_t280605556_gp_0_0_0_0,
	&GenInst_Collection_1_t3428355205_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t4027728135_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m3258900994_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3258900994_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m3164454857_gp_0_0_0_0,
	&GenInst_Queue_1_t1245512621_gp_0_0_0_0,
	&GenInst_Enumerator_t3225660046_gp_0_0_0_0,
	&GenInst_Stack_1_t4193425384_gp_0_0_0_0,
	&GenInst_Enumerator_t2833440198_gp_0_0_0_0,
	&GenInst_HashSet_1_t3707433416_gp_0_0_0_0,
	&GenInst_Enumerator_t2154081504_gp_0_0_0_0,
	&GenInst_PrimeHelper_t1212413519_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1190044049_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m243410678_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m243410678_gp_0_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m3210192612_gp_0_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_Enumerable_ToList_m978993884_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m3875999180_gp_0_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2832362432_gp_0_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t475074476_gp_0_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_Component_GetComponentInChildren_m278292298_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m432043675_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3978037784_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m594717923_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m983846299_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m547656136_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3576923290_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m1754205259_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m933510971_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m394723642_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m2979658420_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m4006422251_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m207993587_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3613586775_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m2818067362_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t336718215_gp_0_0_0_0,
	&GenInst_UnityAction_1_t3655347980_0_0_0,
	&GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0_InvokableCall_2_t2897724777_gp_1_0_0_0,
	&GenInst_UnityAction_2_t2251939000_0_0_0,
	&GenInst_InvokableCall_2_t2897724777_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2897724777_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0_InvokableCall_3_t3004736123_gp_1_0_0_0_InvokableCall_3_t3004736123_gp_2_0_0_0,
	&GenInst_UnityAction_3_t2799906667_0_0_0,
	&GenInst_InvokableCall_3_t3004736123_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3004736123_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3004736123_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0_InvokableCall_4_t3272338627_gp_1_0_0_0_InvokableCall_4_t3272338627_gp_2_0_0_0_InvokableCall_4_t3272338627_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3272338627_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3272338627_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3272338627_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3272338627_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1723064271_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t354605367_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t697054180_gp_0_0_0_0_UnityEvent_2_t697054180_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t1308998058_gp_0_0_0_0_UnityEvent_3_t1308998058_gp_1_0_0_0_UnityEvent_3_t1308998058_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t646578554_gp_0_0_0_0_UnityEvent_4_t646578554_gp_1_0_0_0_UnityEvent_4_t646578554_gp_2_0_0_0_UnityEvent_4_t646578554_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m4182767292_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m2697298702_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1972297587_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m4032946386_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m1635484251_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t1184621110_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1943470346_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m3902694711_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2290263430_gp_0_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_ListPool_1_t4260954668_gp_0_0_0_0,
	&GenInst_List_1_t2287822912_0_0_0,
	&GenInst_ObjectPool_1_t1813863383_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t1108411129_0_0_0,
	&GenInst_PlayerConnection_t3190352615_0_0_0,
	&GenInst_GUILayer_t1634068716_0_0_0,
	&GenInst_AxisEventData_t513062718_0_0_0,
	&GenInst_SpriteRenderer_t3285139998_0_0_0,
	&GenInst_GraphicRaycaster_t1046486851_0_0_0,
	&GenInst_Image_t1384762407_0_0_0,
	&GenInst_Button_t3512035522_0_0_0,
	&GenInst_Dropdown_t597371120_0_0_0,
	&GenInst_CanvasRenderer_t3854993866_0_0_0,
	&GenInst_Corner_t562422201_0_0_0,
	&GenInst_Axis_t1187875949_0_0_0,
	&GenInst_Constraint_t2335916233_0_0_0,
	&GenInst_SubmitEvent_t1157603230_0_0_0,
	&GenInst_OnChangeEvent_t3348859603_0_0_0,
	&GenInst_OnValidateInput_t3514437264_0_0_0,
	&GenInst_LayoutElement_t708853819_0_0_0,
	&GenInst_RectOffset_t817686154_0_0_0,
	&GenInst_TextAnchor_t2808300235_0_0_0,
	&GenInst_AnimationTriggers_t1153907273_0_0_0,
	&GenInst_Animator_t2047190299_0_0_0,
	&GenInst_UnityARVideo_t3776543285_0_0_0,
	&GenInst_MeshRenderer_t1975789539_0_0_0,
	&GenInst_Slider_t402745866_0_0_0,
	&GenInst_RawImage_t2028672565_0_0_0,
	&GenInst_InputField_t611956437_0_0_0,
	&GenInst_BoxSlider_t1609811286_0_0_0,
	&GenInst_UnityARUserAnchorComponent_t3923313875_0_0_0,
	&GenInst_serializableFromEditorMessage_t755252547_0_0_0,
	&GenInst_Light_t455885974_0_0_0,
	&GenInst_DontDestroyOnLoad_t530518805_0_0_0,
	&GenInst_MeshFilter_t3228362100_0_0_0,
	&GenInst_Int32_t3515577538_0_0_0_Int32_t3515577538_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t4247477102_0_0_0_CustomAttributeNamedArgument_t4247477102_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t4128697880_0_0_0_CustomAttributeTypedArgument_t4128697880_0_0_0,
	&GenInst_Color32_t662424039_0_0_0_Color32_t662424039_0_0_0,
	&GenInst_RaycastResult_t1958542877_0_0_0_RaycastResult_t1958542877_0_0_0,
	&GenInst_UICharInfo_t839829031_0_0_0_UICharInfo_t839829031_0_0_0,
	&GenInst_UILineInfo_t2156392613_0_0_0_UILineInfo_t2156392613_0_0_0,
	&GenInst_UIVertex_t2944109340_0_0_0_UIVertex_t2944109340_0_0_0,
	&GenInst_Vector2_t2246369278_0_0_0_Vector2_t2246369278_0_0_0,
	&GenInst_Vector3_t3932393085_0_0_0_Vector3_t3932393085_0_0_0,
	&GenInst_Vector4_t1900979187_0_0_0_Vector4_t1900979187_0_0_0,
	&GenInst_ARHitTestResult_t942592945_0_0_0_ARHitTestResult_t942592945_0_0_0,
	&GenInst_KeyValuePair_2_t1339419795_0_0_0_KeyValuePair_2_t1339419795_0_0_0,
	&GenInst_KeyValuePair_2_t1339419795_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2055206595_0_0_0_KeyValuePair_2_t2055206595_0_0_0,
	&GenInst_KeyValuePair_2_t2055206595_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Boolean_t2440219994_0_0_0_Boolean_t2440219994_0_0_0,
	&GenInst_KeyValuePair_2_t3397783615_0_0_0_KeyValuePair_2_t3397783615_0_0_0,
	&GenInst_KeyValuePair_2_t3397783615_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t178173863_0_0_0_KeyValuePair_2_t178173863_0_0_0,
	&GenInst_KeyValuePair_2_t178173863_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1509590809_0_0_0_KeyValuePair_2_t1509590809_0_0_0,
	&GenInst_KeyValuePair_2_t1509590809_0_0_0_RuntimeObject_0_0_0,
};
