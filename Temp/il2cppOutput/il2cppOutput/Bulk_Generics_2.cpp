﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t3561003404;
// System.Type
struct Type_t;
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct EqualityComparer_1_t3258574742;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>
struct EqualityComparer_1_t3325628940;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>
struct EqualityComparer_1_t326780482;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>
struct EqualityComparer_1_t3766329106;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct EqualityComparer_1_t1843515292;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>
struct EqualityComparer_1_t1814298720;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>
struct EqualityComparer_1_t1951216805;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>
struct EqualityComparer_1_t1299855184;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>
struct EqualityComparer_1_t1206011620;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>
struct EqualityComparer_1_t1137787835;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>
struct EqualityComparer_1_t3785776845;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>
struct EqualityComparer_1_t2555363908;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>
struct EqualityComparer_1_t2006847981;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>
struct EqualityComparer_1_t595662577;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>
struct EqualityComparer_1_t3868451117;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>
struct EqualityComparer_1_t1071577238;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>
struct EqualityComparer_1_t2963046443;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>
struct EqualityComparer_1_t3373603711;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct EqualityComparer_1_t3503033932;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
struct EqualityComparer_1_t524630218;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>
struct EqualityComparer_1_t1312346945;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>
struct EqualityComparer_1_t614606883;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>
struct EqualityComparer_1_t2300630690;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>
struct EqualityComparer_1_t269216792;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>
struct EqualityComparer_1_t3605797846;
// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t503918337;
// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t491757276;
// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t1937162846;
// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t3800846010;
// System.Collections.Generic.GenericComparer`1<System.Object>
struct GenericComparer_1_t837295660;
// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t880638313;
// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t1397660498;
// System.Collections.Generic.GenericEqualityComparer`1<System.Char>
struct GenericEqualityComparer_1_t580076992;
// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t3471057665;
// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t3458896604;
// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t609334878;
// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t2473018042;
// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct GenericEqualityComparer_1_t3804434988;
// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct GenericEqualityComparer_1_t4150206303;
// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct GenericEqualityComparer_1_t3847777641;
// System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.ColorBlock>
struct GenericEqualityComparer_1_t2403501619;
// System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.Navigation>
struct GenericEqualityComparer_1_t1184865476;
// System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.SpriteState>
struct GenericEqualityComparer_1_t3962806610;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t2249360193;
// System.InvalidOperationException
struct InvalidOperationException_t242246640;
// System.String
struct String_t;
// System.ObjectDisposedException
struct ObjectDisposedException_t2063091398;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1812684756;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1961836173;
// System.Object[]
struct ObjectU5BU5D_t3385344125;
// System.Collections.IEnumerator
struct IEnumerator_t2051810174;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2028374934;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t2239795282;
// System.ArgumentNullException
struct ArgumentNullException_t246139103;
// System.ArgumentException
struct ArgumentException_t489606696;
// System.NotImplementedException
struct NotImplementedException_t3535071701;
// System.Collections.Hashtable
struct Hashtable_t1793686270;
// System.Collections.ArrayList
struct ArrayList_t1797015065;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1016514262;
// System.Int32[]
struct Int32U5BU5D_t3215173111;
// System.Char[]
struct CharU5BU5D_t2953840665;
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t1226396739;
// System.IntPtr[]
struct IntPtrU5BU5D_t3930959911;
// System.Collections.IDictionary
struct IDictionary_t1084331215;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2272071576;
// System.Byte
struct Byte_t2640180184;
// System.Double
struct Double_t3942828892;
// System.UInt16
struct UInt16_t3430003764;
// System.Void
struct Void_t4071739332;
// UnityEngine.Sprite
struct Sprite_t2497061588;
// UnityEngine.GameObject
struct GameObject_t2881801184;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t2460995416;
// System.Type[]
struct TypeU5BU5D_t98989581;
// System.Reflection.MemberFilter
struct MemberFilter_t2882044324;
// System.String[]
struct StringU5BU5D_t1589106382;
// UnityEngine.UI.Selectable
struct Selectable_t1875500212;

extern const RuntimeType* GenericEqualityComparer_1_t3218101675_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t98989581_il2cpp_TypeInfo_var;
extern const uint32_t EqualityComparer_1__cctor_m2815452023_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m877790698_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3442706745_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m450573040_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1466159472_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2644827730_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m4211016089_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3678960581_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m994766661_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3520013917_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m141604497_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2066784956_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3733666149_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2695337509_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3188556159_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2826035360_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2850716509_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2557533271_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3228446035_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m870844869_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m4003914194_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m976010431_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m3877116888_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m4042575380_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m2940491964_MetadataUsageId;
extern const uint32_t EqualityComparer_1__cctor_m1621471002_MetadataUsageId;
extern RuntimeClass* InvalidOperationException_t242246640_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1142242952;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1426369967_MetadataUsageId;
extern RuntimeClass* ObjectDisposedException_t2063091398_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral534374098;
extern const uint32_t Enumerator_CheckState_m304388724_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t3215173111_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t1622590248____U24U24fieldU2D0_0_FieldInfo_var;
extern const uint32_t PrimeHelper__cctor_m2306016830_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t2239795282_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2532065760;
extern const uint32_t HashSet_1_Init_m2485840618_MetadataUsageId;
extern const uint32_t HashSet_1_InitArrays_m2246974508_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t246139103_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t489606696_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral615595643;
extern Il2CppCodeGenString* _stringLiteral2081506731;
extern Il2CppCodeGenString* _stringLiteral296885591;
extern Il2CppCodeGenString* _stringLiteral2631732851;
extern const uint32_t HashSet_1_CopyTo_m5591029_MetadataUsageId;
extern const uint32_t HashSet_1_Resize_m2833304277_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t HashSet_1_Remove_m1010352241_MetadataUsageId;
extern RuntimeClass* NotImplementedException_t3535071701_il2cpp_TypeInfo_var;
extern const uint32_t HashSet_1_GetObjectData_m3183843147_MetadataUsageId;
extern const uint32_t HashSet_1_OnDeserialization_m3908179593_MetadataUsageId;

struct TypeU5BU5D_t98989581;
struct ObjectU5BU5D_t3385344125;
struct Int32U5BU5D_t3215173111;
struct LinkU5BU5D_t1226396739;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef COMPARER_1_T432285270_H
#define COMPARER_1_T432285270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.DateTime>
struct  Comparer_1_t432285270  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t432285270_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t432285270 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t432285270_StaticFields, ____default_0)); }
	inline Comparer_1_t432285270 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t432285270 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t432285270 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T432285270_H
#ifndef EQUALITYCOMPARER_1_T3605797846_H
#define EQUALITYCOMPARER_1_T3605797846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>
struct  EqualityComparer_1_t3605797846  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3605797846_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3605797846 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3605797846_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3605797846 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3605797846 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3605797846 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3605797846_H
#ifndef EQUALITYCOMPARER_1_T269216792_H
#define EQUALITYCOMPARER_1_T269216792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>
struct  EqualityComparer_1_t269216792  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t269216792_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t269216792 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t269216792_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t269216792 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t269216792 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t269216792 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T269216792_H
#ifndef EQUALITYCOMPARER_1_T614606883_H
#define EQUALITYCOMPARER_1_T614606883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>
struct  EqualityComparer_1_t614606883  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t614606883_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t614606883 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t614606883_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t614606883 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t614606883 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t614606883 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T614606883_H
#ifndef EQUALITYCOMPARER_1_T1312346945_H
#define EQUALITYCOMPARER_1_T1312346945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>
struct  EqualityComparer_1_t1312346945  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1312346945_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1312346945 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1312346945_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1312346945 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1312346945 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1312346945 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1312346945_H
#ifndef EQUALITYCOMPARER_1_T524630218_H
#define EQUALITYCOMPARER_1_T524630218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
struct  EqualityComparer_1_t524630218  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t524630218_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t524630218 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t524630218_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t524630218 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t524630218 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t524630218 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T524630218_H
#ifndef EQUALITYCOMPARER_1_T3503033932_H
#define EQUALITYCOMPARER_1_T3503033932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct  EqualityComparer_1_t3503033932  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3503033932_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3503033932 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3503033932_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3503033932 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3503033932 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3503033932 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3503033932_H
#ifndef EQUALITYCOMPARER_1_T3373603711_H
#define EQUALITYCOMPARER_1_T3373603711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>
struct  EqualityComparer_1_t3373603711  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3373603711_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3373603711 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3373603711_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3373603711 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3373603711 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3373603711 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3373603711_H
#ifndef EQUALITYCOMPARER_1_T2963046443_H
#define EQUALITYCOMPARER_1_T2963046443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>
struct  EqualityComparer_1_t2963046443  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2963046443_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2963046443 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2963046443_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2963046443 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2963046443 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2963046443 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2963046443_H
#ifndef EQUALITYCOMPARER_1_T1071577238_H
#define EQUALITYCOMPARER_1_T1071577238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>
struct  EqualityComparer_1_t1071577238  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1071577238_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1071577238 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1071577238_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1071577238 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1071577238 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1071577238 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1071577238_H
#ifndef EQUALITYCOMPARER_1_T3868451117_H
#define EQUALITYCOMPARER_1_T3868451117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>
struct  EqualityComparer_1_t3868451117  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3868451117_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3868451117 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3868451117_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3868451117 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3868451117 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3868451117 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3868451117_H
#ifndef EQUALITYCOMPARER_1_T595662577_H
#define EQUALITYCOMPARER_1_T595662577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>
struct  EqualityComparer_1_t595662577  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t595662577_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t595662577 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t595662577_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t595662577 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t595662577 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t595662577 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T595662577_H
#ifndef EQUALITYCOMPARER_1_T2006847981_H
#define EQUALITYCOMPARER_1_T2006847981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>
struct  EqualityComparer_1_t2006847981  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2006847981_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2006847981 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2006847981_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2006847981 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2006847981 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2006847981 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2006847981_H
#ifndef EQUALITYCOMPARER_1_T2555363908_H
#define EQUALITYCOMPARER_1_T2555363908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>
struct  EqualityComparer_1_t2555363908  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2555363908_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2555363908 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2555363908_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2555363908 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2555363908 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2555363908 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2555363908_H
#ifndef COMPARER_1_T420124209_H
#define COMPARER_1_T420124209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct  Comparer_1_t420124209  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t420124209_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t420124209 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t420124209_StaticFields, ____default_0)); }
	inline Comparer_1_t420124209 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t420124209 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t420124209 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T420124209_H
#ifndef COMPARER_1_T1865529779_H
#define COMPARER_1_T1865529779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.Guid>
struct  Comparer_1_t1865529779  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t1865529779_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t1865529779 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t1865529779_StaticFields, ____default_0)); }
	inline Comparer_1_t1865529779 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t1865529779 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t1865529779 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T1865529779_H
#ifndef COMPARER_1_T3729212943_H
#define COMPARER_1_T3729212943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.Int32>
struct  Comparer_1_t3729212943  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t3729212943_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t3729212943 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t3729212943_StaticFields, ____default_0)); }
	inline Comparer_1_t3729212943 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t3729212943 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t3729212943 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T3729212943_H
#ifndef COMPARER_1_T765662593_H
#define COMPARER_1_T765662593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.Object>
struct  Comparer_1_t765662593  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t765662593_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t765662593 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t765662593_StaticFields, ____default_0)); }
	inline Comparer_1_t765662593 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t765662593 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t765662593 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T765662593_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T1108148719_H
#define VALUETYPE_T1108148719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1108148719  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_com
{
};
#endif // VALUETYPE_T1108148719_H
#ifndef SERIALIZATIONINFO_T1812684756_H
#define SERIALIZATIONINFO_T1812684756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t1812684756  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t1793686270 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t1797015065 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___serialized_0)); }
	inline Hashtable_t1793686270 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t1793686270 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t1793686270 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___values_1)); }
	inline ArrayList_t1797015065 * get_values_1() const { return ___values_1; }
	inline ArrayList_t1797015065 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t1797015065 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T1812684756_H
#ifndef PRIMEHELPER_T1801316144_H
#define PRIMEHELPER_T1801316144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
struct  PrimeHelper_t1801316144  : public RuntimeObject
{
public:

public:
};

struct PrimeHelper_t1801316144_StaticFields
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1/PrimeHelper::primes_table
	Int32U5BU5D_t3215173111* ___primes_table_0;

public:
	inline static int32_t get_offset_of_primes_table_0() { return static_cast<int32_t>(offsetof(PrimeHelper_t1801316144_StaticFields, ___primes_table_0)); }
	inline Int32U5BU5D_t3215173111* get_primes_table_0() const { return ___primes_table_0; }
	inline Int32U5BU5D_t3215173111** get_address_of_primes_table_0() { return &___primes_table_0; }
	inline void set_primes_table_0(Int32U5BU5D_t3215173111* value)
	{
		___primes_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___primes_table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMEHELPER_T1801316144_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2953840665* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2953840665* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2953840665** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2953840665* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef HASHSET_1_T2249360193_H
#define HASHSET_1_T2249360193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<System.Object>
struct  HashSet_1_t2249360193  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::table
	Int32U5BU5D_t3215173111* ___table_4;
	// System.Collections.Generic.HashSet`1/Link<T>[] System.Collections.Generic.HashSet`1::links
	LinkU5BU5D_t1226396739* ___links_5;
	// T[] System.Collections.Generic.HashSet`1::slots
	ObjectU5BU5D_t3385344125* ___slots_6;
	// System.Int32 System.Collections.Generic.HashSet`1::touched
	int32_t ___touched_7;
	// System.Int32 System.Collections.Generic.HashSet`1::empty_slot
	int32_t ___empty_slot_8;
	// System.Int32 System.Collections.Generic.HashSet`1::count
	int32_t ___count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::threshold
	int32_t ___threshold_10;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::comparer
	RuntimeObject* ___comparer_11;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::si
	SerializationInfo_t1812684756 * ___si_12;
	// System.Int32 System.Collections.Generic.HashSet`1::generation
	int32_t ___generation_13;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___table_4)); }
	inline Int32U5BU5D_t3215173111* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3215173111** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3215173111* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_links_5() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___links_5)); }
	inline LinkU5BU5D_t1226396739* get_links_5() const { return ___links_5; }
	inline LinkU5BU5D_t1226396739** get_address_of_links_5() { return &___links_5; }
	inline void set_links_5(LinkU5BU5D_t1226396739* value)
	{
		___links_5 = value;
		Il2CppCodeGenWriteBarrier((&___links_5), value);
	}

	inline static int32_t get_offset_of_slots_6() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___slots_6)); }
	inline ObjectU5BU5D_t3385344125* get_slots_6() const { return ___slots_6; }
	inline ObjectU5BU5D_t3385344125** get_address_of_slots_6() { return &___slots_6; }
	inline void set_slots_6(ObjectU5BU5D_t3385344125* value)
	{
		___slots_6 = value;
		Il2CppCodeGenWriteBarrier((&___slots_6), value);
	}

	inline static int32_t get_offset_of_touched_7() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___touched_7)); }
	inline int32_t get_touched_7() const { return ___touched_7; }
	inline int32_t* get_address_of_touched_7() { return &___touched_7; }
	inline void set_touched_7(int32_t value)
	{
		___touched_7 = value;
	}

	inline static int32_t get_offset_of_empty_slot_8() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___empty_slot_8)); }
	inline int32_t get_empty_slot_8() const { return ___empty_slot_8; }
	inline int32_t* get_address_of_empty_slot_8() { return &___empty_slot_8; }
	inline void set_empty_slot_8(int32_t value)
	{
		___empty_slot_8 = value;
	}

	inline static int32_t get_offset_of_count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___count_9)); }
	inline int32_t get_count_9() const { return ___count_9; }
	inline int32_t* get_address_of_count_9() { return &___count_9; }
	inline void set_count_9(int32_t value)
	{
		___count_9 = value;
	}

	inline static int32_t get_offset_of_threshold_10() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___threshold_10)); }
	inline int32_t get_threshold_10() const { return ___threshold_10; }
	inline int32_t* get_address_of_threshold_10() { return &___threshold_10; }
	inline void set_threshold_10(int32_t value)
	{
		___threshold_10 = value;
	}

	inline static int32_t get_offset_of_comparer_11() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___comparer_11)); }
	inline RuntimeObject* get_comparer_11() const { return ___comparer_11; }
	inline RuntimeObject** get_address_of_comparer_11() { return &___comparer_11; }
	inline void set_comparer_11(RuntimeObject* value)
	{
		___comparer_11 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_11), value);
	}

	inline static int32_t get_offset_of_si_12() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___si_12)); }
	inline SerializationInfo_t1812684756 * get_si_12() const { return ___si_12; }
	inline SerializationInfo_t1812684756 ** get_address_of_si_12() { return &___si_12; }
	inline void set_si_12(SerializationInfo_t1812684756 * value)
	{
		___si_12 = value;
		Il2CppCodeGenWriteBarrier((&___si_12), value);
	}

	inline static int32_t get_offset_of_generation_13() { return static_cast<int32_t>(offsetof(HashSet_1_t2249360193, ___generation_13)); }
	inline int32_t get_generation_13() const { return ___generation_13; }
	inline int32_t* get_address_of_generation_13() { return &___generation_13; }
	inline void set_generation_13(int32_t value)
	{
		___generation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T2249360193_H
#ifndef EQUALITYCOMPARER_1_T3785776845_H
#define EQUALITYCOMPARER_1_T3785776845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>
struct  EqualityComparer_1_t3785776845  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3785776845_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3785776845 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3785776845_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3785776845 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3785776845 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3785776845 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3785776845_H
#ifndef EQUALITYCOMPARER_1_T3215232089_H
#define EQUALITYCOMPARER_1_T3215232089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Object>
struct  EqualityComparer_1_t3215232089  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3215232089_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3215232089 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3215232089_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3215232089 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3215232089 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3215232089 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3215232089_H
#ifndef EQUALITYCOMPARER_1_T20131979_H
#define EQUALITYCOMPARER_1_T20131979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Guid>
struct  EqualityComparer_1_t20131979  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t20131979_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t20131979 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t20131979_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t20131979 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t20131979 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t20131979 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T20131979_H
#ifndef EQUALITYCOMPARER_1_T2869693705_H
#define EQUALITYCOMPARER_1_T2869693705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct  EqualityComparer_1_t2869693705  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2869693705_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2869693705 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2869693705_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2869693705 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2869693705 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2869693705 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2869693705_H
#ifndef EQUALITYCOMPARER_1_T2881854766_H
#define EQUALITYCOMPARER_1_T2881854766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct  EqualityComparer_1_t2881854766  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2881854766_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2881854766 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2881854766_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2881854766 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2881854766 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2881854766 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2881854766_H
#ifndef EQUALITYCOMPARER_1_T4285841389_H
#define EQUALITYCOMPARER_1_T4285841389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Char>
struct  EqualityComparer_1_t4285841389  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t4285841389_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t4285841389 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t4285841389_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t4285841389 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t4285841389 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t4285841389 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T4285841389_H
#ifndef EQUALITYCOMPARER_1_T808457599_H
#define EQUALITYCOMPARER_1_T808457599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct  EqualityComparer_1_t808457599  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t808457599_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t808457599 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t808457599_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t808457599 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t808457599 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t808457599 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T808457599_H
#ifndef COMPARER_1_T809005246_H
#define COMPARER_1_T809005246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct  Comparer_1_t809005246  : public RuntimeObject
{
public:

public:
};

struct Comparer_1_t809005246_StaticFields
{
public:
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t809005246 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(Comparer_1_t809005246_StaticFields, ____default_0)); }
	inline Comparer_1_t809005246 * get__default_0() const { return ____default_0; }
	inline Comparer_1_t809005246 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(Comparer_1_t809005246 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_1_T809005246_H
#ifndef EQUALITYCOMPARER_1_T1883815143_H
#define EQUALITYCOMPARER_1_T1883815143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct  EqualityComparer_1_t1883815143  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1883815143_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1883815143 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1883815143_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1883815143 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1883815143 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1883815143 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1883815143_H
#ifndef EQUALITYCOMPARER_1_T1137787835_H
#define EQUALITYCOMPARER_1_T1137787835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>
struct  EqualityComparer_1_t1137787835  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1137787835_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1137787835 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1137787835_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1137787835 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1137787835 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1137787835 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1137787835_H
#ifndef EQUALITYCOMPARER_1_T2300630690_H
#define EQUALITYCOMPARER_1_T2300630690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>
struct  EqualityComparer_1_t2300630690  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t2300630690_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t2300630690 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t2300630690_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t2300630690 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t2300630690 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t2300630690 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T2300630690_H
#ifndef EXCEPTION_T214279536_H
#define EXCEPTION_T214279536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t214279536  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t3930959911* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t214279536 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t3930959911* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t3930959911** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t3930959911* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___inner_exception_1)); }
	inline Exception_t214279536 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t214279536 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t214279536 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t214279536, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t214279536, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T214279536_H
#ifndef EQUALITYCOMPARER_1_T3258574742_H
#define EQUALITYCOMPARER_1_T3258574742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct  EqualityComparer_1_t3258574742  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3258574742_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3258574742 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3258574742_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3258574742 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3258574742 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3258574742 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3258574742_H
#ifndef EQUALITYCOMPARER_1_T1814298720_H
#define EQUALITYCOMPARER_1_T1814298720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>
struct  EqualityComparer_1_t1814298720  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1814298720_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1814298720 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1814298720_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1814298720 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1814298720 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1814298720 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1814298720_H
#ifndef EQUALITYCOMPARER_1_T1206011620_H
#define EQUALITYCOMPARER_1_T1206011620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>
struct  EqualityComparer_1_t1206011620  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1206011620_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1206011620 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1206011620_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1206011620 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1206011620 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1206011620 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1206011620_H
#ifndef EQUALITYCOMPARER_1_T1951216805_H
#define EQUALITYCOMPARER_1_T1951216805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>
struct  EqualityComparer_1_t1951216805  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1951216805_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1951216805 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1951216805_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1951216805 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1951216805 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1951216805 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1951216805_H
#ifndef EQUALITYCOMPARER_1_T326780482_H
#define EQUALITYCOMPARER_1_T326780482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>
struct  EqualityComparer_1_t326780482  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t326780482_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t326780482 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t326780482_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t326780482 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t326780482 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t326780482 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T326780482_H
#ifndef EQUALITYCOMPARER_1_T1843515292_H
#define EQUALITYCOMPARER_1_T1843515292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct  EqualityComparer_1_t1843515292  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1843515292_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1843515292 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1843515292_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1843515292 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1843515292 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1843515292 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1843515292_H
#ifndef EQUALITYCOMPARER_1_T3561003404_H
#define EQUALITYCOMPARER_1_T3561003404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.Single>
struct  EqualityComparer_1_t3561003404  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3561003404_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3561003404 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3561003404_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3561003404 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3561003404 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3561003404 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3561003404_H
#ifndef EQUALITYCOMPARER_1_T1299855184_H
#define EQUALITYCOMPARER_1_T1299855184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>
struct  EqualityComparer_1_t1299855184  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t1299855184_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1299855184 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t1299855184_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t1299855184 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t1299855184 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t1299855184 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T1299855184_H
#ifndef EQUALITYCOMPARER_1_T3766329106_H
#define EQUALITYCOMPARER_1_T3766329106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>
struct  EqualityComparer_1_t3766329106  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3766329106_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3766329106 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3766329106_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3766329106 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3766329106 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3766329106 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3766329106_H
#ifndef EQUALITYCOMPARER_1_T3325628940_H
#define EQUALITYCOMPARER_1_T3325628940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>
struct  EqualityComparer_1_t3325628940  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3325628940_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3325628940 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3325628940_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3325628940 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3325628940 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3325628940 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3325628940_H
#ifndef GENERICEQUALITYCOMPARER_1_T609334878_H
#define GENERICEQUALITYCOMPARER_1_T609334878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct  GenericEqualityComparer_1_t609334878  : public EqualityComparer_1_t20131979
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T609334878_H
#ifndef DEFAULTCOMPARER_T1463699850_H
#define DEFAULTCOMPARER_T1463699850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct  DefaultComparer_t1463699850  : public EqualityComparer_1_t3258574742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1463699850_H
#ifndef GENERICEQUALITYCOMPARER_1_T3458896604_H
#define GENERICEQUALITYCOMPARER_1_T3458896604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct  GenericEqualityComparer_1_t3458896604  : public EqualityComparer_1_t2869693705
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T3458896604_H
#ifndef TIMESPAN_T595369841_H
#define TIMESPAN_T595369841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t595369841 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t595369841_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t595369841  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t595369841  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t595369841  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t595369841  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t595369841 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t595369841  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t595369841  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t595369841 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t595369841  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t595369841_StaticFields, ___Zero_2)); }
	inline TimeSpan_t595369841  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t595369841 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t595369841  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T595369841_H
#ifndef GENERICEQUALITYCOMPARER_1_T3471057665_H
#define GENERICEQUALITYCOMPARER_1_T3471057665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct  GenericEqualityComparer_1_t3471057665  : public EqualityComparer_1_t2881854766
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T3471057665_H
#ifndef DEFAULTCOMPARER_T1530754048_H
#define DEFAULTCOMPARER_T1530754048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>
struct  DefaultComparer_t1530754048  : public EqualityComparer_1_t3325628940
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1530754048_H
#ifndef GENERICEQUALITYCOMPARER_1_T580076992_H
#define GENERICEQUALITYCOMPARER_1_T580076992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Char>
struct  GenericEqualityComparer_1_t580076992  : public EqualityComparer_1_t4285841389
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T580076992_H
#ifndef GENERICEQUALITYCOMPARER_1_T1397660498_H
#define GENERICEQUALITYCOMPARER_1_T1397660498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct  GenericEqualityComparer_1_t1397660498  : public EqualityComparer_1_t808457599
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T1397660498_H
#ifndef COLOR32_T662424039_H
#define COLOR32_T662424039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t662424039 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T662424039_H
#ifndef GENERICCOMPARER_1_T880638313_H
#define GENERICCOMPARER_1_T880638313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct  GenericComparer_1_t880638313  : public Comparer_1_t809005246
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T880638313_H
#ifndef GENERICCOMPARER_1_T837295660_H
#define GENERICCOMPARER_1_T837295660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.Object>
struct  GenericComparer_1_t837295660  : public Comparer_1_t765662593
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T837295660_H
#ifndef DEFAULTCOMPARER_T2826872886_H
#define DEFAULTCOMPARER_T2826872886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct  DefaultComparer_t2826872886  : public EqualityComparer_1_t326780482
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2826872886_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2272071576 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2272071576 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2272071576 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2272071576 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2272071576 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2272071576 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2272071576 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2272071576 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef GENERICCOMPARER_1_T1937162846_H
#define GENERICCOMPARER_1_T1937162846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct  GenericComparer_1_t1937162846  : public Comparer_1_t1865529779
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T1937162846_H
#ifndef CHAR_T1622636488_H
#define CHAR_T1622636488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t1622636488 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t1622636488, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t1622636488_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T1622636488_H
#ifndef GENERICEQUALITYCOMPARER_1_T2473018042_H
#define GENERICEQUALITYCOMPARER_1_T2473018042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct  GenericEqualityComparer_1_t2473018042  : public EqualityComparer_1_t1883815143
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T2473018042_H
#ifndef INT32_T3515577538_H
#define INT32_T3515577538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3515577538 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3515577538, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3515577538_H
#ifndef GENERICEQUALITYCOMPARER_1_T3804434988_H
#define GENERICEQUALITYCOMPARER_1_T3804434988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Object>
struct  GenericEqualityComparer_1_t3804434988  : public EqualityComparer_1_t3215232089
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T3804434988_H
#ifndef U24ARRAYTYPEU241024_T581083018_H
#define U24ARRAYTYPEU241024_T581083018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1024
struct  U24ArrayTypeU241024_t581083018 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241024_t581083018__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241024_T581083018_H
#ifndef U24ARRAYTYPEU24256_T3583076827_H
#define U24ARRAYTYPEU24256_T3583076827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$256
struct  U24ArrayTypeU24256_t3583076827 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t3583076827__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T3583076827_H
#ifndef U24ARRAYTYPEU24120_T1535177066_H
#define U24ARRAYTYPEU24120_T1535177066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$120
struct  U24ArrayTypeU24120_t1535177066 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24120_t1535177066__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24120_T1535177066_H
#ifndef SYSTEMEXCEPTION_T1379941789_H
#define SYSTEMEXCEPTION_T1379941789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t1379941789  : public Exception_t214279536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T1379941789_H
#ifndef MATRIX4X4_T787966842_H
#define MATRIX4X4_T787966842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t787966842 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t787966842_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t787966842  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t787966842  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t787966842  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t787966842 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t787966842  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t787966842  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t787966842 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t787966842  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T787966842_H
#ifndef COLOR_T1361298052_H
#define COLOR_T1361298052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t1361298052 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T1361298052_H
#ifndef ENUM_T2432427458_H
#define ENUM_T2432427458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2432427458  : public ValueType_t1108148719
{
public:

public:
};

struct Enum_t2432427458_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2953840665* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2432427458_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2953840665* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2953840665** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2953840665* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2432427458_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2432427458_marshaled_com
{
};
#endif // ENUM_T2432427458_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T4071739332_H
#define VOID_T4071739332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t4071739332 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T4071739332_H
#ifndef SINGLE_T897798503_H
#define SINGLE_T897798503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t897798503 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t897798503, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T897798503_H
#ifndef DOUBLE_T3942828892_H
#define DOUBLE_T3942828892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t3942828892 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t3942828892, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T3942828892_H
#ifndef LINK_T890348390_H
#define LINK_T890348390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Link<System.Object>
struct  Link_t890348390 
{
public:
	// System.Int32 System.Collections.Generic.HashSet`1/Link::HashCode
	int32_t ___HashCode_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Link::Next
	int32_t ___Next_1;

public:
	inline static int32_t get_offset_of_HashCode_0() { return static_cast<int32_t>(offsetof(Link_t890348390, ___HashCode_0)); }
	inline int32_t get_HashCode_0() const { return ___HashCode_0; }
	inline int32_t* get_address_of_HashCode_0() { return &___HashCode_0; }
	inline void set_HashCode_0(int32_t value)
	{
		___HashCode_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Link_t890348390, ___Next_1)); }
	inline int32_t get_Next_1() const { return ___Next_1; }
	inline int32_t* get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(int32_t value)
	{
		___Next_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T890348390_H
#ifndef BOOLEAN_T2440219994_H
#define BOOLEAN_T2440219994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2440219994 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2440219994, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2440219994_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2440219994_H
#ifndef DEFAULTCOMPARER_T1766128512_H
#define DEFAULTCOMPARER_T1766128512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>
struct  DefaultComparer_t1766128512  : public EqualityComparer_1_t3561003404
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1766128512_H
#ifndef ENUMERATOR_T614413977_H
#define ENUMERATOR_T614413977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct  Enumerator_t614413977 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator::hashset
	HashSet_1_t2249360193 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_hashset_0() { return static_cast<int32_t>(offsetof(Enumerator_t614413977, ___hashset_0)); }
	inline HashSet_1_t2249360193 * get_hashset_0() const { return ___hashset_0; }
	inline HashSet_1_t2249360193 ** get_address_of_hashset_0() { return &___hashset_0; }
	inline void set_hashset_0(HashSet_1_t2249360193 * value)
	{
		___hashset_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashset_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t614413977, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t614413977, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t614413977, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T614413977_H
#ifndef GENERICEQUALITYCOMPARER_1_T3962806610_H
#define GENERICEQUALITYCOMPARER_1_T3962806610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.SpriteState>
struct  GenericEqualityComparer_1_t3962806610  : public EqualityComparer_1_t3373603711
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T3962806610_H
#ifndef GENERICEQUALITYCOMPARER_1_T1184865476_H
#define GENERICEQUALITYCOMPARER_1_T1184865476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.Navigation>
struct  GenericEqualityComparer_1_t1184865476  : public EqualityComparer_1_t595662577
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T1184865476_H
#ifndef GENERICEQUALITYCOMPARER_1_T2403501619_H
#define GENERICEQUALITYCOMPARER_1_T2403501619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.ColorBlock>
struct  GenericEqualityComparer_1_t2403501619  : public EqualityComparer_1_t1814298720
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T2403501619_H
#ifndef GENERICEQUALITYCOMPARER_1_T3847777641_H
#define GENERICEQUALITYCOMPARER_1_T3847777641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct  GenericEqualityComparer_1_t3847777641  : public EqualityComparer_1_t3258574742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T3847777641_H
#ifndef GENERICEQUALITYCOMPARER_1_T4150206303_H
#define GENERICEQUALITYCOMPARER_1_T4150206303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct  GenericEqualityComparer_1_t4150206303  : public EqualityComparer_1_t3561003404
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICEQUALITYCOMPARER_1_T4150206303_H
#ifndef U24ARRAYTYPEU24136_T1586780108_H
#define U24ARRAYTYPEU24136_T1586780108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$136
struct  U24ArrayTypeU24136_t1586780108 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24136_t1586780108__padding[136];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24136_T1586780108_H
#ifndef GENERICCOMPARER_1_T491757276_H
#define GENERICCOMPARER_1_T491757276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct  GenericComparer_1_t491757276  : public Comparer_1_t420124209
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T491757276_H
#ifndef GENERICCOMPARER_1_T3800846010_H
#define GENERICCOMPARER_1_T3800846010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.Int32>
struct  GenericComparer_1_t3800846010  : public Comparer_1_t3729212943
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T3800846010_H
#ifndef DEFAULTCOMPARER_T1971454214_H
#define DEFAULTCOMPARER_T1971454214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TouchScreenKeyboardType>
struct  DefaultComparer_t1971454214  : public EqualityComparer_1_t3766329106
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1971454214_H
#ifndef DEFAULTCOMPARER_T3024722622_H
#define DEFAULTCOMPARER_T3024722622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct  DefaultComparer_t3024722622  : public EqualityComparer_1_t524630218
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3024722622_H
#ifndef DEFAULTCOMPARER_T1708159040_H
#define DEFAULTCOMPARER_T1708159040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct  DefaultComparer_t1708159040  : public EqualityComparer_1_t3503033932
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1708159040_H
#ifndef DEFAULTCOMPARER_T19423828_H
#define DEFAULTCOMPARER_T19423828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ColorBlock>
struct  DefaultComparer_t19423828  : public EqualityComparer_1_t1814298720
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T19423828_H
#ifndef SPRITESTATE_T710398810_H
#define SPRITESTATE_T710398810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t710398810 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t2497061588 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t2497061588 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t2497061588 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t710398810, ___m_HighlightedSprite_0)); }
	inline Sprite_t2497061588 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t2497061588 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t2497061588 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t710398810, ___m_PressedSprite_1)); }
	inline Sprite_t2497061588 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t2497061588 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t2497061588 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t710398810, ___m_DisabledSprite_2)); }
	inline Sprite_t2497061588 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t2497061588 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t2497061588 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t710398810_marshaled_pinvoke
{
	Sprite_t2497061588 * ___m_HighlightedSprite_0;
	Sprite_t2497061588 * ___m_PressedSprite_1;
	Sprite_t2497061588 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t710398810_marshaled_com
{
	Sprite_t2497061588 * ___m_HighlightedSprite_0;
	Sprite_t2497061588 * ___m_PressedSprite_1;
	Sprite_t2497061588 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T710398810_H
#ifndef DEFAULTCOMPARER_T1578728819_H
#define DEFAULTCOMPARER_T1578728819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.SpriteState>
struct  DefaultComparer_t1578728819  : public EqualityComparer_1_t3373603711
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1578728819_H
#ifndef DEFAULTCOMPARER_T1168171551_H
#define DEFAULTCOMPARER_T1168171551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Slider/Direction>
struct  DefaultComparer_t1168171551  : public EqualityComparer_1_t2963046443
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1168171551_H
#ifndef UILINEINFO_T2156392613_H
#define UILINEINFO_T2156392613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UILineInfo
struct  UILineInfo_t2156392613 
{
public:
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;

public:
	inline static int32_t get_offset_of_startCharIdx_0() { return static_cast<int32_t>(offsetof(UILineInfo_t2156392613, ___startCharIdx_0)); }
	inline int32_t get_startCharIdx_0() const { return ___startCharIdx_0; }
	inline int32_t* get_address_of_startCharIdx_0() { return &___startCharIdx_0; }
	inline void set_startCharIdx_0(int32_t value)
	{
		___startCharIdx_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(UILineInfo_t2156392613, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_topY_2() { return static_cast<int32_t>(offsetof(UILineInfo_t2156392613, ___topY_2)); }
	inline float get_topY_2() const { return ___topY_2; }
	inline float* get_address_of_topY_2() { return &___topY_2; }
	inline void set_topY_2(float value)
	{
		___topY_2 = value;
	}

	inline static int32_t get_offset_of_leading_3() { return static_cast<int32_t>(offsetof(UILineInfo_t2156392613, ___leading_3)); }
	inline float get_leading_3() const { return ___leading_3; }
	inline float* get_address_of_leading_3() { return &___leading_3; }
	inline void set_leading_3(float value)
	{
		___leading_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEINFO_T2156392613_H
#ifndef DEFAULTCOMPARER_T3571669642_H
#define DEFAULTCOMPARER_T3571669642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable/Transition>
struct  DefaultComparer_t3571669642  : public EqualityComparer_1_t1071577238
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3571669642_H
#ifndef DEFAULTCOMPARER_T3095754981_H
#define DEFAULTCOMPARER_T3095754981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Navigation>
struct  DefaultComparer_t3095754981  : public EqualityComparer_1_t595662577
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3095754981_H
#ifndef DEFAULTCOMPARER_T211973089_H
#define DEFAULTCOMPARER_T211973089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/LineType>
struct  DefaultComparer_t211973089  : public EqualityComparer_1_t2006847981
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T211973089_H
#ifndef DEFAULTCOMPARER_T3799947588_H
#define DEFAULTCOMPARER_T3799947588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/FillMethod>
struct  DefaultComparer_t3799947588  : public EqualityComparer_1_t1299855184
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3799947588_H
#ifndef DEFAULTCOMPARER_T760489016_H
#define DEFAULTCOMPARER_T760489016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/InputType>
struct  DefaultComparer_t760489016  : public EqualityComparer_1_t2555363908
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T760489016_H
#ifndef DEFAULTCOMPARER_T1990901953_H
#define DEFAULTCOMPARER_T1990901953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/ContentType>
struct  DefaultComparer_t1990901953  : public EqualityComparer_1_t3785776845
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1990901953_H
#ifndef DEFAULTCOMPARER_T3637880239_H
#define DEFAULTCOMPARER_T3637880239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.InputField/CharacterValidation>
struct  DefaultComparer_t3637880239  : public EqualityComparer_1_t1137787835
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3637880239_H
#ifndef DEFAULTCOMPARER_T2073576225_H
#define DEFAULTCOMPARER_T2073576225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Scrollbar/Direction>
struct  DefaultComparer_t2073576225  : public EqualityComparer_1_t3868451117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2073576225_H
#ifndef DEFAULTCOMPARER_T3812439349_H
#define DEFAULTCOMPARER_T3812439349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct  DefaultComparer_t3812439349  : public EqualityComparer_1_t1312346945
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3812439349_H
#ifndef DEFAULTCOMPARER_T156341913_H
#define DEFAULTCOMPARER_T156341913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.ContentSizeFitter/FitMode>
struct  DefaultComparer_t156341913  : public EqualityComparer_1_t1951216805
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T156341913_H
#ifndef DEFAULTCOMPARER_T3706104024_H
#define DEFAULTCOMPARER_T3706104024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Image/Type>
struct  DefaultComparer_t3706104024  : public EqualityComparer_1_t1206011620
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3706104024_H
#ifndef VECTOR4_T1900979187_H
#define VECTOR4_T1900979187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t1900979187 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t1900979187_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t1900979187  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t1900979187  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t1900979187  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t1900979187  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___zeroVector_5)); }
	inline Vector4_t1900979187  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t1900979187 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t1900979187  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___oneVector_6)); }
	inline Vector4_t1900979187  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t1900979187 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t1900979187  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t1900979187  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t1900979187 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t1900979187  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t1900979187  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t1900979187 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t1900979187  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T1900979187_H
#ifndef DEFAULTCOMPARER_T2769309196_H
#define DEFAULTCOMPARER_T2769309196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector4>
struct  DefaultComparer_t2769309196  : public EqualityComparer_1_t269216792
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T2769309196_H
#ifndef VECTOR3_T3932393085_H
#define VECTOR3_T3932393085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3932393085 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3932393085_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3932393085  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3932393085  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3932393085  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3932393085  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3932393085  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3932393085  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3932393085  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3932393085  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3932393085  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3932393085  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3932393085  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3932393085 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3932393085  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___oneVector_5)); }
	inline Vector3_t3932393085  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3932393085 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3932393085  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___upVector_6)); }
	inline Vector3_t3932393085  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3932393085 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3932393085  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___downVector_7)); }
	inline Vector3_t3932393085  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3932393085 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3932393085  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___leftVector_8)); }
	inline Vector3_t3932393085  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3932393085 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3932393085  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___rightVector_9)); }
	inline Vector3_t3932393085  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3932393085 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3932393085  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3932393085  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3932393085 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3932393085  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___backVector_11)); }
	inline Vector3_t3932393085  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3932393085 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3932393085  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3932393085  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3932393085 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3932393085  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3932393085  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3932393085 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3932393085  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3932393085_H
#ifndef DEFAULTCOMPARER_T505755798_H
#define DEFAULTCOMPARER_T505755798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct  DefaultComparer_t505755798  : public EqualityComparer_1_t2300630690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T505755798_H
#ifndef DEFAULTCOMPARER_T1810922954_H
#define DEFAULTCOMPARER_T1810922954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.XR.iOS.ARHitTestResult>
struct  DefaultComparer_t1810922954  : public EqualityComparer_1_t3605797846
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T1810922954_H
#ifndef VECTOR2_T2246369278_H
#define VECTOR2_T2246369278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2246369278 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2246369278_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2246369278  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2246369278  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2246369278  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2246369278  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2246369278  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2246369278  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2246369278  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2246369278  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2246369278  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2246369278 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2246369278  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___oneVector_3)); }
	inline Vector2_t2246369278  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2246369278 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2246369278  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___upVector_4)); }
	inline Vector2_t2246369278  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2246369278 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2246369278  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___downVector_5)); }
	inline Vector2_t2246369278  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2246369278 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2246369278  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___leftVector_6)); }
	inline Vector2_t2246369278  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2246369278 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2246369278  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___rightVector_7)); }
	inline Vector2_t2246369278  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2246369278 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2246369278  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2246369278  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2246369278 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2246369278  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2246369278  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2246369278 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2246369278  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2246369278_H
#ifndef DEFAULTCOMPARER_T3114699287_H
#define DEFAULTCOMPARER_T3114699287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>
struct  DefaultComparer_t3114699287  : public EqualityComparer_1_t614606883
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T3114699287_H
#ifndef DEFAULTCOMPARER_T48640400_H
#define DEFAULTCOMPARER_T48640400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.AspectRatioFitter/AspectMode>
struct  DefaultComparer_t48640400  : public EqualityComparer_1_t1843515292
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCOMPARER_T48640400_H
#ifndef GENERICCOMPARER_1_T503918337_H
#define GENERICCOMPARER_1_T503918337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct  GenericComparer_1_t503918337  : public Comparer_1_t432285270
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICCOMPARER_1_T503918337_H
#ifndef ARGUMENTEXCEPTION_T489606696_H
#define ARGUMENTEXCEPTION_T489606696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t489606696  : public SystemException_t1379941789
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t489606696, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T489606696_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T3535071701_H
#define NOTIMPLEMENTEDEXCEPTION_T3535071701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t3535071701  : public SystemException_t1379941789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T3535071701_H
#ifndef LINETYPE_T3638610376_H
#define LINETYPE_T3638610376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t3638610376 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t3638610376, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T3638610376_H
#ifndef TYPE_T2837774015_H
#define TYPE_T2837774015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t2837774015 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2837774015, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2837774015_H
#ifndef BINDINGFLAGS_T100712108_H
#define BINDINGFLAGS_T100712108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t100712108 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t100712108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T100712108_H
#ifndef INPUTTYPE_T4187126303_H
#define INPUTTYPE_T4187126303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t4187126303 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t4187126303, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T4187126303_H
#ifndef MODE_T2971001583_H
#define MODE_T2971001583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t2971001583 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t2971001583, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2971001583_H
#ifndef ARHITTESTRESULTTYPE_T1760080916_H
#define ARHITTESTRESULTTYPE_T1760080916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t1760080916 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t1760080916, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T1760080916_H
#ifndef RUNTIMETYPEHANDLE_T1986816146_H
#define RUNTIMETYPEHANDLE_T1986816146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t1986816146 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t1986816146, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T1986816146_H
#ifndef DATETIMEKIND_T682864375_H
#define DATETIMEKIND_T682864375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t682864375 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t682864375, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T682864375_H
#ifndef CONTENTTYPE_T1122571944_H
#define CONTENTTYPE_T1122571944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t1122571944 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t1122571944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1122571944_H
#ifndef CHARACTERVALIDATION_T2769550230_H
#define CHARACTERVALIDATION_T2769550230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t2769550230 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t2769550230, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T2769550230_H
#ifndef RAYCASTRESULT_T1958542877_H
#define RAYCASTRESULT_T1958542877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t1958542877 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t2881801184 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t2460995416 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3932393085  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3932393085  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2246369278  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___m_GameObject_0)); }
	inline GameObject_t2881801184 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t2881801184 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t2881801184 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___module_1)); }
	inline BaseRaycaster_t2460995416 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t2460995416 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t2460995416 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___worldPosition_7)); }
	inline Vector3_t3932393085  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3932393085 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3932393085  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___worldNormal_8)); }
	inline Vector3_t3932393085  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3932393085 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3932393085  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t1958542877, ___screenPosition_9)); }
	inline Vector2_t2246369278  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2246369278 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2246369278  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t1958542877_marshaled_pinvoke
{
	GameObject_t2881801184 * ___m_GameObject_0;
	BaseRaycaster_t2460995416 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3932393085  ___worldPosition_7;
	Vector3_t3932393085  ___worldNormal_8;
	Vector2_t2246369278  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t1958542877_marshaled_com
{
	GameObject_t2881801184 * ___m_GameObject_0;
	BaseRaycaster_t2460995416 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3932393085  ___worldPosition_7;
	Vector3_t3932393085  ___worldNormal_8;
	Vector2_t2246369278  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T1958542877_H
#ifndef FILLMETHOD_T2931617579_H
#define FILLMETHOD_T2931617579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t2931617579 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t2931617579, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T2931617579_H
#ifndef ASPECTMODE_T3475277687_H
#define ASPECTMODE_T3475277687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t3475277687 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t3475277687, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T3475277687_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1103124205_H
#define TOUCHSCREENKEYBOARDTYPE_T1103124205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1103124205 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1103124205, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1103124205_H
#ifndef UICHARINFO_T839829031_H
#define UICHARINFO_T839829031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UICharInfo
struct  UICharInfo_t839829031 
{
public:
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t2246369278  ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;

public:
	inline static int32_t get_offset_of_cursorPos_0() { return static_cast<int32_t>(offsetof(UICharInfo_t839829031, ___cursorPos_0)); }
	inline Vector2_t2246369278  get_cursorPos_0() const { return ___cursorPos_0; }
	inline Vector2_t2246369278 * get_address_of_cursorPos_0() { return &___cursorPos_0; }
	inline void set_cursorPos_0(Vector2_t2246369278  value)
	{
		___cursorPos_0 = value;
	}

	inline static int32_t get_offset_of_charWidth_1() { return static_cast<int32_t>(offsetof(UICharInfo_t839829031, ___charWidth_1)); }
	inline float get_charWidth_1() const { return ___charWidth_1; }
	inline float* get_address_of_charWidth_1() { return &___charWidth_1; }
	inline void set_charWidth_1(float value)
	{
		___charWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHARINFO_T839829031_H
#ifndef STREAMINGCONTEXTSTATES_T2000903969_H
#define STREAMINGCONTEXTSTATES_T2000903969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t2000903969 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t2000903969, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T2000903969_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1622590248_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1622590248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1622590248  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$136 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU24136_t1586780108  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU24120_t1535177066  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU24256_t3583076827  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU24256_t3583076827  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D9_9;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-10
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D10_10;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU241024_t581083018  ___U24U24fieldU2D11_11;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24136_t1586780108  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24136_t1586780108 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24136_t1586780108  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU24120_t1535177066  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU24120_t1535177066 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU24120_t1535177066  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU24256_t3583076827  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU24256_t3583076827 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU24256_t3583076827  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU24256_t3583076827  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU24256_t3583076827 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU24256_t3583076827  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D9_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D9_9)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D9_9() const { return ___U24U24fieldU2D9_9; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D9_9() { return &___U24U24fieldU2D9_9; }
	inline void set_U24U24fieldU2D9_9(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D9_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D10_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D10_10)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D10_10() const { return ___U24U24fieldU2D10_10; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D10_10() { return &___U24U24fieldU2D10_10; }
	inline void set_U24U24fieldU2D10_10(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D10_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D11_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1622590248_StaticFields, ___U24U24fieldU2D11_11)); }
	inline U24ArrayTypeU241024_t581083018  get_U24U24fieldU2D11_11() const { return ___U24U24fieldU2D11_11; }
	inline U24ArrayTypeU241024_t581083018 * get_address_of_U24U24fieldU2D11_11() { return &___U24U24fieldU2D11_11; }
	inline void set_U24U24fieldU2D11_11(U24ArrayTypeU241024_t581083018  value)
	{
		___U24U24fieldU2D11_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1622590248_H
#ifndef RUNTIMEFIELDHANDLE_T1393214719_H
#define RUNTIMEFIELDHANDLE_T1393214719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1393214719 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1393214719, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1393214719_H
#ifndef COLORBLOCK_T3446061115_H
#define COLORBLOCK_T3446061115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t3446061115 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t1361298052  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t1361298052  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t1361298052  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t1361298052  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t3446061115, ___m_NormalColor_0)); }
	inline Color_t1361298052  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t1361298052 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t1361298052  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t3446061115, ___m_HighlightedColor_1)); }
	inline Color_t1361298052  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t1361298052 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t1361298052  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t3446061115, ___m_PressedColor_2)); }
	inline Color_t1361298052  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t1361298052 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t1361298052  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t3446061115, ___m_DisabledColor_3)); }
	inline Color_t1361298052  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t1361298052 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t1361298052  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t3446061115, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t3446061115, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T3446061115_H
#ifndef FITMODE_T3582979200_H
#define FITMODE_T3582979200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t3582979200 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t3582979200, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T3582979200_H
#ifndef DIRECTION_T299841542_H
#define DIRECTION_T299841542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t299841542 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t299841542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T299841542_H
#ifndef TRANSITION_T2703339633_H
#define TRANSITION_T2703339633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t2703339633 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t2703339633, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T2703339633_H
#ifndef INVALIDOPERATIONEXCEPTION_T242246640_H
#define INVALIDOPERATIONEXCEPTION_T242246640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t242246640  : public SystemException_t1379941789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T242246640_H
#ifndef UIVERTEX_T2944109340_H
#define UIVERTEX_T2944109340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t2944109340 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t3932393085  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t3932393085  ___normal_1;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t662424039  ___color_2;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_t2246369278  ___uv0_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_t2246369278  ___uv1_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_t2246369278  ___uv2_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_t2246369278  ___uv3_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t1900979187  ___tangent_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___position_0)); }
	inline Vector3_t3932393085  get_position_0() const { return ___position_0; }
	inline Vector3_t3932393085 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3932393085  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___normal_1)); }
	inline Vector3_t3932393085  get_normal_1() const { return ___normal_1; }
	inline Vector3_t3932393085 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t3932393085  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___color_2)); }
	inline Color32_t662424039  get_color_2() const { return ___color_2; }
	inline Color32_t662424039 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color32_t662424039  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_uv0_3() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___uv0_3)); }
	inline Vector2_t2246369278  get_uv0_3() const { return ___uv0_3; }
	inline Vector2_t2246369278 * get_address_of_uv0_3() { return &___uv0_3; }
	inline void set_uv0_3(Vector2_t2246369278  value)
	{
		___uv0_3 = value;
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___uv1_4)); }
	inline Vector2_t2246369278  get_uv1_4() const { return ___uv1_4; }
	inline Vector2_t2246369278 * get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(Vector2_t2246369278  value)
	{
		___uv1_4 = value;
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___uv2_5)); }
	inline Vector2_t2246369278  get_uv2_5() const { return ___uv2_5; }
	inline Vector2_t2246369278 * get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(Vector2_t2246369278  value)
	{
		___uv2_5 = value;
	}

	inline static int32_t get_offset_of_uv3_6() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___uv3_6)); }
	inline Vector2_t2246369278  get_uv3_6() const { return ___uv3_6; }
	inline Vector2_t2246369278 * get_address_of_uv3_6() { return &___uv3_6; }
	inline void set_uv3_6(Vector2_t2246369278  value)
	{
		___uv3_6 = value;
	}

	inline static int32_t get_offset_of_tangent_7() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340, ___tangent_7)); }
	inline Vector4_t1900979187  get_tangent_7() const { return ___tangent_7; }
	inline Vector4_t1900979187 * get_address_of_tangent_7() { return &___tangent_7; }
	inline void set_tangent_7(Vector4_t1900979187  value)
	{
		___tangent_7 = value;
	}
};

struct UIVertex_t2944109340_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t662424039  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t1900979187  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t2944109340  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t662424039  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t662424039 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t662424039  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t1900979187  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t1900979187 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t1900979187  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t2944109340_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t2944109340  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t2944109340 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t2944109340  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T2944109340_H
#ifndef DIRECTION_T1205246216_H
#define DIRECTION_T1205246216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Direction
struct  Direction_t1205246216 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1205246216, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1205246216_H
#ifndef STREAMINGCONTEXT_T1986025897_H
#define STREAMINGCONTEXT_T1986025897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t1986025897 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t1986025897, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t1986025897, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1986025897_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1986025897_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T1986025897_H
#ifndef OBJECTDISPOSEDEXCEPTION_T2063091398_H
#define OBJECTDISPOSEDEXCEPTION_T2063091398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t2063091398  : public InvalidOperationException_t242246640
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2063091398, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2063091398, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T2063091398_H
#ifndef ARHITTESTRESULT_T942592945_H
#define ARHITTESTRESULT_T942592945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResult
struct  ARHitTestResult_t942592945 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.ARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.ARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::localTransform
	Matrix4x4_t787966842  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::worldTransform
	Matrix4x4_t787966842  ___worldTransform_3;
	// System.String UnityEngine.XR.iOS.ARHitTestResult::anchorIdentifier
	String_t* ___anchorIdentifier_4;
	// System.Boolean UnityEngine.XR.iOS.ARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ARHitTestResult_t942592945, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(ARHitTestResult_t942592945, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ARHitTestResult_t942592945, ___localTransform_2)); }
	inline Matrix4x4_t787966842  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t787966842 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t787966842  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(ARHitTestResult_t942592945, ___worldTransform_3)); }
	inline Matrix4x4_t787966842  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t787966842 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t787966842  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchorIdentifier_4() { return static_cast<int32_t>(offsetof(ARHitTestResult_t942592945, ___anchorIdentifier_4)); }
	inline String_t* get_anchorIdentifier_4() const { return ___anchorIdentifier_4; }
	inline String_t** get_address_of_anchorIdentifier_4() { return &___anchorIdentifier_4; }
	inline void set_anchorIdentifier_4(String_t* value)
	{
		___anchorIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorIdentifier_4), value);
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(ARHitTestResult_t942592945, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t942592945_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t787966842  ___localTransform_2;
	Matrix4x4_t787966842  ___worldTransform_3;
	char* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t942592945_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t787966842  ___localTransform_2;
	Matrix4x4_t787966842  ___worldTransform_3;
	Il2CppChar* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
#endif // ARHITTESTRESULT_T942592945_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t1986816146  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t1986816146  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t1986816146 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t1986816146  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t98989581* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2882044324 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2882044324 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2882044324 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t98989581* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t98989581** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t98989581* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2882044324 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2882044324 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2882044324 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2882044324 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2882044324 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2882044324 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ARGUMENTNULLEXCEPTION_T246139103_H
#define ARGUMENTNULLEXCEPTION_T246139103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t246139103  : public ArgumentException_t489606696
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T246139103_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T2239795282_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T2239795282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t2239795282  : public ArgumentException_t489606696
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t2239795282, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T2239795282_H
#ifndef DATETIME_T218649865_H
#define DATETIME_T218649865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t218649865 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t595369841  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t218649865, ___ticks_0)); }
	inline TimeSpan_t595369841  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t595369841 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t595369841  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t218649865, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t218649865_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t218649865  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t218649865  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1589106382* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1589106382* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1589106382* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1589106382* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1589106382* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1589106382* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1589106382* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3215173111* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3215173111* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___MaxValue_2)); }
	inline DateTime_t218649865  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t218649865 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t218649865  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___MinValue_3)); }
	inline DateTime_t218649865  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t218649865 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t218649865  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1589106382* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1589106382* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1589106382* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1589106382* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1589106382* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1589106382* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1589106382* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1589106382* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1589106382* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1589106382** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1589106382* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1589106382* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1589106382** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1589106382* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1589106382* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1589106382** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1589106382* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t3215173111* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t3215173111** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t3215173111* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t3215173111* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t3215173111** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t3215173111* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t218649865_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T218649865_H
#ifndef NAVIGATION_T2227424972_H
#define NAVIGATION_T2227424972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t2227424972 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1875500212 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1875500212 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1875500212 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1875500212 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t2227424972, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t2227424972, ___m_SelectOnUp_1)); }
	inline Selectable_t1875500212 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1875500212 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1875500212 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t2227424972, ___m_SelectOnDown_2)); }
	inline Selectable_t1875500212 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1875500212 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1875500212 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t2227424972, ___m_SelectOnLeft_3)); }
	inline Selectable_t1875500212 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1875500212 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1875500212 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t2227424972, ___m_SelectOnRight_4)); }
	inline Selectable_t1875500212 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1875500212 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1875500212 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t2227424972_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1875500212 * ___m_SelectOnUp_1;
	Selectable_t1875500212 * ___m_SelectOnDown_2;
	Selectable_t1875500212 * ___m_SelectOnLeft_3;
	Selectable_t1875500212 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t2227424972_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1875500212 * ___m_SelectOnUp_1;
	Selectable_t1875500212 * ___m_SelectOnDown_2;
	Selectable_t1875500212 * ___m_SelectOnLeft_3;
	Selectable_t1875500212 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T2227424972_H
#ifndef DATETIMEOFFSET_T206488804_H
#define DATETIMEOFFSET_T206488804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t206488804 
{
public:
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t218649865  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t595369841  ___utc_offset_3;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t206488804, ___dt_2)); }
	inline DateTime_t218649865  get_dt_2() const { return ___dt_2; }
	inline DateTime_t218649865 * get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(DateTime_t218649865  value)
	{
		___dt_2 = value;
	}

	inline static int32_t get_offset_of_utc_offset_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t206488804, ___utc_offset_3)); }
	inline TimeSpan_t595369841  get_utc_offset_3() const { return ___utc_offset_3; }
	inline TimeSpan_t595369841 * get_address_of_utc_offset_3() { return &___utc_offset_3; }
	inline void set_utc_offset_3(TimeSpan_t595369841  value)
	{
		___utc_offset_3 = value;
	}
};

struct DateTimeOffset_t206488804_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t206488804  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t206488804  ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t206488804_StaticFields, ___MaxValue_0)); }
	inline DateTimeOffset_t206488804  get_MaxValue_0() const { return ___MaxValue_0; }
	inline DateTimeOffset_t206488804 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(DateTimeOffset_t206488804  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t206488804_StaticFields, ___MinValue_1)); }
	inline DateTimeOffset_t206488804  get_MinValue_1() const { return ___MinValue_1; }
	inline DateTimeOffset_t206488804 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(DateTimeOffset_t206488804  value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T206488804_H
// System.Type[]
struct TypeU5BU5D_t98989581  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3385344125  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t3215173111  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t1226396739  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Link_t890348390  m_Items[1];

public:
	inline Link_t890348390  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t890348390 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t890348390  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Link_t890348390  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t890348390 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t890348390  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m635741614_gshared (Enumerator_t614413977 * __this, HashSet_1_t2249360193 * ___hashset0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C"  void Enumerator_CheckState_m304388724_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1426369967_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1221158354_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3305687280_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m471441320_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3614126786_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2939335827 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m181058154 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t1986816146  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  RuntimeObject * Activator_CreateInstance_m88096193 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::CompareTo(System.DateTime)
extern "C"  int32_t DateTime_CompareTo_m1460365070 (DateTime_t218649865 * __this, DateTime_t218649865  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::CompareTo(System.DateTimeOffset)
extern "C"  int32_t DateTimeOffset_CompareTo_m2102661453 (DateTimeOffset_t206488804 * __this, DateTimeOffset_t206488804  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Guid::CompareTo(System.Guid)
extern "C"  int32_t Guid_CompareTo_m3259062483 (Guid_t * __this, Guid_t  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::CompareTo(System.Int32)
extern "C"  int32_t Int32_CompareTo_m2487396248 (int32_t* __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern "C"  int32_t TimeSpan_CompareTo_m1948065011 (TimeSpan_t595369841 * __this, TimeSpan_t595369841  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Boolean::GetHashCode()
extern "C"  int32_t Boolean_GetHashCode_m1573992247 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::Equals(System.Boolean)
extern "C"  bool Boolean_Equals_m2184399368 (bool* __this, bool ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Char::GetHashCode()
extern "C"  int32_t Char_GetHashCode_m3083351086 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Char::Equals(System.Char)
extern "C"  bool Char_Equals_m694283921 (Il2CppChar* __this, Il2CppChar ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m725705129 (DateTime_t218649865 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::Equals(System.DateTime)
extern "C"  bool DateTime_Equals_m732866806 (DateTime_t218649865 * __this, DateTime_t218649865  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::GetHashCode()
extern "C"  int32_t DateTimeOffset_GetHashCode_m2644009165 (DateTimeOffset_t206488804 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.DateTimeOffset)
extern "C"  bool DateTimeOffset_Equals_m2301431226 (DateTimeOffset_t206488804 * __this, DateTimeOffset_t206488804  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Guid::GetHashCode()
extern "C"  int32_t Guid_GetHashCode_m3098029872 (Guid_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::Equals(System.Guid)
extern "C"  bool Guid_Equals_m51672329 (Guid_t * __this, Guid_t  ___g0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m1004790790 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Int32)
extern "C"  bool Int32_Equals_m1841422559 (int32_t* __this, int32_t ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3630296696 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m847285714 (float* __this, float ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m2388322581 (TimeSpan_t595369841 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern "C"  bool TimeSpan_Equals_m1189538957 (TimeSpan_t595369841 * __this, TimeSpan_t595369841  ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.ColorBlock::GetHashCode()
extern "C"  int32_t ColorBlock_GetHashCode_m282135087 (ColorBlock_t3446061115 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.ColorBlock::Equals(UnityEngine.UI.ColorBlock)
extern "C"  bool ColorBlock_Equals_m3290335982 (ColorBlock_t3446061115 * __this, ColorBlock_t3446061115  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Navigation::Equals(UnityEngine.UI.Navigation)
extern "C"  bool Navigation_Equals_m2418799643 (Navigation_t2227424972 * __this, Navigation_t2227424972  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.SpriteState::Equals(UnityEngine.UI.SpriteState)
extern "C"  bool SpriteState_Equals_m2438444384 (SpriteState_t710398810 * __this, SpriteState_t710398810  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m635741614(__this, ___hashset0, method) ((  void (*) (Enumerator_t614413977 *, HashSet_1_t2249360193 *, const RuntimeMethod*))Enumerator__ctor_m635741614_gshared)(__this, ___hashset0, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
#define Enumerator_CheckState_m304388724(__this, method) ((  void (*) (Enumerator_t614413977 *, const RuntimeMethod*))Enumerator_CheckState_m304388724_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m4186206386 (InvalidOperationException_t242246640 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1426369967(__this, method) ((  RuntimeObject * (*) (Enumerator_t614413977 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1426369967_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1221158354(__this, method) ((  void (*) (Enumerator_t614413977 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m1221158354_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m3305687280(__this, method) ((  bool (*) (Enumerator_t614413977 *, const RuntimeMethod*))Enumerator_MoveNext_m3305687280_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m471441320(__this, method) ((  RuntimeObject * (*) (Enumerator_t614413977 *, const RuntimeMethod*))Enumerator_get_Current_m471441320_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m3614126786(__this, method) ((  void (*) (Enumerator_t614413977 *, const RuntimeMethod*))Enumerator_Dispose_m3614126786_gshared)(__this, method)
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m1168677330 (ObjectDisposedException_t2063091398 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m2492751932 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1393214719  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m1395591393 (ArgumentOutOfRangeException_t2239795282 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m2488792999 (ArgumentNullException_t246139103 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m429090071 (ArgumentException_t489606696 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m2973674085 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m1924552723 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m2256677597 (NotImplementedException_t3535071701 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4177971849_gshared (EqualityComparer_1_t3561003404 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2815452023_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2815452023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3561003404_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3561003404 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1766128512 * L_8 = (DefaultComparer_t1766128512 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1766128512 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3561003404_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3822138156_gshared (EqualityComparer_1_t3561003404 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3561003404 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, float >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::GetHashCode(T) */, (EqualityComparer_1_t3561003404 *)__this, (float)((*(float*)((float*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1629213490_gshared (EqualityComparer_1_t3561003404 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3561003404 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(T,T) */, (EqualityComparer_1_t3561003404 *)__this, (float)((*(float*)((float*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (float)((*(float*)((float*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::get_Default()
extern "C"  EqualityComparer_1_t3561003404 * EqualityComparer_1_get_Default_m2138861894_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3561003404 * L_0 = ((EqualityComparer_1_t3561003404_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2752844815_gshared (EqualityComparer_1_t3258574742 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m877790698_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m877790698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3258574742_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3258574742 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1463699850 * L_8 = (DefaultComparer_t1463699850 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1463699850 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3258574742_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2890300547_gshared (EqualityComparer_1_t3258574742 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3258574742 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, TimeSpan_t595369841  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::GetHashCode(T) */, (EqualityComparer_1_t3258574742 *)__this, (TimeSpan_t595369841 )((*(TimeSpan_t595369841 *)((TimeSpan_t595369841 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m693803156_gshared (EqualityComparer_1_t3258574742 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3258574742 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, TimeSpan_t595369841 , TimeSpan_t595369841  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T) */, (EqualityComparer_1_t3258574742 *)__this, (TimeSpan_t595369841 )((*(TimeSpan_t595369841 *)((TimeSpan_t595369841 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (TimeSpan_t595369841 )((*(TimeSpan_t595369841 *)((TimeSpan_t595369841 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::get_Default()
extern "C"  EqualityComparer_1_t3258574742 * EqualityComparer_1_get_Default_m1690161082_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3258574742 * L_0 = ((EqualityComparer_1_t3258574742_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1687135463_gshared (EqualityComparer_1_t3325628940 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3442706745_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3442706745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3325628940_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3325628940 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1530754048 * L_8 = (DefaultComparer_t1530754048 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1530754048 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3325628940_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1154280259_gshared (EqualityComparer_1_t3325628940 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3325628940 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Color32_t662424039  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::GetHashCode(T) */, (EqualityComparer_1_t3325628940 *)__this, (Color32_t662424039 )((*(Color32_t662424039 *)((Color32_t662424039 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2596833358_gshared (EqualityComparer_1_t3325628940 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3325628940 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Color32_t662424039 , Color32_t662424039  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::Equals(T,T) */, (EqualityComparer_1_t3325628940 *)__this, (Color32_t662424039 )((*(Color32_t662424039 *)((Color32_t662424039 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Color32_t662424039 )((*(Color32_t662424039 *)((Color32_t662424039 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Color32>::get_Default()
extern "C"  EqualityComparer_1_t3325628940 * EqualityComparer_1_get_Default_m3009071386_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3325628940 * L_0 = ((EqualityComparer_1_t3325628940_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3700865554_gshared (EqualityComparer_1_t326780482 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m450573040_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m450573040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t326780482_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t326780482 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2826872886 * L_8 = (DefaultComparer_t2826872886 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2826872886 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t326780482_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m120291910_gshared (EqualityComparer_1_t326780482 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t326780482 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, RaycastResult_t1958542877  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T) */, (EqualityComparer_1_t326780482 *)__this, (RaycastResult_t1958542877 )((*(RaycastResult_t1958542877 *)((RaycastResult_t1958542877 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4215558528_gshared (EqualityComparer_1_t326780482 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t326780482 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, RaycastResult_t1958542877 , RaycastResult_t1958542877  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::Equals(T,T) */, (EqualityComparer_1_t326780482 *)__this, (RaycastResult_t1958542877 )((*(RaycastResult_t1958542877 *)((RaycastResult_t1958542877 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (RaycastResult_t1958542877 )((*(RaycastResult_t1958542877 *)((RaycastResult_t1958542877 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>::get_Default()
extern "C"  EqualityComparer_1_t326780482 * EqualityComparer_1_get_Default_m205042924_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t326780482 * L_0 = ((EqualityComparer_1_t326780482_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m28044019_gshared (EqualityComparer_1_t3766329106 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1466159472_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1466159472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3766329106_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3766329106 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1971454214 * L_8 = (DefaultComparer_t1971454214 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1971454214 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3766329106_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2540390824_gshared (EqualityComparer_1_t3766329106 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3766329106 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::GetHashCode(T) */, (EqualityComparer_1_t3766329106 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2841660374_gshared (EqualityComparer_1_t3766329106 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3766329106 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::Equals(T,T) */, (EqualityComparer_1_t3766329106 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.TouchScreenKeyboardType>::get_Default()
extern "C"  EqualityComparer_1_t3766329106 * EqualityComparer_1_get_Default_m3807013817_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3766329106 * L_0 = ((EqualityComparer_1_t3766329106_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3841979675_gshared (EqualityComparer_1_t1843515292 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2644827730_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2644827730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1843515292_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1843515292 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t48640400 * L_8 = (DefaultComparer_t48640400 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t48640400 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1843515292_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2534210799_gshared (EqualityComparer_1_t1843515292 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1843515292 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::GetHashCode(T) */, (EqualityComparer_1_t1843515292 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1420809860_gshared (EqualityComparer_1_t1843515292 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1843515292 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::Equals(T,T) */, (EqualityComparer_1_t1843515292 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.AspectRatioFitter/AspectMode>::get_Default()
extern "C"  EqualityComparer_1_t1843515292 * EqualityComparer_1_get_Default_m3497487027_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1843515292 * L_0 = ((EqualityComparer_1_t1843515292_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3239358070_gshared (EqualityComparer_1_t1814298720 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4211016089_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4211016089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1814298720_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1814298720 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t19423828 * L_8 = (DefaultComparer_t19423828 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t19423828 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1814298720_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1851065423_gshared (EqualityComparer_1_t1814298720 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1814298720 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ColorBlock_t3446061115  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::GetHashCode(T) */, (EqualityComparer_1_t1814298720 *)__this, (ColorBlock_t3446061115 )((*(ColorBlock_t3446061115 *)((ColorBlock_t3446061115 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3619361735_gshared (EqualityComparer_1_t1814298720 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1814298720 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ColorBlock_t3446061115 , ColorBlock_t3446061115  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::Equals(T,T) */, (EqualityComparer_1_t1814298720 *)__this, (ColorBlock_t3446061115 )((*(ColorBlock_t3446061115 *)((ColorBlock_t3446061115 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ColorBlock_t3446061115 )((*(ColorBlock_t3446061115 *)((ColorBlock_t3446061115 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ColorBlock>::get_Default()
extern "C"  EqualityComparer_1_t1814298720 * EqualityComparer_1_get_Default_m2116416259_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1814298720 * L_0 = ((EqualityComparer_1_t1814298720_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2342244545_gshared (EqualityComparer_1_t1951216805 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3678960581_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3678960581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1951216805_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1951216805 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t156341913 * L_8 = (DefaultComparer_t156341913 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t156341913 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1951216805_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2147946165_gshared (EqualityComparer_1_t1951216805 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1951216805 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::GetHashCode(T) */, (EqualityComparer_1_t1951216805 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3246081045_gshared (EqualityComparer_1_t1951216805 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1951216805 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::Equals(T,T) */, (EqualityComparer_1_t1951216805 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.ContentSizeFitter/FitMode>::get_Default()
extern "C"  EqualityComparer_1_t1951216805 * EqualityComparer_1_get_Default_m1339673453_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1951216805 * L_0 = ((EqualityComparer_1_t1951216805_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3391938094_gshared (EqualityComparer_1_t1299855184 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m994766661_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m994766661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1299855184_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1299855184 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3799947588 * L_8 = (DefaultComparer_t3799947588 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3799947588 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1299855184_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3640392606_gshared (EqualityComparer_1_t1299855184 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1299855184 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::GetHashCode(T) */, (EqualityComparer_1_t1299855184 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m502979008_gshared (EqualityComparer_1_t1299855184 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1299855184 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::Equals(T,T) */, (EqualityComparer_1_t1299855184 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/FillMethod>::get_Default()
extern "C"  EqualityComparer_1_t1299855184 * EqualityComparer_1_get_Default_m207832683_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1299855184 * L_0 = ((EqualityComparer_1_t1299855184_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m4216555824_gshared (EqualityComparer_1_t1206011620 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3520013917_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3520013917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1206011620_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1206011620 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3706104024 * L_8 = (DefaultComparer_t3706104024 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3706104024 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1206011620_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3537003860_gshared (EqualityComparer_1_t1206011620 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1206011620 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::GetHashCode(T) */, (EqualityComparer_1_t1206011620 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m194766487_gshared (EqualityComparer_1_t1206011620 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1206011620 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::Equals(T,T) */, (EqualityComparer_1_t1206011620 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Image/Type>::get_Default()
extern "C"  EqualityComparer_1_t1206011620 * EqualityComparer_1_get_Default_m2183188077_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1206011620 * L_0 = ((EqualityComparer_1_t1206011620_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3425606009_gshared (EqualityComparer_1_t1137787835 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m141604497_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m141604497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1137787835_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1137787835 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3637880239 * L_8 = (DefaultComparer_t3637880239 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3637880239 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1137787835_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2799547090_gshared (EqualityComparer_1_t1137787835 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1137787835 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::GetHashCode(T) */, (EqualityComparer_1_t1137787835 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m442549318_gshared (EqualityComparer_1_t1137787835 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1137787835 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::Equals(T,T) */, (EqualityComparer_1_t1137787835 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/CharacterValidation>::get_Default()
extern "C"  EqualityComparer_1_t1137787835 * EqualityComparer_1_get_Default_m3978159364_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1137787835 * L_0 = ((EqualityComparer_1_t1137787835_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1062556918_gshared (EqualityComparer_1_t3785776845 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2066784956_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2066784956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3785776845_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3785776845 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1990901953 * L_8 = (DefaultComparer_t1990901953 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1990901953 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3785776845_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3180830715_gshared (EqualityComparer_1_t3785776845 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3785776845 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::GetHashCode(T) */, (EqualityComparer_1_t3785776845 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3041946076_gshared (EqualityComparer_1_t3785776845 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3785776845 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::Equals(T,T) */, (EqualityComparer_1_t3785776845 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/ContentType>::get_Default()
extern "C"  EqualityComparer_1_t3785776845 * EqualityComparer_1_get_Default_m1218489883_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3785776845 * L_0 = ((EqualityComparer_1_t3785776845_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3396532638_gshared (EqualityComparer_1_t2555363908 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3733666149_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3733666149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2555363908_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2555363908 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t760489016 * L_8 = (DefaultComparer_t760489016 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t760489016 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2555363908_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1146017682_gshared (EqualityComparer_1_t2555363908 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2555363908 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::GetHashCode(T) */, (EqualityComparer_1_t2555363908 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2302085514_gshared (EqualityComparer_1_t2555363908 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2555363908 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::Equals(T,T) */, (EqualityComparer_1_t2555363908 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/InputType>::get_Default()
extern "C"  EqualityComparer_1_t2555363908 * EqualityComparer_1_get_Default_m3267667807_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2555363908 * L_0 = ((EqualityComparer_1_t2555363908_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3949586034_gshared (EqualityComparer_1_t2006847981 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2695337509_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2695337509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2006847981_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2006847981 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t211973089 * L_8 = (DefaultComparer_t211973089 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t211973089 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2006847981_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m592094316_gshared (EqualityComparer_1_t2006847981 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2006847981 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::GetHashCode(T) */, (EqualityComparer_1_t2006847981 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4023039762_gshared (EqualityComparer_1_t2006847981 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2006847981 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::Equals(T,T) */, (EqualityComparer_1_t2006847981 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.InputField/LineType>::get_Default()
extern "C"  EqualityComparer_1_t2006847981 * EqualityComparer_1_get_Default_m1972541415_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2006847981 * L_0 = ((EqualityComparer_1_t2006847981_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3638149880_gshared (EqualityComparer_1_t595662577 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3188556159_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3188556159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t595662577_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t595662577 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3095754981 * L_8 = (DefaultComparer_t3095754981 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3095754981 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t595662577_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2849917415_gshared (EqualityComparer_1_t595662577 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t595662577 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Navigation_t2227424972  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::GetHashCode(T) */, (EqualityComparer_1_t595662577 *)__this, (Navigation_t2227424972 )((*(Navigation_t2227424972 *)((Navigation_t2227424972 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1082978029_gshared (EqualityComparer_1_t595662577 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t595662577 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Navigation_t2227424972 , Navigation_t2227424972  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::Equals(T,T) */, (EqualityComparer_1_t595662577 *)__this, (Navigation_t2227424972 )((*(Navigation_t2227424972 *)((Navigation_t2227424972 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Navigation_t2227424972 )((*(Navigation_t2227424972 *)((Navigation_t2227424972 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Navigation>::get_Default()
extern "C"  EqualityComparer_1_t595662577 * EqualityComparer_1_get_Default_m2867256299_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t595662577 * L_0 = ((EqualityComparer_1_t595662577_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2996848918_gshared (EqualityComparer_1_t3868451117 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2826035360_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2826035360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3868451117_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3868451117 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2073576225 * L_8 = (DefaultComparer_t2073576225 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2073576225 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3868451117_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1501093064_gshared (EqualityComparer_1_t3868451117 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3868451117 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::GetHashCode(T) */, (EqualityComparer_1_t3868451117 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2541947410_gshared (EqualityComparer_1_t3868451117 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3868451117 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::Equals(T,T) */, (EqualityComparer_1_t3868451117 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Scrollbar/Direction>::get_Default()
extern "C"  EqualityComparer_1_t3868451117 * EqualityComparer_1_get_Default_m2064837236_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3868451117 * L_0 = ((EqualityComparer_1_t3868451117_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3607462864_gshared (EqualityComparer_1_t1071577238 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2850716509_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2850716509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1071577238_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1071577238 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3571669642 * L_8 = (DefaultComparer_t3571669642 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3571669642 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1071577238_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m4043582220_gshared (EqualityComparer_1_t1071577238 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1071577238 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::GetHashCode(T) */, (EqualityComparer_1_t1071577238 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3631735211_gshared (EqualityComparer_1_t1071577238 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1071577238 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::Equals(T,T) */, (EqualityComparer_1_t1071577238 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable/Transition>::get_Default()
extern "C"  EqualityComparer_1_t1071577238 * EqualityComparer_1_get_Default_m3889857352_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1071577238 * L_0 = ((EqualityComparer_1_t1071577238_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3621694856_gshared (EqualityComparer_1_t2963046443 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2557533271_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2557533271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2963046443_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2963046443 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1168171551 * L_8 = (DefaultComparer_t1168171551 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1168171551 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2963046443_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3641017674_gshared (EqualityComparer_1_t2963046443 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2963046443 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::GetHashCode(T) */, (EqualityComparer_1_t2963046443 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m728646309_gshared (EqualityComparer_1_t2963046443 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2963046443 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::Equals(T,T) */, (EqualityComparer_1_t2963046443 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Slider/Direction>::get_Default()
extern "C"  EqualityComparer_1_t2963046443 * EqualityComparer_1_get_Default_m979971845_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2963046443 * L_0 = ((EqualityComparer_1_t2963046443_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2966907520_gshared (EqualityComparer_1_t3373603711 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3228446035_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3228446035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3373603711_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3373603711 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1578728819 * L_8 = (DefaultComparer_t1578728819 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1578728819 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3373603711_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3391165782_gshared (EqualityComparer_1_t3373603711 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3373603711 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, SpriteState_t710398810  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::GetHashCode(T) */, (EqualityComparer_1_t3373603711 *)__this, (SpriteState_t710398810 )((*(SpriteState_t710398810 *)((SpriteState_t710398810 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1829488848_gshared (EqualityComparer_1_t3373603711 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3373603711 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, SpriteState_t710398810 , SpriteState_t710398810  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::Equals(T,T) */, (EqualityComparer_1_t3373603711 *)__this, (SpriteState_t710398810 )((*(SpriteState_t710398810 *)((SpriteState_t710398810 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (SpriteState_t710398810 )((*(SpriteState_t710398810 *)((SpriteState_t710398810 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.SpriteState>::get_Default()
extern "C"  EqualityComparer_1_t3373603711 * EqualityComparer_1_get_Default_m1941618112_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3373603711 * L_0 = ((EqualityComparer_1_t3373603711_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2344124180_gshared (EqualityComparer_1_t3503033932 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m870844869_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m870844869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3503033932_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3503033932 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1708159040 * L_8 = (DefaultComparer_t1708159040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1708159040 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3503033932_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m938146598_gshared (EqualityComparer_1_t3503033932 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3503033932 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UICharInfo_t839829031  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::GetHashCode(T) */, (EqualityComparer_1_t3503033932 *)__this, (UICharInfo_t839829031 )((*(UICharInfo_t839829031 *)((UICharInfo_t839829031 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m4027196497_gshared (EqualityComparer_1_t3503033932 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3503033932 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UICharInfo_t839829031 , UICharInfo_t839829031  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::Equals(T,T) */, (EqualityComparer_1_t3503033932 *)__this, (UICharInfo_t839829031 )((*(UICharInfo_t839829031 *)((UICharInfo_t839829031 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UICharInfo_t839829031 )((*(UICharInfo_t839829031 *)((UICharInfo_t839829031 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C"  EqualityComparer_1_t3503033932 * EqualityComparer_1_get_Default_m2868108184_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3503033932 * L_0 = ((EqualityComparer_1_t3503033932_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m547887964_gshared (EqualityComparer_1_t524630218 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4003914194_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4003914194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t524630218_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t524630218 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3024722622 * L_8 = (DefaultComparer_t3024722622 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3024722622 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t524630218_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m407401718_gshared (EqualityComparer_1_t524630218 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t524630218 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UILineInfo_t2156392613  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::GetHashCode(T) */, (EqualityComparer_1_t524630218 *)__this, (UILineInfo_t2156392613 )((*(UILineInfo_t2156392613 *)((UILineInfo_t2156392613 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2439449452_gshared (EqualityComparer_1_t524630218 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t524630218 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UILineInfo_t2156392613 , UILineInfo_t2156392613  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::Equals(T,T) */, (EqualityComparer_1_t524630218 *)__this, (UILineInfo_t2156392613 )((*(UILineInfo_t2156392613 *)((UILineInfo_t2156392613 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UILineInfo_t2156392613 )((*(UILineInfo_t2156392613 *)((UILineInfo_t2156392613 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C"  EqualityComparer_1_t524630218 * EqualityComparer_1_get_Default_m2697180242_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t524630218 * L_0 = ((EqualityComparer_1_t524630218_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m660710987_gshared (EqualityComparer_1_t1312346945 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m976010431_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m976010431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1312346945_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t1312346945 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3812439349 * L_8 = (DefaultComparer_t3812439349 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3812439349 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1312346945_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m162317720_gshared (EqualityComparer_1_t1312346945 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t1312346945 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, UIVertex_t2944109340  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::GetHashCode(T) */, (EqualityComparer_1_t1312346945 *)__this, (UIVertex_t2944109340 )((*(UIVertex_t2944109340 *)((UIVertex_t2944109340 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2419583036_gshared (EqualityComparer_1_t1312346945 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t1312346945 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, UIVertex_t2944109340 , UIVertex_t2944109340  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::Equals(T,T) */, (EqualityComparer_1_t1312346945 *)__this, (UIVertex_t2944109340 )((*(UIVertex_t2944109340 *)((UIVertex_t2944109340 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UIVertex_t2944109340 )((*(UIVertex_t2944109340 *)((UIVertex_t2944109340 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>::get_Default()
extern "C"  EqualityComparer_1_t1312346945 * EqualityComparer_1_get_Default_m668526710_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1312346945 * L_0 = ((EqualityComparer_1_t1312346945_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2475590959_gshared (EqualityComparer_1_t614606883 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3877116888_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m3877116888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t614606883_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t614606883 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3114699287 * L_8 = (DefaultComparer_t3114699287 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3114699287 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t614606883_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1383243491_gshared (EqualityComparer_1_t614606883 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t614606883 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Vector2_t2246369278  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::GetHashCode(T) */, (EqualityComparer_1_t614606883 *)__this, (Vector2_t2246369278 )((*(Vector2_t2246369278 *)((Vector2_t2246369278 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2614838027_gshared (EqualityComparer_1_t614606883 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t614606883 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Vector2_t2246369278 , Vector2_t2246369278  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::Equals(T,T) */, (EqualityComparer_1_t614606883 *)__this, (Vector2_t2246369278 )((*(Vector2_t2246369278 *)((Vector2_t2246369278 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector2_t2246369278 )((*(Vector2_t2246369278 *)((Vector2_t2246369278 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::get_Default()
extern "C"  EqualityComparer_1_t614606883 * EqualityComparer_1_get_Default_m3960622587_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t614606883 * L_0 = ((EqualityComparer_1_t614606883_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3521670979_gshared (EqualityComparer_1_t2300630690 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m4042575380_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m4042575380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2300630690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t2300630690 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t505755798 * L_8 = (DefaultComparer_t505755798 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t505755798 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2300630690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m946057017_gshared (EqualityComparer_1_t2300630690 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t2300630690 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Vector3_t3932393085  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::GetHashCode(T) */, (EqualityComparer_1_t2300630690 *)__this, (Vector3_t3932393085 )((*(Vector3_t3932393085 *)((Vector3_t3932393085 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3608601466_gshared (EqualityComparer_1_t2300630690 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t2300630690 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Vector3_t3932393085 , Vector3_t3932393085  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::Equals(T,T) */, (EqualityComparer_1_t2300630690 *)__this, (Vector3_t3932393085 )((*(Vector3_t3932393085 *)((Vector3_t3932393085 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector3_t3932393085 )((*(Vector3_t3932393085 *)((Vector3_t3932393085 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::get_Default()
extern "C"  EqualityComparer_1_t2300630690 * EqualityComparer_1_get_Default_m3455920125_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2300630690 * L_0 = ((EqualityComparer_1_t2300630690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2866290061_gshared (EqualityComparer_1_t269216792 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2940491964_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m2940491964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t269216792_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t269216792 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2769309196 * L_8 = (DefaultComparer_t2769309196 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2769309196 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t269216792_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2284151127_gshared (EqualityComparer_1_t269216792 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t269216792 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Vector4_t1900979187  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::GetHashCode(T) */, (EqualityComparer_1_t269216792 *)__this, (Vector4_t1900979187 )((*(Vector4_t1900979187 *)((Vector4_t1900979187 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3898993263_gshared (EqualityComparer_1_t269216792 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t269216792 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, Vector4_t1900979187 , Vector4_t1900979187  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::Equals(T,T) */, (EqualityComparer_1_t269216792 *)__this, (Vector4_t1900979187 )((*(Vector4_t1900979187 *)((Vector4_t1900979187 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector4_t1900979187 )((*(Vector4_t1900979187 *)((Vector4_t1900979187 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::get_Default()
extern "C"  EqualityComparer_1_t269216792 * EqualityComparer_1_get_Default_m714721793_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t269216792 * L_0 = ((EqualityComparer_1_t269216792_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1357733538_gshared (EqualityComparer_1_t3605797846 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1621471002_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EqualityComparer_1__cctor_m1621471002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(GenericEqualityComparer_1_t3218101675_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t98989581* L_4 = (TypeU5BU5D_t98989581*)((TypeU5BU5D_t98989581*)SZArrayNew(TypeU5BU5D_t98989581_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m181058154(NULL /*static, unused*/, (RuntimeTypeHandle_t1986816146 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t98989581* >::Invoke(81 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t98989581*)L_4);
		RuntimeObject * L_7 = Activator_CreateInstance_m88096193(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3605797846_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(((EqualityComparer_1_t3605797846 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1810922954 * L_8 = (DefaultComparer_t1810922954 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t1810922954 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3605797846_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m3140714074_gshared (EqualityComparer_1_t3605797846 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck((EqualityComparer_1_t3605797846 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, ARHitTestResult_t942592945  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::GetHashCode(T) */, (EqualityComparer_1_t3605797846 *)__this, (ARHitTestResult_t942592945 )((*(ARHitTestResult_t942592945 *)((ARHitTestResult_t942592945 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20670377_gshared (EqualityComparer_1_t3605797846 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		RuntimeObject * L_1 = ___y1;
		NullCheck((EqualityComparer_1_t3605797846 *)__this);
		bool L_2 = VirtFuncInvoker2< bool, ARHitTestResult_t942592945 , ARHitTestResult_t942592945  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::Equals(T,T) */, (EqualityComparer_1_t3605797846 *)__this, (ARHitTestResult_t942592945 )((*(ARHitTestResult_t942592945 *)((ARHitTestResult_t942592945 *)UnBox(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (ARHitTestResult_t942592945 )((*(ARHitTestResult_t942592945 *)((ARHitTestResult_t942592945 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Default()
extern "C"  EqualityComparer_1_t3605797846 * EqualityComparer_1_get_Default_m2958547891_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3605797846 * L_0 = ((EqualityComparer_1_t3605797846_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3158228327_gshared (GenericComparer_1_t503918337 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t432285270 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t432285270 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t432285270 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1684134909_gshared (GenericComparer_1_t503918337 * __this, DateTime_t218649865  ___x0, DateTime_t218649865  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTime_t218649865  L_3 = ___y1;
		int32_t L_4 = DateTime_CompareTo_m1460365070((DateTime_t218649865 *)(&___x0), (DateTime_t218649865 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericComparer_1__ctor_m2830801051_gshared (GenericComparer_1_t491757276 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t420124209 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t420124209 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t420124209 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m3070178112_gshared (GenericComparer_1_t491757276 * __this, DateTimeOffset_t206488804  ___x0, DateTimeOffset_t206488804  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		DateTimeOffset_t206488804  L_3 = ___y1;
		int32_t L_4 = DateTimeOffset_CompareTo_m2102661453((DateTimeOffset_t206488804 *)(&___x0), (DateTimeOffset_t206488804 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1807935417_gshared (GenericComparer_1_t1937162846 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t1865529779 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t1865529779 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t1865529779 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m57312638_gshared (GenericComparer_1_t1937162846 * __this, Guid_t  ___x0, Guid_t  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		Guid_t  L_3 = ___y1;
		int32_t L_4 = Guid_CompareTo_m3259062483((Guid_t *)(&___x0), (Guid_t )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3541762288_gshared (GenericComparer_1_t3800846010 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t3729212943 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t3729212943 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t3729212943 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m4140087201_gshared (GenericComparer_1_t3800846010 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___y1;
		int32_t L_4 = Int32_CompareTo_m2487396248((int32_t*)(&___x0), (int32_t)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.Object>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1407741524_gshared (GenericComparer_1_t837295660 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t765662593 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t765662593 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t765662593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Object>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m462018311_gshared (GenericComparer_1_t837295660 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		RuntimeObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		RuntimeObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		RuntimeObject * L_3 = ___y1;
		NullCheck((RuntimeObject*)(*(&___x0)));
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)(*(&___x0)), (RuntimeObject *)L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C"  void GenericComparer_1__ctor_m3720115279_gshared (GenericComparer_1_t880638313 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Comparer_1_t809005246 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Comparer_1_t809005246 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Comparer_1_t809005246 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m588647651_gshared (GenericComparer_1_t880638313 * __this, TimeSpan_t595369841  ___x0, TimeSpan_t595369841  ___y1, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		TimeSpan_t595369841  L_3 = ___y1;
		int32_t L_4 = TimeSpan_CompareTo_m1948065011((TimeSpan_t595369841 *)(&___x0), (TimeSpan_t595369841 )L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3823724264_gshared (GenericEqualityComparer_1_t1397660498 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t808457599 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t808457599 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t808457599 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1184126350_gshared (GenericEqualityComparer_1_t1397660498 * __this, bool ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Boolean_GetHashCode_m1573992247((bool*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m309803323_gshared (GenericEqualityComparer_1_t1397660498 * __this, bool ___x0, bool ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		bool L_1 = ___y1;
		bool L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		bool L_4 = ___y1;
		bool L_5 = Boolean_Equals_m2184399368((bool*)(&___x0), (bool)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Char>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2597814008_gshared (GenericEqualityComparer_1_t580076992 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t4285841389 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t4285841389 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t4285841389 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Char>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2411975812_gshared (GenericEqualityComparer_1_t580076992 * __this, Il2CppChar ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Char_GetHashCode_m3083351086((Il2CppChar*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Char>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1816542594_gshared (GenericEqualityComparer_1_t580076992 * __this, Il2CppChar ___x0, Il2CppChar ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Il2CppChar L_1 = ___y1;
		Il2CppChar L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Il2CppChar L_4 = ___y1;
		bool L_5 = Char_Equals_m694283921((Il2CppChar*)(&___x0), (Il2CppChar)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1608028795_gshared (GenericEqualityComparer_1_t3471057665 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2881854766 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2881854766 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2881854766 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m126488262_gshared (GenericEqualityComparer_1_t3471057665 * __this, DateTime_t218649865  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTime_GetHashCode_m725705129((DateTime_t218649865 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1776598429_gshared (GenericEqualityComparer_1_t3471057665 * __this, DateTime_t218649865  ___x0, DateTime_t218649865  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		DateTime_t218649865  L_1 = ___y1;
		DateTime_t218649865  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTime_t218649865  L_4 = ___y1;
		bool L_5 = DateTime_Equals_m732866806((DateTime_t218649865 *)(&___x0), (DateTime_t218649865 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m4214335478_gshared (GenericEqualityComparer_1_t3458896604 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t2869693705 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t2869693705 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t2869693705 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3509637740_gshared (GenericEqualityComparer_1_t3458896604 * __this, DateTimeOffset_t206488804  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = DateTimeOffset_GetHashCode_m2644009165((DateTimeOffset_t206488804 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2930175649_gshared (GenericEqualityComparer_1_t3458896604 * __this, DateTimeOffset_t206488804  ___x0, DateTimeOffset_t206488804  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		DateTimeOffset_t206488804  L_1 = ___y1;
		DateTimeOffset_t206488804  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		DateTimeOffset_t206488804  L_4 = ___y1;
		bool L_5 = DateTimeOffset_Equals_m2301431226((DateTimeOffset_t206488804 *)(&___x0), (DateTimeOffset_t206488804 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1014765753_gshared (GenericEqualityComparer_1_t609334878 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t20131979 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t20131979 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t20131979 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1109342461_gshared (GenericEqualityComparer_1_t609334878 * __this, Guid_t  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Guid_GetHashCode_m3098029872((Guid_t *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1264146923_gshared (GenericEqualityComparer_1_t609334878 * __this, Guid_t  ___x0, Guid_t  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Guid_t  L_1 = ___y1;
		Guid_t  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Guid_t  L_4 = ___y1;
		bool L_5 = Guid_Equals_m51672329((Guid_t *)(&___x0), (Guid_t )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m154919147_gshared (GenericEqualityComparer_1_t2473018042 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1883815143 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1883815143 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1883815143 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m4284495750_gshared (GenericEqualityComparer_1_t2473018042 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Int32_GetHashCode_m1004790790((int32_t*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m4070701126_gshared (GenericEqualityComparer_1_t2473018042 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y1;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y1;
		bool L_5 = Int32_Equals_m1841422559((int32_t*)(&___x0), (int32_t)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Object>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2895634128_gshared (GenericEqualityComparer_1_t3804434988 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3215232089 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3215232089 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3215232089 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Object>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2919297102_gshared (GenericEqualityComparer_1_t3804434988 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((RuntimeObject *)(*(&___obj0)));
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)(*(&___obj0)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Object>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1422917199_gshared (GenericEqualityComparer_1_t3804434988 * __this, RuntimeObject * ___x0, RuntimeObject * ___y1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_1 = ___y1;
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		RuntimeObject * L_2 = ___y1;
		NullCheck((RuntimeObject*)(*(&___x0)));
		bool L_3 = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.IEquatable`1<System.Object>::Equals(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (RuntimeObject*)(*(&___x0)), (RuntimeObject *)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Single>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1177098932_gshared (GenericEqualityComparer_1_t4150206303 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3561003404 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3561003404 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3561003404 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3986693942_gshared (GenericEqualityComparer_1_t4150206303 * __this, float ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = Single_GetHashCode_m3630296696((float*)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m3048826617_gshared (GenericEqualityComparer_1_t4150206303 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		float L_1 = ___y1;
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		float L_4 = ___y1;
		bool L_5 = Single_Equals_m847285714((float*)(&___x0), (float)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1661703750_gshared (GenericEqualityComparer_1_t3847777641 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3258574742 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3258574742 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3258574742 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2967890154_gshared (GenericEqualityComparer_1_t3847777641 * __this, TimeSpan_t595369841  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = TimeSpan_GetHashCode_m2388322581((TimeSpan_t595369841 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m4143832688_gshared (GenericEqualityComparer_1_t3847777641 * __this, TimeSpan_t595369841  ___x0, TimeSpan_t595369841  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		TimeSpan_t595369841  L_1 = ___y1;
		TimeSpan_t595369841  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		TimeSpan_t595369841  L_4 = ___y1;
		bool L_5 = TimeSpan_Equals_m1189538957((TimeSpan_t595369841 *)(&___x0), (TimeSpan_t595369841 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.ColorBlock>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1221777579_gshared (GenericEqualityComparer_1_t2403501619 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t1814298720 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t1814298720 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t1814298720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.ColorBlock>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m1062664890_gshared (GenericEqualityComparer_1_t2403501619 * __this, ColorBlock_t3446061115  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_1 = ColorBlock_GetHashCode_m282135087((ColorBlock_t3446061115 *)(&___obj0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.ColorBlock>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m3600024201_gshared (GenericEqualityComparer_1_t2403501619 * __this, ColorBlock_t3446061115  ___x0, ColorBlock_t3446061115  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		ColorBlock_t3446061115  L_1 = ___y1;
		ColorBlock_t3446061115  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ColorBlock_t3446061115  L_4 = ___y1;
		bool L_5 = ColorBlock_Equals_m3290335982((ColorBlock_t3446061115 *)(&___x0), (ColorBlock_t3446061115 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.Navigation>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2467515074_gshared (GenericEqualityComparer_1_t1184865476 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t595662577 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t595662577 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t595662577 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.Navigation>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m169421266_gshared (GenericEqualityComparer_1_t1184865476 * __this, Navigation_t2227424972  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(Navigation_t2227424972 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.Navigation>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m944133998_gshared (GenericEqualityComparer_1_t1184865476 * __this, Navigation_t2227424972  ___x0, Navigation_t2227424972  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		Navigation_t2227424972  L_1 = ___y1;
		Navigation_t2227424972  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		Navigation_t2227424972  L_4 = ___y1;
		bool L_5 = Navigation_Equals_m2418799643((Navigation_t2227424972 *)(&___x0), (Navigation_t2227424972 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.SpriteState>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3431480255_gshared (GenericEqualityComparer_1_t3962806610 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((EqualityComparer_1_t3373603711 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (EqualityComparer_1_t3373603711 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EqualityComparer_1_t3373603711 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.SpriteState>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2574498647_gshared (GenericEqualityComparer_1_t3962806610 * __this, SpriteState_t710398810  ___obj0, const RuntimeMethod* method)
{
	{
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		RuntimeObject * L_1 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (&___obj0));
		NullCheck((RuntimeObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (RuntimeObject *)L_1);
		*(&___obj0) = *(SpriteState_t710398810 *)UnBox(L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UnityEngine.UI.SpriteState>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2461152768_gshared (GenericEqualityComparer_1_t3962806610 * __this, SpriteState_t710398810  ___x0, SpriteState_t710398810  ___y1, const RuntimeMethod* method)
{
	{
		goto IL_0015;
	}
	{
		SpriteState_t710398810  L_1 = ___y1;
		SpriteState_t710398810  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_2);
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0015:
	{
		SpriteState_t710398810  L_4 = ___y1;
		bool L_5 = SpriteState_Equals_m2438444384((SpriteState_t710398810 *)(&___x0), (SpriteState_t710398810 )L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m635741614_gshared (Enumerator_t614413977 * __this, HashSet_1_t2249360193 * ___hashset0, const RuntimeMethod* method)
{
	{
		HashSet_1_t2249360193 * L_0 = ___hashset0;
		__this->set_hashset_0(L_0);
		HashSet_1_t2249360193 * L_1 = ___hashset0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_13();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m635741614_AdjustorThunk (RuntimeObject * __this, HashSet_1_t2249360193 * ___hashset0, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	Enumerator__ctor_m635741614(_thisAdjusted, ___hashset0, method);
}
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1426369967_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1426369967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_CheckState_m304388724((Enumerator_t614413977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t242246640 * L_1 = (InvalidOperationException_t242246640 *)il2cpp_codegen_object_new(InvalidOperationException_t242246640_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4186206386(L_1, (String_t*)_stringLiteral1142242952, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1426369967_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1426369967(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1221158354_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_CheckState_m304388724((Enumerator_t614413977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1221158354_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1221158354(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3305687280_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_CheckState_m304388724((Enumerator_t614413977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_0055;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		HashSet_1_t2249360193 * L_4 = (HashSet_1_t2249360193 *)__this->get_hashset_0();
		int32_t L_5 = V_0;
		NullCheck((HashSet_1_t2249360193 *)L_4);
		int32_t L_6 = ((  int32_t (*) (HashSet_1_t2249360193 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((HashSet_1_t2249360193 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		HashSet_1_t2249360193 * L_7 = (HashSet_1_t2249360193 *)__this->get_hashset_0();
		NullCheck(L_7);
		ObjectU5BU5D_t3385344125* L_8 = (ObjectU5BU5D_t3385344125*)L_7->get_slots_6();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		__this->set_current_3(L_11);
		return (bool)1;
	}

IL_0055:
	{
		int32_t L_12 = (int32_t)__this->get_next_1();
		HashSet_1_t2249360193 * L_13 = (HashSet_1_t2249360193 *)__this->get_hashset_0();
		NullCheck(L_13);
		int32_t L_14 = (int32_t)L_13->get_touched_7();
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3305687280_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	return Enumerator_MoveNext_m3305687280(_thisAdjusted, method);
}
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m471441320_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  RuntimeObject * Enumerator_get_Current_m471441320_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	return Enumerator_get_Current_m471441320(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3614126786_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method)
{
	{
		__this->set_hashset_0((HashSet_1_t2249360193 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3614126786_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	Enumerator_Dispose_m3614126786(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C"  void Enumerator_CheckState_m304388724_gshared (Enumerator_t614413977 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_CheckState_m304388724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t2249360193 * L_0 = (HashSet_1_t2249360193 *)__this->get_hashset_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2063091398 * L_1 = (ObjectDisposedException_t2063091398 *)il2cpp_codegen_object_new(ObjectDisposedException_t2063091398_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1168677330(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		HashSet_1_t2249360193 * L_2 = (HashSet_1_t2249360193 *)__this->get_hashset_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_13();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t242246640 * L_5 = (InvalidOperationException_t242246640 *)il2cpp_codegen_object_new(InvalidOperationException_t242246640_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4186206386(L_5, (String_t*)_stringLiteral534374098, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_CheckState_m304388724_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t614413977 * _thisAdjusted = reinterpret_cast<Enumerator_t614413977 *>(__this + 1);
	Enumerator_CheckState_m304388724(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
extern "C"  void PrimeHelper__cctor_m2306016830_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrimeHelper__cctor_m2306016830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t3215173111* L_0 = (Int32U5BU5D_t3215173111*)((Int32U5BU5D_t3215173111*)SZArrayNew(Int32U5BU5D_t3215173111_il2cpp_TypeInfo_var, (uint32_t)((int32_t)34)));
		RuntimeHelpers_InitializeArray_m2492751932(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (RuntimeFieldHandle_t1393214719 )LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1622590248____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		((PrimeHelper_t1801316144_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_primes_table_0(L_0);
		return;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
extern "C"  bool PrimeHelper_TestPrime_m4047426156_gshared (RuntimeObject * __this /* static, unused */, int32_t ___x0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___x0;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_1 = ___x0;
		double L_2 = sqrt((double)(((double)((double)L_1))));
		V_0 = (int32_t)(((int32_t)((int32_t)L_2)));
		V_1 = (int32_t)3;
		goto IL_0026;
	}

IL_0018:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = V_1;
		if (((int32_t)((int32_t)L_3%(int32_t)L_4)))
		{
			goto IL_0022;
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)2));
	}

IL_0026:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_002f:
	{
		int32_t L_8 = ___x0;
		return (bool)((((int32_t)L_8) == ((int32_t)2))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
extern "C"  int32_t PrimeHelper_CalcPrime_m3036826212_gshared (RuntimeObject * __this /* static, unused */, int32_t ___x0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___x0;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2)))-(int32_t)1));
		goto IL_001d;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_2 = ((  bool (*) (RuntimeObject * /* static, unused */, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)2));
	}

IL_001d:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_6 = ___x0;
		return L_6;
	}
}
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
extern "C"  int32_t PrimeHelper_ToPrime_m3141824047_gshared (RuntimeObject * __this /* static, unused */, int32_t ___x0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0020;
	}

IL_0007:
	{
		int32_t L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t3215173111* L_1 = ((PrimeHelper_t1801316144_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_primes_table_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if ((((int32_t)L_0) > ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t3215173111* L_5 = ((PrimeHelper_t1801316144_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_primes_table_0();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		return L_8;
	}

IL_001c:
	{
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t3215173111* L_11 = ((PrimeHelper_t1801316144_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_primes_table_0();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_12 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_13 = ((  int32_t (*) (RuntimeObject * /* static, unused */, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_13;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C"  void HashSet_1__ctor_m3009937319_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		NullCheck((HashSet_1_t2249360193 *)__this);
		((  void (*) (HashSet_1_t2249360193 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((HashSet_1_t2249360193 *)__this, (int32_t)((int32_t)10), (RuntimeObject*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m4207954649_gshared (HashSet_1_t2249360193 * __this, SerializationInfo_t1812684756 * ___info0, StreamingContext_t1986025897  ___context1, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2939335827((RuntimeObject *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1812684756 * L_0 = ___info0;
		__this->set_si_12(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4045343891_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t614413977  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m635741614((&L_0), (HashSet_1_t2249360193 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t614413977  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m343311015_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1394230_gshared (HashSet_1_t2249360193 * __this, ObjectU5BU5D_t3385344125* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3385344125* L_0 = ___array0;
		int32_t L_1 = ___index1;
		NullCheck((HashSet_1_t2249360193 *)__this);
		((  void (*) (HashSet_1_t2249360193 *, ObjectU5BU5D_t3385344125*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((HashSet_1_t2249360193 *)__this, (ObjectU5BU5D_t3385344125*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4287666857_gshared (HashSet_1_t2249360193 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		((  bool (*) (HashSet_1_t2249360193 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((HashSet_1_t2249360193 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1812428049_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t614413977  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m635741614((&L_0), (HashSet_1_t2249360193 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Enumerator_t614413977  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m2712207054_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_9();
		return L_0;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m2485840618_gshared (HashSet_1_t2249360193 * __this, int32_t ___capacity0, RuntimeObject* ___comparer1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_Init_m2485840618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* G_B4_0 = NULL;
	HashSet_1_t2249360193 * G_B4_1 = NULL;
	RuntimeObject* G_B3_0 = NULL;
	HashSet_1_t2249360193 * G_B3_1 = NULL;
	{
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t2239795282 * L_1 = (ArgumentOutOfRangeException_t2239795282 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2239795282_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1395591393(L_1, (String_t*)_stringLiteral2532065760, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject* L_2 = ___comparer1;
		RuntimeObject* L_3 = (RuntimeObject*)L_2;
		G_B3_0 = L_3;
		G_B3_1 = ((HashSet_1_t2249360193 *)(__this));
		if (L_3)
		{
			G_B4_0 = L_3;
			G_B4_1 = ((HashSet_1_t2249360193 *)(__this));
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6));
		EqualityComparer_1_t3215232089 * L_4 = ((  EqualityComparer_1_t3215232089 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		G_B4_0 = ((RuntimeObject*)(L_4));
		G_B4_1 = ((HashSet_1_t2249360193 *)(G_B3_1));
	}

IL_0020:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_comparer_11(G_B4_0);
		int32_t L_5 = ___capacity0;
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		___capacity0 = (int32_t)((int32_t)10);
	}

IL_002f:
	{
		int32_t L_6 = ___capacity0;
		___capacity0 = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_6)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_7 = ___capacity0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		((  void (*) (HashSet_1_t2249360193 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((HashSet_1_t2249360193 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_generation_13(0);
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m2246974508_gshared (HashSet_1_t2249360193 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_InitArrays_m2246974508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___size0;
		__this->set_table_4(((Int32U5BU5D_t3215173111*)SZArrayNew(Int32U5BU5D_t3215173111_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		int32_t L_1 = ___size0;
		__this->set_links_5(((LinkU5BU5D_t1226396739*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (uint32_t)L_1)));
		__this->set_empty_slot_8((-1));
		int32_t L_2 = ___size0;
		__this->set_slots_6(((ObjectU5BU5D_t3385344125*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (uint32_t)L_2)));
		__this->set_touched_7(0);
		Int32U5BU5D_t3215173111* L_3 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_3);
		__this->set_threshold_10((((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))))))*(float)(0.9f)))))));
		int32_t L_4 = (int32_t)__this->get_threshold_10();
		if (L_4)
		{
			goto IL_0068;
		}
	}
	{
		Int32U5BU5D_t3215173111* L_5 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0068;
		}
	}
	{
		__this->set_threshold_10(1);
	}

IL_0068:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m91888646_gshared (HashSet_1_t2249360193 * __this, int32_t ___index0, int32_t ___hash1, RuntimeObject * ___item2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Link_t890348390  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Int32U5BU5D_t3215173111* L_0 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		goto IL_00a9;
	}

IL_0010:
	{
		LinkU5BU5D_t1226396739* L_4 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		V_1 = (Link_t890348390 )(*(Link_t890348390 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))));
		int32_t L_6 = (int32_t)(&V_1)->get_HashCode_0();
		int32_t L_7 = ___hash1;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = ___hash1;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)-2147483648LL)))))
		{
			goto IL_0082;
		}
	}
	{
		RuntimeObject * L_9 = ___item2;
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		ObjectU5BU5D_t3385344125* L_10 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		if (L_13)
		{
			goto IL_0082;
		}
	}

IL_005b:
	{
		RuntimeObject * L_14 = ___item2;
		if (L_14)
		{
			goto IL_007c;
		}
	}
	{
		ObjectU5BU5D_t3385344125* L_15 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		G_B8_0 = ((((RuntimeObject*)(RuntimeObject *)NULL) == ((RuntimeObject*)(RuntimeObject *)L_18))? 1 : 0);
		goto IL_007d;
	}

IL_007c:
	{
		G_B8_0 = 0;
	}

IL_007d:
	{
		G_B10_0 = G_B8_0;
		goto IL_009a;
	}

IL_0082:
	{
		RuntimeObject* L_19 = (RuntimeObject*)__this->get_comparer_11();
		RuntimeObject * L_20 = ___item2;
		ObjectU5BU5D_t3385344125* L_21 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		RuntimeObject * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck((RuntimeObject*)L_19);
		bool L_25 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_19, (RuntimeObject *)L_20, (RuntimeObject *)L_24);
		G_B10_0 = ((int32_t)(L_25));
	}

IL_009a:
	{
		if (!G_B10_0)
		{
			goto IL_00a1;
		}
	}
	{
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_26 = (int32_t)(&V_1)->get_Next_1();
		V_0 = (int32_t)L_26;
	}

IL_00a9:
	{
		int32_t L_27 = V_0;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m1550812084_gshared (HashSet_1_t2249360193 * __this, ObjectU5BU5D_t3385344125* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3385344125* L_0 = ___array0;
		int32_t L_1 = ___index1;
		int32_t L_2 = (int32_t)__this->get_count_9();
		NullCheck((HashSet_1_t2249360193 *)__this);
		((  void (*) (HashSet_1_t2249360193 *, ObjectU5BU5D_t3385344125*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((HashSet_1_t2249360193 *)__this, (ObjectU5BU5D_t3385344125*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m5591029_gshared (HashSet_1_t2249360193 * __this, ObjectU5BU5D_t3385344125* ___array0, int32_t ___index1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_CopyTo_m5591029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3385344125* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t246139103 * L_1 = (ArgumentNullException_t246139103 *)il2cpp_codegen_object_new(ArgumentNullException_t246139103_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2488792999(L_1, (String_t*)_stringLiteral615595643, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t2239795282 * L_3 = (ArgumentOutOfRangeException_t2239795282 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2239795282_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1395591393(L_3, (String_t*)_stringLiteral2081506731, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index1;
		ObjectU5BU5D_t3385344125* L_5 = ___array0;
		NullCheck(L_5);
		if ((((int32_t)L_4) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		ArgumentException_t489606696 * L_6 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_6, (String_t*)_stringLiteral296885591, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0037:
	{
		ObjectU5BU5D_t3385344125* L_7 = ___array0;
		NullCheck(L_7);
		int32_t L_8 = ___index1;
		int32_t L_9 = ___count2;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))))-(int32_t)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_004d;
		}
	}
	{
		ArgumentException_t489606696 * L_10 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_10, (String_t*)_stringLiteral2631732851, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_004d:
	{
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		goto IL_007e;
	}

IL_0056:
	{
		int32_t L_11 = V_0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		int32_t L_12 = ((  int32_t (*) (HashSet_1_t2249360193 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((HashSet_1_t2249360193 *)__this, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		ObjectU5BU5D_t3385344125* L_13 = ___array0;
		int32_t L_14 = ___index1;
		int32_t L_15 = (int32_t)L_14;
		___index1 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		ObjectU5BU5D_t3385344125* L_16 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (RuntimeObject *)L_19);
	}

IL_007a:
	{
		int32_t L_20 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_007e:
	{
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)__this->get_touched_7();
		if ((((int32_t)L_21) >= ((int32_t)L_22)))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_23 = V_1;
		int32_t L_24 = ___count2;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0056;
		}
	}

IL_0091:
	{
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C"  void HashSet_1_Resize_m2833304277_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_Resize_m2833304277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3215173111* V_1 = NULL;
	LinkU5BU5D_t1226396739* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ObjectU5BU5D_t3385344125* V_7 = NULL;
	int32_t V_8 = 0;
	{
		Int32U5BU5D_t3215173111* L_0 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 15));
		int32_t L_1 = ((  int32_t (*) (RuntimeObject * /* static, unused */, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t3215173111*)((Int32U5BU5D_t3215173111*)SZArrayNew(Int32U5BU5D_t3215173111_il2cpp_TypeInfo_var, (uint32_t)L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t1226396739*)((LinkU5BU5D_t1226396739*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (uint32_t)L_3));
		V_3 = (int32_t)0;
		goto IL_00a6;
	}

IL_0027:
	{
		Int32U5BU5D_t3215173111* L_4 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_4 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_009a;
	}

IL_0038:
	{
		LinkU5BU5D_t1226396739* L_8 = V_2;
		int32_t L_9 = V_4;
		NullCheck(L_8);
		ObjectU5BU5D_t3385344125* L_10 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck((HashSet_1_t2249360193 *)__this);
		int32_t L_14 = ((  int32_t (*) (HashSet_1_t2249360193 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((HashSet_1_t2249360193 *)__this, (RuntimeObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		int32_t L_15 = (int32_t)L_14;
		V_8 = (int32_t)L_15;
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->set_HashCode_0(L_15);
		int32_t L_16 = V_8;
		V_5 = (int32_t)L_16;
		int32_t L_17 = V_5;
		int32_t L_18 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17&(int32_t)((int32_t)2147483647LL)))%(int32_t)L_18));
		LinkU5BU5D_t1226396739* L_19 = V_2;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		Int32U5BU5D_t3215173111* L_21 = V_1;
		int32_t L_22 = V_6;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->set_Next_1(((int32_t)((int32_t)L_24-(int32_t)1)));
		Int32U5BU5D_t3215173111* L_25 = V_1;
		int32_t L_26 = V_6;
		int32_t L_27 = V_4;
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (int32_t)((int32_t)((int32_t)L_27+(int32_t)1)));
		LinkU5BU5D_t1226396739* L_28 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_29 = V_4;
		NullCheck(L_28);
		int32_t L_30 = (int32_t)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29)))->get_Next_1();
		V_4 = (int32_t)L_30;
	}

IL_009a:
	{
		int32_t L_31 = V_4;
		if ((!(((uint32_t)L_31) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_32 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00a6:
	{
		int32_t L_33 = V_3;
		Int32U5BU5D_t3215173111* L_34 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3215173111* L_35 = V_1;
		__this->set_table_4(L_35);
		LinkU5BU5D_t1226396739* L_36 = V_2;
		__this->set_links_5(L_36);
		int32_t L_37 = V_0;
		V_7 = (ObjectU5BU5D_t3385344125*)((ObjectU5BU5D_t3385344125*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (uint32_t)L_37));
		ObjectU5BU5D_t3385344125* L_38 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		ObjectU5BU5D_t3385344125* L_39 = V_7;
		int32_t L_40 = (int32_t)__this->get_touched_7();
		Array_Copy_m2973674085(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_38, (int32_t)0, (RuntimeArray *)(RuntimeArray *)L_39, (int32_t)0, (int32_t)L_40, /*hidden argument*/NULL);
		ObjectU5BU5D_t3385344125* L_41 = V_7;
		__this->set_slots_6(L_41);
		int32_t L_42 = V_0;
		__this->set_threshold_10((((int32_t)((int32_t)((float)((float)(((float)((float)L_42)))*(float)(0.9f)))))));
		return;
	}
}
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m3808839237_gshared (HashSet_1_t2249360193 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		LinkU5BU5D_t1226396739* L_0 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_HashCode_0();
		return ((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648LL)));
	}
}
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m2734102285_gshared (HashSet_1_t2249360193 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((int32_t)-2147483648LL);
	}

IL_0011:
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_comparer_11();
		RuntimeObject * L_2 = ___item0;
		NullCheck((RuntimeObject*)L_1);
		int32_t L_3 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_1, (RuntimeObject *)L_2);
		return ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648LL)));
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C"  bool HashSet_1_Add_m2665068907_gshared (HashSet_1_t2249360193 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		int32_t L_1 = ((  int32_t (*) (HashSet_1_t2249360193 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((HashSet_1_t2249360193 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Int32U5BU5D_t3215173111* L_3 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_3);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))));
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RuntimeObject * L_6 = ___item0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		bool L_7 = ((  bool (*) (HashSet_1_t2249360193 *, int32_t, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((HashSet_1_t2249360193 *)__this, (int32_t)L_4, (int32_t)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		if (!L_7)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		int32_t L_8 = (int32_t)__this->get_count_9();
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_3 = (int32_t)L_9;
		__this->set_count_9(L_9);
		int32_t L_10 = V_3;
		int32_t L_11 = (int32_t)__this->get_threshold_10();
		if ((((int32_t)L_10) <= ((int32_t)L_11)))
		{
			goto IL_005c;
		}
	}
	{
		NullCheck((HashSet_1_t2249360193 *)__this);
		((  void (*) (HashSet_1_t2249360193 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((HashSet_1_t2249360193 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_12 = V_0;
		Int32U5BU5D_t3215173111* L_13 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_13);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))))));
	}

IL_005c:
	{
		int32_t L_14 = (int32_t)__this->get_empty_slot_8();
		V_2 = (int32_t)L_14;
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_16 = (int32_t)__this->get_touched_7();
		int32_t L_17 = (int32_t)L_16;
		V_3 = (int32_t)L_17;
		__this->set_touched_7(((int32_t)((int32_t)L_17+(int32_t)1)));
		int32_t L_18 = V_3;
		V_2 = (int32_t)L_18;
		goto IL_0098;
	}

IL_0081:
	{
		LinkU5BU5D_t1226396739* L_19 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_20 = V_2;
		NullCheck(L_19);
		int32_t L_21 = (int32_t)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->get_Next_1();
		__this->set_empty_slot_8(L_21);
	}

IL_0098:
	{
		LinkU5BU5D_t1226396739* L_22 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		int32_t L_24 = V_0;
		((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->set_HashCode_0(L_24);
		LinkU5BU5D_t1226396739* L_25 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		Int32U5BU5D_t3215173111* L_27 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		int32_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->set_Next_1(((int32_t)((int32_t)L_30-(int32_t)1)));
		Int32U5BU5D_t3215173111* L_31 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (int32_t)((int32_t)((int32_t)L_33+(int32_t)1)));
		ObjectU5BU5D_t3385344125* L_34 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_35 = V_2;
		RuntimeObject * L_36 = ___item0;
		NullCheck(L_34);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (RuntimeObject *)L_36);
		int32_t L_37 = (int32_t)__this->get_generation_13();
		__this->set_generation_13(((int32_t)((int32_t)L_37+(int32_t)1)));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C"  void HashSet_1_Clear_m4205498779_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		__this->set_count_9(0);
		Int32U5BU5D_t3215173111* L_0 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		Int32U5BU5D_t3215173111* L_1 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_1);
		Array_Clear_m1924552723(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t3385344125* L_2 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		ObjectU5BU5D_t3385344125* L_3 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		NullCheck(L_3);
		Array_Clear_m1924552723(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t1226396739* L_4 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		LinkU5BU5D_t1226396739* L_5 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		NullCheck(L_5);
		Array_Clear_m1924552723(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		__this->set_empty_slot_8((-1));
		__this->set_touched_7(0);
		int32_t L_6 = (int32_t)__this->get_generation_13();
		__this->set_generation_13(((int32_t)((int32_t)L_6+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C"  bool HashSet_1_Contains_m1064284759_gshared (HashSet_1_t2249360193 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		int32_t L_1 = ((  int32_t (*) (HashSet_1_t2249360193 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((HashSet_1_t2249360193 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Int32U5BU5D_t3215173111* L_3 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_3);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))));
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RuntimeObject * L_6 = ___item0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		bool L_7 = ((  bool (*) (HashSet_1_t2249360193 *, int32_t, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((HashSet_1_t2249360193 *)__this, (int32_t)L_4, (int32_t)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return L_7;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C"  bool HashSet_1_Remove_m1010352241_gshared (HashSet_1_t2249360193 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_Remove_m1010352241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Link_t890348390  V_4;
	memset(&V_4, 0, sizeof(V_4));
	RuntimeObject * V_5 = NULL;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	{
		RuntimeObject * L_0 = ___item0;
		NullCheck((HashSet_1_t2249360193 *)__this);
		int32_t L_1 = ((  int32_t (*) (HashSet_1_t2249360193 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((HashSet_1_t2249360193 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Int32U5BU5D_t3215173111* L_3 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		NullCheck(L_3);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)2147483647LL)))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))));
		Int32U5BU5D_t3215173111* L_4 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = (int32_t)((int32_t)((int32_t)L_7-(int32_t)1));
		int32_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)(-1)))))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)0;
	}

IL_002d:
	{
		V_3 = (int32_t)(-1);
	}

IL_002f:
	{
		LinkU5BU5D_t1226396739* L_9 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_10 = V_2;
		NullCheck(L_9);
		V_4 = (Link_t890348390 )(*(Link_t890348390 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10))));
		int32_t L_11 = (int32_t)(&V_4)->get_HashCode_0();
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_00c4;
		}
	}
	{
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)-2147483648LL)))))
		{
			goto IL_00a2;
		}
	}
	{
		RuntimeObject * L_14 = ___item0;
		if (!L_14)
		{
			goto IL_007b;
		}
	}
	{
		ObjectU5BU5D_t3385344125* L_15 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		if (L_18)
		{
			goto IL_00a2;
		}
	}

IL_007b:
	{
		RuntimeObject * L_19 = ___item0;
		if (L_19)
		{
			goto IL_009c;
		}
	}
	{
		ObjectU5BU5D_t3385344125* L_20 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_21 = V_2;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		RuntimeObject * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		G_B10_0 = ((((RuntimeObject*)(RuntimeObject *)NULL) == ((RuntimeObject*)(RuntimeObject *)L_23))? 1 : 0);
		goto IL_009d;
	}

IL_009c:
	{
		G_B10_0 = 0;
	}

IL_009d:
	{
		G_B12_0 = G_B10_0;
		goto IL_00ba;
	}

IL_00a2:
	{
		RuntimeObject* L_24 = (RuntimeObject*)__this->get_comparer_11();
		ObjectU5BU5D_t3385344125* L_25 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		RuntimeObject * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		RuntimeObject * L_29 = ___item0;
		NullCheck((RuntimeObject*)L_24);
		bool L_30 = InterfaceFuncInvoker2< bool, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (RuntimeObject*)L_24, (RuntimeObject *)L_28, (RuntimeObject *)L_29);
		G_B12_0 = ((int32_t)(L_30));
	}

IL_00ba:
	{
		if (!G_B12_0)
		{
			goto IL_00c4;
		}
	}
	{
		goto IL_00d5;
	}

IL_00c4:
	{
		int32_t L_31 = V_2;
		V_3 = (int32_t)L_31;
		int32_t L_32 = (int32_t)(&V_4)->get_Next_1();
		V_2 = (int32_t)L_32;
		int32_t L_33 = V_2;
		if ((!(((uint32_t)L_33) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}

IL_00d5:
	{
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00de;
		}
	}
	{
		return (bool)0;
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)__this->get_count_9();
		__this->set_count_9(((int32_t)((int32_t)L_35-(int32_t)1)));
		int32_t L_36 = V_3;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0113;
		}
	}
	{
		Int32U5BU5D_t3215173111* L_37 = (Int32U5BU5D_t3215173111*)__this->get_table_4();
		int32_t L_38 = V_1;
		LinkU5BU5D_t1226396739* L_39 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_40 = V_2;
		NullCheck(L_39);
		int32_t L_41 = (int32_t)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))->get_Next_1();
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (int32_t)((int32_t)((int32_t)L_41+(int32_t)1)));
		goto IL_0135;
	}

IL_0113:
	{
		LinkU5BU5D_t1226396739* L_42 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_43 = V_3;
		NullCheck(L_42);
		LinkU5BU5D_t1226396739* L_44 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		int32_t L_46 = (int32_t)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->get_Next_1();
		((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43)))->set_Next_1(L_46);
	}

IL_0135:
	{
		LinkU5BU5D_t1226396739* L_47 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_48 = V_2;
		NullCheck(L_47);
		int32_t L_49 = (int32_t)__this->get_empty_slot_8();
		((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48)))->set_Next_1(L_49);
		int32_t L_50 = V_2;
		__this->set_empty_slot_8(L_50);
		LinkU5BU5D_t1226396739* L_51 = (LinkU5BU5D_t1226396739*)__this->get_links_5();
		int32_t L_52 = V_2;
		NullCheck(L_51);
		((L_51)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_52)))->set_HashCode_0(0);
		ObjectU5BU5D_t3385344125* L_53 = (ObjectU5BU5D_t3385344125*)__this->get_slots_6();
		int32_t L_54 = V_2;
		Initobj (RuntimeObject_il2cpp_TypeInfo_var, (&V_5));
		RuntimeObject * L_55 = V_5;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (RuntimeObject *)L_55);
		int32_t L_56 = (int32_t)__this->get_generation_13();
		__this->set_generation_13(((int32_t)((int32_t)L_56+(int32_t)1)));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m3183843147_gshared (HashSet_1_t2249360193 * __this, SerializationInfo_t1812684756 * ___info0, StreamingContext_t1986025897  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_GetObjectData_m3183843147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3535071701 * L_0 = (NotImplementedException_t3535071701 *)il2cpp_codegen_object_new(NotImplementedException_t3535071701_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2256677597(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m3908179593_gshared (HashSet_1_t2249360193 * __this, RuntimeObject * ___sender0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HashSet_1_OnDeserialization_m3908179593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t1812684756 * L_0 = (SerializationInfo_t1812684756 *)__this->get_si_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		NotImplementedException_t3535071701 * L_1 = (NotImplementedException_t3535071701 *)il2cpp_codegen_object_new(NotImplementedException_t3535071701_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2256677597(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t614413977  HashSet_1_GetEnumerator_m3173548986_gshared (HashSet_1_t2249360193 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t614413977  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m635741614((&L_0), (HashSet_1_t2249360193 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
