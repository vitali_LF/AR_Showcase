﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t1009044977;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct List_1_t2349647505;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2226230279;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct ConnectionChangeEvent_t190635484;
// System.Byte[]
struct ByteU5BU5D_t3567143369;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445;
// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t2013519524;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t71323286;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerable_1_t762398783;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t638981557;
// System.String
struct String_t;
// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct MessageEventArgs_t2772358345;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityEvent_1_t1459204717;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3533840856;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1
struct U3CAddAndCreateU3Ec__AnonStorey1_t3362117636;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct MessageTypeSubscribers_t675444414;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct MessageEvent_t3114892025;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2202423910;
// UnityEngine.Object
struct Object_t3645472222;
// System.ArgumentException
struct ArgumentException_t489606696;
// System.Object[]
struct ObjectU5BU5D_t3385344125;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t3894937101;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1324617639;
// UnityEngine.GameObject
struct GameObject_t2881801184;
// UnityEngine.Playables.PlayableBehaviour
struct PlayableBehaviour_t716264763;
// System.Type
struct Type_t;
// UnityEngine.PlayerConnectionInternal
struct PlayerConnectionInternal_t3959366414;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4289523592;
// System.Attribute
struct Attribute_t3130080784;
// UnityEngine.RangeAttribute
struct RangeAttribute_t2306247939;
// UnityEngine.RectOffset
struct RectOffset_t817686154;
// UnityEngine.RectTransform
struct RectTransform_t1161610249;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1147600571;
// System.Delegate
struct Delegate_t4015201940;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1432878832;
// UnityEngine.Component
struct Component_t2335505321;
// UnityEngine.Transform
struct Transform_t3580444445;
// System.IAsyncResult
struct IAsyncResult_t4224419158;
// System.AsyncCallback
struct AsyncCallback_t4283869127;
// UnityEngine.Renderer
struct Renderer_t3456269968;
// UnityEngine.Material
struct Material_t1926439680;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3731058050;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t3383400165;
// UnityEngine.Texture
struct Texture_t954505614;
// UnityEngine.RenderTexture
struct RenderTexture_t3737750657;
// UnityEngine.RequireComponent
struct RequireComponent_t3219866953;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2618073252;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3754315317;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t671528035;
// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct GeneratedByOldBindingsGeneratorAttribute_t1773994740;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t2936828858;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t2845240971;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t867521075;
// UnityEngine.Camera
struct Camera_t362346687;
// UnityEngine.GUILayer
struct GUILayer_t1634068716;
// UnityEngine.GUIElement
struct GUIElement_t2757512779;
// UnityEngine.Camera[]
struct CameraU5BU5D_t365668710;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t384831209;
// UnityEngine.SerializeField
struct SerializeField_t2792443689;
// System.Collections.IEnumerator
struct IEnumerator_t2051810174;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t586498270;
// UnityEngine.Sprite
struct Sprite_t2497061588;
// UnityEngine.Texture2D
struct Texture2D_t91252194;
// System.Diagnostics.StackTrace
struct StackTrace_t1780976090;
// System.Text.StringBuilder
struct StringBuilder_t209334805;
// System.Exception
struct Exception_t214279536;
// System.String[]
struct StringU5BU5D_t1589106382;
// System.Char[]
struct CharU5BU5D_t2953840665;
// System.Reflection.ParameterInfo
struct ParameterInfo_t3156340899;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t2141263104;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2352998046;
// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t2870195091;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t3230141494;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t1683994280;
// UnityEngine.TrackedReference
struct TrackedReference_t2612049617;
// UnityEngine.Transform/Enumerator
struct Enumerator_t3595687596;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t260058957;
// System.Action`1<System.Object>
struct Action_1_t2325042916;
// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct RequestAtlasCallback_t3823435737;
// UnityEngine.U2D.SpriteAtlas
struct SpriteAtlas_t2782010525;
// System.AppDomain
struct AppDomain_t2391992810;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t1143045648;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t2712850390;
// UnityEngine.UnityException
struct UnityException_t2602360046;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1812684756;
// UnityEngine.UnityLogWriter
struct UnityLogWriter_t3161904322;
// System.IO.TextWriter
struct TextWriter_t4111495313;
// UnityEngine.UnitySynchronizationContext
struct UnitySynchronizationContext_t1450698442;
// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct Queue_1_t2402182407;
// System.Threading.Thread
struct Thread_t2508967565;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t3011612328;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t1528611714;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t1398859231;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3659678378;
// System.IntPtr[]
struct IntPtrU5BU5D_t3930959911;
// System.Collections.IDictionary
struct IDictionary_t1084331215;
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t4219401622;
// System.Globalization.CultureInfo
struct CultureInfo_t4200194569;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t1571261698;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t180310316;
// System.Globalization.TextInfo
struct TextInfo_t2575416363;
// System.Globalization.CompareInfo
struct CompareInfo_t388894277;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1255745277;
// System.Globalization.Calendar
struct Calendar_t1639743412;
// System.Collections.Hashtable
struct Hashtable_t1793686270;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1210539667;
// System.Int32
struct Int32_t3515577538;
// System.Void
struct Void_t4071739332;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1685419976;
// System.Reflection.MethodBase
struct MethodBase_t3670318294;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1610793130;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t3172597217;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3408587689;
// UnityEngine.UnitySynchronizationContext/WorkRequest[]
struct WorkRequestU5BU5D_t3277092752;
// System.Collections.ArrayList
struct ArrayList_t1797015065;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1016514262;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers[]
struct MessageTypeSubscribersU5BU5D_t1032573067;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t2620507134;
// System.Boolean[]
struct BooleanU5BU5D_t1963761663;
// System.Byte
struct Byte_t2640180184;
// System.Double
struct Double_t3942828892;
// System.UInt16
struct UInt16_t3430003764;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2272071576;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t1581260623;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t2468626509;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t1275866152;
// System.Security.Policy.Evidence
struct Evidence_t1538274723;
// System.Security.PermissionSet
struct PermissionSet_t448394360;
// System.Security.Principal.IPrincipal
struct IPrincipal_t389896833;
// System.AppDomainManager
struct AppDomainManager_t3746511876;
// System.ActivationContext
struct ActivationContext_t711302796;
// System.ApplicationIdentity
struct ApplicationIdentity_t1182042815;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t379728391;
// System.ResolveEventHandler
struct ResolveEventHandler_t3534413881;
// System.EventHandler
struct EventHandler_t1089632771;
// System.Threading.ExecutionContext
struct ExecutionContext_t1695260799;
// System.MulticastDelegate
struct MulticastDelegate_t599438524;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t1865297292;
// System.Type[]
struct TypeU5BU5D_t98989581;
// System.Reflection.MemberFilter
struct MemberFilter_t2882044324;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t4202419344;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3797817720;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t3349755816;

extern RuntimeClass* List_1_t2349647505_il2cpp_TypeInfo_var;
extern RuntimeClass* ConnectionChangeEvent_t190635484_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m164277227_RuntimeMethod_var;
extern const uint32_t PlayerEditorConnectionEvents__ctor_m2224928885_MetadataUsageId;
extern RuntimeClass* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t2013519524_il2cpp_TypeInfo_var;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t959145111_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageEventArgs_t2772358345_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t762398783_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t2085253399_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t2051810174_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3181680589_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m2724902477_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m2035350960_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Where_TisMessageTypeSubscribers_t675444414_m1955804045_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1_Invoke_m2359140723_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2648312538;
extern const uint32_t PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4106540626_MetadataUsageId;
extern RuntimeClass* U3CAddAndCreateU3Ec__AnonStorey1_t3362117636_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageTypeSubscribers_t675444414_il2cpp_TypeInfo_var;
extern RuntimeClass* MessageEvent_t3114892025_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m1575543412_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t675444414_m557389689_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1330338227_RuntimeMethod_var;
extern const uint32_t PlayerEditorConnectionEvents_AddAndCreate_m378817323_MetadataUsageId;
extern const uint32_t U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m1575543412_MetadataUsageId;
extern const uint32_t U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m2724902477_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m1358771383_RuntimeMethod_var;
extern const uint32_t ConnectionChangeEvent__ctor_m2384156855_MetadataUsageId;
extern const RuntimeMethod* UnityEvent_1__ctor_m1455957073_RuntimeMethod_var;
extern const uint32_t MessageEvent__ctor_m3036318349_MetadataUsageId;
extern const uint32_t MessageTypeSubscribers__ctor_m1862007631_MetadataUsageId;
extern RuntimeClass* Object_t3645472222_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m767057946_MetadataUsageId;
extern const uint32_t Object_Destroy_m731272364_MetadataUsageId;
extern const uint32_t Object_DestroyImmediate_m1573878054_MetadataUsageId;
extern const uint32_t Object_Equals_m1066417117_MetadataUsageId;
extern const uint32_t Object_op_Implicit_m1340895429_MetadataUsageId;
extern const uint32_t Object_CompareBaseObjects_m3329896178_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m3366595570_MetadataUsageId;
extern RuntimeClass* ScriptableObject_t1324617639_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t489606696_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3769644588;
extern Il2CppCodeGenString* _stringLiteral4211405290;
extern const uint32_t Object_Instantiate_m3076090176_MetadataUsageId;
extern const uint32_t Object_CheckNullArgument_m3054168192_MetadataUsageId;
extern const uint32_t Object_op_Equality_m3342981539_MetadataUsageId;
extern const uint32_t Object_op_Inequality_m1808560565_MetadataUsageId;
extern const uint32_t Object__cctor_m649958370_MetadataUsageId;
extern RuntimeClass* Vector3_t3932393085_il2cpp_TypeInfo_var;
extern const uint32_t Plane__ctor_m1011206029_MetadataUsageId;
extern RuntimeClass* Mathf_t9189715_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m894877444_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t897798503_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral189236002;
extern const uint32_t Plane_ToString_m624364449_MetadataUsageId;
extern RuntimeClass* Playable_t379060559_il2cpp_TypeInfo_var;
extern const uint32_t Playable_get_Null_m3255192774_MetadataUsageId;
extern const uint32_t Playable__cctor_m1953763072_MetadataUsageId;
extern RuntimeClass* PlayableBinding_t2941134609_il2cpp_TypeInfo_var;
extern const uint32_t PlayableAsset_get_duration_m562987568_MetadataUsageId;
extern const uint32_t PlayableAsset_Internal_CreatePlayable_m3003537458_MetadataUsageId;
struct Object_t3645472222_marshaled_pinvoke;
struct Object_t3645472222;;
struct Object_t3645472222_marshaled_pinvoke;;
struct Object_t3645472222_marshaled_com;
struct Object_t3645472222_marshaled_com;;
extern RuntimeClass* PlayableBindingU5BU5D_t1865297292_il2cpp_TypeInfo_var;
extern const uint32_t PlayableBinding__cctor_m3021184850_MetadataUsageId;
extern RuntimeClass* PlayableHandle_t1495371730_il2cpp_TypeInfo_var;
extern const uint32_t PlayableHandle_get_Null_m3770919321_MetadataUsageId;
extern const uint32_t PlayableHandle_Equals_m3886890033_MetadataUsageId;
extern RuntimeClass* PlayableOutput_t2773044158_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutput__cctor_m3745137773_MetadataUsageId;
extern RuntimeClass* PlayableOutputHandle_t1247041817_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutputHandle_get_Null_m3647248066_MetadataUsageId;
extern const uint32_t PlayableOutputHandle_Equals_m4078288475_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1419227944;
extern Il2CppCodeGenString* _stringLiteral4076503653;
extern const uint32_t PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m244187087_MetadataUsageId;
extern const uint32_t PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m4118275576_MetadataUsageId;
extern RuntimeClass* PropertyName_t3787932763_il2cpp_TypeInfo_var;
extern const uint32_t PropertyName_Equals_m2052821075_MetadataUsageId;
extern RuntimeClass* Int32_t3515577538_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3267787781;
extern const uint32_t PropertyName_ToString_m1539975057_MetadataUsageId;
extern RuntimeClass* Quaternion_t3497065016_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_LookRotation_m3488380577_MetadataUsageId;
extern const uint32_t Quaternion_Inverse_m1326095064_MetadataUsageId;
extern const uint32_t Quaternion_Euler_m2250155234_MetadataUsageId;
extern const uint32_t Quaternion_Internal_FromEulerRad_m4014579486_MetadataUsageId;
extern const uint32_t Quaternion_get_identity_m1563966278_MetadataUsageId;
extern const uint32_t Quaternion_op_Equality_m776083444_MetadataUsageId;
extern const uint32_t Quaternion_op_Inequality_m1992395400_MetadataUsageId;
extern const uint32_t Quaternion_Equals_m3839468225_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral625126391;
extern const uint32_t Quaternion_ToString_m324521689_MetadataUsageId;
extern const uint32_t Quaternion__cctor_m1771063772_MetadataUsageId;
extern const uint32_t Ray_GetPoint_m3306741896_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3457502417;
extern const uint32_t Ray_ToString_m5131743_MetadataUsageId;
extern RuntimeClass* Rect_t952252086_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m551946840_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral314791402;
extern const uint32_t Rect_ToString_m1785269746_MetadataUsageId;
extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t RectOffset_t817686154_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t RectOffset_t817686154_com_FromNativeMethodDefinition_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3079287594;
extern const uint32_t RectOffset_ToString_m2955588694_MetadataUsageId;
extern RuntimeClass* RectTransform_t1161610249_il2cpp_TypeInfo_var;
extern RuntimeClass* ReapplyDrivenProperties_t1147600571_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m65241362_MetadataUsageId;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m3550100840_MetadataUsageId;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m1445631001_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral953137448;
extern const uint32_t RectTransform_GetLocalCorners_m2981119692_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral760554469;
extern const uint32_t RectTransform_GetWorldCorners_m2129982010_MetadataUsageId;
extern RuntimeClass* Vector2_t2246369278_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m770305266_MetadataUsageId;
extern const uint32_t CommandBuffer__ctor_m3971617957_MetadataUsageId;
extern const uint32_t CommandBuffer_Dispose_m3807691801_MetadataUsageId;
extern const uint32_t RenderTargetIdentifier__ctor_m3059967350_MetadataUsageId;
extern RuntimeClass* BuiltinRenderTextureType_t3735833062_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral196967772;
extern const uint32_t RenderTargetIdentifier_ToString_m430154943_MetadataUsageId;
extern const uint32_t RenderTargetIdentifier_GetHashCode_m782971227_MetadataUsageId;
extern RuntimeClass* RenderTargetIdentifier_t35305570_il2cpp_TypeInfo_var;
extern const uint32_t RenderTargetIdentifier_Equals_m2799259023_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral632663534;
extern const uint32_t Resolution_ToString_m1282970970_MetadataUsageId;
extern RuntimeClass* Scene_t1083318001_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m4240418630_MetadataUsageId;
extern RuntimeClass* SceneManager_t793739027_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityAction_2_Invoke_m2791872171_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m3528941024_MetadataUsageId;
extern const RuntimeMethod* UnityAction_1_Invoke_m1991915580_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m398202129_MetadataUsageId;
extern const RuntimeMethod* UnityAction_2_Invoke_m2120342004_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m1668464213_MetadataUsageId;
extern const uint32_t ScriptableObject__ctor_m3277333935_MetadataUsageId;
extern RuntimeClass* SendMouseEvents_t2508014564_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m3480495290_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGUILayer_t1634068716_m3037765150_RuntimeMethod_var;
extern const uint32_t SendMouseEvents_HitTestLegacyGUI_m1463766836_MetadataUsageId;
extern RuntimeClass* Input_t1636556003_il2cpp_TypeInfo_var;
extern RuntimeClass* CameraU5BU5D_t365668710_il2cpp_TypeInfo_var;
extern RuntimeClass* HitInfo_t3525137541_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m507784797_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3814634610;
extern Il2CppCodeGenString* _stringLiteral2112713013;
extern Il2CppCodeGenString* _stringLiteral3725919253;
extern Il2CppCodeGenString* _stringLiteral225260280;
extern Il2CppCodeGenString* _stringLiteral956983439;
extern Il2CppCodeGenString* _stringLiteral3850542936;
extern Il2CppCodeGenString* _stringLiteral392450877;
extern const uint32_t SendMouseEvents_SendEvents_m3061086367_MetadataUsageId;
extern RuntimeClass* HitInfoU5BU5D_t1685419976_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m2605800095_MetadataUsageId;
extern const uint32_t HitInfo_op_Implicit_m1366484775_MetadataUsageId;
extern const uint32_t HitInfo_Compare_m804408274_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1994973061;
extern Il2CppCodeGenString* _stringLiteral2859367373;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m1006292583_MetadataUsageId;
extern const uint32_t SetupCoroutine_InvokeMember_m1820235480_MetadataUsageId;
extern RuntimeClass* StackTraceUtility_t2534606224_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1097760428;
extern Il2CppCodeGenString* _stringLiteral2544850413;
extern const uint32_t StackTraceUtility_SetProjectFolder_m2765252549_MetadataUsageId;
extern RuntimeClass* StackTrace_t1780976090_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_ExtractStackTrace_m2680497121_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3197285762;
extern Il2CppCodeGenString* _stringLiteral2268048544;
extern Il2CppCodeGenString* _stringLiteral1736793488;
extern Il2CppCodeGenString* _stringLiteral3892946235;
extern Il2CppCodeGenString* _stringLiteral1040575422;
extern Il2CppCodeGenString* _stringLiteral2292808322;
extern const uint32_t StackTraceUtility_IsSystemStacktraceType_m3834892923_MetadataUsageId;
extern RuntimeClass* Exception_t214279536_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t209334805_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral742218889;
extern Il2CppCodeGenString* _stringLiteral2281730836;
extern Il2CppCodeGenString* _stringLiteral2637738159;
extern Il2CppCodeGenString* _stringLiteral2874725322;
extern Il2CppCodeGenString* _stringLiteral2186841919;
extern Il2CppCodeGenString* _stringLiteral2413282949;
extern const uint32_t StackTraceUtility_ExtractStringFromExceptionInternal_m2867183001_MetadataUsageId;
extern RuntimeClass* CharU5BU5D_t2953840665_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4089598773;
extern Il2CppCodeGenString* _stringLiteral2409193522;
extern Il2CppCodeGenString* _stringLiteral3585422989;
extern Il2CppCodeGenString* _stringLiteral4228279569;
extern Il2CppCodeGenString* _stringLiteral1058914747;
extern Il2CppCodeGenString* _stringLiteral230745881;
extern Il2CppCodeGenString* _stringLiteral1570534719;
extern Il2CppCodeGenString* _stringLiteral2076976895;
extern Il2CppCodeGenString* _stringLiteral2123922509;
extern Il2CppCodeGenString* _stringLiteral586184310;
extern Il2CppCodeGenString* _stringLiteral1728958755;
extern Il2CppCodeGenString* _stringLiteral537023139;
extern Il2CppCodeGenString* _stringLiteral3158739129;
extern Il2CppCodeGenString* _stringLiteral744691041;
extern const uint32_t StackTraceUtility_PostprocessStacktrace_m3052697160_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2979430886;
extern Il2CppCodeGenString* _stringLiteral1597761991;
extern Il2CppCodeGenString* _stringLiteral1856989646;
extern Il2CppCodeGenString* _stringLiteral2970545265;
extern Il2CppCodeGenString* _stringLiteral3780346290;
extern Il2CppCodeGenString* _stringLiteral1922078842;
extern Il2CppCodeGenString* _stringLiteral3566379873;
extern Il2CppCodeGenString* _stringLiteral2690811494;
extern Il2CppCodeGenString* _stringLiteral2280325812;
extern Il2CppCodeGenString* _stringLiteral442781658;
extern Il2CppCodeGenString* _stringLiteral4036302056;
extern Il2CppCodeGenString* _stringLiteral3391310575;
extern const uint32_t StackTraceUtility_ExtractFormattedStackTrace_m3595538786_MetadataUsageId;
extern const uint32_t StackTraceUtility__cctor_m988910645_MetadataUsageId;
extern const uint32_t Texture__ctor_m57122759_MetadataUsageId;
extern const uint32_t Texture2D__ctor_m2972303337_MetadataUsageId;
extern RuntimeClass* Texture2D_t91252194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3175759356;
extern const uint32_t Texture2D_CreateExternalTexture_m235343200_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchScreenKeyboardType_t1103124205_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2116446054_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard__ctor_m3688752920_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m3102498094_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m2332844710_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_t1683994280_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m2096239018_MetadataUsageId;
extern const uint32_t TrackedReference_op_Equality_m357163524_MetadataUsageId;
extern RuntimeClass* TrackedReference_t2612049617_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m1589786927_MetadataUsageId;
extern const uint32_t Transform_get_forward_m2380180709_MetadataUsageId;
extern RuntimeClass* Enumerator_t3595687596_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m2743450628_MetadataUsageId;
extern RuntimeClass* SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t260058957_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SpriteAtlasManager_Register_m793148487_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m3744639618_RuntimeMethod_var;
extern const uint32_t SpriteAtlasManager_RequestAtlas_m333581262_MetadataUsageId;
extern const uint32_t SpriteAtlasManager__cctor_m1994284186_MetadataUsageId;
extern RuntimeClass* UnhandledExceptionHandler_t4246526107_il2cpp_TypeInfo_var;
extern RuntimeClass* UnhandledExceptionEventHandler_t1143045648_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnhandledExceptionHandler_HandleUnhandledException_m3174347279_RuntimeMethod_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m2324019343_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1859338151;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m3174347279_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1142008342;
extern const uint32_t UnhandledExceptionHandler_PrintException_m1032894626_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral496184237;
extern const uint32_t UnityException__ctor_m616494757_MetadataUsageId;
extern RuntimeClass* TextWriter_t4111495313_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter__ctor_m1903404459_MetadataUsageId;
extern RuntimeClass* UnityLogWriter_t3161904322_il2cpp_TypeInfo_var;
extern RuntimeClass* Console_t2912832404_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter_Init_m1685913522_MetadataUsageId;
extern const uint32_t UnityString_Format_m1784030070_MetadataUsageId;
extern RuntimeClass* Queue_1_t2402182407_il2cpp_TypeInfo_var;
extern RuntimeClass* Thread_t2508967565_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Queue_1__ctor_m1321454259_RuntimeMethod_var;
extern const uint32_t UnitySynchronizationContext__ctor_m842443118_MetadataUsageId;
extern const RuntimeMethod* Queue_1_Dequeue_m1134160965_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_get_Count_m2804029644_RuntimeMethod_var;
extern const uint32_t UnitySynchronizationContext_Exec_m2847479435_MetadataUsageId;
extern RuntimeClass* UnitySynchronizationContext_t1450698442_il2cpp_TypeInfo_var;
extern const uint32_t UnitySynchronizationContext_InitializeSynchronizationContext_m2010976595_MetadataUsageId;
extern const uint32_t UnitySynchronizationContext_ExecuteTasks_m1057183886_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1705123583;
extern const uint32_t Vector2_get_Item_m1427364022_MetadataUsageId;
extern const uint32_t Vector2_set_Item_m1623790336_MetadataUsageId;
extern const uint32_t Vector2_Lerp_m2858900924_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3424384439;
extern const uint32_t Vector2_ToString_m3641689616_MetadataUsageId;
extern const uint32_t Vector2_Equals_m258839576_MetadataUsageId;
extern const uint32_t Vector2_op_Equality_m512848965_MetadataUsageId;
extern const uint32_t Vector2_op_Inequality_m190722814_MetadataUsageId;
extern const uint32_t Vector2_get_zero_m1981541827_MetadataUsageId;
extern const uint32_t Vector2_get_one_m1442422668_MetadataUsageId;
extern const uint32_t Vector2_get_up_m4207689530_MetadataUsageId;
extern const uint32_t Vector2__cctor_m1153611810_MetadataUsageId;

struct ByteU5BU5D_t3567143369;
struct ObjectU5BU5D_t3385344125;
struct PlayableBindingU5BU5D_t1865297292;
struct Vector3U5BU5D_t1432878832;
struct CameraU5BU5D_t365668710;
struct HitInfoU5BU5D_t1685419976;
struct ParameterModifierU5BU5D_t1706708300;
struct StringU5BU5D_t1589106382;
struct CharU5BU5D_t2953840665;
struct ParameterInfoU5BU5D_t1048445298;
struct Color32U5BU5D_t2352998046;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T214279536_H
#define EXCEPTION_T214279536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t214279536  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t3930959911* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t214279536 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t3930959911* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t3930959911** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t3930959911* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___inner_exception_1)); }
	inline Exception_t214279536 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t214279536 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t214279536 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t214279536, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t214279536, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t214279536, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T214279536_H
#ifndef STACKTRACE_T1780976090_H
#define STACKTRACE_T1780976090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackTrace
struct  StackTrace_t1780976090  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::frames
	StackFrameU5BU5D_t4219401622* ___frames_1;
	// System.Boolean System.Diagnostics.StackTrace::debug_info
	bool ___debug_info_2;

public:
	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(StackTrace_t1780976090, ___frames_1)); }
	inline StackFrameU5BU5D_t4219401622* get_frames_1() const { return ___frames_1; }
	inline StackFrameU5BU5D_t4219401622** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(StackFrameU5BU5D_t4219401622* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_debug_info_2() { return static_cast<int32_t>(offsetof(StackTrace_t1780976090, ___debug_info_2)); }
	inline bool get_debug_info_2() const { return ___debug_info_2; }
	inline bool* get_address_of_debug_info_2() { return &___debug_info_2; }
	inline void set_debug_info_2(bool value)
	{
		___debug_info_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKTRACE_T1780976090_H
#ifndef STACKTRACEUTILITY_T2534606224_H
#define STACKTRACEUTILITY_T2534606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StackTraceUtility
struct  StackTraceUtility_t2534606224  : public RuntimeObject
{
public:

public:
};

struct StackTraceUtility_t2534606224_StaticFields
{
public:
	// System.String UnityEngine.StackTraceUtility::projectFolder
	String_t* ___projectFolder_0;

public:
	inline static int32_t get_offset_of_projectFolder_0() { return static_cast<int32_t>(offsetof(StackTraceUtility_t2534606224_StaticFields, ___projectFolder_0)); }
	inline String_t* get_projectFolder_0() const { return ___projectFolder_0; }
	inline String_t** get_address_of_projectFolder_0() { return &___projectFolder_0; }
	inline void set_projectFolder_0(String_t* value)
	{
		___projectFolder_0 = value;
		Il2CppCodeGenWriteBarrier((&___projectFolder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKTRACEUTILITY_T2534606224_H
#ifndef DATAUTILITY_T3649025284_H
#define DATAUTILITY_T3649025284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprites.DataUtility
struct  DataUtility_t3649025284  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAUTILITY_T3649025284_H
#ifndef CULTUREINFO_T4200194569_H
#define CULTUREINFO_T4200194569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t4200194569  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t1571261698 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t180310316 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t2575416363 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t388894277 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t1255745277* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t4200194569 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t1639743412 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t3567143369* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___numInfo_14)); }
	inline NumberFormatInfo_t1571261698 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t1571261698 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t1571261698 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t180310316 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t180310316 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t180310316 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___textInfo_16)); }
	inline TextInfo_t2575416363 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t2575416363 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t2575416363 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___compareInfo_26)); }
	inline CompareInfo_t388894277 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t388894277 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t388894277 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t1255745277* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t1255745277** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t1255745277* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___parent_culture_30)); }
	inline CultureInfo_t4200194569 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t4200194569 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t4200194569 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___calendar_32)); }
	inline Calendar_t1639743412 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t1639743412 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t1639743412 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t3567143369* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t3567143369** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t3567143369* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t4200194569_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t4200194569 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t1793686270 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t1793686270 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t1210539667 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t1210539667 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t4200194569 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t4200194569 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t4200194569 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t1793686270 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t1793686270 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t1793686270 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t1793686270 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t1793686270 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t1793686270 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t1210539667 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t1210539667 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t1210539667 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t4200194569_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t1210539667 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t1210539667 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t1210539667 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T4200194569_H
#ifndef BINDER_T2026886273_H
#define BINDER_T2026886273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t2026886273  : public RuntimeObject
{
public:

public:
};

struct Binder_t2026886273_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t2026886273 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t2026886273_StaticFields, ___default_binder_0)); }
	inline Binder_t2026886273 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t2026886273 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t2026886273 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T2026886273_H
#ifndef SETUPCOROUTINE_T1241325280_H
#define SETUPCOROUTINE_T1241325280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SetupCoroutine
struct  SetupCoroutine_t1241325280  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPCOROUTINE_T1241325280_H
#ifndef SENDMOUSEEVENTS_T2508014564_H
#define SENDMOUSEEVENTS_T2508014564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents
struct  SendMouseEvents_t2508014564  : public RuntimeObject
{
public:

public:
};

struct SendMouseEvents_t2508014564_StaticFields
{
public:
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_t1685419976* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_t1685419976* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_t1685419976* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t365668710* ___m_Cameras_4;

public:
	inline static int32_t get_offset_of_s_MouseUsed_0() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2508014564_StaticFields, ___s_MouseUsed_0)); }
	inline bool get_s_MouseUsed_0() const { return ___s_MouseUsed_0; }
	inline bool* get_address_of_s_MouseUsed_0() { return &___s_MouseUsed_0; }
	inline void set_s_MouseUsed_0(bool value)
	{
		___s_MouseUsed_0 = value;
	}

	inline static int32_t get_offset_of_m_LastHit_1() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2508014564_StaticFields, ___m_LastHit_1)); }
	inline HitInfoU5BU5D_t1685419976* get_m_LastHit_1() const { return ___m_LastHit_1; }
	inline HitInfoU5BU5D_t1685419976** get_address_of_m_LastHit_1() { return &___m_LastHit_1; }
	inline void set_m_LastHit_1(HitInfoU5BU5D_t1685419976* value)
	{
		___m_LastHit_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastHit_1), value);
	}

	inline static int32_t get_offset_of_m_MouseDownHit_2() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2508014564_StaticFields, ___m_MouseDownHit_2)); }
	inline HitInfoU5BU5D_t1685419976* get_m_MouseDownHit_2() const { return ___m_MouseDownHit_2; }
	inline HitInfoU5BU5D_t1685419976** get_address_of_m_MouseDownHit_2() { return &___m_MouseDownHit_2; }
	inline void set_m_MouseDownHit_2(HitInfoU5BU5D_t1685419976* value)
	{
		___m_MouseDownHit_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseDownHit_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentHit_3() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2508014564_StaticFields, ___m_CurrentHit_3)); }
	inline HitInfoU5BU5D_t1685419976* get_m_CurrentHit_3() const { return ___m_CurrentHit_3; }
	inline HitInfoU5BU5D_t1685419976** get_address_of_m_CurrentHit_3() { return &___m_CurrentHit_3; }
	inline void set_m_CurrentHit_3(HitInfoU5BU5D_t1685419976* value)
	{
		___m_CurrentHit_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentHit_3), value);
	}

	inline static int32_t get_offset_of_m_Cameras_4() { return static_cast<int32_t>(offsetof(SendMouseEvents_t2508014564_StaticFields, ___m_Cameras_4)); }
	inline CameraU5BU5D_t365668710* get_m_Cameras_4() const { return ___m_Cameras_4; }
	inline CameraU5BU5D_t365668710** get_address_of_m_Cameras_4() { return &___m_Cameras_4; }
	inline void set_m_Cameras_4(CameraU5BU5D_t365668710* value)
	{
		___m_Cameras_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cameras_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMOUSEEVENTS_T2508014564_H
#ifndef SCREEN_T1942833645_H
#define SCREEN_T1942833645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Screen
struct  Screen_t1942833645  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREEN_T1942833645_H
#ifndef SCENEMANAGER_T793739027_H
#define SCENEMANAGER_T793739027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.SceneManager
struct  SceneManager_t793739027  : public RuntimeObject
{
public:

public:
};

struct SceneManager_t793739027_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> UnityEngine.SceneManagement.SceneManager::sceneLoaded
	UnityAction_2_t2618073252 * ___sceneLoaded_0;
	// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene> UnityEngine.SceneManagement.SceneManager::sceneUnloaded
	UnityAction_1_t3754315317 * ___sceneUnloaded_1;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene> UnityEngine.SceneManagement.SceneManager::activeSceneChanged
	UnityAction_2_t671528035 * ___activeSceneChanged_2;

public:
	inline static int32_t get_offset_of_sceneLoaded_0() { return static_cast<int32_t>(offsetof(SceneManager_t793739027_StaticFields, ___sceneLoaded_0)); }
	inline UnityAction_2_t2618073252 * get_sceneLoaded_0() const { return ___sceneLoaded_0; }
	inline UnityAction_2_t2618073252 ** get_address_of_sceneLoaded_0() { return &___sceneLoaded_0; }
	inline void set_sceneLoaded_0(UnityAction_2_t2618073252 * value)
	{
		___sceneLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___sceneLoaded_0), value);
	}

	inline static int32_t get_offset_of_sceneUnloaded_1() { return static_cast<int32_t>(offsetof(SceneManager_t793739027_StaticFields, ___sceneUnloaded_1)); }
	inline UnityAction_1_t3754315317 * get_sceneUnloaded_1() const { return ___sceneUnloaded_1; }
	inline UnityAction_1_t3754315317 ** get_address_of_sceneUnloaded_1() { return &___sceneUnloaded_1; }
	inline void set_sceneUnloaded_1(UnityAction_1_t3754315317 * value)
	{
		___sceneUnloaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___sceneUnloaded_1), value);
	}

	inline static int32_t get_offset_of_activeSceneChanged_2() { return static_cast<int32_t>(offsetof(SceneManager_t793739027_StaticFields, ___activeSceneChanged_2)); }
	inline UnityAction_2_t671528035 * get_activeSceneChanged_2() const { return ___activeSceneChanged_2; }
	inline UnityAction_2_t671528035 ** get_address_of_activeSceneChanged_2() { return &___activeSceneChanged_2; }
	inline void set_activeSceneChanged_2(UnityAction_2_t671528035 * value)
	{
		___activeSceneChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___activeSceneChanged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGER_T793739027_H
#ifndef RESOURCES_T2842622251_H
#define RESOURCES_T2842622251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resources
struct  Resources_t2842622251  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCES_T2842622251_H
#ifndef RANDOM_T2768725925_H
#define RANDOM_T2768725925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Random
struct  Random_t2768725925  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T2768725925_H
#ifndef PROPERTYNAMEUTILS_T791805472_H
#define PROPERTYNAMEUTILS_T791805472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyNameUtils
struct  PropertyNameUtils_t791805472  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMEUTILS_T791805472_H
#ifndef ATTRIBUTE_T3130080784_H
#define ATTRIBUTE_T3130080784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t3130080784  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T3130080784_H
#ifndef STRINGBUILDER_T209334805_H
#define STRINGBUILDER_T209334805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t209334805  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t209334805, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t209334805, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t209334805, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t209334805, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T209334805_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STACKFRAME_T3907017807_H
#define STACKFRAME_T3907017807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.StackFrame
struct  StackFrame_t3907017807  : public RuntimeObject
{
public:
	// System.Int32 System.Diagnostics.StackFrame::ilOffset
	int32_t ___ilOffset_1;
	// System.Int32 System.Diagnostics.StackFrame::nativeOffset
	int32_t ___nativeOffset_2;
	// System.Reflection.MethodBase System.Diagnostics.StackFrame::methodBase
	MethodBase_t3670318294 * ___methodBase_3;
	// System.String System.Diagnostics.StackFrame::fileName
	String_t* ___fileName_4;
	// System.Int32 System.Diagnostics.StackFrame::lineNumber
	int32_t ___lineNumber_5;
	// System.Int32 System.Diagnostics.StackFrame::columnNumber
	int32_t ___columnNumber_6;
	// System.String System.Diagnostics.StackFrame::internalMethodName
	String_t* ___internalMethodName_7;

public:
	inline static int32_t get_offset_of_ilOffset_1() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___ilOffset_1)); }
	inline int32_t get_ilOffset_1() const { return ___ilOffset_1; }
	inline int32_t* get_address_of_ilOffset_1() { return &___ilOffset_1; }
	inline void set_ilOffset_1(int32_t value)
	{
		___ilOffset_1 = value;
	}

	inline static int32_t get_offset_of_nativeOffset_2() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___nativeOffset_2)); }
	inline int32_t get_nativeOffset_2() const { return ___nativeOffset_2; }
	inline int32_t* get_address_of_nativeOffset_2() { return &___nativeOffset_2; }
	inline void set_nativeOffset_2(int32_t value)
	{
		___nativeOffset_2 = value;
	}

	inline static int32_t get_offset_of_methodBase_3() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___methodBase_3)); }
	inline MethodBase_t3670318294 * get_methodBase_3() const { return ___methodBase_3; }
	inline MethodBase_t3670318294 ** get_address_of_methodBase_3() { return &___methodBase_3; }
	inline void set_methodBase_3(MethodBase_t3670318294 * value)
	{
		___methodBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___methodBase_3), value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_4), value);
	}

	inline static int32_t get_offset_of_lineNumber_5() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___lineNumber_5)); }
	inline int32_t get_lineNumber_5() const { return ___lineNumber_5; }
	inline int32_t* get_address_of_lineNumber_5() { return &___lineNumber_5; }
	inline void set_lineNumber_5(int32_t value)
	{
		___lineNumber_5 = value;
	}

	inline static int32_t get_offset_of_columnNumber_6() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___columnNumber_6)); }
	inline int32_t get_columnNumber_6() const { return ___columnNumber_6; }
	inline int32_t* get_address_of_columnNumber_6() { return &___columnNumber_6; }
	inline void set_columnNumber_6(int32_t value)
	{
		___columnNumber_6 = value;
	}

	inline static int32_t get_offset_of_internalMethodName_7() { return static_cast<int32_t>(offsetof(StackFrame_t3907017807, ___internalMethodName_7)); }
	inline String_t* get_internalMethodName_7() const { return ___internalMethodName_7; }
	inline String_t** get_address_of_internalMethodName_7() { return &___internalMethodName_7; }
	inline void set_internalMethodName_7(String_t* value)
	{
		___internalMethodName_7 = value;
		Il2CppCodeGenWriteBarrier((&___internalMethodName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACKFRAME_T3907017807_H
#ifndef SYSTEMINFO_T3070032223_H
#define SYSTEMINFO_T3070032223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemInfo
struct  SystemInfo_t3070032223  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMINFO_T3070032223_H
#ifndef CRITICALFINALIZEROBJECT_T3259952535_H
#define CRITICALFINALIZEROBJECT_T3259952535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t3259952535  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T3259952535_H
#ifndef EVENTARGS_T1957417505_H
#define EVENTARGS_T1957417505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t1957417505  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t1957417505_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t1957417505 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t1957417505_StaticFields, ___Empty_0)); }
	inline EventArgs_t1957417505 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t1957417505 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t1957417505 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T1957417505_H
#ifndef MARSHALBYREFOBJECT_T714030913_H
#define MARSHALBYREFOBJECT_T714030913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t714030913  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1610793130 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t714030913, ____identity_0)); }
	inline ServerIdentity_t1610793130 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1610793130 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1610793130 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T714030913_H
#ifndef UNITYEVENTBASE_T2081553920_H
#define UNITYEVENTBASE_T2081553920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t2081553920  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t3172597217 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3408587689 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_Calls_0)); }
	inline InvokableCallList_t3172597217 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t3172597217 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t3172597217 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3408587689 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3408587689 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3408587689 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t2081553920, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T2081553920_H
#ifndef VALUETYPE_T1108148719_H
#define VALUETYPE_T1108148719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1108148719  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_com
{
};
#endif // VALUETYPE_T1108148719_H
#ifndef SYNCHRONIZATIONCONTEXT_T3011612328_H
#define SYNCHRONIZATIONCONTEXT_T3011612328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SynchronizationContext
struct  SynchronizationContext_t3011612328  : public RuntimeObject
{
public:

public:
};

struct SynchronizationContext_t3011612328_ThreadStaticFields
{
public:
	// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::currentContext
	SynchronizationContext_t3011612328 * ___currentContext_0;

public:
	inline static int32_t get_offset_of_currentContext_0() { return static_cast<int32_t>(offsetof(SynchronizationContext_t3011612328_ThreadStaticFields, ___currentContext_0)); }
	inline SynchronizationContext_t3011612328 * get_currentContext_0() const { return ___currentContext_0; }
	inline SynchronizationContext_t3011612328 ** get_address_of_currentContext_0() { return &___currentContext_0; }
	inline void set_currentContext_0(SynchronizationContext_t3011612328 * value)
	{
		___currentContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZATIONCONTEXT_T3011612328_H
#ifndef PLAYERCONNECTIONINTERNAL_T3959366414_H
#define PLAYERCONNECTIONINTERNAL_T3959366414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayerConnectionInternal
struct  PlayerConnectionInternal_t3959366414  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTIONINTERNAL_T3959366414_H
#ifndef QUEUE_1_T2402182407_H
#define QUEUE_1_T2402182407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  Queue_1_t2402182407  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	WorkRequestU5BU5D_t3277092752* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t2402182407, ____array_0)); }
	inline WorkRequestU5BU5D_t3277092752* get__array_0() const { return ____array_0; }
	inline WorkRequestU5BU5D_t3277092752** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(WorkRequestU5BU5D_t3277092752* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t2402182407, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Queue_1_t2402182407, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(Queue_1_t2402182407, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T2402182407_H
#ifndef TEXTWRITER_T4111495313_H
#define TEXTWRITER_T4111495313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t4111495313  : public RuntimeObject
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t2953840665* ___CoreNewLine_0;

public:
	inline static int32_t get_offset_of_CoreNewLine_0() { return static_cast<int32_t>(offsetof(TextWriter_t4111495313, ___CoreNewLine_0)); }
	inline CharU5BU5D_t2953840665* get_CoreNewLine_0() const { return ___CoreNewLine_0; }
	inline CharU5BU5D_t2953840665** get_address_of_CoreNewLine_0() { return &___CoreNewLine_0; }
	inline void set_CoreNewLine_0(CharU5BU5D_t2953840665* value)
	{
		___CoreNewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_0), value);
	}
};

struct TextWriter_t4111495313_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t4111495313 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t4111495313_StaticFields, ___Null_1)); }
	inline TextWriter_t4111495313 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t4111495313 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t4111495313 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T4111495313_H
#ifndef SERIALIZATIONINFO_T1812684756_H
#define SERIALIZATIONINFO_T1812684756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t1812684756  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t1793686270 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t1797015065 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___serialized_0)); }
	inline Hashtable_t1793686270 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t1793686270 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t1793686270 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___values_1)); }
	inline ArrayList_t1797015065 * get_values_1() const { return ___values_1; }
	inline ArrayList_t1797015065 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t1797015065 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t1812684756, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T1812684756_H
#ifndef UNHANDLEDEXCEPTIONHANDLER_T4246526107_H
#define UNHANDLEDEXCEPTIONHANDLER_T4246526107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnhandledExceptionHandler
struct  UnhandledExceptionHandler_t4246526107  : public RuntimeObject
{
public:

public:
};

struct UnhandledExceptionHandler_t4246526107_StaticFields
{
public:
	// System.UnhandledExceptionEventHandler UnityEngine.UnhandledExceptionHandler::<>f__mg$cache0
	UnhandledExceptionEventHandler_t1143045648 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(UnhandledExceptionHandler_t4246526107_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline UnhandledExceptionEventHandler_t1143045648 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline UnhandledExceptionEventHandler_t1143045648 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(UnhandledExceptionEventHandler_t1143045648 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONHANDLER_T4246526107_H
#ifndef SPRITEATLASMANAGER_T4232368070_H
#define SPRITEATLASMANAGER_T4232368070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager
struct  SpriteAtlasManager_t4232368070  : public RuntimeObject
{
public:

public:
};

struct SpriteAtlasManager_t4232368070_StaticFields
{
public:
	// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback UnityEngine.U2D.SpriteAtlasManager::atlasRequested
	RequestAtlasCallback_t3823435737 * ___atlasRequested_0;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.U2D.SpriteAtlasManager::<>f__mg$cache0
	Action_1_t260058957 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_atlasRequested_0() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t4232368070_StaticFields, ___atlasRequested_0)); }
	inline RequestAtlasCallback_t3823435737 * get_atlasRequested_0() const { return ___atlasRequested_0; }
	inline RequestAtlasCallback_t3823435737 ** get_address_of_atlasRequested_0() { return &___atlasRequested_0; }
	inline void set_atlasRequested_0(RequestAtlasCallback_t3823435737 * value)
	{
		___atlasRequested_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasRequested_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t4232368070_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Action_1_t260058957 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Action_1_t260058957 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Action_1_t260058957 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLASMANAGER_T4232368070_H
#ifndef ENUMERATOR_T3595687596_H
#define ENUMERATOR_T3595687596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform/Enumerator
struct  Enumerator_t3595687596  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityEngine.Transform/Enumerator::outer
	Transform_t3580444445 * ___outer_0;
	// System.Int32 UnityEngine.Transform/Enumerator::currentIndex
	int32_t ___currentIndex_1;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Enumerator_t3595687596, ___outer_0)); }
	inline Transform_t3580444445 * get_outer_0() const { return ___outer_0; }
	inline Transform_t3580444445 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(Transform_t3580444445 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(Enumerator_t3595687596, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3595687596_H
#ifndef TIME_T2552046409_H
#define TIME_T2552046409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Time
struct  Time_t2552046409  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T2552046409_H
#ifndef UNITYSTRING_T3308935321_H
#define UNITYSTRING_T3308935321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityString
struct  UnityString_t3308935321  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSTRING_T3308935321_H
#ifndef PLAYABLEBEHAVIOUR_T716264763_H
#define PLAYABLEBEHAVIOUR_T716264763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t716264763  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T716264763_H
#ifndef YIELDINSTRUCTION_T1540633877_H
#define YIELDINSTRUCTION_T1540633877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t1540633877  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1540633877_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1540633877_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T1540633877_H
#ifndef MESSAGETYPESUBSCRIBERS_T675444414_H
#define MESSAGETYPESUBSCRIBERS_T675444414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct  MessageTypeSubscribers_t675444414  : public RuntimeObject
{
public:
	// System.String UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::m_messageTypeId
	String_t* ___m_messageTypeId_0;
	// System.Int32 UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::subscriberCount
	int32_t ___subscriberCount_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::messageCallback
	MessageEvent_t3114892025 * ___messageCallback_2;

public:
	inline static int32_t get_offset_of_m_messageTypeId_0() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t675444414, ___m_messageTypeId_0)); }
	inline String_t* get_m_messageTypeId_0() const { return ___m_messageTypeId_0; }
	inline String_t** get_address_of_m_messageTypeId_0() { return &___m_messageTypeId_0; }
	inline void set_m_messageTypeId_0(String_t* value)
	{
		___m_messageTypeId_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_messageTypeId_0), value);
	}

	inline static int32_t get_offset_of_subscriberCount_1() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t675444414, ___subscriberCount_1)); }
	inline int32_t get_subscriberCount_1() const { return ___subscriberCount_1; }
	inline int32_t* get_address_of_subscriberCount_1() { return &___subscriberCount_1; }
	inline void set_subscriberCount_1(int32_t value)
	{
		___subscriberCount_1 = value;
	}

	inline static int32_t get_offset_of_messageCallback_2() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t675444414, ___messageCallback_2)); }
	inline MessageEvent_t3114892025 * get_messageCallback_2() const { return ___messageCallback_2; }
	inline MessageEvent_t3114892025 ** get_address_of_messageCallback_2() { return &___messageCallback_2; }
	inline void set_messageCallback_2(MessageEvent_t3114892025 * value)
	{
		___messageCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___messageCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPESUBSCRIBERS_T675444414_H
#ifndef PLAYEREDITORCONNECTIONEVENTS_T1009044977_H
#define PLAYEREDITORCONNECTIONEVENTS_T1009044977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct  PlayerEditorConnectionEvents_t1009044977  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::messageTypeSubscribers
	List_1_t2349647505 * ___messageTypeSubscribers_0;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::connectionEvent
	ConnectionChangeEvent_t190635484 * ___connectionEvent_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::disconnectionEvent
	ConnectionChangeEvent_t190635484 * ___disconnectionEvent_2;

public:
	inline static int32_t get_offset_of_messageTypeSubscribers_0() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t1009044977, ___messageTypeSubscribers_0)); }
	inline List_1_t2349647505 * get_messageTypeSubscribers_0() const { return ___messageTypeSubscribers_0; }
	inline List_1_t2349647505 ** get_address_of_messageTypeSubscribers_0() { return &___messageTypeSubscribers_0; }
	inline void set_messageTypeSubscribers_0(List_1_t2349647505 * value)
	{
		___messageTypeSubscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageTypeSubscribers_0), value);
	}

	inline static int32_t get_offset_of_connectionEvent_1() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t1009044977, ___connectionEvent_1)); }
	inline ConnectionChangeEvent_t190635484 * get_connectionEvent_1() const { return ___connectionEvent_1; }
	inline ConnectionChangeEvent_t190635484 ** get_address_of_connectionEvent_1() { return &___connectionEvent_1; }
	inline void set_connectionEvent_1(ConnectionChangeEvent_t190635484 * value)
	{
		___connectionEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___connectionEvent_1), value);
	}

	inline static int32_t get_offset_of_disconnectionEvent_2() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t1009044977, ___disconnectionEvent_2)); }
	inline ConnectionChangeEvent_t190635484 * get_disconnectionEvent_2() const { return ___disconnectionEvent_2; }
	inline ConnectionChangeEvent_t190635484 ** get_address_of_disconnectionEvent_2() { return &___disconnectionEvent_2; }
	inline void set_disconnectionEvent_2(ConnectionChangeEvent_t190635484 * value)
	{
		___disconnectionEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectionEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYEREDITORCONNECTIONEVENTS_T1009044977_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2953840665* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2953840665* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2953840665** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2953840665* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef MESSAGEEVENTARGS_T2772358345_H
#define MESSAGEEVENTARGS_T2772358345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct  MessageEventArgs_t2772358345  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Networking.PlayerConnection.MessageEventArgs::playerId
	int32_t ___playerId_0;
	// System.Byte[] UnityEngine.Networking.PlayerConnection.MessageEventArgs::data
	ByteU5BU5D_t3567143369* ___data_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2772358345, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2772358345, ___data_1)); }
	inline ByteU5BU5D_t3567143369* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3567143369** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3567143369* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T2772358345_H
#ifndef LIST_1_T2349647505_H
#define LIST_1_T2349647505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct  List_1_t2349647505  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MessageTypeSubscribersU5BU5D_t1032573067* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2349647505, ____items_1)); }
	inline MessageTypeSubscribersU5BU5D_t1032573067* get__items_1() const { return ____items_1; }
	inline MessageTypeSubscribersU5BU5D_t1032573067** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MessageTypeSubscribersU5BU5D_t1032573067* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2349647505, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2349647505, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2349647505_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MessageTypeSubscribersU5BU5D_t1032573067* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2349647505_StaticFields, ___EmptyArray_4)); }
	inline MessageTypeSubscribersU5BU5D_t1032573067* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MessageTypeSubscribersU5BU5D_t1032573067** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MessageTypeSubscribersU5BU5D_t1032573067* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2349647505_H
#ifndef UNITYLOGWRITER_T3161904322_H
#define UNITYLOGWRITER_T3161904322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityLogWriter
struct  UnityLogWriter_t3161904322  : public TextWriter_t4111495313
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYLOGWRITER_T3161904322_H
#ifndef SERIALIZEPRIVATEVARIABLES_T2293400479_H
#define SERIALIZEPRIVATEVARIABLES_T2293400479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializePrivateVariables
struct  SerializePrivateVariables_t2293400479  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEPRIVATEVARIABLES_T2293400479_H
#ifndef SERIALIZEFIELD_T2792443689_H
#define SERIALIZEFIELD_T2792443689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializeField
struct  SerializeField_t2792443689  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEFIELD_T2792443689_H
#ifndef FORMERLYSERIALIZEDASATTRIBUTE_T384831209_H
#define FORMERLYSERIALIZEDASATTRIBUTE_T384831209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t384831209  : public Attribute_t3130080784
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t384831209, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMERLYSERIALIZEDASATTRIBUTE_T384831209_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef HITINFO_T3525137541_H
#define HITINFO_T3525137541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t3525137541 
{
public:
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t2881801184 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t362346687 * ___camera_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(HitInfo_t3525137541, ___target_0)); }
	inline GameObject_t2881801184 * get_target_0() const { return ___target_0; }
	inline GameObject_t2881801184 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t2881801184 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(HitInfo_t3525137541, ___camera_1)); }
	inline Camera_t362346687 * get_camera_1() const { return ___camera_1; }
	inline Camera_t362346687 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t362346687 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t3525137541_marshaled_pinvoke
{
	GameObject_t2881801184 * ___target_0;
	Camera_t362346687 * ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t3525137541_marshaled_com
{
	GameObject_t2881801184 * ___target_0;
	Camera_t362346687 * ___camera_1;
};
#endif // HITINFO_T3525137541_H
#ifndef SELECTIONBASEATTRIBUTE_T867521075_H
#define SELECTIONBASEATTRIBUTE_T867521075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SelectionBaseAttribute
struct  SelectionBaseAttribute_t867521075  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBASEATTRIBUTE_T867521075_H
#ifndef REQUIREDBYNATIVECODEATTRIBUTE_T2936828858_H
#define REQUIREDBYNATIVECODEATTRIBUTE_T2936828858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct  RequiredByNativeCodeAttribute_t2936828858  : public Attribute_t3130080784
{
public:
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t2936828858, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t2936828858, ___U3COptionalU3Ek__BackingField_1)); }
	inline bool get_U3COptionalU3Ek__BackingField_1() const { return ___U3COptionalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_1() { return &___U3COptionalU3Ek__BackingField_1; }
	inline void set_U3COptionalU3Ek__BackingField_1(bool value)
	{
		___U3COptionalU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDBYNATIVECODEATTRIBUTE_T2936828858_H
#ifndef GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T1773994740_H
#define GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T1773994740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct  GeneratedByOldBindingsGeneratorAttribute_t1773994740  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T1773994740_H
#ifndef INT32_T3515577538_H
#define INT32_T3515577538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3515577538 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3515577538, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3515577538_H
#ifndef BYTE_T2640180184_H
#define BYTE_T2640180184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t2640180184 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t2640180184, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T2640180184_H
#ifndef SCENE_T1083318001_H
#define SCENE_T1083318001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t1083318001 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t1083318001, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T1083318001_H
#ifndef WORKREQUEST_T266095197_H
#define WORKREQUEST_T266095197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext/WorkRequest
struct  WorkRequest_t266095197 
{
public:
	// System.Threading.SendOrPostCallback UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateCallback
	SendOrPostCallback_t1528611714 * ___m_DelagateCallback_0;
	// System.Object UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateState
	RuntimeObject * ___m_DelagateState_1;
	// System.Threading.ManualResetEvent UnityEngine.UnitySynchronizationContext/WorkRequest::m_WaitHandle
	ManualResetEvent_t2620507134 * ___m_WaitHandle_2;

public:
	inline static int32_t get_offset_of_m_DelagateCallback_0() { return static_cast<int32_t>(offsetof(WorkRequest_t266095197, ___m_DelagateCallback_0)); }
	inline SendOrPostCallback_t1528611714 * get_m_DelagateCallback_0() const { return ___m_DelagateCallback_0; }
	inline SendOrPostCallback_t1528611714 ** get_address_of_m_DelagateCallback_0() { return &___m_DelagateCallback_0; }
	inline void set_m_DelagateCallback_0(SendOrPostCallback_t1528611714 * value)
	{
		___m_DelagateCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateCallback_0), value);
	}

	inline static int32_t get_offset_of_m_DelagateState_1() { return static_cast<int32_t>(offsetof(WorkRequest_t266095197, ___m_DelagateState_1)); }
	inline RuntimeObject * get_m_DelagateState_1() const { return ___m_DelagateState_1; }
	inline RuntimeObject ** get_address_of_m_DelagateState_1() { return &___m_DelagateState_1; }
	inline void set_m_DelagateState_1(RuntimeObject * value)
	{
		___m_DelagateState_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateState_1), value);
	}

	inline static int32_t get_offset_of_m_WaitHandle_2() { return static_cast<int32_t>(offsetof(WorkRequest_t266095197, ___m_WaitHandle_2)); }
	inline ManualResetEvent_t2620507134 * get_m_WaitHandle_2() const { return ___m_WaitHandle_2; }
	inline ManualResetEvent_t2620507134 ** get_address_of_m_WaitHandle_2() { return &___m_WaitHandle_2; }
	inline void set_m_WaitHandle_2(ManualResetEvent_t2620507134 * value)
	{
		___m_WaitHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaitHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t266095197_marshaled_pinvoke
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t2620507134 * ___m_WaitHandle_2;
};
// Native definition for COM marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t266095197_marshaled_com
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t2620507134 * ___m_WaitHandle_2;
};
#endif // WORKREQUEST_T266095197_H
#ifndef UNITYSYNCHRONIZATIONCONTEXT_T1450698442_H
#define UNITYSYNCHRONIZATIONCONTEXT_T1450698442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext
struct  UnitySynchronizationContext_t1450698442  : public SynchronizationContext_t3011612328
{
public:
	// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest> UnityEngine.UnitySynchronizationContext::m_AsyncWorkQueue
	Queue_1_t2402182407 * ___m_AsyncWorkQueue_1;
	// System.Int32 UnityEngine.UnitySynchronizationContext::m_MainThreadID
	int32_t ___m_MainThreadID_2;

public:
	inline static int32_t get_offset_of_m_AsyncWorkQueue_1() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t1450698442, ___m_AsyncWorkQueue_1)); }
	inline Queue_1_t2402182407 * get_m_AsyncWorkQueue_1() const { return ___m_AsyncWorkQueue_1; }
	inline Queue_1_t2402182407 ** get_address_of_m_AsyncWorkQueue_1() { return &___m_AsyncWorkQueue_1; }
	inline void set_m_AsyncWorkQueue_1(Queue_1_t2402182407 * value)
	{
		___m_AsyncWorkQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncWorkQueue_1), value);
	}

	inline static int32_t get_offset_of_m_MainThreadID_2() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t1450698442, ___m_MainThreadID_2)); }
	inline int32_t get_m_MainThreadID_2() const { return ___m_MainThreadID_2; }
	inline int32_t* get_address_of_m_MainThreadID_2() { return &___m_MainThreadID_2; }
	inline void set_m_MainThreadID_2(int32_t value)
	{
		___m_MainThreadID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSYNCHRONIZATIONCONTEXT_T1450698442_H
#ifndef PARAMETERMODIFIER_T1933193809_H
#define PARAMETERMODIFIER_T1933193809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t1933193809 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t1963761663* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t1933193809, ____byref_0)); }
	inline BooleanU5BU5D_t1963761663* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t1963761663** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t1963761663* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1933193809_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t1933193809_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T1933193809_H
#ifndef VECTOR4_T1900979187_H
#define VECTOR4_T1900979187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t1900979187 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t1900979187, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t1900979187_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t1900979187  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t1900979187  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t1900979187  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t1900979187  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___zeroVector_5)); }
	inline Vector4_t1900979187  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t1900979187 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t1900979187  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___oneVector_6)); }
	inline Vector4_t1900979187  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t1900979187 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t1900979187  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t1900979187  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t1900979187 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t1900979187  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t1900979187_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t1900979187  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t1900979187 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t1900979187  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T1900979187_H
#ifndef RESOLUTION_T3373471800_H
#define RESOLUTION_T3373471800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resolution
struct  Resolution_t3373471800 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t3373471800, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t3373471800, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t3373471800, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T3373471800_H
#ifndef MATRIX4X4_T787966842_H
#define MATRIX4X4_T787966842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t787966842 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t787966842_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t787966842  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t787966842  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t787966842  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t787966842 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t787966842  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t787966842_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t787966842  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t787966842 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t787966842  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T787966842_H
#ifndef UINT32_T1085449242_H
#define UINT32_T1085449242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1085449242 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t1085449242, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1085449242_H
#ifndef TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T1527619309_H
#define TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T1527619309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
struct  TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309 
{
public:
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::keyboardType
	uint32_t ___keyboardType_0;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::autocorrection
	uint32_t ___autocorrection_1;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::multiline
	uint32_t ___multiline_2;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::secure
	uint32_t ___secure_3;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::alert
	uint32_t ___alert_4;

public:
	inline static int32_t get_offset_of_keyboardType_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309, ___keyboardType_0)); }
	inline uint32_t get_keyboardType_0() const { return ___keyboardType_0; }
	inline uint32_t* get_address_of_keyboardType_0() { return &___keyboardType_0; }
	inline void set_keyboardType_0(uint32_t value)
	{
		___keyboardType_0 = value;
	}

	inline static int32_t get_offset_of_autocorrection_1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309, ___autocorrection_1)); }
	inline uint32_t get_autocorrection_1() const { return ___autocorrection_1; }
	inline uint32_t* get_address_of_autocorrection_1() { return &___autocorrection_1; }
	inline void set_autocorrection_1(uint32_t value)
	{
		___autocorrection_1 = value;
	}

	inline static int32_t get_offset_of_multiline_2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309, ___multiline_2)); }
	inline uint32_t get_multiline_2() const { return ___multiline_2; }
	inline uint32_t* get_address_of_multiline_2() { return &___multiline_2; }
	inline void set_multiline_2(uint32_t value)
	{
		___multiline_2 = value;
	}

	inline static int32_t get_offset_of_secure_3() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309, ___secure_3)); }
	inline uint32_t get_secure_3() const { return ___secure_3; }
	inline uint32_t* get_address_of_secure_3() { return &___secure_3; }
	inline void set_secure_3(uint32_t value)
	{
		___secure_3 = value;
	}

	inline static int32_t get_offset_of_alert_4() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309, ___alert_4)); }
	inline uint32_t get_alert_4() const { return ___alert_4; }
	inline uint32_t* get_address_of_alert_4() { return &___alert_4; }
	inline void set_alert_4(uint32_t value)
	{
		___alert_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T1527619309_H
#ifndef THREADANDSERIALIZATIONSAFEATTRIBUTE_T2870195091_H
#define THREADANDSERIALIZATIONSAFEATTRIBUTE_T2870195091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadAndSerializationSafeAttribute
struct  ThreadAndSerializationSafeAttribute_t2870195091  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADANDSERIALIZATIONSAFEATTRIBUTE_T2870195091_H
#ifndef COLOR32_T662424039_H
#define COLOR32_T662424039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t662424039 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t662424039, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T662424039_H
#ifndef COLOR_T1361298052_H
#define COLOR_T1361298052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t1361298052 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T1361298052_H
#ifndef UNHANDLEDEXCEPTIONEVENTARGS_T2712850390_H
#define UNHANDLEDEXCEPTIONEVENTARGS_T2712850390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventArgs
struct  UnhandledExceptionEventArgs_t2712850390  : public EventArgs_t1957417505
{
public:
	// System.Object System.UnhandledExceptionEventArgs::exception
	RuntimeObject * ___exception_1;
	// System.Boolean System.UnhandledExceptionEventArgs::m_isTerminating
	bool ___m_isTerminating_2;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t2712850390, ___exception_1)); }
	inline RuntimeObject * get_exception_1() const { return ___exception_1; }
	inline RuntimeObject ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(RuntimeObject * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}

	inline static int32_t get_offset_of_m_isTerminating_2() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t2712850390, ___m_isTerminating_2)); }
	inline bool get_m_isTerminating_2() const { return ___m_isTerminating_2; }
	inline bool* get_address_of_m_isTerminating_2() { return &___m_isTerminating_2; }
	inline void set_m_isTerminating_2(bool value)
	{
		___m_isTerminating_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTARGS_T2712850390_H
#ifndef METHODBASE_T3670318294_H
#define METHODBASE_T3670318294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t3670318294  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T3670318294_H
#ifndef UNITYEVENT_1_T1459204717_H
#define UNITYEVENT_1_T1459204717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityEvent_1_t1459204717  : public UnityEventBase_t2081553920
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3385344125* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1459204717, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3385344125* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3385344125** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3385344125* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1459204717_H
#ifndef CHAR_T1622636488_H
#define CHAR_T1622636488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t1622636488 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t1622636488, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t1622636488_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t1622636488_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T1622636488_H
#ifndef UNITYEVENT_1_T2202423910_H
#define UNITYEVENT_1_T2202423910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2202423910  : public UnityEventBase_t2081553920
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3385344125* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2202423910, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3385344125* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3385344125** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3385344125* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2202423910_H
#ifndef VECTOR3_T3932393085_H
#define VECTOR3_T3932393085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3932393085 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3932393085_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3932393085  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3932393085  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3932393085  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3932393085  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3932393085  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3932393085  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3932393085  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3932393085  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3932393085  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3932393085  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3932393085  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3932393085 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3932393085  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___oneVector_5)); }
	inline Vector3_t3932393085  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3932393085 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3932393085  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___upVector_6)); }
	inline Vector3_t3932393085  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3932393085 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3932393085  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___downVector_7)); }
	inline Vector3_t3932393085  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3932393085 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3932393085  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___leftVector_8)); }
	inline Vector3_t3932393085  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3932393085 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3932393085  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___rightVector_9)); }
	inline Vector3_t3932393085  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3932393085 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3932393085  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3932393085  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3932393085 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3932393085  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___backVector_11)); }
	inline Vector3_t3932393085  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3932393085 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3932393085  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3932393085  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3932393085 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3932393085  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3932393085  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3932393085 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3932393085  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3932393085_H
#ifndef QUATERNION_T3497065016_H
#define QUATERNION_T3497065016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t3497065016 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t3497065016_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t3497065016  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t3497065016  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t3497065016 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t3497065016  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T3497065016_H
#ifndef SINGLE_T897798503_H
#define SINGLE_T897798503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t897798503 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t897798503, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T897798503_H
#ifndef UNITYEXCEPTION_T2602360046_H
#define UNITYEXCEPTION_T2602360046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityException
struct  UnityException_t2602360046  : public Exception_t214279536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXCEPTION_T2602360046_H
#ifndef SORTINGLAYER_T447911540_H
#define SORTINGLAYER_T447911540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SortingLayer
struct  SortingLayer_t447911540 
{
public:
	// System.Int32 UnityEngine.SortingLayer::m_Id
	int32_t ___m_Id_0;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(SortingLayer_t447911540, ___m_Id_0)); }
	inline int32_t get_m_Id_0() const { return ___m_Id_0; }
	inline int32_t* get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(int32_t value)
	{
		___m_Id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYER_T447911540_H
#ifndef REQUIRECOMPONENT_T3219866953_H
#define REQUIRECOMPONENT_T3219866953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t3219866953  : public Attribute_t3130080784
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t3219866953, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t3219866953, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t3219866953, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T3219866953_H
#ifndef USEDBYNATIVECODEATTRIBUTE_T2845240971_H
#define USEDBYNATIVECODEATTRIBUTE_T2845240971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct  UsedByNativeCodeAttribute_t2845240971  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEDBYNATIVECODEATTRIBUTE_T2845240971_H
#ifndef BOOLEAN_T2440219994_H
#define BOOLEAN_T2440219994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2440219994 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2440219994, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2440219994_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2440219994_H
#ifndef PROPERTYNAME_T3787932763_H
#define PROPERTYNAME_T3787932763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t3787932763 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t3787932763, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T3787932763_H
#ifndef SYSTEMEXCEPTION_T1379941789_H
#define SYSTEMEXCEPTION_T1379941789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t1379941789  : public Exception_t214279536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T1379941789_H
#ifndef PROPERTYATTRIBUTE_T4289523592_H
#define PROPERTYATTRIBUTE_T4289523592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4289523592  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T4289523592_H
#ifndef PREFERBINARYSERIALIZATION_T665869339_H
#define PREFERBINARYSERIALIZATION_T665869339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PreferBinarySerialization
struct  PreferBinarySerialization_t665869339  : public Attribute_t3130080784
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERBINARYSERIALIZATION_T665869339_H
#ifndef DOUBLE_T3942828892_H
#define DOUBLE_T3942828892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t3942828892 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t3942828892, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T3942828892_H
#ifndef ENUM_T2432427458_H
#define ENUM_T2432427458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2432427458  : public ValueType_t1108148719
{
public:

public:
};

struct Enum_t2432427458_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2953840665* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2432427458_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2953840665* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2953840665** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2953840665* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2432427458_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2432427458_marshaled_com
{
};
#endif // ENUM_T2432427458_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2272071576 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2272071576 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2272071576 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2272071576 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2272071576 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2272071576 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2272071576 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2272071576 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef VOID_T4071739332_H
#define VOID_T4071739332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t4071739332 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T4071739332_H
#ifndef RANGEINT_T21894369_H
#define RANGEINT_T21894369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeInt
struct  RangeInt_t21894369 
{
public:
	// System.Int32 UnityEngine.RangeInt::start
	int32_t ___start_0;
	// System.Int32 UnityEngine.RangeInt::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RangeInt_t21894369, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(RangeInt_t21894369, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEINT_T21894369_H
#ifndef RECT_T952252086_H
#define RECT_T952252086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t952252086 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T952252086_H
#ifndef VECTOR2_T2246369278_H
#define VECTOR2_T2246369278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2246369278 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2246369278_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2246369278  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2246369278  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2246369278  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2246369278  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2246369278  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2246369278  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2246369278  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2246369278  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2246369278  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2246369278 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2246369278  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___oneVector_3)); }
	inline Vector2_t2246369278  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2246369278 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2246369278  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___upVector_4)); }
	inline Vector2_t2246369278  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2246369278 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2246369278  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___downVector_5)); }
	inline Vector2_t2246369278  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2246369278 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2246369278  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___leftVector_6)); }
	inline Vector2_t2246369278  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2246369278 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2246369278  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___rightVector_7)); }
	inline Vector2_t2246369278  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2246369278 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2246369278  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2246369278  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2246369278 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2246369278  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2246369278  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2246369278 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2246369278  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2246369278_H
#ifndef UINTPTR_T_H
#define UINTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UIntPtr
struct  UIntPtr_t 
{
public:
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;

public:
	inline static int32_t get_offset_of__pointer_1() { return static_cast<int32_t>(offsetof(UIntPtr_t, ____pointer_1)); }
	inline void* get__pointer_1() const { return ____pointer_1; }
	inline void** get_address_of__pointer_1() { return &____pointer_1; }
	inline void set__pointer_1(void* value)
	{
		____pointer_1 = value;
	}
};

struct UIntPtr_t_StaticFields
{
public:
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(UIntPtr_t_StaticFields, ___Zero_0)); }
	inline uintptr_t get_Zero_0() const { return ___Zero_0; }
	inline uintptr_t* get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(uintptr_t value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTR_T_H
#ifndef OBJECT_T3645472222_H
#define OBJECT_T3645472222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t3645472222  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t3645472222, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t3645472222_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t3645472222_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t3645472222_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t3645472222_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T3645472222_H
#ifndef PRINCIPALPOLICY_T666315016_H
#define PRINCIPALPOLICY_T666315016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.PrincipalPolicy
struct  PrincipalPolicy_t666315016 
{
public:
	// System.Int32 System.Security.Principal.PrincipalPolicy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrincipalPolicy_t666315016, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINCIPALPOLICY_T666315016_H
#ifndef PLAYABLEHANDLE_T1495371730_H
#define PLAYABLEHANDLE_T1495371730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1495371730 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1495371730, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1495371730, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1495371730_H
#ifndef RECTOFFSET_T817686154_H
#define RECTOFFSET_T817686154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t817686154  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t817686154, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t817686154, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t817686154_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t817686154_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T817686154_H
#ifndef RANGEATTRIBUTE_T2306247939_H
#define RANGEATTRIBUTE_T2306247939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeAttribute
struct  RangeAttribute_t2306247939  : public PropertyAttribute_t4289523592
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t2306247939, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t2306247939, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEATTRIBUTE_T2306247939_H
#ifndef U3CADDANDCREATEU3EC__ANONSTOREY1_T3362117636_H
#define U3CADDANDCREATEU3EC__ANONSTOREY1_T3362117636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1
struct  U3CAddAndCreateU3Ec__AnonStorey1_t3362117636  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::messageId
	Guid_t  ___messageId_0;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(U3CAddAndCreateU3Ec__AnonStorey1_t3362117636, ___messageId_0)); }
	inline Guid_t  get_messageId_0() const { return ___messageId_0; }
	inline Guid_t * get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(Guid_t  value)
	{
		___messageId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDANDCREATEU3EC__ANONSTOREY1_T3362117636_H
#ifndef FILTERMODE_T327170756_H
#define FILTERMODE_T327170756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t327170756 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t327170756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T327170756_H
#ifndef TEXTAREAATTRIBUTE_T2141263104_H
#define TEXTAREAATTRIBUTE_T2141263104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAreaAttribute
struct  TextAreaAttribute_t2141263104  : public PropertyAttribute_t4289523592
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t2141263104, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t2141263104, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTAREAATTRIBUTE_T2141263104_H
#ifndef PARAMETERATTRIBUTES_T3508466278_H
#define PARAMETERATTRIBUTES_T3508466278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_t3508466278 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParameterAttributes_t3508466278, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_T3508466278_H
#ifndef TEXTUREWRAPMODE_T2082338105_H
#define TEXTUREWRAPMODE_T2082338105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t2082338105 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t2082338105, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T2082338105_H
#ifndef TEXTUREFORMAT_T2463160273_H
#define TEXTUREFORMAT_T2463160273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2463160273 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2463160273, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2463160273_H
#ifndef STREAMINGCONTEXTSTATES_T2000903969_H
#define STREAMINGCONTEXTSTATES_T2000903969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t2000903969 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t2000903969, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T2000903969_H
#ifndef TOOLTIPATTRIBUTE_T3230141494_H
#define TOOLTIPATTRIBUTE_T3230141494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t3230141494  : public PropertyAttribute_t4289523592
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t3230141494, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPATTRIBUTE_T3230141494_H
#ifndef TOUCHPHASE_T616769899_H
#define TOUCHPHASE_T616769899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t616769899 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t616769899, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T616769899_H
#ifndef TOUCHTYPE_T3178845108_H
#define TOUCHTYPE_T3178845108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t3178845108 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t3178845108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T3178845108_H
#ifndef TOUCHSCREENKEYBOARD_T1683994280_H
#define TOUCHSCREENKEYBOARD_T1683994280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard
struct  TouchScreenKeyboard_t1683994280  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TouchScreenKeyboard::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t1683994280, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_T1683994280_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1103124205_H
#define TOUCHSCREENKEYBOARDTYPE_T1103124205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1103124205 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1103124205, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1103124205_H
#ifndef PLAYABLEOUTPUTHANDLE_T1247041817_H
#define PLAYABLEOUTPUTHANDLE_T1247041817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t1247041817 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t1247041817, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t1247041817, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T1247041817_H
#ifndef THREADSTATE_T3792119748_H
#define THREADSTATE_T3792119748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadState
struct  ThreadState_t3792119748 
{
public:
	// System.Int32 System.Threading.ThreadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadState_t3792119748, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTATE_T3792119748_H
#ifndef TRACKEDREFERENCE_T2612049617_H
#define TRACKEDREFERENCE_T2612049617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t2612049617  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t2612049617, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t2612049617_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t2612049617_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T2612049617_H
#ifndef MESSAGEEVENT_T3114892025_H
#define MESSAGEEVENT_T3114892025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct  MessageEvent_t3114892025  : public UnityEvent_1_t1459204717
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENT_T3114892025_H
#ifndef U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T992766445_H
#define U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T992766445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct  U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::messageId
	Guid_t  ___messageId_0;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445, ___messageId_0)); }
	inline Guid_t  get_messageId_0() const { return ___messageId_0; }
	inline Guid_t * get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(Guid_t  value)
	{
		___messageId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T992766445_H
#ifndef PLAYABLEGRAPH_T394364097_H
#define PLAYABLEGRAPH_T394364097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t394364097 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t394364097, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t394364097, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T394364097_H
#ifndef SPACEATTRIBUTE_T586498270_H
#define SPACEATTRIBUTE_T586498270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t586498270  : public PropertyAttribute_t4289523592
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t586498270, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEATTRIBUTE_T586498270_H
#ifndef CUBEMAPFACE_T923220006_H
#define CUBEMAPFACE_T923220006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CubemapFace
struct  CubemapFace_t923220006 
{
public:
	// System.Int32 UnityEngine.CubemapFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CubemapFace_t923220006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUBEMAPFACE_T923220006_H
#ifndef DATASTREAMTYPE_T4072303447_H
#define DATASTREAMTYPE_T4072303447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t4072303447 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t4072303447, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T4072303447_H
#ifndef RUNTIMEPLATFORM_T1521016828_H
#define RUNTIMEPLATFORM_T1521016828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t1521016828 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t1521016828, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T1521016828_H
#ifndef LOADSCENEMODE_T3029863218_H
#define LOADSCENEMODE_T3029863218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t3029863218 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t3029863218, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T3029863218_H
#ifndef COMPAREFUNCTION_T2874777320_H
#define COMPAREFUNCTION_T2874777320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_t2874777320 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareFunction_t2874777320, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPAREFUNCTION_T2874777320_H
#ifndef RAY_T1807385980_H
#define RAY_T1807385980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1807385980 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3932393085  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3932393085  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1807385980, ___m_Origin_0)); }
	inline Vector3_t3932393085  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3932393085 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3932393085  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1807385980, ___m_Direction_1)); }
	inline Vector3_t3932393085  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3932393085 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3932393085  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1807385980_H
#ifndef PLANE_T4100015541_H
#define PLANE_T4100015541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t4100015541 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3932393085  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t4100015541, ___m_Normal_0)); }
	inline Vector3_t3932393085  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3932393085 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3932393085  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t4100015541, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T4100015541_H
#ifndef COMMANDBUFFER_T3383400165_H
#define COMMANDBUFFER_T3383400165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t3383400165  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t3383400165, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T3383400165_H
#ifndef COLORWRITEMASK_T3142121968_H
#define COLORWRITEMASK_T3142121968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ColorWriteMask
struct  ColorWriteMask_t3142121968 
{
public:
	// System.Int32 UnityEngine.Rendering.ColorWriteMask::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorWriteMask_t3142121968, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWRITEMASK_T3142121968_H
#ifndef CAMERAEVENT_T1403981775_H
#define CAMERAEVENT_T1403981775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t1403981775 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t1403981775, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T1403981775_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T3659678378_H
#define INDEXOUTOFRANGEEXCEPTION_T3659678378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t3659678378  : public SystemException_t1379941789
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T3659678378_H
#ifndef SENDMESSAGEOPTIONS_T4227618630_H
#define SENDMESSAGEOPTIONS_T4227618630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t4227618630 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t4227618630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T4227618630_H
#ifndef OPERATINGSYSTEMFAMILY_T597865442_H
#define OPERATINGSYSTEMFAMILY_T597865442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_t597865442 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_t597865442, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGSYSTEMFAMILY_T597865442_H
#ifndef WAITHANDLE_T2040275096_H
#define WAITHANDLE_T2040275096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.WaitHandle
struct  WaitHandle_t2040275096  : public MarshalByRefObject_t714030913
{
public:
	// Microsoft.Win32.SafeHandles.SafeWaitHandle System.Threading.WaitHandle::safe_wait_handle
	SafeWaitHandle_t1581260623 * ___safe_wait_handle_2;
	// System.Boolean System.Threading.WaitHandle::disposed
	bool ___disposed_4;

public:
	inline static int32_t get_offset_of_safe_wait_handle_2() { return static_cast<int32_t>(offsetof(WaitHandle_t2040275096, ___safe_wait_handle_2)); }
	inline SafeWaitHandle_t1581260623 * get_safe_wait_handle_2() const { return ___safe_wait_handle_2; }
	inline SafeWaitHandle_t1581260623 ** get_address_of_safe_wait_handle_2() { return &___safe_wait_handle_2; }
	inline void set_safe_wait_handle_2(SafeWaitHandle_t1581260623 * value)
	{
		___safe_wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___safe_wait_handle_2), value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(WaitHandle_t2040275096, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}
};

struct WaitHandle_t2040275096_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	intptr_t ___InvalidHandle_3;

public:
	inline static int32_t get_offset_of_InvalidHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_t2040275096_StaticFields, ___InvalidHandle_3)); }
	inline intptr_t get_InvalidHandle_3() const { return ___InvalidHandle_3; }
	inline intptr_t* get_address_of_InvalidHandle_3() { return &___InvalidHandle_3; }
	inline void set_InvalidHandle_3(intptr_t value)
	{
		___InvalidHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITHANDLE_T2040275096_H
#ifndef MATERIALPROPERTYBLOCK_T3731058050_H
#define MATERIALPROPERTYBLOCK_T3731058050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MaterialPropertyBlock
struct  MaterialPropertyBlock_t3731058050  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t3731058050, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYBLOCK_T3731058050_H
#ifndef DELEGATE_T4015201940_H
#define DELEGATE_T4015201940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t4015201940  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t2468626509 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___data_8)); }
	inline DelegateData_t2468626509 * get_data_8() const { return ___data_8; }
	inline DelegateData_t2468626509 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t2468626509 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T4015201940_H
#ifndef HIDEFLAGS_T2048557387_H
#define HIDEFLAGS_T2048557387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t2048557387 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t2048557387, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T2048557387_H
#ifndef ASYNCOPERATION_T3797817720_H
#define ASYNCOPERATION_T3797817720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t3797817720  : public YieldInstruction_t1540633877
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t1275866152 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t3797817720, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t3797817720, ___m_completeCallback_1)); }
	inline Action_1_t1275866152 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t1275866152 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t1275866152 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3797817720_marshaled_pinvoke : public YieldInstruction_t1540633877_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3797817720_marshaled_com : public YieldInstruction_t1540633877_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T3797817720_H
#ifndef RUNTIMETYPEHANDLE_T1986816146_H
#define RUNTIMETYPEHANDLE_T1986816146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t1986816146 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t1986816146, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T1986816146_H
#ifndef BINDINGFLAGS_T100712108_H
#define BINDINGFLAGS_T100712108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t100712108 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t100712108, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T100712108_H
#ifndef BUILTINRENDERTEXTURETYPE_T3735833062_H
#define BUILTINRENDERTEXTURETYPE_T3735833062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t3735833062 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t3735833062, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T3735833062_H
#ifndef STENCILOP_T1846058230_H
#define STENCILOP_T1846058230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.StencilOp
struct  StencilOp_t1846058230 
{
public:
	// System.Int32 UnityEngine.Rendering.StencilOp::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StencilOp_t1846058230, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILOP_T1846058230_H
#ifndef ARGUMENTEXCEPTION_T489606696_H
#define ARGUMENTEXCEPTION_T489606696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t489606696  : public SystemException_t1379941789
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t489606696, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T489606696_H
#ifndef EDGE_T1816009797_H
#define EDGE_T1816009797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/Edge
struct  Edge_t1816009797 
{
public:
	// System.Int32 UnityEngine.RectTransform/Edge::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Edge_t1816009797, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T1816009797_H
#ifndef AXIS_T1970359846_H
#define AXIS_T1970359846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/Axis
struct  Axis_t1970359846 
{
public:
	// System.Int32 UnityEngine.RectTransform/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1970359846, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1970359846_H
#ifndef CAMERACLEARFLAGS_T3846301771_H
#define CAMERACLEARFLAGS_T3846301771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t3846301771 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t3846301771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T3846301771_H
#ifndef CONNECTIONCHANGEEVENT_T190635484_H
#define CONNECTIONCHANGEEVENT_T190635484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct  ConnectionChangeEvent_t190635484  : public UnityEvent_1_t2202423910
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCHANGEEVENT_T190635484_H
#ifndef MULTICASTDELEGATE_T599438524_H
#define MULTICASTDELEGATE_T599438524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t599438524  : public Delegate_t4015201940
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t599438524 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t599438524 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t599438524, ___prev_9)); }
	inline MulticastDelegate_t599438524 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t599438524 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t599438524 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t599438524, ___kpm_next_10)); }
	inline MulticastDelegate_t599438524 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t599438524 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t599438524 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T599438524_H
#ifndef STREAMINGCONTEXT_T1986025897_H
#define STREAMINGCONTEXT_T1986025897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t1986025897 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t1986025897, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t1986025897, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1986025897_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t1986025897_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T1986025897_H
#ifndef EVENTWAITHANDLE_T1398859231_H
#define EVENTWAITHANDLE_T1398859231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.EventWaitHandle
struct  EventWaitHandle_t1398859231  : public WaitHandle_t2040275096
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTWAITHANDLE_T1398859231_H
#ifndef APPDOMAIN_T2391992810_H
#define APPDOMAIN_T2391992810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppDomain
struct  AppDomain_t2391992810  : public MarshalByRefObject_t714030913
{
public:
	// System.IntPtr System.AppDomain::_mono_app_domain
	intptr_t ____mono_app_domain_1;
	// System.Security.Policy.Evidence System.AppDomain::_evidence
	Evidence_t1538274723 * ____evidence_6;
	// System.Security.PermissionSet System.AppDomain::_granted
	PermissionSet_t448394360 * ____granted_7;
	// System.Security.Principal.PrincipalPolicy System.AppDomain::_principalPolicy
	int32_t ____principalPolicy_8;
	// System.AppDomainManager System.AppDomain::_domain_manager
	AppDomainManager_t3746511876 * ____domain_manager_11;
	// System.ActivationContext System.AppDomain::_activation
	ActivationContext_t711302796 * ____activation_12;
	// System.ApplicationIdentity System.AppDomain::_applicationIdentity
	ApplicationIdentity_t1182042815 * ____applicationIdentity_13;
	// System.AssemblyLoadEventHandler System.AppDomain::AssemblyLoad
	AssemblyLoadEventHandler_t379728391 * ___AssemblyLoad_14;
	// System.ResolveEventHandler System.AppDomain::AssemblyResolve
	ResolveEventHandler_t3534413881 * ___AssemblyResolve_15;
	// System.EventHandler System.AppDomain::DomainUnload
	EventHandler_t1089632771 * ___DomainUnload_16;
	// System.EventHandler System.AppDomain::ProcessExit
	EventHandler_t1089632771 * ___ProcessExit_17;
	// System.ResolveEventHandler System.AppDomain::ResourceResolve
	ResolveEventHandler_t3534413881 * ___ResourceResolve_18;
	// System.ResolveEventHandler System.AppDomain::TypeResolve
	ResolveEventHandler_t3534413881 * ___TypeResolve_19;
	// System.UnhandledExceptionEventHandler System.AppDomain::UnhandledException
	UnhandledExceptionEventHandler_t1143045648 * ___UnhandledException_20;
	// System.ResolveEventHandler System.AppDomain::ReflectionOnlyAssemblyResolve
	ResolveEventHandler_t3534413881 * ___ReflectionOnlyAssemblyResolve_21;

public:
	inline static int32_t get_offset_of__mono_app_domain_1() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____mono_app_domain_1)); }
	inline intptr_t get__mono_app_domain_1() const { return ____mono_app_domain_1; }
	inline intptr_t* get_address_of__mono_app_domain_1() { return &____mono_app_domain_1; }
	inline void set__mono_app_domain_1(intptr_t value)
	{
		____mono_app_domain_1 = value;
	}

	inline static int32_t get_offset_of__evidence_6() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____evidence_6)); }
	inline Evidence_t1538274723 * get__evidence_6() const { return ____evidence_6; }
	inline Evidence_t1538274723 ** get_address_of__evidence_6() { return &____evidence_6; }
	inline void set__evidence_6(Evidence_t1538274723 * value)
	{
		____evidence_6 = value;
		Il2CppCodeGenWriteBarrier((&____evidence_6), value);
	}

	inline static int32_t get_offset_of__granted_7() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____granted_7)); }
	inline PermissionSet_t448394360 * get__granted_7() const { return ____granted_7; }
	inline PermissionSet_t448394360 ** get_address_of__granted_7() { return &____granted_7; }
	inline void set__granted_7(PermissionSet_t448394360 * value)
	{
		____granted_7 = value;
		Il2CppCodeGenWriteBarrier((&____granted_7), value);
	}

	inline static int32_t get_offset_of__principalPolicy_8() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____principalPolicy_8)); }
	inline int32_t get__principalPolicy_8() const { return ____principalPolicy_8; }
	inline int32_t* get_address_of__principalPolicy_8() { return &____principalPolicy_8; }
	inline void set__principalPolicy_8(int32_t value)
	{
		____principalPolicy_8 = value;
	}

	inline static int32_t get_offset_of__domain_manager_11() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____domain_manager_11)); }
	inline AppDomainManager_t3746511876 * get__domain_manager_11() const { return ____domain_manager_11; }
	inline AppDomainManager_t3746511876 ** get_address_of__domain_manager_11() { return &____domain_manager_11; }
	inline void set__domain_manager_11(AppDomainManager_t3746511876 * value)
	{
		____domain_manager_11 = value;
		Il2CppCodeGenWriteBarrier((&____domain_manager_11), value);
	}

	inline static int32_t get_offset_of__activation_12() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____activation_12)); }
	inline ActivationContext_t711302796 * get__activation_12() const { return ____activation_12; }
	inline ActivationContext_t711302796 ** get_address_of__activation_12() { return &____activation_12; }
	inline void set__activation_12(ActivationContext_t711302796 * value)
	{
		____activation_12 = value;
		Il2CppCodeGenWriteBarrier((&____activation_12), value);
	}

	inline static int32_t get_offset_of__applicationIdentity_13() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ____applicationIdentity_13)); }
	inline ApplicationIdentity_t1182042815 * get__applicationIdentity_13() const { return ____applicationIdentity_13; }
	inline ApplicationIdentity_t1182042815 ** get_address_of__applicationIdentity_13() { return &____applicationIdentity_13; }
	inline void set__applicationIdentity_13(ApplicationIdentity_t1182042815 * value)
	{
		____applicationIdentity_13 = value;
		Il2CppCodeGenWriteBarrier((&____applicationIdentity_13), value);
	}

	inline static int32_t get_offset_of_AssemblyLoad_14() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___AssemblyLoad_14)); }
	inline AssemblyLoadEventHandler_t379728391 * get_AssemblyLoad_14() const { return ___AssemblyLoad_14; }
	inline AssemblyLoadEventHandler_t379728391 ** get_address_of_AssemblyLoad_14() { return &___AssemblyLoad_14; }
	inline void set_AssemblyLoad_14(AssemblyLoadEventHandler_t379728391 * value)
	{
		___AssemblyLoad_14 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyLoad_14), value);
	}

	inline static int32_t get_offset_of_AssemblyResolve_15() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___AssemblyResolve_15)); }
	inline ResolveEventHandler_t3534413881 * get_AssemblyResolve_15() const { return ___AssemblyResolve_15; }
	inline ResolveEventHandler_t3534413881 ** get_address_of_AssemblyResolve_15() { return &___AssemblyResolve_15; }
	inline void set_AssemblyResolve_15(ResolveEventHandler_t3534413881 * value)
	{
		___AssemblyResolve_15 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyResolve_15), value);
	}

	inline static int32_t get_offset_of_DomainUnload_16() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___DomainUnload_16)); }
	inline EventHandler_t1089632771 * get_DomainUnload_16() const { return ___DomainUnload_16; }
	inline EventHandler_t1089632771 ** get_address_of_DomainUnload_16() { return &___DomainUnload_16; }
	inline void set_DomainUnload_16(EventHandler_t1089632771 * value)
	{
		___DomainUnload_16 = value;
		Il2CppCodeGenWriteBarrier((&___DomainUnload_16), value);
	}

	inline static int32_t get_offset_of_ProcessExit_17() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___ProcessExit_17)); }
	inline EventHandler_t1089632771 * get_ProcessExit_17() const { return ___ProcessExit_17; }
	inline EventHandler_t1089632771 ** get_address_of_ProcessExit_17() { return &___ProcessExit_17; }
	inline void set_ProcessExit_17(EventHandler_t1089632771 * value)
	{
		___ProcessExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___ProcessExit_17), value);
	}

	inline static int32_t get_offset_of_ResourceResolve_18() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___ResourceResolve_18)); }
	inline ResolveEventHandler_t3534413881 * get_ResourceResolve_18() const { return ___ResourceResolve_18; }
	inline ResolveEventHandler_t3534413881 ** get_address_of_ResourceResolve_18() { return &___ResourceResolve_18; }
	inline void set_ResourceResolve_18(ResolveEventHandler_t3534413881 * value)
	{
		___ResourceResolve_18 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceResolve_18), value);
	}

	inline static int32_t get_offset_of_TypeResolve_19() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___TypeResolve_19)); }
	inline ResolveEventHandler_t3534413881 * get_TypeResolve_19() const { return ___TypeResolve_19; }
	inline ResolveEventHandler_t3534413881 ** get_address_of_TypeResolve_19() { return &___TypeResolve_19; }
	inline void set_TypeResolve_19(ResolveEventHandler_t3534413881 * value)
	{
		___TypeResolve_19 = value;
		Il2CppCodeGenWriteBarrier((&___TypeResolve_19), value);
	}

	inline static int32_t get_offset_of_UnhandledException_20() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___UnhandledException_20)); }
	inline UnhandledExceptionEventHandler_t1143045648 * get_UnhandledException_20() const { return ___UnhandledException_20; }
	inline UnhandledExceptionEventHandler_t1143045648 ** get_address_of_UnhandledException_20() { return &___UnhandledException_20; }
	inline void set_UnhandledException_20(UnhandledExceptionEventHandler_t1143045648 * value)
	{
		___UnhandledException_20 = value;
		Il2CppCodeGenWriteBarrier((&___UnhandledException_20), value);
	}

	inline static int32_t get_offset_of_ReflectionOnlyAssemblyResolve_21() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810, ___ReflectionOnlyAssemblyResolve_21)); }
	inline ResolveEventHandler_t3534413881 * get_ReflectionOnlyAssemblyResolve_21() const { return ___ReflectionOnlyAssemblyResolve_21; }
	inline ResolveEventHandler_t3534413881 ** get_address_of_ReflectionOnlyAssemblyResolve_21() { return &___ReflectionOnlyAssemblyResolve_21; }
	inline void set_ReflectionOnlyAssemblyResolve_21(ResolveEventHandler_t3534413881 * value)
	{
		___ReflectionOnlyAssemblyResolve_21 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionOnlyAssemblyResolve_21), value);
	}
};

struct AppDomain_t2391992810_StaticFields
{
public:
	// System.String System.AppDomain::_process_guid
	String_t* ____process_guid_2;
	// System.AppDomain System.AppDomain::default_domain
	AppDomain_t2391992810 * ___default_domain_10;

public:
	inline static int32_t get_offset_of__process_guid_2() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810_StaticFields, ____process_guid_2)); }
	inline String_t* get__process_guid_2() const { return ____process_guid_2; }
	inline String_t** get_address_of__process_guid_2() { return &____process_guid_2; }
	inline void set__process_guid_2(String_t* value)
	{
		____process_guid_2 = value;
		Il2CppCodeGenWriteBarrier((&____process_guid_2), value);
	}

	inline static int32_t get_offset_of_default_domain_10() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810_StaticFields, ___default_domain_10)); }
	inline AppDomain_t2391992810 * get_default_domain_10() const { return ___default_domain_10; }
	inline AppDomain_t2391992810 ** get_address_of_default_domain_10() { return &___default_domain_10; }
	inline void set_default_domain_10(AppDomain_t2391992810 * value)
	{
		___default_domain_10 = value;
		Il2CppCodeGenWriteBarrier((&___default_domain_10), value);
	}
};

struct AppDomain_t2391992810_ThreadStaticFields
{
public:
	// System.Collections.Hashtable System.AppDomain::type_resolve_in_progress
	Hashtable_t1793686270 * ___type_resolve_in_progress_3;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress
	Hashtable_t1793686270 * ___assembly_resolve_in_progress_4;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress_refonly
	Hashtable_t1793686270 * ___assembly_resolve_in_progress_refonly_5;
	// System.Security.Principal.IPrincipal System.AppDomain::_principal
	RuntimeObject* ____principal_9;

public:
	inline static int32_t get_offset_of_type_resolve_in_progress_3() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810_ThreadStaticFields, ___type_resolve_in_progress_3)); }
	inline Hashtable_t1793686270 * get_type_resolve_in_progress_3() const { return ___type_resolve_in_progress_3; }
	inline Hashtable_t1793686270 ** get_address_of_type_resolve_in_progress_3() { return &___type_resolve_in_progress_3; }
	inline void set_type_resolve_in_progress_3(Hashtable_t1793686270 * value)
	{
		___type_resolve_in_progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_resolve_in_progress_3), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_4() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810_ThreadStaticFields, ___assembly_resolve_in_progress_4)); }
	inline Hashtable_t1793686270 * get_assembly_resolve_in_progress_4() const { return ___assembly_resolve_in_progress_4; }
	inline Hashtable_t1793686270 ** get_address_of_assembly_resolve_in_progress_4() { return &___assembly_resolve_in_progress_4; }
	inline void set_assembly_resolve_in_progress_4(Hashtable_t1793686270 * value)
	{
		___assembly_resolve_in_progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_4), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_refonly_5() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810_ThreadStaticFields, ___assembly_resolve_in_progress_refonly_5)); }
	inline Hashtable_t1793686270 * get_assembly_resolve_in_progress_refonly_5() const { return ___assembly_resolve_in_progress_refonly_5; }
	inline Hashtable_t1793686270 ** get_address_of_assembly_resolve_in_progress_refonly_5() { return &___assembly_resolve_in_progress_refonly_5; }
	inline void set_assembly_resolve_in_progress_refonly_5(Hashtable_t1793686270 * value)
	{
		___assembly_resolve_in_progress_refonly_5 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_refonly_5), value);
	}

	inline static int32_t get_offset_of__principal_9() { return static_cast<int32_t>(offsetof(AppDomain_t2391992810_ThreadStaticFields, ____principal_9)); }
	inline RuntimeObject* get__principal_9() const { return ____principal_9; }
	inline RuntimeObject** get_address_of__principal_9() { return &____principal_9; }
	inline void set__principal_9(RuntimeObject* value)
	{
		____principal_9 = value;
		Il2CppCodeGenWriteBarrier((&____principal_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPDOMAIN_T2391992810_H
#ifndef THREAD_T2508967565_H
#define THREAD_T2508967565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_t2508967565  : public CriticalFinalizerObject_t3259952535
{
public:
	// System.Int32 System.Threading.Thread::lock_thread_id
	int32_t ___lock_thread_id_0;
	// System.IntPtr System.Threading.Thread::system_thread_handle
	intptr_t ___system_thread_handle_1;
	// System.Object System.Threading.Thread::cached_culture_info
	RuntimeObject * ___cached_culture_info_2;
	// System.IntPtr System.Threading.Thread::unused0
	intptr_t ___unused0_3;
	// System.Boolean System.Threading.Thread::threadpool_thread
	bool ___threadpool_thread_4;
	// System.IntPtr System.Threading.Thread::name
	intptr_t ___name_5;
	// System.Int32 System.Threading.Thread::name_len
	int32_t ___name_len_6;
	// System.Threading.ThreadState System.Threading.Thread::state
	int32_t ___state_7;
	// System.Object System.Threading.Thread::abort_exc
	RuntimeObject * ___abort_exc_8;
	// System.Int32 System.Threading.Thread::abort_state_handle
	int32_t ___abort_state_handle_9;
	// System.Int64 System.Threading.Thread::thread_id
	int64_t ___thread_id_10;
	// System.IntPtr System.Threading.Thread::start_notify
	intptr_t ___start_notify_11;
	// System.IntPtr System.Threading.Thread::stack_ptr
	intptr_t ___stack_ptr_12;
	// System.UIntPtr System.Threading.Thread::static_data
	uintptr_t ___static_data_13;
	// System.IntPtr System.Threading.Thread::jit_data
	intptr_t ___jit_data_14;
	// System.IntPtr System.Threading.Thread::lock_data
	intptr_t ___lock_data_15;
	// System.Object System.Threading.Thread::current_appcontext
	RuntimeObject * ___current_appcontext_16;
	// System.Int32 System.Threading.Thread::stack_size
	int32_t ___stack_size_17;
	// System.Object System.Threading.Thread::start_obj
	RuntimeObject * ___start_obj_18;
	// System.IntPtr System.Threading.Thread::appdomain_refs
	intptr_t ___appdomain_refs_19;
	// System.Int32 System.Threading.Thread::interruption_requested
	int32_t ___interruption_requested_20;
	// System.IntPtr System.Threading.Thread::suspend_event
	intptr_t ___suspend_event_21;
	// System.IntPtr System.Threading.Thread::suspended_event
	intptr_t ___suspended_event_22;
	// System.IntPtr System.Threading.Thread::resume_event
	intptr_t ___resume_event_23;
	// System.IntPtr System.Threading.Thread::synch_cs
	intptr_t ___synch_cs_24;
	// System.IntPtr System.Threading.Thread::serialized_culture_info
	intptr_t ___serialized_culture_info_25;
	// System.Int32 System.Threading.Thread::serialized_culture_info_len
	int32_t ___serialized_culture_info_len_26;
	// System.IntPtr System.Threading.Thread::serialized_ui_culture_info
	intptr_t ___serialized_ui_culture_info_27;
	// System.Int32 System.Threading.Thread::serialized_ui_culture_info_len
	int32_t ___serialized_ui_culture_info_len_28;
	// System.Boolean System.Threading.Thread::thread_dump_requested
	bool ___thread_dump_requested_29;
	// System.IntPtr System.Threading.Thread::end_stack
	intptr_t ___end_stack_30;
	// System.Boolean System.Threading.Thread::thread_interrupt_requested
	bool ___thread_interrupt_requested_31;
	// System.Byte System.Threading.Thread::apartment_state
	uint8_t ___apartment_state_32;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Thread::critical_region_level
	int32_t ___critical_region_level_33;
	// System.Int32 System.Threading.Thread::small_id
	int32_t ___small_id_34;
	// System.IntPtr System.Threading.Thread::manage_callback
	intptr_t ___manage_callback_35;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_36;
	// System.Threading.ExecutionContext System.Threading.Thread::ec_to_set
	ExecutionContext_t1695260799 * ___ec_to_set_37;
	// System.IntPtr System.Threading.Thread::interrupt_on_stop
	intptr_t ___interrupt_on_stop_38;
	// System.IntPtr System.Threading.Thread::unused3
	intptr_t ___unused3_39;
	// System.IntPtr System.Threading.Thread::unused4
	intptr_t ___unused4_40;
	// System.IntPtr System.Threading.Thread::unused5
	intptr_t ___unused5_41;
	// System.IntPtr System.Threading.Thread::unused6
	intptr_t ___unused6_42;
	// System.MulticastDelegate System.Threading.Thread::threadstart
	MulticastDelegate_t599438524 * ___threadstart_45;
	// System.Int32 System.Threading.Thread::managed_id
	int32_t ___managed_id_46;
	// System.Security.Principal.IPrincipal System.Threading.Thread::_principal
	RuntimeObject* ____principal_47;
	// System.Boolean System.Threading.Thread::in_currentculture
	bool ___in_currentculture_50;

public:
	inline static int32_t get_offset_of_lock_thread_id_0() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___lock_thread_id_0)); }
	inline int32_t get_lock_thread_id_0() const { return ___lock_thread_id_0; }
	inline int32_t* get_address_of_lock_thread_id_0() { return &___lock_thread_id_0; }
	inline void set_lock_thread_id_0(int32_t value)
	{
		___lock_thread_id_0 = value;
	}

	inline static int32_t get_offset_of_system_thread_handle_1() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___system_thread_handle_1)); }
	inline intptr_t get_system_thread_handle_1() const { return ___system_thread_handle_1; }
	inline intptr_t* get_address_of_system_thread_handle_1() { return &___system_thread_handle_1; }
	inline void set_system_thread_handle_1(intptr_t value)
	{
		___system_thread_handle_1 = value;
	}

	inline static int32_t get_offset_of_cached_culture_info_2() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___cached_culture_info_2)); }
	inline RuntimeObject * get_cached_culture_info_2() const { return ___cached_culture_info_2; }
	inline RuntimeObject ** get_address_of_cached_culture_info_2() { return &___cached_culture_info_2; }
	inline void set_cached_culture_info_2(RuntimeObject * value)
	{
		___cached_culture_info_2 = value;
		Il2CppCodeGenWriteBarrier((&___cached_culture_info_2), value);
	}

	inline static int32_t get_offset_of_unused0_3() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___unused0_3)); }
	inline intptr_t get_unused0_3() const { return ___unused0_3; }
	inline intptr_t* get_address_of_unused0_3() { return &___unused0_3; }
	inline void set_unused0_3(intptr_t value)
	{
		___unused0_3 = value;
	}

	inline static int32_t get_offset_of_threadpool_thread_4() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___threadpool_thread_4)); }
	inline bool get_threadpool_thread_4() const { return ___threadpool_thread_4; }
	inline bool* get_address_of_threadpool_thread_4() { return &___threadpool_thread_4; }
	inline void set_threadpool_thread_4(bool value)
	{
		___threadpool_thread_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___name_5)); }
	inline intptr_t get_name_5() const { return ___name_5; }
	inline intptr_t* get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(intptr_t value)
	{
		___name_5 = value;
	}

	inline static int32_t get_offset_of_name_len_6() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___name_len_6)); }
	inline int32_t get_name_len_6() const { return ___name_len_6; }
	inline int32_t* get_address_of_name_len_6() { return &___name_len_6; }
	inline void set_name_len_6(int32_t value)
	{
		___name_len_6 = value;
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___state_7)); }
	inline int32_t get_state_7() const { return ___state_7; }
	inline int32_t* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(int32_t value)
	{
		___state_7 = value;
	}

	inline static int32_t get_offset_of_abort_exc_8() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___abort_exc_8)); }
	inline RuntimeObject * get_abort_exc_8() const { return ___abort_exc_8; }
	inline RuntimeObject ** get_address_of_abort_exc_8() { return &___abort_exc_8; }
	inline void set_abort_exc_8(RuntimeObject * value)
	{
		___abort_exc_8 = value;
		Il2CppCodeGenWriteBarrier((&___abort_exc_8), value);
	}

	inline static int32_t get_offset_of_abort_state_handle_9() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___abort_state_handle_9)); }
	inline int32_t get_abort_state_handle_9() const { return ___abort_state_handle_9; }
	inline int32_t* get_address_of_abort_state_handle_9() { return &___abort_state_handle_9; }
	inline void set_abort_state_handle_9(int32_t value)
	{
		___abort_state_handle_9 = value;
	}

	inline static int32_t get_offset_of_thread_id_10() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___thread_id_10)); }
	inline int64_t get_thread_id_10() const { return ___thread_id_10; }
	inline int64_t* get_address_of_thread_id_10() { return &___thread_id_10; }
	inline void set_thread_id_10(int64_t value)
	{
		___thread_id_10 = value;
	}

	inline static int32_t get_offset_of_start_notify_11() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___start_notify_11)); }
	inline intptr_t get_start_notify_11() const { return ___start_notify_11; }
	inline intptr_t* get_address_of_start_notify_11() { return &___start_notify_11; }
	inline void set_start_notify_11(intptr_t value)
	{
		___start_notify_11 = value;
	}

	inline static int32_t get_offset_of_stack_ptr_12() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___stack_ptr_12)); }
	inline intptr_t get_stack_ptr_12() const { return ___stack_ptr_12; }
	inline intptr_t* get_address_of_stack_ptr_12() { return &___stack_ptr_12; }
	inline void set_stack_ptr_12(intptr_t value)
	{
		___stack_ptr_12 = value;
	}

	inline static int32_t get_offset_of_static_data_13() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___static_data_13)); }
	inline uintptr_t get_static_data_13() const { return ___static_data_13; }
	inline uintptr_t* get_address_of_static_data_13() { return &___static_data_13; }
	inline void set_static_data_13(uintptr_t value)
	{
		___static_data_13 = value;
	}

	inline static int32_t get_offset_of_jit_data_14() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___jit_data_14)); }
	inline intptr_t get_jit_data_14() const { return ___jit_data_14; }
	inline intptr_t* get_address_of_jit_data_14() { return &___jit_data_14; }
	inline void set_jit_data_14(intptr_t value)
	{
		___jit_data_14 = value;
	}

	inline static int32_t get_offset_of_lock_data_15() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___lock_data_15)); }
	inline intptr_t get_lock_data_15() const { return ___lock_data_15; }
	inline intptr_t* get_address_of_lock_data_15() { return &___lock_data_15; }
	inline void set_lock_data_15(intptr_t value)
	{
		___lock_data_15 = value;
	}

	inline static int32_t get_offset_of_current_appcontext_16() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___current_appcontext_16)); }
	inline RuntimeObject * get_current_appcontext_16() const { return ___current_appcontext_16; }
	inline RuntimeObject ** get_address_of_current_appcontext_16() { return &___current_appcontext_16; }
	inline void set_current_appcontext_16(RuntimeObject * value)
	{
		___current_appcontext_16 = value;
		Il2CppCodeGenWriteBarrier((&___current_appcontext_16), value);
	}

	inline static int32_t get_offset_of_stack_size_17() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___stack_size_17)); }
	inline int32_t get_stack_size_17() const { return ___stack_size_17; }
	inline int32_t* get_address_of_stack_size_17() { return &___stack_size_17; }
	inline void set_stack_size_17(int32_t value)
	{
		___stack_size_17 = value;
	}

	inline static int32_t get_offset_of_start_obj_18() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___start_obj_18)); }
	inline RuntimeObject * get_start_obj_18() const { return ___start_obj_18; }
	inline RuntimeObject ** get_address_of_start_obj_18() { return &___start_obj_18; }
	inline void set_start_obj_18(RuntimeObject * value)
	{
		___start_obj_18 = value;
		Il2CppCodeGenWriteBarrier((&___start_obj_18), value);
	}

	inline static int32_t get_offset_of_appdomain_refs_19() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___appdomain_refs_19)); }
	inline intptr_t get_appdomain_refs_19() const { return ___appdomain_refs_19; }
	inline intptr_t* get_address_of_appdomain_refs_19() { return &___appdomain_refs_19; }
	inline void set_appdomain_refs_19(intptr_t value)
	{
		___appdomain_refs_19 = value;
	}

	inline static int32_t get_offset_of_interruption_requested_20() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___interruption_requested_20)); }
	inline int32_t get_interruption_requested_20() const { return ___interruption_requested_20; }
	inline int32_t* get_address_of_interruption_requested_20() { return &___interruption_requested_20; }
	inline void set_interruption_requested_20(int32_t value)
	{
		___interruption_requested_20 = value;
	}

	inline static int32_t get_offset_of_suspend_event_21() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___suspend_event_21)); }
	inline intptr_t get_suspend_event_21() const { return ___suspend_event_21; }
	inline intptr_t* get_address_of_suspend_event_21() { return &___suspend_event_21; }
	inline void set_suspend_event_21(intptr_t value)
	{
		___suspend_event_21 = value;
	}

	inline static int32_t get_offset_of_suspended_event_22() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___suspended_event_22)); }
	inline intptr_t get_suspended_event_22() const { return ___suspended_event_22; }
	inline intptr_t* get_address_of_suspended_event_22() { return &___suspended_event_22; }
	inline void set_suspended_event_22(intptr_t value)
	{
		___suspended_event_22 = value;
	}

	inline static int32_t get_offset_of_resume_event_23() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___resume_event_23)); }
	inline intptr_t get_resume_event_23() const { return ___resume_event_23; }
	inline intptr_t* get_address_of_resume_event_23() { return &___resume_event_23; }
	inline void set_resume_event_23(intptr_t value)
	{
		___resume_event_23 = value;
	}

	inline static int32_t get_offset_of_synch_cs_24() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___synch_cs_24)); }
	inline intptr_t get_synch_cs_24() const { return ___synch_cs_24; }
	inline intptr_t* get_address_of_synch_cs_24() { return &___synch_cs_24; }
	inline void set_synch_cs_24(intptr_t value)
	{
		___synch_cs_24 = value;
	}

	inline static int32_t get_offset_of_serialized_culture_info_25() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___serialized_culture_info_25)); }
	inline intptr_t get_serialized_culture_info_25() const { return ___serialized_culture_info_25; }
	inline intptr_t* get_address_of_serialized_culture_info_25() { return &___serialized_culture_info_25; }
	inline void set_serialized_culture_info_25(intptr_t value)
	{
		___serialized_culture_info_25 = value;
	}

	inline static int32_t get_offset_of_serialized_culture_info_len_26() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___serialized_culture_info_len_26)); }
	inline int32_t get_serialized_culture_info_len_26() const { return ___serialized_culture_info_len_26; }
	inline int32_t* get_address_of_serialized_culture_info_len_26() { return &___serialized_culture_info_len_26; }
	inline void set_serialized_culture_info_len_26(int32_t value)
	{
		___serialized_culture_info_len_26 = value;
	}

	inline static int32_t get_offset_of_serialized_ui_culture_info_27() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___serialized_ui_culture_info_27)); }
	inline intptr_t get_serialized_ui_culture_info_27() const { return ___serialized_ui_culture_info_27; }
	inline intptr_t* get_address_of_serialized_ui_culture_info_27() { return &___serialized_ui_culture_info_27; }
	inline void set_serialized_ui_culture_info_27(intptr_t value)
	{
		___serialized_ui_culture_info_27 = value;
	}

	inline static int32_t get_offset_of_serialized_ui_culture_info_len_28() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___serialized_ui_culture_info_len_28)); }
	inline int32_t get_serialized_ui_culture_info_len_28() const { return ___serialized_ui_culture_info_len_28; }
	inline int32_t* get_address_of_serialized_ui_culture_info_len_28() { return &___serialized_ui_culture_info_len_28; }
	inline void set_serialized_ui_culture_info_len_28(int32_t value)
	{
		___serialized_ui_culture_info_len_28 = value;
	}

	inline static int32_t get_offset_of_thread_dump_requested_29() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___thread_dump_requested_29)); }
	inline bool get_thread_dump_requested_29() const { return ___thread_dump_requested_29; }
	inline bool* get_address_of_thread_dump_requested_29() { return &___thread_dump_requested_29; }
	inline void set_thread_dump_requested_29(bool value)
	{
		___thread_dump_requested_29 = value;
	}

	inline static int32_t get_offset_of_end_stack_30() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___end_stack_30)); }
	inline intptr_t get_end_stack_30() const { return ___end_stack_30; }
	inline intptr_t* get_address_of_end_stack_30() { return &___end_stack_30; }
	inline void set_end_stack_30(intptr_t value)
	{
		___end_stack_30 = value;
	}

	inline static int32_t get_offset_of_thread_interrupt_requested_31() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___thread_interrupt_requested_31)); }
	inline bool get_thread_interrupt_requested_31() const { return ___thread_interrupt_requested_31; }
	inline bool* get_address_of_thread_interrupt_requested_31() { return &___thread_interrupt_requested_31; }
	inline void set_thread_interrupt_requested_31(bool value)
	{
		___thread_interrupt_requested_31 = value;
	}

	inline static int32_t get_offset_of_apartment_state_32() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___apartment_state_32)); }
	inline uint8_t get_apartment_state_32() const { return ___apartment_state_32; }
	inline uint8_t* get_address_of_apartment_state_32() { return &___apartment_state_32; }
	inline void set_apartment_state_32(uint8_t value)
	{
		___apartment_state_32 = value;
	}

	inline static int32_t get_offset_of_critical_region_level_33() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___critical_region_level_33)); }
	inline int32_t get_critical_region_level_33() const { return ___critical_region_level_33; }
	inline int32_t* get_address_of_critical_region_level_33() { return &___critical_region_level_33; }
	inline void set_critical_region_level_33(int32_t value)
	{
		___critical_region_level_33 = value;
	}

	inline static int32_t get_offset_of_small_id_34() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___small_id_34)); }
	inline int32_t get_small_id_34() const { return ___small_id_34; }
	inline int32_t* get_address_of_small_id_34() { return &___small_id_34; }
	inline void set_small_id_34(int32_t value)
	{
		___small_id_34 = value;
	}

	inline static int32_t get_offset_of_manage_callback_35() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___manage_callback_35)); }
	inline intptr_t get_manage_callback_35() const { return ___manage_callback_35; }
	inline intptr_t* get_address_of_manage_callback_35() { return &___manage_callback_35; }
	inline void set_manage_callback_35(intptr_t value)
	{
		___manage_callback_35 = value;
	}

	inline static int32_t get_offset_of_pending_exception_36() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___pending_exception_36)); }
	inline RuntimeObject * get_pending_exception_36() const { return ___pending_exception_36; }
	inline RuntimeObject ** get_address_of_pending_exception_36() { return &___pending_exception_36; }
	inline void set_pending_exception_36(RuntimeObject * value)
	{
		___pending_exception_36 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_36), value);
	}

	inline static int32_t get_offset_of_ec_to_set_37() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___ec_to_set_37)); }
	inline ExecutionContext_t1695260799 * get_ec_to_set_37() const { return ___ec_to_set_37; }
	inline ExecutionContext_t1695260799 ** get_address_of_ec_to_set_37() { return &___ec_to_set_37; }
	inline void set_ec_to_set_37(ExecutionContext_t1695260799 * value)
	{
		___ec_to_set_37 = value;
		Il2CppCodeGenWriteBarrier((&___ec_to_set_37), value);
	}

	inline static int32_t get_offset_of_interrupt_on_stop_38() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___interrupt_on_stop_38)); }
	inline intptr_t get_interrupt_on_stop_38() const { return ___interrupt_on_stop_38; }
	inline intptr_t* get_address_of_interrupt_on_stop_38() { return &___interrupt_on_stop_38; }
	inline void set_interrupt_on_stop_38(intptr_t value)
	{
		___interrupt_on_stop_38 = value;
	}

	inline static int32_t get_offset_of_unused3_39() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___unused3_39)); }
	inline intptr_t get_unused3_39() const { return ___unused3_39; }
	inline intptr_t* get_address_of_unused3_39() { return &___unused3_39; }
	inline void set_unused3_39(intptr_t value)
	{
		___unused3_39 = value;
	}

	inline static int32_t get_offset_of_unused4_40() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___unused4_40)); }
	inline intptr_t get_unused4_40() const { return ___unused4_40; }
	inline intptr_t* get_address_of_unused4_40() { return &___unused4_40; }
	inline void set_unused4_40(intptr_t value)
	{
		___unused4_40 = value;
	}

	inline static int32_t get_offset_of_unused5_41() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___unused5_41)); }
	inline intptr_t get_unused5_41() const { return ___unused5_41; }
	inline intptr_t* get_address_of_unused5_41() { return &___unused5_41; }
	inline void set_unused5_41(intptr_t value)
	{
		___unused5_41 = value;
	}

	inline static int32_t get_offset_of_unused6_42() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___unused6_42)); }
	inline intptr_t get_unused6_42() const { return ___unused6_42; }
	inline intptr_t* get_address_of_unused6_42() { return &___unused6_42; }
	inline void set_unused6_42(intptr_t value)
	{
		___unused6_42 = value;
	}

	inline static int32_t get_offset_of_threadstart_45() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___threadstart_45)); }
	inline MulticastDelegate_t599438524 * get_threadstart_45() const { return ___threadstart_45; }
	inline MulticastDelegate_t599438524 ** get_address_of_threadstart_45() { return &___threadstart_45; }
	inline void set_threadstart_45(MulticastDelegate_t599438524 * value)
	{
		___threadstart_45 = value;
		Il2CppCodeGenWriteBarrier((&___threadstart_45), value);
	}

	inline static int32_t get_offset_of_managed_id_46() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___managed_id_46)); }
	inline int32_t get_managed_id_46() const { return ___managed_id_46; }
	inline int32_t* get_address_of_managed_id_46() { return &___managed_id_46; }
	inline void set_managed_id_46(int32_t value)
	{
		___managed_id_46 = value;
	}

	inline static int32_t get_offset_of__principal_47() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ____principal_47)); }
	inline RuntimeObject* get__principal_47() const { return ____principal_47; }
	inline RuntimeObject** get_address_of__principal_47() { return &____principal_47; }
	inline void set__principal_47(RuntimeObject* value)
	{
		____principal_47 = value;
		Il2CppCodeGenWriteBarrier((&____principal_47), value);
	}

	inline static int32_t get_offset_of_in_currentculture_50() { return static_cast<int32_t>(offsetof(Thread_t2508967565, ___in_currentculture_50)); }
	inline bool get_in_currentculture_50() const { return ___in_currentculture_50; }
	inline bool* get_address_of_in_currentculture_50() { return &___in_currentculture_50; }
	inline void set_in_currentculture_50(bool value)
	{
		___in_currentculture_50 = value;
	}
};

struct Thread_t2508967565_StaticFields
{
public:
	// System.Collections.Hashtable System.Threading.Thread::datastorehash
	Hashtable_t1793686270 * ___datastorehash_48;
	// System.Object System.Threading.Thread::datastore_lock
	RuntimeObject * ___datastore_lock_49;
	// System.Object System.Threading.Thread::culture_lock
	RuntimeObject * ___culture_lock_51;

public:
	inline static int32_t get_offset_of_datastorehash_48() { return static_cast<int32_t>(offsetof(Thread_t2508967565_StaticFields, ___datastorehash_48)); }
	inline Hashtable_t1793686270 * get_datastorehash_48() const { return ___datastorehash_48; }
	inline Hashtable_t1793686270 ** get_address_of_datastorehash_48() { return &___datastorehash_48; }
	inline void set_datastorehash_48(Hashtable_t1793686270 * value)
	{
		___datastorehash_48 = value;
		Il2CppCodeGenWriteBarrier((&___datastorehash_48), value);
	}

	inline static int32_t get_offset_of_datastore_lock_49() { return static_cast<int32_t>(offsetof(Thread_t2508967565_StaticFields, ___datastore_lock_49)); }
	inline RuntimeObject * get_datastore_lock_49() const { return ___datastore_lock_49; }
	inline RuntimeObject ** get_address_of_datastore_lock_49() { return &___datastore_lock_49; }
	inline void set_datastore_lock_49(RuntimeObject * value)
	{
		___datastore_lock_49 = value;
		Il2CppCodeGenWriteBarrier((&___datastore_lock_49), value);
	}

	inline static int32_t get_offset_of_culture_lock_51() { return static_cast<int32_t>(offsetof(Thread_t2508967565_StaticFields, ___culture_lock_51)); }
	inline RuntimeObject * get_culture_lock_51() const { return ___culture_lock_51; }
	inline RuntimeObject ** get_address_of_culture_lock_51() { return &___culture_lock_51; }
	inline void set_culture_lock_51(RuntimeObject * value)
	{
		___culture_lock_51 = value;
		Il2CppCodeGenWriteBarrier((&___culture_lock_51), value);
	}
};

struct Thread_t2508967565_ThreadStaticFields
{
public:
	// System.Object[] System.Threading.Thread::local_slots
	ObjectU5BU5D_t3385344125* ___local_slots_43;
	// System.Threading.ExecutionContext System.Threading.Thread::_ec
	ExecutionContext_t1695260799 * ____ec_44;

public:
	inline static int32_t get_offset_of_local_slots_43() { return static_cast<int32_t>(offsetof(Thread_t2508967565_ThreadStaticFields, ___local_slots_43)); }
	inline ObjectU5BU5D_t3385344125* get_local_slots_43() const { return ___local_slots_43; }
	inline ObjectU5BU5D_t3385344125** get_address_of_local_slots_43() { return &___local_slots_43; }
	inline void set_local_slots_43(ObjectU5BU5D_t3385344125* value)
	{
		___local_slots_43 = value;
		Il2CppCodeGenWriteBarrier((&___local_slots_43), value);
	}

	inline static int32_t get_offset_of__ec_44() { return static_cast<int32_t>(offsetof(Thread_t2508967565_ThreadStaticFields, ____ec_44)); }
	inline ExecutionContext_t1695260799 * get__ec_44() const { return ____ec_44; }
	inline ExecutionContext_t1695260799 ** get_address_of__ec_44() { return &____ec_44; }
	inline void set__ec_44(ExecutionContext_t1695260799 * value)
	{
		____ec_44 = value;
		Il2CppCodeGenWriteBarrier((&____ec_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_T2508967565_H
#ifndef SPRITE_T2497061588_H
#define SPRITE_T2497061588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t2497061588  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T2497061588_H
#ifndef RESOURCEREQUEST_T2277419115_H
#define RESOURCEREQUEST_T2277419115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ResourceRequest
struct  ResourceRequest_t2277419115  : public AsyncOperation_t3797817720
{
public:
	// System.String UnityEngine.ResourceRequest::m_Path
	String_t* ___m_Path_2;
	// System.Type UnityEngine.ResourceRequest::m_Type
	Type_t * ___m_Type_3;

public:
	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(ResourceRequest_t2277419115, ___m_Path_2)); }
	inline String_t* get_m_Path_2() const { return ___m_Path_2; }
	inline String_t** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(String_t* value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(ResourceRequest_t2277419115, ___m_Type_3)); }
	inline Type_t * get_m_Type_3() const { return ___m_Type_3; }
	inline Type_t ** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Type_t * value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_t2277419115_marshaled_pinvoke : public AsyncOperation_t3797817720_marshaled_pinvoke
{
	char* ___m_Path_2;
	Type_t * ___m_Type_3;
};
// Native definition for COM marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_t2277419115_marshaled_com : public AsyncOperation_t3797817720_marshaled_com
{
	Il2CppChar* ___m_Path_2;
	Type_t * ___m_Type_3;
};
#endif // RESOURCEREQUEST_T2277419115_H
#ifndef PLAYABLEBINDING_T2941134609_H
#define PLAYABLEBINDING_T2941134609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t2941134609 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t3645472222 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t2941134609__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t2941134609, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t2941134609, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t2941134609, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t3645472222 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t3645472222 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t3645472222 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t2941134609, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t2941134609_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t1865297292* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t2941134609_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t1865297292* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t1865297292** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t1865297292* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t2941134609_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t2941134609_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t3645472222_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t2941134609__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t2941134609_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t3645472222_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t2941134609__padding[1];
	};
};
#endif // PLAYABLEBINDING_T2941134609_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t1986816146  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t1986816146  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t1986816146 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t1986816146  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t98989581* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t2882044324 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t2882044324 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t2882044324 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t98989581* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t98989581** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t98989581* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t2882044324 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t2882044324 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t2882044324 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t2882044324 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t2882044324 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t2882044324 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t2882044324 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef PLAYABLEOUTPUT_T2773044158_H
#define PLAYABLEOUTPUT_T2773044158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutput
struct  PlayableOutput_t2773044158 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::m_Handle
	PlayableOutputHandle_t1247041817  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutput_t2773044158, ___m_Handle_0)); }
	inline PlayableOutputHandle_t1247041817  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t1247041817 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t1247041817  value)
	{
		___m_Handle_0 = value;
	}
};

struct PlayableOutput_t2773044158_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::m_NullPlayableOutput
	PlayableOutput_t2773044158  ___m_NullPlayableOutput_1;

public:
	inline static int32_t get_offset_of_m_NullPlayableOutput_1() { return static_cast<int32_t>(offsetof(PlayableOutput_t2773044158_StaticFields, ___m_NullPlayableOutput_1)); }
	inline PlayableOutput_t2773044158  get_m_NullPlayableOutput_1() const { return ___m_NullPlayableOutput_1; }
	inline PlayableOutput_t2773044158 * get_address_of_m_NullPlayableOutput_1() { return &___m_NullPlayableOutput_1; }
	inline void set_m_NullPlayableOutput_1(PlayableOutput_t2773044158  value)
	{
		___m_NullPlayableOutput_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUT_T2773044158_H
#ifndef SCRIPTPLAYABLEOUTPUT_T3039902183_H
#define SCRIPTPLAYABLEOUTPUT_T3039902183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.ScriptPlayableOutput
struct  ScriptPlayableOutput_t3039902183 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::m_Handle
	PlayableOutputHandle_t1247041817  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(ScriptPlayableOutput_t3039902183, ___m_Handle_0)); }
	inline PlayableOutputHandle_t1247041817  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t1247041817 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t1247041817  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTPLAYABLEOUTPUT_T3039902183_H
#ifndef PLAYABLE_T379060559_H
#define PLAYABLE_T379060559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t379060559 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t1495371730  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t379060559, ___m_Handle_0)); }
	inline PlayableHandle_t1495371730  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1495371730 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1495371730  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t379060559_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t379060559  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t379060559_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t379060559  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t379060559 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t379060559  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T379060559_H
#ifndef COMPONENT_T2335505321_H
#define COMPONENT_T2335505321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t2335505321  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T2335505321_H
#ifndef MATERIAL_T1926439680_H
#define MATERIAL_T1926439680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t1926439680  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T1926439680_H
#ifndef TEXTURE_T954505614_H
#define TEXTURE_T954505614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t954505614  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T954505614_H
#ifndef RENDERTARGETIDENTIFIER_T35305570_H
#define RENDERTARGETIDENTIFIER_T35305570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t35305570 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_BufferPointer_3() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_BufferPointer_3)); }
	inline intptr_t get_m_BufferPointer_3() const { return ___m_BufferPointer_3; }
	inline intptr_t* get_address_of_m_BufferPointer_3() { return &___m_BufferPointer_3; }
	inline void set_m_BufferPointer_3(intptr_t value)
	{
		___m_BufferPointer_3 = value;
	}

	inline static int32_t get_offset_of_m_MipLevel_4() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_MipLevel_4)); }
	inline int32_t get_m_MipLevel_4() const { return ___m_MipLevel_4; }
	inline int32_t* get_address_of_m_MipLevel_4() { return &___m_MipLevel_4; }
	inline void set_m_MipLevel_4(int32_t value)
	{
		___m_MipLevel_4 = value;
	}

	inline static int32_t get_offset_of_m_CubeFace_5() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_CubeFace_5)); }
	inline int32_t get_m_CubeFace_5() const { return ___m_CubeFace_5; }
	inline int32_t* get_address_of_m_CubeFace_5() { return &___m_CubeFace_5; }
	inline void set_m_CubeFace_5(int32_t value)
	{
		___m_CubeFace_5 = value;
	}

	inline static int32_t get_offset_of_m_DepthSlice_6() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t35305570, ___m_DepthSlice_6)); }
	inline int32_t get_m_DepthSlice_6() const { return ___m_DepthSlice_6; }
	inline int32_t* get_address_of_m_DepthSlice_6() { return &___m_DepthSlice_6; }
	inline void set_m_DepthSlice_6(int32_t value)
	{
		___m_DepthSlice_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T35305570_H
#ifndef SPRITEATLAS_T2782010525_H
#define SPRITEATLAS_T2782010525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlas
struct  SpriteAtlas_t2782010525  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLAS_T2782010525_H
#ifndef GAMEOBJECT_T2881801184_H
#define GAMEOBJECT_T2881801184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2881801184  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2881801184_H
#ifndef TOUCH_T2641233222_H
#define TOUCH_T2641233222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t2641233222 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2246369278  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2246369278  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2246369278  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Position_1)); }
	inline Vector2_t2246369278  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2246369278 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2246369278  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_RawPosition_2)); }
	inline Vector2_t2246369278  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2246369278 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2246369278  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_PositionDelta_3)); }
	inline Vector2_t2246369278  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2246369278 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2246369278  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t2641233222, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T2641233222_H
#ifndef PARAMETERINFO_T3156340899_H
#define PARAMETERINFO_T3156340899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t3156340899  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.ParameterInfo::marshalAs
	UnmanagedMarshal_t4202419344 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t3156340899, ___marshalAs_6)); }
	inline UnmanagedMarshal_t4202419344 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline UnmanagedMarshal_t4202419344 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(UnmanagedMarshal_t4202419344 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERINFO_T3156340899_H
#ifndef SCRIPTABLEOBJECT_T1324617639_H
#define SCRIPTABLEOBJECT_T1324617639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1324617639  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1324617639_marshaled_pinvoke : public Object_t3645472222_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1324617639_marshaled_com : public Object_t3645472222_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1324617639_H
#ifndef SHADER_T851244957_H
#define SHADER_T851244957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t851244957  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T851244957_H
#ifndef ASYNCCALLBACK_T4283869127_H
#define ASYNCCALLBACK_T4283869127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t4283869127  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T4283869127_H
#ifndef ACTION_1_T260058957_H
#define ACTION_1_T260058957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct  Action_1_t260058957  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T260058957_H
#ifndef FUNC_2_T2013519524_H
#define FUNC_2_T2013519524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct  Func_2_t2013519524  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2013519524_H
#ifndef UNHANDLEDEXCEPTIONEVENTHANDLER_T1143045648_H
#define UNHANDLEDEXCEPTIONEVENTHANDLER_T1143045648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t1143045648  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTHANDLER_T1143045648_H
#ifndef PLAYABLEASSET_T3894937101_H
#define PLAYABLEASSET_T3894937101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t3894937101  : public ScriptableObject_t1324617639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T3894937101_H
#ifndef REAPPLYDRIVENPROPERTIES_T1147600571_H
#define REAPPLYDRIVENPROPERTIES_T1147600571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t1147600571  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REAPPLYDRIVENPROPERTIES_T1147600571_H
#ifndef BEHAVIOUR_T3297694025_H
#define BEHAVIOUR_T3297694025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t3297694025  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T3297694025_H
#ifndef TRANSFORM_T3580444445_H
#define TRANSFORM_T3580444445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3580444445  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3580444445_H
#ifndef REQUESTATLASCALLBACK_T3823435737_H
#define REQUESTATLASCALLBACK_T3823435737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct  RequestAtlasCallback_t3823435737  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTATLASCALLBACK_T3823435737_H
#ifndef TEXTURE2D_T91252194_H
#define TEXTURE2D_T91252194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t91252194  : public Texture_t954505614
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T91252194_H
#ifndef RENDERTEXTURE_T3737750657_H
#define RENDERTEXTURE_T3737750657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t3737750657  : public Texture_t954505614
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T3737750657_H
#ifndef MANUALRESETEVENT_T2620507134_H
#define MANUALRESETEVENT_T2620507134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ManualResetEvent
struct  ManualResetEvent_t2620507134  : public EventWaitHandle_t1398859231
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALRESETEVENT_T2620507134_H
#ifndef SENDORPOSTCALLBACK_T1528611714_H
#define SENDORPOSTCALLBACK_T1528611714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SendOrPostCallback
struct  SendOrPostCallback_t1528611714  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDORPOSTCALLBACK_T1528611714_H
#ifndef ACTION_1_T1275866152_H
#define ACTION_1_T1275866152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.AsyncOperation>
struct  Action_1_t1275866152  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1275866152_H
#ifndef UNITYACTION_2_T2618073252_H
#define UNITYACTION_2_T2618073252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_t2618073252  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T2618073252_H
#ifndef UNITYACTION_1_T3754315317_H
#define UNITYACTION_1_T3754315317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct  UnityAction_1_t3754315317  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3754315317_H
#ifndef UNITYACTION_2_T671528035_H
#define UNITYACTION_2_T671528035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct  UnityAction_2_t671528035  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T671528035_H
#ifndef RENDERER_T3456269968_H
#define RENDERER_T3456269968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t3456269968  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T3456269968_H
#ifndef GUILAYER_T1634068716_H
#define GUILAYER_T1634068716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t1634068716  : public Behaviour_t3297694025
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T1634068716_H
#ifndef SPRITERENDERER_T3285139998_H
#define SPRITERENDERER_T3285139998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3285139998  : public Renderer_t3456269968
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3285139998_H
#ifndef GUIELEMENT_T2757512779_H
#define GUIELEMENT_T2757512779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t2757512779  : public Behaviour_t3297694025
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T2757512779_H
#ifndef RECTTRANSFORM_T1161610249_H
#define RECTTRANSFORM_T1161610249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t1161610249  : public Transform_t3580444445
{
public:

public:
};

struct RectTransform_t1161610249_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1147600571 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t1161610249_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1147600571 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1147600571 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1147600571 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T1161610249_H
#ifndef CAMERA_T362346687_H
#define CAMERA_T362346687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t362346687  : public Behaviour_t3297694025
{
public:

public:
};

struct Camera_t362346687_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t3349755816 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t3349755816 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t3349755816 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t3349755816 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t3349755816 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t3349755816 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t3349755816 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t3349755816 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t3349755816 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t3349755816 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t3349755816 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t3349755816 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T362346687_H
// System.Byte[]
struct ByteU5BU5D_t3567143369  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3385344125  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t1865297292  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PlayableBinding_t2941134609  m_Items[1];

public:
	inline PlayableBinding_t2941134609  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayableBinding_t2941134609 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayableBinding_t2941134609  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PlayableBinding_t2941134609  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayableBinding_t2941134609 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayableBinding_t2941134609  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1432878832  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3932393085  m_Items[1];

public:
	inline Vector3_t3932393085  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3932393085 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3932393085  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3932393085  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3932393085 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3932393085  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t365668710  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t362346687 * m_Items[1];

public:
	inline Camera_t362346687 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t362346687 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t362346687 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t362346687 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t362346687 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t362346687 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1685419976  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HitInfo_t3525137541  m_Items[1];

public:
	inline HitInfo_t3525137541  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HitInfo_t3525137541 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HitInfo_t3525137541  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HitInfo_t3525137541  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HitInfo_t3525137541 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HitInfo_t3525137541  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1706708300  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t1933193809  m_Items[1];

public:
	inline ParameterModifier_t1933193809  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t1933193809 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t1933193809  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t1933193809  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t1933193809 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t1933193809  value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1589106382  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t2953840665  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1048445298  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t3156340899 * m_Items[1];

public:
	inline ParameterInfo_t3156340899 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t3156340899 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t3156340899 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t3156340899 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t3156340899 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t3156340899 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t2352998046  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t662424039  m_Items[1];

public:
	inline Color32_t662424039  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t662424039 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t662424039  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t662424039  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t662424039 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t662424039  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void Object_t3645472222_marshal_pinvoke(const Object_t3645472222& unmarshaled, Object_t3645472222_marshaled_pinvoke& marshaled);
extern "C" void Object_t3645472222_marshal_pinvoke_back(const Object_t3645472222_marshaled_pinvoke& marshaled, Object_t3645472222& unmarshaled);
extern "C" void Object_t3645472222_marshal_pinvoke_cleanup(Object_t3645472222_marshaled_pinvoke& marshaled);
extern "C" void Object_t3645472222_marshal_com(const Object_t3645472222& unmarshaled, Object_t3645472222_marshaled_com& marshaled);
extern "C" void Object_t3645472222_marshal_com_back(const Object_t3645472222_marshaled_com& marshaled, Object_t3645472222& unmarshaled);
extern "C" void Object_t3645472222_marshal_com_cleanup(Object_t3645472222_marshaled_com& marshaled);

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m1366553884_gshared (List_1_t2226230279 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m804630615_gshared (Func_2_t71323286 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  RuntimeObject* Enumerable_Where_TisRuntimeObject_m4119423671_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t71323286 * p1, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisRuntimeObject_m3816297283_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1504245254_gshared (UnityEvent_1_t3533840856 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 System.Linq.Enumerable::SingleOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  RuntimeObject * Enumerable_SingleOrDefault_TisRuntimeObject_m2240561631_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t71323286 * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m2901067668_gshared (List_1_t2226230279 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m1358771383_gshared (UnityEvent_1_t2202423910 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2413420146_gshared (UnityEvent_1_t3533840856 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2791872171_gshared (UnityAction_2_t2618073252 * __this, Scene_t1083318001  p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1991915580_gshared (UnityAction_1_t3754315317 * __this, Scene_t1083318001  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2120342004_gshared (UnityAction_2_t671528035 * __this, Scene_t1083318001  p0, Scene_t1083318001  p1, const RuntimeMethod* method);
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2432164368_gshared (Component_t2335505321 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1757200534_gshared (Action_1_t2325042916 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m1321454259_gshared (Queue_1_t2402182407 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
extern "C"  WorkRequest_t266095197  Queue_1_Dequeue_m1134160965_gshared (Queue_1_t2402182407 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m2804029644_gshared (Queue_1_t2402182407 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::.ctor()
#define List_1__ctor_m164277227(__this, method) ((  void (*) (List_1_t2349647505 *, const RuntimeMethod*))List_1__ctor_m1366553884_gshared)(__this, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m2384156855 (ConnectionChangeEvent_t190635484 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2939335827 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m2273264663 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2035350960(__this, p0, p1, method) ((  void (*) (Func_2_t2013519524 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m804630615_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisMessageTypeSubscribers_t675444414_m1955804045(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2013519524 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m4119423671_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413(__this /* static, unused */, p0, method) ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m3816297283_gshared)(__this /* static, unused */, p0, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m1113447374 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2611403583 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m3912586882 (MessageEventArgs_t2772358345 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::Invoke(T0)
#define UnityEvent_1_Invoke_m2359140723(__this, p0, method) ((  void (*) (UnityEvent_1_t1459204717 *, MessageEventArgs_t2772358345 *, const RuntimeMethod*))UnityEvent_1_Invoke_m1504245254_gshared)(__this, p0, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::.ctor()
extern "C"  void U3CAddAndCreateU3Ec__AnonStorey1__ctor_m614469706 (U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 System.Linq.Enumerable::SingleOrDefault<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t675444414_m557389689(__this /* static, unused */, p0, p1, method) ((  MessageTypeSubscribers_t675444414 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2013519524 *, const RuntimeMethod*))Enumerable_SingleOrDefault_TisRuntimeObject_m2240561631_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern "C"  void MessageTypeSubscribers__ctor_m1862007631 (MessageTypeSubscribers_t675444414 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern "C"  void MessageTypeSubscribers_set_MessageTypeId_m1372988085 (MessageTypeSubscribers_t675444414 * __this, Guid_t  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m3036318349 (MessageEvent_t3114892025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::Add(!0)
#define List_1_Add_m1330338227(__this, p0, method) ((  void (*) (List_1_t2349647505 *, MessageTypeSubscribers_t675444414 *, const RuntimeMethod*))List_1_Add_m2901067668_gshared)(__this, p0, method)
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m409108014 (MessageTypeSubscribers_t675444414 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C"  bool Guid_op_Equality_m2100028761 (RuntimeObject * __this /* static, unused */, Guid_t  p0, Guid_t  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
#define UnityEvent_1__ctor_m1358771383(__this, method) ((  void (*) (UnityEvent_1_t2202423910 *, const RuntimeMethod*))UnityEvent_1__ctor_m1358771383_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::.ctor()
#define UnityEvent_1__ctor_m1455957073(__this, method) ((  void (*) (UnityEvent_1_t1459204717 *, const RuntimeMethod*))UnityEvent_1__ctor_m2413420146_gshared)(__this, method)
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m869444109 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Guid::ToString()
extern "C"  String_t* Guid_ToString_m1679002259 (Guid_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t3645472222 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m2913152063 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___data0, Vector3_t3932393085 * ___pos1, Quaternion_t3497065016 * ___rot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m989819893 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___obj0, float ___t1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m1498630165 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m2582846073 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3342981539 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___x0, Object_t3645472222 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3329896178 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___lhs0, Object_t3645472222 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m3366595570 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___o0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m898979290 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  intptr_t Object_GetCachedPtr_m597461268 (Object_t3645472222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m183498676 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m3054168192 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, String_t* ___message1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m429090071 (ArgumentException_t489606696 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t3645472222 * Object_Internal_InstantiateSingle_m767057946 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___data0, Vector3_t3932393085  ___pos1, Quaternion_t3497065016  ___rot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Vector3_Normalize_m4093520232 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2615379767 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___lhs0, Vector3_t3932393085  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m1011206029 (Plane_t4100015541 * __this, Vector3_t3932393085  ___inNormal0, Vector3_t3932393085  ___inPoint1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3932393085  Ray_get_direction_m2057318423 (Ray_t1807385980 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t3932393085  Ray_get_origin_m1394134582 (Ray_t1807385980 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m4050652555 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m894877444 (Plane_t4100015541 * __this, Ray_t1807385980  ___ray0, float* ___enter1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m1784030070 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3385344125* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Plane::ToString()
extern "C"  String_t* Plane_ToString_m624364449 (Plane_t4100015541 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m856599787 (Playable_t379060559 * __this, PlayableHandle_t1495371730  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t1495371730  Playable_GetHandle_m1375248212 (Playable_t379060559 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m1158199562 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730  ___x0, PlayableHandle_t1495371730  ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m3029208553 (Playable_t379060559 * __this, Playable_t379060559  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t1495371730  PlayableHandle_get_Null_m3770919321 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m3277333935 (ScriptableObject_t1324617639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t379060559  Playable_get_Null_m3255192774 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m1034235824 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Object::MemberwiseClone()
extern "C"  RuntimeObject * Object_MemberwiseClone_m1289829472 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_IsValidInternal_m2490534168 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m2355173092 (PlayableHandle_t1495371730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_INTERNAL_CALL_IsValidInternal_m1162379998 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2171092277 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m2066103627 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730  ___lhs0, PlayableHandle_t1495371730  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m3886890033 (PlayableHandle_t1495371730 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::GetHashCode()
extern "C"  int32_t IntPtr_GetHashCode_m1370834483 (intptr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m1004790790 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m2845659099 (PlayableHandle_t1495371730 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3432723813 (RuntimeObject * __this /* static, unused */, intptr_t p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m2314577909 (PlayableOutput_t2773044158 * __this, PlayableOutputHandle_t1247041817  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t1247041817  PlayableOutput_GetHandle_m825383068 (PlayableOutput_t2773044158 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m1707532421 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1247041817  ___lhs0, PlayableOutputHandle_t1247041817  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m4236001959 (PlayableOutput_t2773044158 * __this, PlayableOutput_t2773044158  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t1247041817  PlayableOutputHandle_get_Null_m3647248066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m1450469857 (PlayableOutputHandle_t1247041817 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m4251769876 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1247041817  ___lhs0, PlayableOutputHandle_t1247041817  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m4078288475 (PlayableOutputHandle_t1247041817 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Guid::ToString(System.String)
extern "C"  String_t* Guid_ToString_m439348276 (Guid_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern "C"  void PlayerConnectionInternal_SendMessage_m417594424 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, ByteU5BU5D_t3567143369* ___data1, int32_t ___playerId2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern "C"  void PlayerConnectionInternal_RegisterInternal_m3617664716 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern "C"  bool PlayerConnectionInternal_IsConnected_m413159681 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern "C"  void PlayerConnectionInternal_DisconnectAll_m3636521271 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m30995859 (Attribute_t3130080784 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PropertyName UnityEngine.PropertyNameUtils::PropertyNameFromString(System.String)
extern "C"  PropertyName_t3787932763  PropertyNameUtils_PropertyNameFromString_m2517934648 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(UnityEngine.PropertyName)
extern "C"  void PropertyName__ctor_m3830377608 (PropertyName_t3787932763 * __this, PropertyName_t3787932763  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(System.String)
extern "C"  void PropertyName__ctor_m280309397 (PropertyName_t3787932763 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyName::.ctor(System.Int32)
extern "C"  void PropertyName__ctor_m1179362628 (PropertyName_t3787932763 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PropertyName::GetHashCode()
extern "C"  int32_t PropertyName_GetHashCode_m2446433919 (PropertyName_t3787932763 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m3722089926 (RuntimeObject * __this /* static, unused */, PropertyName_t3787932763  ___lhs0, PropertyName_t3787932763  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PropertyName::Equals(System.Object)
extern "C"  bool PropertyName_Equals_m2052821075 (PropertyName_t3787932763 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2251207222 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PropertyName::ToString()
extern "C"  String_t* PropertyName_ToString_m1539975057 (PropertyName_t3787932763 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
extern "C"  void PropertyNameUtils_PropertyNameFromString_Injected_m2751720990 (RuntimeObject * __this /* static, unused */, String_t* ___name0, PropertyName_t3787932763 * ___ret1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m2607990742 (Quaternion_t3497065016 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1900152028 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___forward0, Vector3_t3932393085 * ___upwards1, Quaternion_t3497065016 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m3257293516 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016 * ___rotation0, Quaternion_t3497065016 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2321752175 (Vector3_t3932393085 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3932393085  Vector3_op_Multiply_m2900776395 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t3497065016  Quaternion_Internal_FromEulerRad_m4014579486 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___euler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1094838250 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___euler0, Quaternion_t3497065016 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m3847016175 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___a0, Quaternion_t3497065016  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m776083444 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___lhs0, Quaternion_t3497065016  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3630296696 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m513473826 (Quaternion_t3497065016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m847285714 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m3839468225 (Quaternion_t3497065016 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m324521689 (Quaternion_t3497065016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3690955331 (PropertyAttribute_t4289523592 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m2420173727 (RangeInt_t21894369 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3932393085  Vector3_get_normalized_m1820914487 (Vector3_t3932393085 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2331331332 (Ray_t1807385980 * __this, Vector3_t3932393085  ___origin0, Vector3_t3932393085  ___direction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Vector3_op_Addition_m2513759451 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___a0, Vector3_t3932393085  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t3932393085  Ray_GetPoint_m3306741896 (Ray_t1807385980 * __this, float ___distance0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m5131743 (Ray_t1807385980 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3862784288 (Rect_t952252086 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m4084575049 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m2256120598 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3737423361 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m3387259938 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3768944458 (Vector2_t2246369278 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2246369278  Rect_get_position_m831573339 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2246369278  Rect_get_center_m3838774990 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m3005891617 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1542759231 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2246369278  Rect_get_min_m1432445198 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m1526087686 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2148438427 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2246369278  Rect_get_max_m631587217 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m341520840 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m4164729024 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m728299502 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3996664501 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2246369278  Rect_get_size_m1118171761 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m879227740 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m2932165558 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1505073453 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m2859229937 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m554593792 (Rect_t952252086 * __this, Vector2_t2246369278  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1728858246 (Rect_t952252086 * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m2657232469 (Rect_t952252086 * __this, Rect_t952252086  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t952252086  Rect_OrderMinMax_m2598564121 (RuntimeObject * __this /* static, unused */, Rect_t952252086  ___rect0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m2531020201 (Rect_t952252086 * __this, Rect_t952252086  ___other0, bool ___allowInverse1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m4068887963 (RuntimeObject * __this /* static, unused */, Rect_t952252086  ___lhs0, Rect_t952252086  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m3748807894 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m551946840 (Rect_t952252086 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m1785269746 (Rect_t952252086 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m240763499 (RectOffset_t817686154 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m463627648 (RectOffset_t817686154 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m2448312333 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m3991427146 (RectOffset_t817686154 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m664602337 (RectOffset_t817686154 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m1270760253 (RectOffset_t817686154 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2563455708 (RectOffset_t817686154 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1174113108 (RectTransform_t1161610249 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m821634638 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m3477091692 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m2987003428 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m3760943777 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m932856358 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m416641638 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m2033389748 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m1637436926 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m698036193 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m1871221030 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t4015201940 * Delegate_Combine_m4046213137 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * p0, Delegate_t4015201940 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t4015201940 * Delegate_Remove_m897454397 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * p0, Delegate_t4015201940 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m4103194093 (ReapplyDrivenProperties_t1147600571 * __this, RectTransform_t1161610249 * ___driven0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t952252086  RectTransform_get_rect_m2022616935 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m2981119692 (RectTransform_t1161610249 * __this, Vector3U5BU5D_t1432878832* ___fourCornersArray0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3580444445 * Component_get_transform_m1425942785 (Component_t2335505321 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Transform_TransformPoint_m1797663460 (Transform_t3580444445 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2246369278  RectTransform_get_anchorMin_m3262898663 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1623790336 (Vector2_t2246369278 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4291838635 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2246369278  RectTransform_get_anchorMax_m1509281336 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m3836270766 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2246369278  RectTransform_get_sizeDelta_m3804390761 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m826926724 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2246369278  RectTransform_get_anchoredPosition_m3060243242 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2246369278  RectTransform_get_pivot_m57090389 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m1427364022 (Vector2_t2246369278 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2197021314 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t2246369278  RectTransform_GetParentSize_m770305266 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3580444445 * Transform_get_parent_m82173183 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1340895429 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2246369278  Vector2_get_zero_m1981541827 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void CommandBuffer_InitBuffer_m3164580979 (RuntimeObject * __this /* static, unused */, CommandBuffer_t3383400165 * ___buf0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern "C"  void CommandBuffer_Dispose_m3807691801 (CommandBuffer_t3383400165 * __this, bool ___disposing0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1914804560 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m2150910944 (CommandBuffer_t3383400165 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void CommandBuffer_Blit_Texture_m3682817401 (CommandBuffer_t3383400165 * __this, Texture_t954505614 * ___source0, RenderTargetIdentifier_t35305570 * ___dest1, Material_t1926439680 * ___mat2, int32_t ___pass3, Vector2_t2246369278  ___scale4, Vector2_t2246369278  ___offset5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void CommandBuffer_INTERNAL_CALL_Blit_Texture_m2069604155 (RuntimeObject * __this /* static, unused */, CommandBuffer_t3383400165 * ___self0, Texture_t954505614 * ___source1, RenderTargetIdentifier_t35305570 * ___dest2, Material_t1926439680 * ___mat3, int32_t ___pass4, Vector2_t2246369278 * ___scale5, Vector2_t2246369278 * ___offset6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m3059967350 (RenderTargetIdentifier_t35305570 * __this, int32_t ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern "C"  String_t* RenderTargetIdentifier_ToString_m430154943 (RenderTargetIdentifier_t35305570 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m782971227 (RenderTargetIdentifier_t35305570 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  bool RenderTargetIdentifier_Equals_m981627877 (RenderTargetIdentifier_t35305570 * __this, RenderTargetIdentifier_t35305570  ___rhs0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern "C"  bool RenderTargetIdentifier_Equals_m2799259023 (RenderTargetIdentifier_t35305570 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m119370463 (RuntimeObject * __this /* static, unused */, RenderTexture_t3737750657 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m366723182 (RuntimeObject * __this /* static, unused */, RenderTexture_t3737750657 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m4289303328 (Resolution_t3373471800 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m1022399602 (Resolution_t3373471800 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Resolution::ToString()
extern "C"  String_t* Resolution_ToString_m1282970970 (Resolution_t3373471800 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m91486082 (Scene_t1083318001 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m4235140656 (Scene_t1083318001 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m4240418630 (Scene_t1083318001 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m2791872171(__this, p0, p1, method) ((  void (*) (UnityAction_2_t2618073252 *, Scene_t1083318001 , int32_t, const RuntimeMethod*))UnityAction_2_Invoke_m2791872171_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m1991915580(__this, p0, method) ((  void (*) (UnityAction_1_t3754315317 *, Scene_t1083318001 , const RuntimeMethod*))UnityAction_1_Invoke_m1991915580_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m2120342004(__this, p0, p1, method) ((  void (*) (UnityAction_2_t671528035 *, Scene_t1083318001 , Scene_t1083318001 , const RuntimeMethod*))UnityAction_2_Invoke_m2120342004_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Screen::INTERNAL_get_currentResolution(UnityEngine.Resolution&)
extern "C"  void Screen_INTERNAL_get_currentResolution_m2882077263 (RuntimeObject * __this /* static, unused */, Resolution_t3373471800 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m2395954052 (Object_t3645472222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m290099682 (RuntimeObject * __this /* static, unused */, ScriptableObject_t1324617639 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t1324617639 * ScriptableObject_CreateInstanceFromType_m3046537746 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m1132036607 (RequiredByNativeCodeAttribute_t2936828858 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t1634068716_m3037765150(__this, method) ((  GUILayer_t1634068716 * (*) (Component_t2335505321 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2432164368_gshared)(__this, method)
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t2757512779 * GUILayer_HitTest_m1524866709 (GUILayer_t1634068716 * __this, Vector3_t3932393085  ___screenPosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2881801184 * Component_get_gameObject_m3138408676 (Component_t2335505321 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3932393085  Input_get_mousePosition_m3071837934 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m190271839 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m635401728 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t365668710* ___cameras0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t3737750657 * Camera_get_targetTexture_m3060139219 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1808560565 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___x0, Object_t3645472222 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t952252086  Camera_get_pixelRect_m3824066811 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::HitTestLegacyGUI(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.SendMouseEvents/HitInfo&)
extern "C"  void SendMouseEvents_HitTestLegacyGUI_m1463766836 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___camera0, Vector3_t3932393085  ___mousePosition1, HitInfo_t3525137541 * ___hitInfo2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m2361608952 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t1807385980  Camera_ScreenPointToRay_m3701064168 (Camera_t362346687 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m1473945392 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m4120344687 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m4086893319 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_RaycastTry_m3889099052 (Camera_t362346687 * __this, Ray_t1807385980  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m1043993872 (Camera_t362346687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t2881801184 * Camera_RaycastTry2D_m3772444891 (Camera_t362346687 * __this, Ray_t1807385980  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m3061086367 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t3525137541  ___hit1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m4077202833 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m2505676056 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m1366484775 (RuntimeObject * __this /* static, unused */, HitInfo_t3525137541  ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m28937701 (HitInfo_t3525137541 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m804408274 (RuntimeObject * __this /* static, unused */, HitInfo_t3525137541  ___lhs0, HitInfo_t3525137541  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m4104693933 (GameObject_t2881801184 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m158539512 (ArgumentException_t489606696 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m259387555 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m392475457 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m53359737 (Sprite_t2497061588 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m3501512419 (Sprite_t2497061588 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m463112409 (Sprite_t2497061588 * __this, Vector4_t1900979187 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m2615385289 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector4_t1900979187 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m1659787860 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector4_t1900979187 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m3631929608 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector4_t1900979187 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m955661019 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector2_t2246369278 * ___output1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m2123998715 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Int32,System.Boolean)
extern "C"  void StackTrace__ctor_m3276062597 (StackTrace_t1780976090 * __this, int32_t p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m3595538786 (RuntimeObject * __this /* static, unused */, StackTrace_t1780976090 * ___stackTrace0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m2243296700 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m293866894 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m1547787485 (StringBuilder_t209334805 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3213599556 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Exception::GetType()
extern "C"  Type_t * Exception_GetType_m1907036829 (Exception_t214279536 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Trim()
extern "C"  String_t* String_Trim_m3557264989 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2806582524 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::get_InnerException()
extern "C"  Exception_t214279536 * Exception_get_InnerException_m4271498349 (Exception_t214279536 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m333645061 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t209334805 * StringBuilder_Append_m3622130694 (StringBuilder_t209334805 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1589106382* String_Split_m3508706380 (String_t* __this, CharU5BU5D_t2953840665* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m1611231861 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m3834892923 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m3835433624 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1162620119 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m2337007599 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Remove(System.Int32,System.Int32)
extern "C"  String_t* String_Remove_m4198834713 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String,System.Int32)
extern "C"  int32_t String_IndexOf_m4283618274 (String_t* __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.Char,System.Char)
extern "C"  String_t* String_Replace_m3590694696 (String_t* __this, Il2CppChar p0, Il2CppChar p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.String)
extern "C"  int32_t String_LastIndexOf_m1985287707 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Insert(System.Int32,System.String)
extern "C"  String_t* String_Insert_m2763449893 (String_t* __this, int32_t p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m3241538055 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m142941626 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m3807402336 (RuntimeObject * __this /* static, unused */, Texture_t954505614 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m3706805582 (RuntimeObject * __this /* static, unused */, Texture_t954505614 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m1340325120 (Texture_t954505614 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m57122759 (Texture_t954505614 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m3280886297 (RuntimeObject * __this /* static, unused */, Texture2D_t91252194 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, intptr_t ___nativeTex6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D__ctor_m1755139504 (Texture2D_t91252194 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m3746843605 (RuntimeObject * __this /* static, unused */, Texture2D_t91252194 * ___self0, float ___u1, float ___v2, Color_t1361298052 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m415162269 (Texture2D_t91252194 * __this, Color32U5BU5D_t2352998046* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m2370990271 (Texture2D_t91252194 * __this, Color32U5BU5D_t2352998046* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m3719091099 (Texture2D_t91252194 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t2352998046* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetBlockOfPixels32_m727128491 (Texture2D_t91252194 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t2352998046* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m1388850756 (Texture2D_t91252194 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m4171647058 (Touch_t2641233222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2246369278  Touch_get_position_m3725642488 (Touch_t2641233222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m2788782435 (Touch_t2641233222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3850289965 (Touch_t2641233222 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Object)
extern "C"  uint32_t Convert_ToUInt32_m1199381511 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Boolean)
extern "C"  uint32_t Convert_ToUInt32_m3661845794 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3742394844 (TouchScreenKeyboard_t1683994280 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m3424133161 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m2201190236 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t1683994280 * TouchScreenKeyboard_Open_m2096239018 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m3688752920 (TouchScreenKeyboard_t1683994280 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m1674507664 (TouchScreenKeyboard_t1683994280 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m357163524 (RuntimeObject * __this /* static, unused */, TrackedReference_t2612049617 * ___x0, TrackedReference_t2612049617 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  int32_t IntPtr_op_Explicit_m4119414380 (RuntimeObject * __this /* static, unused */, intptr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3044674138 (Component_t2335505321 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m3677915992 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m1521839669 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m103217147 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m793065216 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t3497065016  Transform_get_rotation_m444413307 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3932393085  Vector3_get_forward_m4214875591 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Quaternion_op_Multiply_m675595747 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___rotation0, Vector3_t3932393085  ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m1170447236 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m2992445136 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m1998419987 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m1657065507 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m820755046 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m4270159898 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t3580444445 * Transform_get_parentInternal_m584767176 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m764251424 (Transform_t3580444445 * __this, Transform_t3580444445 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m3849428052 (Transform_t3580444445 * __this, Matrix4x4_t787966842 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m4199768081 (RuntimeObject * __this /* static, unused */, Transform_t3580444445 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m1890131684 (RuntimeObject * __this /* static, unused */, Transform_t3580444445 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m3645558831 (Enumerator_t3595687596 * __this, Transform_t3580444445 * ___outer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3580444445 * Transform_GetChild_m1886946583 (Transform_t3580444445 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2090497278 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.U2D.SpriteAtlas>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3744639618(__this, p0, p1, method) ((  void (*) (Action_1_t260058957 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m1757200534_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m1447585754 (RequestAtlasCallback_t3823435737 * __this, String_t* ___tag0, Action_1_t260058957 * ___action1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
extern "C"  AppDomain_t2391992810 * AppDomain_get_CurrentDomain_m529402762 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UnhandledExceptionEventHandler__ctor_m3215020204 (UnhandledExceptionEventHandler_t1143045648 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_UnhandledException(System.UnhandledExceptionEventHandler)
extern "C"  void AppDomain_add_UnhandledException_m1469772753 (AppDomain_t2391992810 * __this, UnhandledExceptionEventHandler_t1143045648 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern "C"  RuntimeObject * UnhandledExceptionEventArgs_get_ExceptionObject_m3876870412 (UnhandledExceptionEventArgs_t2712850390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m1032894626 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t214279536 * ___e1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m1375071567 (RuntimeObject * __this /* static, unused */, Exception_t214279536 * ___exception0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m73287453 (Exception_t214279536 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_HResult(System.Int32)
extern "C"  void Exception_set_HResult_m3235753660 (Exception_t214279536 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Exception__ctor_m3789217139 (Exception_t214279536 * __this, SerializationInfo_t1812684756 * p0, StreamingContext_t1986025897  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::.ctor()
extern "C"  void TextWriter__ctor_m2762039731 (TextWriter_t4111495313 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::.ctor()
extern "C"  void UnityLogWriter__ctor_m1903404459 (UnityLogWriter_t3161904322 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetOut(System.IO.TextWriter)
extern "C"  void Console_SetOut_m2860715686 (RuntimeObject * __this /* static, unused */, TextWriter_t4111495313 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m19900144 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern "C"  void UnityLogWriter_WriteStringToUnityLog_m2967587076 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m3139124310 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3385344125* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
#define Queue_1__ctor_m1321454259(__this, p0, method) ((  void (*) (Queue_1_t2402182407 *, int32_t, const RuntimeMethod*))Queue_1__ctor_m1321454259_gshared)(__this, p0, method)
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t2508967565 * Thread_get_CurrentThread_m2158879202 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m1330586752 (Thread_t2508967565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::.ctor()
extern "C"  void SynchronizationContext__ctor_m1659093484 (SynchronizationContext_t3011612328 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m4131414619 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
#define Queue_1_Dequeue_m1134160965(__this, method) ((  WorkRequest_t266095197  (*) (Queue_1_t2402182407 *, const RuntimeMethod*))Queue_1_Dequeue_m1134160965_gshared)(__this, method)
// System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern "C"  void WorkRequest_Invoke_m139019247 (WorkRequest_t266095197 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
#define Queue_1_get_Count_m2804029644(__this, method) ((  int32_t (*) (Queue_1_t2402182407 *, const RuntimeMethod*))Queue_1_get_Count_m2804029644_gshared)(__this, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m1585917014 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::get_Current()
extern "C"  SynchronizationContext_t3011612328 * SynchronizationContext_get_Current_m1340098188 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnitySynchronizationContext::.ctor()
extern "C"  void UnitySynchronizationContext__ctor_m842443118 (UnitySynchronizationContext_t1450698442 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetSynchronizationContext(System.Threading.SynchronizationContext)
extern "C"  void SynchronizationContext_SetSynchronizationContext_m1476995085 (RuntimeObject * __this /* static, unused */, SynchronizationContext_t3011612328 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern "C"  void UnitySynchronizationContext_Exec_m2847479435 (UnitySynchronizationContext_t1450698442 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SendOrPostCallback::Invoke(System.Object)
extern "C"  void SendOrPostCallback_Invoke_m3562526081 (SendOrPostCallback_t1528611714 * __this, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Set()
extern "C"  bool EventWaitHandle_Set_m3353019311 (EventWaitHandle_t1398859231 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m4093382970 (IndexOutOfRangeException_t3659678378 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m2797960148 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m3641689616 (Vector2_t2246369278 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m2227745196 (Vector2_t2246369278 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m258839576 (Vector2_t2246369278 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m423695516 (Vector2_t2246369278 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2246369278  Vector2_op_Subtraction_m3277110115 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, Vector2_t2246369278  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m512848965 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___lhs0, Vector2_t2246369278  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m2224928885 (PlayerEditorConnectionEvents_t1009044977 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents__ctor_m2224928885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2349647505 * L_0 = (List_1_t2349647505 *)il2cpp_codegen_object_new(List_1_t2349647505_il2cpp_TypeInfo_var);
		List_1__ctor_m164277227(L_0, /*hidden argument*/List_1__ctor_m164277227_RuntimeMethod_var);
		__this->set_messageTypeSubscribers_0(L_0);
		ConnectionChangeEvent_t190635484 * L_1 = (ConnectionChangeEvent_t190635484 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t190635484_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m2384156855(L_1, /*hidden argument*/NULL);
		__this->set_connectionEvent_1(L_1);
		ConnectionChangeEvent_t190635484 * L_2 = (ConnectionChangeEvent_t190635484 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t190635484_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m2384156855(L_2, /*hidden argument*/NULL);
		__this->set_disconnectionEvent_2(L_2);
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4106540626 (PlayerEditorConnectionEvents_t1009044977 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3567143369* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m4106540626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	MessageEventArgs_t2772358345 * V_2 = NULL;
	MessageEventArgs_t2772358345 * V_3 = NULL;
	MessageTypeSubscribers_t675444414 * V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * L_0 = (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 *)il2cpp_codegen_object_new(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445_il2cpp_TypeInfo_var);
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m2273264663(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * L_1 = V_0;
		Guid_t  L_2 = ___messageId0;
		NullCheck(L_1);
		L_1->set_messageId_0(L_2);
		List_1_t2349647505 * L_3 = __this->get_messageTypeSubscribers_0();
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m2724902477_RuntimeMethod_var;
		Func_2_t2013519524 * L_6 = (Func_2_t2013519524 *)il2cpp_codegen_object_new(Func_2_t2013519524_il2cpp_TypeInfo_var);
		Func_2__ctor_m2035350960(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m2035350960_RuntimeMethod_var);
		RuntimeObject* L_7 = Enumerable_Where_TisMessageTypeSubscribers_t675444414_m1955804045(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_Where_TisMessageTypeSubscribers_t675444414_m1955804045_RuntimeMethod_var);
		V_1 = L_7;
		RuntimeObject* L_8 = V_1;
		bool L_9 = Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_Any_TisMessageTypeSubscribers_t675444414_m2850005413_RuntimeMethod_var);
		if (L_9)
		{
			goto IL_0051;
		}
	}
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * L_10 = V_0;
		NullCheck(L_10);
		Guid_t  L_11 = L_10->get_messageId_0();
		Guid_t  L_12 = L_11;
		RuntimeObject * L_13 = Box(Guid_t_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1113447374(NULL /*static, unused*/, _stringLiteral2648312538, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogError_m2611403583(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_0051:
	{
		MessageEventArgs_t2772358345 * L_15 = (MessageEventArgs_t2772358345 *)il2cpp_codegen_object_new(MessageEventArgs_t2772358345_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m3912586882(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		MessageEventArgs_t2772358345 * L_16 = V_3;
		int32_t L_17 = ___playerId2;
		NullCheck(L_16);
		L_16->set_playerId_0(L_17);
		MessageEventArgs_t2772358345 * L_18 = V_3;
		ByteU5BU5D_t3567143369* L_19 = ___data1;
		NullCheck(L_18);
		L_18->set_data_1(L_19);
		MessageEventArgs_t2772358345 * L_20 = V_3;
		V_2 = L_20;
		RuntimeObject* L_21 = V_1;
		NullCheck(L_21);
		RuntimeObject* L_22 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::GetEnumerator() */, IEnumerable_1_t762398783_il2cpp_TypeInfo_var, L_21);
		V_5 = L_22;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008d;
		}

IL_0075:
		{
			RuntimeObject* L_23 = V_5;
			NullCheck(L_23);
			MessageTypeSubscribers_t675444414 * L_24 = InterfaceFuncInvoker0< MessageTypeSubscribers_t675444414 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::get_Current() */, IEnumerator_1_t2085253399_il2cpp_TypeInfo_var, L_23);
			V_4 = L_24;
			MessageTypeSubscribers_t675444414 * L_25 = V_4;
			NullCheck(L_25);
			MessageEvent_t3114892025 * L_26 = L_25->get_messageCallback_2();
			MessageEventArgs_t2772358345 * L_27 = V_2;
			NullCheck(L_26);
			UnityEvent_1_Invoke_m2359140723(L_26, L_27, /*hidden argument*/UnityEvent_1_Invoke_m2359140723_RuntimeMethod_var);
		}

IL_008d:
		{
			RuntimeObject* L_28 = V_5;
			NullCheck(L_28);
			bool L_29 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2051810174_il2cpp_TypeInfo_var, L_28);
			if (L_29)
			{
				goto IL_0075;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_30 = V_5;
			if (!L_30)
			{
				goto IL_00ac;
			}
		}

IL_00a5:
		{
			RuntimeObject* L_31 = V_5;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3181680589_il2cpp_TypeInfo_var, L_31);
		}

IL_00ac:
		{
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_00ad:
	{
		return;
	}
}
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern "C"  UnityEvent_1_t1459204717 * PlayerEditorConnectionEvents_AddAndCreate_m378817323 (PlayerEditorConnectionEvents_t1009044977 * __this, Guid_t  ___messageId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents_AddAndCreate_m378817323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * V_0 = NULL;
	MessageTypeSubscribers_t675444414 * V_1 = NULL;
	MessageTypeSubscribers_t675444414 * V_2 = NULL;
	UnityEvent_1_t1459204717 * V_3 = NULL;
	{
		U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * L_0 = (U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 *)il2cpp_codegen_object_new(U3CAddAndCreateU3Ec__AnonStorey1_t3362117636_il2cpp_TypeInfo_var);
		U3CAddAndCreateU3Ec__AnonStorey1__ctor_m614469706(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * L_1 = V_0;
		Guid_t  L_2 = ___messageId0;
		NullCheck(L_1);
		L_1->set_messageId_0(L_2);
		List_1_t2349647505 * L_3 = __this->get_messageTypeSubscribers_0();
		U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m1575543412_RuntimeMethod_var;
		Func_2_t2013519524 * L_6 = (Func_2_t2013519524 *)il2cpp_codegen_object_new(Func_2_t2013519524_il2cpp_TypeInfo_var);
		Func_2__ctor_m2035350960(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m2035350960_RuntimeMethod_var);
		MessageTypeSubscribers_t675444414 * L_7 = Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t675444414_m557389689(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_SingleOrDefault_TisMessageTypeSubscribers_t675444414_m557389689_RuntimeMethod_var);
		V_1 = L_7;
		MessageTypeSubscribers_t675444414 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0059;
		}
	}
	{
		MessageTypeSubscribers_t675444414 * L_9 = (MessageTypeSubscribers_t675444414 *)il2cpp_codegen_object_new(MessageTypeSubscribers_t675444414_il2cpp_TypeInfo_var);
		MessageTypeSubscribers__ctor_m1862007631(L_9, /*hidden argument*/NULL);
		V_2 = L_9;
		MessageTypeSubscribers_t675444414 * L_10 = V_2;
		U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * L_11 = V_0;
		NullCheck(L_11);
		Guid_t  L_12 = L_11->get_messageId_0();
		NullCheck(L_10);
		MessageTypeSubscribers_set_MessageTypeId_m1372988085(L_10, L_12, /*hidden argument*/NULL);
		MessageTypeSubscribers_t675444414 * L_13 = V_2;
		MessageEvent_t3114892025 * L_14 = (MessageEvent_t3114892025 *)il2cpp_codegen_object_new(MessageEvent_t3114892025_il2cpp_TypeInfo_var);
		MessageEvent__ctor_m3036318349(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_messageCallback_2(L_14);
		MessageTypeSubscribers_t675444414 * L_15 = V_2;
		V_1 = L_15;
		List_1_t2349647505 * L_16 = __this->get_messageTypeSubscribers_0();
		MessageTypeSubscribers_t675444414 * L_17 = V_1;
		NullCheck(L_16);
		List_1_Add_m1330338227(L_16, L_17, /*hidden argument*/List_1_Add_m1330338227_RuntimeMethod_var);
	}

IL_0059:
	{
		MessageTypeSubscribers_t675444414 * L_18 = V_1;
		MessageTypeSubscribers_t675444414 * L_19 = L_18;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_subscriberCount_1();
		NullCheck(L_19);
		L_19->set_subscriberCount_1(((int32_t)((int32_t)L_20+(int32_t)1)));
		MessageTypeSubscribers_t675444414 * L_21 = V_1;
		NullCheck(L_21);
		MessageEvent_t3114892025 * L_22 = L_21->get_messageCallback_2();
		V_3 = L_22;
		goto IL_0073;
	}

IL_0073:
	{
		UnityEvent_1_t1459204717 * L_23 = V_3;
		return L_23;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::.ctor()
extern "C"  void U3CAddAndCreateU3Ec__AnonStorey1__ctor_m614469706 (U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<AddAndCreate>c__AnonStorey1::<>m__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern "C"  bool U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m1575543412 (U3CAddAndCreateU3Ec__AnonStorey1_t3362117636 * __this, MessageTypeSubscribers_t675444414 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAddAndCreateU3Ec__AnonStorey1_U3CU3Em__0_m1575543412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		MessageTypeSubscribers_t675444414 * L_0 = ___x0;
		NullCheck(L_0);
		Guid_t  L_1 = MessageTypeSubscribers_get_MessageTypeId_m409108014(L_0, /*hidden argument*/NULL);
		Guid_t  L_2 = __this->get_messageId_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_3 = Guid_op_Equality_m2100028761(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m2273264663 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::<>m__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern "C"  bool U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m2724902477 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t992766445 * __this, MessageTypeSubscribers_t675444414 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m2724902477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		MessageTypeSubscribers_t675444414 * L_0 = ___x0;
		NullCheck(L_0);
		Guid_t  L_1 = MessageTypeSubscribers_get_MessageTypeId_m409108014(L_0, /*hidden argument*/NULL);
		Guid_t  L_2 = __this->get_messageId_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_3 = Guid_op_Equality_m2100028761(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m2384156855 (ConnectionChangeEvent_t190635484 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionChangeEvent__ctor_m2384156855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m1358771383(__this, /*hidden argument*/UnityEvent_1__ctor_m1358771383_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m3036318349 (MessageEvent_t3114892025 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEvent__ctor_m3036318349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m1455957073(__this, /*hidden argument*/UnityEvent_1__ctor_m1455957073_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern "C"  void MessageTypeSubscribers__ctor_m1862007631 (MessageTypeSubscribers_t675444414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageTypeSubscribers__ctor_m1862007631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_subscriberCount_1(0);
		MessageEvent_t3114892025 * L_0 = (MessageEvent_t3114892025 *)il2cpp_codegen_object_new(MessageEvent_t3114892025_il2cpp_TypeInfo_var);
		MessageEvent__ctor_m3036318349(L_0, /*hidden argument*/NULL);
		__this->set_messageCallback_2(L_0);
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m409108014 (MessageTypeSubscribers_t675444414 * __this, const RuntimeMethod* method)
{
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_messageTypeId_0();
		Guid_t  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m869444109((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Guid_t  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern "C"  void MessageTypeSubscribers_set_MessageTypeId_m1372988085 (MessageTypeSubscribers_t675444414 * __this, Guid_t  ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Guid_ToString_m1679002259((&___value0), /*hidden argument*/NULL);
		__this->set_m_messageTypeId_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3645472222_marshal_pinvoke(const Object_t3645472222& unmarshaled, Object_t3645472222_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void Object_t3645472222_marshal_pinvoke_back(const Object_t3645472222_marshaled_pinvoke& marshaled, Object_t3645472222& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3645472222_marshal_pinvoke_cleanup(Object_t3645472222_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3645472222_marshal_com(const Object_t3645472222& unmarshaled, Object_t3645472222_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void Object_t3645472222_marshal_com_back(const Object_t3645472222_marshaled_com& marshaled, Object_t3645472222& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3645472222_marshal_com_cleanup(Object_t3645472222_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m2395954052 (Object_t3645472222 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t3645472222 * Object_Internal_CloneSingle_m2864123682 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___data0, const RuntimeMethod* method)
{
	typedef Object_t3645472222 * (*Object_Internal_CloneSingle_m2864123682_ftn) (Object_t3645472222 *);
	static Object_Internal_CloneSingle_m2864123682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m2864123682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	Object_t3645472222 * retVal = _il2cpp_icall_func(___data0);
	return retVal;
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t3645472222 * Object_Internal_InstantiateSingle_m767057946 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___data0, Vector3_t3932393085  ___pos1, Quaternion_t3497065016  ___rot2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m767057946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t3645472222 * V_0 = NULL;
	{
		Object_t3645472222 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object_t3645472222 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m2913152063(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t3645472222 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t3645472222 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m2913152063 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___data0, Vector3_t3932393085 * ___pos1, Quaternion_t3497065016 * ___rot2, const RuntimeMethod* method)
{
	typedef Object_t3645472222 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m2913152063_ftn) (Object_t3645472222 *, Vector3_t3932393085 *, Quaternion_t3497065016 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m2913152063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m2913152063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	Object_t3645472222 * retVal = _il2cpp_icall_func(___data0, ___pos1, ___rot2);
	return retVal;
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m989819893 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___obj0, float ___t1, const RuntimeMethod* method)
{
	typedef void (*Object_Destroy_m989819893_ftn) (Object_t3645472222 *, float);
	static Object_Destroy_m989819893_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m989819893_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m731272364 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m731272364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3645472222 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object_Destroy_m989819893(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m1498630165 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method)
{
	typedef void (*Object_DestroyImmediate_m1498630165_ftn) (Object_t3645472222 *, bool);
	static Object_DestroyImmediate_m1498630165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m1498630165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m1573878054 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m1573878054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t3645472222 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1498630165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m527277425 (Object_t3645472222 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_get_name_m527277425_ftn) (Object_t3645472222 *);
	static Object_get_name_m527277425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m527277425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m3730332729 (Object_t3645472222 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_name_m3730332729_ftn) (Object_t3645472222 *, String_t*);
	static Object_set_name_m3730332729_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m3730332729_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m3187503400 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___target0, const RuntimeMethod* method)
{
	typedef void (*Object_DontDestroyOnLoad_m3187503400_ftn) (Object_t3645472222 *);
	static Object_DontDestroyOnLoad_m3187503400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m3187503400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2254825582 (Object_t3645472222 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_hideFlags_m2254825582_ftn) (Object_t3645472222 *, int32_t);
	static Object_set_hideFlags_m2254825582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2254825582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m362212118 (Object_t3645472222 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_ToString_m362212118_ftn) (Object_t3645472222 *);
	static Object_ToString_m362212118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m362212118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m21289391 (Object_t3645472222 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m2582846073(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern "C"  bool Object_Equals_m1066417117 (Object_t3645472222 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m1066417117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t3645472222 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___other0;
		V_0 = ((Object_t3645472222 *)IsInstClass((RuntimeObject*)L_0, Object_t3645472222_il2cpp_TypeInfo_var));
		Object_t3645472222 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_1, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_4 = ___other0;
		if (((Object_t3645472222 *)IsInstClass((RuntimeObject*)L_4, Object_t3645472222_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t3645472222 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m3329896178(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1340895429 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m1340895429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t3645472222 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3329896178(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3329896178 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___lhs0, Object_t3645472222 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3329896178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t3645472222 * L_0 = ___lhs0;
		V_0 = (bool)((((RuntimeObject*)(Object_t3645472222 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		Object_t3645472222 * L_1 = ___rhs1;
		V_1 = (bool)((((RuntimeObject*)(Object_t3645472222 *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t3645472222 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m3366595570(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t3645472222 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m3366595570(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t3645472222 * L_10 = ___lhs0;
		Object_t3645472222 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m898979290(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m3366595570 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m3366595570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t3645472222 * L_0 = ___o0;
		NullCheck(L_0);
		intptr_t L_1 = Object_GetCachedPtr_m597461268(L_0, /*hidden argument*/NULL);
		bool L_2 = IntPtr_op_Inequality_m183498676(NULL /*static, unused*/, L_1, (intptr_t)(0), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  intptr_t Object_GetCachedPtr_m597461268 (Object_t3645472222 * __this, const RuntimeMethod* method)
{
	intptr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		intptr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		intptr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t3645472222 * Object_Instantiate_m3076090176 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___original0, Vector3_t3932393085  ___position1, Quaternion_t3497065016  ___rotation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m3076090176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t3645472222 * V_0 = NULL;
	{
		Object_t3645472222 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m3054168192(NULL /*static, unused*/, L_0, _stringLiteral3769644588, /*hidden argument*/NULL);
		Object_t3645472222 * L_1 = ___original0;
		if (!((ScriptableObject_t1324617639 *)IsInstClass((RuntimeObject*)L_1, ScriptableObject_t1324617639_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_2, _stringLiteral4211405290, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t3645472222 * L_3 = ___original0;
		Vector3_t3932393085  L_4 = ___position1;
		Quaternion_t3497065016  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object_t3645472222 * L_6 = Object_Internal_InstantiateSingle_m767057946(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t3645472222 * L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m3054168192 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m3054168192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3342981539 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___x0, Object_t3645472222 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m3342981539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t3645472222 * L_0 = ___x0;
		Object_t3645472222 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3329896178(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1808560565 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * ___x0, Object_t3645472222 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m1808560565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t3645472222 * L_0 = ___x0;
		Object_t3645472222 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3329896178(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern "C"  void Object__cctor_m649958370 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m649958370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t3645472222_StaticFields*)il2cpp_codegen_static_fields_for(Object_t3645472222_il2cpp_TypeInfo_var))->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m1011206029 (Plane_t4100015541 * __this, Vector3_t3932393085  ___inNormal0, Vector3_t3932393085  ___inPoint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane__ctor_m1011206029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3932393085  L_0 = ___inNormal0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_1 = Vector3_Normalize_m4093520232(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t3932393085  L_2 = __this->get_m_Normal_0();
		Vector3_t3932393085  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m2615379767(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m1011206029_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___inNormal0, Vector3_t3932393085  ___inPoint1, const RuntimeMethod* method)
{
	Plane_t4100015541 * _thisAdjusted = reinterpret_cast<Plane_t4100015541 *>(__this + 1);
	Plane__ctor_m1011206029(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m894877444 (Plane_t4100015541 * __this, Ray_t1807385980  ___ray0, float* ___enter1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m894877444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector3_t3932393085  L_0 = Ray_get_direction_m2057318423((&___ray0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_1 = __this->get_m_Normal_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m2615379767(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3932393085  L_3 = Ray_get_origin_m1394134582((&___ray0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_4 = __this->get_m_Normal_0();
		float L_5 = Vector3_Dot_m2615379767(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_m_Distance_1();
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m4050652555(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		V_2 = (bool)0;
		goto IL_0062;
	}

IL_004e:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		V_2 = (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
		goto IL_0062;
	}

IL_0062:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
extern "C"  bool Plane_Raycast_m894877444_AdjustorThunk (RuntimeObject * __this, Ray_t1807385980  ___ray0, float* ___enter1, const RuntimeMethod* method)
{
	Plane_t4100015541 * _thisAdjusted = reinterpret_cast<Plane_t4100015541 *>(__this + 1);
	return Plane_Raycast_m894877444(_thisAdjusted, ___ray0, ___enter1, method);
}
// System.String UnityEngine.Plane::ToString()
extern "C"  String_t* Plane_ToString_m624364449 (Plane_t4100015541 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_ToString_m624364449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)4));
		Vector3_t3932393085 * L_1 = __this->get_address_of_m_Normal_0();
		float L_2 = L_1->get_x_1();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_4);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3385344125* L_5 = L_0;
		Vector3_t3932393085 * L_6 = __this->get_address_of_m_Normal_0();
		float L_7 = L_6->get_y_2();
		float L_8 = L_7;
		RuntimeObject * L_9 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3385344125* L_10 = L_5;
		Vector3_t3932393085 * L_11 = __this->get_address_of_m_Normal_0();
		float L_12 = L_11->get_z_3();
		float L_13 = L_12;
		RuntimeObject * L_14 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_14);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_14);
		ObjectU5BU5D_t3385344125* L_15 = L_10;
		float L_16 = __this->get_m_Distance_1();
		float L_17 = L_16;
		RuntimeObject * L_18 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_18);
		String_t* L_19 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral189236002, L_15, /*hidden argument*/NULL);
		V_0 = L_19;
		goto IL_005e;
	}

IL_005e:
	{
		String_t* L_20 = V_0;
		return L_20;
	}
}
extern "C"  String_t* Plane_ToString_m624364449_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Plane_t4100015541 * _thisAdjusted = reinterpret_cast<Plane_t4100015541 *>(__this + 1);
	return Plane_ToString_m624364449(_thisAdjusted, method);
}
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m856599787 (Playable_t379060559 * __this, PlayableHandle_t1495371730  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1495371730  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void Playable__ctor_m856599787_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1495371730  ___handle0, const RuntimeMethod* method)
{
	Playable_t379060559 * _thisAdjusted = reinterpret_cast<Playable_t379060559 *>(__this + 1);
	Playable__ctor_m856599787(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t379060559  Playable_get_Null_m3255192774 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_get_Null_m3255192774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t379060559  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t379060559_il2cpp_TypeInfo_var);
		Playable_t379060559  L_0 = ((Playable_t379060559_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t379060559_il2cpp_TypeInfo_var))->get_m_NullPlayable_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Playable_t379060559  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t1495371730  Playable_GetHandle_m1375248212 (Playable_t379060559 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1495371730  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1495371730  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1495371730  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1495371730  Playable_GetHandle_m1375248212_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Playable_t379060559 * _thisAdjusted = reinterpret_cast<Playable_t379060559 *>(__this + 1);
	return Playable_GetHandle_m1375248212(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m3029208553 (Playable_t379060559 * __this, Playable_t379060559  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1495371730  L_0 = Playable_GetHandle_m1375248212(__this, /*hidden argument*/NULL);
		PlayableHandle_t1495371730  L_1 = Playable_GetHandle_m1375248212((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m1158199562(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool Playable_Equals_m3029208553_AdjustorThunk (RuntimeObject * __this, Playable_t379060559  ___other0, const RuntimeMethod* method)
{
	Playable_t379060559 * _thisAdjusted = reinterpret_cast<Playable_t379060559 *>(__this + 1);
	return Playable_Equals_m3029208553(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.Playable::.cctor()
extern "C"  void Playable__cctor_m1953763072 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable__cctor_m1953763072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1495371730  L_0 = PlayableHandle_get_Null_m3770919321(NULL /*static, unused*/, /*hidden argument*/NULL);
		Playable_t379060559  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m856599787((&L_1), L_0, /*hidden argument*/NULL);
		((Playable_t379060559_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t379060559_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern "C"  void PlayableAsset__ctor_m603547384 (PlayableAsset_t3894937101 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m3277333935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern "C"  double PlayableAsset_get_duration_m562987568 (PlayableAsset_t3894937101 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_get_duration_m562987568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayableBinding_t2941134609_il2cpp_TypeInfo_var);
		double L_0 = ((PlayableBinding_t2941134609_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t2941134609_il2cpp_TypeInfo_var))->get_DefaultDuration_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		double L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern "C"  void PlayableAsset_Internal_CreatePlayable_m3003537458 (RuntimeObject * __this /* static, unused */, PlayableAsset_t3894937101 * ___asset0, PlayableGraph_t394364097  ___graph1, GameObject_t2881801184 * ___go2, intptr_t ___ptr3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_Internal_CreatePlayable_m3003537458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t379060559  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t379060559 * V_1 = NULL;
	{
		PlayableAsset_t3894937101 * L_0 = ___asset0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t379060559_il2cpp_TypeInfo_var);
		Playable_t379060559  L_2 = Playable_get_Null_m3255192774(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0018:
	{
		PlayableAsset_t3894937101 * L_3 = ___asset0;
		PlayableGraph_t394364097  L_4 = ___graph1;
		GameObject_t2881801184 * L_5 = ___go2;
		NullCheck(L_3);
		Playable_t379060559  L_6 = VirtFuncInvoker2< Playable_t379060559 , PlayableGraph_t394364097 , GameObject_t2881801184 * >::Invoke(4 /* UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject) */, L_3, L_4, L_5);
		V_0 = L_6;
	}

IL_0021:
	{
		void* L_7 = IntPtr_ToPointer_m1034235824((&___ptr3), /*hidden argument*/NULL);
		V_1 = (Playable_t379060559 *)L_7;
		Playable_t379060559 * L_8 = V_1;
		Playable_t379060559  L_9 = V_0;
		*(Playable_t379060559 *)L_8 = L_9;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern "C"  void PlayableAsset_Internal_GetPlayableAssetDuration_m3973093308 (RuntimeObject * __this /* static, unused */, PlayableAsset_t3894937101 * ___asset0, intptr_t ___ptrToDouble1, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double* V_1 = NULL;
	{
		PlayableAsset_t3894937101 * L_0 = ___asset0;
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(5 /* System.Double UnityEngine.Playables.PlayableAsset::get_duration() */, L_0);
		V_0 = L_1;
		void* L_2 = IntPtr_ToPointer_m1034235824((&___ptrToDouble1), /*hidden argument*/NULL);
		V_1 = (double*)L_2;
		double* L_3 = V_1;
		double L_4 = V_0;
		*((double*)(L_3)) = (double)L_4;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern "C"  void PlayableBehaviour__ctor_m1153317207 (PlayableBehaviour_t716264763 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern "C"  RuntimeObject * PlayableBehaviour_Clone_m838728640 (PlayableBehaviour_t716264763 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = Object_MemberwiseClone_m1289829472(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t2941134609_marshal_pinvoke(const PlayableBinding_t2941134609& unmarshaled, PlayableBinding_t2941134609_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t2941134609_marshal_pinvoke_back(const PlayableBinding_t2941134609_marshaled_pinvoke& marshaled, PlayableBinding_t2941134609& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t2941134609_marshal_pinvoke_cleanup(PlayableBinding_t2941134609_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t2941134609_marshal_com(const PlayableBinding_t2941134609& unmarshaled, PlayableBinding_t2941134609_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t2941134609_marshal_com_back(const PlayableBinding_t2941134609_marshaled_com& marshaled, PlayableBinding_t2941134609& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t2941134609_marshal_com_cleanup(PlayableBinding_t2941134609_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern "C"  void PlayableBinding__cctor_m3021184850 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableBinding__cctor_m3021184850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayableBinding_t2941134609_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t2941134609_il2cpp_TypeInfo_var))->set_None_0(((PlayableBindingU5BU5D_t1865297292*)SZArrayNew(PlayableBindingU5BU5D_t1865297292_il2cpp_TypeInfo_var, (uint32_t)0)));
		((PlayableBinding_t2941134609_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t2941134609_il2cpp_TypeInfo_var))->set_DefaultDuration_1((std::numeric_limits<double>::infinity()));
		return;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m2355173092 (PlayableHandle_t1495371730 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = PlayableHandle_IsValidInternal_m2490534168(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool PlayableHandle_IsValid_m2355173092_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1495371730 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1495371730 *>(__this + 1);
	return PlayableHandle_IsValid_m2355173092(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_IsValidInternal_m2490534168 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1495371730 * L_0 = ___playable0;
		bool L_1 = PlayableHandle_INTERNAL_CALL_IsValidInternal_m1162379998(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_INTERNAL_CALL_IsValidInternal_m1162379998 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method)
{
	typedef bool (*PlayableHandle_INTERNAL_CALL_IsValidInternal_m1162379998_ftn) (PlayableHandle_t1495371730 *);
	static PlayableHandle_INTERNAL_CALL_IsValidInternal_m1162379998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_IsValidInternal_m1162379998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// System.Type UnityEngine.Playables.PlayableHandle::GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_GetPlayableTypeOf_m3679489103 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method)
{
	Type_t * V_0 = NULL;
	{
		PlayableHandle_t1495371730 * L_0 = ___playable0;
		Type_t * L_1 = PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2171092277(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
// System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2171092277 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730 * ___playable0, const RuntimeMethod* method)
{
	typedef Type_t * (*PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2171092277_ftn) (PlayableHandle_t1495371730 *);
	static PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2171092277_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m2171092277_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)");
	Type_t * retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t1495371730  PlayableHandle_get_Null_m3770919321 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_get_Null_m3770919321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t1495371730  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1495371730  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableHandle_t1495371730_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)10));
		PlayableHandle_t1495371730  L_0 = V_0;
		V_1 = L_0;
		goto IL_0019;
	}

IL_0019:
	{
		PlayableHandle_t1495371730  L_1 = V_1;
		return L_1;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m1158199562 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730  ___x0, PlayableHandle_t1495371730  ___y1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1495371730  L_0 = ___x0;
		PlayableHandle_t1495371730  L_1 = ___y1;
		bool L_2 = PlayableHandle_CompareVersion_m2066103627(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m3886890033 (PlayableHandle_t1495371730 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_Equals_m3886890033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___p0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableHandle_t1495371730_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableHandle_CompareVersion_m2066103627(NULL /*static, unused*/, (*(PlayableHandle_t1495371730 *)__this), ((*(PlayableHandle_t1495371730 *)((PlayableHandle_t1495371730 *)UnBox(L_1, PlayableHandle_t1495371730_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002a;
	}

IL_002a:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableHandle_Equals_m3886890033_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableHandle_t1495371730 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1495371730 *>(__this + 1);
	return PlayableHandle_Equals_m3886890033(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m2845659099 (PlayableHandle_t1495371730 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m1370834483(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m1004790790(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableHandle_GetHashCode_m2845659099_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1495371730 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1495371730 *>(__this + 1);
	return PlayableHandle_GetHashCode_m2845659099(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m2066103627 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1495371730  ___lhs0, PlayableHandle_t1495371730  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		intptr_t L_0 = (&___lhs0)->get_m_Handle_0();
		intptr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m2314577909 (PlayableOutput_t2773044158 * __this, PlayableOutputHandle_t1247041817  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t1247041817  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void PlayableOutput__ctor_m2314577909_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t1247041817  ___handle0, const RuntimeMethod* method)
{
	PlayableOutput_t2773044158 * _thisAdjusted = reinterpret_cast<PlayableOutput_t2773044158 *>(__this + 1);
	PlayableOutput__ctor_m2314577909(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t1247041817  PlayableOutput_GetHandle_m825383068 (PlayableOutput_t2773044158 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t1247041817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t1247041817  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t1247041817  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t1247041817  PlayableOutput_GetHandle_m825383068_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutput_t2773044158 * _thisAdjusted = reinterpret_cast<PlayableOutput_t2773044158 *>(__this + 1);
	return PlayableOutput_GetHandle_m825383068(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m4236001959 (PlayableOutput_t2773044158 * __this, PlayableOutput_t2773044158  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t1247041817  L_0 = PlayableOutput_GetHandle_m825383068(__this, /*hidden argument*/NULL);
		PlayableOutputHandle_t1247041817  L_1 = PlayableOutput_GetHandle_m825383068((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableOutputHandle_op_Equality_m1707532421(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutput_Equals_m4236001959_AdjustorThunk (RuntimeObject * __this, PlayableOutput_t2773044158  ___other0, const RuntimeMethod* method)
{
	PlayableOutput_t2773044158 * _thisAdjusted = reinterpret_cast<PlayableOutput_t2773044158 *>(__this + 1);
	return PlayableOutput_Equals_m4236001959(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern "C"  void PlayableOutput__cctor_m3745137773 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutput__cctor_m3745137773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableOutputHandle_t1247041817  L_0 = PlayableOutputHandle_get_Null_m3647248066(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayableOutput_t2773044158  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PlayableOutput__ctor_m2314577909((&L_1), L_0, /*hidden argument*/NULL);
		((PlayableOutput_t2773044158_StaticFields*)il2cpp_codegen_static_fields_for(PlayableOutput_t2773044158_il2cpp_TypeInfo_var))->set_m_NullPlayableOutput_1(L_1);
		return;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t1247041817  PlayableOutputHandle_get_Null_m3647248066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_get_Null_m3647248066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableOutputHandle_t1247041817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableOutputHandle_t1247041817  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableOutputHandle_t1247041817_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)2147483647LL));
		PlayableOutputHandle_t1247041817  L_0 = V_0;
		V_1 = L_0;
		goto IL_001c;
	}

IL_001c:
	{
		PlayableOutputHandle_t1247041817  L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m1450469857 (PlayableOutputHandle_t1247041817 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m1370834483(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m1004790790(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m1450469857_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t1247041817 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t1247041817 *>(__this + 1);
	return PlayableOutputHandle_GetHashCode_m1450469857(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m1707532421 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1247041817  ___lhs0, PlayableOutputHandle_t1247041817  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t1247041817  L_0 = ___lhs0;
		PlayableOutputHandle_t1247041817  L_1 = ___rhs1;
		bool L_2 = PlayableOutputHandle_CompareVersion_m4251769876(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m4078288475 (PlayableOutputHandle_t1247041817 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_Equals_m4078288475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___p0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableOutputHandle_t1247041817_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableOutputHandle_CompareVersion_m4251769876(NULL /*static, unused*/, (*(PlayableOutputHandle_t1247041817 *)__this), ((*(PlayableOutputHandle_t1247041817 *)((PlayableOutputHandle_t1247041817 *)UnBox(L_1, PlayableOutputHandle_t1247041817_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutputHandle_Equals_m4078288475_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableOutputHandle_t1247041817 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t1247041817 *>(__this + 1);
	return PlayableOutputHandle_Equals_m4078288475(_thisAdjusted, ___p0, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m4251769876 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t1247041817  ___lhs0, PlayableOutputHandle_t1247041817  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		intptr_t L_0 = (&___lhs0)->get_m_Handle_0();
		intptr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern "C"  void PlayerConnectionInternal__ctor_m2449099525 (PlayerConnectionInternal_t3959366414 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.SendMessage(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m244187087 (PlayerConnectionInternal_t3959366414 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3567143369* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_m244187087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Guid_t  L_0 = ___messageId0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_1 = ((Guid_t_StaticFields*)il2cpp_codegen_static_fields_for(Guid_t_il2cpp_TypeInfo_var))->get_Empty_11();
		bool L_2 = Guid_op_Equality_m2100028761(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t489606696 * L_3 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_3, _stringLiteral1419227944, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001d:
	{
		String_t* L_4 = Guid_ToString_m439348276((&___messageId0), _stringLiteral4076503653, /*hidden argument*/NULL);
		ByteU5BU5D_t3567143369* L_5 = ___data1;
		int32_t L_6 = ___playerId2;
		PlayerConnectionInternal_SendMessage_m417594424(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.RegisterInternal(System.Guid)
extern "C"  void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m4118275576 (PlayerConnectionInternal_t3959366414 * __this, Guid_t  ___messageId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_m4118275576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Guid_ToString_m439348276((&___messageId0), _stringLiteral4076503653, /*hidden argument*/NULL);
		PlayerConnectionInternal_RegisterInternal_m3617664716(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.IsConnected()
extern "C"  bool PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_m2174662757 (PlayerConnectionInternal_t3959366414 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = PlayerConnectionInternal_IsConnected_m413159681(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.DisconnectAll()
extern "C"  void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m3795270087 (PlayerConnectionInternal_t3959366414 * __this, const RuntimeMethod* method)
{
	{
		PlayerConnectionInternal_DisconnectAll_m3636521271(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern "C"  bool PlayerConnectionInternal_IsConnected_m413159681 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*PlayerConnectionInternal_IsConnected_m413159681_ftn) ();
	static PlayerConnectionInternal_IsConnected_m413159681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_IsConnected_m413159681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::IsConnected()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern "C"  void PlayerConnectionInternal_RegisterInternal_m3617664716 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, const RuntimeMethod* method)
{
	typedef void (*PlayerConnectionInternal_RegisterInternal_m3617664716_ftn) (String_t*);
	static PlayerConnectionInternal_RegisterInternal_m3617664716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_RegisterInternal_m3617664716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)");
	_il2cpp_icall_func(___messageId0);
}
// System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern "C"  void PlayerConnectionInternal_SendMessage_m417594424 (RuntimeObject * __this /* static, unused */, String_t* ___messageId0, ByteU5BU5D_t3567143369* ___data1, int32_t ___playerId2, const RuntimeMethod* method)
{
	typedef void (*PlayerConnectionInternal_SendMessage_m417594424_ftn) (String_t*, ByteU5BU5D_t3567143369*, int32_t);
	static PlayerConnectionInternal_SendMessage_m417594424_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_SendMessage_m417594424_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)");
	_il2cpp_icall_func(___messageId0, ___data1, ___playerId2);
}
// System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern "C"  void PlayerConnectionInternal_DisconnectAll_m3636521271 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (*PlayerConnectionInternal_DisconnectAll_m3636521271_ftn) ();
	static PlayerConnectionInternal_DisconnectAll_m3636521271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerConnectionInternal_DisconnectAll_m3636521271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerConnectionInternal::DisconnectAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3690955331 (PropertyAttribute_t4289523592 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyName::.ctor(System.String)
extern "C"  void PropertyName__ctor_m280309397 (PropertyName_t3787932763 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		PropertyName_t3787932763  L_1 = PropertyNameUtils_PropertyNameFromString_m2517934648(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		PropertyName__ctor_m3830377608(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PropertyName__ctor_m280309397_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t3787932763 * _thisAdjusted = reinterpret_cast<PropertyName_t3787932763 *>(__this + 1);
	PropertyName__ctor_m280309397(_thisAdjusted, ___name0, method);
}
// System.Void UnityEngine.PropertyName::.ctor(UnityEngine.PropertyName)
extern "C"  void PropertyName__ctor_m3830377608 (PropertyName_t3787932763 * __this, PropertyName_t3787932763  ___other0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (&___other0)->get_id_0();
		__this->set_id_0(L_0);
		return;
	}
}
extern "C"  void PropertyName__ctor_m3830377608_AdjustorThunk (RuntimeObject * __this, PropertyName_t3787932763  ___other0, const RuntimeMethod* method)
{
	PropertyName_t3787932763 * _thisAdjusted = reinterpret_cast<PropertyName_t3787932763 *>(__this + 1);
	PropertyName__ctor_m3830377608(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.PropertyName::.ctor(System.Int32)
extern "C"  void PropertyName__ctor_m1179362628 (PropertyName_t3787932763 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		__this->set_id_0(L_0);
		return;
	}
}
extern "C"  void PropertyName__ctor_m1179362628_AdjustorThunk (RuntimeObject * __this, int32_t ___id0, const RuntimeMethod* method)
{
	PropertyName_t3787932763 * _thisAdjusted = reinterpret_cast<PropertyName_t3787932763 *>(__this + 1);
	PropertyName__ctor_m1179362628(_thisAdjusted, ___id0, method);
}
// System.Boolean UnityEngine.PropertyName::op_Equality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Equality_m3722089926 (RuntimeObject * __this /* static, unused */, PropertyName_t3787932763  ___lhs0, PropertyName_t3787932763  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_id_0();
		int32_t L_1 = (&___rhs1)->get_id_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
		goto IL_0017;
	}

IL_0017:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.PropertyName::op_Inequality(UnityEngine.PropertyName,UnityEngine.PropertyName)
extern "C"  bool PropertyName_op_Inequality_m3653165493 (RuntimeObject * __this /* static, unused */, PropertyName_t3787932763  ___lhs0, PropertyName_t3787932763  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = (&___lhs0)->get_id_0();
		int32_t L_1 = (&___rhs1)->get_id_0();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.PropertyName::GetHashCode()
extern "C"  int32_t PropertyName_GetHashCode_m2446433919 (PropertyName_t3787932763 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_id_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t PropertyName_GetHashCode_m2446433919_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PropertyName_t3787932763 * _thisAdjusted = reinterpret_cast<PropertyName_t3787932763 *>(__this + 1);
	return PropertyName_GetHashCode_m2446433919(_thisAdjusted, method);
}
// System.Boolean UnityEngine.PropertyName::Equals(System.Object)
extern "C"  bool PropertyName_Equals_m2052821075 (PropertyName_t3787932763 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyName_Equals_m2052821075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PropertyName_t3787932763_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___other0;
		bool L_2 = PropertyName_op_Equality_m3722089926(NULL /*static, unused*/, (*(PropertyName_t3787932763 *)__this), ((*(PropertyName_t3787932763 *)((PropertyName_t3787932763 *)UnBox(L_1, PropertyName_t3787932763_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PropertyName_Equals_m2052821075_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	PropertyName_t3787932763 * _thisAdjusted = reinterpret_cast<PropertyName_t3787932763 *>(__this + 1);
	return PropertyName_Equals_m2052821075(_thisAdjusted, ___other0, method);
}
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.String)
extern "C"  PropertyName_t3787932763  PropertyName_op_Implicit_m907270364 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t3787932763  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___name0;
		PropertyName_t3787932763  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PropertyName__ctor_m280309397((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		PropertyName_t3787932763  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.PropertyName UnityEngine.PropertyName::op_Implicit(System.Int32)
extern "C"  PropertyName_t3787932763  PropertyName_op_Implicit_m2268242254 (RuntimeObject * __this /* static, unused */, int32_t ___id0, const RuntimeMethod* method)
{
	PropertyName_t3787932763  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___id0;
		PropertyName_t3787932763  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PropertyName__ctor_m1179362628((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		PropertyName_t3787932763  L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.PropertyName::ToString()
extern "C"  String_t* PropertyName_ToString_m1539975057 (PropertyName_t3787932763 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PropertyName_ToString_m1539975057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_id_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2251207222(NULL /*static, unused*/, _stringLiteral3267787781, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
extern "C"  String_t* PropertyName_ToString_m1539975057_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PropertyName_t3787932763 * _thisAdjusted = reinterpret_cast<PropertyName_t3787932763 *>(__this + 1);
	return PropertyName_ToString_m1539975057(_thisAdjusted, method);
}
// UnityEngine.PropertyName UnityEngine.PropertyNameUtils::PropertyNameFromString(System.String)
extern "C"  PropertyName_t3787932763  PropertyNameUtils_PropertyNameFromString_m2517934648 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	PropertyName_t3787932763  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___name0;
		PropertyNameUtils_PropertyNameFromString_Injected_m2751720990(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		PropertyName_t3787932763  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)
extern "C"  void PropertyNameUtils_PropertyNameFromString_Injected_m2751720990 (RuntimeObject * __this /* static, unused */, String_t* ___name0, PropertyName_t3787932763 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*PropertyNameUtils_PropertyNameFromString_Injected_m2751720990_ftn) (String_t*, PropertyName_t3787932763 *);
	static PropertyNameUtils_PropertyNameFromString_Injected_m2751720990_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PropertyNameUtils_PropertyNameFromString_Injected_m2751720990_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PropertyNameUtils::PropertyNameFromString_Injected(System.String,UnityEngine.PropertyName&)");
	_il2cpp_icall_func(___name0, ___ret1);
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m2607990742 (Quaternion_t3497065016 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m2607990742_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	Quaternion_t3497065016 * _thisAdjusted = reinterpret_cast<Quaternion_t3497065016 *>(__this + 1);
	Quaternion__ctor_m2607990742(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t3497065016  Quaternion_LookRotation_m3488380577 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___forward0, Vector3_t3932393085  ___upwards1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_LookRotation_m3488380577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t3497065016  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_LookRotation_m1900152028(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t3497065016  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t3497065016  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1900152028 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___forward0, Vector3_t3932393085 * ___upwards1, Quaternion_t3497065016 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1900152028_ftn) (Vector3_t3932393085 *, Vector3_t3932393085 *, Quaternion_t3497065016 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1900152028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1900152028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t3497065016  Quaternion_Inverse_m1326095064 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___rotation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Inverse_m1326095064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t3497065016  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Inverse_m3257293516(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t3497065016  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t3497065016  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m3257293516 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016 * ___rotation0, Quaternion_t3497065016 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m3257293516_ftn) (Quaternion_t3497065016 *, Quaternion_t3497065016 *);
	static Quaternion_INTERNAL_CALL_Inverse_m3257293516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m3257293516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t3497065016  Quaternion_Euler_m2250155234 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Euler_m2250155234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t3932393085  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2321752175((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_4 = Vector3_op_Multiply_m2900776395(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Quaternion_t3497065016  L_5 = Quaternion_Internal_FromEulerRad_m4014579486(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t3497065016  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t3497065016  Quaternion_Internal_FromEulerRad_m4014579486 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___euler0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Internal_FromEulerRad_m4014579486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t3497065016  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1094838250(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t3497065016  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t3497065016  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1094838250 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085 * ___euler0, Quaternion_t3497065016 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1094838250_ftn) (Vector3_t3932393085 *, Quaternion_t3497065016 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1094838250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1094838250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t3497065016  Quaternion_get_identity_m1563966278 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_get_identity_m1563966278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Quaternion_t3497065016  L_0 = ((Quaternion_t3497065016_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_t3497065016_il2cpp_TypeInfo_var))->get_identityQuaternion_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Quaternion_t3497065016  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t3497065016  Quaternion_op_Multiply_m399252531 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___lhs0, Quaternion_t3497065016  ___rhs1, const RuntimeMethod* method)
{
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t3497065016  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m2607990742((&L_32), ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t3497065016  L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Quaternion_op_Multiply_m675595747 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___rotation0, Vector3_t3932393085  ___point1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t3932393085  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3932393085  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		float L_0 = (&___rotation0)->get_x_0();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_1();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_2();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_0();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_1();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_2();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_0();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_0();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_1();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_3();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_3();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_3();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t3932393085  L_48 = V_12;
		V_13 = L_48;
		goto IL_0136;
	}

IL_0136:
	{
		Vector3_t3932393085  L_49 = V_13;
		return L_49;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m776083444 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___lhs0, Quaternion_t3497065016  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_op_Equality_m776083444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Quaternion_t3497065016  L_0 = ___lhs0;
		Quaternion_t3497065016  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		float L_2 = Quaternion_Dot_m3847016175(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m1992395400 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___lhs0, Quaternion_t3497065016  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_op_Inequality_m1992395400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Quaternion_t3497065016  L_0 = ___lhs0;
		Quaternion_t3497065016  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		bool L_2 = Quaternion_op_Equality_m776083444(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m3847016175 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  ___a0, Quaternion_t3497065016  ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		float L_4 = (&___a0)->get_z_2();
		float L_5 = (&___b1)->get_z_2();
		float L_6 = (&___a0)->get_w_3();
		float L_7 = (&___b1)->get_w_3();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m513473826 (Quaternion_t3497065016 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3630296696(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3630296696(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3630296696(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m3630296696(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m513473826_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t3497065016 * _thisAdjusted = reinterpret_cast<Quaternion_t3497065016 *>(__this + 1);
	return Quaternion_GetHashCode_m513473826(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m3839468225 (Quaternion_t3497065016 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3839468225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t3497065016  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Quaternion_t3497065016_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t3497065016 *)((Quaternion_t3497065016 *)UnBox(L_1, Quaternion_t3497065016_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m847285714(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m847285714(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m847285714(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m847285714(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m3839468225_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Quaternion_t3497065016 * _thisAdjusted = reinterpret_cast<Quaternion_t3497065016 *>(__this + 1);
	return Quaternion_Equals_m3839468225(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m324521689 (Quaternion_t3497065016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m324521689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3385344125* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral625126391, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m324521689_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t3497065016 * _thisAdjusted = reinterpret_cast<Quaternion_t3497065016 *>(__this + 1);
	return Quaternion_ToString_m324521689(_thisAdjusted, method);
}
// System.Void UnityEngine.Quaternion::.cctor()
extern "C"  void Quaternion__cctor_m1771063772 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion__cctor_m1771063772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t3497065016  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m2607990742((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Quaternion_t3497065016_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_t3497065016_il2cpp_TypeInfo_var))->set_identityQuaternion_4(L_0);
		return;
	}
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m1461288175 (RuntimeObject * __this /* static, unused */, float ___min0, float ___max1, const RuntimeMethod* method)
{
	typedef float (*Random_Range_m1461288175_ftn) (float, float);
	static Random_Range_m1461288175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m1461288175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	float retVal = _il2cpp_icall_func(___min0, ___max1);
	return retVal;
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m3293187892 (RangeAttribute_t2306247939 * __this, float ___min0, float ___max1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3690955331(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m2420173727 (RangeInt_t21894369 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_start_0();
		int32_t L_1 = __this->get_length_1();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t RangeInt_get_end_m2420173727_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RangeInt_t21894369 * _thisAdjusted = reinterpret_cast<RangeInt_t21894369 *>(__this + 1);
	return RangeInt_get_end_m2420173727(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2331331332 (Ray_t1807385980 * __this, Vector3_t3932393085  ___origin0, Vector3_t3932393085  ___direction1, const RuntimeMethod* method)
{
	{
		Vector3_t3932393085  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t3932393085  L_1 = Vector3_get_normalized_m1820914487((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m2331331332_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___origin0, Vector3_t3932393085  ___direction1, const RuntimeMethod* method)
{
	Ray_t1807385980 * _thisAdjusted = reinterpret_cast<Ray_t1807385980 *>(__this + 1);
	Ray__ctor_m2331331332(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t3932393085  Ray_get_origin_m1394134582 (Ray_t1807385980 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = __this->get_m_Origin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3932393085  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3932393085  Ray_get_origin_m1394134582_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1807385980 * _thisAdjusted = reinterpret_cast<Ray_t1807385980 *>(__this + 1);
	return Ray_get_origin_m1394134582(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3932393085  Ray_get_direction_m2057318423 (Ray_t1807385980 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3932393085  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3932393085  Ray_get_direction_m2057318423_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1807385980 * _thisAdjusted = reinterpret_cast<Ray_t1807385980 *>(__this + 1);
	return Ray_get_direction_m2057318423(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t3932393085  Ray_GetPoint_m3306741896 (Ray_t1807385980 * __this, float ___distance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_GetPoint_m3306741896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3932393085  L_0 = __this->get_m_Origin_0();
		Vector3_t3932393085  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_3 = Vector3_op_Multiply_m2900776395(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3932393085  L_4 = Vector3_op_Addition_m2513759451(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t3932393085  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector3_t3932393085  Ray_GetPoint_m3306741896_AdjustorThunk (RuntimeObject * __this, float ___distance0, const RuntimeMethod* method)
{
	Ray_t1807385980 * _thisAdjusted = reinterpret_cast<Ray_t1807385980 *>(__this + 1);
	return Ray_GetPoint_m3306741896(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m5131743 (Ray_t1807385980 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m5131743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t3932393085  L_1 = __this->get_m_Origin_0();
		Vector3_t3932393085  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t3932393085_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		Vector3_t3932393085  L_5 = __this->get_m_Direction_1();
		Vector3_t3932393085  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t3932393085_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral3457502417, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m5131743_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t1807385980 * _thisAdjusted = reinterpret_cast<Ray_t1807385980 *>(__this + 1);
	return Ray_ToString_m5131743(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3862784288 (Rect_t952252086 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3862784288_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect__ctor_m3862784288(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m4084575049 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m4084575049_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_x_m4084575049(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m2256120598 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m2256120598_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_x_m2256120598(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3737423361 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m3737423361_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_y_m3737423361(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m3387259938 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m3387259938_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_y_m3387259938(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2246369278  Rect_get_position_m831573339 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2246369278  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2246369278  Rect_get_position_m831573339_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_position_m831573339(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2246369278  Rect_get_center_m3838774990 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_x_m4084575049(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m3737423361(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		Vector2_t2246369278  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector2_t2246369278  Rect_get_center_m3838774990_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_center_m3838774990(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2246369278  Rect_get_min_m1432445198 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMin_m3005891617(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m1542759231(__this, /*hidden argument*/NULL);
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2246369278  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2246369278  Rect_get_min_m1432445198_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_min_m1432445198(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2246369278  Rect_get_max_m631587217 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMax_m1526087686(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m2148438427(__this, /*hidden argument*/NULL);
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2246369278  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2246369278  Rect_get_max_m631587217_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_max_m631587217(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m341520840 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m341520840_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_width_m341520840(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m4164729024 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m4164729024_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_width_m4164729024(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m728299502 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m728299502_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_height_m728299502(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3996664501 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m3996664501_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_height_m3996664501(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2246369278  Rect_get_size_m1118171761 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2246369278  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2246369278  Rect_get_size_m1118171761_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_size_m1118171761(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m3005891617 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m3005891617_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_xMin_m3005891617(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m879227740 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m1526087686(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m879227740_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_xMin_m879227740(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1542759231 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m1542759231_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_yMin_m1542759231(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m2932165558 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m2148438427(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m2932165558_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_yMin_m2932165558(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m1526087686 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m1526087686_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_xMax_m1526087686(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1505073453 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m1505073453_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_xMax_m1505073453(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2148438427 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m2148438427_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_get_yMax_m2148438427(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m2859229937 (Rect_t952252086 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m2859229937_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	Rect_set_yMax_m2859229937(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m554593792 (Rect_t952252086 * __this, Vector2_t2246369278  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m3005891617(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m1526087686(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m1542759231(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m2148438427(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m554593792_AdjustorThunk (RuntimeObject * __this, Vector2_t2246369278  ___point0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_Contains_m554593792(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1728858246 (Rect_t952252086 * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m3005891617(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m1526087686(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m1542759231(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2148438427(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m1728858246_AdjustorThunk (RuntimeObject * __this, Vector3_t3932393085  ___point0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_Contains_m1728858246(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t952252086  Rect_OrderMinMax_m2598564121 (RuntimeObject * __this /* static, unused */, Rect_t952252086  ___rect0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t952252086  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = Rect_get_xMin_m3005891617((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m1526087686((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		float L_2 = Rect_get_xMin_m3005891617((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m1526087686((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m879227740((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m1505073453((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		float L_5 = Rect_get_yMin_m1542759231((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m2148438427((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = Rect_get_yMin_m1542759231((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m2148438427((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m2932165558((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m2859229937((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0067:
	{
		Rect_t952252086  L_10 = ___rect0;
		V_2 = L_10;
		goto IL_006e;
	}

IL_006e:
	{
		Rect_t952252086  L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m2657232469 (Rect_t952252086 * __this, Rect_t952252086  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m1526087686((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m3005891617(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = Rect_get_xMin_m3005891617((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m1526087686(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = Rect_get_yMax_m2148438427((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m1542759231(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = Rect_get_yMin_m1542759231((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m2148438427(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Overlaps_m2657232469_AdjustorThunk (RuntimeObject * __this, Rect_t952252086  ___other0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_Overlaps_m2657232469(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m2531020201 (Rect_t952252086 * __this, Rect_t952252086  ___other0, bool ___allowInverse1, const RuntimeMethod* method)
{
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		V_0 = (*(Rect_t952252086 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Rect_t952252086  L_1 = V_0;
		Rect_t952252086  L_2 = Rect_OrderMinMax_m2598564121(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t952252086  L_3 = ___other0;
		Rect_t952252086  L_4 = Rect_OrderMinMax_m2598564121(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001f:
	{
		Rect_t952252086  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m2657232469((&V_0), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
extern "C"  bool Rect_Overlaps_m2531020201_AdjustorThunk (RuntimeObject * __this, Rect_t952252086  ___other0, bool ___allowInverse1, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_Overlaps_m2531020201(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m1453864423 (RuntimeObject * __this /* static, unused */, Rect_t952252086  ___lhs0, Rect_t952252086  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Rect_t952252086  L_0 = ___lhs0;
		Rect_t952252086  L_1 = ___rhs1;
		bool L_2 = Rect_op_Equality_m4068887963(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m4068887963 (RuntimeObject * __this /* static, unused */, Rect_t952252086  ___lhs0, Rect_t952252086  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m4084575049((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m4084575049((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004c;
		}
	}
	{
		float L_2 = Rect_get_y_m3737423361((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m3737423361((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = Rect_get_width_m341520840((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m341520840((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004c;
		}
	}
	{
		float L_6 = Rect_get_height_m728299502((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m728299502((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 0;
	}

IL_004d:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m3748807894 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m4084575049(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3630296696((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m341520840(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3630296696((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m3737423361(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3630296696((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m728299502(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3630296696((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m3748807894_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_GetHashCode_m3748807894(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m551946840 (Rect_t952252086 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m551946840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t952252086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Rect_t952252086_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Rect_t952252086 *)((Rect_t952252086 *)UnBox(L_1, Rect_t952252086_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m4084575049(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m4084575049((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m847285714((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m3737423361(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m3737423361((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m847285714((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m341520840(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m341520840((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m847285714((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m728299502(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m728299502((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m847285714((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m551946840_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_Equals_m551946840(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m1785269746 (Rect_t952252086 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m1785269746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m4084575049(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		float L_5 = Rect_get_y_m3737423361(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		float L_9 = Rect_get_width_m341520840(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3385344125* L_12 = L_8;
		float L_13 = Rect_get_height_m728299502(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral314791402, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m1785269746_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t952252086 * _thisAdjusted = reinterpret_cast<Rect_t952252086 *>(__this + 1);
	return Rect_ToString_m1785269746(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t817686154_marshal_pinvoke(const RectOffset_t817686154& unmarshaled, RectOffset_t817686154_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_m_SourceStyle_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t817686154_marshal_pinvoke_back(const RectOffset_t817686154_marshaled_pinvoke& marshaled, RectOffset_t817686154& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t817686154_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t817686154_marshal_pinvoke_cleanup(RectOffset_t817686154_marshaled_pinvoke& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t817686154_marshal_com(const RectOffset_t817686154& unmarshaled, RectOffset_t817686154_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_m_SourceStyle_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, true);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t817686154_marshal_com_back(const RectOffset_t817686154_marshaled_com& marshaled, RectOffset_t817686154& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t817686154_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t817686154_marshal_com_cleanup(RectOffset_t817686154_marshaled_com& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m3235690126 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		RectOffset_Init_m240763499(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern "C"  void RectOffset__ctor_m4222804684 (RectOffset_t817686154 * __this, RuntimeObject * ___sourceStyle0, intptr_t ___source1, const RuntimeMethod* method)
{
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		intptr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m240763499 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef void (*RectOffset_Init_m240763499_ftn) (RectOffset_t817686154 *);
	static RectOffset_Init_m240763499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m240763499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m463627648 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef void (*RectOffset_Cleanup_m463627648_ftn) (RectOffset_t817686154 *);
	static RectOffset_Cleanup_m463627648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m463627648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m3991427146 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_left_m3991427146_ftn) (RectOffset_t817686154 *);
	static RectOffset_get_left_m3991427146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m3991427146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m416747518 (RectOffset_t817686154 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_left_m416747518_ftn) (RectOffset_t817686154 *, int32_t);
	static RectOffset_set_left_m416747518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m416747518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m664602337 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_right_m664602337_ftn) (RectOffset_t817686154 *);
	static RectOffset_get_right_m664602337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m664602337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m544546304 (RectOffset_t817686154 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_right_m544546304_ftn) (RectOffset_t817686154 *, int32_t);
	static RectOffset_set_right_m544546304_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m544546304_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m1270760253 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_top_m1270760253_ftn) (RectOffset_t817686154 *);
	static RectOffset_get_top_m1270760253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m1270760253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3575646632 (RectOffset_t817686154 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_top_m3575646632_ftn) (RectOffset_t817686154 *, int32_t);
	static RectOffset_set_top_m3575646632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3575646632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2563455708 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_bottom_m2563455708_ftn) (RectOffset_t817686154 *);
	static RectOffset_get_bottom_m2563455708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m2563455708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m2757535000 (RectOffset_t817686154 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_bottom_m2757535000_ftn) (RectOffset_t817686154 *, int32_t);
	static RectOffset_set_bottom_m2757535000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m2757535000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m2248454353 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m2248454353_ftn) (RectOffset_t817686154 *);
	static RectOffset_get_horizontal_m2248454353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m2248454353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m2674400955 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_vertical_m2674400955_ftn) (RectOffset_t817686154 *);
	static RectOffset_get_vertical_m2674400955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m2674400955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m1363865764 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0012;
			}
		}

IL_000c:
		{
			RectOffset_Cleanup_m463627648(__this, /*hidden argument*/NULL);
		}

IL_0012:
		{
			IL2CPP_LEAVE(0x1E, FINALLY_0017);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_0017;
	}

FINALLY_0017:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(23)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(23)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_001e:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern "C"  String_t* RectOffset_ToString_m2955588694 (RectOffset_t817686154 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m2955588694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m3991427146(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m664602337(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m1270760253(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3385344125* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m2563455708(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral3079287594, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t952252086  RectTransform_get_rect_m2022616935 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t952252086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_rect_m1174113108(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t952252086  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t952252086  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1174113108 (RectTransform_t1161610249 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1174113108_ftn) (RectTransform_t1161610249 *, Rect_t952252086 *);
	static RectTransform_INTERNAL_get_rect_m1174113108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1174113108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2246369278  RectTransform_get_anchorMin_m3262898663 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMin_m821634638(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4291838635 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m3477091692(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m821634638 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m821634638_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_get_anchorMin_m821634638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m821634638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m3477091692 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m3477091692_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_set_anchorMin_m3477091692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m3477091692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2246369278  RectTransform_get_anchorMax_m1509281336 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMax_m2987003428(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m3836270766 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m3760943777(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m2987003428 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m2987003428_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_get_anchorMax_m2987003428_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m2987003428_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m3760943777 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m3760943777_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_set_anchorMax_m3760943777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m3760943777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2246369278  RectTransform_get_anchoredPosition_m3060243242 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m932856358(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2197021314 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m416641638(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m932856358 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m932856358_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m932856358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m932856358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m416641638 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m416641638_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m416641638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m416641638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2246369278  RectTransform_get_sizeDelta_m3804390761 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_sizeDelta_m2033389748(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m826926724 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m1637436926(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m2033389748 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m2033389748_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_get_sizeDelta_m2033389748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m2033389748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m1637436926 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m1637436926_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_set_sizeDelta_m1637436926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m1637436926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2246369278  RectTransform_get_pivot_m57090389 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_pivot_m698036193(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m2409069685 (RectTransform_t1161610249 * __this, Vector2_t2246369278  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m1871221030(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m698036193 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m698036193_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_get_pivot_m698036193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m698036193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m1871221030 (RectTransform_t1161610249 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m1871221030_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *);
	static RectTransform_INTERNAL_set_pivot_m1871221030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m1871221030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_add_reapplyDrivenProperties_m65241362 (RuntimeObject * __this /* static, unused */, ReapplyDrivenProperties_t1147600571 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m65241362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t1147600571 * V_0 = NULL;
	ReapplyDrivenProperties_t1147600571 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t1147600571 * L_0 = ((RectTransform_t1161610249_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t1161610249_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t1147600571 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t1147600571 * L_2 = V_1;
		ReapplyDrivenProperties_t1147600571 * L_3 = ___value0;
		Delegate_t4015201940 * L_4 = Delegate_Combine_m4046213137(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t1147600571 * L_5 = V_0;
		ReapplyDrivenProperties_t1147600571 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t1147600571 *>((((RectTransform_t1161610249_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t1161610249_il2cpp_TypeInfo_var))->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t1147600571 *)CastclassSealed((RuntimeObject*)L_4, ReapplyDrivenProperties_t1147600571_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t1147600571 * L_7 = V_0;
		ReapplyDrivenProperties_t1147600571 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ReapplyDrivenProperties_t1147600571 *)L_7) == ((RuntimeObject*)(ReapplyDrivenProperties_t1147600571 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m3550100840 (RuntimeObject * __this /* static, unused */, ReapplyDrivenProperties_t1147600571 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m3550100840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t1147600571 * V_0 = NULL;
	ReapplyDrivenProperties_t1147600571 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t1147600571 * L_0 = ((RectTransform_t1161610249_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t1161610249_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t1147600571 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t1147600571 * L_2 = V_1;
		ReapplyDrivenProperties_t1147600571 * L_3 = ___value0;
		Delegate_t4015201940 * L_4 = Delegate_Remove_m897454397(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t1147600571 * L_5 = V_0;
		ReapplyDrivenProperties_t1147600571 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t1147600571 *>((((RectTransform_t1161610249_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t1161610249_il2cpp_TypeInfo_var))->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t1147600571 *)CastclassSealed((RuntimeObject*)L_4, ReapplyDrivenProperties_t1147600571_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t1147600571 * L_7 = V_0;
		ReapplyDrivenProperties_t1147600571 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ReapplyDrivenProperties_t1147600571 *)L_7) == ((RuntimeObject*)(ReapplyDrivenProperties_t1147600571 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C"  void RectTransform_SendReapplyDrivenProperties_m1445631001 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___driven0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m1445631001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t1147600571 * L_0 = ((RectTransform_t1161610249_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t1161610249_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t1147600571 * L_1 = ((RectTransform_t1161610249_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t1161610249_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		RectTransform_t1161610249 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m4103194093(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m2981119692 (RectTransform_t1161610249 * __this, Vector3U5BU5D_t1432878832* ___fourCornersArray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m2981119692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t1432878832* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1432878832* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogError_m2611403583(NULL /*static, unused*/, _stringLiteral953137448, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0020:
	{
		Rect_t952252086  L_2 = RectTransform_get_rect_m2022616935(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m4084575049((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m3737423361((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m1526087686((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m2148438427((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t1432878832* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t3932393085  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2321752175((&L_10), L_8, L_9, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_10;
		Vector3U5BU5D_t1432878832* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t3932393085  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2321752175((&L_14), L_12, L_13, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_14;
		Vector3U5BU5D_t1432878832* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t3932393085  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2321752175((&L_18), L_16, L_17, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_18;
		Vector3U5BU5D_t1432878832* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t3932393085  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2321752175((&L_22), L_20, L_21, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_22;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m2129982010 (RectTransform_t1161610249 * __this, Vector3U5BU5D_t1432878832* ___fourCornersArray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m2129982010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3580444445 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t1432878832* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1432878832* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogError_m2611403583(NULL /*static, unused*/, _stringLiteral760554469, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0020:
	{
		Vector3U5BU5D_t1432878832* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m2981119692(__this, L_2, /*hidden argument*/NULL);
		Transform_t3580444445 * L_3 = Component_get_transform_m1425942785(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0057;
	}

IL_0035:
	{
		Vector3U5BU5D_t1432878832* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3580444445 * L_6 = V_0;
		Vector3U5BU5D_t1432878832* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		Vector3_t3932393085  L_9 = Transform_TransformPoint_m1797663460(L_6, (*(Vector3_t3932393085 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m465610573 (RectTransform_t1161610249 * __this, int32_t ___edge0, float ___inset1, float ___size2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t2246369278  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2246369278  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2246369278  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2246369278  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2246369278  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t2246369278 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t2246369278 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t2246369278 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		G_B4_0 = 1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B4_0 = 0;
	}

IL_0016:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B7_0 = 1;
	}

IL_0025:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0033;
	}

IL_0032:
	{
		G_B10_0 = 0;
	}

IL_0033:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t2246369278  L_5 = RectTransform_get_anchorMin_m3262898663(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m1623790336((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t2246369278  L_8 = V_3;
		RectTransform_set_anchorMin_m4291838635(__this, L_8, /*hidden argument*/NULL);
		Vector2_t2246369278  L_9 = RectTransform_get_anchorMax_m1509281336(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m1623790336((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t2246369278  L_12 = V_3;
		RectTransform_set_anchorMax_m3836270766(__this, L_12, /*hidden argument*/NULL);
		Vector2_t2246369278  L_13 = RectTransform_get_sizeDelta_m3804390761(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m1623790336((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t2246369278  L_16 = V_4;
		RectTransform_set_sizeDelta_m826926724(__this, L_16, /*hidden argument*/NULL);
		Vector2_t2246369278  L_17 = RectTransform_get_anchoredPosition_m3060243242(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ad;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t2246369278  L_22 = RectTransform_get_pivot_m57090389(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m1427364022((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c1;
	}

IL_00ad:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t2246369278  L_27 = RectTransform_get_pivot_m57090389(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m1427364022((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c1:
	{
		Vector2_set_Item_m1623790336(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t2246369278  L_30 = V_5;
		RectTransform_set_anchoredPosition_m2197021314(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m88774652 (RectTransform_t1161610249 * __this, int32_t ___axis0, float ___size1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2246369278  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2246369278  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2246369278  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t2246369278  L_1 = RectTransform_get_sizeDelta_m3804390761(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t2246369278  L_4 = RectTransform_GetParentSize_m770305266(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m1427364022((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t2246369278  L_7 = RectTransform_get_anchorMax_m1509281336(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m1427364022((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t2246369278  L_10 = RectTransform_get_anchorMin_m3262898663(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m1427364022((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m1623790336((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t2246369278  L_13 = V_1;
		RectTransform_set_sizeDelta_m826926724(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t2246369278  RectTransform_GetParentSize_m770305266 (RectTransform_t1161610249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m770305266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t1161610249 * V_0 = NULL;
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t952252086  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3580444445 * L_0 = Transform_get_parent_m82173183(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t1161610249 *)IsInstSealed((RuntimeObject*)L_0, RectTransform_t1161610249_il2cpp_TypeInfo_var));
		RectTransform_t1161610249 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m1340895429(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_3 = Vector2_get_zero_m1981541827(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0023:
	{
		RectTransform_t1161610249 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t952252086  L_5 = RectTransform_get_rect_m2022616935(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t2246369278  L_6 = Rect_get_size_m1118171761((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Vector2_t2246369278  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m1964044257 (ReapplyDrivenProperties_t1147600571 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m4103194093 (ReapplyDrivenProperties_t1147600571 * __this, RectTransform_t1161610249 * ___driven0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m4103194093((ReapplyDrivenProperties_t1147600571 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RectTransform_t1161610249 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t1161610249 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ReapplyDrivenProperties_BeginInvoke_m1642659194 (ReapplyDrivenProperties_t1147600571 * __this, RectTransform_t1161610249 * ___driven0, AsyncCallback_t4283869127 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m1347853047 (ReapplyDrivenProperties_t1147600571 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t1926439680 * Renderer_get_material_m2478695498 (Renderer_t3456269968 * __this, const RuntimeMethod* method)
{
	typedef Material_t1926439680 * (*Renderer_get_material_m2478695498_ftn) (Renderer_t3456269968 *);
	static Renderer_get_material_m2478695498_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2478695498_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	Material_t1926439680 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_SetPropertyBlock_m2525545781 (Renderer_t3456269968 * __this, MaterialPropertyBlock_t3731058050 * ___properties0, const RuntimeMethod* method)
{
	typedef void (*Renderer_SetPropertyBlock_m2525545781_ftn) (Renderer_t3456269968 *, MaterialPropertyBlock_t3731058050 *);
	static Renderer_SetPropertyBlock_m2525545781_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_SetPropertyBlock_m2525545781_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___properties0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m3888123168 (Renderer_t3456269968 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m3888123168_ftn) (Renderer_t3456269968 *);
	static Renderer_get_sortingLayerID_m3888123168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m3888123168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m125615218 (Renderer_t3456269968 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m125615218_ftn) (Renderer_t3456269968 *);
	static Renderer_get_sortingOrder_m125615218_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m125615218_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C"  void CommandBuffer__ctor_m3971617957 (CommandBuffer_t3383400165 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer__ctor_m3971617957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		__this->set_m_Ptr_0((intptr_t)(0));
		CommandBuffer_InitBuffer_m3164580979(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Finalize()
extern "C"  void CommandBuffer_Finalize_m3168502757 (CommandBuffer_t3383400165 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		CommandBuffer_Dispose_m3807691801(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000d;
	}

FINALLY_000d:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(13)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(13)
	{
		IL2CPP_JUMP_TBL(0x14, IL_0014)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose()
extern "C"  void CommandBuffer_Dispose_m2428763316 (CommandBuffer_t3383400165 * __this, const RuntimeMethod* method)
{
	{
		CommandBuffer_Dispose_m3807691801(__this, (bool)1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1914804560(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern "C"  void CommandBuffer_Dispose_m3807691801 (CommandBuffer_t3383400165 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer_Dispose_m3807691801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CommandBuffer_ReleaseBuffer_m2150910944(__this, /*hidden argument*/NULL);
		__this->set_m_Ptr_0((intptr_t)(0));
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void CommandBuffer_InitBuffer_m3164580979 (RuntimeObject * __this /* static, unused */, CommandBuffer_t3383400165 * ___buf0, const RuntimeMethod* method)
{
	typedef void (*CommandBuffer_InitBuffer_m3164580979_ftn) (CommandBuffer_t3383400165 *);
	static CommandBuffer_InitBuffer_m3164580979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_InitBuffer_m3164580979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(___buf0);
}
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m2150910944 (CommandBuffer_t3383400165 * __this, const RuntimeMethod* method)
{
	typedef void (*CommandBuffer_ReleaseBuffer_m2150910944_ftn) (CommandBuffer_t3383400165 *);
	static CommandBuffer_ReleaseBuffer_m2150910944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_ReleaseBuffer_m2150910944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m4257066531 (CommandBuffer_t3383400165 * __this, Texture_t954505614 * ___source0, RenderTargetIdentifier_t35305570  ___dest1, Material_t1926439680 * ___mat2, const RuntimeMethod* method)
{
	{
		Texture_t954505614 * L_0 = ___source0;
		Material_t1926439680 * L_1 = ___mat2;
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2_t2246369278  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3768944458((&L_3), (0.0f), (0.0f), /*hidden argument*/NULL);
		CommandBuffer_Blit_Texture_m3682817401(__this, L_0, (&___dest1), L_1, (-1), L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void CommandBuffer_Blit_Texture_m3682817401 (CommandBuffer_t3383400165 * __this, Texture_t954505614 * ___source0, RenderTargetIdentifier_t35305570 * ___dest1, Material_t1926439680 * ___mat2, int32_t ___pass3, Vector2_t2246369278  ___scale4, Vector2_t2246369278  ___offset5, const RuntimeMethod* method)
{
	{
		Texture_t954505614 * L_0 = ___source0;
		RenderTargetIdentifier_t35305570 * L_1 = ___dest1;
		Material_t1926439680 * L_2 = ___mat2;
		int32_t L_3 = ___pass3;
		CommandBuffer_INTERNAL_CALL_Blit_Texture_m2069604155(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (&___scale4), (&___offset5), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void CommandBuffer_INTERNAL_CALL_Blit_Texture_m2069604155 (RuntimeObject * __this /* static, unused */, CommandBuffer_t3383400165 * ___self0, Texture_t954505614 * ___source1, RenderTargetIdentifier_t35305570 * ___dest2, Material_t1926439680 * ___mat3, int32_t ___pass4, Vector2_t2246369278 * ___scale5, Vector2_t2246369278 * ___offset6, const RuntimeMethod* method)
{
	typedef void (*CommandBuffer_INTERNAL_CALL_Blit_Texture_m2069604155_ftn) (CommandBuffer_t3383400165 *, Texture_t954505614 *, RenderTargetIdentifier_t35305570 *, Material_t1926439680 *, int32_t, Vector2_t2246369278 *, Vector2_t2246369278 *);
	static CommandBuffer_INTERNAL_CALL_Blit_Texture_m2069604155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_INTERNAL_CALL_Blit_Texture_m2069604155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Blit_Texture(UnityEngine.Rendering.CommandBuffer,UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___source1, ___dest2, ___mat3, ___pass4, ___scale5, ___offset6);
}
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m3059967350 (RenderTargetIdentifier_t35305570 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier__ctor_m3059967350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___type0;
		__this->set_m_Type_0(L_0);
		__this->set_m_NameID_1((-1));
		__this->set_m_InstanceID_2(0);
		__this->set_m_BufferPointer_3((intptr_t)(0));
		__this->set_m_MipLevel_4(0);
		__this->set_m_CubeFace_5((-1));
		__this->set_m_DepthSlice_6(0);
		return;
	}
}
extern "C"  void RenderTargetIdentifier__ctor_m3059967350_AdjustorThunk (RuntimeObject * __this, int32_t ___type0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t35305570 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t35305570 *>(__this + 1);
	RenderTargetIdentifier__ctor_m3059967350(_thisAdjusted, ___type0, method);
}
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t35305570  RenderTargetIdentifier_op_Implicit_m4261435474 (RuntimeObject * __this /* static, unused */, int32_t ___type0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t35305570  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___type0;
		RenderTargetIdentifier_t35305570  L_1;
		memset(&L_1, 0, sizeof(L_1));
		RenderTargetIdentifier__ctor_m3059967350((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		RenderTargetIdentifier_t35305570  L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern "C"  String_t* RenderTargetIdentifier_ToString_m430154943 (RenderTargetIdentifier_t35305570 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_ToString_m430154943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Type_0();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(BuiltinRenderTextureType_t3735833062_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		int32_t L_5 = __this->get_m_NameID_1();
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		int32_t L_9 = __this->get_m_InstanceID_2();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		String_t* L_12 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral196967772, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* RenderTargetIdentifier_ToString_m430154943_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t35305570 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t35305570 *>(__this + 1);
	return RenderTargetIdentifier_ToString_m430154943(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m782971227 (RenderTargetIdentifier_t35305570 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_GetHashCode_m782971227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t* L_0 = __this->get_address_of_m_Type_0();
		RuntimeObject * L_1 = Box(BuiltinRenderTextureType_t3735833062_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		*L_0 = *(int32_t*)UnBox(L_1);
		int32_t* L_3 = __this->get_address_of_m_NameID_1();
		int32_t L_4 = Int32_GetHashCode_m1004790790(L_3, /*hidden argument*/NULL);
		int32_t* L_5 = __this->get_address_of_m_InstanceID_2();
		int32_t L_6 = Int32_GetHashCode_m1004790790(L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)23)))+(int32_t)L_4))*(int32_t)((int32_t)23)))+(int32_t)L_6));
		goto IL_0042;
	}

IL_0042:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m782971227_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t35305570 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t35305570 *>(__this + 1);
	return RenderTargetIdentifier_GetHashCode_m782971227(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  bool RenderTargetIdentifier_Equals_m981627877 (RenderTargetIdentifier_t35305570 * __this, RenderTargetIdentifier_t35305570  ___rhs0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = __this->get_m_Type_0();
		int32_t L_1 = (&___rhs0)->get_m_Type_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_2 = __this->get_m_NameID_1();
		int32_t L_3 = (&___rhs0)->get_m_NameID_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_4 = __this->get_m_InstanceID_2();
		int32_t L_5 = (&___rhs0)->get_m_InstanceID_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0083;
		}
	}
	{
		intptr_t L_6 = __this->get_m_BufferPointer_3();
		intptr_t L_7 = (&___rhs0)->get_m_BufferPointer_3();
		bool L_8 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_9 = __this->get_m_MipLevel_4();
		int32_t L_10 = (&___rhs0)->get_m_MipLevel_4();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_11 = __this->get_m_CubeFace_5();
		int32_t L_12 = (&___rhs0)->get_m_CubeFace_5();
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_13 = __this->get_m_DepthSlice_6();
		int32_t L_14 = (&___rhs0)->get_m_DepthSlice_6();
		G_B8_0 = ((((int32_t)L_13) == ((int32_t)L_14))? 1 : 0);
		goto IL_0084;
	}

IL_0083:
	{
		G_B8_0 = 0;
	}

IL_0084:
	{
		V_0 = (bool)G_B8_0;
		goto IL_008a;
	}

IL_008a:
	{
		bool L_15 = V_0;
		return L_15;
	}
}
extern "C"  bool RenderTargetIdentifier_Equals_m981627877_AdjustorThunk (RuntimeObject * __this, RenderTargetIdentifier_t35305570  ___rhs0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t35305570 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t35305570 *>(__this + 1);
	return RenderTargetIdentifier_Equals_m981627877(_thisAdjusted, ___rhs0, method);
}
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern "C"  bool RenderTargetIdentifier_Equals_m2799259023 (RenderTargetIdentifier_t35305570 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_Equals_m2799259023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RenderTargetIdentifier_t35305570  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___obj0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, RenderTargetIdentifier_t35305570_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0027;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___obj0;
		V_1 = ((*(RenderTargetIdentifier_t35305570 *)((RenderTargetIdentifier_t35305570 *)UnBox(L_1, RenderTargetIdentifier_t35305570_il2cpp_TypeInfo_var))));
		RenderTargetIdentifier_t35305570  L_2 = V_1;
		bool L_3 = RenderTargetIdentifier_Equals_m981627877(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0027;
	}

IL_0027:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool RenderTargetIdentifier_Equals_m2799259023_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	RenderTargetIdentifier_t35305570 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t35305570 *>(__this + 1);
	return RenderTargetIdentifier_Equals_m2799259023(_thisAdjusted, ___obj0, method);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m119370463 (RuntimeObject * __this /* static, unused */, RenderTexture_t3737750657 * ___mono0, const RuntimeMethod* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m119370463_ftn) (RenderTexture_t3737750657 *);
	static RenderTexture_Internal_GetWidth_m119370463_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m119370463_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	int32_t retVal = _il2cpp_icall_func(___mono0);
	return retVal;
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m366723182 (RuntimeObject * __this /* static, unused */, RenderTexture_t3737750657 * ___mono0, const RuntimeMethod* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m366723182_ftn) (RenderTexture_t3737750657 *);
	static RenderTexture_Internal_GetHeight_m366723182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m366723182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	int32_t retVal = _il2cpp_icall_func(___mono0);
	return retVal;
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m2404258804 (RenderTexture_t3737750657 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m119370463(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m3953848152 (RenderTexture_t3737750657 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m366723182(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m2000527188 (RequireComponent_t3219866953 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
extern "C"  void RequireComponent__ctor_m2998529138 (RequireComponent_t3219866953 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		Type_t * L_1 = ___requiredComponent21;
		__this->set_m_Type1_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m4289303328 (Resolution_t3373471800 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Width_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Resolution_get_width_m4289303328_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Resolution_t3373471800 * _thisAdjusted = reinterpret_cast<Resolution_t3373471800 *>(__this + 1);
	return Resolution_get_width_m4289303328(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m1022399602 (Resolution_t3373471800 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Height_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Resolution_get_height_m1022399602_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Resolution_t3373471800 * _thisAdjusted = reinterpret_cast<Resolution_t3373471800 *>(__this + 1);
	return Resolution_get_height_m1022399602(_thisAdjusted, method);
}
// System.String UnityEngine.Resolution::ToString()
extern "C"  String_t* Resolution_ToString_m1282970970 (Resolution_t3373471800 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resolution_ToString_m1282970970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Width_0();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		int32_t L_5 = __this->get_m_Height_1();
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3385344125* L_8 = L_4;
		int32_t L_9 = __this->get_m_RefreshRate_2();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t3515577538_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		String_t* L_12 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral632663534, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* Resolution_ToString_m1282970970_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Resolution_t3373471800 * _thisAdjusted = reinterpret_cast<Resolution_t3373471800 *>(__this + 1);
	return Resolution_ToString_m1282970970(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2277419115_marshal_pinvoke(const ResourceRequest_t2277419115& unmarshaled, ResourceRequest_t2277419115_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
extern "C" void ResourceRequest_t2277419115_marshal_pinvoke_back(const ResourceRequest_t2277419115_marshaled_pinvoke& marshaled, ResourceRequest_t2277419115& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2277419115_marshal_pinvoke_cleanup(ResourceRequest_t2277419115_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2277419115_marshal_com(const ResourceRequest_t2277419115& unmarshaled, ResourceRequest_t2277419115_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
extern "C" void ResourceRequest_t2277419115_marshal_com_back(const ResourceRequest_t2277419115_marshaled_com& marshaled, ResourceRequest_t2277419115& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2277419115_marshal_com_cleanup(ResourceRequest_t2277419115_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t3645472222 * Resources_GetBuiltinResource_m4234133296 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const RuntimeMethod* method)
{
	typedef Object_t3645472222 * (*Resources_GetBuiltinResource_m4234133296_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m4234133296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m4234133296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	Object_t3645472222 * retVal = _il2cpp_icall_func(___type0, ___path1);
	return retVal;
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m91486082 (Scene_t1083318001 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_get_handle_m91486082_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t1083318001 * _thisAdjusted = reinterpret_cast<Scene_t1083318001 *>(__this + 1);
	return Scene_get_handle_m91486082(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m4235140656 (Scene_t1083318001 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_GetHashCode_m4235140656_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t1083318001 * _thisAdjusted = reinterpret_cast<Scene_t1083318001 *>(__this + 1);
	return Scene_GetHashCode_m4235140656(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m4240418630 (Scene_t1083318001 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m4240418630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Scene_t1083318001  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Scene_t1083318001_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Scene_t1083318001 *)((Scene_t1083318001 *)UnBox(L_1, Scene_t1083318001_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m91486082(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m91486082((&V_1), /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Scene_Equals_m4240418630_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Scene_t1083318001 * _thisAdjusted = reinterpret_cast<Scene_t1083318001 *>(__this + 1);
	return Scene_Equals_m4240418630(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_Internal_SceneLoaded_m3528941024 (RuntimeObject * __this /* static, unused */, Scene_t1083318001  ___scene0, int32_t ___mode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m3528941024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t2618073252 * L_0 = ((SceneManager_t793739027_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t793739027_il2cpp_TypeInfo_var))->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t2618073252 * L_1 = ((SceneManager_t793739027_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t793739027_il2cpp_TypeInfo_var))->get_sceneLoaded_0();
		Scene_t1083318001  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m2791872171(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m2791872171_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_SceneUnloaded_m398202129 (RuntimeObject * __this /* static, unused */, Scene_t1083318001  ___scene0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m398202129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_1_t3754315317 * L_0 = ((SceneManager_t793739027_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t793739027_il2cpp_TypeInfo_var))->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UnityAction_1_t3754315317 * L_1 = ((SceneManager_t793739027_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t793739027_il2cpp_TypeInfo_var))->get_sceneUnloaded_1();
		Scene_t1083318001  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m1991915580(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m1991915580_RuntimeMethod_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m1668464213 (RuntimeObject * __this /* static, unused */, Scene_t1083318001  ___previousActiveScene0, Scene_t1083318001  ___newActiveScene1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m1668464213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t671528035 * L_0 = ((SceneManager_t793739027_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t793739027_il2cpp_TypeInfo_var))->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t671528035 * L_1 = ((SceneManager_t793739027_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t793739027_il2cpp_TypeInfo_var))->get_activeSceneChanged_2();
		Scene_t1083318001  L_2 = ___previousActiveScene0;
		Scene_t1083318001  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m2120342004(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m2120342004_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t3373471800  Screen_get_currentResolution_m1790603113 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Resolution_t3373471800  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t3373471800  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Screen_INTERNAL_get_currentResolution_m2882077263(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Resolution_t3373471800  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Resolution_t3373471800  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Screen::INTERNAL_get_currentResolution(UnityEngine.Resolution&)
extern "C"  void Screen_INTERNAL_get_currentResolution_m2882077263 (RuntimeObject * __this /* static, unused */, Resolution_t3373471800 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_INTERNAL_get_currentResolution_m2882077263_ftn) (Resolution_t3373471800 *);
	static Screen_INTERNAL_get_currentResolution_m2882077263_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_INTERNAL_get_currentResolution_m2882077263_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::INTERNAL_get_currentResolution(UnityEngine.Resolution&)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m2835571473 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_width_m2835571473_ftn) ();
	static Screen_get_width_m2835571473_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m2835571473_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m998832438 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_height_m998832438_ftn) ();
	static Screen_get_height_m998832438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m998832438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m2584222623 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Screen_get_dpi_m2584222623_ftn) ();
	static Screen_get_dpi_m2584222623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m2584222623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1324617639_marshal_pinvoke(const ScriptableObject_t1324617639& unmarshaled, ScriptableObject_t1324617639_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void ScriptableObject_t1324617639_marshal_pinvoke_back(const ScriptableObject_t1324617639_marshaled_pinvoke& marshaled, ScriptableObject_t1324617639& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1324617639_marshal_pinvoke_cleanup(ScriptableObject_t1324617639_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1324617639_marshal_com(const ScriptableObject_t1324617639& unmarshaled, ScriptableObject_t1324617639_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = unmarshaled.get_m_CachedPtr_0();
}
extern "C" void ScriptableObject_t1324617639_marshal_com_back(const ScriptableObject_t1324617639_marshaled_com& marshaled, ScriptableObject_t1324617639& unmarshaled)
{
	intptr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	unmarshaled_m_CachedPtr_temp_0 = marshaled.___m_CachedPtr_0;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1324617639_marshal_com_cleanup(ScriptableObject_t1324617639_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m3277333935 (ScriptableObject_t1324617639 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m3277333935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m290099682(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m290099682 (RuntimeObject * __this /* static, unused */, ScriptableObject_t1324617639 * ___self0, const RuntimeMethod* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m290099682_ftn) (ScriptableObject_t1324617639 *);
	static ScriptableObject_Internal_CreateScriptableObject_m290099682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m290099682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t1324617639 * ScriptableObject_CreateInstance_m333497138 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	ScriptableObject_t1324617639 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t1324617639 * L_1 = ScriptableObject_CreateInstanceFromType_m3046537746(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		ScriptableObject_t1324617639 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t1324617639 * ScriptableObject_CreateInstanceFromType_m3046537746 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ScriptableObject_t1324617639 * (*ScriptableObject_CreateInstanceFromType_m3046537746_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m3046537746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m3046537746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	ScriptableObject_t1324617639 * retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// System.Void UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute::.ctor()
extern "C"  void GeneratedByOldBindingsGeneratorAttribute__ctor_m3990393407 (GeneratedByOldBindingsGeneratorAttribute_t1773994740 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m2023763117 (RequiredByNativeCodeAttribute_t2936828858 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor(System.String)
extern "C"  void RequiredByNativeCodeAttribute__ctor_m3765186276 (RequiredByNativeCodeAttribute_t2936828858 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		RequiredByNativeCodeAttribute_set_Name_m1132036607(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m1132036607 (RequiredByNativeCodeAttribute_t2936828858 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Optional(System.Boolean)
extern "C"  void RequiredByNativeCodeAttribute_set_Optional_m3645765718 (RequiredByNativeCodeAttribute_t2936828858 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3COptionalU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m3842683587 (UsedByNativeCodeAttribute_t2845240971 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m182768840 (SelectionBaseAttribute_t867521075 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern "C"  void SendMouseEvents_SetMouseMoved_m3480495290 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m3480495290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::HitTestLegacyGUI(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.SendMouseEvents/HitInfo&)
extern "C"  void SendMouseEvents_HitTestLegacyGUI_m1463766836 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___camera0, Vector3_t3932393085  ___mousePosition1, HitInfo_t3525137541 * ___hitInfo2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_HitTestLegacyGUI_m1463766836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUILayer_t1634068716 * V_0 = NULL;
	GUIElement_t2757512779 * V_1 = NULL;
	{
		Camera_t362346687 * L_0 = ___camera0;
		NullCheck(L_0);
		GUILayer_t1634068716 * L_1 = Component_GetComponent_TisGUILayer_t1634068716_m3037765150(L_0, /*hidden argument*/Component_GetComponent_TisGUILayer_t1634068716_m3037765150_RuntimeMethod_var);
		V_0 = L_1;
		GUILayer_t1634068716 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m1340895429(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		GUILayer_t1634068716 * L_4 = V_0;
		Vector3_t3932393085  L_5 = ___mousePosition1;
		NullCheck(L_4);
		GUIElement_t2757512779 * L_6 = GUILayer_HitTest_m1524866709(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GUIElement_t2757512779 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m1340895429(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0041;
		}
	}
	{
		HitInfo_t3525137541 * L_9 = ___hitInfo2;
		GUIElement_t2757512779 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t2881801184 * L_11 = Component_get_gameObject_m3138408676(L_10, /*hidden argument*/NULL);
		L_9->set_target_0(L_11);
		HitInfo_t3525137541 * L_12 = ___hitInfo2;
		Camera_t362346687 * L_13 = ___camera0;
		L_12->set_camera_1(L_13);
		goto IL_0051;
	}

IL_0041:
	{
		HitInfo_t3525137541 * L_14 = ___hitInfo2;
		L_14->set_target_0((GameObject_t2881801184 *)NULL);
		HitInfo_t3525137541 * L_15 = ___hitInfo2;
		L_15->set_camera_1((Camera_t362346687 *)NULL);
	}

IL_0051:
	{
	}

IL_0052:
	{
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern "C"  void SendMouseEvents_DoSendMouseEvents_m507784797 (RuntimeObject * __this /* static, unused */, int32_t ___skipRTCameras0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m507784797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	HitInfo_t3525137541  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Camera_t362346687 * V_4 = NULL;
	CameraU5BU5D_t365668710* V_5 = NULL;
	int32_t V_6 = 0;
	Rect_t952252086  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Ray_t1807385980  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector3_t3932393085  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	GameObject_t2881801184 * V_12 = NULL;
	GameObject_t2881801184 * V_13 = NULL;
	int32_t V_14 = 0;
	float G_B19_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_0 = Input_get_mousePosition_m3071837934(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m190271839(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		CameraU5BU5D_t365668710* L_2 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		CameraU5BU5D_t365668710* L_3 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002f;
		}
	}

IL_0024:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_m_Cameras_4(((CameraU5BU5D_t365668710*)SZArrayNew(CameraU5BU5D_t365668710_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		CameraU5BU5D_t365668710* L_6 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		Camera_GetAllCameras_m635401728(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_7 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t3525137541  L_9 = V_3;
		*(HitInfo_t3525137541 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_12 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_027e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		CameraU5BU5D_t365668710* L_14 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		V_5 = L_14;
		V_6 = 0;
		goto IL_0272;
	}

IL_0086:
	{
		CameraU5BU5D_t365668710* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Camera_t362346687 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		Camera_t362346687 * L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_19, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b8;
		}
	}
	{
		Camera_t362346687 * L_22 = V_4;
		NullCheck(L_22);
		RenderTexture_t3737750657 * L_23 = Camera_get_targetTexture_m3060139219(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_23, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b8;
		}
	}

IL_00b3:
	{
		goto IL_026c;
	}

IL_00b8:
	{
		Camera_t362346687 * L_25 = V_4;
		NullCheck(L_25);
		Rect_t952252086  L_26 = Camera_get_pixelRect_m3824066811(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		Vector3_t3932393085  L_27 = V_0;
		bool L_28 = Rect_Contains_m1728858246((&V_7), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_026c;
	}

IL_00d3:
	{
		Camera_t362346687 * L_29 = V_4;
		Vector3_t3932393085  L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_31 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_31);
		SendMouseEvents_HitTestLegacyGUI_m1463766836(NULL /*static, unused*/, L_29, L_30, ((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		Camera_t362346687 * L_32 = V_4;
		NullCheck(L_32);
		int32_t L_33 = Camera_get_eventMask_m2361608952(L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_026c;
	}

IL_00f7:
	{
		Camera_t362346687 * L_34 = V_4;
		Vector3_t3932393085  L_35 = V_0;
		NullCheck(L_34);
		Ray_t1807385980  L_36 = Camera_ScreenPointToRay_m3701064168(L_34, L_35, /*hidden argument*/NULL);
		V_8 = L_36;
		Vector3_t3932393085  L_37 = Ray_get_direction_m2057318423((&V_8), /*hidden argument*/NULL);
		V_10 = L_37;
		float L_38 = (&V_10)->get_z_3();
		V_9 = L_38;
		float L_39 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		bool L_40 = Mathf_Approximately_m4050652555(NULL /*static, unused*/, (0.0f), L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_012e;
		}
	}
	{
		G_B19_0 = (std::numeric_limits<float>::infinity());
		goto IL_0145;
	}

IL_012e:
	{
		Camera_t362346687 * L_41 = V_4;
		NullCheck(L_41);
		float L_42 = Camera_get_farClipPlane_m1473945392(L_41, /*hidden argument*/NULL);
		Camera_t362346687 * L_43 = V_4;
		NullCheck(L_43);
		float L_44 = Camera_get_nearClipPlane_m4120344687(L_43, /*hidden argument*/NULL);
		float L_45 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_46 = fabsf(((float)((float)((float)((float)L_42-(float)L_44))/(float)L_45)));
		G_B19_0 = L_46;
	}

IL_0145:
	{
		V_11 = G_B19_0;
		Camera_t362346687 * L_47 = V_4;
		Ray_t1807385980  L_48 = V_8;
		float L_49 = V_11;
		Camera_t362346687 * L_50 = V_4;
		NullCheck(L_50);
		int32_t L_51 = Camera_get_cullingMask_m4086893319(L_50, /*hidden argument*/NULL);
		Camera_t362346687 * L_52 = V_4;
		NullCheck(L_52);
		int32_t L_53 = Camera_get_eventMask_m2361608952(L_52, /*hidden argument*/NULL);
		NullCheck(L_47);
		GameObject_t2881801184 * L_54 = Camera_RaycastTry_m3889099052(L_47, L_48, L_49, ((int32_t)((int32_t)L_51&(int32_t)L_53)), /*hidden argument*/NULL);
		V_12 = L_54;
		GameObject_t2881801184 * L_55 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_55, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_019b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_57 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_57);
		GameObject_t2881801184 * L_58 = V_12;
		((L_57)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_58);
		HitInfoU5BU5D_t1685419976* L_59 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_59);
		Camera_t362346687 * L_60 = V_4;
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_60);
		goto IL_01d9;
	}

IL_019b:
	{
		Camera_t362346687 * L_61 = V_4;
		NullCheck(L_61);
		int32_t L_62 = Camera_get_clearFlags_m1043993872(L_61, /*hidden argument*/NULL);
		if ((((int32_t)L_62) == ((int32_t)1)))
		{
			goto IL_01b5;
		}
	}
	{
		Camera_t362346687 * L_63 = V_4;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_clearFlags_m1043993872(L_63, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_64) == ((uint32_t)2))))
		{
			goto IL_01d9;
		}
	}

IL_01b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_65 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_65);
		((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t2881801184 *)NULL);
		HitInfoU5BU5D_t1685419976* L_66 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_66);
		((L_66)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t362346687 *)NULL);
	}

IL_01d9:
	{
		Camera_t362346687 * L_67 = V_4;
		Ray_t1807385980  L_68 = V_8;
		float L_69 = V_11;
		Camera_t362346687 * L_70 = V_4;
		NullCheck(L_70);
		int32_t L_71 = Camera_get_cullingMask_m4086893319(L_70, /*hidden argument*/NULL);
		Camera_t362346687 * L_72 = V_4;
		NullCheck(L_72);
		int32_t L_73 = Camera_get_eventMask_m2361608952(L_72, /*hidden argument*/NULL);
		NullCheck(L_67);
		GameObject_t2881801184 * L_74 = Camera_RaycastTry2D_m3772444891(L_67, L_68, L_69, ((int32_t)((int32_t)L_71&(int32_t)L_73)), /*hidden argument*/NULL);
		V_13 = L_74;
		GameObject_t2881801184 * L_75 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_76 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_75, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_022d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_77 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_77);
		GameObject_t2881801184 * L_78 = V_13;
		((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_78);
		HitInfoU5BU5D_t1685419976* L_79 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_79);
		Camera_t362346687 * L_80 = V_4;
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_80);
		goto IL_026b;
	}

IL_022d:
	{
		Camera_t362346687 * L_81 = V_4;
		NullCheck(L_81);
		int32_t L_82 = Camera_get_clearFlags_m1043993872(L_81, /*hidden argument*/NULL);
		if ((((int32_t)L_82) == ((int32_t)1)))
		{
			goto IL_0247;
		}
	}
	{
		Camera_t362346687 * L_83 = V_4;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_clearFlags_m1043993872(L_83, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_84) == ((uint32_t)2))))
		{
			goto IL_026b;
		}
	}

IL_0247:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_85 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_85);
		((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t2881801184 *)NULL);
		HitInfoU5BU5D_t1685419976* L_86 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_86);
		((L_86)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t362346687 *)NULL);
	}

IL_026b:
	{
	}

IL_026c:
	{
		int32_t L_87 = V_6;
		V_6 = ((int32_t)((int32_t)L_87+(int32_t)1));
	}

IL_0272:
	{
		int32_t L_88 = V_6;
		CameraU5BU5D_t365668710* L_89 = V_5;
		NullCheck(L_89);
		if ((((int32_t)L_88) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_89)->max_length)))))))
		{
			goto IL_0086;
		}
	}
	{
	}

IL_027e:
	{
		V_14 = 0;
		goto IL_02a4;
	}

IL_0286:
	{
		int32_t L_90 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_91 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_92 = V_14;
		NullCheck(L_91);
		SendMouseEvents_SendEvents_m3061086367(NULL /*static, unused*/, L_90, (*(HitInfo_t3525137541 *)((L_91)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_92)))), /*hidden argument*/NULL);
		int32_t L_93 = V_14;
		V_14 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_02a4:
	{
		int32_t L_94 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_95 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_95);
		if ((((int32_t)L_94) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_95)->max_length)))))))
		{
			goto IL_0286;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m3061086367 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t3525137541  ___hit1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m3061086367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t3525137541  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1636556003_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m4077202833(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m2505676056(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		HitInfo_t3525137541  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1366484775(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_5 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		HitInfo_t3525137541  L_7 = ___hit1;
		*(HitInfo_t3525137541 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))) = L_7;
		HitInfoU5BU5D_t1685419976* L_8 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		HitInfo_SendMessage_m28937701(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral3814634610, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_0107;
	}

IL_004f:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_11 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		bool L_13 = HitInfo_op_Implicit_m1366484775(NULL /*static, unused*/, (*(HitInfo_t3525137541 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00d0;
		}
	}
	{
		HitInfo_t3525137541  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_15 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		bool L_17 = HitInfo_Compare_m804408274(NULL /*static, unused*/, L_14, (*(HitInfo_t3525137541 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_18 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		HitInfo_SendMessage_m28937701(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral2112713013, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_20 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		HitInfo_SendMessage_m28937701(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral3725919253, /*hidden argument*/NULL);
		HitInfoU5BU5D_t1685419976* L_22 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3525137541  L_24 = V_2;
		*(HitInfo_t3525137541 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23))) = L_24;
	}

IL_00d0:
	{
		goto IL_0107;
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_25 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		bool L_27 = HitInfo_op_Implicit_m1366484775(NULL /*static, unused*/, (*(HitInfo_t3525137541 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0107;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_28 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		HitInfo_SendMessage_m28937701(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral225260280, /*hidden argument*/NULL);
	}

IL_0107:
	{
		HitInfo_t3525137541  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_31 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		bool L_33 = HitInfo_Compare_m804408274(NULL /*static, unused*/, L_30, (*(HitInfo_t3525137541 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0140;
		}
	}
	{
		HitInfo_t3525137541  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1366484775(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_013a;
		}
	}
	{
		HitInfo_SendMessage_m28937701((&___hit1), _stringLiteral956983439, /*hidden argument*/NULL);
	}

IL_013a:
	{
		goto IL_0198;
	}

IL_0140:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_36 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		bool L_38 = HitInfo_op_Implicit_m1366484775(NULL /*static, unused*/, (*(HitInfo_t3525137541 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0172;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_39 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		HitInfo_SendMessage_m28937701(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral3850542936, /*hidden argument*/NULL);
	}

IL_0172:
	{
		HitInfo_t3525137541  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1366484775(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0197;
		}
	}
	{
		HitInfo_SendMessage_m28937701((&___hit1), _stringLiteral392450877, /*hidden argument*/NULL);
		HitInfo_SendMessage_m28937701((&___hit1), _stringLiteral956983439, /*hidden argument*/NULL);
	}

IL_0197:
	{
	}

IL_0198:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1685419976* L_43 = ((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		HitInfo_t3525137541  L_45 = ___hit1;
		*(HitInfo_t3525137541 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern "C"  void SendMouseEvents__cctor_m2605800095 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m2605800095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t3525137541  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t3525137541  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t3525137541  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t3525137541  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t3525137541  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t3525137541  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t3525137541  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t3525137541  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t3525137541  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t1685419976* L_0 = ((HitInfoU5BU5D_t1685419976*)SZArrayNew(HitInfoU5BU5D_t1685419976_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t3525137541  L_1 = V_0;
		*(HitInfo_t3525137541 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_1;
		HitInfoU5BU5D_t1685419976* L_2 = L_0;
		NullCheck(L_2);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t3525137541  L_3 = V_1;
		*(HitInfo_t3525137541 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_3;
		HitInfoU5BU5D_t1685419976* L_4 = L_2;
		NullCheck(L_4);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3525137541  L_5 = V_2;
		*(HitInfo_t3525137541 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_5;
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t1685419976* L_6 = ((HitInfoU5BU5D_t1685419976*)SZArrayNew(HitInfoU5BU5D_t1685419976_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t3525137541  L_7 = V_3;
		*(HitInfo_t3525137541 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_7;
		HitInfoU5BU5D_t1685419976* L_8 = L_6;
		NullCheck(L_8);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t3525137541  L_9 = V_4;
		*(HitInfo_t3525137541 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_9;
		HitInfoU5BU5D_t1685419976* L_10 = L_8;
		NullCheck(L_10);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t3525137541  L_11 = V_5;
		*(HitInfo_t3525137541 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_11;
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t1685419976* L_12 = ((HitInfoU5BU5D_t1685419976*)SZArrayNew(HitInfoU5BU5D_t1685419976_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t3525137541  L_13 = V_6;
		*(HitInfo_t3525137541 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_13;
		HitInfoU5BU5D_t1685419976* L_14 = L_12;
		NullCheck(L_14);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t3525137541  L_15 = V_7;
		*(HitInfo_t3525137541 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_15;
		HitInfoU5BU5D_t1685419976* L_16 = L_14;
		NullCheck(L_16);
		Initobj (HitInfo_t3525137541_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t3525137541  L_17 = V_8;
		*(HitInfo_t3525137541 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_17;
		((SendMouseEvents_t2508014564_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t2508014564_il2cpp_TypeInfo_var))->set_m_CurrentHit_3(L_16);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3525137541_marshal_pinvoke(const HitInfo_t3525137541& unmarshaled, HitInfo_t3525137541_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3525137541_marshal_pinvoke_back(const HitInfo_t3525137541_marshaled_pinvoke& marshaled, HitInfo_t3525137541& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3525137541_marshal_pinvoke_cleanup(HitInfo_t3525137541_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3525137541_marshal_com(const HitInfo_t3525137541& unmarshaled, HitInfo_t3525137541_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3525137541_marshal_com_back(const HitInfo_t3525137541_marshaled_com& marshaled, HitInfo_t3525137541& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3525137541_marshal_com_cleanup(HitInfo_t3525137541_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m28937701 (HitInfo_t3525137541 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		GameObject_t2881801184 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m4104693933(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m28937701_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	HitInfo_t3525137541 * _thisAdjusted = reinterpret_cast<HitInfo_t3525137541 *>(__this + 1);
	HitInfo_SendMessage_m28937701(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m1366484775 (RuntimeObject * __this /* static, unused */, HitInfo_t3525137541  ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m1366484775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t2881801184 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Camera_t362346687 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_2, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m804408274 (RuntimeObject * __this /* static, unused */, HitInfo_t3525137541  ___lhs0, HitInfo_t3525137541  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m804408274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t2881801184 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t2881801184 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Camera_t362346687 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t362346687 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m317391580 (FormerlySerializedAsAttribute_t384831209 * __this, String_t* ___oldName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m1823559208 (SerializeField_t2792443689 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern "C"  void SetupCoroutine_InvokeMoveNext_m1006292583 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___enumerator0, intptr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m1006292583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___returnValueAddress1;
		bool L_1 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m158539512(L_2, _stringLiteral1994973061, _stringLiteral2859367373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		intptr_t L_3 = ___returnValueAddress1;
		void* L_4 = IntPtr_op_Explicit_m259387555(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		RuntimeObject* L_5 = ___enumerator0;
		NullCheck(L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2051810174_il2cpp_TypeInfo_var, L_5);
		*((int8_t*)(L_4)) = (int8_t)L_6;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern "C"  RuntimeObject * SetupCoroutine_InvokeMember_m1820235480 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___behaviour0, String_t* ___name1, RuntimeObject * ___variable2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m1820235480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3385344125* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		V_0 = (ObjectU5BU5D_t3385344125*)NULL;
		RuntimeObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3385344125* L_1 = V_0;
		RuntimeObject * L_2 = ___variable2;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
	}

IL_0016:
	{
		RuntimeObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m392475457(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		RuntimeObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t3385344125* L_7 = V_0;
		NullCheck(L_4);
		RuntimeObject * L_8 = VirtFuncInvoker8< RuntimeObject *, String_t*, int32_t, Binder_t2026886273 *, RuntimeObject *, ObjectU5BU5D_t3385344125*, ParameterModifierU5BU5D_t1706708300*, CultureInfo_t4200194569 *, StringU5BU5D_t1589106382* >::Invoke(75 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t2026886273 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t1706708300*)(ParameterModifierU5BU5D_t1706708300*)NULL, (CultureInfo_t4200194569 *)NULL, (StringU5BU5D_t1589106382*)(StringU5BU5D_t1589106382*)NULL);
		V_1 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		RuntimeObject * L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m740525257 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*Shader_PropertyToID_m740525257_ftn) (String_t*);
	static Shader_PropertyToID_m740525257_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m740525257_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	int32_t retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern "C"  int32_t SortingLayer_GetLayerValueFromID_m2944670197 (RuntimeObject * __this /* static, unused */, int32_t ___id0, const RuntimeMethod* method)
{
	typedef int32_t (*SortingLayer_GetLayerValueFromID_m2944670197_ftn) (int32_t);
	static SortingLayer_GetLayerValueFromID_m2944670197_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SortingLayer_GetLayerValueFromID_m2944670197_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)");
	int32_t retVal = _il2cpp_icall_func(___id0);
	return retVal;
}
// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C"  void SpaceAttribute__ctor_m466721226 (SpaceAttribute_t586498270 * __this, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3690955331(__this, /*hidden argument*/NULL);
		__this->set_height_0((8.0f));
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C"  void SpaceAttribute__ctor_m3098848991 (SpaceAttribute_t586498270 * __this, float ___height0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3690955331(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_height_0(L_0);
		return;
	}
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t952252086  Sprite_get_rect_m1990412030 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t952252086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_rect_m53359737(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t952252086  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t952252086  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m53359737 (Sprite_t2497061588 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m53359737_ftn) (Sprite_t2497061588 *, Rect_t952252086 *);
	static Sprite_INTERNAL_get_rect_m53359737_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m53359737_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C"  float Sprite_get_pixelsPerUnit_m2015965846 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m2015965846_ftn) (Sprite_t2497061588 *);
	static Sprite_get_pixelsPerUnit_m2015965846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m2015965846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t91252194 * Sprite_get_texture_m2376546489 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	typedef Texture2D_t91252194 * (*Sprite_get_texture_m2376546489_ftn) (Sprite_t2497061588 *);
	static Sprite_get_texture_m2376546489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m2376546489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	Texture2D_t91252194 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern "C"  Texture2D_t91252194 * Sprite_get_associatedAlphaSplitTexture_m3947583649 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	typedef Texture2D_t91252194 * (*Sprite_get_associatedAlphaSplitTexture_m3947583649_ftn) (Sprite_t2497061588 *);
	static Sprite_get_associatedAlphaSplitTexture_m3947583649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_associatedAlphaSplitTexture_m3947583649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_associatedAlphaSplitTexture()");
	Texture2D_t91252194 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t952252086  Sprite_get_textureRect_m3285393066 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t952252086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_textureRect_m3501512419(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t952252086  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t952252086  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m3501512419 (Sprite_t2497061588 * __this, Rect_t952252086 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m3501512419_ftn) (Sprite_t2497061588 *, Rect_t952252086 *);
	static Sprite_INTERNAL_get_textureRect_m3501512419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m3501512419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Sprite::get_packed()
extern "C"  bool Sprite_get_packed_m1091495554 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	typedef bool (*Sprite_get_packed_m1091495554_ftn) (Sprite_t2497061588 *);
	static Sprite_get_packed_m1091495554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_packed_m1091495554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_packed()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t1900979187  Sprite_get_border_m3697378104 (Sprite_t2497061588 * __this, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t1900979187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_INTERNAL_get_border_m463112409(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t1900979187  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t1900979187  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m463112409 (Sprite_t2497061588 * __this, Vector4_t1900979187 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m463112409_ftn) (Sprite_t2497061588 *, Vector4_t1900979187 *);
	static Sprite_INTERNAL_get_border_m463112409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m463112409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C"  Vector4_t1900979187  DataUtility_GetInnerUV_m3811387732 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t1900979187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2497061588 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetInnerUV_m2615385289(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t1900979187  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t1900979187  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m2615385289 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector4_t1900979187 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetInnerUV_m2615385289_ftn) (Sprite_t2497061588 *, Vector4_t1900979187 *);
	static DataUtility_INTERNAL_CALL_GetInnerUV_m2615385289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetInnerUV_m2615385289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C"  Vector4_t1900979187  DataUtility_GetOuterUV_m2791511998 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t1900979187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2497061588 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetOuterUV_m1659787860(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t1900979187  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t1900979187  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m1659787860 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector4_t1900979187 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetOuterUV_m1659787860_ftn) (Sprite_t2497061588 *, Vector4_t1900979187 *);
	static DataUtility_INTERNAL_CALL_GetOuterUV_m1659787860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetOuterUV_m1659787860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C"  Vector4_t1900979187  DataUtility_GetPadding_m3772461224 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, const RuntimeMethod* method)
{
	Vector4_t1900979187  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t1900979187  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2497061588 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetPadding_m3631929608(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t1900979187  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t1900979187  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m3631929608 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector4_t1900979187 * ___value1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetPadding_m3631929608_ftn) (Sprite_t2497061588 *, Vector4_t1900979187 *);
	static DataUtility_INTERNAL_CALL_GetPadding_m3631929608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetPadding_m3631929608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C"  Vector2_t2246369278  DataUtility_GetMinSize_m423040031 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Sprite_t2497061588 * L_0 = ___sprite0;
		DataUtility_Internal_GetMinSize_m955661019(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m955661019 (RuntimeObject * __this /* static, unused */, Sprite_t2497061588 * ___sprite0, Vector2_t2246369278 * ___output1, const RuntimeMethod* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m955661019_ftn) (Sprite_t2497061588 *, Vector2_t2246369278 *);
	static DataUtility_Internal_GetMinSize_m955661019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m955661019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite0, ___output1);
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern "C"  void StackTraceUtility_SetProjectFolder_m2765252549 (RuntimeObject * __this /* static, unused */, String_t* ___folder0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_SetProjectFolder_m2765252549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___folder0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m2123998715(L_0, _stringLiteral1097760428, _stringLiteral2544850413, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		((StackTraceUtility_t2534606224_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var))->set_projectFolder_0(L_1);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern "C"  String_t* StackTraceUtility_ExtractStackTrace_m2680497121 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStackTrace_m2680497121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StackTrace_t1780976090 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		StackTrace_t1780976090 * L_0 = (StackTrace_t1780976090 *)il2cpp_codegen_object_new(StackTrace_t1780976090_il2cpp_TypeInfo_var);
		StackTrace__ctor_m3276062597(L_0, 1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1780976090 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m3595538786(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		V_1 = L_3;
		String_t* L_4 = V_1;
		V_2 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m3834892923 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_IsSystemStacktraceType_m3834892923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___name0;
		V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m2243296700(L_1, _stringLiteral3197285762, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m2243296700(L_3, _stringLiteral2268048544, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m2243296700(L_5, _stringLiteral1736793488, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m2243296700(L_7, _stringLiteral3892946235, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m2243296700(L_9, _stringLiteral1040575422, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m2243296700(L_11, _stringLiteral2292808322, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0066;
	}

IL_0065:
	{
		G_B7_0 = 1;
	}

IL_0066:
	{
		V_1 = (bool)G_B7_0;
		goto IL_006c;
	}

IL_006c:
	{
		bool L_13 = V_1;
		return L_13;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern "C"  void StackTraceUtility_ExtractStringFromExceptionInternal_m2867183001 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___exceptiono0, String_t** ___message1, String_t** ___stackTrace2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStringFromExceptionInternal_m2867183001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t214279536 * V_0 = NULL;
	StringBuilder_t209334805 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	StackTrace_t1780976090 * V_5 = NULL;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___exceptiono0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentException_t489606696 * L_1 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_1, _stringLiteral742218889, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject * L_2 = ___exceptiono0;
		V_0 = ((Exception_t214279536 *)IsInstClass((RuntimeObject*)L_2, Exception_t214279536_il2cpp_TypeInfo_var));
		Exception_t214279536 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentException_t489606696 * L_4 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_4, _stringLiteral2281730836, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002a:
	{
		Exception_t214279536 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004c;
	}

IL_003f:
	{
		Exception_t214279536 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m293866894(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004c:
	{
		StringBuilder_t209334805 * L_10 = (StringBuilder_t209334805 *)il2cpp_codegen_object_new(StringBuilder_t209334805_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1547787485(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message1;
		*((RuntimeObject **)(L_11)) = (RuntimeObject *)_stringLiteral2637738159;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_11), (RuntimeObject *)_stringLiteral2637738159);
		V_2 = _stringLiteral2637738159;
		goto IL_0106;
	}

IL_0064:
	{
		String_t* L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m293866894(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		Exception_t214279536 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_14);
		V_2 = L_15;
		goto IL_008e;
	}

IL_007c:
	{
		Exception_t214279536 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		String_t* L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3213599556(NULL /*static, unused*/, L_17, _stringLiteral2874725322, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
	}

IL_008e:
	{
		Exception_t214279536 * L_20 = V_0;
		NullCheck(L_20);
		Type_t * L_21 = Exception_GetType_m1907036829(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		V_3 = L_22;
		V_4 = _stringLiteral2637738159;
		Exception_t214279536 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_23);
		if (!L_24)
		{
			goto IL_00b4;
		}
	}
	{
		Exception_t214279536 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_25);
		V_4 = L_26;
	}

IL_00b4:
	{
		String_t* L_27 = V_4;
		NullCheck(L_27);
		String_t* L_28 = String_Trim_m3557264989(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m293866894(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00dc;
		}
	}
	{
		String_t* L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m2806582524(NULL /*static, unused*/, L_30, _stringLiteral2186841919, /*hidden argument*/NULL);
		V_3 = L_31;
		String_t* L_32 = V_3;
		String_t* L_33 = V_4;
		String_t* L_34 = String_Concat_m2806582524(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		V_3 = L_34;
	}

IL_00dc:
	{
		String_t** L_35 = ___message1;
		String_t* L_36 = V_3;
		*((RuntimeObject **)(L_35)) = (RuntimeObject *)L_36;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_35), (RuntimeObject *)L_36);
		Exception_t214279536 * L_37 = V_0;
		NullCheck(L_37);
		Exception_t214279536 * L_38 = Exception_get_InnerException_m4271498349(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00fe;
		}
	}
	{
		String_t* L_39 = V_3;
		String_t* L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m333645061(NULL /*static, unused*/, _stringLiteral2413282949, L_39, _stringLiteral2874725322, L_40, /*hidden argument*/NULL);
		V_2 = L_41;
	}

IL_00fe:
	{
		Exception_t214279536 * L_42 = V_0;
		NullCheck(L_42);
		Exception_t214279536 * L_43 = Exception_get_InnerException_m4271498349(L_42, /*hidden argument*/NULL);
		V_0 = L_43;
	}

IL_0106:
	{
		Exception_t214279536 * L_44 = V_0;
		if (L_44)
		{
			goto IL_0064;
		}
	}
	{
		StringBuilder_t209334805 * L_45 = V_1;
		String_t* L_46 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m2806582524(NULL /*static, unused*/, L_46, _stringLiteral2874725322, /*hidden argument*/NULL);
		NullCheck(L_45);
		StringBuilder_Append_m3622130694(L_45, L_47, /*hidden argument*/NULL);
		StackTrace_t1780976090 * L_48 = (StackTrace_t1780976090 *)il2cpp_codegen_object_new(StackTrace_t1780976090_il2cpp_TypeInfo_var);
		StackTrace__ctor_m3276062597(L_48, 1, (bool)1, /*hidden argument*/NULL);
		V_5 = L_48;
		StringBuilder_t209334805 * L_49 = V_1;
		StackTrace_t1780976090 * L_50 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		String_t* L_51 = StackTraceUtility_ExtractFormattedStackTrace_m3595538786(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		StringBuilder_Append_m3622130694(L_49, L_51, /*hidden argument*/NULL);
		String_t** L_52 = ___stackTrace2;
		StringBuilder_t209334805 * L_53 = V_1;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_53);
		*((RuntimeObject **)(L_52)) = (RuntimeObject *)L_54;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_52), (RuntimeObject *)L_54);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern "C"  String_t* StackTraceUtility_PostprocessStacktrace_m3052697160 (RuntimeObject * __this /* static, unused */, String_t* ___oldString0, bool ___stripEngineInternalInformation1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_PostprocessStacktrace_m3052697160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t1589106382* V_1 = NULL;
	StringBuilder_t209334805 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		String_t* L_0 = ___oldString0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_1;
		goto IL_02a4;
	}

IL_0012:
	{
		String_t* L_2 = ___oldString0;
		CharU5BU5D_t2953840665* L_3 = ((CharU5BU5D_t2953840665*)SZArrayNew(CharU5BU5D_t2953840665_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_2);
		StringU5BU5D_t1589106382* L_4 = String_Split_m3508706380(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = ___oldString0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m293866894(L_5, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_7 = (StringBuilder_t209334805 *)il2cpp_codegen_object_new(StringBuilder_t209334805_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1547787485(L_7, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_0046;
	}

IL_0037:
	{
		StringU5BU5D_t1589106382* L_8 = V_1;
		int32_t L_9 = V_3;
		StringU5BU5D_t1589106382* L_10 = V_1;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m3557264989(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_14);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_14);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_16 = V_3;
		StringU5BU5D_t1589106382* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		V_4 = 0;
		goto IL_028e;
	}

IL_0057:
	{
		StringU5BU5D_t1589106382* L_18 = V_1;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_5 = L_21;
		String_t* L_22 = V_5;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m293866894(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_24 = V_5;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m1611231861(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_007e;
		}
	}

IL_0079:
	{
		goto IL_0288;
	}

IL_007e:
	{
		String_t* L_26 = V_5;
		NullCheck(L_26);
		bool L_27 = String_StartsWith_m2243296700(L_26, _stringLiteral4089598773, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_0288;
	}

IL_0094:
	{
		bool L_28 = ___stripEngineInternalInformation1;
		if (!L_28)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_29 = V_5;
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m2243296700(L_29, _stringLiteral2409193522, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00b0;
		}
	}
	{
		goto IL_0298;
	}

IL_00b0:
	{
		bool L_31 = ___stripEngineInternalInformation1;
		if (!L_31)
		{
			goto IL_0107;
		}
	}
	{
		int32_t L_32 = V_4;
		StringU5BU5D_t1589106382* L_33 = V_1;
		NullCheck(L_33);
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_33)->max_length))))-(int32_t)1)))))
		{
			goto IL_0107;
		}
	}
	{
		String_t* L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		bool L_35 = StackTraceUtility_IsSystemStacktraceType_m3834892923(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0107;
		}
	}
	{
		StringU5BU5D_t1589106382* L_36 = V_1;
		int32_t L_37 = V_4;
		NullCheck(L_36);
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		String_t* L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		bool L_40 = StackTraceUtility_IsSystemStacktraceType_m3834892923(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00e4;
		}
	}
	{
		goto IL_0288;
	}

IL_00e4:
	{
		String_t* L_41 = V_5;
		NullCheck(L_41);
		int32_t L_42 = String_IndexOf_m3835433624(L_41, _stringLiteral3585422989, /*hidden argument*/NULL);
		V_6 = L_42;
		int32_t L_43 = V_6;
		if ((((int32_t)L_43) == ((int32_t)(-1))))
		{
			goto IL_0106;
		}
	}
	{
		String_t* L_44 = V_5;
		int32_t L_45 = V_6;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m1162620119(L_44, 0, L_45, /*hidden argument*/NULL);
		V_5 = L_46;
	}

IL_0106:
	{
	}

IL_0107:
	{
		String_t* L_47 = V_5;
		NullCheck(L_47);
		int32_t L_48 = String_IndexOf_m3835433624(L_47, _stringLiteral4228279569, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)(-1))))
		{
			goto IL_011e;
		}
	}
	{
		goto IL_0288;
	}

IL_011e:
	{
		String_t* L_49 = V_5;
		NullCheck(L_49);
		int32_t L_50 = String_IndexOf_m3835433624(L_49, _stringLiteral1058914747, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)(-1))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_0288;
	}

IL_0135:
	{
		String_t* L_51 = V_5;
		NullCheck(L_51);
		int32_t L_52 = String_IndexOf_m3835433624(L_51, _stringLiteral230745881, /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)(-1))))
		{
			goto IL_014c;
		}
	}
	{
		goto IL_0288;
	}

IL_014c:
	{
		bool L_53 = ___stripEngineInternalInformation1;
		if (!L_53)
		{
			goto IL_0179;
		}
	}
	{
		String_t* L_54 = V_5;
		NullCheck(L_54);
		bool L_55 = String_StartsWith_m2243296700(L_54, _stringLiteral1570534719, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0179;
		}
	}
	{
		String_t* L_56 = V_5;
		NullCheck(L_56);
		bool L_57 = String_EndsWith_m2337007599(L_56, _stringLiteral2076976895, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0179;
		}
	}
	{
		goto IL_0288;
	}

IL_0179:
	{
		String_t* L_58 = V_5;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m2243296700(L_58, _stringLiteral2123922509, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0197;
		}
	}
	{
		String_t* L_60 = V_5;
		NullCheck(L_60);
		String_t* L_61 = String_Remove_m4198834713(L_60, 0, 3, /*hidden argument*/NULL);
		V_5 = L_61;
	}

IL_0197:
	{
		String_t* L_62 = V_5;
		NullCheck(L_62);
		int32_t L_63 = String_IndexOf_m3835433624(L_62, _stringLiteral586184310, /*hidden argument*/NULL);
		V_7 = L_63;
		V_8 = (-1);
		int32_t L_64 = V_7;
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01c0;
		}
	}
	{
		String_t* L_65 = V_5;
		int32_t L_66 = V_7;
		NullCheck(L_65);
		int32_t L_67 = String_IndexOf_m4283618274(L_65, _stringLiteral2076976895, L_66, /*hidden argument*/NULL);
		V_8 = L_67;
	}

IL_01c0:
	{
		int32_t L_68 = V_7;
		if ((((int32_t)L_68) == ((int32_t)(-1))))
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_69 = V_8;
		int32_t L_70 = V_7;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_01e5;
		}
	}
	{
		String_t* L_71 = V_5;
		int32_t L_72 = V_7;
		int32_t L_73 = V_8;
		int32_t L_74 = V_7;
		NullCheck(L_71);
		String_t* L_75 = String_Remove_m4198834713(L_71, L_72, ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)L_74))+(int32_t)1)), /*hidden argument*/NULL);
		V_5 = L_75;
	}

IL_01e5:
	{
		String_t* L_76 = V_5;
		NullCheck(L_76);
		String_t* L_77 = String_Replace_m2123998715(L_76, _stringLiteral1728958755, _stringLiteral2637738159, /*hidden argument*/NULL);
		V_5 = L_77;
		String_t* L_78 = V_5;
		NullCheck(L_78);
		String_t* L_79 = String_Replace_m2123998715(L_78, _stringLiteral1097760428, _stringLiteral2544850413, /*hidden argument*/NULL);
		V_5 = L_79;
		String_t* L_80 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		String_t* L_81 = ((StackTraceUtility_t2534606224_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_80);
		String_t* L_82 = String_Replace_m2123998715(L_80, L_81, _stringLiteral2637738159, /*hidden argument*/NULL);
		V_5 = L_82;
		String_t* L_83 = V_5;
		NullCheck(L_83);
		String_t* L_84 = String_Replace_m3590694696(L_83, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_5 = L_84;
		String_t* L_85 = V_5;
		NullCheck(L_85);
		int32_t L_86 = String_LastIndexOf_m1985287707(L_85, _stringLiteral537023139, /*hidden argument*/NULL);
		V_9 = L_86;
		int32_t L_87 = V_9;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_0274;
		}
	}
	{
		String_t* L_88 = V_5;
		int32_t L_89 = V_9;
		NullCheck(L_88);
		String_t* L_90 = String_Remove_m4198834713(L_88, L_89, 5, /*hidden argument*/NULL);
		V_5 = L_90;
		String_t* L_91 = V_5;
		int32_t L_92 = V_9;
		NullCheck(L_91);
		String_t* L_93 = String_Insert_m2763449893(L_91, L_92, _stringLiteral3158739129, /*hidden argument*/NULL);
		V_5 = L_93;
		String_t* L_94 = V_5;
		String_t* L_95 = V_5;
		NullCheck(L_95);
		int32_t L_96 = String_get_Length_m293866894(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_97 = String_Insert_m2763449893(L_94, L_96, _stringLiteral744691041, /*hidden argument*/NULL);
		V_5 = L_97;
	}

IL_0274:
	{
		StringBuilder_t209334805 * L_98 = V_2;
		String_t* L_99 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m2806582524(NULL /*static, unused*/, L_99, _stringLiteral2874725322, /*hidden argument*/NULL);
		NullCheck(L_98);
		StringBuilder_Append_m3622130694(L_98, L_100, /*hidden argument*/NULL);
	}

IL_0288:
	{
		int32_t L_101 = V_4;
		V_4 = ((int32_t)((int32_t)L_101+(int32_t)1));
	}

IL_028e:
	{
		int32_t L_102 = V_4;
		StringU5BU5D_t1589106382* L_103 = V_1;
		NullCheck(L_103);
		if ((((int32_t)L_102) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_103)->max_length)))))))
		{
			goto IL_0057;
		}
	}

IL_0298:
	{
		StringBuilder_t209334805 * L_104 = V_2;
		NullCheck(L_104);
		String_t* L_105 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_104);
		V_0 = L_105;
		goto IL_02a4;
	}

IL_02a4:
	{
		String_t* L_106 = V_0;
		return L_106;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m3595538786 (RuntimeObject * __this /* static, unused */, StackTrace_t1780976090 * ___stackTrace0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractFormattedStackTrace_m3595538786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t209334805 * V_0 = NULL;
	int32_t V_1 = 0;
	StackFrame_t3907017807 * V_2 = NULL;
	MethodBase_t3670318294 * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t1048445298* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	String_t* V_12 = NULL;
	int32_t G_B27_0 = 0;
	int32_t G_B29_0 = 0;
	{
		StringBuilder_t209334805 * L_0 = (StringBuilder_t209334805 *)il2cpp_codegen_object_new(StringBuilder_t209334805_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1547787485(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_02ba;
	}

IL_0013:
	{
		StackTrace_t1780976090 * L_1 = ___stackTrace0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t3907017807 * L_3 = VirtFuncInvoker1< StackFrame_t3907017807 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t3907017807 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t3670318294 * L_5 = VirtFuncInvoker0< MethodBase_t3670318294 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t3670318294 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_02b6;
	}

IL_002e:
	{
		MethodBase_t3670318294 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_02b6;
	}

IL_0042:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m293866894(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0075;
		}
	}
	{
		StringBuilder_t209334805 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m3622130694(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m3622130694(L_17, _stringLiteral2979430886, /*hidden argument*/NULL);
	}

IL_0075:
	{
		StringBuilder_t209334805 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m3622130694(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3622130694(L_21, _stringLiteral1597761991, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_22 = V_0;
		MethodBase_t3670318294 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m3622130694(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3622130694(L_25, _stringLiteral1856989646, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t3670318294 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t1048445298* L_27 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1048445298* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = (bool)1;
		goto IL_00f4;
	}

IL_00bb:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00d4;
		}
	}
	{
		StringBuilder_t209334805 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m3622130694(L_29, _stringLiteral2970545265, /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_00d4:
	{
		V_8 = (bool)0;
	}

IL_00d7:
	{
		StringBuilder_t209334805 * L_30 = V_0;
		ParameterInfoU5BU5D_t1048445298* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		ParameterInfo_t3156340899 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		NullCheck(L_30);
		StringBuilder_Append_m3622130694(L_30, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00f4:
	{
		int32_t L_38 = V_6;
		ParameterInfoU5BU5D_t1048445298* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_39)->max_length)))))))
		{
			goto IL_00bb;
		}
	}
	{
		StringBuilder_t209334805 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m3622130694(L_40, _stringLiteral744691041, /*hidden argument*/NULL);
		StackFrame_t3907017807 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_41);
		V_9 = L_42;
		String_t* L_43 = V_9;
		if (!L_43)
		{
			goto IL_02a9;
		}
	}
	{
		Type_t * L_44 = V_4;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_45, _stringLiteral3780346290, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0147;
		}
	}
	{
		Type_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_48, _stringLiteral1922078842, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_020c;
		}
	}

IL_0147:
	{
		Type_t * L_50 = V_4;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_51, _stringLiteral3566379873, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0173;
		}
	}
	{
		Type_t * L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_54, _stringLiteral1922078842, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_020c;
		}
	}

IL_0173:
	{
		Type_t * L_56 = V_4;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_57, _stringLiteral2690811494, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_019f;
		}
	}
	{
		Type_t * L_59 = V_4;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_60, _stringLiteral1922078842, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_020c;
		}
	}

IL_019f:
	{
		Type_t * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_63, _stringLiteral2280325812, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01cb;
		}
	}
	{
		Type_t * L_65 = V_4;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_66, _stringLiteral442781658, /*hidden argument*/NULL);
		if (L_67)
		{
			goto IL_020c;
		}
	}

IL_01cb:
	{
		MethodBase_t3670318294 * L_68 = V_3;
		NullCheck(L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_68);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_70 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_69, _stringLiteral4036302056, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_71 = V_4;
		NullCheck(L_71);
		String_t* L_72 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_71);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_73 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_72, _stringLiteral3391310575, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0209;
		}
	}
	{
		Type_t * L_74 = V_4;
		NullCheck(L_74);
		String_t* L_75 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_74);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_76 = String_op_Equality_m3241538055(NULL /*static, unused*/, L_75, _stringLiteral1922078842, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)(L_76));
		goto IL_020a;
	}

IL_0209:
	{
		G_B27_0 = 0;
	}

IL_020a:
	{
		G_B29_0 = G_B27_0;
		goto IL_020d;
	}

IL_020c:
	{
		G_B29_0 = 1;
	}

IL_020d:
	{
		V_10 = (bool)G_B29_0;
		bool L_77 = V_10;
		if (L_77)
		{
			goto IL_02a8;
		}
	}
	{
		StringBuilder_t209334805 * L_78 = V_0;
		NullCheck(L_78);
		StringBuilder_Append_m3622130694(L_78, _stringLiteral3158739129, /*hidden argument*/NULL);
		String_t* L_79 = V_9;
		NullCheck(L_79);
		String_t* L_80 = String_Replace_m2123998715(L_79, _stringLiteral1097760428, _stringLiteral2544850413, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		String_t* L_81 = ((StackTraceUtility_t2534606224_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_80);
		bool L_82 = String_StartsWith_m2243296700(L_80, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_026a;
		}
	}
	{
		String_t* L_83 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var);
		String_t* L_84 = ((StackTraceUtility_t2534606224_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_84);
		int32_t L_85 = String_get_Length_m293866894(L_84, /*hidden argument*/NULL);
		String_t* L_86 = V_9;
		NullCheck(L_86);
		int32_t L_87 = String_get_Length_m293866894(L_86, /*hidden argument*/NULL);
		String_t* L_88 = ((StackTraceUtility_t2534606224_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var))->get_projectFolder_0();
		NullCheck(L_88);
		int32_t L_89 = String_get_Length_m293866894(L_88, /*hidden argument*/NULL);
		NullCheck(L_83);
		String_t* L_90 = String_Substring_m1162620119(L_83, L_85, ((int32_t)((int32_t)L_87-(int32_t)L_89)), /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_026a:
	{
		StringBuilder_t209334805 * L_91 = V_0;
		String_t* L_92 = V_9;
		NullCheck(L_91);
		StringBuilder_Append_m3622130694(L_91, L_92, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_93 = V_0;
		NullCheck(L_93);
		StringBuilder_Append_m3622130694(L_93, _stringLiteral1597761991, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_94 = V_0;
		StackFrame_t3907017807 * L_95 = V_2;
		NullCheck(L_95);
		int32_t L_96 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_95);
		V_11 = L_96;
		String_t* L_97 = Int32_ToString_m142941626((&V_11), /*hidden argument*/NULL);
		NullCheck(L_94);
		StringBuilder_Append_m3622130694(L_94, L_97, /*hidden argument*/NULL);
		StringBuilder_t209334805 * L_98 = V_0;
		NullCheck(L_98);
		StringBuilder_Append_m3622130694(L_98, _stringLiteral744691041, /*hidden argument*/NULL);
	}

IL_02a8:
	{
	}

IL_02a9:
	{
		StringBuilder_t209334805 * L_99 = V_0;
		NullCheck(L_99);
		StringBuilder_Append_m3622130694(L_99, _stringLiteral2874725322, /*hidden argument*/NULL);
	}

IL_02b6:
	{
		int32_t L_100 = V_1;
		V_1 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02ba:
	{
		int32_t L_101 = V_1;
		StackTrace_t1780976090 * L_102 = ___stackTrace0;
		NullCheck(L_102);
		int32_t L_103 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_102);
		if ((((int32_t)L_101) < ((int32_t)L_103)))
		{
			goto IL_0013;
		}
	}
	{
		StringBuilder_t209334805 * L_104 = V_0;
		NullCheck(L_104);
		String_t* L_105 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_104);
		V_12 = L_105;
		goto IL_02d3;
	}

IL_02d3:
	{
		String_t* L_106 = V_12;
		return L_106;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern "C"  void StackTraceUtility__cctor_m988910645 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility__cctor_m988910645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((StackTraceUtility_t2534606224_StaticFields*)il2cpp_codegen_static_fields_for(StackTraceUtility_t2534606224_il2cpp_TypeInfo_var))->set_projectFolder_0(_stringLiteral2637738159);
		return;
	}
}
// UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern "C"  int32_t SystemInfo_get_operatingSystemFamily_m77710511 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_operatingSystemFamily_m77710511_ftn) ();
	static SystemInfo_get_operatingSystemFamily_m77710511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystemFamily_m77710511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystemFamily()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TextAreaAttribute__ctor_m2536522519 (TextAreaAttribute_t2141263104 * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3690955331(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines0;
		__this->set_minLines_0(L_0);
		int32_t L_1 = ___maxLines1;
		__this->set_maxLines_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m57122759 (Texture_t954505614 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture__ctor_m57122759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		Object__ctor_m2395954052(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m3807402336 (RuntimeObject * __this /* static, unused */, Texture_t954505614 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m3807402336_ftn) (Texture_t954505614 *);
	static Texture_Internal_GetWidth_m3807402336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m3807402336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m3706805582 (RuntimeObject * __this /* static, unused */, Texture_t954505614 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m3706805582_ftn) (Texture_t954505614 *);
	static Texture_Internal_GetHeight_m3706805582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m3706805582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m4278059958 (Texture_t954505614 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetWidth_m3807402336(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m350999365 (Texture_t954505614 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetHeight_m3706805582(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m3782441195 (Texture_t954505614 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_filterMode_m3782441195_ftn) (Texture_t954505614 *, int32_t);
	static Texture_set_filterMode_m3782441195_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m3782441195_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern "C"  int32_t Texture_get_wrapMode_m1478607365 (Texture_t954505614 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_get_wrapMode_m1478607365_ftn) (Texture_t954505614 *);
	static Texture_get_wrapMode_m1478607365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_get_wrapMode_m1478607365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::get_wrapMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m3779086107 (Texture_t954505614 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_wrapMode_m3779086107_ftn) (Texture_t954505614 *, int32_t);
	static Texture_set_wrapMode_m3779086107_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_wrapMode_m3779086107_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern "C"  Vector2_t2246369278  Texture_get_texelSize_m3873742773 (Texture_t954505614 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Texture_INTERNAL_get_texelSize_m1340325120(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2246369278  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m1340325120 (Texture_t954505614 * __this, Vector2_t2246369278 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_INTERNAL_get_texelSize_m1340325120_ftn) (Texture_t954505614 *, Vector2_t2246369278 *);
	static Texture_INTERNAL_get_texelSize_m1340325120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_INTERNAL_get_texelSize_m1340325120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m2972303337 (Texture2D_t91252194 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m2972303337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m57122759(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		Texture2D_Internal_Create_m3280886297(NULL /*static, unused*/, __this, L_0, L_1, 4, (bool)1, (bool)0, (intptr_t)(0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D__ctor_m1755139504 (Texture2D_t91252194 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method)
{
	{
		Texture__ctor_m57122759(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		bool L_4 = ___linear4;
		intptr_t L_5 = ___nativeTex5;
		Texture2D_Internal_Create_m3280886297(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m3280886297 (RuntimeObject * __this /* static, unused */, Texture2D_t91252194 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, intptr_t ___nativeTex6, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Internal_Create_m3280886297_ftn) (Texture2D_t91252194 *, int32_t, int32_t, int32_t, bool, bool, intptr_t);
	static Texture2D_Internal_Create_m3280886297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m3280886297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  Texture2D_t91252194 * Texture2D_CreateExternalTexture_m235343200 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D_CreateExternalTexture_m235343200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t91252194 * V_0 = NULL;
	{
		intptr_t L_0 = ___nativeTex5;
		bool L_1 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t489606696 * L_2 = (ArgumentException_t489606696 *)il2cpp_codegen_object_new(ArgumentException_t489606696_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m429090071(L_2, _stringLiteral3175759356, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		int32_t L_3 = ___width0;
		int32_t L_4 = ___height1;
		int32_t L_5 = ___format2;
		bool L_6 = ___mipmap3;
		bool L_7 = ___linear4;
		intptr_t L_8 = ___nativeTex5;
		Texture2D_t91252194 * L_9 = (Texture2D_t91252194 *)il2cpp_codegen_object_new(Texture2D_t91252194_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1755139504(L_9, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0030;
	}

IL_0030:
	{
		Texture2D_t91252194 * L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern "C"  void Texture2D_UpdateExternalTexture_m4206825322 (Texture2D_t91252194 * __this, intptr_t ___nativeTex0, const RuntimeMethod* method)
{
	typedef void (*Texture2D_UpdateExternalTexture_m4206825322_ftn) (Texture2D_t91252194 *, intptr_t);
	static Texture2D_UpdateExternalTexture_m4206825322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_UpdateExternalTexture_m4206825322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)");
	_il2cpp_icall_func(__this, ___nativeTex0);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C"  Texture2D_t91252194 * Texture2D_get_whiteTexture_m1129212270 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Texture2D_t91252194 * (*Texture2D_get_whiteTexture_m1129212270_ftn) ();
	static Texture2D_get_whiteTexture_m1129212270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m1129212270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	Texture2D_t91252194 * retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C"  Color_t1361298052  Texture2D_GetPixelBilinear_m506938874 (Texture2D_t91252194 * __this, float ___u0, float ___v1, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t1361298052  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___u0;
		float L_1 = ___v1;
		Texture2D_INTERNAL_CALL_GetPixelBilinear_m3746843605(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t1361298052  L_2 = V_0;
		V_1 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Color_t1361298052  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m3746843605 (RuntimeObject * __this /* static, unused */, Texture2D_t91252194 * ___self0, float ___u1, float ___v2, Color_t1361298052 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixelBilinear_m3746843605_ftn) (Texture2D_t91252194 *, float, float, Color_t1361298052 *);
	static Texture2D_INTERNAL_CALL_GetPixelBilinear_m3746843605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixelBilinear_m3746843605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___u1, ___v2, ___value3);
}
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m2370990271 (Texture2D_t91252194 * __this, Color32U5BU5D_t2352998046* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	typedef void (*Texture2D_SetAllPixels32_m2370990271_ftn) (Texture2D_t91252194 *, Color32U5BU5D_t2352998046*, int32_t);
	static Texture2D_SetAllPixels32_m2370990271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetAllPixels32_m2370990271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors0, ___miplevel1);
}
// System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetBlockOfPixels32_m727128491 (Texture2D_t91252194 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t2352998046* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method)
{
	typedef void (*Texture2D_SetBlockOfPixels32_m727128491_ftn) (Texture2D_t91252194 *, int32_t, int32_t, int32_t, int32_t, Color32U5BU5D_t2352998046*, int32_t);
	static Texture2D_SetBlockOfPixels32_m727128491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetBlockOfPixels32_m727128491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m3654505251 (Texture2D_t91252194 * __this, Color32U5BU5D_t2352998046* ___colors0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Color32U5BU5D_t2352998046* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels32_m415162269(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m415162269 (Texture2D_t91252194 * __this, Color32U5BU5D_t2352998046* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	{
		Color32U5BU5D_t2352998046* L_0 = ___colors0;
		int32_t L_1 = ___miplevel1;
		Texture2D_SetAllPixels32_m2370990271(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m2644004579 (Texture2D_t91252194 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t2352998046* ___colors4, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___blockWidth2;
		int32_t L_3 = ___blockHeight3;
		Color32U5BU5D_t2352998046* L_4 = ___colors4;
		int32_t L_5 = V_0;
		Texture2D_SetPixels32_m3719091099(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m3719091099 (Texture2D_t91252194 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, Color32U5BU5D_t2352998046* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___blockWidth2;
		int32_t L_3 = ___blockHeight3;
		Color32U5BU5D_t2352998046* L_4 = ___colors4;
		int32_t L_5 = ___miplevel5;
		Texture2D_SetBlockOfPixels32_m727128491(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m1388850756 (Texture2D_t91252194 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Apply_m1388850756_ftn) (Texture2D_t91252194 *, bool, bool);
	static Texture2D_Apply_m1388850756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m1388850756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m2708811936 (Texture2D_t91252194 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m1388850756(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ThreadAndSerializationSafeAttribute::.ctor()
extern "C"  void ThreadAndSerializationSafeAttribute__ctor_m829506510 (ThreadAndSerializationSafeAttribute_t2870195091 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m30995859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2801171705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_deltaTime_m2801171705_ftn) ();
	static Time_get_deltaTime_m2801171705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m2801171705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m2940816486 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledTime_m2940816486_ftn) ();
	static Time_get_unscaledTime_m2940816486_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m2940816486_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m3953826238 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m3953826238_ftn) ();
	static Time_get_unscaledDeltaTime_m3953826238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m3953826238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m1933914159 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m1933914159_ftn) ();
	static Time_get_realtimeSinceStartup_m1933914159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m1933914159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m269072515 (TooltipAttribute_t3230141494 * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m3690955331(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_tooltip_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m4171647058 (Touch_t2641233222 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FingerId_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_fingerId_m4171647058_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t2641233222 * _thisAdjusted = reinterpret_cast<Touch_t2641233222 *>(__this + 1);
	return Touch_get_fingerId_m4171647058(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2246369278  Touch_get_position_m3725642488 (Touch_t2641233222 * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2246369278  L_0 = __this->get_m_Position_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2246369278  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2246369278  Touch_get_position_m3725642488_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t2641233222 * _thisAdjusted = reinterpret_cast<Touch_t2641233222 *>(__this + 1);
	return Touch_get_position_m3725642488(_thisAdjusted, method);
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m2788782435 (Touch_t2641233222 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Phase_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_phase_m2788782435_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t2641233222 * _thisAdjusted = reinterpret_cast<Touch_t2641233222 *>(__this + 1);
	return Touch_get_phase_m2788782435(_thisAdjusted, method);
}
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3850289965 (Touch_t2641233222 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Type_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_type_m3850289965_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t2641233222 * _thisAdjusted = reinterpret_cast<Touch_t2641233222 *>(__this + 1);
	return Touch_get_type_m3850289965(_thisAdjusted, method);
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m3688752920 (TouchScreenKeyboard_t1683994280 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard__ctor_m3688752920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType1;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(TouchScreenKeyboardType_t1103124205_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2116446054_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m1199381511(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->set_keyboardType_0(L_3);
		bool L_4 = ___autocorrection2;
		uint32_t L_5 = Convert_ToUInt32_m3661845794(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->set_autocorrection_1(L_5);
		bool L_6 = ___multiline3;
		uint32_t L_7 = Convert_ToUInt32_m3661845794(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->set_multiline_2(L_7);
		bool L_8 = ___secure4;
		uint32_t L_9 = Convert_ToUInt32_m3661845794(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_secure_3(L_9);
		bool L_10 = ___alert5;
		uint32_t L_11 = Convert_ToUInt32_m3661845794(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->set_alert_4(L_11);
		String_t* L_12 = ___text0;
		String_t* L_13 = ___textPlaceholder6;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3742394844(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m3424133161 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m3424133161_ftn) (TouchScreenKeyboard_t1683994280 *);
	static TouchScreenKeyboard_Destroy_m3424133161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m3424133161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m2766124957 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m3424133161(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2448312333(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3742394844 (TouchScreenKeyboard_t1683994280 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3742394844_ftn) (TouchScreenKeyboard_t1683994280 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t1527619309 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3742394844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m3742394844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments0, ___text1, ___textPlaceholder2);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C"  bool TouchScreenKeyboard_get_isSupported_m2728162255 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = Application_get_platform_m2201190236(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))))
		{
			case 0:
			{
				goto IL_006d;
			}
			case 1:
			{
				goto IL_006d;
			}
			case 2:
			{
				goto IL_006d;
			}
			case 3:
			{
				goto IL_0034;
			}
			case 4:
			{
				goto IL_0034;
			}
			case 5:
			{
				goto IL_0066;
			}
			case 6:
			{
				goto IL_0034;
			}
			case 7:
			{
				goto IL_0034;
			}
			case 8:
			{
				goto IL_0066;
			}
		}
	}

IL_0034:
	{
		int32_t L_2 = V_0;
		switch (((int32_t)((int32_t)L_2-(int32_t)((int32_t)30))))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0066;
			}
			case 2:
			{
				goto IL_0066;
			}
		}
	}
	{
		int32_t L_3 = V_0;
		switch (((int32_t)((int32_t)L_3-(int32_t)8)))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0074;
			}
			case 2:
			{
				goto IL_0074;
			}
			case 3:
			{
				goto IL_0066;
			}
		}
	}
	{
		goto IL_0074;
	}

IL_0066:
	{
		V_1 = (bool)1;
		goto IL_007b;
	}

IL_006d:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_0074:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_007b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t1683994280 * TouchScreenKeyboard_Open_m3102498094 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m3102498094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	TouchScreenKeyboard_t1683994280 * V_2 = NULL;
	{
		V_0 = _stringLiteral2637738159;
		V_1 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1683994280 * L_7 = TouchScreenKeyboard_Open_m2096239018(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_001c;
	}

IL_001c:
	{
		TouchScreenKeyboard_t1683994280 * L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t1683994280 * TouchScreenKeyboard_Open_m2332844710 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m2332844710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	TouchScreenKeyboard_t1683994280 * V_3 = NULL;
	{
		V_0 = _stringLiteral2637738159;
		V_1 = (bool)0;
		V_2 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = V_2;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1683994280 * L_7 = TouchScreenKeyboard_Open_m2096239018(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		goto IL_001d;
	}

IL_001d:
	{
		TouchScreenKeyboard_t1683994280 * L_8 = V_3;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t1683994280 * TouchScreenKeyboard_Open_m2096239018 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m2096239018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_t1683994280 * V_0 = NULL;
	{
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = ___alert5;
		String_t* L_6 = ___textPlaceholder6;
		TouchScreenKeyboard_t1683994280 * L_7 = (TouchScreenKeyboard_t1683994280 *)il2cpp_codegen_object_new(TouchScreenKeyboard_t1683994280_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m3688752920(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0016;
	}

IL_0016:
	{
		TouchScreenKeyboard_t1683994280 * L_8 = V_0;
		return L_8;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C"  String_t* TouchScreenKeyboard_get_text_m398749616 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m398749616_ftn) (TouchScreenKeyboard_t1683994280 *);
	static TouchScreenKeyboard_get_text_m398749616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m398749616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C"  void TouchScreenKeyboard_set_text_m3345933536 (TouchScreenKeyboard_t1683994280 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m3345933536_ftn) (TouchScreenKeyboard_t1683994280 *, String_t*);
	static TouchScreenKeyboard_set_text_m3345933536_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m3345933536_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_hideInput_m1314180630 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m1314180630_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m1314180630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m1314180630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C"  bool TouchScreenKeyboard_get_active_m2328391735 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m2328391735_ftn) (TouchScreenKeyboard_t1683994280 *);
	static TouchScreenKeyboard_get_active_m2328391735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m2328391735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_active_m90826581 (TouchScreenKeyboard_t1683994280 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m90826581_ftn) (TouchScreenKeyboard_t1683994280 *, bool);
	static TouchScreenKeyboard_set_active_m90826581_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m90826581_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C"  bool TouchScreenKeyboard_get_done_m3479232618 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m3479232618_ftn) (TouchScreenKeyboard_t1683994280 *);
	static TouchScreenKeyboard_get_done_m3479232618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m3479232618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C"  bool TouchScreenKeyboard_get_wasCanceled_m2840762566 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m2840762566_ftn) (TouchScreenKeyboard_t1683994280 *);
	static TouchScreenKeyboard_get_wasCanceled_m2840762566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m2840762566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
extern "C"  bool TouchScreenKeyboard_get_canGetSelection_m1963061075 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_canGetSelection_m1963061075_ftn) (TouchScreenKeyboard_t1683994280 *);
	static TouchScreenKeyboard_get_canGetSelection_m1963061075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_canGetSelection_m1963061075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_canGetSelection()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.RangeInt UnityEngine.TouchScreenKeyboard::get_selection()
extern "C"  RangeInt_t21894369  TouchScreenKeyboard_get_selection_m3559330131 (TouchScreenKeyboard_t1683994280 * __this, const RuntimeMethod* method)
{
	RangeInt_t21894369  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RangeInt_t21894369  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t* L_0 = (&V_0)->get_address_of_start_0();
		int32_t* L_1 = (&V_0)->get_address_of_length_1();
		TouchScreenKeyboard_GetSelectionInternal_m1674507664(__this, L_0, L_1, /*hidden argument*/NULL);
		RangeInt_t21894369  L_2 = V_0;
		V_1 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		RangeInt_t21894369  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m1674507664 (TouchScreenKeyboard_t1683994280 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_GetSelectionInternal_m1674507664_ftn) (TouchScreenKeyboard_t1683994280 *, int32_t*, int32_t*);
	static TouchScreenKeyboard_GetSelectionInternal_m1674507664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_GetSelectionInternal_m1674507664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)");
	_il2cpp_icall_func(__this, ___start0, ___length1);
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2612049617_marshal_pinvoke(const TrackedReference_t2612049617& unmarshaled, TrackedReference_t2612049617_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void TrackedReference_t2612049617_marshal_pinvoke_back(const TrackedReference_t2612049617_marshaled_pinvoke& marshaled, TrackedReference_t2612049617& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2612049617_marshal_pinvoke_cleanup(TrackedReference_t2612049617_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2612049617_marshal_com(const TrackedReference_t2612049617& unmarshaled, TrackedReference_t2612049617_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.get_m_Ptr_0();
}
extern "C" void TrackedReference_t2612049617_marshal_com_back(const TrackedReference_t2612049617_marshaled_com& marshaled, TrackedReference_t2612049617& unmarshaled)
{
	intptr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	unmarshaled_m_Ptr_temp_0 = marshaled.___m_Ptr_0;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t2612049617_marshal_com_cleanup(TrackedReference_t2612049617_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m357163524 (RuntimeObject * __this /* static, unused */, TrackedReference_t2612049617 * ___x0, TrackedReference_t2612049617 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m357163524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	{
		TrackedReference_t2612049617 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t2612049617 * L_1 = ___y1;
		V_1 = L_1;
		RuntimeObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0067;
	}

IL_0018:
	{
		RuntimeObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		TrackedReference_t2612049617 * L_5 = ___x0;
		NullCheck(L_5);
		intptr_t L_6 = L_5->get_m_Ptr_0();
		bool L_7 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_6, (intptr_t)(0), /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0067;
	}

IL_0034:
	{
		RuntimeObject * L_8 = V_0;
		if (L_8)
		{
			goto IL_0050;
		}
	}
	{
		TrackedReference_t2612049617 * L_9 = ___y1;
		NullCheck(L_9);
		intptr_t L_10 = L_9->get_m_Ptr_0();
		bool L_11 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_10, (intptr_t)(0), /*hidden argument*/NULL);
		V_2 = L_11;
		goto IL_0067;
	}

IL_0050:
	{
		TrackedReference_t2612049617 * L_12 = ___x0;
		NullCheck(L_12);
		intptr_t L_13 = L_12->get_m_Ptr_0();
		TrackedReference_t2612049617 * L_14 = ___y1;
		NullCheck(L_14);
		intptr_t L_15 = L_14->get_m_Ptr_0();
		bool L_16 = IntPtr_op_Equality_m3432723813(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		goto IL_0067;
	}

IL_0067:
	{
		bool L_17 = V_2;
		return L_17;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern "C"  bool TrackedReference_Equals_m1589786927 (TrackedReference_t2612049617 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m1589786927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m357163524(NULL /*static, unused*/, ((TrackedReference_t2612049617 *)IsInstClass((RuntimeObject*)L_0, TrackedReference_t2612049617_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m2630422246 (TrackedReference_t2612049617 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		intptr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m4119414380(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Transform::.ctor()
extern "C"  void Transform__ctor_m2159262194 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m3044674138(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3932393085  Transform_get_position_m2750929393 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_position_m3677915992(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m1998703727 (Transform_t3580444445 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_position_m1521839669(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m3677915992 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_position_m3677915992_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_get_position_m3677915992_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m3677915992_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m1521839669 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_position_m1521839669_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_set_position_m1521839669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m1521839669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3932393085  Transform_get_localPosition_m3155957202 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localPosition_m103217147(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m176382003 (Transform_t3580444445 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localPosition_m793065216(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m103217147 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m103217147_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_get_localPosition_m103217147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m103217147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m793065216 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m793065216_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_set_localPosition_m793065216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m793065216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t3932393085  Transform_get_forward_m2380180709 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_forward_m2380180709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t3497065016  L_0 = Transform_get_rotation_m444413307(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_1 = Vector3_get_forward_m4214875591(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_2 = Quaternion_op_Multiply_m675595747(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t3932393085  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t3497065016  Transform_get_rotation_m444413307 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t3497065016  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_rotation_m1170447236(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t3497065016  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t3497065016  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3054645015 (Transform_t3580444445 * __this, Quaternion_t3497065016  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_rotation_m2992445136(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m1170447236 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m1170447236_ftn) (Transform_t3580444445 *, Quaternion_t3497065016 *);
	static Transform_INTERNAL_get_rotation_m1170447236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m1170447236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m2992445136 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m2992445136_ftn) (Transform_t3580444445 *, Quaternion_t3497065016 *);
	static Transform_INTERNAL_set_rotation_m2992445136_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m2992445136_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t3497065016  Transform_get_localRotation_m3142450957 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Quaternion_t3497065016  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t3497065016  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localRotation_m1998419987(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t3497065016  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t3497065016  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m401507485 (Transform_t3580444445 * __this, Quaternion_t3497065016  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localRotation_m1657065507(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m1998419987 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m1998419987_ftn) (Transform_t3580444445 *, Quaternion_t3497065016 *);
	static Transform_INTERNAL_get_localRotation_m1998419987_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m1998419987_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m1657065507 (Transform_t3580444445 * __this, Quaternion_t3497065016 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m1657065507_ftn) (Transform_t3580444445 *, Quaternion_t3497065016 *);
	static Transform_INTERNAL_set_localRotation_m1657065507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m1657065507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t3932393085  Transform_get_localScale_m2665032488 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localScale_m820755046(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2330505053 (Transform_t3580444445 * __this, Vector3_t3932393085  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localScale_m4270159898(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m820755046 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m820755046_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_get_localScale_m820755046_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m820755046_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m4270159898 (Transform_t3580444445 * __this, Vector3_t3932393085 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m4270159898_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_set_localScale_m4270159898_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m4270159898_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3580444445 * Transform_get_parent_m82173183 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Transform_t3580444445 * V_0 = NULL;
	{
		Transform_t3580444445 * L_0 = Transform_get_parentInternal_m584767176(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Transform_t3580444445 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t3580444445 * Transform_get_parentInternal_m584767176 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	typedef Transform_t3580444445 * (*Transform_get_parentInternal_m584767176_ftn) (Transform_t3580444445 *);
	static Transform_get_parentInternal_m584767176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m584767176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	Transform_t3580444445 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m892470385 (Transform_t3580444445 * __this, Transform_t3580444445 * ___parent0, const RuntimeMethod* method)
{
	{
		Transform_t3580444445 * L_0 = ___parent0;
		Transform_SetParent_m764251424(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m764251424 (Transform_t3580444445 * __this, Transform_t3580444445 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method)
{
	typedef void (*Transform_SetParent_m764251424_ftn) (Transform_t3580444445 *, Transform_t3580444445 *, bool);
	static Transform_SetParent_m764251424_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m764251424_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent0, ___worldPositionStays1);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t787966842  Transform_get_worldToLocalMatrix_m3343408588 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t787966842  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t787966842  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m3849428052(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t787966842  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t787966842  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m3849428052 (Transform_t3580444445 * __this, Matrix4x4_t787966842 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m3849428052_ftn) (Transform_t3580444445 *, Matrix4x4_t787966842 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m3849428052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m3849428052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Transform_TransformPoint_m1797663460 (Transform_t3580444445 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformPoint_m4199768081(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m4199768081 (RuntimeObject * __this /* static, unused */, Transform_t3580444445 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m4199768081_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_CALL_TransformPoint_m4199768081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m4199768081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Transform_InverseTransformPoint_m3824069262 (Transform_t3580444445 * __this, Vector3_t3932393085  ___position0, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m1890131684(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3932393085  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3932393085  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m1890131684 (RuntimeObject * __this /* static, unused */, Transform_t3580444445 * ___self0, Vector3_t3932393085 * ___position1, Vector3_t3932393085 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m1890131684_ftn) (Transform_t3580444445 *, Vector3_t3932393085 *, Vector3_t3932393085 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m1890131684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m1890131684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2090497278 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Transform_get_childCount_m2090497278_ftn) (Transform_t3580444445 *);
	static Transform_get_childCount_m2090497278_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m2090497278_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m432771876 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	typedef void (*Transform_SetAsFirstSibling_m432771876_ftn) (Transform_t3580444445 *);
	static Transform_SetAsFirstSibling_m432771876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m432771876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m4122222148 (Transform_t3580444445 * __this, Transform_t3580444445 * ___parent0, const RuntimeMethod* method)
{
	typedef bool (*Transform_IsChildOf_m4122222148_ftn) (Transform_t3580444445 *, Transform_t3580444445 *);
	static Transform_IsChildOf_m4122222148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m4122222148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	bool retVal = _il2cpp_icall_func(__this, ___parent0);
	return retVal;
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  RuntimeObject* Transform_GetEnumerator_m2743450628 (Transform_t3580444445 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m2743450628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Enumerator_t3595687596 * L_0 = (Enumerator_t3595687596 *)il2cpp_codegen_object_new(Enumerator_t3595687596_il2cpp_TypeInfo_var);
		Enumerator__ctor_m3645558831(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3580444445 * Transform_GetChild_m1886946583 (Transform_t3580444445 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef Transform_t3580444445 * (*Transform_GetChild_m1886946583_ftn) (Transform_t3580444445 *, int32_t);
	static Transform_GetChild_m1886946583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m1886946583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	Transform_t3580444445 * retVal = _il2cpp_icall_func(__this, ___index0);
	return retVal;
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m3645558831 (Enumerator_t3595687596 * __this, Transform_t3580444445 * ___outer0, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m2939335827(__this, /*hidden argument*/NULL);
		Transform_t3580444445 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m1130429462 (Enumerator_t3595687596 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Transform_t3580444445 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t3580444445 * L_2 = Transform_GetChild_m1886946583(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2059373658 (Enumerator_t3595687596 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		Transform_t3580444445 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m2090497278(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		V_2 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		goto IL_0027;
	}

IL_0027:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m1894957514 (Enumerator_t3595687596 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern "C"  bool SpriteAtlasManager_RequestAtlas_m333581262 (RuntimeObject * __this /* static, unused */, String_t* ___tag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager_RequestAtlas_m333581262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B3_0 = NULL;
	RequestAtlasCallback_t3823435737 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	RequestAtlasCallback_t3823435737 * G_B2_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t3823435737 * L_0 = ((SpriteAtlasManager_t4232368070_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t3823435737 * L_1 = ((SpriteAtlasManager_t4232368070_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		String_t* L_2 = ___tag0;
		Action_1_t260058957 * L_3 = ((SpriteAtlasManager_t4232368070_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if (L_3)
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_002a;
		}
	}
	{
		intptr_t L_4 = (intptr_t)SpriteAtlasManager_Register_m793148487_RuntimeMethod_var;
		Action_1_t260058957 * L_5 = (Action_1_t260058957 *)il2cpp_codegen_object_new(Action_1_t260058957_il2cpp_TypeInfo_var);
		Action_1__ctor_m3744639618(L_5, NULL, L_4, /*hidden argument*/Action_1__ctor_m3744639618_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var);
		((SpriteAtlasManager_t4232368070_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_1(L_5);
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var);
		Action_1_t260058957 * L_6 = ((SpriteAtlasManager_t4232368070_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		NullCheck(G_B3_1);
		RequestAtlasCallback_Invoke_m1447585754(G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0042;
	}

IL_003b:
	{
		V_0 = (bool)0;
		goto IL_0042;
	}

IL_0042:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern "C"  void SpriteAtlasManager_Register_m793148487 (RuntimeObject * __this /* static, unused */, SpriteAtlas_t2782010525 * ___spriteAtlas0, const RuntimeMethod* method)
{
	typedef void (*SpriteAtlasManager_Register_m793148487_ftn) (SpriteAtlas_t2782010525 *);
	static SpriteAtlasManager_Register_m793148487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpriteAtlasManager_Register_m793148487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)");
	_il2cpp_icall_func(___spriteAtlas0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern "C"  void SpriteAtlasManager__cctor_m1994284186 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager__cctor_m1994284186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SpriteAtlasManager_t4232368070_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t4232368070_il2cpp_TypeInfo_var))->set_atlasRequested_0((RequestAtlasCallback_t3823435737 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_RequestAtlasCallback_t3823435737 (RequestAtlasCallback_t3823435737 * __this, String_t* ___tag0, Action_1_t260058957 * ___action1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, Il2CppMethodPointer);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___tag0' to native representation
	char* ____tag0_marshaled = NULL;
	____tag0_marshaled = il2cpp_codegen_marshal_string(___tag0);

	// Marshaling of parameter '___action1' to native representation
	Il2CppMethodPointer ____action1_marshaled = NULL;
	____action1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___action1));

	// Native function invocation
	il2cppPInvokeFunc(____tag0_marshaled, ____action1_marshaled);

	// Marshaling cleanup of parameter '___tag0' native representation
	il2cpp_codegen_marshal_free(____tag0_marshaled);
	____tag0_marshaled = NULL;

}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestAtlasCallback__ctor_m2370365278 (RequestAtlasCallback_t3823435737 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m1447585754 (RequestAtlasCallback_t3823435737 * __this, String_t* ___tag0, Action_1_t260058957 * ___action1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestAtlasCallback_Invoke_m1447585754((RequestAtlasCallback_t3823435737 *)__this->get_prev_9(),___tag0, ___action1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___tag0, Action_1_t260058957 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___tag0, Action_1_t260058957 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Action_1_t260058957 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::BeginInvoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* RequestAtlasCallback_BeginInvoke_m2212992393 (RequestAtlasCallback_t3823435737 * __this, String_t* ___tag0, Action_1_t260058957 * ___action1, AsyncCallback_t4283869127 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___tag0;
	__d_args[1] = ___action1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RequestAtlasCallback_EndInvoke_m1157672333 (RequestAtlasCallback_t3823435737 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m2324019343 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m2324019343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppDomain_t2391992810 * G_B2_0 = NULL;
	AppDomain_t2391992810 * G_B1_0 = NULL;
	{
		AppDomain_t2391992810 * L_0 = AppDomain_get_CurrentDomain_m529402762(NULL /*static, unused*/, /*hidden argument*/NULL);
		UnhandledExceptionEventHandler_t1143045648 * L_1 = ((UnhandledExceptionHandler_t4246526107_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t4246526107_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		intptr_t L_2 = (intptr_t)UnhandledExceptionHandler_HandleUnhandledException_m3174347279_RuntimeMethod_var;
		UnhandledExceptionEventHandler_t1143045648 * L_3 = (UnhandledExceptionEventHandler_t1143045648 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t1143045648_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m3215020204(L_3, NULL, L_2, /*hidden argument*/NULL);
		((UnhandledExceptionHandler_t4246526107_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t4246526107_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_0(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		UnhandledExceptionEventHandler_t1143045648 * L_4 = ((UnhandledExceptionHandler_t4246526107_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t4246526107_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		NullCheck(G_B2_0);
		AppDomain_add_UnhandledException_m1469772753(G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m3174347279 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___sender0, UnhandledExceptionEventArgs_t2712850390 * ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m3174347279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t214279536 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t2712850390 * L_0 = ___args1;
		NullCheck(L_0);
		RuntimeObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m3876870412(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t214279536 *)IsInstClass((RuntimeObject*)L_1, Exception_t214279536_il2cpp_TypeInfo_var));
		Exception_t214279536 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		Exception_t214279536 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m1032894626(NULL /*static, unused*/, _stringLiteral1859338151, L_3, /*hidden argument*/NULL);
		Exception_t214279536 * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = Exception_GetType_m1907036829(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		Exception_t214279536 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_7);
		Exception_t214279536 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_9);
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		goto IL_004b;
	}

IL_0041:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272(NULL /*static, unused*/, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m1032894626 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t214279536 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m1032894626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t214279536 * L_0 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t959145111_il2cpp_TypeInfo_var);
		Debug_LogException_m1375071567(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Exception_t214279536 * L_1 = ___e1;
		NullCheck(L_1);
		Exception_t214279536 * L_2 = Exception_get_InnerException_m4271498349(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Exception_t214279536 * L_3 = ___e1;
		NullCheck(L_3);
		Exception_t214279536 * L_4 = Exception_get_InnerException_m4271498349(L_3, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m1032894626(NULL /*static, unused*/, _stringLiteral1142008342, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272_ftn) (String_t*, String_t*, String_t*);
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m3853615272_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)");
	_il2cpp_icall_func(___managedExceptionType0, ___managedExceptionMessage1, ___managedExceptionStack2);
}
// System.Void UnityEngine.UnityException::.ctor()
extern "C"  void UnityException__ctor_m616494757 (UnityException_t2602360046 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m616494757_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception__ctor_m73287453(__this, _stringLiteral496184237, /*hidden argument*/NULL);
		Exception_set_HResult_m3235753660(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void UnityException__ctor_m2606737738 (UnityException_t2602360046 * __this, SerializationInfo_t1812684756 * ___info0, StreamingContext_t1986025897  ___context1, const RuntimeMethod* method)
{
	{
		SerializationInfo_t1812684756 * L_0 = ___info0;
		StreamingContext_t1986025897  L_1 = ___context1;
		Exception__ctor_m3789217139(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::.ctor()
extern "C"  void UnityLogWriter__ctor_m1903404459 (UnityLogWriter_t3161904322 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter__ctor_m1903404459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextWriter_t4111495313_il2cpp_TypeInfo_var);
		TextWriter__ctor_m2762039731(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern "C"  void UnityLogWriter_WriteStringToUnityLog_m2967587076 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	typedef void (*UnityLogWriter_WriteStringToUnityLog_m2967587076_ftn) (String_t*);
	static UnityLogWriter_WriteStringToUnityLog_m2967587076_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityLogWriter_WriteStringToUnityLog_m2967587076_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)");
	_il2cpp_icall_func(___s0);
}
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m1685913522 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter_Init_m1685913522_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityLogWriter_t3161904322 * L_0 = (UnityLogWriter_t3161904322 *)il2cpp_codegen_object_new(UnityLogWriter_t3161904322_il2cpp_TypeInfo_var);
		UnityLogWriter__ctor_m1903404459(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t2912832404_il2cpp_TypeInfo_var);
		Console_SetOut_m2860715686(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::Write(System.Char)
extern "C"  void UnityLogWriter_Write_m2206327453 (UnityLogWriter_t3161904322 * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Char_ToString_m19900144((&___value0), /*hidden argument*/NULL);
		UnityLogWriter_WriteStringToUnityLog_m2967587076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::Write(System.String)
extern "C"  void UnityLogWriter_Write_m1169693687 (UnityLogWriter_t3161904322 * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		UnityLogWriter_WriteStringToUnityLog_m2967587076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m1784030070 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3385344125* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityString_Format_m1784030070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t3385344125* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m3139124310(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::.ctor()
extern "C"  void UnitySynchronizationContext__ctor_m842443118 (UnitySynchronizationContext_t1450698442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__ctor_m842443118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Queue_1_t2402182407 * L_0 = (Queue_1_t2402182407 *)il2cpp_codegen_object_new(Queue_1_t2402182407_il2cpp_TypeInfo_var);
		Queue_1__ctor_m1321454259(L_0, ((int32_t)20), /*hidden argument*/Queue_1__ctor_m1321454259_RuntimeMethod_var);
		__this->set_m_AsyncWorkQueue_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2508967565_il2cpp_TypeInfo_var);
		Thread_t2508967565 * L_1 = Thread_get_CurrentThread_m2158879202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1330586752(L_1, /*hidden argument*/NULL);
		__this->set_m_MainThreadID_2(L_2);
		SynchronizationContext__ctor_m1659093484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern "C"  void UnitySynchronizationContext_Exec_m2847479435 (UnitySynchronizationContext_t1450698442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Exec_m2847479435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	WorkRequest_t266095197  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t214279536 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t214279536 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2402182407 * L_0 = __this->get_m_AsyncWorkQueue_1();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m4131414619(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0014:
		{
			Queue_1_t2402182407 * L_2 = __this->get_m_AsyncWorkQueue_1();
			NullCheck(L_2);
			WorkRequest_t266095197  L_3 = Queue_1_Dequeue_m1134160965(L_2, /*hidden argument*/Queue_1_Dequeue_m1134160965_RuntimeMethod_var);
			V_1 = L_3;
			WorkRequest_Invoke_m139019247((&V_1), /*hidden argument*/NULL);
		}

IL_0029:
		{
			Queue_1_t2402182407 * L_4 = __this->get_m_AsyncWorkQueue_1();
			NullCheck(L_4);
			int32_t L_5 = Queue_1_get_Count_m2804029644(L_4, /*hidden argument*/Queue_1_get_Count_m2804029644_RuntimeMethod_var);
			if ((((int32_t)L_5) > ((int32_t)0)))
			{
				goto IL_0014;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t214279536 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		RuntimeObject * L_6 = V_0;
		Monitor_Exit_m1585917014(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t214279536 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::InitializeSynchronizationContext()
extern "C"  void UnitySynchronizationContext_InitializeSynchronizationContext_m2010976595 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_InitializeSynchronizationContext_m2010976595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SynchronizationContext_t3011612328 * L_0 = SynchronizationContext_get_Current_m1340098188(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		UnitySynchronizationContext_t1450698442 * L_1 = (UnitySynchronizationContext_t1450698442 *)il2cpp_codegen_object_new(UnitySynchronizationContext_t1450698442_il2cpp_TypeInfo_var);
		UnitySynchronizationContext__ctor_m842443118(L_1, /*hidden argument*/NULL);
		SynchronizationContext_SetSynchronizationContext_m1476995085(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::ExecuteTasks()
extern "C"  void UnitySynchronizationContext_ExecuteTasks_m1057183886 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_ExecuteTasks_m1057183886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnitySynchronizationContext_t1450698442 * V_0 = NULL;
	{
		SynchronizationContext_t3011612328 * L_0 = SynchronizationContext_get_Current_m1340098188(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((UnitySynchronizationContext_t1450698442 *)IsInstSealed((RuntimeObject*)L_0, UnitySynchronizationContext_t1450698442_il2cpp_TypeInfo_var));
		UnitySynchronizationContext_t1450698442 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UnitySynchronizationContext_t1450698442 * L_2 = V_0;
		NullCheck(L_2);
		UnitySynchronizationContext_Exec_m2847479435(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t266095197_marshal_pinvoke(const WorkRequest_t266095197& unmarshaled, WorkRequest_t266095197_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
extern "C" void WorkRequest_t266095197_marshal_pinvoke_back(const WorkRequest_t266095197_marshaled_pinvoke& marshaled, WorkRequest_t266095197& unmarshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t266095197_marshal_pinvoke_cleanup(WorkRequest_t266095197_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t266095197_marshal_com(const WorkRequest_t266095197& unmarshaled, WorkRequest_t266095197_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
extern "C" void WorkRequest_t266095197_marshal_com_back(const WorkRequest_t266095197_marshaled_com& marshaled, WorkRequest_t266095197& unmarshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t266095197_marshal_com_cleanup(WorkRequest_t266095197_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern "C"  void WorkRequest_Invoke_m139019247 (WorkRequest_t266095197 * __this, const RuntimeMethod* method)
{
	{
		SendOrPostCallback_t1528611714 * L_0 = __this->get_m_DelagateCallback_0();
		RuntimeObject * L_1 = __this->get_m_DelagateState_1();
		NullCheck(L_0);
		SendOrPostCallback_Invoke_m3562526081(L_0, L_1, /*hidden argument*/NULL);
		ManualResetEvent_t2620507134 * L_2 = __this->get_m_WaitHandle_2();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		ManualResetEvent_t2620507134 * L_3 = __this->get_m_WaitHandle_2();
		NullCheck(L_3);
		EventWaitHandle_Set_m3353019311(L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void WorkRequest_Invoke_m139019247_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	WorkRequest_t266095197 * _thisAdjusted = reinterpret_cast<WorkRequest_t266095197 *>(__this + 1);
	WorkRequest_Invoke_m139019247(_thisAdjusted, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3768944458 (Vector2_t2246369278 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m3768944458_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	Vector2__ctor_m3768944458(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m1427364022 (Vector2_t2246369278 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_Item_m1427364022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		float L_2 = __this->get_x_0();
		V_0 = L_2;
		goto IL_0036;
	}

IL_001f:
	{
		float L_3 = __this->get_y_1();
		V_0 = L_3;
		goto IL_0036;
	}

IL_002b:
	{
		IndexOutOfRangeException_t3659678378 * L_4 = (IndexOutOfRangeException_t3659678378 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m4093382970(L_4, _stringLiteral1705123583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0036:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector2_get_Item_m1427364022_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	return Vector2_get_Item_m1427364022(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1623790336 (Vector2_t2246369278 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_set_Item_m1623790336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		float L_2 = ___value1;
		__this->set_x_0(L_2);
		goto IL_0036;
	}

IL_001f:
	{
		float L_3 = ___value1;
		__this->set_y_1(L_3);
		goto IL_0036;
	}

IL_002b:
	{
		IndexOutOfRangeException_t3659678378 * L_4 = (IndexOutOfRangeException_t3659678378 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3659678378_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m4093382970(L_4, _stringLiteral1705123583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0036:
	{
		return;
	}
}
extern "C"  void Vector2_set_Item_m1623790336_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	Vector2_set_Item_m1623790336(_thisAdjusted, ___index0, ___value1, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2246369278  Vector2_Lerp_m2858900924 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, Vector2_t2246369278  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Lerp_m2858900924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t9189715_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2797960148(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_0();
		float L_3 = (&___b1)->get_x_0();
		float L_4 = (&___a0)->get_x_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_1();
		float L_7 = (&___b1)->get_y_1();
		float L_8 = (&___a0)->get_y_1();
		float L_9 = ___t2;
		Vector2_t2246369278  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3768944458((&L_10), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_0046;
	}

IL_0046:
	{
		Vector2_t2246369278  L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2246369278  Vector2_Scale_m2692866854 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, Vector2_t2246369278  ___b1, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t2246369278  L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m3641689616 (Vector2_t2246369278 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m3641689616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3385344125* L_0 = ((ObjectU5BU5D_t3385344125*)SZArrayNew(ObjectU5BU5D_t3385344125_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3385344125* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t897798503_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m1784030070(NULL /*static, unused*/, _stringLiteral3424384439, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Vector2_ToString_m3641689616_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	return Vector2_ToString_m3641689616(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m2227745196 (Vector2_t2246369278 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3630296696(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3630296696(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_002c;
	}

IL_002c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t Vector2_GetHashCode_m2227745196_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	return Vector2_GetHashCode_m2227745196(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m258839576 (Vector2_t2246369278 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m258839576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B5_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector2_t2246369278_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_004c;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector2_t2246369278 *)((Vector2_t2246369278 *)UnBox(L_1, Vector2_t2246369278_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m847285714(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m847285714(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0046;
	}

IL_0045:
	{
		G_B5_0 = 0;
	}

IL_0046:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004c;
	}

IL_004c:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Vector2_Equals_m258839576_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	return Vector2_Equals_m258839576(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m2911690954 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___lhs0, Vector2_t2246369278  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___lhs0)->get_x_0();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_y_1();
		float L_3 = (&___rhs1)->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0026;
	}

IL_0026:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m423695516 (Vector2_t2246369278 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0022;
	}

IL_0022:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float Vector2_get_sqrMagnitude_m423695516_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t2246369278 * _thisAdjusted = reinterpret_cast<Vector2_t2246369278 *>(__this + 1);
	return Vector2_get_sqrMagnitude_m423695516(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2246369278  Vector2_op_Addition_m4252524769 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, Vector2_t2246369278  ___b1, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t2246369278  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2246369278  Vector2_op_Subtraction_m3277110115 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, Vector2_t2246369278  ___b1, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t2246369278  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2246369278  Vector2_op_Multiply_m3268495446 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_1();
		float L_3 = ___d1;
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector2_t2246369278  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2246369278  Vector2_op_Division_m1158683211 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_1();
		float L_3 = ___d1;
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector2_t2246369278  L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m512848965 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___lhs0, Vector2_t2246369278  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_op_Equality_m512848965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t2246369278  L_0 = ___lhs0;
		Vector2_t2246369278  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_2 = Vector2_op_Subtraction_m3277110115(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_sqrMagnitude_m423695516((&V_0), /*hidden argument*/NULL);
		V_1 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Inequality_m190722814 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___lhs0, Vector2_t2246369278  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_op_Inequality_m190722814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector2_t2246369278  L_0 = ___lhs0;
		Vector2_t2246369278  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		bool L_2 = Vector2_op_Equality_m512848965(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2246369278  Vector2_op_Implicit_m497246090 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  ___v0, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t2246369278  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3932393085  Vector2_op_Implicit_m2591794359 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___v0, const RuntimeMethod* method)
{
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_0();
		float L_1 = (&___v0)->get_y_1();
		Vector3_t3932393085  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2321752175((&L_2), L_0, L_1, (0.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001f;
	}

IL_001f:
	{
		Vector3_t3932393085  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2246369278  Vector2_get_zero_m1981541827 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_zero_m1981541827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_0 = ((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->get_zeroVector_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t2246369278  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2246369278  Vector2_get_one_m1442422668 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_one_m1442422668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_0 = ((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->get_oneVector_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t2246369278  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t2246369278  Vector2_get_up_m4207689530 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_up_m4207689530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_0 = ((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->get_upVector_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t2246369278  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Vector2::.cctor()
extern "C"  void Vector2__cctor_m1153611810 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2__cctor_m1153611810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t2246369278  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3768944458((&L_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_zeroVector_2(L_0);
		Vector2_t2246369278  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m3768944458((&L_1), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_oneVector_3(L_1);
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_upVector_4(L_2);
		Vector2_t2246369278  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3768944458((&L_3), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_downVector_5(L_3);
		Vector2_t2246369278  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3768944458((&L_4), (-1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_leftVector_6(L_4);
		Vector2_t2246369278  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3768944458((&L_5), (1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_rightVector_7(L_5);
		Vector2_t2246369278  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3768944458((&L_6), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_positiveInfinityVector_8(L_6);
		Vector2_t2246369278  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m3768944458((&L_7), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector2_t2246369278_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t2246369278_il2cpp_TypeInfo_var))->set_negativeInfinityVector_9(L_7);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
