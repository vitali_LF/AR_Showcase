﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Canvas
struct Canvas_t2667390772;
// UnityEngine.Behaviour
struct Behaviour_t3297694025;
// UnityEngine.Camera
struct Camera_t362346687;
// UnityEngine.Material
struct Material_t1926439680;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t1608056246;
// System.Delegate
struct Delegate_t4015201940;
// System.IAsyncResult
struct IAsyncResult_t4224419158;
// System.AsyncCallback
struct AsyncCallback_t4283869127;
// UnityEngine.CanvasGroup
struct CanvasGroup_t2669816671;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t3854993866;
// UnityEngine.Texture
struct Texture_t954505614;
// UnityEngine.Mesh
struct Mesh_t4237566844;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t323345135;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1311628880;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t2336627130;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3920572369;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t3575182278;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t894813333;
// UnityEngine.RectTransform
struct RectTransform_t1161610249;
// UnityEngine.Transform
struct Transform_t3580444445;
// UnityEngine.Object
struct Object_t3645472222;
// System.String
struct String_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t907000053;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1432878832;
// System.Char[]
struct CharU5BU5D_t2953840665;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3868613195;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1632896738;
// System.Int32[]
struct Int32U5BU5D_t3215173111;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2352998046;
// System.Void
struct Void_t4071739332;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t2468626509;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1147600571;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t3349755816;

extern RuntimeClass* Canvas_t2667390772_il2cpp_TypeInfo_var;
extern RuntimeClass* WillRenderCanvases_t1608056246_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_add_willRenderCanvases_m1144156398_MetadataUsageId;
extern const uint32_t Canvas_remove_willRenderCanvases_m4294221135_MetadataUsageId;
extern const uint32_t Canvas_SendWillRenderCanvases_m3559155133_MetadataUsageId;
extern RuntimeClass* RectTransformUtility_t2671632890_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1348189536_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m4183794563_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m572670876_MetadataUsageId;
extern RuntimeClass* Vector2_t2246369278_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3932393085_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t3497065016_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m1953132754_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m4197255120_MetadataUsageId;
extern RuntimeClass* Object_t3645472222_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m3183574266_MetadataUsageId;
extern RuntimeClass* RectTransform_t1161610249_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3943889344_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m3715915365_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1432878832_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m2248621842_MetadataUsageId;

struct Vector3U5BU5D_t1432878832;


#ifndef U3CMODULEU3E_T1227682091_H
#define U3CMODULEU3E_T1227682091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1227682091 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1227682091_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef UISYSTEMPROFILERAPI_T2668800988_H
#define UISYSTEMPROFILERAPI_T2668800988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_t2668800988  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_T2668800988_H
#ifndef LIST_1_T323345135_H
#define LIST_1_T323345135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t323345135  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_t907000053* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t323345135, ____items_1)); }
	inline UIVertexU5BU5D_t907000053* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_t907000053** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_t907000053* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t323345135, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t323345135, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t323345135_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UIVertexU5BU5D_t907000053* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t323345135_StaticFields, ___EmptyArray_4)); }
	inline UIVertexU5BU5D_t907000053* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UIVertexU5BU5D_t907000053** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UIVertexU5BU5D_t907000053* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T323345135_H
#ifndef RECTTRANSFORMUTILITY_T2671632890_H
#define RECTTRANSFORMUTILITY_T2671632890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t2671632890  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t2671632890_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t1432878832* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t2671632890_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_t1432878832* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_t1432878832** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_t1432878832* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T2671632890_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t2953840665* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t2953840665* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t2953840665** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t2953840665* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T1311628880_H
#define LIST_1_T1311628880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t1311628880  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1432878832* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1311628880, ____items_1)); }
	inline Vector3U5BU5D_t1432878832* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1432878832** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1432878832* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1311628880, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1311628880, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1311628880_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t1432878832* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1311628880_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t1432878832* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t1432878832** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t1432878832* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1311628880_H
#ifndef LIST_1_T3920572369_H
#define LIST_1_T3920572369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t3920572369  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_t3868613195* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3920572369, ____items_1)); }
	inline Vector2U5BU5D_t3868613195* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_t3868613195** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_t3868613195* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3920572369, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3920572369, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3920572369_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_t3868613195* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3920572369_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_t3868613195* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_t3868613195** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_t3868613195* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3920572369_H
#ifndef VALUETYPE_T1108148719_H
#define VALUETYPE_T1108148719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1108148719  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1108148719_marshaled_com
{
};
#endif // VALUETYPE_T1108148719_H
#ifndef LIST_1_T3575182278_H
#define LIST_1_T3575182278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_t3575182278  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t1632896738* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3575182278, ____items_1)); }
	inline Vector4U5BU5D_t1632896738* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t1632896738** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t1632896738* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3575182278, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3575182278, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3575182278_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector4U5BU5D_t1632896738* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3575182278_StaticFields, ___EmptyArray_4)); }
	inline Vector4U5BU5D_t1632896738* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector4U5BU5D_t1632896738** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector4U5BU5D_t1632896738* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3575182278_H
#ifndef LIST_1_T894813333_H
#define LIST_1_T894813333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t894813333  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t3215173111* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t894813333, ____items_1)); }
	inline Int32U5BU5D_t3215173111* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t3215173111** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t3215173111* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t894813333, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t894813333, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t894813333_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t3215173111* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t894813333_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t3215173111* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t3215173111** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t3215173111* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T894813333_H
#ifndef LIST_1_T2336627130_H
#define LIST_1_T2336627130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t2336627130  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_t2352998046* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2336627130, ____items_1)); }
	inline Color32U5BU5D_t2352998046* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_t2352998046** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_t2352998046* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2336627130, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2336627130, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2336627130_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Color32U5BU5D_t2352998046* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2336627130_StaticFields, ___EmptyArray_4)); }
	inline Color32U5BU5D_t2352998046* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Color32U5BU5D_t2352998046** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Color32U5BU5D_t2352998046* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2336627130_H
#ifndef ENUM_T2432427458_H
#define ENUM_T2432427458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2432427458  : public ValueType_t1108148719
{
public:

public:
};

struct Enum_t2432427458_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t2953840665* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2432427458_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t2953840665* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t2953840665** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t2953840665* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2432427458_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2432427458_marshaled_com
{
};
#endif // ENUM_T2432427458_H
#ifndef RECT_T952252086_H
#define RECT_T952252086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t952252086 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t952252086, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T952252086_H
#ifndef COLOR_T1361298052_H
#define COLOR_T1361298052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t1361298052 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t1361298052, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T1361298052_H
#ifndef QUATERNION_T3497065016_H
#define QUATERNION_T3497065016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t3497065016 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t3497065016_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t3497065016  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t3497065016_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t3497065016  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t3497065016 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t3497065016  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T3497065016_H
#ifndef VECTOR3_T3932393085_H
#define VECTOR3_T3932393085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3932393085 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3932393085, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3932393085_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3932393085  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3932393085  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3932393085  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3932393085  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3932393085  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3932393085  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3932393085  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3932393085  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3932393085  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3932393085  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3932393085  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3932393085 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3932393085  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___oneVector_5)); }
	inline Vector3_t3932393085  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3932393085 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3932393085  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___upVector_6)); }
	inline Vector3_t3932393085  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3932393085 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3932393085  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___downVector_7)); }
	inline Vector3_t3932393085  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3932393085 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3932393085  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___leftVector_8)); }
	inline Vector3_t3932393085  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3932393085 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3932393085  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___rightVector_9)); }
	inline Vector3_t3932393085  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3932393085 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3932393085  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3932393085  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3932393085 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3932393085  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___backVector_11)); }
	inline Vector3_t3932393085  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3932393085 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3932393085  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3932393085  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3932393085 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3932393085  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3932393085_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3932393085  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3932393085 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3932393085  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3932393085_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T3515577538_H
#define INT32_T3515577538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t3515577538 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t3515577538, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T3515577538_H
#ifndef SINGLE_T897798503_H
#define SINGLE_T897798503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t897798503 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t897798503, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T897798503_H
#ifndef BOOLEAN_T2440219994_H
#define BOOLEAN_T2440219994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2440219994 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2440219994, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2440219994_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2440219994_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2440219994_H
#ifndef VOID_T4071739332_H
#define VOID_T4071739332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t4071739332 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T4071739332_H
#ifndef VECTOR2_T2246369278_H
#define VECTOR2_T2246369278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2246369278 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2246369278, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2246369278_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2246369278  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2246369278  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2246369278  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2246369278  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2246369278  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2246369278  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2246369278  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2246369278  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2246369278  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2246369278 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2246369278  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___oneVector_3)); }
	inline Vector2_t2246369278  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2246369278 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2246369278  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___upVector_4)); }
	inline Vector2_t2246369278  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2246369278 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2246369278  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___downVector_5)); }
	inline Vector2_t2246369278  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2246369278 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2246369278  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___leftVector_6)); }
	inline Vector2_t2246369278  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2246369278 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2246369278  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___rightVector_7)); }
	inline Vector2_t2246369278  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2246369278 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2246369278  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2246369278  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2246369278 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2246369278  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2246369278_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2246369278  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2246369278 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2246369278  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2246369278_H
#ifndef PLANE_T4100015541_H
#define PLANE_T4100015541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t4100015541 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t3932393085  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t4100015541, ___m_Normal_0)); }
	inline Vector3_t3932393085  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t3932393085 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t3932393085  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t4100015541, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T4100015541_H
#ifndef DELEGATE_T4015201940_H
#define DELEGATE_T4015201940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t4015201940  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t2468626509 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t4015201940, ___data_8)); }
	inline DelegateData_t2468626509 * get_data_8() const { return ___data_8; }
	inline DelegateData_t2468626509 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t2468626509 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T4015201940_H
#ifndef SAMPLETYPE_T3378127598_H
#define SAMPLETYPE_T3378127598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi/SampleType
struct  SampleType_t3378127598 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi/SampleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleType_t3378127598, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T3378127598_H
#ifndef RAY_T1807385980_H
#define RAY_T1807385980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t1807385980 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3932393085  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3932393085  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t1807385980, ___m_Origin_0)); }
	inline Vector3_t3932393085  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3932393085 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3932393085  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t1807385980, ___m_Direction_1)); }
	inline Vector3_t3932393085  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3932393085 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3932393085  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T1807385980_H
#ifndef RENDERMODE_T1424844737_H
#define RENDERMODE_T1424844737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t1424844737 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t1424844737, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T1424844737_H
#ifndef OBJECT_T3645472222_H
#define OBJECT_T3645472222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t3645472222  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t3645472222, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t3645472222_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t3645472222_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t3645472222_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t3645472222_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T3645472222_H
#ifndef COMPONENT_T2335505321_H
#define COMPONENT_T2335505321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t2335505321  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T2335505321_H
#ifndef TEXTURE_T954505614_H
#define TEXTURE_T954505614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t954505614  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T954505614_H
#ifndef MESH_T4237566844_H
#define MESH_T4237566844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t4237566844  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T4237566844_H
#ifndef MATERIAL_T1926439680_H
#define MATERIAL_T1926439680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t1926439680  : public Object_t3645472222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T1926439680_H
#ifndef MULTICASTDELEGATE_T599438524_H
#define MULTICASTDELEGATE_T599438524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t599438524  : public Delegate_t4015201940
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t599438524 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t599438524 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t599438524, ___prev_9)); }
	inline MulticastDelegate_t599438524 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t599438524 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t599438524 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t599438524, ___kpm_next_10)); }
	inline MulticastDelegate_t599438524 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t599438524 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t599438524 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T599438524_H
#ifndef CANVASRENDERER_T3854993866_H
#define CANVASRENDERER_T3854993866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_t3854993866  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_T3854993866_H
#ifndef CANVASGROUP_T2669816671_H
#define CANVASGROUP_T2669816671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t2669816671  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T2669816671_H
#ifndef ASYNCCALLBACK_T4283869127_H
#define ASYNCCALLBACK_T4283869127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t4283869127  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T4283869127_H
#ifndef WILLRENDERCANVASES_T1608056246_H
#define WILLRENDERCANVASES_T1608056246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas/WillRenderCanvases
struct  WillRenderCanvases_t1608056246  : public MulticastDelegate_t599438524
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_T1608056246_H
#ifndef BEHAVIOUR_T3297694025_H
#define BEHAVIOUR_T3297694025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t3297694025  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T3297694025_H
#ifndef TRANSFORM_T3580444445_H
#define TRANSFORM_T3580444445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3580444445  : public Component_t2335505321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3580444445_H
#ifndef RECTTRANSFORM_T1161610249_H
#define RECTTRANSFORM_T1161610249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t1161610249  : public Transform_t3580444445
{
public:

public:
};

struct RectTransform_t1161610249_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1147600571 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t1161610249_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1147600571 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1147600571 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1147600571 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T1161610249_H
#ifndef CAMERA_T362346687_H
#define CAMERA_T362346687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t362346687  : public Behaviour_t3297694025
{
public:

public:
};

struct Camera_t362346687_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t3349755816 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t3349755816 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t3349755816 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t3349755816 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t3349755816 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t3349755816 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t3349755816 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t3349755816 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t3349755816 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t362346687_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t3349755816 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t3349755816 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t3349755816 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T362346687_H
#ifndef CANVAS_T2667390772_H
#define CANVAS_T2667390772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t2667390772  : public Behaviour_t3297694025
{
public:

public:
};

struct Canvas_t2667390772_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t1608056246 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t2667390772_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t1608056246 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t1608056246 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t1608056246 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T2667390772_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1432878832  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3932393085  m_Items[1];

public:
	inline Vector3_t3932393085  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3932393085 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3932393085  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3932393085  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3932393085 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3932393085  value)
	{
		m_Items[index] = value;
	}
};



// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m1626382119 (Behaviour_t3297694025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t4015201940 * Delegate_Combine_m4046213137 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * p0, Delegate_t4015201940 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t4015201940 * Delegate_Remove_m897454397 (RuntimeObject * __this /* static, unused */, Delegate_t4015201940 * p0, Delegate_t4015201940 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C"  void WillRenderCanvases_Invoke_m909805475 (WillRenderCanvases_t1608056246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern "C"  void Canvas_SendWillRenderCanvases_m3559155133 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C"  bool CanvasGroup_get_blocksRaycasts_m302460917 (CanvasGroup_t2669816671 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_SetColor_m3614729622 (RuntimeObject * __this /* static, unused */, CanvasRenderer_t3854993866 * ___self0, Color_t1361298052 * ___color1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_GetColor_m1322085719 (RuntimeObject * __this /* static, unused */, CanvasRenderer_t3854993866 * ___self0, Color_t1361298052 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3298077761 (RuntimeObject * __this /* static, unused */, CanvasRenderer_t3854993866 * ___self0, Rect_t952252086 * ___rect1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C"  int32_t CanvasRenderer_get_materialCount_m457838510 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m3399311511 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C"  void CanvasRenderer_set_materialCount_m3662488854 (CanvasRenderer_t3854993866 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetMaterial_m1504826497 (CanvasRenderer_t3854993866 * __this, Material_t1926439680 * ___material0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetTexture_m576023973 (CanvasRenderer_t3854993866 * __this, Texture_t954505614 * ___texture0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitUIVertexStreamsInternal_m1472523664 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___verts0, RuntimeObject * ___positions1, RuntimeObject * ___colors2, RuntimeObject * ___uv0S3, RuntimeObject * ___uv1S4, RuntimeObject * ___uv2S5, RuntimeObject * ___uv3S6, RuntimeObject * ___normals7, RuntimeObject * ___tangents8, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SplitIndicesStreamsInternal(System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitIndicesStreamsInternal_m482009823 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___verts0, RuntimeObject * ___indices1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_CreateUIVertexStreamInternal_m322823956 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___verts0, RuntimeObject * ___positions1, RuntimeObject * ___colors2, RuntimeObject * ___uv0S3, RuntimeObject * ___uv1S4, RuntimeObject * ___uv2S5, RuntimeObject * ___uv3S6, RuntimeObject * ___normals7, RuntimeObject * ___tangents8, RuntimeObject * ___indices9, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m218225114 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, Vector2_t2246369278 * ___screenPoint1, Camera_t362346687 * ___cam2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3956955504 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___point0, Transform_t3580444445 * ___elementTransform1, Canvas_t2667390772 * ___canvas2, Vector2_t2246369278 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m2439407777 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rectTransform0, Canvas_t2667390772 * ___canvas1, Rect_t952252086 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2246369278  Vector2_get_zero_m1981541827 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3932393085  Vector2_op_Implicit_m2591794359 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t1807385980  RectTransformUtility_ScreenPointToRay_m3183574266 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___cam0, Vector2_t2246369278  ___screenPos1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t3497065016  Transform_get_rotation_m444413307 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t3932393085  Vector3_get_back_m1633092186 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Quaternion_op_Multiply_m675595747 (RuntimeObject * __this /* static, unused */, Quaternion_t3497065016  p0, Vector3_t3932393085  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3932393085  Transform_get_position_m2750929393 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m1011206029 (Plane_t4100015541 * __this, Vector3_t3932393085  p0, Vector3_t3932393085  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m894877444 (Plane_t4100015541 * __this, Ray_t1807385980  p0, float* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t3932393085  Ray_GetPoint_m3306741896 (Ray_t1807385980 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m1953132754 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, Vector2_t2246369278  ___screenPoint1, Camera_t362346687 * ___cam2, Vector3_t3932393085 * ___worldPoint3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3932393085  Transform_InverseTransformPoint_m3824069262 (Transform_t3580444445 * __this, Vector3_t3932393085  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2246369278  Vector2_op_Implicit_m497246090 (RuntimeObject * __this /* static, unused */, Vector3_t3932393085  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1808560565 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * p0, Object_t3645472222 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t1807385980  Camera_ScreenPointToRay_m3701064168 (Camera_t362346687 * __this, Vector3_t3932393085  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3932393085  Vector3_get_forward_m4214875591 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2331331332 (Ray_t1807385980 * __this, Vector3_t3932393085  p0, Vector3_t3932393085  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3342981539 (RuntimeObject * __this /* static, unused */, Object_t3645472222 * p0, Object_t3645472222 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3580444445 * Transform_GetChild_m1886946583 (Transform_t3580444445 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3943889344 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m2090497278 (Transform_t3580444445 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2246369278  RectTransform_get_pivot_m57090389 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m1427364022 (Vector2_t2246369278 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1623790336 (Vector2_t2246369278 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m2409069685 (RectTransform_t1161610249 * __this, Vector2_t2246369278  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2246369278  RectTransform_get_anchoredPosition_m3060243242 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2197021314 (RectTransform_t1161610249 * __this, Vector2_t2246369278  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2246369278  RectTransform_get_anchorMin_m3262898663 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2246369278  RectTransform_get_anchorMax_m1509281336 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4291838635 (RectTransform_t1161610249 * __this, Vector2_t2246369278  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m3836270766 (RectTransform_t1161610249 * __this, Vector2_t2246369278  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m3715915365 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2246369278  RectTransformUtility_GetTransposed_m1404395155 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2246369278  RectTransform_get_sizeDelta_m3804390761 (RectTransform_t1161610249 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m826926724 (RectTransform_t1161610249 * __this, Vector2_t2246369278  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3768944458 (Vector2_t2246369278 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Canvas::.ctor()
extern "C"  void Canvas__ctor_m363019631 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m1626382119(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C"  int32_t Canvas_get_renderMode_m2169200733 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Canvas_get_renderMode_m2169200733_ftn) (Canvas_t2667390772 *);
	static Canvas_get_renderMode_m2169200733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m2169200733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C"  bool Canvas_get_isRootCanvas_m2477361563 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m2477361563_ftn) (Canvas_t2667390772 *);
	static Canvas_get_isRootCanvas_m2477361563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m2477361563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C"  Camera_t362346687 * Canvas_get_worldCamera_m956234605 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef Camera_t362346687 * (*Canvas_get_worldCamera_m956234605_ftn) (Canvas_t2667390772 *);
	static Canvas_get_worldCamera_m956234605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m956234605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	Camera_t362346687 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C"  float Canvas_get_scaleFactor_m3758264617 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef float (*Canvas_get_scaleFactor_m3758264617_ftn) (Canvas_t2667390772 *);
	static Canvas_get_scaleFactor_m3758264617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m3758264617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C"  void Canvas_set_scaleFactor_m79495680 (Canvas_t2667390772 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Canvas_set_scaleFactor_m79495680_ftn) (Canvas_t2667390772 *, float);
	static Canvas_set_scaleFactor_m79495680_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m79495680_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C"  float Canvas_get_referencePixelsPerUnit_m2415290087 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m2415290087_ftn) (Canvas_t2667390772 *);
	static Canvas_get_referencePixelsPerUnit_m2415290087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m2415290087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C"  void Canvas_set_referencePixelsPerUnit_m3316239945 (Canvas_t2667390772 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m3316239945_ftn) (Canvas_t2667390772 *, float);
	static Canvas_set_referencePixelsPerUnit_m3316239945_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m3316239945_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C"  bool Canvas_get_pixelPerfect_m3790202323 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m3790202323_ftn) (Canvas_t2667390772 *);
	static Canvas_get_pixelPerfect_m3790202323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m3790202323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C"  int32_t Canvas_get_renderOrder_m629683614 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m629683614_ftn) (Canvas_t2667390772 *);
	static Canvas_get_renderOrder_m629683614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m629683614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Canvas::get_overrideSorting()
extern "C"  bool Canvas_get_overrideSorting_m3618959464 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef bool (*Canvas_get_overrideSorting_m3618959464_ftn) (Canvas_t2667390772 *);
	static Canvas_get_overrideSorting_m3618959464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_overrideSorting_m3618959464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_overrideSorting()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
extern "C"  void Canvas_set_overrideSorting_m3214261333 (Canvas_t2667390772 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Canvas_set_overrideSorting_m3214261333_ftn) (Canvas_t2667390772 *, bool);
	static Canvas_set_overrideSorting_m3214261333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_overrideSorting_m3214261333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_overrideSorting(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C"  int32_t Canvas_get_sortingOrder_m789515004 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m789515004_ftn) (Canvas_t2667390772 *);
	static Canvas_get_sortingOrder_m789515004_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m789515004_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
extern "C"  void Canvas_set_sortingOrder_m1027811129 (Canvas_t2667390772 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Canvas_set_sortingOrder_m1027811129_ftn) (Canvas_t2667390772 *, int32_t);
	static Canvas_set_sortingOrder_m1027811129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingOrder_m1027811129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_targetDisplay()
extern "C"  int32_t Canvas_get_targetDisplay_m203742504 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Canvas_get_targetDisplay_m203742504_ftn) (Canvas_t2667390772 *);
	static Canvas_get_targetDisplay_m203742504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_targetDisplay_m203742504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_targetDisplay()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C"  int32_t Canvas_get_sortingLayerID_m2398499292 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m2398499292_ftn) (Canvas_t2667390772 *);
	static Canvas_get_sortingLayerID_m2398499292_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m2398499292_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
extern "C"  void Canvas_set_sortingLayerID_m1502105631 (Canvas_t2667390772 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Canvas_set_sortingLayerID_m1502105631_ftn) (Canvas_t2667390772 *, int32_t);
	static Canvas_set_sortingLayerID_m1502105631_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingLayerID_m1502105631_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
extern "C"  Canvas_t2667390772 * Canvas_get_rootCanvas_m134997521 (Canvas_t2667390772 * __this, const RuntimeMethod* method)
{
	typedef Canvas_t2667390772 * (*Canvas_get_rootCanvas_m134997521_ftn) (Canvas_t2667390772 *);
	static Canvas_get_rootCanvas_m134997521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_rootCanvas_m134997521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_rootCanvas()");
	Canvas_t2667390772 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C"  Material_t1926439680 * Canvas_GetDefaultCanvasMaterial_m845278071 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Material_t1926439680 * (*Canvas_GetDefaultCanvasMaterial_m845278071_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m845278071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m845278071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	Material_t1926439680 * retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
extern "C"  Material_t1926439680 * Canvas_GetETC1SupportedCanvasMaterial_m2160535804 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Material_t1926439680 * (*Canvas_GetETC1SupportedCanvasMaterial_m2160535804_ftn) ();
	static Canvas_GetETC1SupportedCanvasMaterial_m2160535804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetETC1SupportedCanvasMaterial_m2160535804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()");
	Material_t1926439680 * retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern "C"  void Canvas_add_willRenderCanvases_m1144156398 (RuntimeObject * __this /* static, unused */, WillRenderCanvases_t1608056246 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Canvas_add_willRenderCanvases_m1144156398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WillRenderCanvases_t1608056246 * V_0 = NULL;
	WillRenderCanvases_t1608056246 * V_1 = NULL;
	{
		WillRenderCanvases_t1608056246 * L_0 = ((Canvas_t2667390772_StaticFields*)il2cpp_codegen_static_fields_for(Canvas_t2667390772_il2cpp_TypeInfo_var))->get_willRenderCanvases_2();
		V_0 = L_0;
	}

IL_0006:
	{
		WillRenderCanvases_t1608056246 * L_1 = V_0;
		V_1 = L_1;
		WillRenderCanvases_t1608056246 * L_2 = V_1;
		WillRenderCanvases_t1608056246 * L_3 = ___value0;
		Delegate_t4015201940 * L_4 = Delegate_Combine_m4046213137(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		WillRenderCanvases_t1608056246 * L_5 = V_0;
		WillRenderCanvases_t1608056246 * L_6 = InterlockedCompareExchangeImpl<WillRenderCanvases_t1608056246 *>((((Canvas_t2667390772_StaticFields*)il2cpp_codegen_static_fields_for(Canvas_t2667390772_il2cpp_TypeInfo_var))->get_address_of_willRenderCanvases_2()), ((WillRenderCanvases_t1608056246 *)CastclassSealed((RuntimeObject*)L_4, WillRenderCanvases_t1608056246_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		WillRenderCanvases_t1608056246 * L_7 = V_0;
		WillRenderCanvases_t1608056246 * L_8 = V_1;
		if ((!(((RuntimeObject*)(WillRenderCanvases_t1608056246 *)L_7) == ((RuntimeObject*)(WillRenderCanvases_t1608056246 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern "C"  void Canvas_remove_willRenderCanvases_m4294221135 (RuntimeObject * __this /* static, unused */, WillRenderCanvases_t1608056246 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Canvas_remove_willRenderCanvases_m4294221135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WillRenderCanvases_t1608056246 * V_0 = NULL;
	WillRenderCanvases_t1608056246 * V_1 = NULL;
	{
		WillRenderCanvases_t1608056246 * L_0 = ((Canvas_t2667390772_StaticFields*)il2cpp_codegen_static_fields_for(Canvas_t2667390772_il2cpp_TypeInfo_var))->get_willRenderCanvases_2();
		V_0 = L_0;
	}

IL_0006:
	{
		WillRenderCanvases_t1608056246 * L_1 = V_0;
		V_1 = L_1;
		WillRenderCanvases_t1608056246 * L_2 = V_1;
		WillRenderCanvases_t1608056246 * L_3 = ___value0;
		Delegate_t4015201940 * L_4 = Delegate_Remove_m897454397(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		WillRenderCanvases_t1608056246 * L_5 = V_0;
		WillRenderCanvases_t1608056246 * L_6 = InterlockedCompareExchangeImpl<WillRenderCanvases_t1608056246 *>((((Canvas_t2667390772_StaticFields*)il2cpp_codegen_static_fields_for(Canvas_t2667390772_il2cpp_TypeInfo_var))->get_address_of_willRenderCanvases_2()), ((WillRenderCanvases_t1608056246 *)CastclassSealed((RuntimeObject*)L_4, WillRenderCanvases_t1608056246_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		WillRenderCanvases_t1608056246 * L_7 = V_0;
		WillRenderCanvases_t1608056246 * L_8 = V_1;
		if ((!(((RuntimeObject*)(WillRenderCanvases_t1608056246 *)L_7) == ((RuntimeObject*)(WillRenderCanvases_t1608056246 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern "C"  void Canvas_SendWillRenderCanvases_m3559155133 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Canvas_SendWillRenderCanvases_m3559155133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WillRenderCanvases_t1608056246 * L_0 = ((Canvas_t2667390772_StaticFields*)il2cpp_codegen_static_fields_for(Canvas_t2667390772_il2cpp_TypeInfo_var))->get_willRenderCanvases_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		WillRenderCanvases_t1608056246 * L_1 = ((Canvas_t2667390772_StaticFields*)il2cpp_codegen_static_fields_for(Canvas_t2667390772_il2cpp_TypeInfo_var))->get_willRenderCanvases_2();
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m909805475(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C"  void Canvas_ForceUpdateCanvases_m491111552 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		Canvas_SendWillRenderCanvases_m3559155133(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_WillRenderCanvases_t1608056246 (WillRenderCanvases_t1608056246 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C"  void WillRenderCanvases__ctor_m4117095394 (WillRenderCanvases_t1608056246 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C"  void WillRenderCanvases_Invoke_m909805475 (WillRenderCanvases_t1608056246 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WillRenderCanvases_Invoke_m909805475((WillRenderCanvases_t1608056246 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3()));
	}
}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* WillRenderCanvases_BeginInvoke_m1626179964 (WillRenderCanvases_t1608056246 * __this, AsyncCallback_t4283869127 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C"  void WillRenderCanvases_EndInvoke_m280541295 (WillRenderCanvases_t1608056246 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C"  float CanvasGroup_get_alpha_m4250929446 (CanvasGroup_t2669816671 * __this, const RuntimeMethod* method)
{
	typedef float (*CanvasGroup_get_alpha_m4250929446_ftn) (CanvasGroup_t2669816671 *);
	static CanvasGroup_get_alpha_m4250929446_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_alpha_m4250929446_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_alpha()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C"  void CanvasGroup_set_alpha_m2515933556 (CanvasGroup_t2669816671 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*CanvasGroup_set_alpha_m2515933556_ftn) (CanvasGroup_t2669816671 *, float);
	static CanvasGroup_set_alpha_m2515933556_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m2515933556_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C"  bool CanvasGroup_get_interactable_m2229591687 (CanvasGroup_t2669816671 * __this, const RuntimeMethod* method)
{
	typedef bool (*CanvasGroup_get_interactable_m2229591687_ftn) (CanvasGroup_t2669816671 *);
	static CanvasGroup_get_interactable_m2229591687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m2229591687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C"  bool CanvasGroup_get_blocksRaycasts_m302460917 (CanvasGroup_t2669816671 * __this, const RuntimeMethod* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m302460917_ftn) (CanvasGroup_t2669816671 *);
	static CanvasGroup_get_blocksRaycasts_m302460917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m302460917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C"  bool CanvasGroup_get_ignoreParentGroups_m3625611394 (CanvasGroup_t2669816671 * __this, const RuntimeMethod* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m3625611394_ftn) (CanvasGroup_t2669816671 *);
	static CanvasGroup_get_ignoreParentGroups_m3625611394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m3625611394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool CanvasGroup_IsRaycastLocationValid_m3891008839 (CanvasGroup_t2669816671 * __this, Vector2_t2246369278  ___sp0, Camera_t362346687 * ___eventCamera1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m302460917(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C"  void CanvasRenderer_SetColor_m3914554622 (CanvasRenderer_t3854993866 * __this, Color_t1361298052  ___color0, const RuntimeMethod* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m3614729622(NULL /*static, unused*/, __this, (&___color0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_SetColor_m3614729622 (RuntimeObject * __this /* static, unused */, CanvasRenderer_t3854993866 * ___self0, Color_t1361298052 * ___color1, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m3614729622_ftn) (CanvasRenderer_t3854993866 *, Color_t1361298052 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m3614729622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m3614729622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___color1);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C"  Color_t1361298052  CanvasRenderer_GetColor_m3084951917 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	Color_t1361298052  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t1361298052  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		CanvasRenderer_INTERNAL_CALL_GetColor_m1322085719(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Color_t1361298052  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Color_t1361298052  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_GetColor_m1322085719 (RuntimeObject * __this /* static, unused */, CanvasRenderer_t3854993866 * ___self0, Color_t1361298052 * ___value1, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_GetColor_m1322085719_ftn) (CanvasRenderer_t3854993866 *, Color_t1361298052 *);
	static CanvasRenderer_INTERNAL_CALL_GetColor_m1322085719_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_GetColor_m1322085719_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___value1);
}
// System.Void UnityEngine.CanvasRenderer::EnableRectClipping(UnityEngine.Rect)
extern "C"  void CanvasRenderer_EnableRectClipping_m4216305513 (CanvasRenderer_t3854993866 * __this, Rect_t952252086  ___rect0, const RuntimeMethod* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3298077761(NULL /*static, unused*/, __this, (&___rect0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3298077761 (RuntimeObject * __this /* static, unused */, CanvasRenderer_t3854993866 * ___self0, Rect_t952252086 * ___rect1, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3298077761_ftn) (CanvasRenderer_t3854993866 *, Rect_t952252086 *);
	static CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3298077761_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3298077761_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1);
}
// System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
extern "C"  void CanvasRenderer_DisableRectClipping_m3957352765 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_DisableRectClipping_m3957352765_ftn) (CanvasRenderer_t3854993866 *);
	static CanvasRenderer_DisableRectClipping_m3957352765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_DisableRectClipping_m3957352765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::DisableRectClipping()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
extern "C"  void CanvasRenderer_set_hasPopInstruction_m3349074692 (CanvasRenderer_t3854993866 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_set_hasPopInstruction_m3349074692_ftn) (CanvasRenderer_t3854993866 *, bool);
	static CanvasRenderer_set_hasPopInstruction_m3349074692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_hasPopInstruction_m3349074692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C"  int32_t CanvasRenderer_get_materialCount_m457838510 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*CanvasRenderer_get_materialCount_m457838510_ftn) (CanvasRenderer_t3854993866 *);
	static CanvasRenderer_get_materialCount_m457838510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_materialCount_m457838510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_materialCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C"  void CanvasRenderer_set_materialCount_m3662488854 (CanvasRenderer_t3854993866 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_set_materialCount_m3662488854_ftn) (CanvasRenderer_t3854993866 *, int32_t);
	static CanvasRenderer_set_materialCount_m3662488854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_materialCount_m3662488854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_materialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetMaterial_m1504826497 (CanvasRenderer_t3854993866 * __this, Material_t1926439680 * ___material0, int32_t ___index1, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m1504826497_ftn) (CanvasRenderer_t3854993866 *, Material_t1926439680 *, int32_t);
	static CanvasRenderer_SetMaterial_m1504826497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m1504826497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetMaterial_m3751325793 (CanvasRenderer_t3854993866 * __this, Material_t1926439680 * ___material0, Texture_t954505614 * ___texture1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = CanvasRenderer_get_materialCount_m457838510(__this, /*hidden argument*/NULL);
		int32_t L_1 = Math_Max_m3399311511(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		CanvasRenderer_set_materialCount_m3662488854(__this, L_1, /*hidden argument*/NULL);
		Material_t1926439680 * L_2 = ___material0;
		CanvasRenderer_SetMaterial_m1504826497(__this, L_2, 0, /*hidden argument*/NULL);
		Texture_t954505614 * L_3 = ___texture1;
		CanvasRenderer_SetTexture_m576023973(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
extern "C"  void CanvasRenderer_set_popMaterialCount_m1377782507 (CanvasRenderer_t3854993866 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_set_popMaterialCount_m1377782507_ftn) (CanvasRenderer_t3854993866 *, int32_t);
	static CanvasRenderer_set_popMaterialCount_m1377782507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_popMaterialCount_m1377782507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetPopMaterial_m885548523 (CanvasRenderer_t3854993866 * __this, Material_t1926439680 * ___material0, int32_t ___index1, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SetPopMaterial_m885548523_ftn) (CanvasRenderer_t3854993866 *, Material_t1926439680 *, int32_t);
	static CanvasRenderer_SetPopMaterial_m885548523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetPopMaterial_m885548523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetTexture_m576023973 (CanvasRenderer_t3854993866 * __this, Texture_t954505614 * ___texture0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SetTexture_m576023973_ftn) (CanvasRenderer_t3854993866 *, Texture_t954505614 *);
	static CanvasRenderer_SetTexture_m576023973_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetTexture_m576023973_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetAlphaTexture_m3667416821 (CanvasRenderer_t3854993866 * __this, Texture_t954505614 * ___texture0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SetAlphaTexture_m3667416821_ftn) (CanvasRenderer_t3854993866 *, Texture_t954505614 *);
	static CanvasRenderer_SetAlphaTexture_m3667416821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetAlphaTexture_m3667416821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C"  void CanvasRenderer_SetMesh_m2382286621 (CanvasRenderer_t3854993866 * __this, Mesh_t4237566844 * ___mesh0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SetMesh_m2382286621_ftn) (CanvasRenderer_t3854993866 *, Mesh_t4237566844 *);
	static CanvasRenderer_SetMesh_m2382286621_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMesh_m2382286621_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh0);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C"  void CanvasRenderer_Clear_m126606404 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_Clear_m126606404_ftn) (CanvasRenderer_t3854993866 *);
	static CanvasRenderer_Clear_m126606404_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m126606404_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreams(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_SplitUIVertexStreams_m1188859515 (RuntimeObject * __this /* static, unused */, List_1_t323345135 * ___verts0, List_1_t1311628880 * ___positions1, List_1_t2336627130 * ___colors2, List_1_t3920572369 * ___uv0S3, List_1_t3920572369 * ___uv1S4, List_1_t3920572369 * ___uv2S5, List_1_t3920572369 * ___uv3S6, List_1_t1311628880 * ___normals7, List_1_t3575182278 * ___tangents8, List_1_t894813333 * ___indices9, const RuntimeMethod* method)
{
	{
		List_1_t323345135 * L_0 = ___verts0;
		List_1_t1311628880 * L_1 = ___positions1;
		List_1_t2336627130 * L_2 = ___colors2;
		List_1_t3920572369 * L_3 = ___uv0S3;
		List_1_t3920572369 * L_4 = ___uv1S4;
		List_1_t3920572369 * L_5 = ___uv2S5;
		List_1_t3920572369 * L_6 = ___uv3S6;
		List_1_t1311628880 * L_7 = ___normals7;
		List_1_t3575182278 * L_8 = ___tangents8;
		CanvasRenderer_SplitUIVertexStreamsInternal_m1472523664(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		List_1_t323345135 * L_9 = ___verts0;
		List_1_t894813333 * L_10 = ___indices9;
		CanvasRenderer_SplitIndicesStreamsInternal_m482009823(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitUIVertexStreamsInternal_m1472523664 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___verts0, RuntimeObject * ___positions1, RuntimeObject * ___colors2, RuntimeObject * ___uv0S3, RuntimeObject * ___uv1S4, RuntimeObject * ___uv2S5, RuntimeObject * ___uv3S6, RuntimeObject * ___normals7, RuntimeObject * ___tangents8, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SplitUIVertexStreamsInternal_m1472523664_ftn) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *);
	static CanvasRenderer_SplitUIVertexStreamsInternal_m1472523664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitUIVertexStreamsInternal_m1472523664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___uv2S5, ___uv3S6, ___normals7, ___tangents8);
}
// System.Void UnityEngine.CanvasRenderer::SplitIndicesStreamsInternal(System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitIndicesStreamsInternal_m482009823 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___verts0, RuntimeObject * ___indices1, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_SplitIndicesStreamsInternal_m482009823_ftn) (RuntimeObject *, RuntimeObject *);
	static CanvasRenderer_SplitIndicesStreamsInternal_m482009823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitIndicesStreamsInternal_m482009823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitIndicesStreamsInternal(System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___indices1);
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_CreateUIVertexStream_m855315256 (RuntimeObject * __this /* static, unused */, List_1_t323345135 * ___verts0, List_1_t1311628880 * ___positions1, List_1_t2336627130 * ___colors2, List_1_t3920572369 * ___uv0S3, List_1_t3920572369 * ___uv1S4, List_1_t3920572369 * ___uv2S5, List_1_t3920572369 * ___uv3S6, List_1_t1311628880 * ___normals7, List_1_t3575182278 * ___tangents8, List_1_t894813333 * ___indices9, const RuntimeMethod* method)
{
	{
		List_1_t323345135 * L_0 = ___verts0;
		List_1_t1311628880 * L_1 = ___positions1;
		List_1_t2336627130 * L_2 = ___colors2;
		List_1_t3920572369 * L_3 = ___uv0S3;
		List_1_t3920572369 * L_4 = ___uv1S4;
		List_1_t3920572369 * L_5 = ___uv2S5;
		List_1_t3920572369 * L_6 = ___uv3S6;
		List_1_t1311628880 * L_7 = ___normals7;
		List_1_t3575182278 * L_8 = ___tangents8;
		List_1_t894813333 * L_9 = ___indices9;
		CanvasRenderer_CreateUIVertexStreamInternal_m322823956(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_CreateUIVertexStreamInternal_m322823956 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___verts0, RuntimeObject * ___positions1, RuntimeObject * ___colors2, RuntimeObject * ___uv0S3, RuntimeObject * ___uv1S4, RuntimeObject * ___uv2S5, RuntimeObject * ___uv3S6, RuntimeObject * ___normals7, RuntimeObject * ___tangents8, RuntimeObject * ___indices9, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_CreateUIVertexStreamInternal_m322823956_ftn) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *);
	static CanvasRenderer_CreateUIVertexStreamInternal_m322823956_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_CreateUIVertexStreamInternal_m322823956_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___uv2S5, ___uv3S6, ___normals7, ___tangents8, ___indices9);
}
// System.Boolean UnityEngine.CanvasRenderer::get_cull()
extern "C"  bool CanvasRenderer_get_cull_m456291770 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	typedef bool (*CanvasRenderer_get_cull_m456291770_ftn) (CanvasRenderer_t3854993866 *);
	static CanvasRenderer_get_cull_m456291770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_cull_m456291770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_cull()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
extern "C"  void CanvasRenderer_set_cull_m705460468 (CanvasRenderer_t3854993866 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*CanvasRenderer_set_cull_m705460468_ftn) (CanvasRenderer_t3854993866 *, bool);
	static CanvasRenderer_set_cull_m705460468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_cull_m705460468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_cull(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C"  int32_t CanvasRenderer_get_absoluteDepth_m2565115375 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m2565115375_ftn) (CanvasRenderer_t3854993866 *);
	static CanvasRenderer_get_absoluteDepth_m2565115375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m2565115375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
extern "C"  bool CanvasRenderer_get_hasMoved_m488326089 (CanvasRenderer_t3854993866 * __this, const RuntimeMethod* method)
{
	typedef bool (*CanvasRenderer_get_hasMoved_m488326089_ftn) (CanvasRenderer_t3854993866 *);
	static CanvasRenderer_get_hasMoved_m488326089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_hasMoved_m488326089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_hasMoved()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1348189536 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, Vector2_t2246369278  ___screenPoint1, Camera_t362346687 * ___cam2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1348189536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RectTransform_t1161610249 * L_0 = ___rect0;
		Camera_t362346687 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m218225114(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m218225114 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, Vector2_t2246369278 * ___screenPoint1, Camera_t362346687 * ___cam2, const RuntimeMethod* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m218225114_ftn) (RectTransform_t1161610249 *, Vector2_t2246369278 *, Camera_t362346687 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m218225114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m218225114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	bool retVal = _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
	return retVal;
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern "C"  Vector2_t2246369278  RectTransformUtility_PixelAdjustPoint_m4183794563 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___point0, Transform_t3580444445 * ___elementTransform1, Canvas_t2667390772 * ___canvas2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m4183794563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2246369278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3580444445 * L_0 = ___elementTransform1;
		Canvas_t2667390772 * L_1 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3956955504(NULL /*static, unused*/, (&___point0), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t2246369278  L_2 = V_0;
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Vector2_t2246369278  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3956955504 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278 * ___point0, Transform_t3580444445 * ___elementTransform1, Canvas_t2667390772 * ___canvas2, Vector2_t2246369278 * ___value3, const RuntimeMethod* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3956955504_ftn) (Vector2_t2246369278 *, Transform_t3580444445 *, Canvas_t2667390772 *, Vector2_t2246369278 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3956955504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3956955504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___value3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C"  Rect_t952252086  RectTransformUtility_PixelAdjustRect_m572670876 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rectTransform0, Canvas_t2667390772 * ___canvas1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m572670876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t952252086  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t952252086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t1161610249 * L_0 = ___rectTransform0;
		Canvas_t2667390772 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m2439407777(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t952252086  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Rect_t952252086  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m2439407777 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rectTransform0, Canvas_t2667390772 * ___canvas1, Rect_t952252086 * ___value2, const RuntimeMethod* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m2439407777_ftn) (RectTransform_t1161610249 *, Canvas_t2667390772 *, Rect_t952252086 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m2439407777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m2439407777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m1953132754 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, Vector2_t2246369278  ___screenPoint1, Camera_t362346687 * ___cam2, Vector3_t3932393085 * ___worldPoint3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m1953132754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t1807385980  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t4100015541  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		Vector3_t3932393085 * L_0 = ___worldPoint3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_1 = Vector2_get_zero_m1981541827(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3932393085  L_2 = Vector2_op_Implicit_m2591794359(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)L_0 = L_2;
		Camera_t362346687 * L_3 = ___cam2;
		Vector2_t2246369278  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		Ray_t1807385980  L_5 = RectTransformUtility_ScreenPointToRay_m3183574266(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t1161610249 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t3497065016  L_7 = Transform_get_rotation_m444413307(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_8 = Vector3_get_back_m1633092186(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t3497065016_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_9 = Quaternion_op_Multiply_m675595747(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t3932393085  L_11 = Transform_get_position_m2750929393(L_10, /*hidden argument*/NULL);
		Plane__ctor_m1011206029((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t1807385980  L_12 = V_0;
		bool L_13 = Plane_Raycast_m894877444((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_004c;
		}
	}
	{
		V_3 = (bool)0;
		goto IL_0061;
	}

IL_004c:
	{
		Vector3_t3932393085 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t3932393085  L_16 = Ray_GetPoint_m3306741896((&V_0), L_15, /*hidden argument*/NULL);
		*(Vector3_t3932393085 *)L_14 = L_16;
		V_3 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m4197255120 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, Vector2_t2246369278  ___screenPoint1, Camera_t362346687 * ___cam2, Vector2_t2246369278 * ___localPoint3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m4197255120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3932393085  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t2246369278 * L_0 = ___localPoint3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_1 = Vector2_get_zero_m1981541827(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector2_t2246369278 *)L_0 = L_1;
		RectTransform_t1161610249 * L_2 = ___rect0;
		Vector2_t2246369278  L_3 = ___screenPoint1;
		Camera_t362346687 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m1953132754(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Vector2_t2246369278 * L_6 = ___localPoint3;
		RectTransform_t1161610249 * L_7 = ___rect0;
		Vector3_t3932393085  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t3932393085  L_9 = Transform_InverseTransformPoint_m3824069262(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_10 = Vector2_op_Implicit_m497246090(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		*(Vector2_t2246369278 *)L_6 = L_10;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_11 = V_1;
		return L_11;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t1807385980  RectTransformUtility_ScreenPointToRay_m3183574266 (RuntimeObject * __this /* static, unused */, Camera_t362346687 * ___cam0, Vector2_t2246369278  ___screenPos1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m3183574266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t1807385980  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3932393085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t362346687 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t362346687 * L_2 = ___cam0;
		Vector2_t2246369278  L_3 = ___screenPos1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_4 = Vector2_op_Implicit_m2591794359(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t1807385980  L_5 = Camera_ScreenPointToRay_m3701064168(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_004a;
	}

IL_001f:
	{
		Vector2_t2246369278  L_6 = ___screenPos1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2246369278_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_7 = Vector2_op_Implicit_m2591794359(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t3932393085 * L_8 = (&V_1);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t3932393085  L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3932393085_il2cpp_TypeInfo_var);
		Vector3_t3932393085  L_11 = Vector3_get_forward_m4214875591(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t1807385980  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m2331331332((&L_12), L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_004a;
	}

IL_004a:
	{
		Ray_t1807385980  L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3943889344 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3943889344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t1161610249 * V_1 = NULL;
	Vector2_t2246369278  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2246369278  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2246369278  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2246369278  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t1161610249 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00f3;
	}

IL_0012:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 0;
		goto IL_0048;
	}

IL_0020:
	{
		RectTransform_t1161610249 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3580444445 * L_5 = Transform_GetChild_m1886946583(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t1161610249 *)IsInstSealed((RuntimeObject*)L_5, RectTransform_t1161610249_il2cpp_TypeInfo_var));
		RectTransform_t1161610249 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_6, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		RectTransform_t1161610249 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3943889344(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_0;
		RectTransform_t1161610249 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m2090497278(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0055:
	{
		RectTransform_t1161610249 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2246369278  L_15 = RectTransform_get_pivot_m57090389(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m1427364022((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m1623790336((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_19 = ___rect0;
		Vector2_t2246369278  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m2409069685(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0084;
		}
	}
	{
		goto IL_00f3;
	}

IL_0084:
	{
		RectTransform_t1161610249 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t2246369278  L_23 = RectTransform_get_anchoredPosition_m3060243242(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m1427364022((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m1623790336((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_27 = ___rect0;
		Vector2_t2246369278  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m2197021314(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t2246369278  L_30 = RectTransform_get_anchorMin_m3262898663(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t1161610249 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2246369278  L_32 = RectTransform_get_anchorMax_m1509281336(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m1427364022((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m1427364022((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m1623790336((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m1623790336((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_40 = ___rect0;
		Vector2_t2246369278  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m4291838635(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_42 = ___rect0;
		Vector2_t2246369278  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m3836270766(L_42, L_43, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m3715915365 (RuntimeObject * __this /* static, unused */, RectTransform_t1161610249 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m3715915365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t1161610249 * V_1 = NULL;
	{
		RectTransform_t1161610249 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3342981539(NULL /*static, unused*/, L_0, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00b4;
	}

IL_0012:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0020:
	{
		RectTransform_t1161610249 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3580444445 * L_5 = Transform_GetChild_m1886946583(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t1161610249 *)IsInstSealed((RuntimeObject*)L_5, RectTransform_t1161610249_il2cpp_TypeInfo_var));
		RectTransform_t1161610249 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3645472222_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1808560565(NULL /*static, unused*/, L_6, (Object_t3645472222 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		RectTransform_t1161610249 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m3715915365(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_10 = V_0;
		RectTransform_t1161610249 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m2090497278(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0054:
	{
		RectTransform_t1161610249 * L_13 = ___rect0;
		RectTransform_t1161610249 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2246369278  L_15 = RectTransform_get_pivot_m57090389(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_16 = RectTransformUtility_GetTransposed_m1404395155(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m2409069685(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_17 = ___rect0;
		RectTransform_t1161610249 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t2246369278  L_19 = RectTransform_get_sizeDelta_m3804390761(L_18, /*hidden argument*/NULL);
		Vector2_t2246369278  L_20 = RectTransformUtility_GetTransposed_m1404395155(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m826926724(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b4;
	}

IL_0081:
	{
		RectTransform_t1161610249 * L_22 = ___rect0;
		RectTransform_t1161610249 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t2246369278  L_24 = RectTransform_get_anchoredPosition_m3060243242(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var);
		Vector2_t2246369278  L_25 = RectTransformUtility_GetTransposed_m1404395155(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m2197021314(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_26 = ___rect0;
		RectTransform_t1161610249 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t2246369278  L_28 = RectTransform_get_anchorMin_m3262898663(L_27, /*hidden argument*/NULL);
		Vector2_t2246369278  L_29 = RectTransformUtility_GetTransposed_m1404395155(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4291838635(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t1161610249 * L_30 = ___rect0;
		RectTransform_t1161610249 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2246369278  L_32 = RectTransform_get_anchorMax_m1509281336(L_31, /*hidden argument*/NULL);
		Vector2_t2246369278  L_33 = RectTransformUtility_GetTransposed_m1404395155(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m3836270766(L_30, L_33, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2246369278  RectTransformUtility_GetTransposed_m1404395155 (RuntimeObject * __this /* static, unused */, Vector2_t2246369278  ___input0, const RuntimeMethod* method)
{
	Vector2_t2246369278  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___input0)->get_y_1();
		float L_1 = (&___input0)->get_x_0();
		Vector2_t2246369278  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3768944458((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t2246369278  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern "C"  void RectTransformUtility__cctor_m2248621842 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m2248621842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RectTransformUtility_t2671632890_StaticFields*)il2cpp_codegen_static_fields_for(RectTransformUtility_t2671632890_il2cpp_TypeInfo_var))->set_s_Corners_0(((Vector3U5BU5D_t1432878832*)SZArrayNew(Vector3U5BU5D_t1432878832_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Void UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)
extern "C"  void UISystemProfilerApi_BeginSample_m4234145020 (RuntimeObject * __this /* static, unused */, int32_t ___type0, const RuntimeMethod* method)
{
	typedef void (*UISystemProfilerApi_BeginSample_m4234145020_ftn) (int32_t);
	static UISystemProfilerApi_BeginSample_m4234145020_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UISystemProfilerApi_BeginSample_m4234145020_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)");
	_il2cpp_icall_func(___type0);
}
// System.Void UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)
extern "C"  void UISystemProfilerApi_EndSample_m1305348269 (RuntimeObject * __this /* static, unused */, int32_t ___type0, const RuntimeMethod* method)
{
	typedef void (*UISystemProfilerApi_EndSample_m1305348269_ftn) (int32_t);
	static UISystemProfilerApi_EndSample_m1305348269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UISystemProfilerApi_EndSample_m1305348269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)");
	_il2cpp_icall_func(___type0);
}
// System.Void UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)
extern "C"  void UISystemProfilerApi_AddMarker_m4034374101 (RuntimeObject * __this /* static, unused */, String_t* ___name0, Object_t3645472222 * ___obj1, const RuntimeMethod* method)
{
	typedef void (*UISystemProfilerApi_AddMarker_m4034374101_ftn) (String_t*, Object_t3645472222 *);
	static UISystemProfilerApi_AddMarker_m4034374101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UISystemProfilerApi_AddMarker_m4034374101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___name0, ___obj1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
