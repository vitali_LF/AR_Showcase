﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t1587671786_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1587671786_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1587671786_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1587671786_0_0_0;
extern "C" void Escape_t1500098419_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t1500098419_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t1500098419_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t1500098419_0_0_0;
extern "C" void PreviousInfo_t1824241943_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t1824241943_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t1824241943_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t1824241943_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t1324644548();
extern const RuntimeType AppDomainInitializer_t1324644548_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t477965785();
extern const RuntimeType Swapper_t477965785_0_0_0;
extern "C" void DictionaryEntry_t3507565976_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3507565976_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3507565976_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3507565976_0_0_0;
extern "C" void Slot_t1231688804_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t1231688804_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t1231688804_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t1231688804_0_0_0;
extern "C" void Slot_t697738515_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t697738515_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t697738515_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t697738515_0_0_0;
extern "C" void Enum_t2432427458_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t2432427458_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t2432427458_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t2432427458_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3480986752();
extern const RuntimeType ReadDelegate_t3480986752_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2632784088();
extern const RuntimeType WriteDelegate_t2632784088_0_0_0;
extern "C" void MonoIOStat_t966205337_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t966205337_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t966205337_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t966205337_0_0_0;
extern "C" void MonoEnumInfo_t3774359917_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3774359917_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3774359917_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3774359917_0_0_0;
extern "C" void CustomAttributeNamedArgument_t4247477102_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t4247477102_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t4247477102_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t4247477102_0_0_0;
extern "C" void CustomAttributeTypedArgument_t4128697880_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t4128697880_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t4128697880_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t4128697880_0_0_0;
extern "C" void ILTokenInfo_t598344192_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t598344192_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t598344192_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t598344192_0_0_0;
extern "C" void MonoResource_t2877269471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t2877269471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t2877269471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t2877269471_0_0_0;
extern "C" void MonoWin32Resource_t2032464017_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoWin32Resource_t2032464017_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoWin32Resource_t2032464017_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoWin32Resource_t2032464017_0_0_0;
extern "C" void RefEmitPermissionSet_t4262511222_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t4262511222_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t4262511222_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t4262511222_0_0_0;
extern "C" void MonoEventInfo_t293995643_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t293995643_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t293995643_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t293995643_0_0_0;
extern "C" void MonoMethodInfo_t3401988495_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t3401988495_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t3401988495_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t3401988495_0_0_0;
extern "C" void MonoPropertyInfo_t2250684552_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t2250684552_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t2250684552_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t2250684552_0_0_0;
extern "C" void ParameterModifier_t1933193809_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1933193809_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1933193809_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1933193809_0_0_0;
extern "C" void ResourceCacheItem_t2363372085_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t2363372085_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t2363372085_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t2363372085_0_0_0;
extern "C" void ResourceInfo_t3026636918_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3026636918_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3026636918_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3026636918_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t1973313903();
extern const RuntimeType CrossContextDelegate_t1973313903_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3366050221();
extern const RuntimeType CallbackHandler_t3366050221_0_0_0;
extern "C" void SerializationEntry_t2235695213_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t2235695213_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t2235695213_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t2235695213_0_0_0;
extern "C" void StreamingContext_t1986025897_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t1986025897_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t1986025897_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t1986025897_0_0_0;
extern "C" void DSAParameters_t1143224510_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1143224510_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1143224510_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1143224510_0_0_0;
extern "C" void RSAParameters_t3299593293_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t3299593293_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t3299593293_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t3299593293_0_0_0;
extern "C" void SecurityFrame_t3607465435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t3607465435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t3607465435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t3607465435_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3633341268();
extern const RuntimeType ThreadStart_t3633341268_0_0_0;
extern "C" void ValueType_t1108148719_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t1108148719_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t1108148719_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t1108148719_0_0_0;
extern "C" void X509ChainStatus_t261084987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t261084987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t261084987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t261084987_0_0_0;
extern "C" void IntStack_t4138582879_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t4138582879_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t4138582879_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t4138582879_0_0_0;
extern "C" void Interval_t389189043_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t389189043_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t389189043_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t389189043_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t2256576524();
extern const RuntimeType CostDelegate_t2256576524_0_0_0;
extern "C" void UriScheme_t3530988980_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t3530988980_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t3530988980_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t3530988980_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t3065966669();
extern const RuntimeType Action_t3065966669_0_0_0;
extern "C" void AnimationCurve_t2893751101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t2893751101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t2893751101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t2893751101_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t2572717340();
extern const RuntimeType LogCallback_t2572717340_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t1978334135();
extern const RuntimeType LowMemoryCallback_t1978334135_0_0_0;
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t2350432906_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t2350432906_0_0_0;
extern "C" void AssetBundleRequest_t3347087835_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t3347087835_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t3347087835_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t3347087835_0_0_0;
extern "C" void AsyncOperation_t3797817720_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t3797817720_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t3797817720_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t3797817720_0_0_0;
extern "C" void Coroutine_t260305101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t260305101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t260305101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t260305101_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t2649428469();
extern const RuntimeType CSSMeasureFunc_t2649428469_0_0_0;
extern "C" void CullingGroup_t637827745_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t637827745_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t637827745_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t637827745_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t1464331389();
extern const RuntimeType StateChanged_t1464331389_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1656811884();
extern const RuntimeType DisplaysUpdatedDelegate_t1656811884_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t191347160();
extern const RuntimeType UnityAction_t191347160_0_0_0;
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t2081117541_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t2081117541_0_0_0;
extern "C" void Gradient_t2667682126_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t2667682126_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t2667682126_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t2667682126_0_0_0;
extern "C" void Object_t3645472222_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t3645472222_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t3645472222_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t3645472222_0_0_0;
extern "C" void PlayableBinding_t2941134609_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t2941134609_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t2941134609_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t2941134609_0_0_0;
extern "C" void RectOffset_t817686154_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t817686154_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t817686154_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t817686154_0_0_0;
extern "C" void ResourceRequest_t2277419115_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t2277419115_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t2277419115_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t2277419115_0_0_0;
extern "C" void ScriptableObject_t1324617639_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t1324617639_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t1324617639_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t1324617639_0_0_0;
extern "C" void HitInfo_t3525137541_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3525137541_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3525137541_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t3525137541_0_0_0;
extern "C" void TrackedReference_t2612049617_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t2612049617_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t2612049617_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t2612049617_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3823435737();
extern const RuntimeType RequestAtlasCallback_t3823435737_0_0_0;
extern "C" void WorkRequest_t266095197_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t266095197_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t266095197_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t266095197_0_0_0;
extern "C" void WaitForSeconds_t1644069752_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1644069752_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1644069752_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t1644069752_0_0_0;
extern "C" void YieldInstruction_t1540633877_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t1540633877_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t1540633877_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t1540633877_0_0_0;
extern "C" void RaycastHit2D_t218385889_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t218385889_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t218385889_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t218385889_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2723651790();
extern const RuntimeType FontTextureRebuildCallback_t2723651790_0_0_0;
extern "C" void TextGenerationSettings_t1492523250_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1492523250_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1492523250_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1492523250_0_0_0;
extern "C" void TextGenerator_t2871023231_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t2871023231_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t2871023231_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t2871023231_0_0_0;
extern "C" void AnimationEvent_t300192047_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t300192047_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t300192047_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t300192047_0_0_0;
extern "C" void AnimatorTransitionInfo_t3746244356_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t3746244356_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t3746244356_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t3746244356_0_0_0;
extern "C" void HumanBone_t380088121_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t380088121_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t380088121_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t380088121_0_0_0;
extern "C" void SkeletonBone_t1430513151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t1430513151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t1430513151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t1430513151_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1197740906();
extern const RuntimeType PCMReaderCallback_t1197740906_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t3586252241();
extern const RuntimeType PCMSetPositionCallback_t3586252241_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2586401630();
extern const RuntimeType AudioConfigurationChangeHandler_t2586401630_0_0_0;
extern "C" void GcAchievementData_t1576042669_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t1576042669_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t1576042669_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t1576042669_0_0_0;
extern "C" void GcAchievementDescriptionData_t745756929_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t745756929_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t745756929_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t745756929_0_0_0;
extern "C" void GcLeaderboard_t3922680439_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t3922680439_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t3922680439_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t3922680439_0_0_0;
extern "C" void GcScoreData_t2566310542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2566310542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2566310542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2566310542_0_0_0;
extern "C" void GcUserProfileData_t1753967645_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t1753967645_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t1753967645_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t1753967645_0_0_0;
extern "C" void Event_t3625350923_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t3625350923_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t3625350923_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t3625350923_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2894641446();
extern const RuntimeType WindowFunction_t2894641446_0_0_0;
extern "C" void GUIContent_t602529594_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t602529594_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t602529594_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t602529594_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3419666957();
extern const RuntimeType SkinChangedDelegate_t3419666957_0_0_0;
extern "C" void GUIStyle_t3615497928_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3615497928_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3615497928_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3615497928_0_0_0;
extern "C" void GUIStyleState_t2425398439_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t2425398439_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t2425398439_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t2425398439_0_0_0;
extern "C" void Collision_t3299101783_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t3299101783_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t3299101783_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t3299101783_0_0_0;
extern "C" void ControllerColliderHit_t3853337050_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t3853337050_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t3853337050_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t3853337050_0_0_0;
extern "C" void RaycastHit_t3372097066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t3372097066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t3372097066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t3372097066_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t1608056246();
extern const RuntimeType WillRenderCanvases_t1608056246_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t1745550773();
extern const RuntimeType SessionStateChanged_t1745550773_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t2489343964();
extern const RuntimeType UpdatedEventHandler_t2489343964_0_0_0;
extern "C" void RaycastResult_t1958542877_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t1958542877_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t1958542877_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t1958542877_0_0_0;
extern "C" void ColorTween_t2856906502_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t2856906502_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t2856906502_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t2856906502_0_0_0;
extern "C" void FloatTween_t2756583341_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t2756583341_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t2756583341_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t2756583341_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t3514437264();
extern const RuntimeType OnValidateInput_t3514437264_0_0_0;
extern "C" void Navigation_t2227424972_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t2227424972_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t2227424972_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t2227424972_0_0_0;
extern "C" void SpriteState_t710398810_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t710398810_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t710398810_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t710398810_0_0_0;
extern "C" void ARAnchor_t2991191395_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARAnchor_t2991191395_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARAnchor_t2991191395_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARAnchor_t2991191395_0_0_0;
extern "C" void ARHitTestResult_t942592945_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARHitTestResult_t942592945_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARHitTestResult_t942592945_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARHitTestResult_t942592945_0_0_0;
extern "C" void ARKitSessionConfiguration_t3393651406_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitSessionConfiguration_t3393651406_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitSessionConfiguration_t3393651406_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitSessionConfiguration_t3393651406_0_0_0;
extern "C" void ARKitWorldTrackingSessionConfiguration_t3856677714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitWorldTrackingSessionConfiguration_t3856677714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitWorldTrackingSessionConfiguration_t3856677714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitWorldTrackingSessionConfiguration_t3856677714_0_0_0;
extern "C" void ARPlaneAnchor_t3727658320_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARPlaneAnchor_t3727658320_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARPlaneAnchor_t3727658320_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARPlaneAnchor_t3727658320_0_0_0;
extern "C" void ARUserAnchor_t2968539873_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARUserAnchor_t2968539873_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARUserAnchor_t2968539873_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARUserAnchor_t2968539873_0_0_0;
extern "C" void UnityARCamera_t3124383669_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARCamera_t3124383669_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARCamera_t3124383669_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARCamera_t3124383669_0_0_0;
extern "C" void UnityARHitTestResult_t3674613141_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARHitTestResult_t3674613141_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARHitTestResult_t3674613141_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARHitTestResult_t3674613141_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorAdded_t4244461251();
extern const RuntimeType ARAnchorAdded_t4244461251_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorRemoved_t872492321();
extern const RuntimeType ARAnchorRemoved_t872492321_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorUpdated_t532484182();
extern const RuntimeType ARAnchorUpdated_t532484182_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionCallback_t1297913645();
extern const RuntimeType ARSessionCallback_t1297913645_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionFailed_t532221400();
extern const RuntimeType ARSessionFailed_t532221400_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorAdded_t3319197303();
extern const RuntimeType ARUserAnchorAdded_t3319197303_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorRemoved_t2261685995();
extern const RuntimeType ARUserAnchorRemoved_t2261685995_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorUpdated_t2797778030();
extern const RuntimeType ARUserAnchorUpdated_t2797778030_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorAdded_t1970944026();
extern const RuntimeType internal_ARAnchorAdded_t1970944026_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3417896546();
extern const RuntimeType internal_ARAnchorRemoved_t3417896546_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t1543169425();
extern const RuntimeType internal_ARAnchorUpdated_t1543169425_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFrameUpdate_t2996439740();
extern const RuntimeType internal_ARFrameUpdate_t2996439740_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t2912316026();
extern const RuntimeType internal_ARSessionTrackingChanged_t2912316026_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t442523547();
extern const RuntimeType internal_ARUserAnchorAdded_t442523547_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t1115337844();
extern const RuntimeType internal_ARUserAnchorRemoved_t1115337844_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t2457302376();
extern const RuntimeType internal_ARUserAnchorUpdated_t2457302376_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[124] = 
{
	{ NULL, Context_t1587671786_marshal_pinvoke, Context_t1587671786_marshal_pinvoke_back, Context_t1587671786_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1587671786_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t1500098419_marshal_pinvoke, Escape_t1500098419_marshal_pinvoke_back, Escape_t1500098419_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t1500098419_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t1824241943_marshal_pinvoke, PreviousInfo_t1824241943_marshal_pinvoke_back, PreviousInfo_t1824241943_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t1824241943_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t1324644548, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t1324644548_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t477965785, NULL, NULL, NULL, NULL, NULL, &Swapper_t477965785_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3507565976_marshal_pinvoke, DictionaryEntry_t3507565976_marshal_pinvoke_back, DictionaryEntry_t3507565976_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3507565976_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t1231688804_marshal_pinvoke, Slot_t1231688804_marshal_pinvoke_back, Slot_t1231688804_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t1231688804_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t697738515_marshal_pinvoke, Slot_t697738515_marshal_pinvoke_back, Slot_t697738515_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t697738515_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t2432427458_marshal_pinvoke, Enum_t2432427458_marshal_pinvoke_back, Enum_t2432427458_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t2432427458_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t3480986752, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t3480986752_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t2632784088, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t2632784088_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t966205337_marshal_pinvoke, MonoIOStat_t966205337_marshal_pinvoke_back, MonoIOStat_t966205337_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t966205337_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3774359917_marshal_pinvoke, MonoEnumInfo_t3774359917_marshal_pinvoke_back, MonoEnumInfo_t3774359917_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3774359917_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t4247477102_marshal_pinvoke, CustomAttributeNamedArgument_t4247477102_marshal_pinvoke_back, CustomAttributeNamedArgument_t4247477102_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t4247477102_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t4128697880_marshal_pinvoke, CustomAttributeTypedArgument_t4128697880_marshal_pinvoke_back, CustomAttributeTypedArgument_t4128697880_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t4128697880_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t598344192_marshal_pinvoke, ILTokenInfo_t598344192_marshal_pinvoke_back, ILTokenInfo_t598344192_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t598344192_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t2877269471_marshal_pinvoke, MonoResource_t2877269471_marshal_pinvoke_back, MonoResource_t2877269471_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t2877269471_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoWin32Resource_t2032464017_marshal_pinvoke, MonoWin32Resource_t2032464017_marshal_pinvoke_back, MonoWin32Resource_t2032464017_marshal_pinvoke_cleanup, NULL, NULL, &MonoWin32Resource_t2032464017_0_0_0 } /* System.Reflection.Emit.MonoWin32Resource */,
	{ NULL, RefEmitPermissionSet_t4262511222_marshal_pinvoke, RefEmitPermissionSet_t4262511222_marshal_pinvoke_back, RefEmitPermissionSet_t4262511222_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t4262511222_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t293995643_marshal_pinvoke, MonoEventInfo_t293995643_marshal_pinvoke_back, MonoEventInfo_t293995643_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t293995643_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t3401988495_marshal_pinvoke, MonoMethodInfo_t3401988495_marshal_pinvoke_back, MonoMethodInfo_t3401988495_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t3401988495_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t2250684552_marshal_pinvoke, MonoPropertyInfo_t2250684552_marshal_pinvoke_back, MonoPropertyInfo_t2250684552_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t2250684552_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1933193809_marshal_pinvoke, ParameterModifier_t1933193809_marshal_pinvoke_back, ParameterModifier_t1933193809_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1933193809_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t2363372085_marshal_pinvoke, ResourceCacheItem_t2363372085_marshal_pinvoke_back, ResourceCacheItem_t2363372085_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t2363372085_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3026636918_marshal_pinvoke, ResourceInfo_t3026636918_marshal_pinvoke_back, ResourceInfo_t3026636918_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3026636918_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t1973313903, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t1973313903_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3366050221, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3366050221_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t2235695213_marshal_pinvoke, SerializationEntry_t2235695213_marshal_pinvoke_back, SerializationEntry_t2235695213_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t2235695213_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t1986025897_marshal_pinvoke, StreamingContext_t1986025897_marshal_pinvoke_back, StreamingContext_t1986025897_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t1986025897_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1143224510_marshal_pinvoke, DSAParameters_t1143224510_marshal_pinvoke_back, DSAParameters_t1143224510_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1143224510_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t3299593293_marshal_pinvoke, RSAParameters_t3299593293_marshal_pinvoke_back, RSAParameters_t3299593293_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t3299593293_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t3607465435_marshal_pinvoke, SecurityFrame_t3607465435_marshal_pinvoke_back, SecurityFrame_t3607465435_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t3607465435_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t3633341268, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t3633341268_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t1108148719_marshal_pinvoke, ValueType_t1108148719_marshal_pinvoke_back, ValueType_t1108148719_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t1108148719_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t261084987_marshal_pinvoke, X509ChainStatus_t261084987_marshal_pinvoke_back, X509ChainStatus_t261084987_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t261084987_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t4138582879_marshal_pinvoke, IntStack_t4138582879_marshal_pinvoke_back, IntStack_t4138582879_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t4138582879_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t389189043_marshal_pinvoke, Interval_t389189043_marshal_pinvoke_back, Interval_t389189043_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t389189043_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t2256576524, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t2256576524_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t3530988980_marshal_pinvoke, UriScheme_t3530988980_marshal_pinvoke_back, UriScheme_t3530988980_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t3530988980_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t3065966669, NULL, NULL, NULL, NULL, NULL, &Action_t3065966669_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t2893751101_marshal_pinvoke, AnimationCurve_t2893751101_marshal_pinvoke_back, AnimationCurve_t2893751101_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t2893751101_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t2572717340, NULL, NULL, NULL, NULL, NULL, &LogCallback_t2572717340_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t1978334135, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t1978334135_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t2350432906_marshal_pinvoke, AssetBundleCreateRequest_t2350432906_marshal_pinvoke_back, AssetBundleCreateRequest_t2350432906_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t2350432906_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t3347087835_marshal_pinvoke, AssetBundleRequest_t3347087835_marshal_pinvoke_back, AssetBundleRequest_t3347087835_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t3347087835_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t3797817720_marshal_pinvoke, AsyncOperation_t3797817720_marshal_pinvoke_back, AsyncOperation_t3797817720_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t3797817720_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, Coroutine_t260305101_marshal_pinvoke, Coroutine_t260305101_marshal_pinvoke_back, Coroutine_t260305101_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t260305101_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t2649428469, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t2649428469_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t637827745_marshal_pinvoke, CullingGroup_t637827745_marshal_pinvoke_back, CullingGroup_t637827745_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t637827745_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t1464331389, NULL, NULL, NULL, NULL, NULL, &StateChanged_t1464331389_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1656811884, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t1656811884_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t191347160, NULL, NULL, NULL, NULL, NULL, &UnityAction_t191347160_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t2081117541_marshal_pinvoke, FailedToLoadScriptObject_t2081117541_marshal_pinvoke_back, FailedToLoadScriptObject_t2081117541_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t2081117541_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t2667682126_marshal_pinvoke, Gradient_t2667682126_marshal_pinvoke_back, Gradient_t2667682126_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t2667682126_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Object_t3645472222_marshal_pinvoke, Object_t3645472222_marshal_pinvoke_back, Object_t3645472222_marshal_pinvoke_cleanup, NULL, NULL, &Object_t3645472222_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t2941134609_marshal_pinvoke, PlayableBinding_t2941134609_marshal_pinvoke_back, PlayableBinding_t2941134609_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t2941134609_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RectOffset_t817686154_marshal_pinvoke, RectOffset_t817686154_marshal_pinvoke_back, RectOffset_t817686154_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t817686154_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t2277419115_marshal_pinvoke, ResourceRequest_t2277419115_marshal_pinvoke_back, ResourceRequest_t2277419115_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t2277419115_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t1324617639_marshal_pinvoke, ScriptableObject_t1324617639_marshal_pinvoke_back, ScriptableObject_t1324617639_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t1324617639_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3525137541_marshal_pinvoke, HitInfo_t3525137541_marshal_pinvoke_back, HitInfo_t3525137541_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3525137541_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t2612049617_marshal_pinvoke, TrackedReference_t2612049617_marshal_pinvoke_back, TrackedReference_t2612049617_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t2612049617_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3823435737, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3823435737_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t266095197_marshal_pinvoke, WorkRequest_t266095197_marshal_pinvoke_back, WorkRequest_t266095197_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t266095197_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t1644069752_marshal_pinvoke, WaitForSeconds_t1644069752_marshal_pinvoke_back, WaitForSeconds_t1644069752_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1644069752_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t1540633877_marshal_pinvoke, YieldInstruction_t1540633877_marshal_pinvoke_back, YieldInstruction_t1540633877_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t1540633877_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastHit2D_t218385889_marshal_pinvoke, RaycastHit2D_t218385889_marshal_pinvoke_back, RaycastHit2D_t218385889_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t218385889_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2723651790, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2723651790_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1492523250_marshal_pinvoke, TextGenerationSettings_t1492523250_marshal_pinvoke_back, TextGenerationSettings_t1492523250_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1492523250_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t2871023231_marshal_pinvoke, TextGenerator_t2871023231_marshal_pinvoke_back, TextGenerator_t2871023231_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t2871023231_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, AnimationEvent_t300192047_marshal_pinvoke, AnimationEvent_t300192047_marshal_pinvoke_back, AnimationEvent_t300192047_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t300192047_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t3746244356_marshal_pinvoke, AnimatorTransitionInfo_t3746244356_marshal_pinvoke_back, AnimatorTransitionInfo_t3746244356_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t3746244356_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t380088121_marshal_pinvoke, HumanBone_t380088121_marshal_pinvoke_back, HumanBone_t380088121_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t380088121_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t1430513151_marshal_pinvoke, SkeletonBone_t1430513151_marshal_pinvoke_back, SkeletonBone_t1430513151_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t1430513151_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1197740906, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1197740906_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t3586252241, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t3586252241_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2586401630, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2586401630_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ NULL, GcAchievementData_t1576042669_marshal_pinvoke, GcAchievementData_t1576042669_marshal_pinvoke_back, GcAchievementData_t1576042669_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t1576042669_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t745756929_marshal_pinvoke, GcAchievementDescriptionData_t745756929_marshal_pinvoke_back, GcAchievementDescriptionData_t745756929_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t745756929_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t3922680439_marshal_pinvoke, GcLeaderboard_t3922680439_marshal_pinvoke_back, GcLeaderboard_t3922680439_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t3922680439_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2566310542_marshal_pinvoke, GcScoreData_t2566310542_marshal_pinvoke_back, GcScoreData_t2566310542_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2566310542_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t1753967645_marshal_pinvoke, GcUserProfileData_t1753967645_marshal_pinvoke_back, GcUserProfileData_t1753967645_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t1753967645_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t3625350923_marshal_pinvoke, Event_t3625350923_marshal_pinvoke_back, Event_t3625350923_marshal_pinvoke_cleanup, NULL, NULL, &Event_t3625350923_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t2894641446, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t2894641446_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t602529594_marshal_pinvoke, GUIContent_t602529594_marshal_pinvoke_back, GUIContent_t602529594_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t602529594_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t3419666957, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t3419666957_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3615497928_marshal_pinvoke, GUIStyle_t3615497928_marshal_pinvoke_back, GUIStyle_t3615497928_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3615497928_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t2425398439_marshal_pinvoke, GUIStyleState_t2425398439_marshal_pinvoke_back, GUIStyleState_t2425398439_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t2425398439_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, Collision_t3299101783_marshal_pinvoke, Collision_t3299101783_marshal_pinvoke_back, Collision_t3299101783_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t3299101783_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t3853337050_marshal_pinvoke, ControllerColliderHit_t3853337050_marshal_pinvoke_back, ControllerColliderHit_t3853337050_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t3853337050_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, RaycastHit_t3372097066_marshal_pinvoke, RaycastHit_t3372097066_marshal_pinvoke_back, RaycastHit_t3372097066_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t3372097066_0_0_0 } /* UnityEngine.RaycastHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t1608056246, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t1608056246_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t1745550773, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t1745550773_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t2489343964, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t2489343964_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, RaycastResult_t1958542877_marshal_pinvoke, RaycastResult_t1958542877_marshal_pinvoke_back, RaycastResult_t1958542877_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t1958542877_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t2856906502_marshal_pinvoke, ColorTween_t2856906502_marshal_pinvoke_back, ColorTween_t2856906502_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t2856906502_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t2756583341_marshal_pinvoke, FloatTween_t2756583341_marshal_pinvoke_back, FloatTween_t2756583341_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t2756583341_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ DelegatePInvokeWrapper_OnValidateInput_t3514437264, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t3514437264_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t2227424972_marshal_pinvoke, Navigation_t2227424972_marshal_pinvoke_back, Navigation_t2227424972_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t2227424972_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t710398810_marshal_pinvoke, SpriteState_t710398810_marshal_pinvoke_back, SpriteState_t710398810_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t710398810_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ARAnchor_t2991191395_marshal_pinvoke, ARAnchor_t2991191395_marshal_pinvoke_back, ARAnchor_t2991191395_marshal_pinvoke_cleanup, NULL, NULL, &ARAnchor_t2991191395_0_0_0 } /* UnityEngine.XR.iOS.ARAnchor */,
	{ NULL, ARHitTestResult_t942592945_marshal_pinvoke, ARHitTestResult_t942592945_marshal_pinvoke_back, ARHitTestResult_t942592945_marshal_pinvoke_cleanup, NULL, NULL, &ARHitTestResult_t942592945_0_0_0 } /* UnityEngine.XR.iOS.ARHitTestResult */,
	{ NULL, ARKitSessionConfiguration_t3393651406_marshal_pinvoke, ARKitSessionConfiguration_t3393651406_marshal_pinvoke_back, ARKitSessionConfiguration_t3393651406_marshal_pinvoke_cleanup, NULL, NULL, &ARKitSessionConfiguration_t3393651406_0_0_0 } /* UnityEngine.XR.iOS.ARKitSessionConfiguration */,
	{ NULL, ARKitWorldTrackingSessionConfiguration_t3856677714_marshal_pinvoke, ARKitWorldTrackingSessionConfiguration_t3856677714_marshal_pinvoke_back, ARKitWorldTrackingSessionConfiguration_t3856677714_marshal_pinvoke_cleanup, NULL, NULL, &ARKitWorldTrackingSessionConfiguration_t3856677714_0_0_0 } /* UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration */,
	{ NULL, ARPlaneAnchor_t3727658320_marshal_pinvoke, ARPlaneAnchor_t3727658320_marshal_pinvoke_back, ARPlaneAnchor_t3727658320_marshal_pinvoke_cleanup, NULL, NULL, &ARPlaneAnchor_t3727658320_0_0_0 } /* UnityEngine.XR.iOS.ARPlaneAnchor */,
	{ NULL, ARUserAnchor_t2968539873_marshal_pinvoke, ARUserAnchor_t2968539873_marshal_pinvoke_back, ARUserAnchor_t2968539873_marshal_pinvoke_cleanup, NULL, NULL, &ARUserAnchor_t2968539873_0_0_0 } /* UnityEngine.XR.iOS.ARUserAnchor */,
	{ NULL, UnityARCamera_t3124383669_marshal_pinvoke, UnityARCamera_t3124383669_marshal_pinvoke_back, UnityARCamera_t3124383669_marshal_pinvoke_cleanup, NULL, NULL, &UnityARCamera_t3124383669_0_0_0 } /* UnityEngine.XR.iOS.UnityARCamera */,
	{ NULL, UnityARHitTestResult_t3674613141_marshal_pinvoke, UnityARHitTestResult_t3674613141_marshal_pinvoke_back, UnityARHitTestResult_t3674613141_marshal_pinvoke_cleanup, NULL, NULL, &UnityARHitTestResult_t3674613141_0_0_0 } /* UnityEngine.XR.iOS.UnityARHitTestResult */,
	{ DelegatePInvokeWrapper_ARAnchorAdded_t4244461251, NULL, NULL, NULL, NULL, NULL, &ARAnchorAdded_t4244461251_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded */,
	{ DelegatePInvokeWrapper_ARAnchorRemoved_t872492321, NULL, NULL, NULL, NULL, NULL, &ARAnchorRemoved_t872492321_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARAnchorUpdated_t532484182, NULL, NULL, NULL, NULL, NULL, &ARAnchorUpdated_t532484182_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_ARSessionCallback_t1297913645, NULL, NULL, NULL, NULL, NULL, &ARSessionCallback_t1297913645_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback */,
	{ DelegatePInvokeWrapper_ARSessionFailed_t532221400, NULL, NULL, NULL, NULL, NULL, &ARSessionFailed_t532221400_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed */,
	{ DelegatePInvokeWrapper_ARUserAnchorAdded_t3319197303, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorAdded_t3319197303_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded */,
	{ DelegatePInvokeWrapper_ARUserAnchorRemoved_t2261685995, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorRemoved_t2261685995_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARUserAnchorUpdated_t2797778030, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorUpdated_t2797778030_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARAnchorAdded_t1970944026, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorAdded_t1970944026_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3417896546, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorRemoved_t3417896546_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARAnchorUpdated_t1543169425, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorUpdated_t1543169425_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFrameUpdate_t2996439740, NULL, NULL, NULL, NULL, NULL, &internal_ARFrameUpdate_t2996439740_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate */,
	{ DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t2912316026, NULL, NULL, NULL, NULL, NULL, &internal_ARSessionTrackingChanged_t2912316026_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t442523547, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorAdded_t442523547_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t1115337844, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorRemoved_t1115337844_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t2457302376, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorUpdated_t2457302376_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated */,
	NULL,
};
